$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    });

    $(document).on('click', '.modal-ajax', function (e) {
        e.preventDefault();

        var url = $(this).attr('data-url');
        var $element = $(this);

        $.ajax({
            url: url,
            method: 'GET',
            success: function (response) {
                var $modal = $('#base-modal');

                $modal.find('.modal-body').html(response.view);
                $modal.find('h3').html(response.title);
                $modal.modal();
                // $element.trigger('after.load.modal', $modal);

                var ShowEmail = new (function () {
                    var self = this;

                    self.init = function () {
                        // code which may call other functions in self
                        submitForm('#create-contact');
                    };
                })();

                ShowEmail.init();
            },
        });
    });

    $('.add-document').on('after.load.modal', function (e, modal) {
        addDocumentEvents($(modal), '.add-document-form');
    });

    $('.add-break-btn').on('after.load.modal', function (e, modal) {
        $(modal)
            .find('.submit-btn')
            .on('click', function () {
                $('#add-break').submit();
            });

        $('.datepicker').datepicker();
        submitForm('#add-break');
    });

    $(document).on('change', '#client-inbox', function () {
        $.ajax({
            url: '/inbox/client-properties/' + $(this).val() + '/' + $('input[name=email]').val(),
            method: 'GET',
            success: function (response) {
                let dropdown = JSON.parse(response);

                $('.property-dropdown').replaceWith(dropdown);
            },
        });
    });

    $('#global-search-btn').click(function () {
        let $this = this;
        $($this).prop('disabled', true).find('span').removeClass('sr-only');

        let search = $('#global-search').val();
        $.ajax({
            url: '/global/search',
            method: 'GET',
            data: { search: search },
            success: function (response) {
                let result = JSON.parse(response);

                $('#search-results').replaceWith(result);

                $($this).prop('disabled', false).find('span').addClass('sr-only');
            },
        });
    });

    $('#global-search').keypress(function (ev) {
        if (ev.which === 13) $('#global-search-btn').click();
    });

    //pagination
    $('body').on('click', '.next-page-search, .prev-page-search', function () {
        let url = $(this).data('url');
        let search = $('#global-search').val();

        if (url) {
            $.ajax({
                url: url,
                data: {
                    url: url,
                    search: search,
                },
                success: function (response) {
                    let result = JSON.parse(response);
                    $('#search-results').replaceWith(result);

                    // $('.pagination-replace').replaceWith(response.view);
                    // $('.pagination').parent().replaceWith(response.paginationView);
                },
            });
        }
    });

    $('#find-property').keyup(function () {
        if ($(this).val().length > 2 || $(this).val().length === 0) {
            $.ajax({
                url: '/property/search',
                data: {
                    search: $(this).val(),
                },
                success: function (response) {
                    let result = JSON.parse(response);
                    $('#properties-list-replace').replaceWith(result);
                },
            });
        }
    });
    $(document).on(
        'click',
        '.services-charge, .services-charge-add-note, .insurance-add-note, .property-add-note, .utility-add-note',
        function () {
            // hide select category for property
            if ($(this).hasClass('property-add-note')) {
                $('#note_type_id.note-category').removeClass('sr-only');
            } else {
                $('#note_type_id.note-category').addClass('sr-only');
            }

            const selectedValue = $(this).data('selected');
            const selectId = $(this).data('id');
            $modal = $($(this).data('target'));
            $select = $modal.find('select');

            // notes
            let objectTypeId = $(this).data('object_type_id');
            let objectId = $(this).data('object_id');
            $('input[name=object_type_id]').val(objectTypeId);
            $('input[name=object_id]').val(objectId);

            if (selectId) {
                $select = $modal.find(`.${selectId}`);
                if ($(this).data('convert')) {
                    $select.closest('form').append('<input name="cover_note_received" value="1" type="hidden">');
                }
            }
            $select.val(selectedValue);
            $select.trigger('change');
        }
    );

    $(document).on('click', '.email-template', function () {
        const tabType = $(this).data('tab_type');
        const key = $(this).closest('td').data('key');
        const button = $(`td[data-key="${key}"] .${tabType}`);
        if (!button.length) {
            return false;
        }
        const $modal = $('#modal-new-email');
        $modal.find('#subject').val('');
        $modal.find('#message').val('');
        const templateId = $(this).data('template-id');
        const type = $(this).data('type');
        const property = $('input[name=property]').val();
        const model = $(this).data('model');

        $('#budget_type').val(templateId);
        $('#service_charge').val(button.data('object_id'));
        $('#template_property').val(property);

        $modal.find('input#subject').val('');
        $modal.find('textarea#autoexpand-body').val('');
        $modal.find('input#to').val('');
        $modal.find('input#cc').val('');
        $modal.find('div.attachments').html('');

        $.ajax({
            url: `/property/load-template-data/${templateId}`,
            data: {
                scId: button.data('object_id'),
                type,
                property,
                model: model,
            },
            success: function (response) {
                $modal.find('input#subject').val(response.subject);
                $modal.find('textarea#autoexpand-body').val(response.body);
                $modal.find('input#to').val(response.to);
                $modal.find('input#cc').val(response.cc);
                $modal.find('div.attachments').replaceWith(JSON.parse(response.attachmentsDropdown));
                $modal.find('input[name=modal_model]').val(model);

                setTimeout(() => {
                    $('textarea#autoexpand-body')
                        .each(function () {
                            this.setAttribute('style', 'height:' + this.scrollHeight + 'px;overflow-y:hidden;');
                        })
                        .on('input', function () {
                            this.style.height = 'auto';
                            this.style.height = this.scrollHeight + 'px';
                        });
                }, 500);

                $('.select2-email').select2({
                    placeholder: 'Select...',
                    allowClear: true,
                });
            },
        });
    });

    $(document).on('click', '.pagination .page-link', function (event) {
        if (!$(this).parents('nav').hasClass('default-pagination')) {
            event.stopPropagation();
            event.preventDefault();
            $url = $(this).attr('href');

            $.ajax({
                url: $url,
                success: function (response) {
                    let result = JSON.parse(response);
                    $('#properties-list-replace').replaceWith(result);
                },
            });
        }
    });

    $(document).on('submit', '#link-email-to-property', function (event) {
        $(this).find('button[type=submit]').prop('disabled', 'disabled');
    });

    $(document).on('change', '#add-email-template #attachments', function (event) {
        let attachments = [];

        // const selected = $(':selected', this);
        //
        // const optgroup = selected.closest('optgroup');
        //
        // const model = optgroup.attr('label');
        // const docId = $(this).val();

        $('#add-email-template #attachments option:selected').each(function () {
            attachments.push($(this).val());
        });

        let attachmentsStr = attachments.join(',');

        $('input[name=attachment_ids]').val(attachmentsStr);

        return false;
    });

    $(document).on('click', '.add-re-diarise', function (event) {
        const $modal = $('#reDiariseModal');
        const $this = $(this);

        const model = $this.data('model');
        const modelInstance = $this.data('object_id');
        const objectType = $this.data('object_type');
        const noteType = $this.data('selected');
        const property = $('input[name=property]').val();
        const date = $this.data('chosen_date');

        $modal.find('input[name=diary_date]').val('');
        $modal.find('textarea[name=diarise_note]').val('');

        $modal.find($('input[name=model]')).val(model);
        $modal.find($('input[name=modelInstance]')).val(modelInstance);
        $modal.find($('input[name=property_diarie]')).val(property);
        $modal.find($('input[name=object_type]')).val(objectType);
        $modal.find($('input[name=note_type]')).val(noteType);
        if (date) {
            //auto select diary date
            $modal.find('input[name=diary_date]').val(date);
        }

        $modal.modal();
    });

    $(document).on('click', '.clickable-name', function (event) {
        const email = $(this).data('email');
        const property = $('input[name=property]').val();
        const url = `/emails/create?to=${email}&property_id=${property}`;
        window.location.replace(url);
    });

    $(document).on('click', '.utility-add-note', function (event) {
        $('input[name=note_type_id]').hide;
    });

    $(document).on('change', '.note-filter', function (event) {
        let type = $(this).data('type');
        let selected = $(this).val();
        let replaceWith = $(this).data('replace-with');
        let property = $(':selected', this).data('property');

        $.ajax({
            method: 'GET',
            url: '/property/filter-note',
            data: { type: type, selected: selected, property: property },
            success: function (response) {
                let notes = JSON.parse(response);
                $(`#${replaceWith} section`).replaceWith(notes);
            },
        });
    });

    if (lengthOk()) {
        $('#search_button').attr('disabled', false);
    }

    $("input[name='searchBy']").keyup(function () {
        if (lengthOk()) {
            $('#search_button').attr('disabled', false);
        }
    });

    $('#inlineFormCustomSelect').change(function () {
        if (lengthOk()) {
            $('#searchForm').submit();
        }
    });

    $(document).on('change', '.note_type_dropdown', function (event) {
        let propertyChosen = false;

        const selected = $(':selected', this);

        if (['service charge', 'insurance', 'approval'].includes(selected.data('item-type'))) {
            const property = $('#property-inbox').val();
            const $anchor = $(this).parents('tr').find('a');
            const fileUrl = $anchor.attr('href');
            const unique = $($anchor).data('unique');
            $(this).val('');

            $.ajax({
                url: '/inbox/load-crud/',
                method: 'GET',
                data: {
                    property: property,
                    type: selected.val(),
                },
                success: function (response) {
                    const data = JSON.parse(response);
                    //
                    if (!$('#base-modal').length) {
                        $('body').append(data.modal);
                    }

                    $('#type-crud div:not(.modal)').replaceWith(data.crud);
                    $('#type-pdf div:not(.modal)').replaceWith(data.pdf);

                    eachPdf(fileUrl, 'the-canvas1');

                    $('input[name="modal_save"]').val(unique);
                    $('#modal-attach-to').modal();

                    var ShowEmail = new (function () {
                        var self = this;

                        self.init = function () {
                            // code which may call other functions in self
                            submitForm('#create-service-charge');
                            submitForm('#create-insurance');
                            submitForm('#add-payment');
                            submitForm('#create-contact');
                            $('.datepicker').datepicker();
                        };
                    })();

                    ShowEmail.init();
                },
            });
        }

        const optgroup = selected.closest('optgroup');
        const selectedValue = optgroup.data('value');

        let label = optgroup.attr('label');

        if (!optgroup.length) {
            label = $(this).find('option:selected').data('type');
            propertyChosen = true;
        }

        const selectedOptionId = $(this).val();

        $(this).parents('.attachment-row').find('select.add-new-option').removeAttr('disabled');

        // set attachment types for inbox
        $(this).parents('.note_type_replace').find('.attach_to_type').val(label);

        let selectDiv;

        if ($(this).parents('tr').length) {
            selectDiv = $(this).parents('tr').find('.document_type_select');
        } else {
            selectDiv = $('.document_type_select');
        }

        // property doesn't have optgroup, so select is different
        if (propertyChosen) {
            $('#add-document').attr('action', '/properties/' + selectedOptionId + '/documents');
        } else {
            $('#add-document').attr('action', optgroup.data('url') + selectedOptionId + '/documents');
        }

        if (selectedValue) {
            selectDiv.hide();
            selectDiv.find('select').val(selectedValue);
        } else {
            selectDiv.show();
        }
    });

    $(document).on('change', 'select#supplier_id', function () {
        if ($(this).val()) {
            var url = $(this).find(':selected').data('url');

            $('.new-contact').attr('data-url', $(this).find(':selected').data('new-contact-url'));

            $.ajax({
                url: url,
                success: function (response) {
                    changeEntityAjaxResponse(response.view, 'select#contact_id');
                },
            });
        } else {
            $('select#contact_id option').each(function () {
                if ($(this).val()) {
                    $(this).remove();
                }
            });

            $('select#contact_id').attr('disabled', true);
        }
    });

    $(document).on('click', '.attach-budget', function (event) {
        if ($(this).hasClass('approvals-upload')) {
            let selected = $(this).data('selected');
            let $modal = $('#add-document');
            $modal.attr('action', `/payments/${selected}/documents`);
            $modal
                .find('select[name=note_category]')
                .parents('.control-group')
                .hide()
                .find('.attach_to_type')
                .val('Payment');
            $modal.append('<input type="hidden" name="note_category" value="' + selected + '"/>');
        }
        $('#documentModal').modal();
    });
    $(document).on('click', '.show-note-category', function (event) {
        const $input = $('select[name=note_type_id]');

        $input.parents('.control-group').removeClass('sr-only');

        $input.val(1);

        $('#noteModal').modal();
    });

    $(document).on('click', '.add-alert', function (event) {
        let $this = $(this);
        let $modal = $('#alert-modal');
        let object_type_id = $this.data('object-type');
        let object_id = $this.data('selected');

        $modal.find('#alert-oti').val(object_type_id);
        $modal.find('#alert-oi').val(object_id);
        $modal.modal();
    });

    $(document).on('click', '.close-delete-alert', function (event) {
        let id = $(this).data('id');
        let $that = $(this);

        $that.find('span').remove();
        $that.find('i').removeClass('sr-only');

        $.ajax({
            url: '/property/alert-delete/',
            method: 'DELETE',
            data: { id: id },
            success: function (response) {
                $that.parents('.alert-warning').remove();
            },
        });
    });

    $(document).on('click', '.delete-policy-document', function (event) {
        $('#modal-delete-document').modal();
        let document = $(this).data('document_id');
        $('input[name=deleting_document]').val(document);
    });

    $(document).on('change', '#property-inbox', function (event) {
        $(this).parents('.modal-body').find('select.add-new-option').prop('disabled', 'disabled');

        $('.attachments-table').addClass('sr-only');

        const property = $(this).val();

        $.ajax({
            url: '/property/attach-to/' + property,
            method: 'GET',
            data: { property: property },
            success: function (response) {
                let dropdown = JSON.parse(response);
                $('.note_type_replace').replaceWith(dropdown);
                $('.attachments-table').removeClass('sr-only');
            },
        });
    });

    //makeIframe();
});

function makeIframe() {
    let iframe = document.getElementById('email-iframe');

    if (!iframe) {
        return false;
    }

    let doc = iframe.contentWindow.document;
    let emailBody = $('#email-body').html();
    doc.open();
    doc.write(emailBody);
    doc.close();

    $('#email-body').empty();
}

function lengthOk() {
    let $input = $('input[name=searchBy]').val();

    if ($input && $input.length > 2) {
        return true;
    }
    return false;
}

function addDocumentEvents($modal, formName) {
    $modal
        .find('input[id=document_file]')
        .off()
        .change(function () {
            var path = $(this).val();

            $modal.find('input#input').val(path.replace('C:\\fakepath\\', ''));
        });

    $modal
        .find('#input, .browse-btn')
        .off()
        .on('click', function () {
            $modal.find('input[id=document_file]').click();
        });

    formEvents($modal, formName);
}

function formEvents($modal, formName) {
    submitForm(formName);

    $modal
        .find('.submit-btn')
        .off()
        .on('click', function (e) {
            e.preventDefault();

            $modal.find('form').submit();
        });
}

function addContact(view, elementName) {
    changeEntityAjaxResponse(view, elementName);
    $('#base-modal').modal('hide');
}

function changeContact(elementName) {
    $(elementName).on('change', function () {
        if ($(this).find(':selected').hasClass('add-new')) {
            $('.new-contact').trigger('click');
        }
    });
}

$(document).on('change', 'select#contact_id', function () {
    if ($(this).find(':selected').hasClass('add-new')) {
        $('.new-contact').trigger('click');
        let modals = document.getElementsByClassName('modal-backdrop');

        modals[0].classList.add('contact-modal-z');
    }
});
$(document).on('hidden.bs.modal', '#base-modal', function (e) {
    let modals = document.getElementsByClassName('modal-backdrop');
    modals[0].classList.remove('contact-modal-z');
});

function changeEntityAjaxResponse(view, elementName) {
    $(elementName).replaceWith(view);
    $(elementName).removeAttr('disabled');

    changeContact(elementName);
}
