@extends('layouts_new.app', [
    'pageTitle' => ($contact->id ? 'Edit' : 'Add').' Contact',
    'pageNav'   => 'contact',
    'page'      => 'ContactNew',
])

@section('container')
    @include('contacts.includes.form', [
        'title'      => ($contact->id ? 'Edit' : 'New').' Contact',
        'restMethod' => $contact->id ? 'put' : 'post',
        'route'      => $contact->id ? ['contacts.update', $contact] : [$entity->createContactRoute, $entity->id]
    ])
@endsection
