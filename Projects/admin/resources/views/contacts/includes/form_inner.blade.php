 {{ Form::open(['route' => $route, 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => 'create-contact']) }}
    <div id='error-message'></div>
    <div id="first_name" class="control-group">
        {{ Form::label('first_name', 'First Name', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('first_name',$contact->first_name, array('class' => 'form-control')) }}
            <span id="first_name" class="help-block"></span>
        </div>
    </div>
    <div id="surname" class="control-group">
        {{ Form::label('surname', 'Surname', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('surname',$contact->surname, array('class' => 'form-control')) }}
            <span id="surname" class="help-block"></span>
        </div>
    </div>
    <div id="telephone" class="control-group">
        {{ Form::label('telephone', 'Telephone Number', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('telephone',$contact->telephone, array('class' => 'form-control')) }}
            <span id="telephone" class="help-block"></span>
        </div>
    </div>
    <div id="fax" class="control-group">
        {{ Form::label('fax', 'Fax Number', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('fax',$contact->fax, array('class' => 'form-control')) }}
            <span id="fax" class="help-block"></span>
        </div>
    </div>
    <div id="email" class="control-group">
        {{ Form::label('email', 'Email', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('email',$contact->email, array('class' => 'form-control')) }}
            <span id="email" class="help-block"></span>
        </div>
    </div>
    <div id="mobile" class="control-group">
        {{ Form::label('mobile', 'Mobile Number', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('mobile',$contact->mobile, array('class' => 'form-control')) }}
            <span id="mobile" class="help-block"></span>
        </div>
    </div>

    <input type="hidden" name="is_ajax" value="{{ isset($isAjax) ? $isAjax : 0 }}">

    <!-- submit button -->
    <div class="form-actions">
        <button id="save-contact" class="btn btn-block mr-2 mb-2 btn-outline-primary disable-spinner" type="submit">
            Save Changes
            <span class="spinner-border spinner-border-sm sr-only" role="status"
                  aria-hidden="true"></span>
        </button>
    </div>
{{ Form::close()}}
