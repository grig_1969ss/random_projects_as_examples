@section('container')
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-4 order-md-1">
                <div class="mb-5 text-center">
                    <h6 class="h3">{{ $contact->id ? 'Edit' : 'Add' }} Contact</h6>
                    <div class="dropdown-divider"></div>
                </div>
                @include('contacts.includes.form_inner')
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
