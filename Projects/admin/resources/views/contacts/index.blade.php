@extends('layouts_new.app', ['pageTitle' => 'Contacts', 'pageNav' => 'contact', 'page' => 'ContactsIndex'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Contacts</h2>
                    </header>
                    <table id="example-2" class="datatable table table-striped table-bordered table-hover dataTable"
                           aria-describedby="example-2_info" style="width: 880px;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Company Type</th>
                            <th>Telephone</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach ($contacts as $contact)
                            <?php
                            $type = ($contact->contactable_type == 'App\\Models\\Client') ? 'Client' : 'Managing Agent/Landlord';
                            $telephone = ($contact->mobile) ? $contact->mobile : $contact->telephone;
                            $telephone = ($telephone) ? $telephone : 'No Telephone Given';
                            $email = ($contact->email) ? $contact->email : 'No Email Given';
                            ?>
                            <tr class="odd gradeX">
                                <td>{{ Html::link('/contacts/'.$contact->id, $contact->first_name.' '.$contact->surname) }}</td>
                                <td>{{ Html::link('/contacts/'.$contact->id, (optional($contact->contactable))->getShowName() ?: 'No Company Given') }}</td>
                                <td>{{ Html::link('/contacts/'.$contact->id, $type) }}</td>
                                <td>{{ Html::link('/contacts/'.$contact->id, $telephone) }}</td>
                                <td>{{ Html::link('/contacts/'.$contact->id, $email) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->
        </div>
        <!-- /Grid row -->
    </div>
    <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
