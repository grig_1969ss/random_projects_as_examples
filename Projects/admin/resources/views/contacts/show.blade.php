@extends('layouts_new.app', ['pageTitle' => 'Contact', 'pageNav' => 'contact', 'page' => 'ContactsDetails'])

@section('container')
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="row order-md-1">
            {{-- col-md-5 col-lg-4--}}
                <div class="col-6">
                    <div class="mb-5 text-center">
                        <h6 class="h3">{{ $contact->first_name }} {{ $contact->surname }}</h6>
                        <a href="{{route('contacts.edit',$contact->id)}}" class="btn btn-alt">Edit</a>
                        <div class="dropdown-divider"></div>
                    </div>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Contact Name</dt>
                            <dd>
                                <a href="{{ $contact->contactable->getShowUrl() }}">
                                    {{ $contact->first_name }} {{ $contact->surname }}
                                </a>
                            </dd>
                            <dt>Address</dt>
                            <dd>{{ $contact->contactable->address->address_1 }}</dd>
                            <dd>{{ $contact->contactable->address->address_2 }}</dd>
                            <dd>{{ $contact->contactable->address->town }}</dd>
                            <dd>{{ $contact->contactable->address->county }}</dd>
                            <dd>{{ $contact->contactable->address->post_code }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ Util::showValue($contact->telephone) }}</dd>
                            <dd>{{ Util::showValue($contact->mobile) }}</dd>
                            @if($contact->fax)
                                <dt>Fax</dt>
                                <dd>{{ $contact->fax }}</dd>
                            @endif
                            <dt>Email</dt>
                            <dd>{!! Util::emailLink($contact->email) !!}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($contact->created_at) }}
                                by {{ $contact->user->name }}</dd>
                        </dl>
                    </section>
                </div>


                <div class="col-6">
                    @include('partials.show_notes', ['id' => $contact->id])
                </div>
            </div>


            @include('partials.add_comment', [
                'id' => $contact->id,
                'type' => 'contacts',
                'route' => ['contacts.notes.store', $contact]
            ])
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
