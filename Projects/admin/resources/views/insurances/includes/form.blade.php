<div class="section section-sm">
    <div id="template-page" class="container-fluid">
        <div class="col-12 mb-4 d-flex justify-content-between">
            <div class="data-container">
                <header>
                    <h2>{{ $title }}</h2>
                </header>
                <div>
                    <form method="{{ $restMethod }}"
                          action="{{ $insurance->id ? route('insurances.update', $insurance) : route('properties.insurances.store', $property) }}"
                          class="form-horizontal"
                          id="create-insurance">

                        @if($insurance->id)
                            {{ method_field('PUT') }}
                        @endif

                        @csrf
                        <div id='error-message'></div>

                        <input type="hidden" name="modal_save" value="">

                        <div id="insurance_renewal_date" class="control-group">
                            <label for="insurance_renewal_date" class="control-label">Renewal Date</label>
                            <div class="form-group controls">
                                <div class="input-append">
                                    <input type="text"
                                           name="insurance_renewal_date"
                                           id="insurance_renewal_date"
                                           value="{{ $insurance->present()->renewalDate('') }}"
                                           class="datepicker form-control"
                                           data-date-format="dd/mm/yyyy"
                                           autocomplete="off">
                                    <span class="add-on"><i class="awe-calendar"></i></span>
                                </div>
                                <span id="insurance_renewal_date" class="help-block"></span>
                            </div>
                        </div>
                        <div id="covered_by" class="control-group">
                            <label for="covered_by" class="control-label">Covered By</label>
                            <div class="form-group controls">
                                <select name="covered_by" id="covered_by" class="form-control add-new-option">
                                    <option value="">Select...</option>
                                    @foreach($insuranceParties as $insuranceParty)
                                        <option value="{{ $insuranceParty->id }}"
                                            {{ $insurance->covered_by == $insuranceParty->id ? 'selected' : '' }}>
                                            {{ $insuranceParty->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span id="covered_by" class="help-block"></span>
                            </div>
                        </div>
                        <div id="insurance_premium" class="control-group">
                            <label for="insurance_premium" class="control-label">Insurance Premium</label>
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                                    <input type="text"
                                           name="insurance_premium"
                                           id="insurance_premium"
                                           class="form-control"
                                           value="{{ $insurance->insurance_premium }}">
                                </div>
                                <span id="insurance_premium" class="help-block"></span>
                            </div>
                        </div>
                        <div id="insurer" class="control-group">
                            <label for="insurer" class="control-label">Insurer</label>
                            <div class="form-group controls">
                                <input type="text"
                                       name="insurer"
                                       id="insurer"
                                       class="form-control"
                                       value="{{ $insurance->insurer }}">
                                <span id="insurer" class="help-block"></span>
                            </div>
                        </div>
                        <div id="loss_of_rent" class="control-group">
                            <label for="loss_of_rent" class="control-label">Loss of Rent</label>
                            <div class="form-group controls">
                                <div class="input-append">
                                    <input type="text"
                                           name="loss_of_rent"
                                           id="loss_of_rent"
                                           class="form-control"
                                           value="{{ $insurance->loss_of_rent }}">
                                    <span class="add-on">years</span>
                                </div>
                                <span id="loss_of_rent" class="help-block"></span>
                            </div>
                        </div>
                        <div id="terrorism_cover" class="control-group">
                            <label for="terrorism_cover" class="control-label">Terrorism Covered</label>
                            <div class="controls">
                                <input type="hidden" name="terrorism_cover" value="0">
                                <input type="checkbox"
                                       name="terrorism_cover"
                                       id="terrorism_cover"
                                       value="1"
                                    {{ $insurance->terrorism_cover ? 'checked' : '' }}>
                                <span id="terrorism_cover" class="help-block"></span>
                            </div>
                        </div>
                        <div id="terrorism_premium" class="control-group">
                            <label for="terrorism_premium" class="control-label">Terrorism Premium</label>
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                                    <input type="text"
                                           name="terrorism_premium"
                                           id="terrorism_premium"
                                           class="form-control"
                                           value="{{ $insurance->terrorism_premium }}">
                                </div>
                                <span id="terrorism_premium" class="help-block"></span>
                            </div>
                        </div>
                        <div id="period_of_insurance" class="control-group">
                            <label for="period_of_insurance" class="control-label">Period of Insurance</label>
                            <div class="form-group controls">
                                <input type="text"
                                       name="period_of_insurance"
                                       id="period_of_insurance"
                                       class="form-control"
                                       value="{{ $insurance->period_of_insurance }}">
                                <span id="period_of_insurance" class="help-block"></span>
                            </div>
                        </div>
                        <div id="" class="control-group">
                            <label for="cover_note_received" class="control-label">Cover Note Received</label>
                            <div class="form-group controls">
                                <select name="cover_note_received" id="cover_note_received" class="form-control">
                                    <option @if(!$insurance->cover_note_received) selected @endif value="0">No
                                    </option>
                                    <option @if($insurance->cover_note_received) selected @endif value="1">Yes
                                    </option>
                                </select>
                                <span id="note_received" class="help-block"></span>
                            </div>
                        </div>
                        <div id="notes" class="control-group">
                            <label for="notes" class="control-label">Note</label>
                            <div class="form-group controls">
                                <textarea name="notes"
                                          id="notes"
                                          class="form-control">{{ $insurance->notes }}</textarea>
                                <span id="notes" class="help-block"></span>
                            </div>
                        </div>
                        <div id="diary_date" class="control-group sr-only">
                            <label for="diary_date" class="control-label">Diary</label>
                            <div class="form-group controls">
                                <div class="input-append">
                                    <input type="text"
                                           name="diary_date"
                                           id="diary_date"
                                           value="{{ $insurance->present()->diaryDate('') }}"
                                           class="datepicker form-control"
                                           data-date-format="dd/mm/yyyy"
                                           autocomplete="off">
                                    <span class="add-on"><i class="awe-calendar"></i></span>
                                </div>
                                <span id="diary_date" class="help-block"></span>
                            </div>
                        </div>


                        <div id="block_policy" class="control-group">
                            <label for="block_policy" class="control-label">Block Policy</label>
                            <div class="controls">
                                <input type="hidden" name="block_policy" value="0">
                                <input type="checkbox"
                                       name="block_policy"
                                       id="block_policy"
                                       value="1"
                                    {{ $insurance->block_policy ? 'checked' : '' }}>
                                <span id="block_policy" class="help-block"></span>
                            </div>
                        </div>
                        <div id="plate_glass" class="control-group">
                            <label for="plate_glass" class="control-label">Plate Glass</label>
                            <div class="form-group controls">
                                <select class="form-control" name="plate_glass" id="plate_glass">
                                    <option value="">Select...</option>
                                    @foreach($insuranceParties as $insuranceParty)
                                        <option value="{{ $insuranceParty->id }}"
                                            {{ $insurance->plate_glass == $insuranceParty->id ? 'selected' : '' }}>
                                            {{ $insuranceParty->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span id="plate_glass" class="help-block"></span>
                            </div>
                        </div>
                        <div id="tenant_reimburses" class="control-group">
                            <label for="tenant_reimburses" class="control-label">Tenant Reimburses</label>
                            <div class="controls">
                                <input type="hidden" name="tenant_reimburses" value="0">
                                <input type="checkbox"
                                       name="tenant_reimburses"
                                       id="tenant_reimburses"
                                       value="1"
                                    {{ $insurance->tenant_reimburses ? 'checked' : '' }}>
                                <span id="tenant_reimburses" class="help-block"></span>
                            </div>
                        </div>
                        <div id="valuation_fee_payable" class="control-group">
                            <label for="valuation_fee_payable" class="control-label">Valuation Fee
                                Payable</label>
                            <div class="controls">
                                <input type="hidden" name="valuation_fee_payable" value="0">
                                <input type="checkbox"
                                       name="valuation_fee_payable"
                                       id="valuation_fee_payable"
                                       value="1"
                                    {{ $insurance->valuation_fee_payable ? 'checked' : '' }}>
                                <span id="valuation_fee_payable" class="help-block"></span>
                            </div>
                        </div>

                        <!-- submit button -->
                        <div class="form-actions">
                            <button class="btn btn-large btn-primary disable-spinner" type="submit">
                                Save Changes
                                <span class="spinner-border spinner-border-sm sr-only" role="status"
                                      aria-hidden="true"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush


