@extends('layouts_new.app', [
    'pageTitle' => ($insurance->id ? 'Edit' : 'Add').' Insurance',
    'pageNav' => 'property',
    'page' => 'PropertyInsurance'
])

@section('container')
    @include('insurances.includes.form', [
        'title' => ($insurance->id ? 'Edit' : 'Add').' Insurance',
        'restMethod' => ($insurance->id ? 'PUT' : 'POST')
    ])
@endsection