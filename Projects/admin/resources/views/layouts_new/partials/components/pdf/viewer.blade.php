@include('layouts_new.partials.components.pdf.container')

@push('js')

    <script>
        $(document).on('click', '.insurance-pdf-preview', function (event) {
            let $this = $(this);
            let $theCanvas = $('#the-canvas');
            $theCanvas.addClass('sr-only');
            $this.find('.insurance-spinner').removeClass('sr-only');
            $('#the-canvas1 #the-canvas1').empty();
            let url = $this.data('href');

            eachPdf(url, 'the-canvas1', $this, true);

            $('#insurance-table-preview').addClass(' col-md-6 col-lg-6');
            $theCanvas.addClass(' col-md-6 col-lg-6');
        });

        $(document).on('click', '.hide-pdf-preview', function (event) {
            let $theCanvas = $('#the-canvas');
            $('#the-canvas1').html('');
            $theCanvas.addClass('sr-only');
            $(this).addClass('sr-only');

            $('#insurance-table-preview').removeClass(' col-md-6 col-lg-6');
            $theCanvas.removeClass(' col-md-6 col-lg-6');
        });


    </script>

@endpush


