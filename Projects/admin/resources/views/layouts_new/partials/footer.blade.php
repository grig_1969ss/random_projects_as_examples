<footer class="footer bg-primary text-white py-6 overflow-hidden">
    <div class="container-fluid">
        <div class="content">
            <div class="row">
                <div class="col-md-5"><a class="footer-brand mr-lg-5" href="../../index.html"><img
                            src="{{asset('dist/img/brand/light.svg')}}" alt="Footer logo"></a>
                    <p class="mt-4 text-gray">Because every Pixel matters.</p>
                    <p class="text-gray">Follow us to get fresh updates and free tutorials:</p>
                    <ul class="social-buttons mb-5 mb-lg-0">
                        <li><a href="https://twitter.com/themesberg" target="_blank"
                               class="btn btn-lg btn-link btn-twitter text-white" data-toggle="tooltip"
                               data-placement="top" title="Follow us on Twitter"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/Themesberg-214738555737136/" target="_blank"
                               class="btn btn-lg btn-link btn-facebook text-white" data-toggle="tooltip"
                               data-placement="top" title="Like us on Facebook"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="https://github.com/themesberg" target="_blank"
                               class="btn btn-lg btn-link btn-github text-white" data-toggle="tooltip"
                               data-placement="top" title="Star us on Github"><i class="fab fa-github"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCEl11YeRhWQ-iFS0naimRsA" target="_blank"
                               class="btn btn-lg btn-link btn-youtube text-white" data-toggle="tooltip"
                               data-placement="top" title="Watch tutorials"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-2 mb-5 mb-lg-0"><h6>Themesberg</h6>
                    <ul class="links-vertical">
                        <li><a class="text-gray" href="https://themesberg.com/blog" target="_blank">Blog</a></li>
                        <li><a class="text-gray" href="https://themesberg.com/why-our-themes" target="_blank">Why our
                                themes?</a></li>
                        <li><a class="text-gray" href="https://themesberg.com/about" target="_blank">About Us</a></li>
                        <li><a class="text-gray" href="https://themesberg.com/contact" target="_blank">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 col-md-2 mb-5 mb-lg-0"><h6>Pages</h6>
                    <ul class="links-vertical">
                        <li><a class="text-gray" href="../../index.html">Home</a></li>
                        <li><a class="text-gray" href="../pages/blog-right-sidebar.html">Blog</a></li>
                        <li><a class="text-gray" href="../pages/about-startup.html">About</a></li>
                        <li><a class="text-gray" href="../pages/about-team.html">Team</a></li>
                        <li><a class="text-gray" href="../pages/contact.html">Contact</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 mb-5 mb-lg-0"><h6>Other</h6>
                    <ul class="links-vertical">
                        <li><a class="text-gray" href="../../docs/introduction.html">Documentation <span
                                    class="badge badge-sm badge-secondary ml-2">v1.0</span></a></li>
                        <li><a class="text-gray" target="_blank" href="../../docs/changelog.html">Changelog</a></li>
                        <li><a class="text-gray" target="_blank" href="#">Support</a></li>
                        <li><a class="text-gray" target="_blank" href="https://themesberg.com/licensing">License</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="copyright mt-4 text-center">&#xA9;<script>document.write(new Date().getFullYear())</script>
            <a href="https://themesberg.com" target="_blank">Themesberg</a>. All rights reserved.
        </div>
    </div>
</footer><!-- End of Footer --><!-- Core -->
