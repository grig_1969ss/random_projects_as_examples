<header class="header-global">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-dark navbar-theme-dark mb-4">
        <div class="container-fluid position-relative menu-wrapper">
            <a class="navbar-brand mr-lg-5" href="/">
                <img class="navbar-brand-dark" src="{{asset('/img/moved/icons/light.svg')}}" alt="CAPA Logo">
                @php
                    $auth = auth()->user();
                    $exists = ($auth->clients->where('id', $auth->client_id)->count() > 0);
                    $activeClient = $auth->clients->where('id', $auth->client_id)->first();
                    $logo = $activeClient ? 'img/moved/logos/'. $activeClient->logo : null;
                @endphp
                @if($auth->clients && $activeClient && $exists && file_exists($logo))
                    <img class="navbar-brand-client pl-2"
                         src="{{asset($logo)}}"
                         alt="Client Logo">
                @endif

                <img class="navbar-brand-light" src="{{asset('img/moved/icons/dark.svg')}}" alt="menuimage">

            </a>
            <div class="navbar-collapse collapse" id="navbar-dark-profile">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="/">
                                <img src="{{asset('img/moved/icons/dark.svg')}}" alt="menuimage">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <i class="fas fa-times" data-toggle="collapse" role="button"
                               data-target="#navbar-dark-profile" aria-controls="navbar-dark-profile"
                               aria-expanded="false" aria-label="Toggle navigation"></i>
                        </div>
                    </div>
                </div>
                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                    @if(!auth()->user()->hasRole('client'))
                        <li class="nav-item dropdown">
                            <a href="{{route('inbox.index')}}" class="nav-link" role="button">
                            <span class="nav-link-inner-text">Inbox
                                @if($allEmails)
                                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="All Emails"
                                          class="badge badge-secondary">{{$allEmails}}</span>
                                @endif
                                @if($myEmails)
                                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="My Emails"
                                          class="badge badge-tertiary">{{$myEmails}}</span>
                                @endif
                            </span>
                            </a>
                        </li>
                    @endif

                    @if(!auth()->user()->hasRole('client'))
                        <li class="nav-item dropdown">
                            <a href="{{route('diary.index')}}" class="nav-link" role="button">
                                <span class="nav-link-inner-text">Diary</span>
                            </a>
                        </li>
                    @endif

                    <li class="nav-item dropdown">
                        <a href="{{route('properties.index')}}" class="nav-link" role="button">
                            <span class="nav-link-inner-text">Properties</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link" data-toggle="dropdown" role="button">
                            <i class="fas fa-angle-down nav-link-arrow"></i>
                            <span class="nav-link-inner-text">Contacts</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('agents.index')}}" class="dropdown-item">
                                    Managing Agents
                                </a>
                                <a href="{{ route('landlords.index') }}" class="dropdown-item">
                                    Landlords
                                </a>
                                @if(!auth()->user()->hasRole('client'))
                                    <a href="{{ route('clients.index') }}" class="dropdown-item">
                                        Clients
                                    </a>
                                @endif
                                <a href="{{ route('suppliers.index') }}" class="dropdown-item">
                                    Suppliers
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown mega-dropdown">
                        @if(!auth()->user()->hasRole('client'))
                            <a href="#" class="nav-link" data-toggle="dropdown" role="button">
                                <i class="fas fa-angle-down nav-link-arrow"></i>
                                <span class="nav-link-inner-text">Reports</span>
                            </a>
                        @endif
                        <div class="dropdown-menu">
                            <div class="row">
                                <div class="col pl-0">
                                    <ul class="list-style-none">
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/approvals') }}">Approvals</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/weekly-store-approvals') }}">Weekly Store Approvals</a>
                                        </li><li><a class="dropdown-item"
                                               href="{{ url('reports/new/weekly-rent-invoices') }}">Weekly Rent invoices</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/invoices-on-hold') }}">Invoices On Hold</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/annual-service-charge') }}">Annual Service Charge</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/annual-insurance') }}">Annual Insurance</a></li>
                                    </ul>
                                </div>
                                <div class="col pl-0">
                                    <ul class="list-style-none">
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/missing-service-charge-budgets') }}">Missing Service Charge Budgets</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/missing-service-charge-reconciliations') }}">Missing Service Charge Reconciliations</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/missing-insurance') }}">Missing Insurance</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/service-charge-and-insurance-renewal-dates') }}">Service charge & Insurance renewal Dates</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/missing-letters-to-franchisee') }}">Missing Letters to Franchisee</a></li>
                                        <li><a class="dropdown-item"
                                               href="{{ url('reports/new/missing-real-estate-change-form') }}">Missing Real Estate Change Form</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </li>

                    {{--<li class="nav-item dropdown mega-dropdown">--}}
                            {{--@if(!auth()->user()->hasRole('client'))--}}
                                {{--<a href="#" class="nav-link" data-toggle="dropdown" role="button">--}}
                                    {{--<i class="fas fa-angle-down nav-link-arrow"></i>--}}
                                    {{--<span class="nav-link-inner-text">Old Reports</span>--}}
                                {{--</a>--}}
                            {{--@endif--}}
                            {{--<div class="dropdown-menu">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col pl-0">--}}
                                        {{--<ul class="list-style-none">--}}
                                            {{--<li><a class="dropdown-item"--}}
                                                   {{--href="{{ route('reports.properties') }}">Properties</a></li>--}}
                                            {{--<li><a class="dropdown-item"--}}
                                                   {{--href="{{ route('reports.approvals') }}">Approvals</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.master-spreadsheet') }}">Master--}}
                                                    {{--Spreadsheet</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.rent-review') }}">Rent--}}
                                                    {{--Reviews</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.lease-expiry') }}">Lease--}}
                                                    {{--Expiries</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.rateable-value') }}">Rateable--}}
                                                    {{--Value</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.break-option') }}">Break--}}
                                                    {{--Option</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.condition') }}">Schedule Of--}}
                                                    {{--Condition</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.document-checklist') }}">Document--}}
                                                    {{--Checklist</a></li>--}}
                                            {{--<li><a class="dropdown-item"--}}
                                                   {{--href="{{ route('reports.approvals-by-property') }}">Approvals by--}}
                                                    {{--Property</a></li>--}}
                                            {{--<li><a class="dropdown-item" href="{{ route('reports.invoices-on-hold') }}">Invoices--}}
                                                    {{--on Hold</a></li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.maintenance') }}" class="dropdown-item">--}}
                                                    {{--Outstanding Maintenance--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                    {{--<div class="col pl-0">--}}
                                        {{--<ul class="list-style-none">--}}
                                            {{--<li>--}}
                                                {{--<a href="{{route('properties.overview', Carbon\Carbon::now()->addYear()->isoFormat('YYYY-MM-DD'))}}"--}}
                                                   {{--class="dropdown-item">--}}
                                                    {{--Property Record Overview--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.rental-liability') }}" class="dropdown-item">--}}
                                                    {{--Rent--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.budget')}}" class="dropdown-item">--}}
                                                    {{--Comparison on SC Budgets--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.actual')}}" class="dropdown-item">--}}
                                                    {{--Comparison on Annual Charges--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.missing',['budget']) }}" class="dropdown-item">--}}
                                                    {{--Missing Budgets--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.missingrecs',['actual']) }}"--}}
                                                   {{--class="dropdown-item">--}}
                                                    {{--Missing Reconciliations--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.service-charge') }}" class="dropdown-item">--}}
                                                    {{--Service Charge--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.on-account') }}" class="dropdown-item">--}}
                                                    {{--On Account (Quarterly)--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="{{ route('reports.insurance-renewal-dates') }}"--}}
                                                   {{--class="dropdown-item">--}}
                                                    {{--Insurance Renewal Dates--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li><a class="dropdown-item"--}}
                                                   {{--href="{{ route('reports.service-charge-annual-budget') }}">Service Charge--}}
                                                    {{--Annual Budget</a>--}}
                                            {{--</li>--}}
                                            {{--<li><a class="dropdown-item"--}}
                                                   {{--href="{{ route('reports.insurance-annual-budget') }}">Insurance Annual--}}
                                                    {{--Budget</a>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}


                    @php
                        $propertyID = Util::propertyId();
                    @endphp

                    @if($propertyID)
                        @if(!auth()->user()->hasRole('client'))
                            <li class="nav-item dropdown">
                                <div class="btn-group">
                                    <button type="button"
                                            class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-angle-down dropdown-arrow"></i>
                                        <span><i class="fas fa-plus dropdown-arrow"></i></span>
                                    </button>

                                    <div class="dropdown-menu" id="action-dropdown" x-placement="top-start"
                                         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -197px, 0px);">

                                        <a class="dropdown-item"
                                           href="{{route('properties.service-charges.create', $propertyID)}}">Add
                                            Service
                                            Charge</a>
                                        <a class="dropdown-item"
                                           href="{{route('properties.insurances.create', $propertyID)}}">Add
                                            Insurance</a>
                                        <a class="dropdown-item"
                                           href="{{route('properties.payments.create', $propertyID)}}">Add
                                            Approval</a>
                                        <a href="{{route('properties.rent-reviews.create', $propertyID)}}"
                                           class="dropdown-item">Add Rent Review</a>
                                        <a href="{{route('properties.client-queries.create', $propertyID)}}"
                                           class="dropdown-item">Add Client Queries</a>
                                        <a href="{{route('properties.maintenances.create', $propertyID)}}"
                                           class="dropdown-item">Add Maintenance</a>
                                        <a href="{{route('properties.breaks.create', $propertyID)}}"
                                           class="dropdown-item">Add Break Option</a>
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>

            <div class="d-flex align-items-center">
                <ul class="flex-row list-style-none mr-4 d-none d-sm-flex">
                    @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_maintenance)
                        <li class="mr-2 navbar-icon-item">
                            <a href="{{route('reports.maintenance')}}">
                                <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle mr-2"
                                        type="button"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-wrench"></span>
                                </button>
                                <span
                                    class="badge badge-pill badge-success navbar-icon-item_number"><?= is_numeric($activeMaintenanceCount) ? $activeMaintenanceCount : $activeMaintenanceCount ?>
                             </span>
                            </a>
                        </li>
                    @endif

                    @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_rent_reviews)
                        <li class="mr-2 navbar-icon-item">
                            <a href="{{route('reports.rent-review')}}">
                                <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle mr-2"
                                        type="button"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span class="far fa-calendar"></span>
                                </button>
                                <span
                                    class="badge badge-pill badge-success navbar-icon-item_number"><?= is_numeric($rentReviewDueCount) ? $rentReviewDueCount : 0 ?>
                        </span>
                            </a>
                        </li>
                    @endif

                    @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_lease)
                        <li class="mr-2 navbar-icon-item">
                            <a href="{{route('reports.lease-expiry')}}">
                                <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle mr-2"
                                        type="button"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-calendar-week"></span>
                                </button>
                                <span.
                                    class="badge badge-pill badge-success navbar-icon-item_number"><?= is_numeric($leaseExpiryCount) ? $leaseExpiryCount : 0 ?>
                                </span.>
                            </a>
                        </li>
                    @endif
                    @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_approvals)
                        <li class="mr-2 navbar-icon-item">
                            <a href="{{route('payments.index')}}">
                                <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle mr-2"
                                        type="button"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-edit"></span>
                                </button>
                                <span.
                                    class="badge badge-pill badge-success navbar-icon-item_number"><?= is_numeric($approvalsCount) ? $approvalsCount : 0 ?>
                                </span.>
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="dropdown pl-1">
                    <div class="d-flex align-items-center" id="dropdownMenuButton" data-toggle="dropdown"
                         aria-expanded="false">
                        <div>
                            <p class="font-small text-light m-0">{{auth()->user()->name}}</p>
                            <p class="font-small text-light m-0">{{auth()->user()->client->client_name ?? ''}}</p>
                        </div>
                        <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle ml-2" type="button"
                                aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-user"></span>
                        </button>
                    </div>
                    <div class="dropdown-menu dropdown-menu-md" aria-labelledby="dropdownMenuButton">
                        <h6 class="dropdown-header">CAPA Property</h6>

                        <a class="dropdown-item" href="{{url('account/switch-client')}}">Switch Client</a>
                        @if(auth()->user()->hasRole('superAdmin'))
                            <a class="dropdown-item" href="{{url('account/settings')}}">Settings</a>
                            <a class="dropdown-item" href="{{ url('settings/staff') }}">Users</a>
                        @endif

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" onclick="$('#logout').submit()"><i
                                class="fas fa-sign-out-alt mr-2"></i>Sign out</a>
                    </div>
                    <form id="logout" action="{{ route('logout') }}" method="POST" hidden>
                        @csrf
                        <button type="submit">
                            <span>Logout</span>
                        </button>
                    </form>
                </div>
                <button class="navbar-toggler ml-2" type="button" data-toggle="collapse"
                        data-target="#navbar-dark-profile" aria-controls="navbar-dark-profile" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
</header>


{{--Modal--}}
<div class="modal fade " id="modal-search" tabindex="-1" role="dialog"
     aria-labelledby="modal-default"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Search</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <div class="d-flex flex-row mt-5 justify-content-center">

                        <div class="input-group">
                            <input id="global-search" class="form-control form-control-xl border-0 " type="text">
                            <div class="input-group-prepend">
                                <button id="global-search-btn" type="button" class="btn btn-primary">Search
                                    <span class="spinner-border spinner-border-sm sr-only" role="status"
                                          aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="mt-2 ml-1">
                        <div id="search-results"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
