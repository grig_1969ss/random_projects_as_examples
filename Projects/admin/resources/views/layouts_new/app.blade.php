<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ site_title($pageTitle) }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="description" content="{{ config('app.meta_description') }}">
    <meta name="author" content="Up Technology Ltd"><!-- Favicon -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('dist/img/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('dist/img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('dist/img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('dist/img/favicon/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('dist/img/favicon/safari-pinned-tab.svg')}}" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="{{ mix('/css/all.css') }}" rel="stylesheet">
    @stack('css')
</head>
<body data-page="{{ $page }}">

@include('layouts_new.partials.header')

<main>
    @yield('container')
    {{--@include('layouts_new.partials.footer')--}}
</main>


{{--@include('layouts_new.partials.footer')--}}
{{--<script src="{{asset('/js/jquery-3.4.1.min.js')}}"></script>--}}
<script src="{{asset('dist/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('dist/vendor/popper/popper.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dist/vendor/headroom/headroom.min.js')}}"></script><!-- Vendor JS -->
<script src="{{asset('dist/vendor/onscreen/onscreen.min.js')}}"></script>
<script src="{{asset('dist/vendor/nouislider/js/nouislider.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('dist/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('dist/vendor/owl-carousel/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('dist/vendor/parallax/jarallax.min.js')}}"></script>
<script src="{{asset('dist/vendor/smooth-scroll/smooth-scroll.polyfills.min.js')}}"></script>
<script src="{{asset('dist/vendor/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('dist/vendor/countdown/jquery.countdown.min.js')}}"></script>
<script src="{{asset('dist/vendor/prism/prism.js')}}"></script><!-- pixel JS -->
<script src="{{asset('dist/js/pixel.js')}}"></script>
<script src="{{asset('vendor/moment.js')}}"></script>
<script src="{{asset('js/moved/scripts.js')}}"></script>
<script src="{{mix('js/moved/custom.js')}}"></script>
<script src="{{asset('js/select2.full.min.js')}}"></script>
<script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
<script src="{{asset('dist/vendor/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>


@stack('js')

<script type="text/javascript">

    $(document).ready(function () {
        $.fn.datetimepicker.Constructor.Default = $.extend({},
            $.fn.datetimepicker.Constructor.Default,
            {
                icons:
                    {
                        time: 'fas fa-clock',
                        date: 'fas fa-calendar',
                        up: 'fas fa-arrow-up',
                        down: 'fas fa-arrow-down',
                        previous: 'fas fa-arrow-circle-left',
                        next: 'fas fa-arrow-circle-right',
                        today: 'far fa-calendar-check-o',
                        clear: 'fas fa-trash',
                        close: 'far fa-times'
                    }
            });


        let datetimepicker = $('.reminder-datetimepicker').find('input[name=datetime]');
        datetimepicker.addClass('datetimepicker-input');
        datetimepicker.attr('data-toggle', 'datetimepicker');

        $('#datetimepicker2').datetimepicker({
            language: 'en',
            pick12HourFormat: true,
        });

        // $('.datepicker').datepicker({
        //     format: 'dd/mm/yyyy'
        // });

    });
</script>


</body>
</html>
