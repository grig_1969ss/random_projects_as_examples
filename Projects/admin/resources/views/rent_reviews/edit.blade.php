@extends('layouts_new.app', [
    'pageTitle' => ($rentReview->id ? 'Edit' : 'Add').' Rent Review',
    'pageNav' => 'property',
    'page' => 'PropertyRentReview'
])

@section('container')
    @include('rent_reviews.includes.form', [
        'title' => ($rentReview->id ? 'Edit' : 'Add').' Rent Review',
        'restMethod' => ($rentReview->id ? 'PUT' : 'POST')
    ])
@endsection