@php
   $date = ($rentReview->date) ? Date::dmY($rentReview->date) : null;
   $notice_dates = ($rentReview->notice_dates) ? Date::dmY($rentReview->notice_dates) : null;

@endphp
<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-md-5 col-lg-4 order-md-1">
            <div class="mb-5 text-center">
                <h6 class="h3">{{ $title }}</h6>
                <div class="dropdown-divider"></div>
            </div>
            {{ Form::open([
                'route' => ($rentReview->id ? ['rent-reviews.update', $rentReview] : ['properties.rent-reviews.store', $property]),
                'method' => $restMethod,
                'class' => 'form-horizontal', 'id' => 'create-rent-review'
            ]) }}
            <div id='error-message'></div>
            <div id="break_option" class="control-group">
                {{ Form::label('break_option', 'Is there a Break Option', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::select('break_option', array(null => 'Select...', 'Yes' => 'Yes', 'No' => 'No'), $rentReview->break_option ,['class'=>'form-control']) }}
                    <span id="break_option" class="help-block"></span>
                </div>
            </div>
            <div id="no_floors" class="control-group">
                {{ Form::label('no_floors', 'No. of Floors', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::text('no_floors',$rentReview->no_floors, array('class' => 'form-control')) }}
                    <span id="no_floors" class="help-block"></span>
                </div>
            </div>
            <div id="time_of_essence" class="control-group">
                {{ Form::label('floor_areas_confirmed', 'Floor areas confirmed', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::checkbox('floor_areas_confirmed', '1', $rentReview->floor_areas_confirmed)}}
                    <span id="time_of_essence" class="help-block"></span>
                </div>
            </div>
            <div id="itza" class="control-group">
                {{ Form::label('itza', 'ITZA', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::text('itza',$rentReview->itza, array('class' => 'form-control')) }}
                    <span id="itza" class="help-block"></span>
                </div>
            </div>
            <div id="floors_occupied" class="control-group">
                {{ Form::label('floors_occupied', 'Breakdown of Areas', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::text('floors_occupied',$rentReview->floors_occupied, array('class' => 'form-control')) }}
                    <span id="floors_occupied" class="help-block"></span>
                </div>
            </div>
            <div id="rent_review_pattern" class="control-group">
                {{ Form::label('rent_review_pattern', 'Rent Review Pattern', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::text('rent_review_pattern',$rentReview->rent_review_pattern, array('class' => 'form-control')) }}
                    <span id="rent_review_pattern" class="help-block"></span>
                </div>
            </div>
            <div id="date" class="control-group">
                {{ Form::label('date', 'Date', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-append">
                        {{ Form::text('date',$date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                        <span class="add-on"><i class="awe-calendar"></i></span>
                    </div>
                    <span id="date" class="help-block"></span>
                </div>
            </div>
            <div id="rent" class="control-group">
                {{ Form::label('rent', 'Rent', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-prepend">
                        <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                        {{ Form::text('rent',$rentReview->rent, array('class' => 'form-control')) }}
                    </div>
                    <span id="rent" class="help-block"></span>
                </div>
            </div>
            <div id="arbitrator_or_expert" class="control-group">
                {{ Form::label('arbitrator_or_expert', 'Arbitrator / Expert', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::select('arbitrator_or_expert', array(null => 'Select...', 'Arbitrator' => 'Arbitrator', 'Expert' => 'Expert'), $rentReview->arbitrator_or_expert,['class'=> 'form-control']) }}
                    <span id="arbitrator_or_expert" class="help-block"></span>
                </div>
            </div>
            <div id="time_of_essence" class="control-group">
                {{ Form::label('time_of_essence', 'Time of Essence', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::checkbox('time_of_essence', '1', $rentReview->time_of_essence)}}
                    <span id="time_of_essence" class="help-block"></span>
                </div>
            </div>
            <div id="interest" class="control-group">
                {{ Form::label('interest', 'Interest', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::checkbox('interest', '1', $rentReview->interest)}}
                    <span id="interest" class="help-block"></span>
                </div>
            </div>
            <div id="bank_id" class="control-group">
                {{ Form::label('bank_id', 'Bank', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::text('bank_id',$rentReview->bank_id, array('class' => 'form-control')) }}
                    <span id="bank_id" class="help-block"></span>
                </div>
            </div>
            <div id="base_rate" class="control-group">
                {{ Form::label('base_rate', 'Base Rate', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-prepend">
                        <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                        {{ Form::text('base_rate',$rentReview->base_rate, array('class' => 'form-control')) }}
                    </div>
                    <span id="base_rate" class="help-block"></span>
                </div>
            </div>
        <!-- <div id="gross_area" class="control-group">
			{{ Form::label('gross_area', 'Gross Area', array('class' => 'control-label')) }}
            <div class="controls">
{{ Form::text('gross_area',$rentReview->gross_area, array('class' => 'input-xlarge')) }}
            <span id="gross_area" class="help-block"></span>
        </div>
    </div>
    <div id="net_area" class="control-group">
{{ Form::label('net_area', 'Net Area', array('class' => 'control-label')) }}
            <div class="controls">
{{ Form::text('net_area',$rentReview->net_area, array('class' => 'form-control')) }}
            <span id="net_area" class="help-block"></span>
        </div>
    </div> -->
            <div id="notice_by" class="control-group">
                {{ Form::label('notice_by', 'Notice By', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::select('notice_by', array(null => 'Select...', 'Landlord' => 'Landlord', 'Tenant' => 'Tenant'), $rentReview->notice_by,['class'=> 'form-control']) }}
                    <span id="notice_by" class="help-block"></span>
                </div>
            </div>
            <div id="notice_period" class="control-group">
                {{ Form::label('notice_period', 'Notice Period', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    {{ Form::text('notice_period',$rentReview->notice_period, array('class' => 'form-control')) }}
                    <span id="notice_period" class="help-block"></span>
                </div>
            </div>
            <div id="notice_dates" class="control-group">
                {{ Form::label('notice_dates', 'Notice Dates', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-append">
                        {{ Form::text('notice_dates',$notice_dates, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                        <span class="add-on"><i class="awe-calendar"></i></span>
                    </div>
                    <span id="notice_dates" class="help-block"></span>
                </div>
            </div>
            <div id="notice_amount" class="control-group">
                {{ Form::label('notice_amount', 'Notice Amount', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-prepend">
                        <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                        {{ Form::text('notice_amount',$rentReview->notice_amount, array('class' => 'form-control')) }}
                    </div>
                    <span id="notice_amount" class="help-block"></span>
                </div>
            </div>
            <div id="turnover_rent" class="control-group">
                {{ Form::label('turnover_rent', 'Turnover Rent', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::checkbox('turnover_rent', '1', $rentReview->turnover_rent)}}
                    <span id="turnover_rent" class="help-block"></span>
                </div>
            </div>
            <div id="turnover_above" class="control-group">
                {{ Form::label('turnover_above', 'Turnover Above', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-prepend">
                        <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                        {{ Form::text('turnover_above',$rentReview->turnover_above, array('class' => 'form-control')) }}
                    </div>
                    <span id="turnover_above" class="help-block"></span>
                </div>
            </div>
            <div id="base_rent" class="control-group">
                {{ Form::label('base_rent', 'Base Rent', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-prepend">
                        <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                        {{ Form::text('base_rent',$rentReview->base_rent, array('class' => 'form-control')) }}
                    </div>
                    <span id="base_rent" class="help-block"></span>
                </div>
            </div>
            <div id="turnover" class="control-group">
                {{ Form::label('turnover', 'Turnover Above', array('class' => 'control-label')) }}
                <div class="form-group controls">
                    <div class="input-prepend">
                        <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                        {{ Form::text('turnover',$rentReview->turnover, array('class' => 'form-control')) }}
                    </div>
                    <span id="turnover" class="help-block"></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="notes">Analysis of Revised Rent</label>
                <div class="form-group controls">
                            <textarea id="notes" class="form-control" name="notes"
                                      rows="3">{{$rentReview->notes ? $rentReview->notes : '' }}</textarea>
                    <span id="notes" class="help-block"></span>
                </div>
            </div>
            <!-- submit button -->
            <div class="form-actions">
                <button id="save-rent-review" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
                    Save Changes
                </button>
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
