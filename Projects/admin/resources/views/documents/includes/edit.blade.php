<form method="POST"
      action="{{ route($entity->documentStoreRoute, $entity->id) }}"
      class="form-horizontal add-document-form"
      enctype="multipart/form-data"
      accept-charset="UTF-8">
    @csrf

    <div id='error-message'></div>
    <div id="document_file" class="control-group">
        <label for="document_file" class="control-label">File</label>
        <input id="document_file" name="document_file" type="file" style="display:none">
        <div class="controls">
            <input id='input' name="document_file" class="input-large" type="text">
            <a class="btn browse-btn">Browse</a>
            <span id="document_file" class="help-block">Select a file to upload</span>
        </div>
    </div>
    <div id="document_type_id" class="control-group">
        <label class="control-label" for="document_type_id">Select Category</label>
        <div class="controls">
            <select name="document_type_id" id="document_type_id" class="add-new-option">
                <option>Select...</option>
                @foreach($documentTypes as $documentType)
                    <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                @endforeach
            </select>
            <span id="document_type_id" class="help-block"></span>
        </div>
    </div>
    <div id="comments" class="control-group">
        <label class="control-label" for="comments">Comments</label>
        <div class="controls">
            <textarea name="comments" id="comments" class="input-xlarge" rows="3"></textarea>
            <span id="comments" class="help-block"></span>
        </div>
    </div>
</form>