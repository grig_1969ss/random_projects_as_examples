<div id="note_type_id" class="control-group note_type_replace">
    <label for="note_type_dropdown">Attach to</label>
    <select @if(isset($arrayNames) || isset($addNew)) name="note_category[]" @else name="note_category"
            @endif  class="form-control note_type_dropdown">
        <option disabled selected value="">Select...</option>
        @foreach($documentDropdown as $type => $itemData)
            @if($type == 'properties')
                <option data-value="{{$itemData['value']}}" data-url="{{$itemData['url']}}" data-item-type=""
                        value="{{$itemData['items']['id']}}" data-type="Property">
                    {{$itemData['items']['value']}}
                </option>
            @else
                <optgroup
                    data-url="{{$itemData['url']}}"
                    label="{{$itemData['label']}}"
                    data-value="{{$itemData['value']}}"
                >
                    @foreach($itemData['items'] as $option)
                        <option data-item-type="{{$option['id']}}" value="{{$option['id']}}">{{$option['value']}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endforeach
    </select>
    <input type="hidden" name="attach_to_type[]" class="attach_to_type" value="">
    <span id="note_type_id" class="help-block"></span>
</div>
