<div class="control-group mt-2 document_type_select">
    <label class="control-label" for="document_type_select">Select Category</label>
    <div class="form-group controls">
        {{ Form::select( request()->segment(1) !== 'inbox'? 'document_type_id' : 'document_type_id[]', $documentTypes, null, array('class' => 'form-control add-new-option')) }}
        <span id="document_type_id" class="help-block"></span>
    </div>
</div>
