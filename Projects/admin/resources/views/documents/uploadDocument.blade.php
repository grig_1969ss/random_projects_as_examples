<div class="modal fade" id="documentModal" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Upload Document</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => ['properties.documents.store', $property->id], 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'add-document', 'files' => true]) }}
                <div id='error-message'></div>

                @include('documents.includes.attachToDropdown')

                @include('documents.includes.documentType')

                <div id="document_file" class="control-group">
                    {{ Form::label('document_file', 'File', array('class' => 'control-label')) }}
                    <input id="document_file" name="document_file" type="file" style="display:none">
                    <div class="form-group controls">
                        <input id='input' name="document_file" class="form-control" type="text"
                               onclick="$(this).closest('.modal-body').find('input[id=document_file]').click();"/>
                        <a class="btn"
                           onclick="$(this).closest('.modal-body').find('input[id=document_file]').click();">Browse</a>
                        <span id="document_file" class="help-block">Select a file to upload</span>
                    </div>
                </div>
                <div id="comments" class="control-group">
                    <label class="control-label" for="comments">Comments</label>
                    <div class="form-group controls">
                        <textarea id="comments" class="form-control" rows="3"></textarea>
                        <span id="comments" class="help-block"></span>
                    </div>
                </div>
                @include('partials.modal_footer', array('buttonName' => 'Save Document'))
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
