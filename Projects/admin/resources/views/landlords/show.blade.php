@extends('layouts_new.app', ['pageTitle' => 'Landlord', 'pageNav' => 'landlord', 'page' => 'LandlordsDetail'])

@section('container')


    <div class="section section-sm">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">{{ $landlord->landlord_name }}</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a href="/landlords/{{ $landlord->id }}/edit" class="btn add-property btn-outline-primary">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-3 align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Managing Agent/Landlord</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">{{ $landlord->landlord_name }}</p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Address</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ $landlord->address->address_1 }}<br>
                                {{ $landlord->address->address_2 }}<br>
                                {{ $landlord->address->town }}<br>
                                {{ $landlord->address->county }}<br>
                                {{ $landlord->address->post_code }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Telephone</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($landlord->telephone) }}<br>
                                {{ Util::showValue($landlord->mobile) }}
                            </p>
                        </div>
                    </div>

                    @if($landlord->fax)
                        <div class="row align-items-center">
                            <div class="col-5">
                                <small class="text-uppercase text-muted">Fax</small>
                            </div>
                            <div class="col-7 pl-0">
                                <p class="mb-0">
                                    {{ $landlord->fax }}
                                </p>
                            </div>
                        </div>
                    @endif

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Email</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($landlord->email) }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Added</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Date::dmYHi($landlord->created_at) }}
                                by {{ $landlord->user->name }}
                            </p>
                        </div>
                    </div>

                </div>
                <div class="col-6">
                    <!-- Title  -->
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">Contacts</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a  href="{{ route('landlords.contacts.create', $landlord) }}" class="btn add-property btn-outline-primary">Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-5">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Contact Name</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($landlord->contact as $contact)
                                        <?php $contactUrl = '/contacts/'.$contact->id; ?>
                                        <tr>
                                            <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('partials.show_properties', [
                            'id' => $landlord->id,
                            'type'=> 'landlord',
                            'showClients' => true,
                            'header' => 'Landlord'
            ])


        </div>
    </div>








    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $landlord->landlord_name }}</h2>
                        <ul class="data-header-actions">
                            <li><a href="/landlords/{{ $landlord->id }}/edit" class="btn btn-alt">Edit</a></li>
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Managing Agent/Landlord</dt>
                            <dd>{{ $landlord->landlord_name }}</dd>
                            <dt>Address</dt>
                            <dd>{{ $landlord->address->address_1 }}</dd>
                            <dd>{{ $landlord->address->address_2 }}</dd>
                            <dd>{{ $landlord->address->town }}</dd>
                            <dd>{{ $landlord->address->county }}</dd>
                            <dd>{{ $landlord->address->post_code }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ Util::showValue($landlord->telephone) }}</dd>
                            <dd>{{ Util::showValue($landlord->mobile) }}</dd>
                            @if($landlord->fax)
                                <dt>Fax</dt>
                                <dd>{{ $landlord->fax }}</dd>
                            @endif
                            <dt>Email</dt>
                            <dd>{{ Util::showValue($landlord->email) }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($landlord->created_at) }}
                                by {{ $landlord->user->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
            <!-- /Data block -->

            <!-- Data block -->
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Contacts</h2>
                        <ul class="data-header-actions">
                            <li>
                                <a href="{{ route('landlords.contacts.create', $landlord) }}" class="btn btn-alt">
                                    Add
                                </a>
                            </li>
                        </ul>
                    </header>
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Telephone</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($landlord->contact as $contact)
                            <?php $contactUrl = '/contacts/'.$contact->id; ?>
                            <tr class="odd gradeX">
                                <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

        <!-- Grid row -->
        <div class="row">
            <!-- Data block -->
            <article class="span12 data-block">
                <!-- Render Show Properties Partial -->
                @include('partials.show_properties', [
                    'id' => $landlord->id,
                    'type'=> 'landlord',
                    'showClients' => true,
                    'header' => 'Landlord'
                ])
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

        <!-- Grid row -->
        <div class="row">

            <!-- Render Show Notes Partial -->
            <article class="span12 data-block">
                @include('partials.show_notes', ['id' => $landlord->id])
            </article>

            <!-- Render Hidden Add Notes Partial -->
            @include('partials.add_comment', [
                'id' => $landlord->id,
                'type' => 'landlords',
                'route' => ['landlords.notes.store', $landlord]
            ])
        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
