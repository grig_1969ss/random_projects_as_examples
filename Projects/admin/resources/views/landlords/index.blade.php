@extends('layouts_new.app', ['pageTitle' => 'Landlords', 'pageNav' => 'landlord', 'page' => 'LandlordsIndex'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Landlords</h5>
                    </div>
                    <div class="dropdown">
                        @if (!$read_only)
                        <div class="btn-group mr-2 mb-2">
                            <a href="{{ route('landlords.create') }}" class="btn add-property btn-outline-primary">Add Landlord</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Main Contact</th>
                                <th>Property</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($landlords as $landlord)
                                <tr>
                                    @if ($read_only == 0)
                                        <td>{{ Html::link('/landlords/'.$landlord->id, $landlord->landlord_name ? $landlord->landlord_name :'No Name Given'	) }}</td>
                                        <td>{{ Html::link('/landlords/'.$landlord->id, $landlord->telephone ? $landlord->telephone :'No Telephone Given') }}</td>
                                        <td>{{ Html::link('/landlords/'.$landlord->id, $landlord->email ? $landlord->email :'No Email Given') }}</td>
                                        <td>{{ Html::link('/contacts/'.$landlord->contact_id, Util::joinName(array( $landlord->first_name, $landlord->surname ))) }}</td>
                                        <td>{{ Html::link('/properties/'.$landlord->property_id, $landlord->property ? $landlord->property :'No Properties Exist') }}</td>
                                    @else
                                        <td>{{$landlord->landlord_name ? $landlord->landlord_name :'No Name Given'}}</td>
                                        <td>{{$landlord->telephone ? $landlord->telephone :'No Telephone Given'}}</td>
                                        <td>{{$landlord->email ? $landlord->email :'No Email Given'}}</td>
                                        <td>{{Util::joinName(array( $landlord->first_name, $landlord->surname )) }}</td>
                                        <td>{{$landlord->property ? $landlord->property :'No Properties Exist' }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <nav class="property-list-pagination default-pagination" aria-label="Page navigation example">
                            <span>Showing {{$landlords->firstItem()}} to {{$landlords->lastItem()}} of {{$landlords->total()}} entries</span>
                            {{$landlords->links()}}
                        </nav>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
