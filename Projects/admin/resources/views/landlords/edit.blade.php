@extends('layouts_new.app', [
    'pageTitle' => ($landlord->id ? 'Edit' : 'Add').' Landlord',
    'pageNav'   => 'landlord',
    'page'      => 'LandlordNew'
])

@section('container')
    @include('landlords.includes.form', [
                               'title'      => ($landlord->id ? 'Edit' : 'New').' Landlord',
                               'restMethod' => $landlord->id ? 'put' : 'post',
                               'isModal'    => false,
                               'route'      => $landlord->id ? ['landlords.update', $landlord] : 'landlords.store',
    ])
@endsection
