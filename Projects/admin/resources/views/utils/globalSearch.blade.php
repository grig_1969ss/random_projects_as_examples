<div id="search-results">
    <ul class="list-group list-group-flush bg-white shadow-sm border border-soft">
        @foreach($properties as $property)
            <a href="{{route( $property->route.'.show', $property->id)}}">
                <li class="list-group-item border-0 clearfix">
                    {{$property->name}} <span class="float-right">{{$property->result_type}}</span>
                </li>
            </a>
        @endforeach
    </ul>

    <div class="timeline-item top-item shadow-sm border border-light mt-2">
        <div class="container-fluid position-relative mail-top-header mb-1 mt-1">
            <div class="ml-auto top-header_pagination">
                <div class="pr-4 pl-4 flex-fill">
                    <div class="d-flex justify-content-between pagination align-items-center">
                        <div>
                            @php
                                $pagination = json_decode($properties->toJson());
                            @endphp

                            <a href="{{route('search',['searchBy' => $search])}}" class="search-all">See all results
                                >></a>
                            {{--                        {{ 1 }} - {{ 10 }} of {{ 785 }}--}}
                        </div>
                        {{--                    <div>--}}
                        {{--                        <i class="fas fa-arrow-left pl-3 pr-1 icon prev-page-search-"--}}
                        {{--                           data-url="{{ $pagination->prev_page_url }}"--}}
                        {{--                        ></i>--}}
                        {{--                        <i class="fas fa-arrow-right pr-3 icon next-page-search-"--}}
                        {{--                           data-url="{{ $pagination->next_page_url }}"--}}
                        {{--                        ></i>--}}
                        {{--                    </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
