@extends('layouts_new.app', ['pageTitle' => 'Claims', 'pageNav' => 'claim', 'page' => 'ClaimsIndex'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Claims</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li class="demoTabs">
                                    <a href="{{ route('claims.create') }}" class="btn btn-alt">
                                        Add Claim
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </header>
                    <table id="example-2" class="datatable table table-striped table-bordered table-hover dataTable"
                           aria-describedby="example-2_info" style="width: 880px;">
                        <thead>
                        <tr>
                            <th>Client</th>
                            <th>Audit Name</th>
                            <th>Claim Number</th>
                            <th>Auditor</th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach ($claims as $claim)
                            <tr class="odd gradeX">
                                @if (!$read_only)
                                    <td>
                                        <a href="{{ route('claims.show', $claim) }}">
                                            {{ $claim->present()->client }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('claims.show', $claim) }}">
                                            {{ $claim->present()->auditRegister }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('claims.show', $claim) }}">{{ $claim->claim_number }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('claims.show', $claim) }}">
                                            {{ $claim->present()->auditor }}
                                        </a>
                                    </td>
                                @else
                                    <td>{{ $claim->present()->client }}</td>
                                    <td>{{ $claim->present()->auditRegister }}</td>
                                    <td>{{ $claim->claim_number }}</td>
                                    <td>{{ $claim->present()->auditor }}</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->
        </div>
        <!-- /Grid row -->
    </div>
    <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
