@extends('layouts_new.app', [
    'pageTitle' => ($claim->id ? 'Edit' : 'Add').' Claim',
    'pageNav' => 'claim',
    'page' => 'ClaimNew'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $claim->id ? 'Edit' : 'New' }} Claim</h2>
                    </header>
                    <div class="span8">
                        @include('claims.includes.form')
                    </div>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
