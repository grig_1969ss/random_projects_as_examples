@extends('layouts_new.app',
    ['pageTitle' => 'Claim', 'pageNav' => 'claim', 'page' => 'ClaimsDetail'])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Claim #{{ $claim->claim_number }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="{{ route('claims.edit', $claim) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Client</dt>
                            <dd>
                                @if($claim->client)
                                    <a href="{{ route('clients.show', $claim->client) }}">
                                        {{ $claim->present()->client }}
                                    </a>
                                @else
                                    -
                                @endif
                            </dd>
                            <dt>Audit Name</dt>
                            <dd>
                                @if($claim->auditRegister)
                                    <a href="{{ route('audit-registers.show', $claim->auditRegister) }}">
                                        {{ $claim->present()->auditRegister }}
                                    </a>
                                @else
                                    -
                                @endif
                            </dd>
                            <dt>Claim Number</dt>
                            <dd>{{ $claim->claim_number }}</dd>
                            <dt>Auditor</dt>
                            <dd>{{ $claim->present()->auditor }}</dd>
                            <dt>Supplier Number</dt>
                            <dd>{{ $claim->supplier_number }}</dd>
                            <dt>Supplier Name</dt>
                            <dd>{{ $claim->supplier_name }}</dd>
                            <dt>Claim Type</dt>
                            <dd>{{ $claim->present()->claimType }}</dd>
                            <dt>Claim Subtype</dt>
                            <dd>{{ $claim->present()->claimSubtype }}</dd>
                            <dt>Invoice Number</dt>
                            <dd>{{ $claim->invoice_number }}</dd>
                            <dt>Invoice Date</dt>
                            <dd>{{ $claim->present()->invoiceDate }}</dd>
                            <dt>Currency</dt>
                            <dd>{{ $claim->present()->currency }}</dd>
                            <dt>Original Net Value</dt>
                            <dd>{!! $claim->present()->originalNetValue !!}</dd>
                            <dt>VAT</dt>
                            <dd>{!! $claim->present()->vat !!}</dd>
                            <dt>Gross Value</dt>
                            <dd>{!! $claim->present()->grossValue !!}</dd>
                            <dt>Claim Adjustment</dt>
                            <dd>{!! $claim->present()->claimAdjustment !!}</dd>
                            <dt>Cancelled Claim</dt>
                            <dd>{!! $claim->present()->cancelledClaim !!}</dd>
                            <dt>Net Recovery</dt>
                            <dd>{!! $claim->present()->netRecovery !!}</dd>
                            <dt>Claim Status</dt>
                            <dd>{{ $claim->present()->claimStatus }}</dd>
                            <dt>Date Passed to Client</dt>
                            <dd>{{ $claim->present()->clientDate }}</dd>
                            <dt>PO Number</dt>
                            <dd>{{ $claim->po_number }}</dd>
                            <dt>Property Number</dt>
                            <dd>{{ $claim->property_number }}</dd>
                            <dt>Property Address</dt>
                            <dd>{{ $claim->property_address }}</dd>
                            <dt>Supplier Contact</dt>
                            <dd>{{ $claim->supplier_contact }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($claim->created_at) }}
                                by {{ optional($claim->createdBy)->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
        </div>

        <div class="row">
            <article class="span12 data-block">
                @include('partials.show_notes', ['id' => $claim->id])
            </article>

            @include('partials.add_comment', [
                'id'    => $claim->id,
                'type'  => 'claims',
                'route' => ['claims.notes.store', $claim]
            ])
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
