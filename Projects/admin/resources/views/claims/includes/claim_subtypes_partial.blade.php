<select name="claim_subtype_id"
        class="input-large"
        id="claim_subtype_id"
        {{ $claimSubtypes->count() == 0 ? 'disabled' : '' }}>
    <option value="">Select...</option>
    @foreach($claimSubtypes as $claimSubtype)
        <option value="{{ $claimSubtype->id }}"
                {{ $claimSubtype->id == $claim->claim_subtype_id ? 'selected' : '' }}>
            {{ $claimSubtype->name }}
        </option>
    @endforeach
</select>