<fieldset>
    <div id='error-message'></div>

    <form method="POST"
          action="{{ $claim->id ? route('claims.update', $claim) : route('claims.store') }}"
          class="form-horizontal"
          id="create-claim">
        {{ csrf_field() }}

        @if($claim->id)
            {{ method_field('PUT') }}
        @endif

        <div id="client_id" class="control-group">
            <label for="client_id" class="control-label">Client</label>
            <div class="controls">
                <select name="client_id" class="input-large" id="client_id">
                    <option value="">Select...</option>
                    @foreach($clients as $client)
                        <option value="{{ $client->id }}"
                                {{ $client->id == $claim->client_id ? 'selected' : '' }}>
                            {{ $client->client_name }}
                        </option>
                    @endforeach
                </select>
                <span id="client_id" class="help-block"></span>
            </div>
        </div>

        <div id="audit_register_id" class="control-group">
            <label for="audit_register_id" class="control-label">Audit Name</label>
            <div class="controls">
                @include('claims.includes.audit_registers_partial')
                <span id="audit_register_id" class="help-block"></span>
            </div>
        </div>

        <div id="claim_number" class="control-group">
            <label for="claim_number" class="control-label">Claim Number</label>
            <div class="controls">
                <input type="text"
                       name="claim_number"
                       id="claim_number"
                       class="input-medium"
                       value="{{ $claim->id ? $claim->claim_number : $claimNumber }}"
                       readonly>
                <span id="claim_number" class="help-block"></span>
            </div>
        </div>

        <div id="auditor_id" class="control-group">
            <label for="auditor_id" class="control-label">Auditor</label>
            <div class="controls">
                <select name="auditor_id" class="input-large" id="auditor_id">
                    <option value="">Select...</option>
                    @foreach($auditors as $auditor)
                        <option value="{{ $auditor->id }}"
                                {{ $auditor->id == $claim->audit_register_id ? 'selected' : '' }}>
                            {{ $auditor->name }}
                        </option>
                    @endforeach
                </select>
                <span id="auditor_id" class="help-block"></span>
            </div>
        </div>

        <div id="supplier_number" class="control-group">
            <label for="supplier_number" class="control-label">Supplier Number</label>
            <div class="controls">
                <input type="text"
                       name="supplier_number"
                       id="supplier_number"
                       class="input-medium"
                       value="{{ $claim->supplier_number }}">
                <span id="supplier_number" class="help-block"></span>
            </div>
        </div>

        <div id="supplier_name" class="control-group">
            <label for="supplier_name" class="control-label">Supplier Name</label>
            <div class="controls">
                <input type="text"
                       name="supplier_name"
                       id="supplier_name"
                       class="input-medium"
                       value="{{ $claim->supplier_name }}">
                <span id="supplier_name" class="help-block"></span>
            </div>
        </div>

        <div id="claim_type_id" class="control-group">
            <label for="claim_type_id" class="control-label">Claim Type</label>
            <div class="controls">
                <select name="claim_type_id" class="input-large" id="claim_type_id">
                    <option value="">Select...</option>
                    @foreach($claimTypes as $claimType)
                        <option value="{{ $claimType->id }}"
                                {{ $claimType->id == $claim->claim_type_id ? 'selected' : '' }}>
                            {{ $claimType->name }}
                        </option>
                    @endforeach
                </select>
                <span id="claim_type_id" class="help-block"></span>
            </div>
        </div>

        <div id="claim_subtype_id" class="control-group">
            <label for="claim_subtype_id" class="control-label">Claim Subtype</label>
            <div class="controls">
                @include('claims.includes.claim_subtypes_partial')
                <span id="claim_subtype_id" class="help-block"></span>
            </div>
        </div>

        <div id="invoice_number" class="control-group">
            <label for="invoice_number" class="control-label">Invoice Number</label>
            <div class="controls">
                <input type="text"
                       name="invoice_number"
                       id="invoice_number"
                       class="input-medium"
                       value="{{ $claim->invoice_number }}">
                <span id="invoice_number" class="help-block"></span>
            </div>
        </div>

        <div class="control-group">
            <label for="invoice_date" class="control-label">Invoice Date</label>
            <div class="controls">
                <div class="input-append" id="invoice_date">
                    <input type="text"
                           name="invoice_date"
                           id="invoice_date"
                           class="datepicker input-small"
                           data-date-format="dd/mm/yyyy"
                           value="{{ $claim->present()->invoiceDate('') }}">
                    <span class="add-on"><i class="awe-calendar"></i></span>
                </div>
                <span id="invoice_date" class="help-block"></span>
            </div>
        </div>

        <div id="currency_id" class="control-group">
            <label for="currency_id" class="control-label">Currency</label>
            <div class="controls">
                <select name="currency_id" class="input-large" id="currency_id">
                    <option value="">Select...</option>
                    @foreach($currencies as $currency)
                        <option value="{{ $currency->id }}"
                                {{ $currency->id == $claim->currency_id ? 'selected' : '' }}>
                            {{ $currency->name }}
                        </option>
                    @endforeach
                </select>
                <span id="currency_id" class="help-block"></span>
            </div>
        </div>

        <div id="original_net_value" class="control-group">
            <label for="original_net_value" class="control-label">Original Net Value</label>
            <div class="controls">
                <input type="text"
                       name="original_net_value"
                       id="original_net_value"
                       class="input-medium"
                       value="{{ $claim->original_net_value }}">
                <span id="original_net_value" class="help-block"></span>
            </div>
        </div>

        <div id="vat" class="control-group">
            <label for="vat" class="control-label">VAT</label>
            <div class="controls">
                <input type="text" name="vat" id="vat" class="input-medium" value="{{ $claim->vat }}">
                <span id="vat" class="help-block"></span>
            </div>
        </div>

        <div id="gross_value" class="control-group">
            <label for="gross_value" class="control-label">Gross Value</label>
            <div class="controls">
                <input type="text"
                       name="gross_value"
                       id="gross_value"
                       class="input-medium"
                       value="{{ $claim->gross_value == 0 ? null : $claim->gross_value }}"
                       disabled>
                <span id="gross_value" class="help-block"></span>
            </div>
        </div>

        <div id="claim_adjustment" class="control-group">
            <label for="claim_adjustment" class="control-label">Claim Adjustment</label>
            <div class="controls">
                <input type="text"
                       name="claim_adjustment"
                       id="claim_adjustment"
                       class="input-medium"
                       value="{{ $claim->claim_adjustment }}">
                <span id="claim_adjustment" class="help-block"></span>
            </div>
        </div>

        <div id="cancelled_claim" class="control-group">
            <label for="cancelled_claim" class="control-label">Cancelled Claim</label>
            <div class="controls">
                <input type="text"
                       name="cancelled_claim"
                       id="cancelled_claim"
                       class="input-medium"
                       value="{{ $claim->cancelled_claim }}">
                <span id="cancelled_claim" class="help-block"></span>
            </div>
        </div>

        <div id="net_recovery" class="control-group">
            <label for="net_recovery" class="control-label">Net Recovery</label>
            <div class="controls">
                <input type="text"
                       name="net_recovery"
                       id="net_recovery"
                       class="input-medium"
                       value="{{ $claim->net_recovery == 0 ? null : $claim->net_recovery }}"
                       disabled>
                <span id="net_recovery" class="help-block"></span>
            </div>
        </div>

        <div id="claim_status_id" class="control-group">
            <label for="claim_status_id" class="control-label">Claim Status</label>
            <div class="controls">
                <select name="claim_status_id" class="input-large" id="claim_status_id">
                    <option value="">Select...</option>
                    @foreach($claimStatuses as $claimStatus)
                        <option value="{{ $claimStatus->id }}"
                                {{ $claimStatus->id == $claim->claim_status_id ? 'selected' : '' }}>
                            {{ $claimStatus->name }}
                        </option>
                    @endforeach
                </select>
                <span id="claim_status_id" class="help-block"></span>
            </div>
        </div>

        <div class="control-group">
            <label for="client_date" class="control-label">Client Date</label>
            <div class="controls">
                <div class="input-append" id="client_date">
                    <input type="text"
                           name="client_date"
                           id="client_date"
                           class="datepicker input-small"
                           data-date-format="dd/mm/yyyy"
                           value="{{ $claim->present()->clientDate('') }}">
                    <span class="add-on"><i class="awe-calendar"></i></span>
                </div>
                <span id="client_date" class="help-block"></span>
            </div>
        </div>

        <div id="po_number" class="control-group">
            <label for="po_number" class="control-label">PO Number</label>
            <div class="controls">
                <input type="text" name="po_number" id="po_number" class="input-medium" value="{{ $claim->po_number }}">
                <span id="po_number" class="help-block"></span>
            </div>
        </div>

        <div id="property_number" class="control-group">
            <label for="property_number" class="control-label">Property Number</label>
            <div class="controls">
                <input type="text"
                       name="property_number"
                       id="property_number"
                       class="input-medium"
                       value="{{ $claim->property_number }}">
                <span id="property_number" class="help-block"></span>
            </div>
        </div>

        <div id="property_address" class="control-group">
            <label for="property_address" class="control-label">Property Address</label>
            <div class="controls">
                <input type="text"
                       name="property_address"
                       id="property_address"
                       class="input-xlarge"
                       value="{{ $claim->property_address }}">
                <span id="property_address" class="help-block"></span>
            </div>
        </div>

        <div id="supplier_contact" class="control-group">
            <label for="supplier_contact" class="control-label">Supplier Contact</label>
            <div class="controls">
                <input type="text"
                       name="supplier_contact"
                       id="supplier_contact"
                       class="input-medium"
                       value="{{ $claim->supplier_contact }}">
                <span id="supplier_contact" class="help-block"></span>
            </div>
        </div>

        <div class="form-actions">
            <button id="save-claim" class="btn btn-large btn-primary" type="submit">
                Save Changes
            </button>
        </div>
    </form>
</fieldset>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
