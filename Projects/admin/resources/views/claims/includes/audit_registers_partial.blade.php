<select name="audit_register_id"
        class="input-large"
        id="audit_register_id"
        {{ $auditRegisters->count() == 0 ? 'disabled' : '' }}>
    <option value="">Select...</option>
    @foreach($auditRegisters as $auditRegister)
        <option value="{{ $auditRegister->id }}"
                {{ $auditRegister->id == $claim->audit_register_id ? 'selected' : '' }}>
            {{ $auditRegister->audit_name }}
        </option>
    @endforeach
</select>