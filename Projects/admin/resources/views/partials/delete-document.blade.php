<fieldset>
    <div id='error-message'></div>
    <form method="POST" class="form-horizontal" id="delete-document">
        {{ method_field('DELETE') }}
        @csrf
        <div class="form-actions">
            <a class="btn btn-alt btn-default" data-dismiss="modal" href="#">Cancel</a>
            <button class="btn btn-alt btn-primary" type="submit">Delete</button>
        </div>
    </form>
</fieldset>