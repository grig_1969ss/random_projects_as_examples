<fieldset>
	{{ Form::open(['url' => "properties/save-client-query/", 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => 'save-client-query']) }}
	<div id='error-message'></div>
	<div id="due_date" class="control-group">
		{{ Form::label('due_date', 'Due Date', array('class' => 'control-label')) }}
		<div class="controls">
			<div class="input-append">
				{{ $due_date = ($clientQuery->due_date) ? Date::dmY($clientQuery->due_date) : null }}
				{{ Form::text('due_date',$due_date, array('class' => 'datepicker input-small', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
				<span class="add-on"><i class="awe-calendar"></i></span>
			</div>
			<span id="due_date" class="help-block"></span>
		</div>
	</div>
	<?php
        $statusDropdown = [
            null => 'Select...',
            1    => 'In Progress',
            2    => 'Closed',
        ];
    ?>
	<div id="client_query_status_id" class="control-group">
		{{ Form::label('client_query_status_id', 'Client Query Status', array('class' => 'control-label')) }}
		<div class="controls">
			{{ Form::select('client_query_status_id', $statusDropdown, $clientQuery->client_query_status_id, array('id'=>'client_query_status_id', 'class' => 'add-new-option')) }}
			<span id="client_query_status_id" class="help-block"></span>
		</div>
	</div>
	<div id="details" class="control-group">
		{{ Form::label('details', 'Details', array('class' => 'control-label')) }}
		<div class="controls">
			{{ Form::textarea('details',$clientQuery->details, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput')) }}
			<span id="details" class="help-block"></span>
		</div>
	</div>
	@if($restMethod == 'POST')
		{{ Form::hidden('property_id',$id) }}
		{{ Form::hidden('id', null) }}
	@else
		{{ Form::hidden('id',$clientQuery->id) }}
		{{ Form::hidden('property_id',$clientQuery->property_id) }}
	@endif
	@if(isset($clientQuery->diary->id))
		<div id="diary_date" class="control-group">
			{{ Form::label('diary_date', 'Diary Date', array('class' => 'control-label')) }}
			<div class="controls">
				<div class="input-append">
					{{ $diary_date = ($clientQuery->diary->date) ? Date::dmY($clientQuery->diary->date) : null }}
					{{ Form::text('diary_date',$diary_date, array('class' => 'datepicker input-small', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
					<span class="add-on"><i class="awe-calendar"></i></span>
					<span id="diary_date" class="help-block"></span>
				</div>
			</div>
		</div>
		{{ Form::hidden('diary_id', $clientQuery->diary->id) }}
		@else
		{{ Form::hidden('diary_date', Date::dmY(date('Y-m-d H:i:s'))) }}
		@endif
		<div class="form-actions">
		    <button id="save-client" class="btn btn-large btn-primary" type="submit">
		    	Save Changes
		    </button>
		</div>
{{ Form::close() }}
</fieldset>

@push('js')
	<script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
