<fieldset>
<div id='error-message'></div>
{{ Form::open(['url' => "settings/type/", 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => $formId]) }}
{{ Form::hidden('id', null, array('id' => 'type-id')) }}
{{ Form::hidden('model_name', null, array('id' => 'model-name')) }}
@if ($isDelete)
<!-- submit button -->
<div class="form-actions">
	<a class="btn btn-alt btn-default" data-dismiss="modal" href="#">Cancel</a>
    <button class="btn btn-alt btn-primary" type="submit">Delete</button>
</div>
@else
<div id="ctrl-grp-name" class="control-group">
	<label id='label-name' class="control-label" for="name">Name</label>
	<div class="controls">
		{{ Form::text('name', null, array('id' => 'type-name', 'class' => 'input-medium')) }}
		<span id="help-name" class="help-block"></span>
	</div>
</div>
<!-- submit button -->
<div class="form-actions">
    <button id="{{$formId}}" class="btn btn-large btn-primary" type="submit">
    	Save Changes
    </button>
</div>
@endif
{{ Form::close()}}
</fieldset>