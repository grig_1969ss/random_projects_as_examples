<div class="modal fade" id="noteModal" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Create New Note</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => $route, 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'add-note']) }}
                <div id='error-message'></div>

                <input type="hidden" name="object_type_id" value="">
                <input type="hidden" name="object_id" value="">

                <div id="note_type_id" class="control-group note-category">
                    {{ Form::label('note_type_id', 'Note Category', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        {{ Form::select('note_type_id', $noteTypes, null, array('class' => 'form-control add-new-option note_type_id')) }}
                        <span id="note_type_id" class="help-block"></span>
                    </div>
                </div>
                {{ Form::hidden('object_type', $type)}}
                <div id="note_text" class="control-group">
                    <label class="control-label" for="note_text">Note</label>
                    <div class="form-group controls">
                        <textarea id="note_text" class="form-control" name="note_text" rows="3"></textarea>
                        <span id="note_text" class="help-block"></span>
                    </div>
                </div>
                @include('partials.modal_footer', ['buttonName' => 'Save Note'])
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
