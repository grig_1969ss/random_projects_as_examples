<!-- Add Payment -->
<div class="modal fade hide" id="paymentModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Add Payment</h3>
	</div>
	<div class="modal-body">
		{{ Form::open(['route' => ['properties.payments.store', $property], 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'add-payment']) }}
		<div id='error-message'></div>
		<div id="payment_date" class="control-group">
			{{ Form::label('payment_date', 'Payement Date', array('class' => 'control-label')) }}
			<div class="controls">
				<div class="input-append">
					{{ $payment_date = null }}
					{{ Form::text('payment_date',$payment_date, array('class' => 'datepicker input-small', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
					<span class="add-on"><i class="awe-calendar"></i></span>
				</div>
				<span id="payment_date" class="help-block"></span>
			</div>
		</div>
		<div id="payment_amount" class="control-group">
			{{ Form::label('payment_amount', 'Payment Amount', array('class' => 'control-label')) }}
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
					{{ Form::text('payment_amount',null, array('class' => 'input-small')) }}
				</div>
				<span id="payment_amount" class="help-block"></span>
			</div>
		</div>
		<div id="payment_ref" class="control-group">
			{{ Form::label('payment_ref', 'Payment Reference', array('class' => 'control-label')) }}
			<div class="controls">
				{{ Form::text('payment_ref',null, array('class' => 'input-medium')) }}
				<span id="payment_ref" class="help-block"></span>
			</div>
		</div>
	</div>
	@include('partials.modal_footer', ['buttonName' => 'Save Payment'])
	{{ Form::close() }}
</div>
<!-- End Note -->