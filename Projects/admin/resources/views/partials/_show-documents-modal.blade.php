@if (!$documentTables)
<p>No documents have been added for this property.</p>
@else
@foreach($documentTables as $header => $documents)
<h4>{{$header}}</h4>
<table class='table table-bordered table-condensed'>
    <thead>
        <th>Name</th>
        <th>Uploaded At</th>
        <th>Uploaded By</th>
    </thead>
    <tbody>
        @foreach($documents as $document)
            @php
                $link = $document->getUrl();
            @endphp

            <tr>
                <td><a target="_new" href='{{ $link }}'>{{ $document->document_name }}</a></td>
                <td><a target="_new" href='{{ $link }}'>{{ Date::dmYHi($document->created_at) }}</a></td>
                <td><a target="_new" href='{{ $link }}'>{{ $document->user_name }}</a></td>
            </tr>
        @endforeach
    </tbody>
</table>
<hr>
@endforeach
@endif