<div class="modal fade" id="disposalModal" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Dispose Property</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => ['properties.dispose', $property], 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'dispose-property', 'autocomplete'=>'off']) }}
                <div id='error-message'></div>
                <div id="disposal_type_id" class="control-group">
                    {{ Form::label('disposal_type_id', 'Disposal Type', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        {{ Form::select('disposal_type_id', $disposalTypes, 1, array('class' => 'form-control add-new-option')) }}
                        <span id="disposal_type_id" class="help-block"></span>
                    </div>
                </div>
                <div id="payment_date" class="control-group">
                    {{ Form::label('disposal_date', 'Disposal Date', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        <div class="input-append">
                            {{ $disposal_date = null }}
                            {{ Form::text('disposal_date', $disposal_date, array('class' => 'form-control datepicker')) }}
                            <span class="add-on"><i class="awe-calendar"></i></span>
                        </div>
                    </div>
                    <span id="disposal_date" class="help-block"></span>
                </div>
            </div>
            @include('partials.modal_footer', ['buttonName' => 'Dispose'])
        </div>
    </div>
</div>


<!-- Add Note -->
<!-- <a class="btn btn-alt btn-primary" data-toggle="modal" href="#demoModal2">Add Comment</a> -->
{{--<div class="modal fade hide" id="disposalModal">--}}
{{--	<div class="modal-header">--}}
{{--		<button type="button" class="close" data-dismiss="modal">×</button>--}}
{{--		<h3>Dispose Property</h3>--}}
{{--	</div>--}}
{{--	<div class="modal-body">--}}
{{--		{{ Form::open(['route' => ['properties.dispose', $property], 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'dispose-property']) }}--}}
{{--		<div id='error-message'></div>--}}
{{--		<div id="disposal_type_id" class="control-group">--}}
{{--			{{ Form::label('disposal_type_id', 'Disposal Type', array('class' => 'control-label')) }}--}}
{{--			<div class="controls">--}}
{{--				{{ Form::select('disposal_type_id', $disposalTypes, 1, array('class' => 'add-new-option')) }}--}}
{{--				<span id="disposal_type_id" class="help-block"></span>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div id="payment_date" class="control-group">--}}
{{--			{{ Form::label('disposal_date', 'Disposal Date', array('class' => 'control-label')) }}--}}
{{--			<div class="form-group controls">--}}
{{--				<div class="input-append">--}}
{{--					{{ $disposal_date = null }}--}}
{{--					{{ Form::text('disposal_date', $disposal_date, array('class' => 'datepicker')) }}--}}
{{--					<span class="add-on"><i class="awe-calendar"></i></span>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<span id="disposal_date" class="help-block"></span>--}}
{{--		</div>--}}
{{--	</div>--}}

{{--	@include('partials.modal_footer', ['buttonName' => 'Dispose'])--}}

{{--	{{ Form::close() }}--}}
{{--</div>--}}
<!-- End Note -->
