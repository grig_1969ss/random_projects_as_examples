<div class="modal-footer">
    <span class="loading dark" style='display:none; float:left;' data-original-title="Loading, please wait…">Loading…</span>
    <a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
    <button  class="btn mr-2 mb-2 btn-outline-primary {{ isset($customClass) ? $customClass : '' }}" data-loading-text="Please Wait..." type="submit">
        @if(isset($buttonName))
            {{ $buttonName }}
        @else
            Save Changes
        @endif
    </button>
</div>
