<!-- Title  -->
<div class="row ml-0 mr-0">
    <div class="col-12 mb-4 d-flex justify-content-between">
        <div>
            <h5 class="font-weight-bold">Properties</h5>
        </div>

        <div class="form-group col-4 focused"  >
            <input name="searchBy" type="text" placeholder="Search..." class="form-control" id="find-property">
        </div>

        <div class="dropdown">
            @if(!auth()->user()->isClient())
                <div class="btn-group mr-2 mb-2">
                    <a href="{{route('properties.create')}}" class="btn add-property btn-outline-primary">Add
                        Property</a>
                </div>
            @endif

            <div class="btn-group mr-2 mb-2">
                <form action="{{url(request()->path())}}">
                    <select onchange="this.form.submit()" name="status" class="custom-select mr-sm-2"
                            id="inlineFormCustomSelect">
                        <option value="">All properties</option>
                        @foreach(\App\Models\PropertyStatus::get() as $status)
                            <option
                                value="{{$status->id}}"
                                @if(request()->get('status') == $status->id)
                                selected
                                @endif
                            >
                                {{$status->name}}
                            </option>
                        @endforeach
                    </select>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- End of Title -->
@include('properties.includes.properties_list')


@push('js')

@endpush

