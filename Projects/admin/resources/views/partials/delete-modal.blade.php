<div class="modal fade" id="modal-delete-confirm" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Delete a record</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove this record ?</p>
            </div>

            <form action="" method="post">
                @csrf
                @method('delete')
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-default ml-auto" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
