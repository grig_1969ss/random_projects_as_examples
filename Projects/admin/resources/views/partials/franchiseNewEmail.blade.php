@if(!auth()->user()->hasRole('client'))
    <div class="modal fade" id="modal-new-email" tabindex="-1" role="dialog" aria-labelledby="modal-default"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-default">New email</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('emails.store')}}" method="post" id="add-email-template">
                        @csrf
                        <input type="hidden" id="budget_type" name="budget_type" value="">
                        <input type="hidden" id="service_charge" name="service_charge" value="">
                        <input type="hidden" id="template_property" name="template_property" value="">
                        <input type="hidden" name="modal_model" value="">
                        <div class="form-group control-group" id="to">
                            <label for="to">To</label>
                            <div>
                                <input id="to" name="to" type="email" class="form-control"/>
                            </div>
                            <span id="to" class="help-block"></span>
                        </div>

                        <div class="form-group control-group" id="cc">
                            <label for="cc">Cc</label>
                            <div>
                                <input id="cc" name="cc" type="email" class="form-control"/>
                            </div>
                            <span id="cc" class="help-block"></span>
                        </div>

                        <div class="form-group control-group" id="subject">
                            <label for="subject">Subject</label>
                            <div>
                                <input id="subject" name="subject" type="text" class="form-control"/>
                            </div>
                            <span id="subject" class="help-block"></span>
                        </div>

                        <div class="form-group control-group" id="body">
                            <label for="body">Message</label>
                            <div>
                            <textarea id="autoexpand-body" name="body" class="form-control" cols="10"
                                      rows="3"></textarea>
                            </div>
                            <span id="body" class="help-block"></span>
                        </div>

                        <div class="attachments"></div>

                        @include('partials.modal_footer', array('buttonName' => 'Send'))

                    </form>

                </div>

            </div>
        </div>
    </div>
@endif
