<section>
    <div id='notes-section' class="{{preg_replace('/[^a-z]/', "_", strtolower($type))}}">
        @if($notes->count() == 0)
            <p id='no-notes'>No notes have been added</p>
        @else
            @foreach($notes as $note)
                @if( !isset($type) || ($note->noteType->name === $type))
                    <p>
                        @if($note->noteType)
                            {{ \App\Helpers\Date::dmYHi($note->created_at)}} - <strong>{{ $note->user->name ?? '-' }}</strong>
                            -
                            <code>{{ $note->noteType->name }}</code><br>{{ $note->note_text }}
                        @endif
                    </p>
                @endif
            @endforeach
        @endif
    </div>
</section>
