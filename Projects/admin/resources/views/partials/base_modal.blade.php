{{--<div class="modal fade hide" id="base-modal">--}}
{{--    <div class="modal-header">--}}
{{--        <button type="button" class="close" data-dismiss="modal">×</button>--}}
{{--        <h3></h3>--}}
{{--    </div>--}}
{{--    <div class="modal-body"></div>--}}

{{--    @if(isset($showFooter) && $showFooter)--}}
{{--        <div class="modal-footer">--}}
{{--            <span class="loading dark" style='display:none; float:left;' data-original-title="Loading, please wait…">Loading…</span>--}}
{{--            <a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>--}}
{{--            <button  class="btn btn-alt btn-primary {{ isset($customClass) ? $customClass : '' }}" data-loading-text="Please Wait...">--}}
{{--                @if(isset($buttonName))--}}
{{--                    {{ $buttonName }}--}}
{{--                @else--}}
{{--                    Save Changes--}}
{{--                @endif--}}
{{--            </button>--}}
{{--        </div>--}}
{{--    @endif--}}
{{--</div>--}}

<div class="modal fade" id="base-modal" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header">
                <h3 class="modal-title" id="modal-title-default"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"></div>

            @if(isset($showFooter) && $showFooter)
                <div class="modal-footer">
                    <span class="loading dark" style='display:none; float:left;' data-original-title="Loading, please wait…">Loading…</span>
                    <a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
                    <button  class="btn btn-alt btn-primary {{ isset($customClass) ? $customClass : '' }}" data-loading-text="Please Wait...">
                        @if(isset($buttonName))
                            {{ $buttonName }}
                        @else
                            Save Changes
                        @endif
                    </button>
                </div>
            @endif

        </div>
    </div>
</div>
