<section>
    <div class="col-12 mb-4 d-flex justify-content-between">
        <div>
            <h4 class="font-weight-bold">Diary</h4>
        </div>
        <div class="d-none d-md-block">

            @if(request()->get('type') == 'my')
                <a href="{{route('diary.index')}}" class="btn btn-outline-primary btn-sm" type="button">All Diaries</a>
            @else
                <a href="{{route('diary.index')}}?type=my" class="btn btn-outline-primary btn-sm" type="button">My Diaries</a>
            @endif

            @if( isset($buttons) )
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-angle-down dropdown-arrow"></i>
                        <span>Actions</span>
                    </button>

                    <div class="dropdown-menu" id="action-dropdown" x-placement="bottom-start"
                         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 31px, 0px);">
                        <a href="/diary" class="dropdown-item">All</a>
                        <a href="/diary/typeId/1" class="dropdown-item">Service Charge</a>
                        <a href="/diary/typeId/2" class="dropdown-item">Client Query</a>
                        <a href="/diary/typeId/4" class="dropdown-item">Maintenance</a>
                        <a href="/diary/typeId/5" class="dropdown-item">Break Option</a>
                        <a href="/diary/typeId/6" class="dropdown-item">Approval</a>
                        <a href="/diary/typeId/6" class="dropdown-item">Insurance</a>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <form>
        <table class="table">
            <tbody>
            @foreach($diaries as $title => $diaryList)
                @if( $diaryList )
                    <tr>
                        <td rowspan="{{ count($diaryList) }}"><h3>{{ $title }}</h3></td>
                    <?php $count = 0; ?>
                    @foreach ($diaryList as $diary)
                        @if($count != 0)
                            <tr>
                                @endif
                                <td>
                                    <p>
                                        <a href="/properties/{{ $diary->property_id }}">{{Util::diaryProperty($diary->property_id)}}</a>
                                        @if($diary->diary_type_id == 1)
                                            <span class="label">Service Charge</span>
                                        @elseif($diary->diary_type_id == 2)
                                            <span class="label label-info">Client Query</span>
                                        @elseif($diary->diary_type_id == 3)
                                            <span class="label label-success">Rent Review</span>
                                        @elseif($diary->diary_type_id == 4)
                                            <span class="label label-important">Maintenance</span>
                                        @elseif($diary->diary_type_id == 5)
                                            <span class="label label-success">Break Option</span>
                                        @elseif($diary->diary_type_id == 6)
                                            <span class="label">Approval</span>
                                        @elseif($diary->diary_type_id == 7)
                                            <span class="label label-info">Insurance</span>
                                        @elseif($diary->diary_type_id == 8)
                                            <span class="label label-info">Email</span>
                                        @endif
                                    </p>
                                    <span>
								@if($diary->status == 1)
                                            {{'completed' }}
                                        @else
                                            {{'due' }}
                                        @endif
                                        <time>{{ $diary->date ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $diary->date)->format('M d') : '' }}</time>
								 - {{ $diary->name }}
							</span>
                                </td>
                            </tr>
                            <?php $count++?>
                            @endforeach
                        @else
                            <tr>
                                <td rowspan="1">
                                    <h3>{{ $title }}</h3>
                                </td>
                                <td>
                                    <p>No items {{ strtolower($title) }}</p>
                                </td>
                            </tr>
                        @endif
                    @endforeach
            </tbody>
        </table>
    </form>
</section>
