@if(!auth()->user()->hasRole('client'))
    <div class="data-container mt-3">
        <header>
            <h2>Notes</h2>
            <ul class="data-header-actions">
                @if(isset($read_only) && $read_only == 0)
                    <button type="button" class="btn mr-2 mb-2 btn-pill btn-outline-gray show-note-category">
                            Add Note
                    </button>

                    <button type="button" class="btn mr-2 mb-2 btn-pill btn-outline-gray add-re-diarise"
                            data-toggle="modal" data-model="Property"  data-object_id="{{request()->segment(2)}}"
                            data-object-type="1" data-selected="1"  data-object_type="properties" data-chosen_date="{{$diaryDate}}"
                            data-target="#reDiariseModal">Re-diarise
                    </button>
                @endif
            </ul>
        </header>

        @include('partials.show_note_section')

    </div>
@endif
