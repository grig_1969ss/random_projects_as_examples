{{ Form::open(['route' => $action, 'method' => ($rent->id ? 'PUT' : 'POST'), 'class' => 'form-horizontal', 'id' => 'rent-form']) }}

<div id='error-message'></div>
<div id="exp_rent_date" class="control-group">
    <label class="control-label" for="rent">Rent Expiry Date</label>
    <div class="form-group controls">
        <div class="input-append">
            {{ $exp_rent_date = ($rent->exp_rent_date) ? Date::dmY($rent->exp_rent_date) : null }}
            <input id="exp_rent_date" type='text' name="exp_rent_date" class='datepicker form-control' data-date-format='dd/mm/yyyy'
                   value="{{ $exp_rent_date }}" autocomplete="off">
            <span class="add-on"><i class="awe-calendar"></i></span>
            <span id="purchase_date" class="help-block"></span>
        </div>
        <span id="exp_rent_date" class="help-block"></span>
    </div>
</div>
<div id="rent" class="control-group">
    <label class="control-label" for="rent">Rent</label>
    <div class="form-group controls">
        <div class="input-prepend">
            <span class="add-on">{!! $property ? $property->present()->currencyHtmlCode() : '&pound;' !!}</span>
            <input id="rent" type='text' class="form-control" name="rent" value="{{ $rent->rent}}">
        </div>
        <span id="rent" class="help-block"></span>
    </div>
</div>
<div id="vat" class="control-group">
    <label class="control-label" for="vat">VAT</label>
    <div class="controls">
        <input type="hidden" name="vat" value="0">
        <input id="vat" type='checkbox' class="input-medium" name="vat" {{ (($rent->vat == 0) ? '' : 'checked') }} value="1">
        <span id="vat" class="help-block"></span>
    </div>
</div>
<div id="contracted_rent" class="control-group">
    <label class="control-label" for="contracted_rent">Contracted Rent</label>
    <div class="form-group controls">
        <div class="input-prepend">
            <span class="add-on">{!! $property ? $property->present()->currencyHtmlCode() : '&pound;' !!}</span>
            <input id="contracted_rent" type='text' class="form-control" name="contracted_rent" value="{{ $rent->contracted_rent}}">
        </div>
        <span id="contracted_rent" class="help-block"></span>
    </div>
</div>
<div class="form-actions">
    <span class="loading dark" style='display:none; float:left;' data-original-title="Loading, please wait…">Loading…</span>
    <a id="save-rent" class="btn btn-block mr-2 mb-2 btn-outline-primary" data-loading-text="Please Wait...">
        Save Rent
    </a>
</div>

{{ Form::close() }}
