<div id='documents-section'>
    @foreach ($documents as $document)
        @include('properties.includes.single_document')
    @endforeach
</div>
@if ($read_only == 0)
    <a class="modalButton btn btn-alt btn-primary" data-toggle="modal" href="#documentModal">Add Document</a>
@endif



