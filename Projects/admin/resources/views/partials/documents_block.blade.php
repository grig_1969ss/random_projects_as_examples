<div class="div-repl-form data-block documents-block">
    <header>
        <h2>Documents</h2>
        @if(!$read_only)
            <ul class="data-header-actions">
                <li><a href="#" class="btn btn-alt add-document modal-ajax" data-url="{{ $url }}">Add Document</a></li>
            </ul>
        @endif
    </header>

    <div class="content">
        @if($documents->count() == 0)
            <p>No documents found.</p>
        @else
            @foreach ($documents as $document)
                @include('properties.includes.single_document')
            @endforeach
        @endif
    </div>
</div>