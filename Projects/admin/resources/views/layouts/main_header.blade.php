<header class="container-fluid">
	<!-- Main page logo -->
	<h1>
		<span class="logo_main"><a href="/" class="brand">CAPA</a></span>

		@if(auth()->user()->client && auth()->user()->client->logo && file_exists('img/logos/'.auth()->user()->client->logo))
			<span class="logo_client">
				<a href="/"><img src="{{ asset('img/moved/body.jplogos/'.auth()->user()->client->logo) }}"></a>
			</span>
		@endif
	</h1>

	<div class="header-notices">
		<ul class="header-alerts">

			<!-- Active Maintenance Icon -->
			<li class="span1">
				<a href="/reports/maintenance">
					<img height="64" border="0" src="/img/moved/icons/icon-maintenance.png" alt="Maintenance">
					<span class="bubble"><?= is_numeric($activeMaintenanceCount) ? $activeMaintenanceCount : $activeMaintenanceCount ?></span>
                    <span class='header-icon-text'>Maintenance</span>
				</a>
			</li>
			<!-- /Active Maintenance Icon -->

			<!-- Rent Review Icon -->
			<li class="span1">
				<a href="{{ route('reports.rent-review', ['icon' => 1]) }}">
					<img height="64" border="0" src="/img/moved/icons/icon-rentreview.png" alt="Rent review due within next 6 months">
					<span class="bubble"><?= is_numeric($rentReviewDueCount) ? $rentReviewDueCount : 0 ?></span>
                    <span class='header-icon-text'>Rent Review</span>
				</a>
			</li>
			<!-- Rent Review Icon -->

			<!-- Lease Expiry Icon -->
			<li class="span1">
				<a href="{{ route('reports.lease-expiry', ['icon' => 1]) }}">
					<img height="64" border="0" src="/img/moved/icons/icon-leaseexpiry.png" alt="Lease Expiry due within next 12 Months">
					<span class="bubble"><?= is_numeric($leaseExpiryCount) ? $leaseExpiryCount : 0 ?></span>
                    <span class='header-icon-text'>Lease Expiry</span>
				</a>
			</li>
			<!-- /Lease Expiry Icon -->

            @if(false)
			<!-- Refit Icon -->
			<li class="span1">
				<a href="/reports/refit">
					<img height="64" border="0" src="/img/moved/icons/icon-refit.png" alt="Properties undergoing re-fit">
				<span class="bubble"><?= is_numeric($refitCount) ? $refitCount : 0 ?></span>
				<span class='header-icon-text'>Re-fit</span>
            </a>
			</li>
			<!-- /Refit Icon -->
            @endif
		</ul>
	</div>
</header>
