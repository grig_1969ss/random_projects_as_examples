<nav class="main-navigation" role="navigation">
	<ul>
		<li {{ ($pageNav == 'diary') ? 'class="current"' : '' }}>
            <a href="{{ route('diary.index') }}" class="no-submenu">
                <span class="awe-tasks"></span>Diary @badge($diaryCount)
            </a>
        </li>
		<li {{ ($pageNav == 'property') ? 'class="current"' : '' }}>
            <a href="{{ route('properties.index') }}" class="no-submenu">
                <span class="awe-home"></span>Properties @badge($propertyCount)
            </a>
        </li>
		<li {{ ($pageNav == 'agent') ? ' class="current"' : '' }}>
			<a href="/agents" class="no-submenu"><span class="awe-briefcase"></span>Managing Agents</a>
		</li>
		<li {{ ($pageNav == 'landlord') ? ' class="current"' : '' }}>
			<a href="/landlords" class="no-submenu"><span class="awe-briefcase"></span>Landlords</a>
		</li>
		<li {{ ($pageNav == 'client') ? ' class="current"' : '' }}>
			<a href="/clients" class="no-submenu"><span class="awe-briefcase"></span>Clients</a>
		</li>
		<li {{ ($pageNav == 'supplier') ? ' class="current"' : '' }}>
			<a href="{{ route('suppliers.index') }}" class="no-submenu"><span class="awe-briefcase"></span>Suppliers</a>
		</li>
		{{--<li {{ ($pageNav == 'administrator') ? ' class="current"' : '' }}>
			<a href="{{ route('administrators.index') }}" class="no-submenu">
				<span class="awe-briefcase"></span>Administrators
			</a>
		</li>--}}
		{{--<li {{ ($pageNav == 'auditor') ? ' class="current"' : '' }}>
			<a href="{{ route('auditors.index') }}" class="no-submenu"><span class="awe-briefcase"></span> Auditors</a>
		</li>--}}
		<li {{ ($pageNav == 'payment') ? ' class="current"' : '' }}>
			<a href="/payments" class="no-submenu"><span class="awe-edit"></span>Approvals</a>
		</li>
		{{--<li {{ ($pageNav == 'auditRegister') ? ' class="current"' : '' }}>
			<a href="/audit-registers" class="no-submenu"><span class="awe-edit"></span>Audit Registers</a>
		</li>--}}
        {{--<li {{ ($pageNav == 'claim') ? ' class="current"' : '' }}>
            <a href="/claims" class="no-submenu"><span class="awe-file"></span>Claims</a>
        </li>--}}
		<li {{ ($pageNav == 'report') ? ' class="current"' : '' }}>
			<a href="#"><span class="awe-signal"></span>Reports 1</a>
			<ul>
                <li><a href="{{ route('reports.master-spreadsheet') }}">Master Spreadsheet</a></li>
				<li><a href="{{ route('reports.rent-review') }}">Rent Reviews</a></li>
				<li><a href="{{ route('reports.lease-expiry') }}">Lease Expiries</a></li>
				<li><a href="{{ route('reports.rateable-value') }}">Rateable Value</a></li>
				<li><a href="{{ route('reports.break-option') }}">Break Option</a></li>
				<li><a href="{{ route('reports.condition') }}">Schedule Of Condition</a></li>
				<li><a href="{{ route('reports.document-checklist') }}">Document Checklist</a></li>
				<li><a href="{{ route('reports.approvals') }}">Approvals</a></li>
				<li><a href="{{ route('reports.approvals-by-property') }}">Approvals by Property</a></li>
				<li><a href="{{ route('reports.invoices-on-hold') }}">Invoices on Hold</a></li>
				{{--<li><a href="{{ route('reports.claims') }}">Claims</a></li>--}}
			</ul>
		</li>
		<li {{ ($pageNav == 'report') ? ' class="current"' : '' }}>
			<a href="#"><span class="awe-signal"></span>Reports 2</a>
			<ul>
				<li><a href="{{ route('reports.maintenance') }}">Outstanding Maintenance</a></li>
				<li><a href="{{ route('reports.rental-liability') }}">Rent</a></li>
				<li><a href="/reports/budget">Comparison on SC Budgets</a></li>
				<li><a href="/reports/actual">Comparison on Annual Charges</a></li>
				<li><a href="/reports/missing/budget">Missing Budgets</a></li>
				<li><a href="/reports/missingrecs/actual">Missing Reconciliations</a></li>
				<li><a href="{{ route('reports.service-charge') }}">Service Charge</a></li>
				<li><a href="/reports/on-account">On Account (Quarterly)</a></li>
				<li><a href="{{ route('reports.insurance-renewal-dates') }}">Insurance Renewal Dates</a></li>
				<li><a href="{{ route('reports.service-charge-annual-budget') }}">Service Charge Annual Budget</a></li>
				<li><a href="{{ route('reports.insurance-annual-budget') }}">Insurance Annual Budget</a></li>
			</ul>
		</li>
	</ul>
</nav>
