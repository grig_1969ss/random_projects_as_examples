<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js ie8 ie" lang="en">    <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9 ie" lang="en">    <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <title>{{ $pageTitle }} | CAPA Audit</title>
    <meta name="description" content="">
    <meta name="author" content="Softlabs | www.softlabs.co.uk">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/moved/capa-blue.css') }}">
    <link rel="stylesheet" type="text/css" media="all"
          href="{{ asset('/css/moved/custom.css?v='.filemtime('css/moved/custom.css')) }}">
    @if(App::environment('local'))
        <link rel="stylesheet" type="text/css" media="all" href="{{ asset('/css/moved/app.css') }}">
    @else
        <link rel="stylesheet" type="text/css" media="all" href="{{ asset('/css/moved/app.css?v='.filemtime('css/moved/app.css')) }}">
    @endif
    @stack('css')
    <link rel="shortcut icon" href="{{ asset('img/moved/icons/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="{{ asset('img/moved/icons/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="{{ asset('img/moved/icons/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('img/moved/icons/apple-touch-icon-57-precomposed.png') }}">
    <script src="{{ asset('js/moved/libs/jquery.js') }}"></script>
    <script src="{{ asset('js/moved/libs/modernizr.js') }}"></script>
    <script src="{{ asset('js/moved/libs/selectivizr.js') }}"></script>

    <script>
        $(document).ready(function () {
            // Tooltips
            $('[title]').tooltip({
                placement: 'top'
            });
        });
    </script>
</head>

<body data-page="{{ $page }}">
@if(!in_array($page, ['LoginIndex', 'AuthPage']))
{{--    @include('layouts.main_header')--}}
@endif

<section class="container-fluid" role="main">
    @if(!in_array($page, ['LoginIndex', 'AuthPage']))
{{--        @include('layouts.main_left')--}}
    @endif

    @if(session()->has('error'))
        <div class="content-block">
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">×</button>
                <strong>{{ session('error') }}</strong>
            </div>
        </div>
    @endif

    @yield('container')
</section>

<script src="{{ asset('js/moved/navigation.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-affix.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-alert.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-button.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-carousel.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-collapse.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-dropdown.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-modal.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-popover.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-scrollspy.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-tab.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-tooltip.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-transition.js') }}"></script>
<script src="{{ asset('js/moved/bootstrap/bootstrap-typeahead.js') }}"></script>
<script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@if(App::environment('local'))
{{--    <script src="{{ asset('js/moved/scripts.js') }}"></script>--}}
    <script src="{{ asset('js/moved/custom.js') }}"></script>
@else
{{--    <script src="{{ asset('js/moved/scripts.js?v='.filemtime('js/scripts.js')) }}"></script>--}}
    <script src="{{ asset('js/moved/custom.js?v='.filemtime('js/custom.js')) }}"></script>
@endif

@stack('js')
</body>
</html>
