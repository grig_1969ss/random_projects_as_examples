<div class="navigation-block">
    <section class="user-profile">
        <figure class="clearfix">
            <img class="edit-info"
                 alt="{{ auth()->user()->name }}"
                 src="{{ auth()->user()->avatar != '' ?  auth()->user()->avatar : asset('img/moved/assets/avatar.png') }}"
                 width="60"
                 height="60">
            <figcaption>
                <strong><a href="#" class="edit-info">{{ auth()->user()->name }}</a></strong>
                <em>{{ auth()->user()->job_title }}</em>
                <ul>
                    <li>
                        <a class="btn btn-primary btn-flat" href="{{ url('settings') }}" title="Account settings">
                            settings
                        </a>
                    </li>
                    @role('superAdmin')
                        <li>
                            <a href="{{ route('staff.index') }}"
                               class="btn btn-primary btn-flat"
                               title="Account settings">staff</a>
                        </li>
                    @endrole
                    <li><a class="btn btn-primary btn-flat logout-a" href="#" title="Logout">logout</a></li>
                </ul>

                <form method="POST" action="{{ route('logout') }}" class="logout-form hidden">
                    @csrf
                </form>
            </figcaption>
        </figure>
    </section>

    @yield('search')

    <form class="side-search">
        <input id="side-search" type="text" class="rounded" placeholder="To search type and hit enter">
    </form>

    @include('layouts.main_nav')
</div>
