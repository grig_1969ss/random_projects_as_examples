@extends('layouts_new.app', ['pageTitle' => 'Administrator', 'pageNav' => 'administrator', 'page' => 'AdministratorsDetail'])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $administrator->name }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="{{ route('administrators.edit', $administrator) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Name</dt>
                            <dd>{{ $administrator->name }}</dd>
                            <dt>Address</dt>
                            <dd>{{ $administrator->address->address_1 }}</dd>
                            <dd>{{ $administrator->address->address_2 }}</dd>
                            <dd>{{ $administrator->address->town }}</dd>
                            <dd>{{ $administrator->address->county }}</dd>
                            <dd>{{ $administrator->address->post_code }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($administrator->created_at) }}
                                by {{ optional($administrator->createdBy)->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Contacts</h2>
                        <ul class="data-header-actions">
                            <li><a href="{{ route('administrators.contacts.create', $administrator) }}" class="btn btn-alt">Add</a></li>
                        </ul>
                    </header>
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Telephone</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($administrator->contacts as $contact)
                            <?php $contactUrl = '/contacts/'.$contact->id; ?>
                            <tr class="odd gradeX">
                                <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
