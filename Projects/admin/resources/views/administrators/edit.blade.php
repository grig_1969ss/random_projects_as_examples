@extends('layouts_new.app', [
    'pageTitle' => ($administrator->id ? 'Edit' : 'Add').' Administrator',
    'pageNav' => 'administrator',
    'page' => 'AdministratorNew'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $administrator->id ? 'Edit' : 'New' }} Administrator</h2>
                    </header>
                    <div class="span8">
                        @include('administrators.includes.form')
                    </div>
                </div>
            </article>
        </div>
    </div>
@endsection