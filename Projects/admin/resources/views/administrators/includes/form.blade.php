<fieldset>
    <div id='error-message'></div>

    <form method="POST"
          action="{{ $administrator->id ? route('administrators.update', $administrator) : route('administrators.store') }}"
          class="form-horizontal"
          id="create-administrator">
        {{ csrf_field() }}

        @if($administrator->id)
            {{ method_field('PUT') }}
        @endif

        <div id="name" class="control-group">
            <label for="name" class="control-label">Name</label>
            <div class="controls">
                <input type="text" name="name" id="name" class="input-medium" value="{{ $administrator->name }}">
                <span id="name" class="help-block"></span>
            </div>
        </div>

        @if (!$administrator->id)
            <div id="contact-first_name" class="control-group">
                <label for="contact-first_name" class="control-label">Contact First Name</label>
                <div class="controls">
                    <input type="text" name="contact[first_name]" id="contact-first_name" class="input-medium">
                    <span id="contact-first_name" class="help-block"></span>
                </div>
            </div>
            <div id="contact-surname" class="control-group">
                <label for="contact-surname" class="control-label">Contact Surname</label>
                <div class="controls">
                    <input type="text" name="contact[surname]" id="contact-surname" class="input-medium">
                    <span id="contact-surname" class="help-block"></span>
                </div>
            </div>
            <div id="contact-email" class="control-group">
                <label for="contact-email" class="control-label">Contact Email</label>
                <div class="controls">
                    <input type="text" name="contact[email]" id="contact-email" class="input-medium">
                    <span id="contact-email" class="help-block"></span>
                </div>
            </div>
        @endif

        <div id="address-address_1" class="control-group">
            <label for="address_1" class="control-label">Address Line 1</label>
            <div class="controls">
                <input type="text"
                       name="address[address_1]"
                       id="address_1"
                       class="input-xlarge"
                       value="{{ $address->address_1 }}">
                <span id="address-address_1" class="help-block"></span>
            </div>
        </div>

        <div id="address-address_2" class="control-group">
            <label for="address_2" class="control-label">Address Line 2</label>
            <div class="controls">
                <input type="text"
                       name="address[address_2]"
                       id="address_2"
                       class="input-xlarge"
                       value="{{ $address->address_2 }}">
                <span id="address-address_2" class="help-block"></span>
            </div>
        </div>

        <div id="address-town" class="control-group">
            <label for="town" class="control-label">Town</label>
            <div class="controls">
                <input type="text" name="address[town]" id="town" class="input-medium" value="{{ $address->town }}">
                <span id="address-town" class="help-block"></span>
            </div>
        </div>

        <div id="address-county" class="control-group">
            <label for="county" class="control-label">County</label>
            <div class="controls">
                <input type="text"
                       name="address[county]"
                       id="county"
                       class="input-medium"
                       value="{{ $address->county }}">
                <span id="address-county" class="help-block"></span>
            </div>
        </div>

        <div id="address-post_code" class="control-group">
            <label for="post_code" class="control-label">Post Code</label>
            <div class="controls">
                <input type="text"
                       name="address[post_code]"
                       id="post_code"
                       class="input-small"
                       value="{{ $address->post_code }}">
                <span id="address-post_code" class="help-block"></span>
            </div>
        </div>

        <div class="form-actions">
            <button id="save-administrator" class="btn btn-large btn-primary" type="submit">
                Save Changes
            </button>
        </div>
    </form>
</fieldset>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
