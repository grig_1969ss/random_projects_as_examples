@extends('layouts_new.app',
    ['pageTitle' => 'Administrators', 'pageNav' => 'administrator', 'page' => 'AdministratorsIndex'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Administrators</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li class="demoTabs">
                                    <a href="{{ route('administrators.create') }}" class="btn btn-alt">
                                        Add Administrator
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </header>
                    <table id="example-2" class="datatable table table-striped table-bordered table-hover dataTable"
                           aria-describedby="example-2_info" style="width: 880px;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Main Contact</th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach ($administrators as $administrator)
                            <tr class="odd gradeX">
                                @if (!$read_only)
                                    <td>
                                        <a href="{{ route('administrators.show', $administrator) }}">
                                            {{ $administrator->name }}
                                        </a>
                                    </td>
                                    <td>
                                        @if($administrator->mainContact)
                                            <a href="{{ route('contacts.show', $administrator->mainContact) }}">
                                                {{ $administrator->present()->mainContact }}
                                            </a>
                                        @endif
                                    </td>
                                @else
                                    <td>{{ $administrator->present()->name }}</td>
                                    <td>{{ $administrator->present()->mainContact }}</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->
        </div>
        <!-- /Grid row -->
    </div>
    <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
