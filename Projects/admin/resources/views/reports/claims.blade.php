@extends('layouts_new.app', [
    'pageTitle' => 'Reports | Claims',
    'pageNav'   => 'reports',
    'page'      => 'ReportsClaims'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Claims</h2>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">
                                Sort By <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="client">Client</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="audit_register">Audit Name</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="claim_number">Claim Number</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="auditor">Auditor</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="supplier_number">Supplier Number</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="supplier_name">Supplier Name</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="claim_type">Claim Type</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="claim_subtype">Claim Subtype</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="invoice_number">Invoice Number</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="invoice_date">Invoice Date</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="original_net_value">Original Net Value</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="vat">VAT</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="gross_value">Gross Value</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="claim_adjustment">Claim Adjustment</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="cancelled_claim">Cancelled Claim</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="net_recovery">Net Recovery</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="claim_status">Claim Status</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="client_date">Date Passed to Client</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="po_number">PO Number</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="property_number">Property Number</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="property_address">Property Address</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="supplier_contact">Supplier Contact</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right p-r-10">
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('reports.claims-csv') }}"
                                       data-sortfield="client"
                                       class="btn btn-info print-report"><span class="awe-table"></span> Export</a>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @if($claims->count() == 0)
                            <p>No claims found.</p>
                        @else
                            @include('reports.includes.claims_table')
                        @endif
                    </section>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
