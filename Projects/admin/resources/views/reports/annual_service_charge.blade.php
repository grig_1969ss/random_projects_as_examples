@extends('layouts_new.app', ['pageTitle' => 'Reports | Annual Service Charge (Budget)', 'pageNav' => 'reports', 'page' => 'ReportsServiceCharge'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Annual Service Charge Report ({{$type}})</h2>
                        <input type='hidden' id='report-type' value='{{ strtolower($type) }}'>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">Sort By <span class="caret"></span></a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="reference">Reference</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="property_name">Property</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li><a href="{{ route('reports.annual-service-charge-csv') }}" data-sortfield="" class="btn btn-info"><span class="awe-table"></span> Export</a></li>
                                <li>
                                    <div class="input-append">
                                        <input id='date-filter'
                                               class="datepicker input-small change-date"
                                               type="text"
                                               name="sc.year_end"
                                               data-date-format="dd/mm/yyyy"
                                               data-sortfield="sc.year_end"
                                               autocomplete="off">
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @include('reports.includes.annual_service_charge_table')
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
