@extends('layouts_new.app', ['pageTitle' => 'Reports | Maintenance', 'pageNav' => 'reports', 'page' => 'ReportsMaintenance'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Properties with Active Maintenance</h2>
                        <a href="{{ route('reports.maintenance-csv') }}"
                           class="btn btn-info print-report mb-2">
                            <span class="awe-table"></span>Export
                        </a>
                    </header>
                    <section>
                        <!-- Active Maintenance Table -->
                        @if(!$properties)
                            <p>No properties have active maintenance.</p>
                        @else
                            <table id="example-2"
                                   class="datatable table table-striped table-bordered table-hover dataTable">
                                <thead>
                                <tr>
                                    <th>Region</th>
                                    <th>Ref</th>
                                    <th>Property</th>
                                    <th>Resolved</th>
                                    <th>Notice Date</th>
                                    <th>Address</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($properties as $property)
                                    <tr>
                                        <td><a href="/properties/{{$property->id}}">{{ $property->region_name }}</a>
                                        </td>
                                        <td width='100px'><a
                                                href="/properties/{{$property->id}}">{{ $property->reference }}</a></td>
                                        <td><a href="/properties/{{$property->id}}">{{ $property->property_name }}</a>
                                        </td>
                                        <td>
                                            <a href="/properties/{{$property->id}}">{{ $property->resolved ? 'Yes' : 'No' }}</a>
                                        </td>
                                        <td>
                                            <a href="/properties/{{$property->id}}">{{ Date::dmY($property->notice_date) }}</a>
                                        </td>
                                        <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->address)) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    @endif
                    <!-- /Active Maintenance Table -->

                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
