@extends('layouts_new.app', ['pageTitle' => 'Reports | Approvals', 'pageNav' => 'reports', 'page' => 'ReportsApprovals'])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Approvals</h2>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">
                                Sort By <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="site_no">Site No</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="store_name">Store Name</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="company">Company</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="payment_ref">Invoice No</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="payment_date">Invoice Date</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="gross_invoice_amount">Amount</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="payment_type">Description</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="period">Period</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="note">Note</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right p-r-10">
                            <ul class="data-header-actions date-range">
                                <li>
                                    <div class="input-append">
                                        <label>From: </label>
                                        <input id='date-filter-from'
                                               class="datepicker input-small change-date"
                                               value='{{ $viewDate }}'
                                               type="text"
                                               name="rent_review_date_from"
                                               data-date-format="dd/mm/yyyy"
                                               autocomplete="off">
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-append">
                                        <label>To: </label>
                                        <input id='date-filter-to'
                                               class="datepicker input-small change-date"
                                               value='{{ $viewDate }}'
                                               type="text"
                                               name="rent_review_date_to"
                                               data-date-format="dd/mm/yyyy" autocomplete="off">
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right p-r-10">
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('reports.approvals-pdf') }}"
                                       data-sortfield="site_no"
                                       class="btn btn-info print-report"><span class="awe-table"></span> Export</a>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @if($approvals->count() == 0)
                            <p>No approvals found.</p>
                        @else
                            @include('reports.includes.approvals_table')
                        @endif
                    </section>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
