@extends('layouts_new.app', [
    'pageTitle' => 'Reports | Invoices on Hold',
    'pageNav'   => 'reports',
    'page'      => 'ReportsInvoicesOnHold'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Invoices On Hold</h2>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">
                                Sort By <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="site_no">Property No</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="supplier">Supplier</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="contact_name">Contact Name</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="payment_ref">Inv Number</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="payment_date">Invoice Date</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="gross_invoice_amount">Inv Gross</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="tax_amount">Inv VAT</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="payment_type">Type</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="created_at">Date Entered</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="note">Notes</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="diary_date">Diary Date</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right p-r-10">
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('reports.invoices-on-hold-csv') }}"
                                       data-sortfield="site_no"
                                       class="btn btn-info print-report"><span class="awe-table"></span> Export</a>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @if($approvals->count() == 0)
                            <p>No invoices found.</p>
                        @else
                            @include('reports.includes.invoices_on_hold_table')
                        @endif
                    </section>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
