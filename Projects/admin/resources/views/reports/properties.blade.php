@extends('layouts_new.app', ['pageTitle' => 'Reports | Properties', 'pageNav' => 'reports', 'page' => 'ReportsProperties'])

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css">
@endpush

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            <div class="col-12 mb-4 d-flex justify-content-between">

                @if($properties->count() == 0)
                    <p>No properties found.</p>
                @else
                    @include('reports.includes.properties_table')
                @endif

            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script>

    <script>
        // Setup - add a text input to each footer cell
        $(document).ready(function () {

            $('#properties-reports-table thead tr').clone(true).appendTo('#properties-reports-table thead');
            $('#properties-reports-table thead tr:eq(1) th').each(function (i) {
                let title = $(this).text();

                $(this).html('<input class="input-search" type="text" placeholder="Search..." />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });

                $('input', this).on('click', function (e) {
                    e.stopPropagation();
                });

            });

            let table = $('#properties-reports-table').DataTable({
                lengthChange: false,
                paginate: false,
                "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
            });

        });

    </script>
@endpush


<script>
    $(document).ready(function () {

        $('#example').DataTable({});
    });
</script>
