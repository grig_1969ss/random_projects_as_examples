@extends('layouts_new.app', [
    'pageTitle' => 'Reports | Service Charge Annual Budget',
    'pageNav'   => 'reports',
    'page'      => 'ReportsServiceChargeAnnualBudget'
])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Service Charge Annual Budget Report</h2>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li>
                                    <select name="client_id" id="client_id">
                                        <option value="">Select...</option>
                                        @foreach($clients as $client)
                                            <option value="{{ $client->id }}"
                                                    {{ $clientId && $clientId == $client->id ? 'selected' : '' }}
                                                    data-url="{{ route('reports.service-charge-annual-budget', ['client_id' => $client->id]) }}">
                                                {{ $client->client_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <a href="{{ route('reports.service-charge-annual-budget-csv', $clientId ? ['client_id' => $clientId] : []) }}"
                                       data-sortfield=""
                                       class="btn btn-info">
                                        <span class="awe-table"></span> Export
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @include('reports.includes.service_charge_annual_budget_table')
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
