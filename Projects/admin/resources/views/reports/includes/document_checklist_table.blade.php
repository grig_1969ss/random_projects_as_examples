@if(!$properties)
    <p>No properties exist</p>
@else
    <div class="sortTables_wrapper" data-order="{{ $order }}">
        <table class='table table-striped table-bordered table-hover'>
            <thead>
            <tr>
                <th class="region">Region</th>
                <th class="reference">Ref</th>
                <th class="property_name">Property Name</th>
                <th style='text-align:center' class="lease">Lease</th>
                <th style='text-align:center' class="sublease">Sublease</th>
                <th style='text-align:center' class="licence_to_assign">Licence to Assign</th>
                <th style='text-align:center' class="licence_for_alterations">Licence for Alterations</th>
                <th style='text-align:center' class="rent_review_memorandum">Rent Review Memorandum</th>
                <th style='text-align:center' class="landlord_notices">Landlord Notices</th>
                <th style='text-align:center' class="schedule_of_condition">Schedule of Conditions</th>
                <th style='text-align:center' class="schedule_of_dilapidations">Schedule of Dilapidations</th>
                <th style='text-align:center' class="demise_plan">Demise Plan</th>
                <th style='text-align:center' class="goad_plan">Goad Plan</th>
            </tr>
            </thead>
            <tbody>
            @foreach($properties as $property)
                <tr>
                    <td><a href="#show-documents-modal" class='show-documents-modal'>{{ Util::showValue($property->region_name ) }}</a></td>
                    <td><a href="#show-documents-modal" class='show-documents-modal'>{{ Util::showValue($property->reference ) }}</a></td>
                    <td><a href="#show-documents-modal" class='show-documents-modal'>{{ Util::showValue($property->property_name) }}</a></td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->lease)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->sublease)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->licence_to_assign)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->licence_for_alterations)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->rent_review_memorandum)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->landlord_notices)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->schedule_of_condition)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->schedule_of_dilapidations)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->demise_plan)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    <td style='text-align:center;'>
                        <a href="#show-documents-modal" class='show-documents-modal'>
                            @if($property->goad_plan)
                                <img src="/img/moved/icons/accept.png">
                            @endif
                        </a>
                    </td>
                    {{ Form::hidden('property_id', $property->id, array('class' => 'property-id')) }}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
