<div class="sortTables_wrapper" data-order="">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="reference" width='15%'>Ref</th>
            <th class="property_name" width='35%'>Property</th>
            <th class="insurance_renewal_date" width='35%'>Renewal Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td><a href="/properties/{{ $item->id }}">{{ $item->reference }}</a></td>
                <td><a href="/properties/{{ $item->id }}">{{ $item->property_name }}</a></td>
                <td><a href="/properties/{{ $item->id }}">{{ $item->insurance_renewal_date }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>