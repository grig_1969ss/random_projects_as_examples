@if(!$properties)
    <p>No properties with break options.</p>
@else
    @include('reports.includes.generic_report_table')
@endif