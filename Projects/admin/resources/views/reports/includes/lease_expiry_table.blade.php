@if(!$properties)
    <p>No properties with leases expiring are due.</p>
@else
    @include('reports.includes.generic_report_table')
@endif