<div class="sortTables_wrapper" data-order="">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="reference" width='15%'>Ref</th>
            <th class="property_name" width='35%'>Property</th>
            <th class="service_charge" width='35%'>Service Charge</th>
        </tr>
        </thead>
        <tbody>
        @foreach($serviceCharges as $serviceCharge)
            <tr>
                <td><a href="/properties/{{ $serviceCharge->id }}">{{ $serviceCharge->reference }}</a></td>
                <td><a href="/properties/{{ $serviceCharge->id }}">{{ $serviceCharge->property_name }}</a></td>
                <td>
                    <a href="/properties/{{ $serviceCharge->id }}">
                        {{$serviceCharge->year_end}}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>