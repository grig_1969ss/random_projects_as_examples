<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class="table table-striped table-bordered table-hover">

        <thead>
        <tr>
            <th class="region" width='5%'>Region</th>
            <th class="reference" width='5%'>Ref</th>
            <th class="property_name" width='35%'>Property</th>
            <?php
            $serviceCharge = null;
            $maxDate = null;
            $minDate = null;
            $yearArray = [];
            // Loop through properties to get the earliest and latest service charge date
            foreach ($properties as $property) :
                if (isset($property->service_charge)) {
                    foreach ($property->service_charge as $key => $service_charge) :
                        if (is_null($maxDate) || $service_charge->year_end > $maxDate) :
                            $maxDate = Date::Y($service_charge->year_end);
                    endif;
                    if (is_null($minDate) || $service_charge->year_end < $minDate) :
                            $minDate = Date::Y($service_charge->year_end);
                    endif;
                    endforeach;
                }
            endforeach;
            ?>
            <?php if (is_null($maxDate) || is_null($minDate)) :?>
            <th class="service_charge_0" width='15%' style='text-align:center'>2010</th>
            <th class="service_charge_1" width='15%' style='text-align:center'>2011</th>
            <th class="service_charge_2" width='15%' style='text-align:center'>2012</th>
            <th class="service_charge_3" width='15%' style='text-align:center'>2013</th>
            <?php else : ?>
            <?php
            // loop through generating table headers for all dates from the minDate to the maxDate
            while ($minDate <= $maxDate) :
            ?>
            <?php $yearArray[] = $minDate ?>
            <?php $totalArray[$minDate] = 0 ?>
            <th class="service_charge_{{$minDate}}" width='15%' style='text-align:center'>{{$minDate}}</th>
            <?php $minDate++; ?>
            <?php endwhile; ?>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php

        ?>
        @foreach($properties as $property)
            <tr>
                <td><a href="/properties/{{ $property->id }}">{{ $property->region_name }}</a></td>
                <td width='100px'><a href="/properties/{{ $property->id }}">{{ $property->reference }}</a></td>
                <td><a href="/properties/{{ $property->id }}">{{ $property->property_name }}</a></td>
                @if (isset($property->service_charge))
                    <?php $key = 0; ?>
                    <?php
                    // loop through every year displaying service charges for that year
                    foreach ($yearArray ?? '' as $yearKey => $year) :
                    ?>
                    <?php
                    if (isset($property->service_charge[$key - 1]) and isset($property->service_charge[$key]) and $property->service_charge[$key - 1]->annual_payment != 0 and $property->service_charge[$key]->annual_payment != 0) :
                        $prevCompare = (($property->service_charge[$key]->annual_payment - $property->service_charge[$key - 1]->annual_payment) / $property->service_charge[$key - 1]->annual_payment) * 100;
                        $prevCompare = number_format($prevCompare, 2);
                    else:
                        $prevCompare = '-';
                    endif;
                    ?>
                    <td style='text-align:center'>
                        <a href="/properties/{{ $property->id }}">
                            <?php
                            // check that a service charge exists for the header date
                            if (isset($property->service_charge[$key]) and Date::Y($property->service_charge[$key]->year_end) == $year) :?>
                            Year End. {{ Date::dmY($property->service_charge[$key]->year_end) }}
                            <br>
                            £{{ number_format($property->service_charge[$key]->annual_payment, 2) }} ({{ $prevCompare }}%)
                            <?php $totalArray[$year] += $property->service_charge[$key]->annual_payment; ?>
                            <?php $key++; ?>
                            <?php else : ?>
                            -
                            <?php endif?>
                        </a>
                    </td>
                    <?php endforeach; ?>
                @else
                    <td style='text-align:center'><a href="/properties/{{ $property->id }}">-</a></td>
                @endif
            </tr>
        @endforeach

        <?php
        $key = 0;
        foreach ($yearArray ?? '' as $yearKey => $year) {
            if (isset($totalArray[$year - 1]) and isset($totalArray[$year]) and ($totalArray[$year - 1] != 0) and ($totalArray[$year] != 0)) :
                $prevCompare = (($totalArray[$year] - $totalArray[$year - 1]) / $totalArray[$year - 1]) * 100;
            $prevCompare = number_format($prevCompare, 2); else:
                $prevCompare = '-';
            endif;
            $key++;
            $compareArray[$year] = $prevCompare;
        }
        ?>

        <tr>
            <td colspan="3">Total</td>
            <?php foreach ($yearArray ?? '' as $yearKey => $year) : ?>
            <td style='text-align:center'>£{{number_format($totalArray[$year], 2)}} ({{$compareArray[$year]}}%)</td>
            <?php endforeach; ?>
        </tr>
        </tbody>
    </table>
</div>
