@php
    $yearBudgetTd = $yearBudgetTh = $yearRecTd = $yearRecTh  = $yearInsTd = $yearInsTh  = \Carbon\Carbon::now()->year;
    $subYears =  \Carbon\Carbon::now()->subYears(3)->year;
@endphp
<div class="sortTables_wrapper" data-order="">
    <table id="properties-reports-table"
           class='datatable table table-striped table-bordered table-hover dataTable table-responsive '>
        <thead>
        <tr>
            <th class="">Property</th>
            <th class="">Agent</th>
            <th class="">Client Surveyor</th>
            <th class="">Client Admin</th>
            <th class="">Store Tel No.</th>
            @for($i = $yearBudgetTh; $subYears<=$i; $i--)
                <th class="">Budget {{$i}}</th>
            @endfor
            @for($i = $yearRecTh; $subYears<=$i; $i--)
                <th class="">Reconciliation {{$i}}</th>
            @endfor
            @for($i = $yearInsTh; $subYears<=$i; $i--)
                <th class="">Schedule Received {{$i}}</th>
            @endfor
        </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            @php
                $las4sc = Util::las4sc($property);
                $las4ins = Util::las4ins($property);
            @endphp
            <tr>
                @php
                    $link = route('properties.show', $property->id);
                @endphp

                <td><a href="{{$link}}">{{Util::propertyName($property)}}</a></td>
                <td><a href="{{$link}}">{{Util::showAgent($property)}}</a></td>
                <td><a href="{{$link}}">{{$property->clientSurveyor->name ?? '-'}}</a></td>
                <td><a href="{{$link}}">{{$property->clientOwner->name ?? '-'}}</a></td>
                <td><a href="{{$link}}">{{$property->store_telephone ?? '-'}}</a></td>
                @for($i = $yearBudgetTd; $subYears<=$i; $i--)
                    <td class="text-center">
                        @if(!in_array($i, array_keys($las4sc)) || !$las4sc[$i]['budget_details_recd'])
                            <span class="text-danger">No</span>
                        @else
                            <span class="text-success">Yes</span>
                        @endif
                    </td>
                @endfor

                @for($i = $yearRecTd; $subYears<=$i; $i--)
                    <td class="text-center">
                        @if(!in_array($i, array_keys($las4sc)) || !$las4sc[$i]['annual_reconciliation_recd'])
                            <span class="text-danger">No</span>
                        @else
                            <span class="text-success">Yes</span>
                        @endif
                    </td>
                @endfor

                @for($i = $yearInsTd; $subYears<=$i; $i--)
                    <td class="text-center">
                        @if(!in_array($i, array_keys($las4ins)) || !$las4ins[$i]['cover_note_received'])
                            <span class="text-danger">No</span>
                        @else
                            <span class="text-success">Yes</span>
                        @endif
                    </td>
                @endfor

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
