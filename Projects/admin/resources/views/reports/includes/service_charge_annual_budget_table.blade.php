<div class="sortTables_wrapper" data-order="">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="property_ref" width="10%">Property Ref</th>
            <th class="property_name" width="30%">Property Name</th>
            <th class="owner_type" width="20%">Owner Type</th>
            <th class="renewal_date" width="20%">Renewal Date</th>
            <th class="annual_budget" width="20%">Annual Budget</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td><a href="/properties/{{ $item->id }}">{{ $item->property_ref }}</a></td>
                <td><a href="/properties/{{ $item->id }}">{{ $item->property_name }}</a></td>
                <td><a href="/properties/{{ $item->id }}">{{ $item->owner_type }}</a></td>
                <td><a href="/properties/{{ $item->id }}">{{ $item->renewal_date }}</a></td>
                <td><a href="/properties/{{ $item->id }}">{!! $item->annual_budget !!}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>