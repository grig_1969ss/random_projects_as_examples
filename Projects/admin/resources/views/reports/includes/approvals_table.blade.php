<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="site_no">Site No</th>
            <th class="store_name">Store Name</th>
            <th class="company">Company</th>
            <th class="payment_ref">Invoice No</th>
            <th class="payment_date">Invoice Date</th>
            <th class="gross_invoice_amount">Amount</th>
            <th class="payment_type">Description</th>
            <th class="period">Period</th>
            <th class="note">Notes</th>
        </tr>
        </thead>
        <tbody>
        @foreach($approvals as $approval)
            <tr>
                @php
                    $link = route('properties.show', $approval->property_id);
                @endphp

                <td><a href="{{ $link }}">{{ $approval->site_no }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->store_name }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->company }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->invoice_no }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->invoice_date }}</a></td>
                <td><a href="{{ $link }}">{!! $currency.' '.number_format($approval->amount, 2) !!}</a></td>
                <td><a href="{{ $link }}">{{ $approval->payment_type }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->period }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->notes }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>