<!DOCTYPE html>
<html>
<head>
    <style>
        html * {
            font-family: Arial !important;
        }
        table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        }

        th {
            text-align: left;
        }
        .w-100 {
            width: 100%;
        }
        .w-50 {
            width: 50%;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        /*.body {
            width: 29.7cm;
            border: 1px solid black;
            padding: 10px;
        }*/
        .bordered {
            border-collapse: collapse;
        }
        .bordered td, .bordered th {
            border: 1px solid black;
            padding: 2px 5px;
        }
        .no-border {
            border: 0 !important;
        }
        .fixed {
            table-layout: fixed;
        }
    </style>
</head>
<body>
<div>
    

    <table class="w-100">
        <tr>
            <td style="width: 20%" class="w-50 text-left"><img src="{{ public_path('img/moved/template_logo.png') }}"></td>
            <td style="width: 40%"  class="w-50 text-left"><h3>Invoice Approval Sheet</h3></td>
            <td style="width: 40%"  class="w-50 text-center">Ref: {{ $dateFrom }} - {{ $dateTo }}</td>
        </tr>
    </table>

    @foreach($approvals as $key => $paymentType)
        <h3>{{ $key }}</h3>

        <table class="w-100 bordered fixed">
            <tr>
                <th style="width: 5%">Site No</th>
                <th style="width: 10%">Store Name</th>
                <th style="width: 10%">Company</th>
                <th style="width: 10%">Invoice No</th>
                <th style="width: 10%" class="text-right">Invoice Date</th>
                <th style="width: 8%" class="text-right">Amount</th>
                <th style="width: 10%">Description</th>
                <th style="width: 10%">Period</th>
                <th style="width: 17%">Notes</th>
            </tr>
            @php
                $sumAmount = 0;
            @endphp
            @foreach($paymentType as $approval)
                <tr>
                    <td>{{ $approval->site_no }}</td>
                    <td>{{ $approval->store_name }}</td>
                    <td>{{ $approval->company }}</td>
                    <td>{{ $approval->invoice_no }}</td>
                    <td class="text-right">{{ $approval->invoice_date }}</td>
                    <td class="text-right">{!! $currency.' '.number_format($approval->amount, 2) !!}</td>
                    <td>{{ $approval->payment_type }}</td>
                    <td>{{ $approval->period }}</td>
                    <td>{{ $approval->notes }}</td>
                </tr>

                @php
                    $sumAmount += $approval->amount;
                @endphp
            @endforeach
            <tr>
                <td colspan="4" class="no-border"></td>
                <td class="text-right">Total</td>
                <td class="text-right">{!! $currency.' '.number_format($sumAmount, 2) !!}</td>
                <td colspan="3" class="no-border"></td>
            </tr>
        </table>

        @if($key == 'Service Charge & Insurance')
            <p>The above invoices have been approved by Capa.</p>
        @endif
    @endforeach
</div>
</body>
</html>
