<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="site_no">Property No</th>
            <th class="supplier">Supplier</th>
            <th class="contact_name">Contact Name</th>
            <th class="payment_ref">Inv Number</th>
            <th class="payment_date">Invoice Date</th>
            <th class="gross_invoice_amount">Inv Gross</th>
            <th class="tax_amount">Inv VAT</th>
            <th class="payment_type">Type</th>
            <th class="created_at">Period</th>
            <th class="notes">Notes</th>
            <th class="diary_date">Diary Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($approvals as $approval)
            <tr>
                @php
                    $link = route('properties.show', $approval->property);
                @endphp
                <td><a href="{{ $link }}">{{ $approval->property->reference }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->present()->supplier }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->present()->contact }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->payment_ref }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->present()->paymentDate }}</a></td>
                <td><a href="{{ $link }}">{!! $approval->present()->grossInvoiceAmount !!}</a></td>
                <td><a href="{{ $link }}">{!! $approval->present()->taxAmount !!}</a></td>
                <td><a href="{{ $link }}">{{ $approval->present()->paymentType }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->period }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->note }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->present()->diaryDate }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>