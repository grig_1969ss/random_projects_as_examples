<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="region" width='5%'>Region</th>
            <th class="reference" width='5%'>Ref</th>
            <th class="property_name" width='35%'>Property</th>
            <th class="year_end" width='15%' style='text-align:center'>Year End</th>
            <th class="town">Address</th>
            <th>Landlord Name</th>
            <th>Landlord Address</th>
            <th>Managing Agent</th>
            <th>Managing Agent Address</th>
        </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            <tr>
                <td><a href="/properties/{{$property->id}}">{{ $property->region_name }}</a></td>
                <td width='100px'><a href="/properties/{{$property->id}}">{{ $property->reference }}</a></td>
                <td><a href="/properties/{{$property->id}}">{{ $property->property_name }}</a></td>
                <td style='text-align:center'><a href="/properties/{{$property->id}}">{{ Date::dmY($property->year_end) }}</a></td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->address)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->landlord_name)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->landlord_address)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->landlord_agent)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->agent_address)) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>