<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class='datatable table table-striped table-bordered table-hover dataTable table-responsive'>
        <thead>
        <tr>
            <th class="reference">Region</th>
            <th class="reference">Ref</th>
            <th class="property_name">Property</th>
            <th class="town">Address</th>
            <th>General</th>
            <th>Lease Start Date</th>
            <th class="rent_review_date">Rent Review Date</th>
            <th class="lease_expiry_date">Lease Expiry Date</th>
            <th>Current Rent</th>
            <th class="break_option_date">Break Option Date</th>
            <th class="break_option_notice_period">Notice Date</th>
            <th class="rateable_value">Rateable Value</th>
            <th>Landlord Name</th>
            <th>Landlord Address</th>
            <th>Managing Agent</th>
            <th>Managing Agent Address</th>
            <th>Repair Liability</th>
            <th>Schedule of Condition</th>
            <th>Repair Obligations Note</th>
            <th>Prohibitions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            <tr>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->region_name)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->reference)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->property_name)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->address)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->general_info)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue(Date::dmY($property->lease_start_date))) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue(Date::dmY($property->rent_review_date))) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue(Date::dmY($property->lease_expiry_date))) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->current_rent)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue(Date::dmY($property->break_option_date))) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue(Date::dmY($property->break_option_notice_period))) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->rateable_value)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->landlord_name)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->landlord_address)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->landlord_agent)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->agent_address)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->repair_liability)) }}</td>
                <td><a target='_blank' href="{{$property->schedule_of_condition_link}}">{{ Util::showValue($property->schedule_of_condition) }}</a></td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->repair_obligations_note)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->user_clause_note)) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
