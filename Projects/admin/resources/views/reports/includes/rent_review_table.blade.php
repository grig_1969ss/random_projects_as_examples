@if(!$properties)
    <p>No properties rent reviews are due.</p>
@else
    @include('reports.includes.generic_report_table')
@endif