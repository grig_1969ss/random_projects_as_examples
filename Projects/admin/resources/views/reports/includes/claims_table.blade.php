<div class="sortTables_wrapper table-responsive" data-order="{{ $order }}" style="max-height: 600px">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="client">Client</th>
            <th class="audit_register">Audit Register</th>
            <th class="claim_number">Claim Number</th>
            <th class="auditor">Auditor</th>
            <th class="supplier_number">Supplier Number</th>
            <th class="supplier_name">Supplier Name</th>
            <th class="claim_type">Claim Type</th>
            <th class="claim_subtype">Claim Subtype</th>
            <th class="invoice_number">Invoice Number</th>
            <th class="invoice_date">Invoice Date</th>
            <th class="original_net_value">Original Net Value</th>
            <th class="vat">VAT</th>
            <th class="gross_value">Gross Value</th>
            <th class="claim_adjustment">Claim Adjustment</th>
            <th class="cancelled_claim">Cancelled Claim</th>
            <th class="net_recovery">Net Recovery</th>
            <th class="claim_status">Claim Status</th>
            <th class="client_date">Date Passed to Client</th>
            <th class="po_number">PO Number</th>
            <th class="property_number">Property Number</th>
            <th class="property_address">Property Address</th>
            <th class="supplier_contact">Supplier Contact</th>
        </tr>
        </thead>
        <tbody>
        @foreach($claims as $claim)
            <tr>
                @php
                    $link = route('claims.show', $claim->id);
                @endphp
                <td><a href="{{ $link }}">{{ $claim->client }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->audit_register }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->claim_number }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->auditor }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->supplier_number }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->supplier_name }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->claim_type }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->claim_subtype }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->invoice_number }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->custom_invoice_date }}</a></td>
                <td><a href="{{ $link }}">{!! $claim->original_net_value !!}</a></td>
                <td><a href="{{ $link }}">{!! $claim->vat !!}</a></td>
                <td><a href="{{ $link }}">{!! $claim->custom_gross_value !!}</a></td>
                <td><a href="{{ $link }}">{!! $claim->claim_adjustment !!}</a></td>
                <td><a href="{{ $link }}">{!! $claim->cancelled_claim !!}</a></td>
                <td><a href="{{ $link }}">{!! $claim->custom_net_recovery !!}</a></td>
                <td><a href="{{ $link }}">{{ $claim->claim_status }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->custom_client_date }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->po_number }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->property_number }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->property_address }}</a></td>
                <td><a href="{{ $link }}">{{ $claim->supplier_contact }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>