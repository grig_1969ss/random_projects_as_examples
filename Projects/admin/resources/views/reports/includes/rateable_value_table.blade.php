@if(!$properties)
    <p>No properties with ratable values.</p>
@else
    @include('reports.includes.generic_report_table')
@endif