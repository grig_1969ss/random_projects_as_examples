<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="region">Region</th>
            <th class="reference">Ref</th>
            <th class="property_name">Property</th>
            <th class="town">Address</th>
            <th class="document_name">Document</th>
        </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            <tr>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->region_name)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->reference)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->property_name)) }}</td>
                <td>{{ Html::link('/properties/'.$property->id, Util::showValue($property->address)) }}</td>
                <td><a href="{{ Storage::url($property->document_file) }}">{{ $property->document_name }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>