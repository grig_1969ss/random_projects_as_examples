<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="region" width='5%'>Region</th>
            <th class="reference" width='5%'>Ref</th>
            <th class="property_name" width='25%'>Property</th>
            <th class="rent_0" width='15%' style='text-align:center'>Rent Payment Frequency</th>
            <th class="rent_0" width='15%' style='text-align:center'>Current Rent</th>
            <th class="rent_1" width='15%' style='text-align:center'>+1</th>
            <th class="rent_2" width='15%' style='text-align:center'>+2</th>
            <th class="rent_3" width='15%' style='text-align:center'>+3</th>
            <th class="rent_4" width='15%' style='text-align:center'>+4</th>
            <th class="rent_5" width='15%' style='text-align:center'>+5</th>
        </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            <tr>
                <td>{{ $property->region_name }}</td>
                <td width='100px'>{{ $property->reference }}</td>
                <td>{{ $property->property_name }}</td>
                <td>{{ $property->rent_payment_frequency }}</td>
                @if (isset($property->rent))
                    <?php foreach ($property->rent as $key => $rent) : ?>
                    <?php
                    if (isset($property->rent[$key - 1]) and isset($property->rent[$key]) and $property->rent[$key - 1]->rent != 0 and $property->rent[$key]->rent != 0) :
                        $prevCompare = (($property->rent[$key]->rent - $property->rent[$key - 1]->rent) / $property->rent[$key - 1]->rent) * 100;
                        $prevCompare = number_format($prevCompare, 2);
                    else:
                        $prevCompare = '-';
                    endif;
                    ?>
                    <td style='text-align:center'><a href='/properties/{{$property->id}}'>Exp. {{ Date::dmY($rent->exp_rent_date) }}<br>£{{ number_format($rent->rent, 2) }} ({{ $prevCompare }}%)</td>
                    <?php endforeach; ?>
                    @for ($i = 0+count($property->rent); $i <= 5; $i++)
                        <td style='text-align:center'>-</td>
                    @endfor
                @else
                    <td style='text-align:center'>-</td>
                    <td style='text-align:center'>-</td>
                    <td style='text-align:center'>-</td>
                    <td style='text-align:center'>-</td>
                    <td style='text-align:center'>-</td>
                    <td style='text-align:center'>-</td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>