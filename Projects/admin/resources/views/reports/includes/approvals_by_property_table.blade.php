<div class="sortTables_wrapper">
    <table class='table table-striped table-bordered table-hover'>
        <thead>
        <tr>
            <th class="property_name">Property Name</th>
            <th class="supplier">Supplier</th>
            <th class="invoice_date">Supplier Invoice Date</th>
            <th class="payment_ref">Supplier Invoice Number</th>
            <th class="payment_type">Payment Type</th>
            <th class="period">Period</th>
            <th class="gross_invoice_amount">Gross Invoice Value</th>
            <th class="approved_date">Approved Date</th>
            <th class="held">Held</th>
        </tr>
        </thead>
        <tbody>
        @foreach($approvals as $approval)
            <tr>
                @php
                    $link = route('properties.show', $approval->property_id);
                @endphp

                <td><a href="{{ $link }}">{{ $approval->property_name }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->supplier }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->invoice_date }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->payment_ref }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->payment_type }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->period }}</a></td>
                <td><a href="{{ $link }}">{!! $currency !!}{{ $approval->gross_invoice_amount }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->approved_date }}</a></td>
                <td><a href="{{ $link }}">{{ $approval->held }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>