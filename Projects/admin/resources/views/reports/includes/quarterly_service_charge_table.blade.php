<div class="sortTables_wrapper" data-order="{{ $order }}">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="region" width='5%'>Region</th>
            <th class="reference" width='5%'>Ref</th>
            <th class="property_name" width='35%'>Property</th>

            <th class="service_charge_0" width='15%' style='text-align:center'>Quarter 1</th>
            <th class="service_charge_1" width='15%' style='text-align:center'>Quarter 2</th>
            <th class="service_charge_2" width='15%' style='text-align:center'>Quarter 3</th>
            <th class="service_charge_3" width='15%' style='text-align:center'>Quarter 4</th>
        </tr>
        </thead>
        <tbody>
        <?php

        ?>
        @foreach($properties as $propertyId => $property)
            <tr>
                <td></td>
                <td width='100px'><a href="/properties/{{ $propertyId }}">{{ $property['ref'] }}</a></td>
                <td><a href="/properties/{{ $propertyId }}">{{ $property['property_name'] }}</a></td>
                <td style="text-align: center;"><a href="/properties/{{$propertyId}}">{{ ($property['current_year_payment']) ? '£' . $property['current_year_payment'] : '-'}}</a></td>
                <td style="text-align: center;"><a href="/properties/{{$propertyId}}">{{ ($property['current_year_payment']) ? '£' . $property['current_year_payment'] : '-'}}</a></td>
                <td style="text-align: center;"><a href="/properties/{{$propertyId}}">{{ ($property['current_year_payment']) ? '£' . $property['current_year_payment'] : '-'}}</a></td>
                <td style="text-align: center;"><a href="/properties/{{$propertyId}}">{{ ($property['current_year_payment']) ? '£' . $property['current_year_payment'] : '-'}}</a></td>
            </tr>
        @endforeach


        <tr>
            <td colspan="3">Total</td>
            <td style="text-align: center;">£{{ $quarter_total }}</td>
            <td style="text-align: center;">£{{ $quarter_total }}</td>
            <td style="text-align: center;">£{{ $quarter_total }}</td>
            <td style="text-align: center;">£{{ $quarter_total }}</td>
        </tr>
        </tbody>
    </table>
</div>