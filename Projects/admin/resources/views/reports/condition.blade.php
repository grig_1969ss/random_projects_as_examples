@extends('layouts_new.app', ['pageTitle' => 'Reports | Schedule of Condition', 'pageNav' => 'reports', 'page' => 'ReportsCondition'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Schedule of Condition</h2>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">Sort By <span class="caret"></span></a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="reference">Reference</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="property_name">Property</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="town">Address</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="document_name">Document</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('reports.condition-csv') }}"
                                       data-sortfield="property_name"
                                       class="btn btn-info print-report">
                                        <span class="awe-table"></span> Export
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @include('reports.includes.condition_table')
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
