@extends('layouts_new.app', ['pageTitle' => $constants['pageTitle'], 'pageNav' => 'reports', 'page' => $constants['page']])

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
    <link rel="stylesheet" href="{{asset('dist/css/buttons.dataTables.min.css')}}">
@endpush


@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            <div class="data-container">
                <div class="col-12 mb-4 d-flex justify-content-around align-items-center">
                    <div>
                        <h5 class="font-weight-bold">{{$constants['pageName']}}</h5>
                    </div>
                </div>


                <form action="" method="get">
                    @if(request()->get('show_surveyor'))
                        <input type="hidden" name="show_surveyor" value="true">
                    @endif
                    <div class="row">
                        @if(Util::showReportDatePickers( request()->get('show_datepicker'),  request()->segment(3)) )
                            <input type="hidden" name="show_datepicker" value="true">

                            <div class="col-2 report-form-filters">
                                <!-- Form -->
                                <small class="d-block font-weight-normal mb-1">From</small>
                                <div class="form-group focused">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input
                                            id='date-from'
                                            value="{{request()->get('date_from') ?? ''}}"
                                            class="form-control datepicker change-date"
                                            name="date_from"
                                            type="text"
                                            data-date-format="dd/mm/yyyy"
                                            autocomplete="off"
                                            placeholder="Select date"
                                        >
                                    </div>
                                </div>
                                <!-- End of Form -->
                            </div>
                            <div class="col-2 report-form-filters">
                                <!-- To -->
                                <small class="d-block font-weight-normal mb-1">To</small>
                                <div class="form-group focused">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input
                                            id='date-to'
                                            value="{{request()->get('date_to') ?? ''}}"
                                            class="form-control datepicker change-date"
                                            placeholder="Select date"
                                            name="date_to"
                                            data-date-format="dd/mm/yyyy"
                                            autocomplete="off"
                                            type="text">
                                    </div>
                                </div>
                                <!-- End of Form -->
                            </div>
                        @endif

                        @if(Util::showReportSurveyor(request()->get('show_surveyor'),  request()->segment(3)) )
                            <div class="col-2 report-form-filters">
                                <div class="form-group">
                                    <label for="surveyor_id"></label>
                                    <select id="surveyor_id" name="surveyor_id" class="custom-select mr-sm-2">
                                        <option value="" disabled selected>Select a surveyor</option>
                                        @foreach($surveyors as $surveyor)
                                            <option
                                                @if(request()->get('surveyor_id') == $surveyor->id) selected @endif
                                            value="{{$surveyor->id}}">{{$surveyor->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                        <div id="report-filter-submit" class="col-2 sr-only">
                            <div class="form-group">
                                <button class="mt-4 btn btn-outline-gray print-report" type="submit">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

                @if(Util::showReportExportButton( request()->get('show_export'), request()->segment(3)) )
                    <a href="{{route('reports.download.zip')}}" class="dt-button buttons-excel buttons-html5"
                       tabindex="0"
                       aria-controls="common-report-table" type="button"><span>Export</span></a>
                @endif

                <table id="common-report-table" class='table table-responsive table-striped table-bordered table-hover'></table>
            </div>

        </div>

    </div>

    <input type="hidden" id="headers" value="{{$headers}}">
    <input type="hidden" id="data" value="{{$data}}">

@endsection

@push('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script>
    <script src="{{asset('dist/js/dataTable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('dist/js/dataTable/jszip.min.js')}}"></script>
    <script src="{{asset('dist/js/dataTable/pdfmake.min.js')}}"></script>
    <script src="{{asset('dist/js/dataTable/vfs_fonts.js')}}"></script>
    <script src="{{asset('dist/js/dataTable/buttons.html5.min.js')}}"></script>


    <script>
        // Setup - add a text input to each footer cell
        $(document).ready(function () {

            let table = $('#common-report-table').DataTable({
               // scrollX: "100%",
              // "bAutoWidth": false,
                lengthChange: false,
                paginate: true,
                orderCellsTop: true,
                sDom: "Bfrtip",
                buttons: [
                    'excel',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        customize: function (doc) {
                            console.log(doc.defaultStyle.fontSize);
                            doc.defaultStyle.fontSize = 8
                        }
                    }
                ],
                // sPaginationType: "bootstrap",
                "dom": '<"top"Bfrtip<"clear">>rt<"bottom"iflp<"clear">>',
                "data": JSON.parse($('#data').val()),
                "columns": JSON.parse($('#headers').val())
            });

            // stop conflict with tr click and document url click
            $('#common-report-table').on('click', 'td:last-child', function (e) {
                if ($(e.target).closest('a').length) {
                    e.stopPropagation();
                }
            });

            $('#common-report-table').on('click', 'tr', function (e) {

                // prevent continue this block, when click is just for sorting
                if ($(e.target).first().prop('nodeName').toLowerCase() === 'th') {
                    return false;
                }

                let propertyId;
                let data = table.row(this).data();

                if (data.property_id) {
                    propertyId = data.property_id
                } else {
                    propertyId = data.id;
                }

                window.open('/properties/' + propertyId, '_blank');

            });

            $('#common-report-table thead tr').clone(true).appendTo('#common-report-table thead');
            $('#common-report-table thead tr:eq(1) th').each(function (i) {
                let title = $(this).text();

                $(this).html('<input class="input-search" type="text" placeholder="Search..." />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });

                $('input', this).on('click', function (e) {
                    e.stopPropagation();
                });

            });

            // show submit button if any filter is shown
            let filterIsShown = $('.report-form-filters').length;
            if (filterIsShown) {
                $('#report-filter-submit').removeClass('sr-only');
            }

        });


    </script>
@endpush
