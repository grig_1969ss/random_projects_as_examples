@extends('layouts_new.app', ['pageTitle' => 'Reports | Document Checklist', 'pageNav' => 'reports', 'page' => 'ReportsDocumentChecklist'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Document Checklist</h2>

                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">Sort By <span class="caret"></span></a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href='#' class='sort-by' data-sortfield='reference'>Reference</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='property_name'>Property Name</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='lease'>Lease</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='sublease'>Sublease</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='licence_to_assign'>Licence to Assign</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='licence_for_alterations'>Licence for Alterations</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='rent_review_memorandum'>Rent Review Memorandum</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='landlord_notices'>Landlord Notices</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='schedule_of_condition'>Schedule of Conditions</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='schedule_of_dilapidations'>Schedule of Dilapidations</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='demise_plan'>Demise Plan</a></li>
                                <li><a href='#' class='sort-by' data-sortfield='goad_plan'>Goad Plan</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('reports.document-checklist-csv') }}"
                                       data-sortfield="property_name"
                                       class="btn btn-info print-report">
                                        <span class="awe-table"></span> Export
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </header>
                    <section class="report-table">
                        @include('reports.includes.document_checklist_table')
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->

    <div class="modal fade hide" id="documentModal">
        <div class="modal-header">
            Documents<button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body">
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
