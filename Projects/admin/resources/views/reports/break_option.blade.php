@extends('layouts_new.app', ['pageTitle' => 'Reports | Break Option', 'pageNav' => 'reports', 'page' => 'ReportsBreakOption'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Break Option</h2>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">Sort By <span class="caret"></span></a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="reference">Reference</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="property_name">Property</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="town">Address</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="break_option_date">Break Option Date</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="break_option_notice_period">Notice Date</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li>
                                    <div class="input-append">
                                        <label style="display: inline;">From: </label>
                                        <input id='date-filter-from'
                                               class="datepicker input-small change-date"
                                               value='{{ $viewDate }}'
                                               type="text"
                                               name="rent_review_date_from"
                                               data-date-format="dd/mm/yyyy" autocomplete="off">
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-append">
                                        <label style="display: inline;">To: </label>
                                        <input id='date-filter-to'
                                               class="datepicker input-small change-date"
                                               value='{{ $viewDate }}'
                                               type="text"
                                               name="rent_review_date_to"
                                               data-date-format="dd/mm/yyyy" autocomplete="off">
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li><a href="{{ route('reports.break-option-csv') }}" data-sortfield="break_option_date" class="btn btn-info print-report"><span class="awe-table"></span> Export</a></li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @include('reports.includes.break_option_table')
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
