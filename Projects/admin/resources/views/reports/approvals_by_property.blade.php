@extends('layouts_new.app', [
    'pageTitle' => 'Reports | Approvals By Property',
    'pageNav'   => 'reports',
    'page'      => 'ReportsApprovalsByProperty'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Approvals By Property</h2>
                        <div class="pull-right p-r-10">
                            <div class="">
                                <table>
                                    <tr class="v-middle">
                                        <td class="p-5"><strong>Property:</strong></td>
                                        <td class="p-5">
                                            <select name="property_id" id="property_id" style="margin-bottom: 0">
                                                <option value="">Select...</option>
                                                @foreach($properties as $property)
                                                    <option value="{{ $property->id }}">
                                                        {{ $property->reference }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="btn-group pull-right p-r-10">
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('reports.approvals-by-property-csv') }}"
                                       data-sortfield="payment_type"
                                       class="btn btn-info print-report"><span class="awe-table"></span> Export</a>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @if($approvals->count() == 0)
                            <p>No approvals found.</p>
                        @else
                            @include('reports.includes.approvals_table')
                        @endif
                    </section>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
