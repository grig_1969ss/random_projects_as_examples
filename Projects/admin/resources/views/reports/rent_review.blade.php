@extends('layouts_new.app', ['pageTitle' => 'Reports | Rent Reviews', 'pageNav' => 'reports', 'page' => 'ReportsRentReviewTable'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">

            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">

                    <div class="col-12 mb-4 d-flex justify-content-around align-items-center">
                        <div>
                            <h5 class="font-weight-bold">Rent Reviews</h5>
                        </div>

                        <div >
                            <div class="btn-group mr-2 mb-2">
                                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-angle-down dropdown-arrow"></i>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <button type="button" class="btn btn-secondary">Sort By</button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                    <a class="dropdown-item sort-by" href="#" data-sortfield="reference">Reference</a>
                                    <a class="dropdown-item sort-by" href="#"
                                       data-sortfield="property_name">Property</a>
                                    <a class="dropdown-item sort-by" href="#" data-sortfield="town">Address</a>
                                    <a class="dropdown-item sort-by" href="#" data-sortfield="rent_review_date">Rent
                                        Review Date</a>
                                </div>
                            </div>
                        </div>
                        <div >
                            <!-- Form -->
                            <small class="d-block font-weight-normal mb-1">From</small>
                            <div class="form-group focused">
                                <div class="input-group input-group-border">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input
                                        id='date-filter-from'
                                        value='{{ $viewDate }}'
                                        class="form-control datepicker change-date"
                                        name="rent_review_date_from"
                                        type="text"
                                        data-date-format="dd/mm/yyyy"
                                        autocomplete="off"
                                        placeholder="Select date"
                                    >
                                </div>
                            </div>
                            <!-- End of Form -->
                        </div>
                        <div >
                            <!-- Form -->
                            <small class="d-block font-weight-normal mb-1">To</small>
                            <div class="form-group focused">
                                <div class="input-group input-group-border">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input
                                        id='date-filter-to'
                                        value='{{ $viewDate }}'
                                        class="form-control datepicker change-date"
                                        placeholder="Select date"
                                        name="rent_review_date_to"
                                        data-date-format="dd/mm/yyyy"
                                        autocomplete="off"
                                        type="text">
                                </div>
                            </div>
                            <!-- End of Form -->
                        </div>

                        <a href="{{ route('reports.rent-review-csv') }}" data-sortfield="rent_review_date"
                           class="btn mr-2 mb-2 btn-pill btn-outline-secondary print-report" type="button">Export
                        </a>

                    </div>

                    <section class="report-table">
                        @include('reports.includes.rent_review_table')
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
