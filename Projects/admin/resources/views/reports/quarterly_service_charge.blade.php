@extends('layouts_new.app', ['pageTitle' => 'Reports | Quarter Service Charge (Actual)', 'pageNav' => 'reports', 'page' => 'ReportsServiceCharge'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">
            <!-- Example jQuery Visualize -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Quarter Service Charge Report ({{$type == 'onaccount' ? 'budget' : 'actual'}})</h2>
                        <input type='hidden' id='report-type' value='{{ strtolower($type) }}'>
                        <div class="btn-group pull-right">
                            <a href="#" data-toggle="dropdown" class="btn btn-alt btn-inverse dropdown-toggle">Sort By <span class="caret"></span></a>
                            <ul class="dropdown-menu datatable-controls">
                                <li><a href="#" class="sort-by" data-sortfield="reference">Reference</a></li>
                                <li><a href="#" class="sort-by" data-sortfield="property_name">Property</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="padding-right: 10px;">
                            <ul class="data-header-actions">
                                <li>
                                    <a id="csv-link"
                                       href="{{ route('reports.quarter-budget-csv') }}"
                                       data-sortfield=""
                                       class="btn btn-info">
                                        <span class="awe-table"></span> Export
                                    </a>
                                </li>
                                <li>
                                    <div class="input-append">
                                        {{ Form::select('sc.year_end', $yearsRange, null, ['class' => 'input-small change-date', 'data-sortfield' => 'p.property_name', 'id' => 'date-filter']) }}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <section class="report-table">
                        @include('reports.includes.quarterly_service_charge_table', ['properties' => $properties, 'order' => $order, 'quarter_total' => $quarter_total, 'last_year_quarter_total' => $quarter_total])
                    </section>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

    </div>
    <!-- /Right (content) side -->
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.visualize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/moved/plugins/jquery.jgrowl.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#date-filter').on('change', function () {
                var year = this.value;

                var link = '/reports/quarterservicechargebudget';

                link = link + '?date=' + year;

                $('#csv-link').attr('href', link);
            });
        });
    </script>
@endpush
