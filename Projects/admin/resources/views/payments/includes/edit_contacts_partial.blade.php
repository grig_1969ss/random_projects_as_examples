<select name="contact_id" class="input-medium" id="contact_id" {{ isset($disable) && $disable ? 'disabled' : '' }}>
    <option value="">Select...</option>
    <option value="" class="add-new">Add new contact</option>
    @foreach($contacts as $contact)
        <option value="{{ $contact->id }}" {{ $contact->id == $contactId ? 'selected' : '' }}>
            {{ $contact->present()->nameEmail }}
        </option>
    @endforeach
</select>