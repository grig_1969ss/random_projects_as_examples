<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-md-5 col-lg-4 order-md-1">
            <div class="mb-5 text-center">
                <h6 class="h3">{{ $title }}</h6>
                <div class="dropdown-divider"></div>
            </div>
            <form method="POST" action="{{ $route }}" class="form-horizontal payment-form" id="add-payment">
                {{ csrf_field() }}

                @if($payment->id)
                    {{ method_field('PUT') }}
                @endif

                <input type="hidden" id="modal_save" name="modal_save" value="">

                <div id='error-message'></div>

                <div id="period" class="control-group">
                    <label for="period" class="control-label">Period</label>
                    <div class="form-group controls">
                        <input type="text" name="period" id="period" class="form-control"
                               value="{{ $payment->period }}">
                    </div>
                </div>
                <div id="description" class="control-group">
                    <label for="description" class="control-label">Description</label>
                    <div class="form-group controls">
                                <textarea name="description" id="description"
                                          class="form-control">{{ $payment->description }}</textarea>
                        <span id="description" class="help-block"></span>
                    </div>
                </div>
                <div id="supplier_id" class="control-group">
                    <label for="supplier_id" class="control-label">Supplier</label>
                    <div class="form-group controls">
                        <select name="supplier_id" class="form-control" id="supplier_id">
                            <option value="">Select...</option>
                            @foreach($suppliers as $supplier)
                                <option value="{{ $supplier->id }}"
                                        data-url="{{ route('ajax.suppliers.contacts.select', $supplier) }}"
                                        data-new-contact-url="{{ route('ajax.suppliers.contacts.create', $supplier) }}"
                                    {{ $supplier->id == $payment->supplier_id ? 'selected' : '' }}>
                                    {{ $supplier->name }}
                                </option>
                            @endforeach
                        </select>
                        <span id="supplier_id" class="help-block"></span>
                    </div>
                </div>
                <div id="contact_id" class="control-group">
                    <label for="contact_id" class="control-label">Supplier Contact</label>
                    <div class="form-group controls">
                        @include('partials.edit_contacts_partial', [
                            'disable' => ($payment->supplier_id ? false : true),
                            'elementName' => 'contact_id'
                        ])
                        <span id="contact_id" class="help-block"></span>
                    </div>

                    <input type="hidden"
                           class="new-contact modal-ajax form-control"
                           data-url="{{ $payment->supplier ? route('ajax.suppliers.contacts.create', $payment->supplier) : '' }}">
                </div>
                <div id="payment_type_id" class="control-group">
                    <label for="payment_type_id" class="control-label">Type</label>
                    <div class="form-group controls">
                        <select name="payment_type_id" class="form-control" id="payment_type_id">
                            <option value="">Select...</option>
                            @foreach($paymentTypes as $paymentType)
                                <option value="{{ $paymentType->id }}"
                                    {{ $payment->payment_type_id == $paymentType->id ? 'selected' : '' }}>
                                    {{ $paymentType->name }}
                                </option>
                            @endforeach
                        </select>
                        <span id="payment_type_id" class="help-block"></span>
                    </div>
                </div>
                <div id="payment_date" class="control-group">
                    {{ Form::label('payment_date', 'Invoice Date', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        <div class="input-append">
                            {{ Form::text('payment_date', $payment->present()->paymentDate(''), array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                            <span class="add-on"><i class="awe-calendar"></i></span>
                        </div>
                        <span id="payment_date" class="help-block"></span>
                    </div>
                </div>
                <div id="gross_invoice_amount" class="control-group">
                    {{ Form::label('gross_invoice_amount', 'Gross Invoice Amount', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        <div class="input-prepend">
                            <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                            {{ Form::text('gross_invoice_amount', $payment->gross_invoice_amount, array('class' => 'form-control')) }}
                        </div>
                        <span id="gross_invoice_amount" class="help-block"></span>
                    </div>
                </div>
                <div id="tax_amount" class="control-group">
                    {{ Form::label('tax_amount', 'Tax Amount', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        <div class="input-prepend">
                            <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                            {{ Form::text('tax_amount', $payment->tax_amount, array('class' => 'form-control')) }}
                        </div>
                        <span id="tax_amount" class="help-block"></span>
                    </div>
                </div>
                <div id="date_approved" class="control-group">
                    <label for="date_approved" class="control-label">Approved Date</label>
                    <div class="form-group controls">
                        <div class="input-append">
                            <input type="text"
                                   name="date_approved"
                                   id="date_approved"
                                   class="datepicker form-control"
                                   data-date-format="dd/mm/yyyy"
                                   value="{{ $payment->present()->dateApproved('') }}"
                                   autocomplete="off">
                            <span class="add-on"><i class="awe-calendar"></i></span>
                        </div>
                        <span id="date_approved" class="help-block"></span>
                    </div>
                </div>
                <div id="hold" class="control-group">
                    <label for="hold" class="control-label">Hold?</label>
                    <div class="controls">
                        <input type="hidden" name="hold" value="0">
                        <input type="checkbox" name="hold" id="hold"
                               value="1" {{ $payment->hold ? 'checked' : '' }}>
                        <span id="hold" class="help-block"></span>
                    </div>
                </div>
                <div id="note" class="control-group">
                    <label for="note" class="control-label">Notes</label>
                    <div class="form-group controls">
                        <textarea name="note" id="note" class="form-control">{{ $payment->note }}</textarea>
                        <span id="note" class="help-block"></span>
                    </div>
                </div>

                <div id="payment_ref" class="control-group">
                    {{ Form::label('payment_ref', 'Invoice Ref', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        {{ Form::text('payment_ref', $payment->payment_ref, array('class' => 'form-control')) }}
                        <span id="payment_ref" class="help-block"></span>
                    </div>
                </div>

                <div id="diary_date" class="control-group">
                    <label for="diary_date" class="control-label">Diary Date</label>
                    <div class="form-group controls">
                        <div class="input-append">
                            <input type="text"
                                   name="diary_date"
                                   id="diary_date"
                                   class="datepicker form-control"
                                   data-date-format="dd/mm/yyyy"
                                   value="{{ $payment->present()->diaryDate('') }}" autocomplete="off">
                            <span class="add-on"><i class="awe-calendar"></i></span>
                        </div>
                        <span id="diary_date" class="help-block"></span>
                    </div>
                </div>

                <div class="form-actions">
                    <button id="save-rent-review" class="btn mr-2 mb-2 btn-outline-primary disable-spinner" type="submit">
                        Save Changes
                        <span class="spinner-border spinner-border-sm sr-only" role="status"
                              aria-hidden="true"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

