@extends('layouts_new.app', ['pageTitle' => 'Approvals', 'pageNav' => 'payment', 'page' => 'PaymentsIndex'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
            <div class="row ml-0 mr-0">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Approvals</h5>
                    </div>

                    <div class="form-group col-4">
                        <input name="searchBy" type="text" placeholder="Search..." class="form-control"
                               id="find-property">
                    </div>

                </div>
            </div>
            <!-- End of Title -->
            <div id="properties-list-replace">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-5">
                                <table id="example-2" class="datatable table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                    <tr>
                                        <th scope="col">Invoice Date</th>
                                        <th scope="col">Gross Invoice Amount</th>
                                        <th scope="col">Reference</th>
                                        <th scope="col">Client</th>
                                        <th scope="col">Property</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($payments as $payment)
                                        <?php $propertyLink = '/properties/'.$payment->id ?>
                                        <tr class="odd gradeX">
                                            @if ($read_only == 0)
                                                <td>{{ Html::link($propertyLink, Date::dmY($payment->payment_date)) }}</td>
                                                <td>{{ Html::link($propertyLink, \App\Helpers\Currency::gbp($payment->gross_invoice_amount)) }}</td>
                                                <td>{{ Html::link($propertyLink, $payment->payment_ref) }}</td>
                                                <td>{{ Html::link($propertyLink, $payment->client_name) }}</td>
                                                <td>{{ Html::link($propertyLink, $payment->property_name) }}</td>
                                            @else
                                                <td>{{ Date::dmY($payment->payment_date) }}</td>
                                                <td>{{ \App\Helpers\Currency::gbp($payment->gross_invoice_amount) }}</td>
                                                <td>{{ $payment->payment_ref }}</td>
                                                <td>{{ $payment->client_name }}</td>
                                                <td>{{ $payment->property_name }}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- /Right (content) side -->

@endsection

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
