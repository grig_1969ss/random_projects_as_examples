@extends('layouts_new.app', [
    'pageTitle' => ($payment->id ? 'Edit' : 'Add').' Approval',
    'pageNav' => 'property',
    'page' => 'PropertyPayment'
])

@section('container')

    @include('payments.includes.form')

    @include('partials.base_modal')

    @push('js')
        <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    @endpush
@endsection
