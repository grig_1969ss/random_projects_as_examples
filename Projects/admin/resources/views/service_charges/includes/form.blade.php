@php
    $year_end = ($serviceCharge->year_end) ? Date::dmY($serviceCharge->year_end) : null;
    $service_charge_commencement_date = ($serviceCharge->service_charge_commencement_date) ? Date::dmY($serviceCharge->service_charge_commencement_date) : null;
    $date_invoiced = ($serviceCharge->date_invoiced) ? Date::dmY($serviceCharge->date_invoiced) : null;
    $date_received = ($serviceCharge->date_received) ? Date::dmY($serviceCharge->date_received) : null;
    $diary_date = ($serviceCharge->diary && $serviceCharge->diary->date) ? Date::dmY($serviceCharge->diary->date) : null;
@endphp

<div class="section section-sm">
    <div id="template-page" class="container-fluid">
        <div class="col-12 mb-4 d-flex justify-content-between">
            <div class="data-container">
                <header>
                    <h2>{{ $title }}</h2>
                </header>
                <div class="span8">
                    {{ Form::open([
                        'route' => ($serviceCharge->id ? ['service-charges.update', $serviceCharge] : ['properties.service-charges.store', $property]),
                        'method' => $restMethod,
                        'class' => 'form-horizontal',
                        'id' => 'create-service-charge'
                    ]) }}

                    <input type="hidden" id="modal_save" name="modal_save" value="">

                    <fieldset>
                        <div id='error-message'></div>
                        <div id="service_charge_status_id" class="control-group">
                            {{ Form::label('service_charge_status_id', 'Service Charge Status', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('service_charge_status_id', $serviceChargeStatusesDropdown, $serviceCharge->service_charge_status_id, ['class' => 'form-control add-new-option']) }}
                                <span id="service_charge_status_id" class="help-block"></span>
                            </div>
                        </div>

                        <div id="year_end" class="control-group">
                            {{ Form::label('year_end', 'Year End', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                <div class="input-append">
                                    {{ Form::text('year_end',$year_end, ['class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off']) }}
                                    <span class="add-on"><i class="awe-calendar"></i></span>
                                    <span id="year_end" class="help-block"></span>
                                </div>
                            </div>
                        </div>

                        <div id="apportionment_type_id" class="control-group">
                            {{ Form::label('apportionment_type_id', 'Method of Apportionment', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('apportionment_type_id', $apportionmentTypesDropdown, $serviceCharge->apportionment_type_id, ['class' => 'form-control add-new-option']) }}
                                <span id="apportionment_type_id" class="help-block"></span>
                            </div>
                        </div>
                        <div id="percentage_apportion" class="control-group">
                            {{ Form::label('percentage_apportion', 'Apportionment', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::textarea('percentage_apportion',$serviceCharge->percentage_apportion, array('class' => 'form-control', 'rows' => '3'))  }}
                                <span id="percentage_apportion" class="help-block"></span>
                            </div>
                        </div>
                        <div id="annual_budget" class="control-group">
                            {{ Form::label('annual_budget', 'Annual Budget', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-group input-group-border">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                    </div>
                                    {{ Form::text('annual_budget',$serviceCharge->annual_budget, array('class' => 'form-control')) }}
                                </div>
                                <span id="annual_budget" class="help-block"></span>
                            </div>
                        </div>

                        <div id="budget_quarterly_payment" class="control-group" data-source="budget_year_end">
                            {{ Form::label('budget_quarterly_payment', 'Quarterly Payment (budget)', array('class' => 'control-label')) }}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group focused">
                                        <div class="input-group input-group-border">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                            </div>
                                            {{ Form::text('budget_quarterly_payment',$serviceCharge->budget_quarterly_payment, array('class' => 'form-control')) }}
                                        </div>
                                        <span id="budget_quarterly_payment" class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group focused">
                                        <div class="input-group input-group-border">
                                            <select name="budget_quarterly_payment_months" id="budget_quarterly_payment_months"
                                            class="form-control">
                                                <option @if($serviceCharge->budget_quarterly_payment_months == 3) selected
                                                        @endif value="3">Quarterly
                                                </option>
                                                <option @if($serviceCharge->budget_quarterly_payment_months == 1) selected
                                                        @endif  value="1">Monthly
                                                </option>
                                                <option @if($serviceCharge->budget_quarterly_payment_months == 6) selected
                                                        @endif  value="6">Bi-Annually
                                                </option>
                                                <option @if($serviceCharge->budget_quarterly_payment_months == 12) selected
                                                        @endif  value="12">Annually
                                                </option>
                                            </select>
                                        </div>
                                        <span id="budget_quarterly_payment_months" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="balance_service_charge" class="control-group">
                            {{ Form::label('balance_service_charge', 'Service Charge Reconciliation', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-group input-group-border">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                    </div>
                                    {{ Form::text('balance_service_charge',$serviceCharge->balance_service_charge, array('class' => 'form-control')) }}
                                </div>
                                <span id="balance_service_charge" class="help-block"></span>
                            </div>
                        </div>

                        <div id="actual_year_end" class="control-group">
                            {{ Form::label('actual_year_end', 'Total Cost', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-group input-group-border">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                    </div>
                                    {{ Form::text('actual_year_end',$serviceCharge->actual_year_end, array('class' => 'form-control')) }}
                                </div>
                                <span id="actual_year_end" class="help-block"></span>
                            </div>
                        </div>
                        <div id="budget_details_recd" class="control-group">
                            {{ Form::label('budget_details_recd', 'Budget Details Recd', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('budget_details_recd', ['0'=> 'No', '1'=> 'Yes'], $serviceCharge->budget_details_recd, ['class' => 'form-control']) }}
                                <span id="budget_details_recd" class="help-block"></span>
                            </div>
                        </div>
                        <div id="franchisee_updated" class="control-group">
                            {{ Form::label('franchisee_updated', 'Franchisee Updated', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('franchisee_updated', ['0'=> 'No', '1'=> 'Yes'], $serviceCharge->franchisee_updated, ['class' => 'form-control']) }}
                                <span id="franchisee_updated" class="help-block"></span>
                            </div>
                        </div>
                        <div id="real_estate_updated" class="control-group">
                            {{ Form::label('real_estate_updated', 'Real Estate Updated', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('real_estate_updated', ['0'=> 'No', '1'=> 'Yes'], $serviceCharge->real_estate_updated, ['class' => 'form-control']) }}
                                <span id="real_estate_updated" class="help-block"></span>
                            </div>
                        </div>
                        <div id="annual_reconciliation_recd" class="control-group">
                            {{ Form::label('annual_reconciliation_recd', 'Annual Reconciliation Recd', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('annual_reconciliation_recd', ['0'=> 'No', '1'=> 'Yes'], $serviceCharge->annual_reconciliation_recd, ['class' => 'form-control']) }}
                                <span id="annual_reconciliation_recd" class="help-block"></span>
                            </div>
                        </div>
                        <div id="annual_franchisee_updated" class="control-group">
                            {{ Form::label('annual_franchisee_updated', 'Franchisee Updated', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('annual_franchisee_updated', ['0'=> 'No', '1'=> 'Yes'], $serviceCharge->annual_franchisee_updated, ['class' => 'form-control']) }}
                                <span id="annual_franchisee_updated" class="help-block"></span>
                            </div>
                        </div>
                        <div id="annual_real_estate_updated" class="control-group">
                            {{ Form::label('annual_real_estate_updated', 'Real Estate Updated', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                {{ Form::select('annual_real_estate_updated', ['0'=> 'No', '1'=> 'Yes'], $serviceCharge->annual_real_estate_updated, ['class' => 'form-control']) }}
                                <span id="annual_real_estate_updated" class="help-block"></span>
                            </div>
                        </div>


                        <div id="exclusions" class="control-group">
                            {{ Form::label('exclusions', 'Exclusions', ['class' => 'control-label']) }}
                            <div class="controls">
                                {{ Form::checkbox('exclusions', '1', $serviceCharge->exclusions) }}
                                <span id="exclusions" class="help-block"></span>
                            </div>
                        </div>
                        <div id="year_end_cost_per_sq_ft" class="control-group">
                            {{ Form::label('year_end_cost_per_sq_ft', 'Year Cost Per Square Feet', ['class' => 'control-label']) }}
                            <div class="form-group controls">
                                <div class="input-group input-group-border">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                    </div>
                                    {{ Form::text('year_end_cost_per_sq_ft',$serviceCharge->year_end_cost_per_sq_ft, ['class' => 'form-control']) }}
                                </div>
                                <span id="year_end_cost_per_sq_ft" class="help-block"></span>
                            </div>
                        </div>

                        <div id="invoiced" class="control-group">
                            {{ Form::label('invoiced', 'Invoiced', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::checkbox('invoiced', '1', $serviceCharge->invoiced) }}
                                <span id="invoiced" class="help-block"></span>
                            </div>
                        </div>

                        <div id="budget_year_end" class="control-group">
                            {{ Form::label('budget_year_end', 'Budget Year End', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('budget_year_end',$serviceCharge->budget_year_end, array('class' => 'form-control')) }}
                                <span id="budget_year_end" class="help-block"></span>
                            </div>
                        </div>

                        <div id="wip_amount" class="control-group">
                            {{ Form::label('wip_amount', 'WIP Amount', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('wip_amount',$serviceCharge->wip_amount, array('class' => 'form-control')) }}
                                <span id="wip_amount" class="help-block"></span>
                            </div>
                        </div>


                        <div id="service_charge_budget_differential" class="control-group">
                            {{ Form::label('service_charge_budget_differential', 'Service Charge Budget Differential', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-group input-group-border">
                                    {{ Form::text('service_charge_budget_differential',$serviceCharge->service_charge_budget_differential, array('class' => 'form-control')) }}
                                    <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                </div>
                                <span id="service_charge_budget_differential" class="help-block"></span>
                            </div>
                        </div>
                        <div id="service_charge_commencement_date" class="control-group">
                            {{ Form::label('service_charge_commencement_date', 'Service Charge Commencement Date', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-append">
                                    {{ Form::text('service_charge_commencement_date',$service_charge_commencement_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                    <span class="add-on"><i class="awe-calendar"></i></span>
                                    <span id="service_charge_commencement_date" class="help-block"></span>
                                </div>
                            </div>
                        </div>

                        <div class="sr-only">
                            <div id="agent_contributions" class="control-group">
                                {{ Form::label('agent_contributions', 'Managing Agent Contributions', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                        </div>
                                        {{ Form::text('agent_contributions',$serviceCharge->agent_contributions, array('class' => 'form-control')) }}
                                    </div>
                                    <span id="agent_contributions" class="help-block"></span>
                                </div>
                            </div>

                            <div id="potential_savings_against_budget" class="control-group">
                                {{ Form::label('potential_savings_against_budget', 'Potential Savings Against Budget', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                        </div>
                                        {{ Form::text('potential_savings_against_budget',$serviceCharge->potential_savings_against_budget, array('class' => 'form-control')) }}
                                    </div>
                                    <span id="potential_savings_against_budget" class="help-block"></span>
                                </div>
                            </div>
                            <div id="amount_invoiced" class="control-group">
                                {{ Form::label('amount_invoiced', 'Amount Invoiced', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                        </div>
                                        {{ Form::text('amount_invoiced',$serviceCharge->amount_invoiced, array('class' => 'form-control')) }}
                                    </div>
                                    <span id="amount_invoiced" class="help-block"></span>
                                </div>
                            </div>
                            <div id="date_invoiced" class="control-group">
                                {{ Form::label('date_invoiced', 'Date Invoiced', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ Form::text('date_invoiced',$date_invoiced, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="date_invoiced" class="help-block"></span>
                                    </div>
                                </div>
                            </div>

                            <div id="vat" class="control-group">
                                {{ Form::label('vat', 'VAT', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('vat', '1', $serviceCharge->vat) }}
                                    <span id="vat" class="help-block"></span>
                                </div>
                            </div>

                            <div id="amount_received" class="control-group">
                                {{ Form::label('amount_received', 'Amount received', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                        </div>
                                        {{ Form::text('amount_received',$serviceCharge->amount_received, array('class' => 'form-control')) }}
                                    </div>
                                    <span id="amount_received" class="help-block"></span>
                                </div>
                            </div>
                            <div id="date_received" class="control-group">
                                {{ Form::label('date_received', 'Date received', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ Form::text('date_received',$date_received, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="date_received" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="confirmed" class="control-group">
                                {{ Form::label('confirmed', 'Confirmed', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('confirmed', '1', $serviceCharge->confirmed) }}
                                    <span id="confirmed" class="help-block"></span>
                                </div>
                            </div>
                            <div id="potential_budget" class="control-group">
                                {{ Form::label('potential_budget', 'Potential Budget', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                        </div>
                                        {{ Form::text('potential_budget',$serviceCharge->potential_budget, array('class' => 'form-control')) }}
                                    </div>
                                    <span id="potential_budget" class="help-block"></span>
                                </div>
                            </div>

                            <div id="current_contribution" class="control-group">
                                {{ Form::label('current_contribution', 'Current Contribution', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-group input-group-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{!! $property->present()->currencyHtmlCode !!}</span>
                                        </div>
                                        {{ Form::text('current_contribution',$serviceCharge->current_contribution, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                    </div>
                                    <span id="current_contribution" class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div id="current_turnover" style='display:none' class="control-group">
                            {{ Form::label('current_turnover', 'Current Turnover', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $property->present()->currencyHtmlCode !!}</span>
                                    {{ Form::text('current_turnover',$serviceCharge->current_turnover, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                </div>
                                <span id="current_turnover" class="help-block"></span>
                            </div>
                        </div>

                        <div id="charge_details" class="control-group">
                            {{ Form::label('charge_details', 'Charge Details', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <textarea id="charge_details" class="form-control" name="charge_details"
                                              rows="3">{{$serviceCharge->charge_details ? $serviceCharge->charge_details : ''}}</textarea>
                                </div>
                                <span id="charge_details" class="help-block"></span>
                            </div>
                        </div>
                        <div id="wip" class="control-group">
                            {{ Form::label('wip', 'WIP', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <textarea id="wip" class="form-control" name="wip"
                                              rows="3">{{$serviceCharge->wip ? $serviceCharge->wip : ''}}</textarea>
                                </div>
                                <span id="wip" class="help-block"></span>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="notes">Notes</label>
                            <div class="form-group controls">
                                <textarea id="notes" class="form-control" name="notes"
                                          rows="3">{{$serviceCharge->notes ? $serviceCharge->notes : ''}}</textarea>
                                <span id="notes" class="help-block"></span>
                            </div>
                        </div>

                        <div class="sr-only ">
                            @if(isset($serviceCharge->diary->id))
                                <div id="diary_date" class="control-group">
                                    {{ Form::label('diary_date', 'Diary Date', array('class' => 'control-label')) }}
                                    <div class="form-group controls">
                                        <div class="input-append">
                                            {{ Form::text('diary_date',$diary_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                            <span class="add-on"><i class="awe-calendar"></i></span>
                                            <span id="diary_date" class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::hidden('diary_id', $serviceCharge->diary->id) }}
                            @else
                                <div id="diary_date" class="control-group">
                                    {{ Form::label('diary_date', 'Diary Date', array('class' => 'control-label')) }}
                                    <div class="form-group controls">
                                        <div class="input-append">
                                            {{ Form::text('diary_date', null, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                            <span class="add-on"><i class="awe-calendar"></i></span>
                                            <span id="diary_date" class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <!-- submit button -->
                        <div class="form-actions">
                            <button id="save-service-charge" class="btn btn-large btn-primary disable-spinner"
                                    type="submit">
                                Save Changes
                                <span class="spinner-border spinner-border-sm sr-only" role="status"
                                      aria-hidden="true"></span>
                            </button>
                        </div>
                    </fieldset>
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Right (content) side -->

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
