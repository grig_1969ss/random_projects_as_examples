@extends('layouts_new.app', [
    'pageTitle' => ($serviceCharge->id ? 'Edit' : 'Add').' Service Charge',
    'pageNav'   => 'property',
    'page'      => 'PropertyServiceCharge'
])

@section('container')
    @include('service_charges.includes.form', [
        'title'      => ($serviceCharge->id ? 'Edit' : 'Add').' Service Charge',
        'restMethod' => ($serviceCharge->id ? 'PUT' : 'POST')
    ])
@endsection