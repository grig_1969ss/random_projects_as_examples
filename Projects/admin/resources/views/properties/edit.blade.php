
@extends('layouts_new.app', [
    'pageTitle' => $property->id ? 'Edit Property' : 'Add Property',
    'pageNav'   => 'property',
    'page'      => 'PropertyNew'
])

@section('container')
    @include('properties.includes.form', ['title' => 'New Property'])
@endsection
