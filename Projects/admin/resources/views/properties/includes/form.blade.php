@push('css')
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
@endpush
<!-- Right (content) side -->
<div class="container-fluid" role="main">
    <!-- Grid row -->
    <div class="row">
        <div id='error-message'></div>
        <form method="POST"
              action="{{ $property->id ? route('properties.update', $property) : route('properties.store') }}"
              class="form-horizontal form-clear"
              id="create-property">
            @if($property->id)
                {{ method_field('PUT') }}
            @endif

            @csrf
            <div class="col-12 col-md-6 col-lg-12 order-md-1">
                <div class="data-container">
                    <div class="mb-5 text-center">
                        <h2 class="h2">Property Details</h2>
                    </div>
                    <div class="">
                        <div id="property_name" class="control-group">
                            <label for="property_name" class="control-label">Property Name</label>
                            <div class="form-group controls">
                                <input type="text" name="property_name"
                                       class="form-control  @error('property_name') invalid-input @enderror"
                                       value="{{ $property->property_name }}">
                                <span id="property_name" class="help-block"></span>
                            </div>
                        </div>

                        @if(isset($objectId))

                            @if($objectName == 'agent')
                                <?php
                                $agentSelector = $objectId;
                                $clientSelector = $property->client_id;
                                $landlordSelector = $property->landlord_id;
                                ?>
                                <input type="hidden" name="agent_id" value="{{ $objectId }}">
                            @elseif($objectName == 'client')
                                <?php
                                $agentSelector = $property->agent_id;
                                $landlordSelector = $property->landlord_id;
                                $clientSelector = $objectId;
                                ?>
                                <input type="hidden" name="client_id" value="{{ $objectId }}">
                            @else
                                <?php
                                $landlordSelector = $objectId;
                                $clientSelector = $property->client_id;
                                $agentSelector = $property->agent_id;
                                ?>
                                <input type="hidden" name="landlord_id" value="{{ $objectId }}">
                            @endif
                        @else
                            <?php
                            $agentSelector = $property->agent_id;
                            $clientSelector = $property->client_id;
                            $landlordSelector = $property->landlord_id;

                            $sClient = $clientId;

                            if (! isset($property->client_id)) {
                                $clientSelector = (isset($sClient)) ? $clientId : $property->client_id;
                            }

                            if (! isset($property->agent_id)) {
                                $agentSelector = 34;
                            }

                            // add new client or agent option
                            ?>
                            <div id="agent_name" class="control-group">
                                <label for="agent_id" class="control-label">Managing Agent Name</label>
                                <div class="form-group controls">
                                    <select class="form-control select2" name="agent_id" id="agent_id">
                                        @foreach($agentsDropdown as $key => $value)
                                            @if($key)
                                                <option

                                                    value="{{ $key }}"
                                                    data-url="{{ route('ajax.agents.contacts.select', $key) }}"
                                                    data-new-contact-url="{{ route('ajax.agents.contacts.create', $key) }}"
                                                    {{ $key == $property->agent_id ? 'selected' : '' }}>
                                                    {{ $value }}
                                                </option>
                                            @else
                                                <option
                                                    value="{{ $key }}"
                                                    {{ $key == $property->agent_id ? 'selected' : '' }}>
                                                    {{ $value }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>

                                    <button id="add-agent-propertyy"
                                            class="btn btn-sm mt-2 mr-2 mb-2 btn-pill btn-outline-gray"
                                            data-toggle="modal" data-target="#addAgentModal" type="button">Add Managing
                                        Agent
                                    </button>
                                    <span id="agent_name" class="help-block"></span>
                                </div>
                            </div>

                            <div id="agent_contact_id" class="control-group">
                                <label for="agent_contact_id" class="control-label">Managing Agent Contact</label>
                                <div class="form-group controls">
                                    @include('partials.edit_contacts_partial', [
                                        'disable' => ($property->agent_id ? false : true),
                                        'elementName' => 'agent_contact_id'
                                    ])
                                    <span id="agent_contact_id" class="help-block"></span>
                                </div>

                                <input type="hidden"
                                       class="new-contact modal-ajax"
                                       data-url="{{ $property->agent ? route('ajax.agents.contacts.create', $property->agent) : '' }}">
                            </div>

                            <div id="landlord_id" class="control-group">
                                <label for="landlord_id" class="control-label">Landlord Name</label>
                                <div class="form-group controls">
                                    <select class="form-control select2" name="landlord_id" id="landlord_id">
                                        @foreach($landlordsDropdown as $key => $value)
                                            <option
                                                value="{{ $key }}" {{ $key == $landlordSelector ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>

                                    <button data-toggle="modal"
                                            data-target="#addLandlordModal"
                                            class="btn btn-sm mt-2 mr-2 mb-2 btn-pill btn-outline-gray" type="button">
                                        Add Landlord
                                    </button>
                                    <span id="landlord_name" class="help-block"></span>
                                </div>
                            </div>
                            @if(!$clientId)
                                <div id="client_id" class="control-group">
                                    <label for="client_id" class="control-label">Client Name</label>
                                    <div class="form-group controls">
                                        <select class="form-control" name="client_id" id="client_id">
                                            @foreach($clientsDropdown as $key => $value)
                                                <option
                                                    value="{{ $key }}" {{ $key == $clientSelector ? 'selected' : '' }}>{{ $value }}</option>
                                            @endforeach
                                        </select>

                                        <button id="add-client-property"
                                                class="btn btn-sm mt-2 mr-2 mb-2 btn-pill btn-outline-gray"
                                                type="button">Add Client
                                        </button>
                                        <span id="client_id" class="help-block"></span>
                                    </div>
                                </div>
                            @else
                                <input type="hidden" name="client_id" value="{{ $clientId }}">
                            @endif
                        @endif

                        <div id="property_status_id" class="control-group">
                            <label for="property_status_id" class="control-label">Property Status</label>
                            <div class="form-group controls">
                                <select class="form-control add-new-option" name="property_status_id"
                                        id="property_status_id">
                                    @foreach($propertyStatusesArray as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ $key == $property->property_status_id ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <span id="property_status_id" class="help-block"></span>
                            </div>
                        </div>

                        <div id="property_type_id" class="control-group">
                            <label for="property_type_id" class="control-label">Property Type</label>
                            <div class="form-group controls">
                                <select name="property_type_id" id="property_type_id"
                                        class="form-control select2 add-new-option">
                                    @foreach($propertyTypesDropdown as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ $key == $property->property_type_id ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <span id="property_type_id" class="help-block"></span>
                            </div>
                        </div>
                        <div id="address_1" class="control-group">
                            <label for="address_1" class="control-label">Address Line 1</label>
                            <div class="form-group controls">
                                <input type="text" name="address_1" class="form-control"
                                       value="{{ $address->address_1 }}" id="address_1">
                                <span id="address_1" class="help-block"></span>
                            </div>
                        </div>
                        <div id="address_2" class="control-group">
                            <label for="address_2" class="control-label">Address Line 2</label>
                            <div class="form-group controls">
                                <input type="text" name="address_2" class="form-control"
                                       value="{{ $address->address_2 }}" id="address_2">
                                <span id="address_2" class="help-block"></span>
                            </div>
                        </div>

                        <div id="" class="control-group">
                            <label for="capa_owner_id_select" class="control-label">CAPA Main Contact</label>
                            <div class="form-group controls">
                                <select class="form-control select2" name="capa_owner_id" id="capa_owner_id_select">
                                    <option selected disabled value="">Select...</option>
                                    @foreach($users as $user)
                                        <option @if($user->id == $property->capa_owner_id) selected
                                                @endif value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                <span id="capa_owner_id" class="help-block"></span>
                            </div>
                        </div>

                        <div id="" class="control-group">
                            <label for="client_owner_id_select" class="control-label">Client Main Contact</label>
                            <div class="form-group controls">
                                <select class="form-control select2" name="client_owner_id" id="client_owner_id_select">
                                    <option selected disabled value="">Select...</option>
                                    @foreach($users as $user)
                                        <option @if($user->id == $property->client_owner_id) selected
                                                @endif value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                <span id="client_owner_id" class="help-block"></span>
                            </div>
                        </div>

                        <div id="town" class="control-group">
                            <label for="town" class="control-label">Town</label>
                            <div class="form-group controls">
                                <input type="text" name="town" class="form-control" value="{{ $address->town }}"
                                       id="town">
                                <span id="town" class="help-block"></span>
                            </div>
                        </div>
                        <div id="county" class="control-group">
                            <label for="county" class="control-label">County</label>
                            <div class="form-group controls">
                                <input type="text" name="county" class="form-control" value="{{ $address->county }}"
                                       id="county">
                                <span id="county" class="help-block"></span>
                            </div>
                        </div>
                        <div id="post_code" class="control-group">
                            <label for="post_code" class="control-label">Post Code</label>
                            <div class="form-group controls">
                                <input type="text" name="post_code" class="form-control"
                                       value="{{ $address->post_code }}" id="post_code">
                                <span id="post_code" class="help-block"></span>
                            </div>
                        </div>
                        <div id="store_contact" class="control-group">
                            {{ Form::label('store_contact', 'Store Contact', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('store_contact',$property->store_contact, array('class' => 'form-control')) }}
                                <span id="store_contact" class="help-block"></span>
                            </div>
                        </div>
                        <div id="store_telephone" class="control-group">
                            {{ Form::label('store_telephone', 'Store Telephone', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('store_telephone',$property->store_telephone, array('class' => 'form-control')) }}
                                <span id="store_telephone" class="help-block"></span>
                            </div>
                        </div>

                        <div id="region_id" class="control-group">
                            {{ Form::label('region_id', 'Region', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::select('region_id', $regionsDropdown, $property->region_id, array('class' => 'form-control select2 add-new-option')) }}
                                <span id="region_id" class="help-block"></span>
                            </div>
                        </div>

                        <div id="reference" class="control-group">
                            {{ Form::label('reference', 'Reference', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('reference',$property->reference, array('class' => 'form-control')) }}
                                <span id="reference" class="help-block"></span>
                            </div>
                        </div>
                        <div id="floor_area_net_trading" class="control-group">
                            {{ Form::label('floor_area_net_trading', 'Floor Area Net Trading', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-append">
                                    {{ Form::text('floor_area_net_trading',$property->floor_area_net_trading, array('class' => 'form-control', 'id' => 'appendedInput')) }}
                                    <span class="add-on">SqFt</span>
                                </div>
                                <span id="floor_area_net_trading" class="help-block"></span>
                            </div>
                        </div>
                        <div id="tenure_type_id" class="control-group">
                            {{ Form::label('tenure_type_id', 'Tenure Type', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::select('tenure_type_id', $tenureTypesDropdown, $property->tenure_type_id, array('class' => 'form-control select2 add-new-option')) }}
                                <span id="tenure_type_id" class="help-block"></span>
                            </div>
                        </div>
                        <div id="purchase_date" class="control-group">
                            {{ Form::label('purchase_date', 'Completion Date', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-append">
                                    {{ $purchase_date = ($property->purchase_date) ? Date::dmY($property->purchase_date) : null }}
                                    {{ Form::text('purchase_date',$purchase_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                    <span class="add-on"><i class="awe-calendar"></i></span>
                                    <span id="purchase_date" class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div id="commencement_date" class="control-group">
                            {{ Form::label('commencement_date', 'Rent Commencement Date', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-append">
                                    {{ $commencement_date = ($property->commencement_date) ? Date::dmY($property->commencement_date) : null }}
                                    {{ Form::text('commencement_date',$commencement_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                    <span class="add-on"><i class="awe-calendar"></i></span>
                                    <span id="commencement_date" class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div id="purchase_price" class="control-group">
                            {{ Form::label('purchase_price', 'Purchase Price', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $currencyCode !!}</span>
                                    {{ Form::text('purchase_price',$property->purchase_price, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                </div>
                                <span id="purchase_price" class="help-block"></span>
                            </div>
                        </div>
                        <div id="book_value" class="control-group">
                            {{ Form::label('book_value', 'Book Value', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $currencyCode !!}</span>
                                    {{ Form::text('book_value',$property->book_value, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                </div>
                                <span id="book_value" class="help-block"></span>
                            </div>
                        </div>
                        <div id="freehold_value" class="control-group">
                            {{ Form::label('freehold_value', 'Freehold Value', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $currencyCode !!}</span>
                                    {{ Form::text('freehold_value',$property->freehold_value, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                </div>
                                <span id="freehold_value" class="help-block"></span>
                            </div>
                        </div>
                        <div id="repair_liability" class="control-group">
                            {{ Form::label('repair_liability', 'Repair Liability', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                <div class="input-prepend">
                                    <span class="add-on">{!! $currencyCode !!}</span>
                                    {{ Form::text('repair_liability',$property->repair_liability, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                </div>
                                <span id="repair_liability" class="help-block"></span>
                            </div>
                        </div>
                        <div id="auto_service_charge" class="control-group">
                            {{ Form::label('auto_service_charge', 'Auto Service Charge', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::checkbox('auto_service_charge', '1', $property->auto_service_charge)}}
                                <span id="auto_service_charge" class="help-block"></span>
                            </div>
                        </div>
                        <div id="general_info" class="control-group">
                            {{ Form::label('general_info', 'General Info', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::textarea('general_info', $property->general_info, array('cols' => 50, 'rows' => 3, 'class' => 'form-control')) }}
                                <span id="general_info" class="help-block"></span>
                            </div>
                        </div>
                        <div id="inside_landlord" class="control-group">
                            {{ Form::label('inside_landlord', 'Inside Landlord & Tenant Act', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::select('inside_landlord',array( '1' => 'TBC', 2 => 'NO', 3 => 'YES'), $property->inside_landlord ? $property->inside_landlord : null, array('class'=>'form-control'))}}
                                <span id="inside_landlord" class="help-block"></span>
                            </div>
                        </div>
                        <div id="owner_type_id" class="control-group">
                            <label for="owner_type_id" class="control-label">Owner Type</label>
                            <div class="form-group controls">
                                <select class="form-control" name="owner_type_id" id="owner_type_id">
                                    <option value="">Select...</option>
                                    @foreach($ownerTypes as $ownerType)
                                        <option value="{{ $ownerType->id }}"
                                            {{ $property->owner_type_id == $ownerType->id ? 'selected' : '' }}>
                                            {{ $ownerType->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span id="owner_type_id" class="help-block"></span>
                            </div>
                        </div>
                        <div id="client_surveyor" class="control-group">
                            {{ Form::label('client_surveyor', 'Client Surveyor', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('client_surveyor',$property->client_surveyor, array('class' => 'form-control')) }}
                                <span id="client_surveyor" class="help-block"></span>
                            </div>
                        </div>
                        <div id="client_admin" class="control-group">
                            {{ Form::label('client_admin', 'Client Admin', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('client_admin', $property->client_admin, array('class' => 'form-control')) }}
                                <span id="client_admin" class="help-block"></span>
                            </div>
                        </div>
                        <div id="capa_admin" class="control-group">
                            {{ Form::label('capa_admin', 'Capa Admin', array('class' => 'control-label')) }}
                            <div class="form-group controls">
                                {{ Form::text('capa_admin', $property->capa_admin, array('class' => 'form-control')) }}
                                <span id="capa_admin" class="help-block"></span>
                            </div>
                        </div>

                        <div id="capa_owner2_id" class="control-group">
                            <label for="capa_owner2_id">Capa Admin 2</label>
                            <div class="form-group controls">
                                <select id="capa_owner2_id" name="capa_owner2_id" class="form-control">
                                    <option disabled selected value="">Select...</option>
                                    @foreach($capaOwners2 as $owners2)
                                        <option  @if($owners2->id == $property->capa_owner2_id) selected @endif  value="{{$owners2->id}}">{{$owners2->name}}</option>
                                    @endforeach
                                </select>
                                <span id="capa_owner2_id" class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--            <!-- /Data block -->--}}
            {{--            <!-- Data block For Lease Details -->--}}
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Lease Details</h2>
                    </header>
                    <div class="div-repl-form">
                        <fieldset>
                            <div id="date_last_inspected" class="control-group">
                                {{ Form::label('date_last_inspected', 'Date Last Inspected', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $date_last_inspected = ($property->date_last_inspected) ? Date::dmY($property->date_last_inspected) : null }}
                                        {{ Form::text('date_last_inspected',$date_last_inspected, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="date_last_inspected" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="lease_start_date" class="control-group">
                                {{ Form::label('lease_start_date', 'Lease Start Date', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $lease_start_date = ($property->lease_start_date) ? Date::dmY($property->lease_start_date) : null }}
                                        {{ Form::text('lease_start_date',$lease_start_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="lease_start_date" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="lease_expiry_date" class="control-group">
                                {{ Form::label('lease_expiry_date', 'Lease Date Expiry', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $lease_expiry_date = ($property->lease_expiry_date) ? Date::dmY($property->lease_expiry_date) : null }}
                                        {{ Form::text('lease_expiry_date',$lease_expiry_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="lease_expiry_date" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="rent_review_date" class="control-group">
                                {{ Form::label('rent_review_date', 'Rent Review Date', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $rent_review_date = ($property->rent_review_date) ? Date::dmY($property->rent_review_date) : null }}
                                        {{ Form::text('rent_review_date',$rent_review_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="rent_review_date" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="lease_notice_dates" class="control-group">
                                {{ Form::label('lease_notice_dates', 'Lease Notice Date', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $lease_notice_dates = ($property->lease_notice_dates) ? Date::dmY($property->lease_notice_dates) : null }}
                                        {{ Form::text('lease_notice_dates',$lease_notice_dates, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="lease_notice_dates" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="erv_date" class="control-group">
                                {{ Form::label('erv_date', 'ERV Date', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $erv_date = ($property->erv_date) ? Date::dmY($property->erv_date) : null }}
                                        {{ Form::text('erv_date',$erv_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="erv_date" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="erv_amount" class="control-group">
                                {{ Form::label('erv_amount', 'ERV Amount', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-prepend">
                                        <span class="add-on">{!! $currencyCode !!}</span>
                                        {{ Form::text('erv_amount',$property->erv_amount, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                        <span id="erv_amount" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="guarantor" class="control-group">
                                {{ Form::label('guarantor', 'Guarantor', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('guarantor',$property->guarantor, array('class' => 'form-control')) }}
                                    <span id="guarantor" class="help-block"></span>
                                </div>
                            </div>
                            <div id="rent_payment_frequency_id" class="control-group">
                                {{ Form::label('rent_payment_frequency_id', 'Rent Payment Frequency', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('rent_payment_frequency_id', $rentPaymentFrequencyDropdown, $property->rent_payment_frequency_id, array('class' => 'form-control')) }}
                                    <span id="rent_payment_frequency_id" class="help-block"></span>
                                </div>
                            </div>
                            <div id="payment_frequency" class="control-group">
                                {{ Form::label('payment_frequency', 'Rent Payment Frequency Notes', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('payment_frequency',$property->payment_frequency, array('class' => 'form-control')) }}
                                    <span id="payment_frequency" class="help-block"></span>
                                </div>
                            </div>
                            <div id="payment_dates" class="control-group">
                                {{ Form::label('payment_dates', 'Rent Payment Dates', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('payment_dates',$property->payment_dates, array('class' => 'form-control')) }}
                                    <span id="payment_dates" class="help-block"></span>
                                </div>
                            </div>
                            <div id="deposit" class="control-group">
                                {{ Form::label('deposit', 'Deposit', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-prepend">
                                        <span class="add-on">{!! $currencyCode !!}</span>
                                        {{ Form::text('deposit',$property->deposit, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                        <span id="deposit" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="rent_paid" class="control-group">
                                {{ Form::label('rent_paid', 'Rent Paid', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-prepend">
                                        <span class="add-on">{!! $currencyCode !!}</span>
                                        {{ Form::text('rent_paid',$property->rent_paid, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                        <span id="rent_paid" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="break_clause_type_id" class="control-group">
                                {{ Form::label('break_clause_type_id', 'Break Clause', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('break_clause_type_id', $breakTypesDropdown, $property->break_clause_type_id, array('class' => 'form-control add-new-option')) }}
                                    <span id="break_clause_type_id" class="help-block"></span>
                                </div>
                            </div>
                            <div id="undergoing_refit" class="control-group">
                                {{ Form::label('undergoing_refit', 'Undergoing Re-Fit', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('undergoing_refit', '1', $property->undergoing_refit)}}
                                    <span id="undergoing_refit" class="help-block"></span>
                                </div>
                            </div>
                            <div id="has_service_charge" class="control-group">
                                {{ Form::label('has_service_charge', 'Property has Service Charge?', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('has_service_charge', '1', $property->has_service_charge)}}
                                    <span id="has_service_charge" class="help-block"></span>
                                </div>
                            </div>
                            <div id="has_break_options" class="control-group">
                                <label for="has_break_options" class="control-label">Property has Break Option?</label>
                                <div class="controls">
                                    <input type="hidden" name="has_break_options" value="0">
                                    <input type="checkbox"
                                           name="has_break_options"
                                           id="has_break_options"
                                           value="1"
                                        {{ $property->id ? ($property->has_break_options ? 'checked' : '') : 'checked' }}>
                                </div>
                            </div>
                            <div id="has_client_queries" class="control-group">
                                <label for="has_client_queries" class="control-label">Property has Client Query?</label>
                                <div class="controls">
                                    <input type="hidden" name="has_client_queries" value="0">
                                    <input type="checkbox"
                                           name="has_client_queries"
                                           id="has_client_queries"
                                           value="1"
                                        {{ $property->id ? ($property->has_client_queries ? 'checked' : '') : 'checked' }}>
                                </div>
                            </div>
                            <div id="has_maintenances" class="control-group">
                                <label for="has_maintenances" class="control-label">Property has Maintenances?</label>
                                <div class="controls">
                                    <input type="hidden" name="has_maintenances" value="0">
                                    <input type="checkbox"
                                           name="has_maintenances"
                                           id="has_maintenances"
                                           value="1"
                                        {{ $property->id ? ($property->has_maintenances ? 'checked' : '') : 'checked' }}>
                                </div>
                            </div>
                            <div id="has_rent_reviews" class="control-group">
                                <label for="has_rent_reviews" class="control-label">Property has Rent Reviews?</label>
                                <div class="controls">
                                    <input type="hidden" name="has_rent_reviews" value="0">
                                    <input type="checkbox"
                                           name="has_rent_reviews"
                                           id="has_rent_reviews"
                                           value="1"
                                        {{ $property->id ? ($property->has_rent_reviews ? 'checked' : '') : 'checked' }}>
                                </div>
                            </div>
                            {{--                            <div id="has_notes" class="control-group">--}}
                            {{--                                <label for="has_notes" class="control-label">Property has Notes?</label>--}}
                            {{--                                <div class="controls">--}}
                            {{--                                    <input type="hidden" name="has_notes" value="0">--}}
                            {{--                                    <input type="checkbox"--}}
                            {{--                                           checked="checked"--}}
                            {{--                                           name="has_notes"--}}
                            {{--                                           id="has_notes"--}}
                            {{--                                           disabled="disabled"--}}
                            {{--                                           value="1"--}}
                            {{--                                        {{ $property->id ? ($property->has_notes ? 'checked' : '') : 'checked' }}>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div id="lease_comments" class="control-group">
                                {{ Form::label('lease_comments', 'Lease Comments', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::textarea('lease_comments', $property->lease_comments, array('cols' => 50, 'rows' => 3,'class'=>'form-control')) }}
                                    <span id="lease_comments" class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </article>
            <!-- /Data block -->
            <div class='clearfix'></div>
            <!-- Data block For Lease Terms -->
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Lease Terms</h2>
                    </header>
                    <div class="div-repl-form">
                        <fieldset>
                            <div class="control-group"><h3>Alienation</h3></div>
                            <div id="alienation_type_id" class="control-group">
                                {{ Form::label('alienation_type_id', 'Alienation', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('alienation_type_id', $alienationTypesDropdown, $property->alienation_type_id, array('class' => 'form-control add-new-option')) }}
                                    <span id="alienation_type_id" class="help-block"></span>
                                </div>
                            </div>
                            <div id="concessions" class="control-group">
                                {{ Form::label('concessions', 'Concessions', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('concessions', '1', $property->concessions)}}
                                    <span id="concessions" class="help-block"></span>
                                </div>
                            </div>
                            <div id="share" class="control-group">
                                {{ Form::label('share', 'Share', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('share', '1', $property->share) }}
                                    <span id="share" class="help-block"></span>
                                </div>
                            </div>
                            <div id="alienation_note" class="control-group">
                                <div class="form-group controls">
                                    {{ Form::textarea('alienation_note',$property->alienation_note, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput','class'=>'form-control' )) }}
                                    <span id="alienation_note" class="help-block"></span>
                                </div>
                            </div>


                            <div class="control-group"><h3>Alterations</h3></div>
                            <div id="non_structural" class="control-group">
                                {{ Form::label('non_structural', 'Non Structural', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('non_structural', '1', $property->non_structural) }}
                                    <span id="non_structural" class="help-block"></span>
                                </div>
                            </div>
                            <div id="structural" class="control-group">
                                {{ Form::label('structural', 'Structural', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('structural', '1', $property->structural) }}
                                    <span id="structural" class="help-block"></span>
                                </div>
                            </div>
                            <div id="landlord_consent" class="control-group">
                                {{ Form::label('landlord_consent', 'Landlord Consent', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('landlord_consent', '1', $property->landlord_consent) }}
                                    <span id="landlord_consent" class="help-block"></span>
                                </div>
                            </div>
                            <div id="schedule_condition" class="control-group">
                                {{ Form::label('schedule_condition', 'Schedule of Condition', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('schedule_condition', '1', $property->schedule_condition) }}
                                    <span id="schedule_condition" class="help-block"></span>
                                </div>
                            </div>
                            <div id="alteration_note" class="control-group">
                                <div class="form-group controls">
                                    {{ Form::textarea('alteration_note',$property->alteration_note, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput','class'=>'form-control')) }}
                                    <span id="alteration_note" class="help-block"></span>
                                </div>
                            </div>


                            <div class="control-group"><h3>User Clause</h3></div>
                            <div id="restrictions" class="control-group">
                                {{ Form::label('restrictions', 'Restrictions', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('restrictions', '1', $property->restrictions) }}
                                    <span id="restrictions" class="help-block"></span>
                                </div>
                            </div>
                            <div id="user_type_id" class="control-group">
                                {{ Form::label('user_type_id', 'User Type', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('user_type_id', $userTypesDropdown, $property->user_type_id, array('class' => 'form-control add-new-option')) }}
                                    <span id="user_type_id" class="help-block"></span>
                                </div>
                            </div>
                            <div id="user_clause_note" class="control-group">
                                <div class="form-group controls">
                                    {{ Form::textarea('user_clause_note',$property->user_clause_note, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput', 'class'=> 'form-control')) }}
                                    <span id="user_clause_note" class="help-block"></span>
                                </div>
                            </div>


                            <div id="initial_concessions" class="control-group">
                                {{ Form::label('initial_concessions', 'Initial Concessions', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-prepend">
                                        <span class="add-on">{!! $currencyCode !!}</span>
                                        {{ Form::text('initial_concessions',$property->initial_concessions, array('class' => 'form-control', 'id' => 'prependedInput')) }}
                                        <span id="initial_concessions" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="initial_concessions_note" class="control-group">
                                {{ Form::label('initial_concessions_note', 'Initial Concessions', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::textarea('initial_concessions_note',$property->initial_concessions_note, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput','class'=> 'form-control')) }}
                                    <span id="initial_concessions_note" class="help-block"></span>
                                </div>
                            </div>

                            <div id="subletting_type_id" class="control-group">
                                {{ Form::label('subletting_type_id', 'Subletting Type', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('subletting_type_id', $sublettingTypesDropdown, $property->subletting_type_id, array('class' => 'form-control add-new-option')) }}
                                    <span id="subletting_type_id" class="help-block"></span>
                                </div>
                            </div>

                            <div class="control-group"><h3>Ext. Decoration</h3></div>
                            <div id="ext_frequency" class="control-group">
                                {{ Form::label('ext_frequency', 'Frequency', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('ext_frequency',$property->ext_frequency, array('class' => 'form-control')) }}
                                    <span id="ext_frequency" class="help-block"></span>
                                </div>
                            </div>
                            <div id="ext_first_due" class="control-group">
                                {{ Form::label('ext_first_due', 'First Due', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $ext_first_due = ($property->ext_first_due) ? Date::dmY($property->ext_first_due) : null }}
                                        {{ Form::text('ext_first_due',$ext_first_due, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="ext_first_due" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="ext_next_due" class="control-group">
                                {{ Form::label('ext_next_due', 'Next Due', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $ext_next_due = ($property->ext_next_due) ? Date::dmY($property->ext_next_due) : null }}
                                        {{ Form::text('ext_next_due',$ext_next_due, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="ext_next_due" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="ext_final_year" class="control-group">
                                {{ Form::label('ext_final_year', 'Final Year', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('ext_final_year', '1', $property->ext_final_year)}}
                                    <span id="ext_final_year" class="help-block"></span>
                                </div>
                            </div>

                            <div class="control-group"><h3>Int. Decoration</h3></div>
                            <div id="int_frequency" class="control-group">
                                {{ Form::label('int_frequency', 'Frequency', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('int_frequency',$property->int_frequency, array('class' => 'form-control')) }}
                                    <span id="int_frequency" class="help-block"></span>
                                </div>
                            </div>
                            <div id="int_first_due" class="control-group">
                                {{ Form::label('int_first_due', 'First Due', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $int_first_due = ($property->int_first_due) ? Date::dmY($property->int_first_due) : null }}
                                        {{ Form::text('int_first_due',$int_first_due, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="int_first_due" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="int_next_due" class="control-group">
                                {{ Form::label('int_next_due', 'Next Due', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-append">
                                        {{ $int_next_due = ($property->int_next_due) ? Date::dmY($property->int_next_due) : null }}
                                        {{ Form::text('int_next_due',$int_next_due, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                                        <span class="add-on"><i class="awe-calendar"></i></span>
                                        <span id="int_next_due" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="int_final_year" class="control-group">
                                {{ Form::label('int_final_year', 'Final Year', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::checkbox('int_final_year', '1', $property->int_final_year) }}
                                    <span id="int_final_year" class="help-block"></span>
                                </div>
                            </div>

                            <div id="repair_classification_type_id" class="control-group">
                                {{ Form::label('repair_classification_type_id', 'Repair Classifications', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('repair_classification_type_id', $repairClassificationTypesDropdown, $property->repair_classification_type_id, array('class' => 'form-control add-new-option'))  }}
                                    <span id="repair_classification_type_id" class="help-block"></span>
                                </div>
                            </div>
                            <div id="repair_obligations_note" class="control-group">
                                <div class="form-group controls">
                                    {{ Form::textarea('repair_obligations_note',$property->repair_obligations_note, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput','class'=>'form-control')) }}
                                    <span id="repair_obligations_note" class="help-block"></span>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                </div>
            </article>
            <!-- /Data block -->

            <!-- Data block For Insurance -->
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Rateable Value</h2>
                    </header>
                    <div class="div-repl-form">
                        <fieldset>
                            <div id="rateable_value" class="control-group">
                                {{ Form::label('rateable_value', 'Current Rateable Value', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    <div class="input-prepend">
                                        <span class="add-on">{!! $currencyCode !!}</span>
                                        {{ Form::text('rateable_value',$property->rateable_value, array('class' => 'form-control')) }}
                                        <span id="rateable_value" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="proposed_rateable_value" class="control-group">
                                {{ Form::label('proposed_rateable_value', 'Proposed Rateable Value', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('proposed_rateable_value',$property->proposed_rateable_value, array('class' => 'form-control')) }}
                                    <span id="proposed_rateable_value" class="help-block"></span>
                                </div>
                            </div>
                            <div id="agreed_rateable_value" class="control-group">
                                {{ Form::label('agreed_rateable_value', 'Agreed Rateable Value', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::text('agreed_rateable_value',$property->agreed_rateable_value, array('class' => 'form-control')) }}
                                    <span id="agreed_rateable_value" class="help-block"></span>
                                </div>
                            </div>
                            <div id="rv_notes" class="control-group">
                                {{ Form::label('rv_notes', 'Notes', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::textarea('rv_notes',$property->rv_notes, array('class' => 'input-xlarge','rows'=> 3, 'id' => 'prependedInput', 'class'=> 'form-control')) }}
                                    <span id="rv_notes" class="help-block"></span>
                                </div>
                            </div>
                            <div id="appeal_type_id" class="control-group">
                                {{ Form::label('appeal_type_id', 'Appeal Type', array('class' => 'control-label')) }}
                                <div class="form-group controls">
                                    {{ Form::select('appeal_type_id', $appealTypesDropdown, $property->appeal_type_id, array('class' => 'form-control add-new-option')) }}
                                    <span id="appeal_type_id" class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </article>
            <!-- /Data block -->
            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <div class="div-repl-form">
                        <fieldset>
                            <div id='error-message'></div>
                        {{ Form::hidden('address_id', $property->address_id)}}
                        <!-- submit button -->
                            <div class="form-actions">
                                <button class="btn btn-large btn-primary" type="submit">
                                    Save Changes
                                </button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </article>
        </form>
    </div>
    <!-- /Grid row -->
</div>
<!-- /Right (content) side -->

<!-- Div for rendering Add Agent Form -->
{{--<div id='addAgentForm'>--}}
{{--    <div class="modal fade hide" id="addAgentModal">--}}
{{--        <div class="modal-header">--}}
{{--            <button type="button" class="close" data-dismiss="modal">×</button>--}}
{{--            <h3>Managing Add Agent</h3>--}}
{{--        </div>--}}
{{--        <div class="modal-body">--}}
{{--            @include('agents.includes.form', [--}}
{{--                'agent'=> null,--}}
{{--                'address' => null,--}}
{{--                'isModal' => true,--}}
{{--                'restMethod' => 'post',--}}
{{--                'route' => 'agents.store'--}}
{{--            ])--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


<div class="modal fade" id="addAgentModal" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Create Managing Agent</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                @include('agents.includes.form', [
                    'agent'=> null,
                    'address' => null,
                    'isModal' => true,
                    'restMethod' => 'post',
                    'route' => 'agents.store'
                ])
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id='addLandlordModal' tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Create Landlord</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                @include('landlords.includes.form', [
                         'landlord' => null,
                         'address' => null,
                         'isModal' => true,
                         'restMethod' => 'post',
                         'route' => 'landlords.store'
                ])
            </div>
        </div>
    </div>
</div>


<!-- Div for rendering Add Landlord Form -->
{{--<div>--}}
{{--    <div class="modal fade hide" id="addLandlordModal">--}}
{{--        <div class="modal-header">--}}
{{--            <button type="button" class="close" data-dismiss="modal">×</button>--}}
{{--            <h3>Add Landlord</h3>--}}
{{--        </div>--}}
{{--        <div class="modal-body">--}}
{{--            @include('landlords.includes.form', [--}}
{{--                'landlord' => null,--}}
{{--                'address' => null,--}}
{{--                'isModal' => true,--}}
{{--                'restMethod' => 'post',--}}
{{--                'route' => 'landlords.store'--}}
{{--            ])--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Div for rendering Add Client Form -->
<div id='addClientForm'>
    <div class="modal fade hide" id="addClientModal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>Add Client</h3>
        </div>
        <div class="modal-body">
            @include('clients.includes.form', [
                'client'     => null,
                'address'    => null,
                'isModal'    => true,
                'restMethod' => 'post',
                'route'      => 'clients.store',
            ])
        </div>
    </div>
</div>

@include('partials.base_modal')

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).ready(function () {
                $(".select2").select2();
            });
        });
    </script>
@endpush
