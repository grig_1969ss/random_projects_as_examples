<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($payments as $key => $payment)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-payment-{{$payment->id}}-tab" data-toggle="tab"
               href="#nav-payment-{{$payment->id}}" role="tab"
               aria-controls="nav-payment-{{$payment->id}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >{{ $payment->payment_ref }}</a>
        @endforeach
    </div>
</nav>
