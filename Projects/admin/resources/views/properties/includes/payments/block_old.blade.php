<div class="row">
    {{--            <header>--}}
    {{--                <h2>Approvals</h2>--}}
    {{--                <ul class="data-header-actions">--}}
    {{--                    @if ($read_only == 0)--}}
    {{--                        <a href="{{ route('properties.payments.create', $property) }}" class="btn btn-alt">--}}
    {{--                            New Approval--}}
    {{--                        </a>--}}
    {{--                    @endif--}}
    {{--                </ul>--}}
    {{--            </header>--}}
    <div class="col-md-12">
        @include('properties.includes.payments.tabs')

        <div class="tab-content" id="nav-tabContent">
            @if($payments->count() == 0)
                No approval found.
                {{--                                @if(!$read_only)--}}
                {{--                                    <a href="{{ route('properties.payments.create', $property) }}">Add Approval</a>--}}
                {{--                                @endif--}}
            @endif
            @foreach($payments as $key => $payment)
                {{--                                    <header>--}}
                {{--                                        <h2>Approval - {{ $payment->payment_ref }}</h2>--}}
                {{--                                        <ul class="data-header-actions">--}}
                {{--                                            @if(!$read_only)--}}
                {{--                                                <li>--}}
                {{--                                                    <a href="{{ route('payments.edit', $payment) }}"--}}
                {{--                                                       class="btn btn-warning btn-alt">Edit</a>--}}
                {{--                                                </li>--}}
                {{--                                            @endif--}}
                {{--                                            @role('superAdmin')--}}
                {{--                                                <li>--}}
                {{--                                                    <button class="btn btn-danger btn-alt delete-btn"--}}
                {{--                                                            data-form-class="delete-approval-form">--}}
                {{--                                                        Delete--}}
                {{--                                                    </button>--}}

                {{--                                                    <form action="{{ route('payments.destroy', $payment) }}"--}}
                {{--                                                          method="POST"--}}
                {{--                                                          class="delete-approval-form">--}}
                {{--                                                        @csrf--}}
                {{--                                                        @method('DELETE')--}}
                {{--                                                    </form>--}}
                {{--                                                </li>--}}
                {{--                                            @endrole--}}
                {{--                                        </ul>--}}
                {{--                                    </header>--}}
                @include('properties.includes.payments.inner_section')
            @endforeach
        </div>
    </div>
</div>


