<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-payment-{{$payment->id}}" role="tabpanel"
     aria-labelledby="nav-payment-{{$payment->id}}-tab">

    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Period</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->period or '-' }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Description</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->description or '-' }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Supplier</small>
                </div>
                <div class="col-sm-6 pl-0">
                    @if($payment->supplier)
                        <p class="mb-0">
                            <a href="{{ route('suppliers.show', $payment->supplier) }}">
                                {{ $payment->present()->supplier }}
                            </a>
                        </p>
                    @else
                        <p class="mb-0">-</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Supplier Contact</small>
                </div>
                <div class="col-sm-6 pl-0">
                    @if($payment->contact)
                        <p class="mb-0">
                            <a href="{{ route('contacts.show', $payment->contact) }}">{{ $payment->present()->contact }}</a>
                        </p>
                    @else
                        <p class="mb-0">-</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Type</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->present()->paymentType }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Invoice Date</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->present()->paymentDate }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Gross Invoice Amount</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $payment->present()->grossInvoiceAmount !!}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Tax Amount</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $payment->present()->taxAmount !!}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Approved Date</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->present()->dateApproved }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Hold?</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->present()->hold }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Notes</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $payment->note }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.payments.documents.create', $payment),--}}
{{--    'documents' => $payment->documents--}}
{{--])--}}
