@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endpush
<table id="property-approvals-table"
       class="datatable table table-striped table-bordered table-hover dataTable table-responsive">
    <thead>
    <tr>
        @foreach($payments['headers'] as $header)
            <th>{{$header}}</th>
        @endforeach
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach(\Illuminate\Support\Arr::get($payments, 'rows', []) as $key => $paymentRow)
        @continue($key === 'yearsExists')
        <tr>
            @foreach($paymentRow as $data)
                <td>
                    @if(!next($paymentRow))
                        <div class="row">
                            @if($data != '')
                                <div class="col-5">
                                    {!! $data !!}
                                </div>
                            @else
                                <div class="col-6">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-angle-down dropdown-arrow"></i>
                                            <span>Attach</span>
                                        </button>
                                        <div class="dropdown-menu" id="action-dropdown" x-placement="top-start">
                                            <button type="button"
                                                    class="dropdown-item services-charge attach-budget approvals-upload"
                                                    data-toggle="modal" data-selected="{{$paymentRow[0]}}"
                                                    data-target="#documentModal"
                                                    data-id="note_type_dropdown">
                                                Attach Document
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @else
                        {!! $data !!}
                    @endif

                </td>

            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>

@push('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>

    <script>
        // Setup - add a text input to each footer cell
        $(document).ready(function () {

            $('#property-approvals-table thead tr').clone(true).appendTo('#property-approvals-table thead');
            $('#property-approvals-table thead tr:eq(1) th').each(function (i) {
                let title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });

                $('input', this).on('click', function (e) {
                    e.stopPropagation();
                });

            });

            let table = $('#property-approvals-table').DataTable();

        });

    </script>
@endpush
