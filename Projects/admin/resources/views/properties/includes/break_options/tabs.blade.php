<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($breaks as $key => $break)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-break-{{$break->id}}-tab" data-toggle="tab"
               href="#nav-break-{{$break->id}}" role="tab"
               aria-controls="nav-break-{{$break->id}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >Break Option {{Date::dmY($break->break_date)}}</a>
        @endforeach
    </div>
</nav>
