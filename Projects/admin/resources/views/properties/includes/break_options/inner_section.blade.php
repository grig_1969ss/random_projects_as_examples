<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-break-{{$break->id}}" role="tabpanel"
     aria-labelledby="nav-break-{{$break->id}}-tab">

    <div class="col-12 mb-4 d-flex justify-content-between">
        <div class="dropdown">
            <div class="btn-group mr-2 mb-2">
                <a href="{{route('breaks.edit',$break->id)}}?property={{$propertyID}}"
                   class="btn add-property btn-outline-primary">Edit</a>
                <button data-url="{{route('breaks.destroy', $break->id)}}" data-target="#modal-delete-confirm" data-toggle="modal"
                   class="btn ml-2 add-property btn-outline-danger delete-confirm-button">Delete</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Break Clause</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($break->breakType->name) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Break Option Date</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue(Date::dmY($break->break_date)) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Break Notice Date</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue(Date::dmY($break->notice_period)) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">No. of Days</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($break->number_of_days) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Details</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($break->break_clause) }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.breaks.documents.create', $break),--}}
{{--    'documents' => $break->documents--}}
{{--])--}}
