<div class="row">

{{--            <header>--}}
{{--                <h2>Break Options</h2>--}}
{{--                <ul class="data-header-actions">--}}
{{--                    @if ($read_only == 0)--}}
{{--                        <li>--}}
{{--                            <button class="modalButton btn btn-alt modal-ajax add-break-btn"--}}
{{--                                    data-url="{{ route('properties.breaks.create', $property) }}">--}}
{{--                                New Break Option--}}
{{--                            </button>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                </ul>--}}
{{--            </header>--}}
<!-- Tab #three -->
    <!-- Second level tabs -->
    <div class="col-md-12">
        @include('properties.includes.break_options.tabs')

        <div class="tab-content" id="nav-tabContent">
            @if($breaks->count() == 0)
                No Break Option Found.
                {{--                    @if (!$read_only)--}}
                {{--                        <a href="#"--}}
                {{--                           class="modal-ajax add-break-btn"--}}
                {{--                           data-url="{{ route('properties.breaks.create', $property) }}">--}}
                {{--                            Add Break Option--}}
                {{--                        </a>--}}
                {{--                    @endif--}}
            @endif
            @foreach($breaks as $key => $break)
                {{--                        <header>--}}
                {{--                            <h2>Break Option - {{Date::dmY($break->break_date)}}</h2>--}}
                {{--                            <ul class="data-header-actions">--}}
                {{--                                @if(!$read_only)--}}
                {{--                                    <li>--}}
                {{--                                        <button class="btn btn-warning btn-alt modal-ajax add-break-btn"--}}
                {{--                                                data-url="{{ route('breaks.edit', $break) }}">--}}
                {{--                                            Edit--}}
                {{--                                        </button>--}}
                {{--                                    </li>--}}
                {{--                                    <li>--}}
                {{--                                        <a href="#"--}}
                {{--                                           class="btn btn-warning btn-alt delete-break"--}}
                {{--                                           id="deleteBreak">Delete</a>--}}

                {{--                                        <form method="POST"--}}
                {{--                                              action="{{ route('breaks.destroy', $break) }}"--}}
                {{--                                              class="hidden">--}}
                {{--                                            {{ method_field('DELETE') }}--}}
                {{--                                            @csrf--}}
                {{--                                        </form>--}}
                {{--                                    </li>--}}
                {{--                                @endif--}}
                {{--                            </ul>--}}
                {{--                        </header>--}}
                @include('properties.includes.break_options.inner_section')
            @endforeach
        </div>
    </div>
</div>

