<div class="modal fade" id="reDiariseModal" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Re Diarise</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{route('rediarise')}}" method="post" id="re-diarise">
                    @csrf

                    <input type="hidden" name="model" value="">
                    <input type="hidden" name="modelInstance" value="">
                    <input type="hidden" name="property_diarie" value="">
                    <input type="hidden" name="object_type" value="">
                    <input type="hidden" name="note_type" value="">
                    <div id="date" class="form-group control-group">
                        <small class="d-block font-weight-normal mb-3">Diary Date</small>
                        <div class="form-group focused">
                            <input data-date-format="dd/mm/yyyy" name="diary_date" class="form-control datepicker" type="text" autocomplete="off">
                        </div>
                        <span id="date" class="help-block"></span>
                    </div>

                    <div id="diarise_note" class="form-group control-group">
                        <label for="diarise-note">Note</label>
                        <div>
                            <textarea class="form-control" name="diarise_note" id="diarise-note" cols="10"
                                      rows="3"></textarea>
                        </div>
                        <span id="diarise_note" class="help-block"></span>
                    </div>

                    @include('partials.modal_footer', array('buttonName' => 'Save'))
                </form>

            </div>

        </div>
    </div>
</div>
