<div class="row">
{{--            <header>--}}
{{--                <h2>Client Query</h2>--}}
{{--                <ul class="data-header-actions">--}}
{{--                    @if(!$read_only)--}}
{{--                        <li>--}}
{{--                            <a id="addClientQueryModal"--}}
{{--                               href="{{ route('properties.client-queries.create', $property) }}"--}}
{{--                               class="modalButton btn btn-alt clientQueryModal">--}}
{{--                                New Client Query--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                </ul>--}}
{{--            </header>--}}
<!-- Tab #three -->
    <div class="col-md-12">
            @include('properties.includes.client_queries.tabs')

            <div class="tab-content" id="nav-tabContent">
                @if($clientQueries->count() == 0)
                    No Client Query Found.
                    @if(!$read_only)
{{--                        <a data-toggle="modal"--}}
{{--                           class='clientQueryModal'--}}
{{--                           id="addClientQueryModal"--}}
{{--                           href="{{ route('properties.client-queries.create', $property) }}">--}}
{{--                            Add Client Query--}}
{{--                        </a>--}}
                    @endif
                @endif

                @foreach($clientQueries as $key => $clientQuery)

{{--                        <header>--}}
{{--                            <h2>Review - {{Date::dmY($clientQuery->due_date)}}</h2>--}}
{{--                            <ul class="data-header-actions">--}}
{{--                                @if(!$read_only)--}}
{{--                                    <li>--}}
{{--                                        <a href="{{ route('client-queries.edit', $clientQuery) }}"--}}
{{--                                           class="btn btn-warning btn-alt clientQueryModal"--}}
{{--                                           id="editClientQueryModal">Edit</a>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                            </ul>--}}
{{--                        </header>--}}
                        @include('properties.includes.client_queries.inner_section')
                @endforeach
            </div>
        </div>
    </div>

<!-- /Data block -->

