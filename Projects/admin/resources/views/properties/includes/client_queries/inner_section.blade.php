<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-query-{{$clientQuery->id}}" role="tabpanel"
     aria-labelledby="nav-query-{{$clientQuery->id}}-tab">

    <div class="col-12 mb-4 d-flex justify-content-between">
        <div class="dropdown">
            <div class="btn-group mr-2 mb-2">
                <a href="{{route('client-queries.edit',$clientQuery->id)}}?property={{$propertyID}}"
                   class="btn add-property btn-outline-primary">Edit</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Break Clause</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">eeeeeeeeeeeee</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Client Query Status</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $clientQuery->clientQueryStatus ? Util::showValue($clientQuery->clientQueryStatus->name):'-' }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Details</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($clientQuery->details) }}</p>
                </div>
            </div>
        </div>
    </div>

</div>

{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.client-queries.documents.create', $clientQuery),--}}
{{--    'documents' => $clientQuery->documents--}}
{{--])--}}
