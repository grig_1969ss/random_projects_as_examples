<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($clientQueries as $key => $clientQuery)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-query-{{$clientQuery->id}}-tab" data-toggle="tab"
               href="#nav-query-{{$clientQuery->id}}" role="tab"
               aria-controls="nav-query-{{$clientQuery->id}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >Query {{Date::dmY($clientQuery->due_date)}}</a>
        @endforeach
    </div>
</nav>
