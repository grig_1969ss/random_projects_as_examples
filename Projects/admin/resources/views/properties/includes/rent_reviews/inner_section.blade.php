<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-rreview-{{$rentReview->id}}" role="tabpanel"
     aria-labelledby="nav-rreview-{{$rentReview->id}}-tab">
    <div class="row">

        <div class="col-12 mb-4 d-flex justify-content-between">
            <div class="dropdown">
                <div class="btn-group mr-2 mb-2">
                    <a href="{{route('rent-reviews.edit',$rentReview->id)}}?property={{$propertyID}}"
                       class="btn add-property btn-outline-primary">Edit</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Break Option</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->break_option) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Rent</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {!! $rentReview->present()->formatNumber($rentReview->rent) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Time of Essence</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Form::checkbox('share', '1', $rentReview->time_of_essence, array('disabled' => 'disabled')) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Rent Review Pattern</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->rent_review_pattern) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Interest?</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Form::checkbox('share', '1', $rentReview->interest, array('disabled' => 'disabled')) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Notice By</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->notice_by) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Notice Period</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->notice_period) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Notice Amount</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {!! $rentReview->present()->formatNumber($rentReview->notice_amount) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Notice Dates</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Date::dmY($rentReview->notice_dates) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Turnover Rent</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Form::checkbox('share', '1', $rentReview->turnover_rent, array('disabled' => 'disabled')) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">No. of Floors</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->no_floors) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">ITZA</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->itza) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Breakdown of Areas</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->floors_occupied) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Bank</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->bank_id) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Base Rate</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {!! $rentReview->present()->formatNumber($rentReview->base_rate) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Turnover Above</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {!! $rentReview->present()->formatNumber($rentReview->turnover_above) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Notes</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($rentReview->notes) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Floor areas confirmed</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Form::checkbox('share', '1', $rentReview->floor_areas_confirmed, array('disabled' => 'disabled')) }}
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>


{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.rent-reviews.documents.create', $rentReview),--}}
{{--    'documents' => $rentReview->documents--}}
{{--])--}}
