<div class="row">
    {{--            <header>--}}
    {{--                <h2>Rent Review</h2>--}}
    {{--                <ul class="data-header-actions">--}}
    {{--                    @if(!$read_only)--}}
    {{--                        <li>--}}
    {{--                            <a class="btn btn-alt"--}}
    {{--                               href="{{ route('properties.rent-reviews.create', $property) }}">--}}
    {{--                                New Rent Review--}}
    {{--                            </a>--}}
    {{--                        </li>--}}
    {{--                    @endif--}}
    {{--                </ul>--}}
    {{--            </header>--}}
    <div class="col-md-12">
        @include('properties.includes.rent_reviews.tabs')

        <div class="tab-content" id="nav-tabContent">
            @if($rentReviews->count() == 0)
                No Rent Review found.
                {{--                                @if(!$read_only)--}}
                {{--                                    <a href="{{ route('properties.rent-reviews.create', $property) }}" >--}}
                {{--                                        Add Rent Review--}}
                {{--                                    </a>--}}
                {{--                                @endif--}}
            @endif
            @foreach($rentReviews as $key => $rentReview)
                {{--                                    <header>--}}
                {{--                                        <h2>Review - {{Date::dmY($rentReview->date)}}</h2>--}}
                {{--                                        <ul class="data-header-actions">--}}
                {{--                                            @if(!$read_only)--}}
                {{--                                                <li>--}}
                {{--                                                    <a class="btn btn-warning btn-alt"--}}
                {{--                                                       href="{{ route('rent-reviews.edit', $rentReview) }}">--}}
                {{--                                                        Edit--}}
                {{--                                                    </a>--}}
                {{--                                                </li>--}}
                {{--                                            @endif--}}
                {{--                                        </ul>--}}
                {{--                                    </header>--}}
                @include('properties.includes.rent_reviews.inner_section')
            @endforeach
        </div>
    </div>
</div>

