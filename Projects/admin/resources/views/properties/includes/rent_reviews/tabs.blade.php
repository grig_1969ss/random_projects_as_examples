<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($rentReviews as $key => $rentReview)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-rreview-{{$rentReview->id}}-tab" data-toggle="tab"
               href="#nav-rreview-{{$rentReview->id}}" role="tab"
               aria-controls="nav-rreview-{{$rentReview->id}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >Review {{Date::dmY($rentReview->date)}}</a>
        @endforeach
    </div>
</nav>
