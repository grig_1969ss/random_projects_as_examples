<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">New Alert</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{route('property.alert.create')}}" method="POST" id="add-alert">
                    @csrf
                    <input id="alert-oti" type="hidden" name="object_type_id" value="">
                    <input id="alert-oi" type="hidden" name="object_id" value="">
                    <input id="property_id" type="hidden" name="property_id" value="{{request()->segment(2)}}">
                    <div id="message" class="form-group control-group">
                        <label for="message">Message</label>
                        <div>
                            <textarea id="message" name="message" class="form-control" type="text"></textarea>
                        </div>
                        <span id="message" class="help-block"></span>
                    </div>

                    @include('partials.modal_footer', array('buttonName' => 'Save'))

                </form>

            </div>
        </div>
    </div>
</div>
