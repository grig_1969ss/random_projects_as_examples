<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($property->insurances as $key => $insurance)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-insurance-{{$insurance->id}}-tab" data-toggle="tab"
               href="#nav-insurance-{{$insurance->id}}" role="tab"
               aria-controls="nav-insurance-{{$insurance->id}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >Insurance {{ $insurance->present()->renewalDate }}</a>
        @endforeach
    </div>
</nav>
