<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-insurance-{{$insurance->id}}" role="tabpanel"
     aria-labelledby="nav-insurance-{{$insurance->id}}-tab">

    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Covered By</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->coveredBy }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Block Policy</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->blockPolicy }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Insurance Premium</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $insurance->present()->insurancePremium !!}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Loss of Rent</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->lossOfRent }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Terrorism Cover</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->terrorismCover }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Terrorism Premium</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $insurance->present()->terrorismPremium !!}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Plate Glass</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->plateGlass }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Tenant Reimburses</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->tenantReimburses }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Valuation Fee Payable</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->present()->valuationFeePayable }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Period of Insurance</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->period_of_insurance }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Insurer</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $insurance->insurer }}</p>
                </div>
            </div>
        </div>
    </div>
</div>


{{--<section>--}}
{{--    <div class="row-fluid">--}}
{{--        <div class="span2"><strong>Covered By</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->coveredBy }}</div>--}}

{{--        <div class="span2"><strong>Block Policy</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->blockPolicy }}</div>--}}
{{--    </div>--}}
{{--    <div class="row-fluid">--}}
{{--        <div class="span2"><strong>Insurance Premium</strong></div>--}}
{{--        <div class="span3">{!! $insurance->present()->insurancePremium !!}</div>--}}

{{--        <div class="span2"><strong>Loss of Rent</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->lossOfRent }}</div>--}}
{{--    </div>--}}
{{--    <div class="row-fluid">--}}
{{--        <div class="span2"><strong>Terrorism Cover</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->terrorismCover }}</div>--}}

{{--        <div class="span2"><strong>Terrorism Premium</strong></div>--}}
{{--        <div class="span3">{!! $insurance->present()->terrorismPremium !!}</div>--}}
{{--    </div>--}}
{{--    <div class="row-fluid">--}}
{{--        <div class="span2"><strong>Plate Glass</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->plateGlass }}</div>--}}

{{--        <div class="span2"><strong>Tenant Reimburses</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->tenantReimburses }}</div>--}}
{{--    </div>--}}
{{--    <div class="row-fluid">--}}
{{--        <div class="span2"><strong>Valuation Fee Payable</strong></div>--}}
{{--        <div class="span3">{{ $insurance->present()->valuationFeePayable }}</div>--}}

{{--        <div class="span2"><strong>Period of Insurance</strong></div>--}}
{{--        <div class="span3">{{ $insurance->period_of_insurance }}</div>--}}
{{--    </div>--}}
{{--    <div class="row-fluid">--}}
{{--        <div class="span2"><strong>Insurer</strong></div>--}}
{{--        <div class="span3">{{ $insurance->insurer }}</div>--}}
{{--    </div>--}}
{{--</section>--}}

{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.insurances.documents.create', $insurance),--}}
{{--    'documents' => $insurance->documents--}}
{{--])--}}
