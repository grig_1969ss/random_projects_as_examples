<div class="row">
    {{--            <header>--}}
    {{--                <h2>Insurances</h2>--}}
    {{--                <ul class="data-header-actions">--}}
    {{--                    @if(!$read_only)--}}
    {{--                        <li>--}}
    {{--                            <a class="btn btn-alt"--}}
    {{--                               href="{{ route('properties.insurances.create', $property) }}">New Insurance</a>--}}
    {{--                        </li>--}}
    {{--                    @endif--}}
    {{--                </ul>--}}
    {{--            </header>--}}
    <div class="col-md-12">
        @include('properties.includes.insurances.tabs')

        <div class="tab-content" id="nav-tabContent">
            @if($property->insurances->count() == 0)
                No insurance found.
                {{--                @if(!$read_only)--}}
                {{--                    <a href="{{ route('properties.insurances.create', $property) }}">Add Insurance</a>--}}
                {{--                @endif--}}
            @endif

            @foreach($property->insurances as $key => $insurance)
                    {{--                    <header>--}}
                    {{--                        <h2>Insurance - {{ $insurance->present()->renewalDate }}</h2>--}}
                    {{--                        <ul class="data-header-actions">--}}
                    {{--                            @if(!$read_only)--}}
                    {{--                                <li>--}}
                    {{--                                    <a class="btn btn-warning btn-alt"--}}
                    {{--                                       href="{{ route('insurances.edit', $insurance) }}">Edit</a>--}}
                    {{--                                </li>--}}
                    {{--                                @role('superAdmin')--}}
                    {{--                                <li>--}}
                    {{--                                    <button class="btn btn-danger btn-alt delete-btn"--}}
                    {{--                                            data-form-class="delete-insurance-form">--}}
                    {{--                                        Delete--}}
                    {{--                                    </button>--}}

                    {{--                                    <form action="{{ route('insurances.destroy', $insurance) }}"--}}
                    {{--                                          method="POST"--}}
                    {{--                                          class="delete-insurance-form">--}}
                    {{--                                        @csrf--}}
                    {{--                                        @method('DELETE')--}}
                    {{--                                    </form>--}}
                    {{--                                </li>--}}
                    {{--                                @endrole--}}
                    {{--                            @endif--}}
                    {{--                        </ul>--}}
                    {{--                    </header>--}}
                    @include('properties.includes.insurances.inner_section')
            @endforeach
        </div>
    </div>
</div>
