<div id="insurance-table-preview">
    <table class="table table-responsive">
        @foreach($insurances as $key =>  $insuranceRow)
            @continue($key === 'yearsExists')
            <tr @if(in_array($insuranceRow[0],['Terrorism Covered','Terrorism Premium','Cover Note Received'])) class="merge-rows" @endif>
                @foreach($insuranceRow as $keyRow => $column)
                    <td data-key="{{$keyRow}}">
                        @if($key =='yearRow' && $keyRow)
                            <div class="btn-group">
                                <button type="button"
                                        class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-angle-down dropdown-arrow"></i>
                                    <span>Actions</span>
                                </button>
                                <div class="dropdown-menu" id="action-dropdown" x-placement="top-start">
                                    <a class="dropdown-item btn" href="{{$column['updateUrl']}}">Edit</a>
                                    <button type="button" class="dropdown-item insurance-add-note"
                                            data-toggle="modal" data-object_id="{!! $column['value'] !!}"
                                            data-selected="10" data-object_type_id="9" data-target="#noteModal">Add Note
                                    </button>
                                    <button type="button" class="dropdown-item add-re-diarise"
                                            data-object_id="{!! $column['value'] !!}" data-model="Insurance"
                                            data-selected="10" data-object_type="insurances" data-object_type_id="9"
                                            data-chosen_date="{!! $column['diaryDate'] !!}">
                                        Re Diarise
                                    </button>
                                    <button type="button" class="dropdown-item add-alert"
                                            data-object-type="9"
                                            data-selected="{!! $column['value'] !!}"
                                            data-id="note_type_dropdown">
                                        Add Alert
                                    </button>
                                    <button type="button" class="dropdown-item services-charge" data-toggle="modal"
                                            data-selected="{{$column['value']}}" data-id="note_type_dropdown"
                                            data-target="#documentModal" data-convert="true">Attach Schedule
                                    </button>
                                </div>
                            </div>
                            <br>
                            <span><a href="#"> {!! $column['label'] !!} </a></span>
                        @elseif($key == 'noteReceivedRow' && $keyRow !=0)

                            @if($column['schedule'] == 'Yes')
                                <i class="fas fa-circle icon-success" data-toggle="tooltip"
                                   data-placement="top" data-original-title=""></i>

                                @if(!$column['franchisee_updated'])
                                    <i class="fas fa-circle icon-danger email-template"
                                       data-model="Insurance" data-type="Franchisee Updated"
                                       data-tab_type="insurance-add-note"
                                       data-placement="top" data-original-title="" data-toggle="modal"
                                       data-template-id="{{config('services.email_templates.ins_franchisee_rec_type_id')}}"
                                       data-target="#modal-new-email"></i>
                                @else
                                    <i class="fas fa-circle icon-success"
                                       data-placement="top" data-original-title=""></i>
                                @endif
                            @else
                                <i class="fas fa-circle icon-danger email-template" data-toggle="tooltip"
                                   data-model="Insurance" data-type="Franchisee Updated"
                                   data-tab_type="insurance-add-note"
                                   data-placement="top" data-original-title=""
                                   data-template-id="{{config('services.email_templates.ins_franchisee_rec_type_id')}}"></i>
                                @if(!$column['franchisee_updated'])
                                    <i class="fas fa-circle icon-gray" data-toggle="tooltip"
                                       data-placement="top" data-original-title=""></i>
                                @else
                                    <i class="fas fa-circle icon-success"
                                       data-placement="top" data-original-title=""></i>
                                @endif
                            @endif

                        @else
                            {!! $column !!}
                        @endif
                    </td>
                @endforeach
            </tr>
        @endforeach
    </table>
</div>
@include('layouts_new.partials.components.pdf.viewer')
@push('js')
    <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
    <script src="{{asset('js/pdf.js')}}"></script>
@endpush

