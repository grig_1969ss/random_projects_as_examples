<div id="properties-list-replace">
    <div class="row">
        <div class="col-md-12">
            <div class="mb-5">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Property</th>
                        <th scope="col">Region</th>
                        <th scope="col">Property Ref</th>
                        <th scope="col">Post Code</th>
                        <th scope="col">Client</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($properties as $property)
                        <tr>
                            <td>
                                <a href="{{ route('properties.show', $property->id) }}">
                                    {{ Util::showValue($property->property_name) }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('properties.show', $property->id) }}">
                                    {{ Util::showValue($property->region_name) }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('properties.show', $property->id) }}">
                                    {{ Util::showValue($property->reference) }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('properties.show', $property->id) }}">
                                    {{ Util::showValue($property->post_code) }}
                                </a>
                            </td>

                            @if($showClients)
                                <td>
                                    <a href="{{ route('properties.show', $property->id) }}">
                                        {{ Util::showValue($property->client_name) }}
                                    </a>
                                </td>
                            @else
                                <td>
                                    <a href="{{ route('properties.show', $property->id) }}">
                                        {{ Util::showValue($property->agent_name) }}
                                    </a>
                                </td>
                            @endif
                            <td>
                                <a href="{{ route('properties.show', $property->id) }}">
                                    <span class="label label-{{ $property->button }}">{{ $property->name }}</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>


                @if(request()->segment(1) == 'properties' || request()->segment(1) == 'property' )

                    <nav class="property-list-pagination default-pagination" aria-label="Page navigation example">
                        <span>Showing {{$properties->firstItem()}} to {{$properties->lastItem()}} of {{$properties->total()}} entries</span>
                        {{
                        $properties
                        ->appends([
                            'status' => request()->get('status'),
                            'search' => $search ?? null,
                            ])
                        ->links()
                        }}
                    </nav>
                @endif


            </div>
        </div>
    </div>
</div>
