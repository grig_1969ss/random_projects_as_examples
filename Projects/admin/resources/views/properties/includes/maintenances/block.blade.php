<div class="row">

{{--            <header>--}}
{{--                <h2>Maintenance</h2>--}}
{{--                <ul class="data-header-actions">--}}
{{--                    @if(!$read_only)--}}
{{--                        <li>--}}
{{--                            <a href="{{ route('properties.maintenances.create', $property) }}"--}}
{{--                               id="addClientQueryModal"--}}
{{--                               class="modalButton btn btn-alt clientQueryModal">New Maintenance Job</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                </ul>--}}
{{--            </header>--}}
<!-- Tab #four -->
    <!-- Second level tabs -->
    <div class="col-md-12">
        @include('properties.includes.maintenances.tabs')

        <div class="tab-content" id="nav-tabContent">
            @if($maintenance->count() == 0)
                No Maintenace Jobs Found.
                @if(!$read_only)
                    {{--                                    <a href="{{ route('properties.maintenances.create', $property) }}"--}}
                    {{--                                       class="maintenanceModal"--}}
                    {{--                                       id="addmaintenanceModal">Add Maintenace Job</a>--}}
                @endif
            @endif
            @foreach($maintenance as $key => $maintenanceJob)

                {{--                                    <header>--}}
                {{--                                        <h2>Due Date - {{Date::dmY($maintenanceJob->notice_date)}}</h2>--}}
                {{--                                        <ul class="data-header-actions">--}}
                {{--                                            @if(!$read_only)--}}
                {{--                                                <li>--}}
                {{--                                                    <a href="{{ route('maintenances.edit', $maintenanceJob) }}"--}}
                {{--                                                       class="btn btn-warning btn-alt maintenanceModal"--}}
                {{--                                                       id="editmaintenanceModal">Edit</a>--}}
                {{--                                                </li>--}}
                {{--                                                <li>--}}
                {{--                                                    <a class="modalButton btn btn-alt btn-primary maintenance-photo-modal" data-toggle="modal" href="#photoModal">Add Photo</a>--}}
                {{--                                                    {{ Form::hidden('maintenance-id', $maintenanceJob->id) }}--}}
                {{--                                                </li>--}}
                {{--                                            @endif--}}
                {{--                                        </ul>--}}
                {{--                                    </header>--}}
                @include('properties.includes.maintenances.inner_section')
            @endforeach
        </div>
    </div>
</div>

