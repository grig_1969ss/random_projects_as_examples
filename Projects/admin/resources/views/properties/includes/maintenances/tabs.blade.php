<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($maintenance as $key => $maintenanceJob)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-maintenance-{{$maintenanceJob->id}}-tab" data-toggle="tab"
               href="#nav-maintenance-{{$maintenanceJob->id}}" role="tab"
               aria-controls="nav-maintenance-{{$maintenanceJob->id}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >Due Date {{Date::dmY($maintenanceJob->notice_date)}}</a>
        @endforeach
    </div>
</nav>
