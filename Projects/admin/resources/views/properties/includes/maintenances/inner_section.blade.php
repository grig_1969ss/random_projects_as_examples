<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-maintenance-{{$maintenanceJob->id}}" role="tabpanel"
     aria-labelledby="nav-maintenance-{{$maintenanceJob->id}}-tab">


    <div class="col-12 mb-4 d-flex justify-content-between">
        <div class="dropdown">
            <div class="btn-group mr-2 mb-2">
                <a href="{{route('maintenances.edit',$maintenanceJob->id)}}?property={{$propertyID}}"
                   class="btn add-property btn-outline-primary">Edit</a>
            </div>
        </div>
    </div>

    <div class="row">

        {{--        <div class="maintenance-photo-list-{{$maintenanceJob->id}}">--}}
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Details</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($maintenanceJob->notes) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Resolved</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Form::checkbox('resolved', '1', $maintenanceJob->resolved, array('disabled' => 'disabled')) }}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">
                    </small>
                </div>
                <div class="col-sm-6 pl-0">
                    <?php $i = 1; ?>
                    @foreach($maintenanceJob->maintenancePhotos as $photo)
                        @include('partials.maintenance-photo-item')
                        <?php $i++; ?>
                    @endforeach
                </div>
            </div>
        </div>

        {{--        </div>--}}
    </div>
</div>


{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.maintenances.documents.create', $maintenanceJob),--}}
{{--    'documents' => $maintenanceJob->documents--}}
{{--])--}}
