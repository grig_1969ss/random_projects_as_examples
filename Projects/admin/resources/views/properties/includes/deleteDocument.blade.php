<div class="modal fade" id="modal-delete-document" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Delete the document</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{route('docs-delete')}}" method="post">
                @csrf
                <input type="hidden" name="deleting_document" value="">
                <div class="modal-body">
                    <p>Are you sure you want to delete this document?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-primary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-sm btn-warning ml-auto">Yes</button>
                </div>
            </form>

        </div>
    </div>
</div>
