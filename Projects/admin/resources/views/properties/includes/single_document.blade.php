<p>
    {{ Date::dmYHi($document->created_at)}} - <strong>{{ $document->present()->createdBy }}</strong> -
    <code>{{$document->typeName}}</code>
</p>
<p>
    <a href="{{ route('documents.show', $document) }}" target="_blank">{{ $document->document_name }}</a>&nbsp;
    <a href="{{ route('documents.show', $document) }}" target="_blank" class="btn btn-alt btn-primary">Open</a>
    @if ($read_only == 0)
        <a class="modalButton btn btn-alt btn-danger delete-document-link"
           data-url="{{ route('documents.destroy', $document->id) }}">Delete</a>
    @endif
</p>