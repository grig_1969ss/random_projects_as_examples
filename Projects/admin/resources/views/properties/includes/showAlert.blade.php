@foreach($alerts as $alert)
    @if($alert->object_type_id == $type)
        <div class="">
            <div class="">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="fas fa-exclamation-circle"></i></span>
                    <span class="alert-inner--text">{{$alert->message}}</span>
                    <button data-id="{{$alert->id}}" type="button" class="close close-delete-alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <i class="fas fa-spinner sr-only fa-spin"></i>
                    </button>
                </div>
            </div>
        </div>
    @endif
@endforeach
