<div class="modal fade" id="modal-reminder" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Alert</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id='error-message'></div>
            <form action="{{route('diary.store')}}" method="post" id="add-diary">
                @csrf
                <input type="hidden" name="property" value="{{request()->segment(2)}}">
                <div class="modal-body">

                    <div id="note_category" class="control-group">
                        @include('documents.includes.attachToDropdown')
                        <span id="note_category" class="help-block"></span>
                    </div>

                    <div id="datetime" class="form-group control-group mt-3 reminder-datetimepicker">
                        <label for="datetimepicker2">Due Date</label>
                        <div id="datetimepicker2" class="input-append">
                            <input name="datetime" type="text" class="form-control"
                                   id="datetimepicker2" autocomplete="off" data-toggle=""
                                   data-target="#datetimepicker2"/>
                        </div>
                        <span id="datetime" class="help-block"></span>
                    </div>

                    <div id="alert_comments" class="form-group control-group">
                        <label for="alert-comments">Comments</label>
                        <textarea class="form-control" name="alert_comments" id="alert-comments" cols="10"
                                  rows="3"></textarea>
                        <span id="alert_comments" class="help-block"></span>
                    </div>

                </div>

                @include('partials.modal_footer', array('buttonName' => 'Save'))

            </form>
        </div>
    </div>
</div>
