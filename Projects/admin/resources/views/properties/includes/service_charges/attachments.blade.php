<div class="form-group control-group attachments" id="attachments">
    <label for="attachments">Attachment</label>
    <div>
        <select class="form-control select2-email" name="attachments[]" id="attachments" multiple="multiple">
            @foreach($attachmentsDropdown as $model => $attachments)
                <optgroup label="{{$model}}">
                    @foreach($attachments as $attachment)
                        @foreach($attachment as $docId => $document)
                            <option value="{{$docId}}">{{$document}}</option>
                        @endforeach
                    @endforeach
                </optgroup>
            @endforeach
        </select>
    </div>
    <span id="attachments" class="help-block"></span>
</div>

<input type="hidden" name="attachment_ids" value="">
