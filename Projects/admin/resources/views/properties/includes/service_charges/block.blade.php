<div class="row">

@if($property->has_service_charge)

    {{--                <header>--}}
    {{--                    <h2>Service Charge</h2>--}}
    {{--                    <ul class="data-header-actions">--}}
    {{--                        @if(!$read_only)--}}
    {{--                            <li>--}}
    {{--                                <a class="btn btn-alt"--}}
    {{--                                   href="{{ route('properties.service-charges.create', $property) }}">--}}
    {{--                                    New Service Charge--}}
    {{--                                </a>--}}
    {{--                            </li>--}}
    {{--                        @endif--}}
    {{--                    </ul>--}}
    {{--                </header>--}}

        <div class="col-md-12">
            @include('properties.includes.service_charges.tabs')

            <div class="tab-content" id="nav-tabContent">
                @if(!$serviceCharges)
                    No Service Charge found.
                @endif
                @foreach($serviceCharges as $key => $charge)
                    {{--                                         <header>--}}
                    {{--                                            <h2>Service Charge - Y/E {{Date::Y($serviceCharge->year_end)}}</h2>--}}
                    {{--                                            <ul class="data-header-actions">--}}
                    {{--                                                @if(!$read_only)--}}
                    {{--                                                    <li>--}}
                    {{--                                                        <a class="btn btn-warning btn-alt"--}}
                    {{--                                                           href="{{ route('service-charges.edit', $serviceCharge) }}">--}}
                    {{--                                                            Edit Y/E {{Date::Y($serviceCharge->year_end)}}--}}
                    {{--                                                        </a>--}}
                    {{--                                                    </li>--}}
                    {{--                                                    <li>--}}
                    {{--                                                        <button class="btn btn-danger btn-alt delete-btn"--}}
                    {{--                                                                data-form-class="delete-service-charge-form">--}}
                    {{--                                                            Delete--}}
                    {{--                                                        </button>--}}

                    {{--                                                        <form action="{{ route('service-charges.destroy', $serviceCharge) }}"--}}
                    {{--                                                              method="POST"--}}
                    {{--                                                              class="delete-service-charge-form">--}}
                    {{--                                                            @csrf--}}
                    {{--                                                            @method('DELETE')--}}
                    {{--                                                        </form>--}}
                    {{--                                                    </li>--}}
                    {{--                                                @endif--}}
                    {{--                                            </ul>--}}
                    {{--                                        </header>--}}

                    @include('properties.includes.service_charges.inner_section')
                @endforeach
            </div>
        </div>

@endif

</div>
