<div class="tab-pane fade  @if($key == 0) active show @endif"
     id="nav-charge-{{Date::Y($charge->year_end)}}" role="tabpanel"
     aria-labelledby="nav-charge-{{Date::Y($charge->year_end)}}-tab">
    <div class="row">

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">Status</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($charge->serviceChargeStatus->name) }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Actual
                        vs Budget</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::actualVsBudget($charge->actual_year_end, $charge->budget_year_end) }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Method
                        of Apportion</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ $charge->apportionmentType ? Util::showValue($charge->apportionmentType->name):'-' }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Percentage
                        Apportion</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($charge->percentage_apportion) }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Exclusions?</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        <input type="checkbox" disabled
                               @if($charge->exclusions) checked @endif>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Charge
                        Details</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {{ Util::showValue($charge->notes) }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Y/E
                        Cost SqFt</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $charge->present()->formatNumber($charge->year_end_cost_per_sq_ft) !!}</p>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Budget
                        Differential %</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($charge->service_charge_budget_differential) }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Budgeta
                        Y/E</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $charge->present()->formatNumber($charge->budget_year_end) !!}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Actual
                        Y/E</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $charge->present()->actualPayment !!}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">WIP
                        Amount</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $charge->present()->formatNumber($charge->wip_amount) !!} </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Qrt
                        Payment (Budget)</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0"> {!! $charge->present()->formatNumber($charge->budget_quarterly_payment) !!}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Qrt
                        Payment (Actual)</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0"> {!! $charge->present()->formatNumber($charge->actual_quarterly_payment) !!}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Balancing
                        S/C</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $charge->present()->formatNumber($charge->balance_service_charge) !!}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Square
                        Footage</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Annual
                        Budget {{Date::Y($charge->year_end)}}</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{!! $charge->present()->formatNumber($charge->annual_budget) !!}</p>
                </div>
            </div>
        </div>
        @foreach($charge->getPreviousServiceCharges() as $previousServiceCharge)
            <div class="col-md-4">
                <div class="row pt-3 align-items-center">
                    <div class="col-sm-5">
                        <small class="text-uppercase text-muted">Annual
                            Budget {{Date::Y($previousServiceCharge->year_end)}}</small>
                    </div>
                    <div class="col-sm-6 pl-0">
                        <p class="mb-0">{!! $previousServiceCharge->present()->formatNumber($previousServiceCharge->annual_budget) !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">
                        Invoiced?
                    </small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        <input type="checkbox" disabled
                               @if($charge->invoiced) checked @endif>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">
                        Amount Invoiced
                    </small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        {!! $charge->present()->formatNumber($charge->amount_invoiced) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">
                        VAT?
                    </small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">
                        <input type="checkbox" disabled
                               @if($charge->vat) checked @endif>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-6">
                    <small class="text-uppercase text-muted">
                        Current Contribution
                    </small>
                </div>
                <div class="col-sm-5 pl-0">
                    <p class="mb-0">
                        {!! $charge->present()->formatNumber($charge->current_contribution) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-6">
                    <small class="text-uppercase text-muted">
                        Forcast Contribution Y/E +1
                    </small>
                </div>
                <div class="col-sm-5 pl-0">
                    <p class="mb-0">
                        {!! $charge->present()->formatNumber($charge->forcast_contribution_year1) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-6">
                    <small class="text-uppercase text-muted">
                        Forcast Contribution Y/E +2
                    </small>
                </div>
                <div class="col-sm-5 pl-0">
                    <p class="mb-0">
                        {!! $charge->present()->formatNumber($charge->forcast_contribution_year2) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-6">
                    <small class="text-uppercase text-muted">
                        Forcast Y/E +1
                    </small>
                </div>
                <div class="col-sm-5 pl-0">
                    <p class="mb-0">
                        {!! $charge->present()->formatNumber($charge->forcast_turnover_year1) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-6">
                    <small class="text-uppercase text-muted">
                        Forcast Y/E +2
                    </small>
                </div>
                <div class="col-sm-5 pl-0">
                    <p class="mb-0">
                        {!! $charge->present()->formatNumber($charge->forcast_turnover_year2) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Charge
                        Details</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($charge->charge_details) }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small class="text-uppercase text-muted">Lease
                        Comments</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($property->lease_comments) }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row pt-3 align-items-center">
                <div class="col-sm-5">
                    <small
                        class="text-uppercase text-muted">WIP</small>
                </div>
                <div class="col-sm-6 pl-0">
                    <p class="mb-0">{{ Util::showValue($charge->wip) }}</p>
                </div>
            </div>
        </div>


    </div>
</div>


{{--@include('partials.documents_block', [--}}
{{--    'url' => route('ajax.service-charges.documents.create', $charge),--}}
{{--    'documents' => $charge->documents--}}
{{--])--}}
