<nav>
    <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
        @foreach($serviceCharges as $key => $charge)
            <a class="nav-item nav-link @if($key == 0) active @endif"
               id="nav-charge-{{Date::Y($charge->year_end)}}-tab" data-toggle="tab"
               href="#nav-charge-{{Date::Y($charge->year_end)}}" role="tab"
               aria-controls="nav-charge-{{Date::Y($charge->year_end)}}"
               @if($key == 0) aria-selected="true"
               @else aria-selected="false" @endif >Y/E {{Date::Y($charge->year_end)}}</a>
        @endforeach
    </div>
</nav>
