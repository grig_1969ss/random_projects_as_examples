@include('properties.includes.showAlert',['type'=>8])

<div class="row">
    <div class="col-md-12">
        <div class="mb-5">
            <table class="table">
                <tbody>
                @foreach($serviceCharges as $key => $chargeRow)
                    @continue($key === 'yearsExists')
                    <tr @if(in_array($chargeRow[0],['Quarterly Payment'])) class="merge-rows" @endif>
                        @foreach($chargeRow as $keyRow => $column)
                            <td @if($keyRow == 0) class="bolder-font" @endif data-key="{{$keyRow}}">
                                @if($key =='yearRow' && $keyRow)
                                    @if(!auth()->user()->hasRole('client'))
                                        <div class="btn-group">
                                            <button type="button"
                                                    class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-angle-down dropdown-arrow"></i>
                                                <span>Actions</span>
                                            </button>
                                            <div class="dropdown-menu" id="action-dropdown" x-placement="top-start">
                                                <a class="dropdown-item btn" href="{{$column['updateUrl']}}">Edit</a>
                                                <button type="button" class="dropdown-item services-charge-add-note"
                                                        data-object_id="{!! $column['value'] !!}"
                                                        data-toggle="modal" data-selected="2" data-object_type_id="8"
                                                        data-target="#noteModal">Add
                                                    Note
                                                </button>
                                                <button type="button" class="dropdown-item add-re-diarise"
                                                        data-object_id="{!! $column['value'] !!}"
                                                        data-model="ServiceCharge"
                                                        data-selected="2" data-object_type_id="8"
                                                        data-object_type="service_charges"
                                                        data-chosen_date="{!! $column['diaryDate'] !!}"
                                                >Re Diarise
                                                </button>
                                                <button type="button" class="dropdown-item add-alert"
                                                        data-object-type="8"
                                                        data-selected="{!! $column['value'] !!}"
                                                        data-id="note_type_dropdown">
                                                    Add Alert
                                                </button>
                                                <button type="button"
                                                        class="dropdown-item services-charge attach-budget"
                                                        data-toggle="modal" data-selected="{!! $column['value'] !!}"
                                                        data-target="#documentModal" data-id="note_type_dropdown">Attach
                                                    Document
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                                    <br>
                                    <span><a href="#"> {!! $column['label'] !!} </a></span>
                                @elseif(in_array($key, ['budgetDetailsRecdRow', 'annualReconciliationRecdRow']))
                                    @if(!is_array($column))
                                        {{$column}}
                                    @else
                                        @foreach($column as $recKey => $rec)

                                            @if(
                                                  (Arr::get($column, 'Budget Details Recd') == 'N' || Arr::get($column, 'Annual Reconciliation Recd') == 'N' )
                                                  && !in_array($recKey,['Budget Details Recd','Annual Reconciliation Recd'])
                                             )
                                                <i class="fas fa-circle icon-gray" data-toggle="tooltip"
                                                   data-placement="top" data-original-title="{{$recKey}}"></i>
                                            @else
                                                @if($rec == 'Y')
                                                    <i class="fas fa-circle icon-success" data-toggle="tooltip"
                                                       data-placement="top" data-original-title="{{$recKey}}"></i>
                                                @endif
                                                @if($rec == 'N')
                                                    <a
                                                        @if(!in_array($recKey,['Budget Details Recd','Annual Reconciliation Recd']))
                                                        class="email-template" data-tab_type="services-charge-add-note"
                                                        data-type="{{$recKey}}"
                                                        data-toggle="modal" data-target="#modal-new-email"
                                                        data-model="ServiceCharge"
                                                        @if($key == 'budgetDetailsRecdRow')
                                                        data-template-id="{{$recKey == 'Real Estate Updated'?  config('services.email_templates.sc_realestate_budget_type_id') : config('services.email_templates.sc_franchisee_budget_type_id')}}"
                                                        @else
                                                        @if($recKey == 'Franchisee Updated')
                                                        data-template-id="{{config('services.email_templates.sc_franchisee_rec_type_id')}}"
                                                        @endif
                                                        @endif

                                                        @else href="#"
                                                        @endif >
                                                        <i class="fas fa-circle icon-danger" data-toggle="tooltip"
                                                           data-placement="top" data-original-title="{{$recKey}}"></i>
                                                    </a>
                                                @endif
                                            @endif

                                        @endforeach
                                    @endif
                                @else
                                    {!! $column !!}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>

