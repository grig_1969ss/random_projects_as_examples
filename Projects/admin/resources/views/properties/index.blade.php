@extends('layouts_new.app', ['pageTitle' => 'Properties', 'pageNav' => 'property', 'page' => 'PropertiesIndex'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            @include('partials.show_properties', [
                'id' => null,
                'type'=> null,
                'showClients' => true,
                'header' => 'Client'
            ])
        </div>
    </div>
@endsection
