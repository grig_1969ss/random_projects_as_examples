@extends('layouts_new.app', ['pageTitle' => 'Properties', 'pageNav' => 'property', 'page' => 'PropertiesIndex'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
        {{--            <div class="row">--}}
        {{--                <div class="col-12 mb-4 d-flex justify-content-between">--}}
        {{--                    <div>--}}
        {{--                        <h5 class="font-weight-bold">Properties</h5>--}}
        {{--                    </div>--}}
        {{--                    <div class="dropdown">--}}
        {{--                        <div class="btn-group mr-2 mb-2">--}}
        {{--                            <a class="btn add-property btn-outline-primary">Add Property</a>--}}
        {{--                        </div>--}}
        {{--                        <div class="btn-group mr-2 mb-2">--}}
        {{--                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">--}}
        {{--                                <option>All properties</option>--}}
        {{--                                <option>Active</option>--}}
        {{--                                <option>Disposed</option>--}}
        {{--                            </select>--}}
        {{--                        </div>--}}

        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-responsive table-overview">
                            <thead>
                            <tr class="sections">
                                <th></th>
                                <th colspan="5">Service Charge</th>
                                <th colspan="5">Insurances</th>
                                <th colspan="5">Approvals</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                @for($i = 0; $i < 3; $i++)
                                    @foreach($years as $key => $year)
                                        <td>{{$year}}</td>
                                        @if(($key + 1) %4 == 0 && $key !=0 && $i !=2)
                                            <td></td>
                                        @endif
                                    @endforeach
                                @endfor
                            </tr>
                            @foreach($rows as $row)
                                <tr>
                                    @foreach($row as $key => $value)
                                        <td @if($value === 'Y') class="green-td"
                                            @elseif($value === 'N' && $key != 0) class="red-td" @endif >
                                            {{$value}}</td>

                                        @if($key %4 == 0 && $key !=0 && $key !=12)
                                            <td></td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        <nav class="property-list-pagination" aria-label="Page navigation example">
                            <span>Showing {{$properties->firstItem()}} to {{$properties->lastItem()}} of {{$properties->total()}} entries</span>
                            {{ $properties ->links()  }}
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
