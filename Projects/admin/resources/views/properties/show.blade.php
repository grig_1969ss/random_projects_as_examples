@extends('layouts_new.app', ['pageTitle' => 'Property', 'pageNav' => 'property', 'page' => 'PropertiesDetail'])
@push('css')
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
@endpush
@section('container')

    @php
        $propertyID = Util::propertyId();
    @endphp


    @if($errors->any())
        @foreach($errors as $error)
            <div>{{$error->message}}</div>
        @endforeach
    @endif

    <div class="section section-sm">
        <div id="template-page" class="container-fluid">
            <div class="col-12 mb-4 d-flex justify-content-between">
                <div>
                    <h5 class="font-weight-bold">{{ $property->property_name }}</h5>
                </div>
                <div class="d-none d-md-block">
                    @if(!auth()->user()->hasRole('client'))
                        <a href="{{route('properties.edit', $property->id)}}"
                           class="btn btn-sm btn-danger animate-hover mr-3">Edit Property</a>
                        <button class="btn btn-sm btn-primary animate-hover mr-3">Upload</button>
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-angle-down dropdown-arrow"></i>
                                <span>Actions</span>
                            </button>

                            <div class="dropdown-menu" id="action-dropdown" x-placement="bottom-start"
                                 style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 31px, 0px);">
                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_service_charge)
                                    <a class="dropdown-item"
                                       href="{{route('properties.service-charges.create', $property->id)}}">Add Service
                                        Charge</a>
                                @endif

                                <a class="dropdown-item"
                                   href="{{route('properties.insurances.create', $property->id)}}">Add Insurance</a>

                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_approvals)
                                    <a class="dropdown-item"
                                       href="{{route('properties.payments.create', $property->id)}}">
                                        Add Approval</a>
                                @endif

                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_utilities)
                                    <a class="dropdown-item"
                                       href="{{route('properties.utilities.create', $property->id)}}">
                                        Add Utilities</a>
                                @endif

                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_rent_reviews)
                                    <a href="{{route('properties.rent-reviews.create', $property->id)}}"
                                       class="dropdown-item">Add Rent Review</a>
                                @endif
                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_client_queries)
                                    <a href="{{route('properties.client-queries.create', $property->id)}}"
                                       class="dropdown-item">Add Client Queries</a>
                                @endif
                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_maintenance)
                                    <a href="{{route('properties.maintenances.create', $property->id)}}"
                                       class="dropdown-item">Add Maintenance</a>
                                @endif
                                @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_break_options)
                                    <a href="{{route('properties.breaks.create', $property->id)}}"
                                       class="dropdown-item">Add Break Option</a>
                                @endif
                                <a href="{{route('properties.breaks.create', $property->id)}}" data-object_type_id="1"
                                   data-toggle="modal" data-target="#noteModal" data-selected="1"
                                   class="dropdown-item property-add-note">Add Note</a>
                                <div class="dropdown-divider"></div>
                                <a href="#" data-object-type="1" data-selected="{{$property->id}}"
                                   class="dropdown-item add-alert">Add Alert</a>
                                <a href="#" data-toggle="modal" data-target="#modal-reminder"
                                   class="dropdown-item">Add Reminder</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('properties.duplicate', $property->id) }}">Duplicate
                                    Property</a>
                                <a id="disposeProperty" class="dropdown-item" href="#">Dispose Property</a>

                            </div>
                        </div>
                    @endif
                </div>
            </div>

            @include('properties.includes.modal-reminder')

            <div class="row ml-0 mr-0">
                <div class="col-md-12">
                    <!-- Tab -->
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                               role="tab" aria-controls="nav-home" aria-selected="true">Property</a>
                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_service_charge)
                                <a class="nav-item nav-link" id="nav-service-tab" data-toggle="tab" href="#nav-service"
                                   role="tab" aria-controls="nav-service" aria-selected="true">Service Charge</a>
                            @endif

                            <a class="nav-item nav-link" id="nav-insurance-tab" data-toggle="tab"
                               href="#nav-insurance"
                               role="tab" aria-controls="nav-insurance" aria-selected="true">Insurance</a>

                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_approvals)
                                <a class="nav-item nav-link" id="nav-approval-tab" data-toggle="tab"
                                   href="#nav-approval"
                                   role="tab" aria-controls="nav-approval" aria-selected="true">Approvals</a>
                            @endif

                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_utilities)
                                <a class="nav-item nav-link" id="nav-utilities-tab" data-toggle="tab"
                                   href="#nav-utilities"
                                   role="tab" aria-controls="nav-utilities" aria-selected="true">Utilities</a>
                            @endif

                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_maintenance)
                                <a class="nav-item nav-link" id="nav-maintenance-tab" data-toggle="tab"
                                   href="#nav-maintenance"
                                   role="tab" aria-controls="nav-maintenance" aria-selected="true">Maintenance</a>
                            @endif

                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_rent_reviews)
                                <a class="nav-item nav-link" id="nav-rent-tab" data-toggle="tab" href="#nav-rent"
                                   role="tab"
                                   aria-controls="nav-rent" aria-selected="false">Rent</a>
                            @endif

                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_lease)
                                <a class="nav-item nav-link" id="nav-lease-tab" data-toggle="tab" href="#nav-lease"
                                   role="tab" aria-controls="nav-lease" aria-selected="false">Lease</a>
                            @endif

                            <a class="nav-item nav-link" id="nav-documents-tab" data-toggle="tab"
                               href="#nav-documents"
                               role="tab" aria-controls="nav-documents" aria-selected="false">Documents</a>
                            @if(!auth()->user()->hasRole('client'))
                                <a class="nav-item nav-link" id="nav-emails-tab" data-toggle="tab" href="#nav-emails"
                                   role="tab" aria-controls="nav-emails" aria-selected="false">Emails</a>
                            @endif
                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_rent_reviews)
                                <a class="nav-item nav-link" id="nav-rent_reviews-tab" data-toggle="tab" href="#nav-rent_reviews"
                                   role="tab" aria-controls="nav-rent_reviews" aria-selected="false">Rent Review</a>
                            @endif
                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_break_options)
                                <a class="nav-item nav-link" id="nav-break_options-tab" data-toggle="tab" href="#nav-break_options"
                                   role="tab" aria-controls="nav-break_options" aria-selected="false">Break Options</a>
                            @endif
                            @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_client_queries)
                                <a class="nav-item nav-link" id="nav-client_queries-tab" data-toggle="tab" href="#nav-client_queries"
                                   role="tab" aria-controls="nav-client_queries" aria-selected="false">Client Query</a>
                            @endif
                        </div>
                    </nav>
                    <div class="tab-content pt-3 pl-3 pr-3" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                             aria-labelledby="nav-home-tab">

                            @include('properties.includes.showAlert',['type'=>1])

                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="row pt-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Property Address</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ Util::showValue($property->property_name) }}</p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Property Ref</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ Util::showValue($property->reference) }}</p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Property Type</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->propertytypes ? Util::showValue($property->propertytypes->name):'-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Address</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">
                                                {{ Util::showValue($property->address->address_1) }}<br>
                                                {{ Util::showValue($property->address->address_2) }}<br>
                                                {{ Util::showValue($property->address->town) }}<br>
                                                {{ Util::showValue($property->address->county) }}<br>
                                                {{ Util::showValue($property->address->post_code) }}
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">CAPA Main Contact</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">
                                                {{$property->capaOwner->name ?? '-'}}
                                                @if(isset($property->capaOwner->email))
                                                    <i data-email="{{$property->capaOwner->email}}"
                                                       class="fa fa-envelope clickable-name">
                                                    </i>
                                                @endif
                                            </p>

                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Client Main Contact</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">
                                                {{$property->clientOwner->name ?? '-'}}
                                                @if(isset($property->clientOwner->email))
                                                    <i data-email="{{$property->clientOwner->email}}"
                                                       class="fa fa-envelope clickable-name">
                                                    </i>
                                                @endif
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row pt-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Region Name</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->region ? Util::showValue($property->region->name):'-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Region Manager</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->region ? Util::showValue($property->region->manager):'-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">General Info</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ Util::showText($property->general_info) }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Inside Landlord &amp;
                                                Tenant Act
                                            </small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ \App\Models\Property::getInsideLandlord($property->inside_landlord) }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Floor Area Net Trading</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->floor_area_net_trading ? Util::showValue($property->floor_area_net_trading):'-' }}
                                                Sq Ft</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Tenure</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->tenureType ? Util::showValue($property->tenureType->name):'-' }}</p>
                                        </div>
                                    </div>

                                    @include('partials.show_notes', [
                                                  'id' => $property->id, 'notes' => $notes, 'read_only' => $read_only,
                                                  'type' => 'General', 'filters' => $serviceCharges['yearRow']
                                             ])

                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Client Surveyor</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->clientSurveyor->name ?? '-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Client Admin</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->clientAdmin->name ?? '-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Capa Admin</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->capaAdmin->name ?? '-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Capa Admin 2</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->capaAdmin2->name ?? '-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Store Contact</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ Util::showValue($property->store_contact) }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Store Telephone</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ Util::showValue($property->store_telephone) }}</p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Client Name</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            @if($property->client)
                                                <p class="mb-0">
                                                    <a href="/clients/{{ $property->client_id }}">
                                                        {{ Util::showValue($property->client->client_name) }}
                                                    </a>
                                                </p>
                                            @else
                                                <p class="mb-0"> - </p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Franchisee</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">

                                            <p class="mb-0">
                                                {{$property->users->first()->name ?? '-'}}
                                                @if(isset($property->users->first()->email))
                                                    <i data-email="{{$property->users->first()->email}}"
                                                       class="fa fa-envelope clickable-name"></i>
                                                @endif
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Managing Agent</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            @if($property->agent)
                                                <p class="mb-0">
                                                    <a href="/agents/{{ $property->agent_id }}">
                                                        {{ Util::showValue($property->agent->agent_name) }}
                                                    </a>
                                                    @if($property->agent->email)
                                                        <i data-email="{{$property->agent->email}}"
                                                           class="fa fa-envelope clickable-name">
                                                        </i>
                                                    @endif
                                                </p>
                                            @else
                                                <p class="mb-0"> - </p>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Managing Agent Contact</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            @if($property->agentContact)
                                                <p class="mb-0">
                                                    <a href="{{ route('contacts.show', $property->agentContact) }}">
                                                        {{ $property->agentContact->present()->nameEmail }}
                                                    </a>
                                                    @if($property->agentContact->email)
                                                        <i data-email="{{$property->agentContact->email}}"
                                                           class="fa fa-envelope clickable-name">
                                                        </i>
                                                    @endif
                                                </p>
                                            @else
                                                <p class="mb-0">-</p>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Landlord</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            @if($property->landlord)
                                                <p class="mb-0">
                                                    <a href="/landlords/{{ $property->landlord_id }}">
                                                        {{ Util::showValue($property->landlord->landlord_name) }}
                                                    </a>
                                                    @if($property->landlord->email)
                                                        <i data-email="{{$property->landlord->email}}"
                                                           class="fa fa-envelope clickable-name">
                                                        </i>
                                                    @endif
                                                </p>
                                            @else
                                                <p class="mb-0">-</p>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="row py-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted" landlord="" contact=""></small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">-</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_service_charge)
                            <div class="tab-pane fade" id="nav-service" role="tabpanel"
                                 aria-labelledby="nav-service-tab">
                                @if($serviceCharges)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-pane fade show active" id="tab-14" role="tabpanel"
                                                 aria-labelledby="tab-14">
                                                @include('properties.includes.service_charges.serviceChargeTable')
                                            </div>
                                        </div>
                                    </div>

                                    @if(!auth()->user()->hasRole('client'))
                                        <div class="form-group">
                                            <label for="">Filter</label>
                                            <select data-replace-with="sc-note-replace" data-type="Service Charge"
                                                    class="form-control note-filter">
                                                <option selected data-property="{{$property->id}}" value="all">All Notes
                                                </option>
                                                @foreach(Arr::except($serviceCharges['yearRow'],[0]) as $filter)
                                                    <option value="{{$filter['value']}}">{{$filter['label']}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div id="sc-note-replace" class="">
                                            @include('partials.show_note_section', ['type' => 'Service Charge'])
                                        </div>
                                    @endif


                                @else
                                    <p>No service charges exist, please add one to use this tab</p>
                                @endif

                            </div>
                        @endif

                        <div class="tab-pane fade" id="nav-insurance" role="tabpanel"
                             aria-labelledby="nav-insurance-tab">

                            @if(!$property->insurances->isEmpty())
                                @include('properties.includes.showAlert',['type'=>9])
                                <div class="row">

                                    @include('properties.includes.insurances.block')

                                </div>

                                @if(!auth()->user()->hasRole('client'))
                                    <div class="form-group">
                                        <label for="">Filter</label>
                                        <select data-replace-with="insurance-note-replace" data-type="Insurance"
                                                class="form-control note-filter">
                                            <option selected data-property="{{$property->id}}" value="all">All Notes
                                            </option>
                                            @foreach(Arr::except($insurances['yearRow'],[0]) as $filter)
                                                <option value="{{$filter['value']}}">{{$filter['label']}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div id="insurance-note-replace">
                                        @include('partials.show_note_section',
                                                ['type' => 'Insurance'])
                                    </div>
                                @endif
                            @else
                                <p>No insurances exist, please add one to use this tab</p>
                            @endif

                        </div>

                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_approvals)
                            <div class="tab-pane fade" id="nav-approval" role="tabpanel"
                                 aria-labelledby="nav-approval-tab">
                                @if(!empty($payments))
                                    <div class="row">
                                        @include('properties.includes.payments.block')
                                    </div>
                                @else
                                    <p>No approvals exist, please add one to use this tab</p>
                                @endif
                            </div>
                        @endif


                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_utilities)
                            <div class="tab-pane fade" id="nav-utilities" role="tabpanel"
                                 aria-labelledby="nav-utilities-tab">
                                @if(!empty($utilities))
                                    <div class="row">
                                        @include('utilities.includes.table',['utilities' => $utilities])
                                    </div>

                                    @if(!auth()->user()->hasRole('client'))
                                        <div class="form-group">
                                            <label for="">Filter</label>
                                            <select data-replace-with="utility-note-replace" data-type="Utility"
                                                    class="form-control note-filter">
                                                <option selected data-property="{{$property->id}}" value="all">All Notes
                                                </option>
                                                @foreach($utilities['rows'] as $filter)
                                                    <option value="{{$filter[0]}}">Period: {{$filter[1]}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div id="utility-note-replace">
                                            @include('partials.show_note_section',
                                                    ['type' => 'Utility'])
                                        </div>
                                    @endif

                                @else
                                    <p>No utilities exist, please add one to use this tab</p>
                                @endif
                            </div>
                        @endif

                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_maintenance)
                            <div class="tab-pane fade" id="nav-maintenance" role="tabpanel"
                                 aria-labelledby="nav-maintenance-tab">
                                @if($property->has_maintenances)
                                    <div class="row">
                                        @include('properties.includes.maintenances.block')
                                    </div>
                                @else
                                    <p>No maintenances exist, please add one to use this tab</p>
                                @endif
                            </div>
                        @endif

                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_rent_reviews)
                            <div class="tab-pane fade show" id="nav-rent" role="tabpanel"
                                 aria-labelledby="nav-rent-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row pt-3 align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Completion Date</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->purchase_date) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Rent Commencement Date</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->commencement_date) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Current Rent</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0"> <?php $curRent = (isset($currentRent->rent)) ? $currentRent->rent : null; ?></p>
                                            </div>
                                        </div>

                                        <div class="row pt-2 align-items-center">
                                            <div class="col-sm-3">
                                                <small
                                                    class="text-uppercase text-muted">{{ $property->rentPaymentFrequency ? Util::showValue($property->rentPaymentFrequency->name) : '-' }}</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">-</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Rent Payment Frequency
                                                    Notes
                                                </small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->payment_frequency) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Rent Payment Dates</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->payment_dates) }} </p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Deposit</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{!! $property->present()->formatNumber($property->deposit) !!}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Service Charge</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                @if($property->has_service_charge == 1)
                                                    <p class="mb-0">Yes</p>
                                                @else
                                                    <p class="mb-0">No</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Lease Start</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->lease_start_date) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Expiry Date</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->lease_expiry_date) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted">Rent Review Date</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->rent_review_date) }}</p>
                                            </div>
                                        </div>


                                    </div>
                                    <table id="rent-table" class="datatable table table-bordered dataTable"
                                           aria-describedby="example-2_info">
                                        <thead>
                                        <tr>
                                            <th>Rent Exp Date</th>
                                            <th>Rent</th>
                                            <th>VAT</th>
                                            <th>Contracted Rent</th>
                                            @if ($read_only == 0)
                                                <th>
                                                    <a id='add-rent'
                                                       data-property-id="{{$property->id}}"
                                                       href="#rentModal"
                                                       class="modalButton btn btn-alt btn-primary"
                                                       data-url="{{ route('properties.rents.create', $property) }}">
                                                        Add Rent
                                                    </a>
                                                </th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                                        @foreach ($rents as $rent)
                                            <?php
                                            $date1 = new DateTime('now');
                                            $date2 = new DateTime($rent->exp_rent_date);
                                            $oldRent = ($date1 > $date2);
                                            ?>
                                            <tr id="rent-{{$rent->id}}" class="odd gradeX">
                                                <td>
                                                    @if ($read_only == 0)
                                                        <a class='edit-rent
                                                        <?= ($rent->id == isset($currentRent->id) ? $currentRent->id : 0) ? 'highlight' : '' ?>
                                                        <?= $oldRent ? 'grey' : '' ?>'
                                                           data-id="{{$rent->id}}"
                                                           data-property-id="{{$rent->property_id}}"
                                                           {{ $oldRent ? 'disabled=disabled' : ''}}
                                                           href="#rentModal"
                                                           data-url="{{ route('rents.edit', $rent->id) }}">
                                                            @endif
                                                            {{ Date::dmY($rent->exp_rent_date) }}
                                                            @if ($read_only == 0)
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($read_only == 0)
                                                        <a class='edit-rent <?= ($rent->id == isset($currentRent->id) ? $currentRent->id : 0) ? 'highlight' : '' ?> <?= $oldRent ? 'grey' : '' ?>'
                                                           data-id="{{$rent->id}}"
                                                           data-property-id="{{$rent->property_id}}"
                                                           {{ $oldRent ? 'disabled=disabled' : ''}}
                                                           href="#rentModal">
                                                            @endif
                                                            {!! $rent->present()->formatNumber($rent->rent) !!}
                                                        </a>
                                                </td>
                                                <td>
                                                    @if ($read_only == 0)
                                                        <a class='edit-rent <?= ($rent->id == isset($currentRent->id) ? $currentRent->id : 0) ? 'highlight' : '' ?> <?= $oldRent ? 'grey' : '' ?>'
                                                           data-id="{{$rent->id}}"
                                                           data-property-id="{{$rent->property_id}}"
                                                           {{ $oldRent ? 'disabled=disabled' : ''}}
                                                           href="#rentModal">
                                                            @endif
                                                            {{ $rent->vat == 0 ? 'No' : 'Yes' }}
                                                            @if ($read_only == 0)
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($read_only == 0)
                                                        <a class='edit-rent <?= ($rent->id == isset($currentRent->id) ? $currentRent->id : 0) ? 'highlight' : '' ?> <?= $oldRent ? 'grey' : '' ?>'
                                                           data-id="{{$rent->id}}"
                                                           data-property-id="{{$rent->property_id}}"
                                                           {{ $oldRent ? 'disabled=disabled' : ''}}
                                                           href="#rentModal">
                                                            @endif
                                                            {!! $rent->present()->formatNumber($rent->contracted_rent) !!}
                                                            @if ($read_only == 0)
                                                        </a>
                                                    @endif
                                                </td>
                                                @if(!$read_only)
                                                    <td>
                                                        <a data-url="{{route('rents.destroy', $rent->id)}}"
                                                           data-target="#modal-delete-confirm" data-toggle="modal"
                                                           class="delete-confirm-button">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        <div class="tab-pane fade show" id="nav-rates" role="tabpanel"
                             aria-labelledby="nav-home-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row pt-3 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Current Rateable Value</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{!! $property->present()->formatNumber($property->rateable_value) !!}</p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Proposed Rateable Value</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{!! $property->present()->formatNumber($property->proposed_rateable_value) !!}</p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Agreed Rateable Value</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{!! $property->present()->formatNumber($property->agreed_rateable_value) !!}</p>
                                        </div>
                                    </div>

                                    <div class="row pt-2 align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">User Type</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ $property->appealType ? Util::showValue($property->appealType->name):'-' }}</p>
                                        </div>
                                    </div>

                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <small class="text-uppercase text-muted">Notes</small>
                                        </div>
                                        <div class="col-sm-9 pl-0">
                                            <p class="mb-0">{{ Util::showValue($property->rv_notes) }}</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-rates" role="tabpanel" aria-labelledby="nav-rates-tab">
                            <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed.
                                Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident
                                chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod
                                Pinterest in do umami readymade swag.</p>
                            <p>Day handsome addition horrible sensible goodness two contempt. Evening for
                                married his account removal. Estimable me disposing of be moonlight cordially
                                curiosity.</p>
                        </div>
                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_lease)
                            <div class="tab-pane fade show" id="nav-lease" role="tabpanel"
                                 aria-labelledby="nav-lease-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Lease Notice Date</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->lease_notice_dates) }} </p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Guarantor</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->guarantor) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Lease Comments</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->lease_comments) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Owner Type</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ $property->present()->ownerType }}</p>
                                            </div>
                                        </div>

                                        <h5 class="mt-3">ALIENATION</h5>
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Assignment</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ $property->alienationType ? Util::showValue($property->alienationType->name):'-' }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Subletting</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">
                                                    {{ $property->sublettingType ? Util::showValue($property->sublettingType->name):'-' }}
                                                    <br>
                                                    {{ Util::showYes($property->concessions) }}
                                                    <br>
                                                    {{ Util::showYes($property->share) }}
                                                </p>
                                                <p>{{ Util::showValue($property->alienation_note) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-8">-</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">-</p>
                                            </div>
                                        </div>

                                        <h5>ALTERATIONS</h5>
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Non Structural</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showYes($property->non_structural) }}</p>
                                            </div>
                                        </div>

                                        <div class="row pt-2 align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Structural</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showYes($property->structural) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Landlord Consent</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showYes($property->landlord_consent) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Schedule of
                                                    Condition
                                                </small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showYes($property->schedule_condition) }}</p>
                                                <p class="mb-0">{{ Util::showValue($property->alteration_note) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-8">-</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">-</p>
                                            </div>
                                        </div>

                                        <h5>USER CLAUSE</h5>
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Restrictions</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->restrictions) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">User Type</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ $property->userType ? Util::showValue($property->userType->name):'-' }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Initial
                                                    Concessions
                                                </small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{!! $property->present()->formatNumber($property->initial_concessions) !!}</p>
                                                <p class="mb-0">{{ Util::showValue($property->initial_concessions_note) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-8">-</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">-</p>
                                            </div>
                                        </div>

                                        <h5>REPAIR</h5>
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Repair
                                                    Classification
                                                </small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ $property->repairClassificationType ?Util::showValue($property->repairClassificationType->name):'-' }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Repair Obligations</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->repair_obligations_note) }}</p>
                                            </div>
                                        </div>

                                        <h5>EXT. DECORATION</h5>
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Frequency</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->ext_frequency) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">First Due</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->ext_first_due) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Next Due</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->ext_next_due) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Final Year</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showYes($property->ext_final_year) }}</p>
                                            </div>
                                        </div>

                                        <h5>INT. DECORATION</h5>
                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Frequency</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showValue($property->int_frequency) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">First Due</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->int_first_due) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Next Due</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Date::dmY($property->int_next_due) }}</p>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-sm-3">
                                                <small class="text-uppercase text-muted ml-5">Final Year</small>
                                            </div>
                                            <div class="col-sm-9 pl-0">
                                                <p class="mb-0">{{ Util::showYes($property->int_final_year) }}</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="tab-pane fade" id="nav-documents" role="tabpanel"
                             aria-labelledby="nav-documents-tab">
                            @include('partials.add_document', ['id' => $property->id, 'type' => 'properties'])
                        </div>
                        @if(!auth()->user()->hasRole('client'))
                            <div class="tab-pane fade" id="nav-emails" role="tabpanel"
                                 aria-labelledby="nav-emails-tab">
                                @include('admin.inbox.partials.inboxContent')
                            </div>
                        @endif
                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_rent_reviews)
                            <div class="tab-pane fade" id="nav-rent_reviews" role="tabpanel"
                                 aria-labelledby="nav-rent_reviews-tab">
                                @if($property->has_rent_reviews)
                                    @include('properties.includes.rent_reviews.block')
                                @endif
                            </div>
                        @endif
                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_break_options)
                            <div class="tab-pane fade" id="nav-break_options" role="tabpanel"
                                 aria-labelledby="nav-break_options-tab">
                                @if($property->has_break_options)
                                    @include('properties.includes.break_options.block')
                                @endif
                            </div>
                        @endif
                        @if(isset(auth()->user()->client_id) && !auth()->user()->client->hide_client_queries)
                            <div class="tab-pane fade" id="nav-client_queries" role="tabpanel"
                                 aria-labelledby="nav-client_queries-tab">
                                @if($property->has_client_queries)
                                    @include('properties.includes.client_queries.block')
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="content-block" role="main">
        <div class="row">

        <!-- Render Hidden Add Notes Partial -->
        @include('partials.add_comment', ['type' => 'properties', 'noteTypes' => $noteTypes, 'route' => ['properties.notes.store', $property]])

        <!-- Render Hidden Add / Edit Payments Partial -->
        @include('partials.add_payment', array('id' => $property->id, 'type' => 'properties' ))
        @include('partials.edit_payment', array('id' => $property->id, 'type' => 'properties' ))
        <!-- Render Hidden Dispose Property Partial -->
            @include('partials.dispose_property', ['id' => $property->id, 'type' => 'properties'])
        </div>
        <!-- /Grid row -->
        <div id="clientModal" class='modal fade hide'>
            @include('partials.client_query_form', ['id' => $property->id, 'title'=>'Add Client Query','clientQuery' => $clientQueryNew,'restMethod' => 'POST'])
        </div>

        <div id='deleteDocumentForm'>
            <div class="modal fade hide" id="deleteDocumentModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3 id='type-form-header'>Delete this Document?</h3>
                </div>
                <div class="modal-body">
                    @include('partials.delete-document', ['restMethod' => 'delete', 'isDelete' => true, 'id' =>$property->id])
                </div>
            </div>

            <div id='rentForm'>
                <div class="modal fade" id="rentModal" tabindex="-1" role="dialog" aria-labelledby="modal-default"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="modal-title-default">Add Rent</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link text-danger ml-auto" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal fade hide" id="photoModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Upload Photo</h3>
                </div>
                <div class="modal-body">
                    {{ Form::open([
                        'route' => ['maintenances.photos.store', '__id__'],
                        'method' => 'POST',
                        'class' => 'form-horizontal',
                        'id' => 'upload-maintenance-photo',
                        'files' => true]) }}

                    <div id='error-message'></div>
                    <div id="photo_maintenance" class="control-group">
                        {{ Form::label('photo_maintenance', 'File', array('class' => 'control-label')) }}
                        <input id="photo_maintenance" name="photo_maintenance" type="file" style="display:none">
                        <div class="controls">
                            <input id='photo-input' name="photo_maintenance" class="input-large" type="text"
                                   onclick="$('input[id=photo_maintenance]').click();"/>
                            <a class="btn" onclick="$('input[id=photo_maintenance]').click();">Browse</a>
                            <span id="photo_maintenance" class="help-block">Select a photo to upload</span>
                        </div>
                    </div>

                    @include('partials.modal_footer', ['buttonName' => 'Upload Photo'])
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>


    @include('documents.uploadDocument')

    @include('partials.base_modal', ['showFooter' => true, 'customClass' => 'submit-btn'])
    @include('partials.delete-modal')
    @include('properties.includes.alertModal')
    @include('properties.includes.reDiarise')
    @include('properties.includes.deleteDocument')
    @include('partials.franchiseNewEmail')

@endsection

