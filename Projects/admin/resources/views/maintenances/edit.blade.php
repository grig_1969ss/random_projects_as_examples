@extends('layouts_new.app', [
    'pageTitle' => ($maintenance->id ? 'Edit' : 'Add').' Maintenance',
    'pageNav'   => 'property',
    'page'      => 'PropertyMaintenance'
])

@section('container')
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-4 order-md-1">
                <div class="mb-5 text-center">
                    <h6 class="h3">{{ $maintenance->id ? 'Edit' : 'Add' }}
                        Maintenance</h6>
                    <div class="dropdown-divider"></div>
                </div>
                @include('maintenances.includes.form', [
                    'title' => ($maintenance->id ? 'Edit' : 'Add').' Maintenance',
                    'restMethod' => ($maintenance->id ? 'PUT' : 'POST')
                ])
            </div>
        </div>
    </div>
@endsection
