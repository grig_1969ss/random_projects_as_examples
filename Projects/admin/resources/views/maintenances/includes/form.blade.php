@php
    $notice_date = ($maintenance->notice_date) ? Date::dmY($maintenance->notice_date) : null;
    $diary_date = ($maintenance->diary->date) ? Date::dmY($maintenance->diary->date) : null ;
@endphp

{{ Form::open([
        'route' => $maintenance->id ? ['maintenances.update', $maintenance] : ['properties.maintenances.store', $property],
        'method' => $restMethod,
        'class' => 'form-horizontal',
        'id' => 'save-maintenance'
    ]) }}

<div id='error-message'></div>
<div id="notice_date" class="control-group">
    {{ Form::label('notice_date', 'Due Date', array('class' => 'control-label')) }}
    <div class="form-group controls">
        <div class="input-append">
            {{ Form::text('notice_date',$notice_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
            <span class="add-on"><i class="awe-calendar"></i></span>
        </div>
        <span id="notice_date" class="help-block"></span>
    </div>
</div>
<div id="resolved" class="control-group">
    {{ Form::label('resolved', 'Resolved', array('class' => 'control-label')) }}
    <div class="controls">
        {{ Form::checkbox('resolved', '1', $maintenance->resolved)}}
        <span id="resolved" class="help-block"></span>
    </div>
</div>
<div id="notes" class="control-group">
    {{ Form::label('notes', 'Details', array('class' => 'control-label')) }}
    <div class="form-group controls">
        {{ Form::textarea('notes',$maintenance->notes, array('class' => 'form-control','rows'=> 3, 'id' => 'prependedInput')) }}
        <span id="notes" class="help-block"></span>
    </div>
</div>

@if(isset($maintenance->diary->id))
    <div id="diary_date" class="control-group">
        {{ Form::label('diary_date', 'Diary Date', array('class' => 'control-label')) }}
        <div class="form-group controls">
            <div class="input-append">
                {{ Form::text('diary_date',$diary_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                <span class="add-on"><i class="awe-calendar"></i></span>
                <span id="diary_date" class="help-block"></span>
            </div>
        </div>
    </div>
    {{ Form::hidden('diary_id', $maintenance->diary->id) }}
@else
    {{ Form::hidden('diary_date', Date::dmY(date('Y-m-d H:i:s'))) }}
@endif
<div class="form-actions">
    <button id="save-maintenance" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
        Save Changes
    </button>
</div>
{{ Form::close() }}

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
