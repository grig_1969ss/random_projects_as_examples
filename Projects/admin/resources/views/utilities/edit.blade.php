@extends('layouts_new.app', [
    'pageTitle' => ($utility->id ? 'Edit' : 'Add').' Utility',
    'pageNav' => 'property',
    'page' => 'PropertyUtility'
])

@section('container')
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-4 order-md-1">
                <div class="mb-5 text-center">
                    <h6 class="h3">{{ $title }}</h6>
                    <div class="dropdown-divider"></div>
                </div>
                <form method="POST" action="{{ $route }}" class="form-horizontal utility-form" id="create-utility">
                    {{ csrf_field() }}

                    @if($utility->id)
                        {{ method_field('PUT') }}
                    @endif

                    <div id='error-message'></div>
                    <input type="hidden" name="property_id" value="{{$utility->property_id ?? request()->segment(2)}}">

                    <div id="period" class="form-group control-group">
                        <label for="period">Period</label>
                        <div>
                            <input class="form-control" type="text" name="period" id="period"
                                   value="{{ $utility->period }}">
                        </div>
                        <span id="period" class="help-block"></span>
                    </div>

                    <div id="payment_type" class="form-group control-group">
                        <label for="payment_type">Utility Type</label>
                        <div>
                            <select name="payment_type" id="payment_type" class="form-control">
                                <option disabled selected value="">Select...</option>
                                @foreach($payments as $payment)
                                    <option @if($payment->id == $utility->payment_type) selected @endif value="{{$payment->id}}">{{$payment->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <span id="payment_type" class="help-block"></span>
                    </div>

                    <div id="invoice_number" class="form-group control-group">
                        <label for="invoice_number">Invoice Number</label>
                        <div>
                            <input class="form-control" type="text" name="invoice_number" id="invoice_number"
                                   value="{{ $utility->invoice_number }}">
                        </div>
                        <span id="invoice_number" class="help-block"></span>
                    </div>

                    <div id="date_from_to" class="form-group control-group">
                        <label for="date_from_to">Date From To</label>
                        <div>
                            <input class="form-control" type="text" name="date_from_to" id="date_from_to"
                                   value="{{ $utility->date_from_to }}">
                        </div>
                        <span id="date_from_to" class="help-block"></span>
                    </div>

                    <div id="opening_read" class="form-group control-group">
                        <label for="opening_read">Opening Read</label>
                        <div>
                            <input class="form-control" type="number" name="opening_read" id="opening_read"
                                   value="{{ $utility->opening_read }}">
                        </div>
                        <span id="opening_read" class="help-block"></span>
                    </div>

                    <div id="closing_read" class="form-group control-group">
                        <label for="closing_read">Closing Read</label>
                        <div>
                            <input class="form-control" type="number" name="closing_read" id="closing_read"
                                   value="{{ $utility->closing_read }}">
                        </div>
                        <span id="closing_read" class="help-block"></span>
                    </div>

                    <div id="amount_net" class="form-group control-group">
                        <label for="amount_net">Amount Net</label>
                        <div>
                            <input class="form-control" step="0.01" type="number" name="amount_net" id="amount_net"
                                   value="{{ $utility->amount_net }}">
                        </div>
                        <span id="amount_net" class="help-block"></span>
                    </div>

                    <div id="archived" class="form-group control-group">
                        <label for="archived">Archived</label>
                        <div>
                            <select class="form-control" name="archived" id="archived">
                                <option @if(!$utility->archived) selected @endif value="0">No</option>
                                <option @if($utility->archived) selected @endif value="1">Yes</option>
                            </select>
                        </div>
                        <span id="archived" class="help-block"></span>
                    </div>

                    <div class="form-actions">
                        <button id="save-utility" class="btn mr-2 mb-2 btn-outline-primary" type="submit">
                            Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push('js')
        <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    @endpush
@endsection
