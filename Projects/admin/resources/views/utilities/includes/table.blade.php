<table class="table table-responsive">
    <thead>
    <tr>
        @foreach($utilities['headers'] as $header)
            <td>{{$header}}</td>

        @endforeach
        <td>Actions</td>
    </tr>
    </thead>
    <tbody>
    @foreach($utilities['rows'] as $row)
        <tr>
            @foreach($row as $key => $utility)
                @continue($key == 0)
                <td>
                    <a href="{{route('utilities.edit', $row[0])}}">
                        {{$utility}}
                    </a>
                </td>
            @endforeach
            <td>
                <button data-note-type-id="11" data-object_type_id="11" data-selected="11" data-object_id="{{$row[0]}}"
                        data-toggle="modal" data-target="#noteModal" class="btn btn-sm btn-success animate-hover mr-3 utility-add-note">
                    Add Note
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

