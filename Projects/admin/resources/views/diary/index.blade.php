@extends('layouts_new.app', ['pageTitle' => 'Diary', 'pageNav' => 'diary', 'page' => 'DiaryIndex'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            @include('partials.show_diary', ['buttons' => 'buttons'])
        </div>
    </div>
@endsection
