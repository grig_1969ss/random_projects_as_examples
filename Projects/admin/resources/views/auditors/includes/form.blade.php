<fieldset>
    <div id='error-message'></div>

    <form method="POST"
          action="{{ $auditor->id ? route('auditors.update', $auditor) : route('auditors.store') }}"
          class="form-horizontal"
          id="create-auditor">
        {{ csrf_field() }}

        @if($auditor->id)
            {{ method_field('PUT') }}
        @endif

        <div id="name" class="control-group">
            <label for="name" class="control-label">Name</label>
            <div class="controls">
                <input type="text" name="name" id="name" class="input-medium" value="{{ $auditor->name }}">
                <span id="name" class="help-block"></span>
            </div>
        </div>

        <div id="email" class="control-group">
            <label for="email" class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" id="email" class="input-medium" value="{{ $auditor->email }}">
                <span id="email" class="help-block"></span>
            </div>
        </div>

        <div class="form-actions">
            <button id="save-auditor" class="btn btn-large btn-primary" type="submit">
                Save Changes
            </button>
        </div>
    </form>
</fieldset>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
