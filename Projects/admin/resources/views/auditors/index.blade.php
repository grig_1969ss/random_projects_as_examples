@extends('layouts_new.app',
    ['pageTitle' => 'Auditors', 'pageNav' => 'auditor', 'page' => 'AuditorsIndex'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Auditors</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li class="demoTabs">
                                    <a href="{{ route('auditors.create') }}" class="btn btn-alt">
                                        Add Auditor
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </header>
                    <table id="example-2" class="datatable table table-striped table-bordered table-hover dataTable"
                           aria-describedby="example-2_info" style="width: 880px;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach ($auditors as $auditor)
                            <tr class="odd gradeX">
                                @if (!$read_only)
                                    <td>
                                        <a href="{{ route('auditors.show', $auditor) }}">
                                            {{ $auditor->name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('auditors.show', $auditor) }}">
                                            {{ $auditor->email }}
                                        </a>
                                    </td>
                                @else
                                    <td>{{ $auditor->name }}</td>
                                    <td>{{ $auditor->email }}</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->
        </div>
        <!-- /Grid row -->
    </div>
    <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
