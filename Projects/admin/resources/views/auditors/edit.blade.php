@extends('layouts_new.app', [
    'pageTitle' => ($auditor->id ? 'Edit' : 'Add').' Auditor',
    'pageNav' => 'auditor',
    'page' => 'AuditorNew'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $auditor->id ? 'Edit' : 'New' }} Auditor</h2>
                    </header>
                    <div class="span8">
                        @include('auditors.includes.form')
                    </div>
                </div>
            </article>
        </div>
    </div>
@endsection