@extends('layouts_new.app', ['pageTitle' => 'Auditor', 'pageNav' => 'auditor', 'page' => 'AuditorsDetail'])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $auditor->name }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="{{ route('auditors.edit', $auditor) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Name</dt>
                            <dd>{{ $auditor->name }}</dd>
                            <dt>Email</dt>
                            <dd>{{ $auditor->email }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($auditor->created_at) }}
                                by {{ optional($auditor->createdBy)->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
