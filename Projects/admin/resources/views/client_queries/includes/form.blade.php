@php
    $due_date = ($clientQuery->due_date) ? Date::dmY($clientQuery->due_date) : null;
    $diary_date = ($clientQuery->diary && $clientQuery->diary->date) ? Date::dmY($clientQuery->diary->date) : null;
@endphp

{{ Form::open([
    'route' => ($clientQuery->id ? ['client-queries.update', $clientQuery] : ['properties.client-queries.store', $property]),
    'method' => ($clientQuery->id ? 'PUT' : 'POST'),
    'class' => 'form-horizontal',
    'id' => 'save-client-query'
]) }}

<div id='error-message'></div>
<div id="due_date" class="control-group">
    {{ Form::label('due_date', 'Due Date', array('class' => 'control-label')) }}
    <div class="form-group controls">
        <div class="input-append">
            {{ Form::text('due_date',$due_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
            <span class="add-on"><i class="awe-calendar"></i></span>
        </div>
        <span id="due_date" class="help-block"></span>
    </div>
</div>
<?php
$statusDropdown = [
    null => 'Select...',
    1 => 'In Progress',
    2 => 'Closed',
];
?>
<div id="client_query_status_id" class="control-group">
    {{ Form::label('client_query_status_id', 'Client Query Status', array('class' => 'control-label')) }}
    <div class="form-group controls">
        {{ Form::select('client_query_status_id', $statusDropdown, $clientQuery->client_query_status_id, array('id'=>'client_query_status_id', 'class' => 'form-control add-new-option')) }}
        <span id="client_query_status_id" class="help-block"></span>
    </div>
</div>
<div id="details" class="control-group">
    {{ Form::label('details', 'Details', array('class' => 'control-label')) }}
    <div class="form-group controls">
        {{ Form::textarea('details',$clientQuery->details, array('class' => 'form-control','rows'=> 3, 'id' => 'prependedInput')) }}
        <span id="details" class="help-block"></span>
    </div>
</div>

@if(isset($clientQuery->diary->id))
    <div id="diary_date" class="control-group">
        {{ Form::label('diary_date', 'Diary Date', array('class' => 'control-label')) }}
        <div class="form-group controls">
            <div class="input-append">
                {{ Form::text('diary_date',$diary_date, array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                <span class="add-on"><i class="awe-calendar"></i></span>
                <span id="diary_date" class="help-block"></span>
            </div>
        </div>
    </div>
    {{ Form::hidden('diary_id', $clientQuery->diary->id) }}
@else
    {{ Form::hidden('diary_date', Date::dmY(date('Y-m-d H:i:s'))) }}
@endif
<div class="form-actions">
    <button id="save-client" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
        Save Changes
    </button>
</div>

{{ Form::close() }}

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
