@extends('layouts_new.app', [
    'pageTitle' => ($clientQuery->id ? 'Edit' : 'Add').' Client Query',
    'pageNav'   => 'property',
    'page'      => 'PropertyClientQuery',
])

@section('container')
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-4 order-md-1">
                <div class="mb-5 text-center">
                    <h6 class="h3">{{ $clientQuery->id ? 'Edit' : 'Add' }} Client Query</h6>
                    <div class="dropdown-divider"></div>
                </div>
                @include('client_queries.includes.form')
            </div>
        </div>
    </div>
@endsection
