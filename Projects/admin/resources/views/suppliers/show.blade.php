@extends('layouts_new.app', ['pageTitle' => 'Supplier', 'pageNav' => 'supplier', 'page' => 'SuppliersDetail'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">  {{ $supplier->name }}</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a  href="{{ route('suppliers.edit', $supplier) }}" class="btn add-property btn-outline-primary">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3 align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Supplier</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">{{ $supplier->name }}</p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Address</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ $supplier->address->address_1 }}<br>
                                {{ $supplier->address->address_2 }}<br>
                                {{ $supplier->address->town }}<br>
                                {{ $supplier->address->county }}<br>
                                {{ $supplier->address->post_code }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Telephone</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($supplier->telephone) }}<br>
                                {{ Util::showValue($supplier->mobile) }}
                            </p>
                        </div>
                    </div>

                    @if($supplier->fax)
                        <div class="row align-items-center">
                            <div class="col-5">
                                <small class="text-uppercase text-muted">Fax</small>
                            </div>
                            <div class="col-7 pl-0">
                                <p class="mb-0">
                                    {{ $supplier->fax }}
                                </p>
                            </div>
                        </div>
                    @endif

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Email</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($supplier->email) }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Added</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Date::dmYHi($supplier->created_at) }}
                                by {{ $supplier->user->name }}
                            </p>
                        </div>
                    </div>

                </div>
                <div class="col-6">
                    <!-- Title  -->
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">Contacts</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a href="{{ route('suppliers.contacts.create', $supplier) }}" class="btn add-property btn-outline-primary">Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-5">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Contact Name</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($supplier->contacts as $contact)
                                        <?php $contactUrl = '/contacts/'.$contact->id; ?>
                                        <tr>
                                            <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="content-block" role="main">
        <div class="row">
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $supplier->name }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="{{ route('suppliers.edit', $supplier) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Supplier</dt>
                            <dd>{{ $supplier->name }}</dd>
                            <dt>Address</dt>
                            <dd>{{ $supplier->address->address_1 }}</dd>
                            <dd>{{ $supplier->address->address_2 }}</dd>
                            <dd>{{ $supplier->address->town }}</dd>
                            <dd>{{ $supplier->address->county }}</dd>
                            <dd>{{ $supplier->address->post_code }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ Util::showValue($supplier->telephone) }}</dd>
                            <dd>{{ Util::showValue($supplier->mobile) }}</dd>
                            @if($supplier->fax)
                                <dt>Fax</dt>
                                <dd>{{ $supplier->fax }}</dd>
                            @endif
                            <dt>Email</dt>
                            <dd>{{ Util::showValue($supplier->email) }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($supplier->created_at) }}
                                by {{ $supplier->user->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Contacts</h2>
                        <ul class="data-header-actions">
                            <li><a href="{{ route('suppliers.contacts.create', $supplier) }}" class="btn btn-alt">Add</a></li>
                        </ul>
                    </header>
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Telephone</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($supplier->contacts as $contact)
                            <?php $contactUrl = '/contacts/'.$contact->id; ?>
                            <tr class="odd gradeX">
                                <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
        </div>

        <div class="row">
            <article class="span12 data-block">
                @include('partials.show_notes', ['id' => $supplier->id])
            </article>

            @include('partials.add_comment', [
                'id'    => $supplier->id,
                'type'  => 'suppliers',
                'route' => ['suppliers.notes.store', $supplier]
            ])

        </div>
    </div>
@endsection


