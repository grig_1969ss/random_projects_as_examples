@extends('layouts_new.app', [
    'pageTitle' => ($supplier->id ? 'Edit' : 'Add').' Supplier',
    'pageNav' => 'supplier',
    'page' => 'SupplierNew'
])

@section('container')
    @include('suppliers.includes.form',[
      'title'      => ($supplier->id ? 'Edit' : 'New').' Supplier',
    ])
@endsection
