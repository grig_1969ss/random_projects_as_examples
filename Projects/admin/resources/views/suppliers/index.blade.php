@extends('layouts_new.app',  ['pageTitle' => 'Suppliers', 'pageNav' => 'supplier', 'page' => 'SuppliersIndex'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Suppliers</h5>
                    </div>
                    <div class="dropdown">
                        @if ($read_only == 0)
                            <div class="btn-group mr-2 mb-2">
                                <a href="{{route('suppliers.create')}}" class="btn add-property btn-outline-primary">Add Supplier</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Main Contact</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($suppliers as $supplier)
                                <tr>
                                    @if ($read_only == 0)
                                        <td>
                                            <a href="{{ route('suppliers.show', $supplier->id) }}">
                                                {{ $supplier->present()->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('suppliers.show', $supplier->id) }}">
                                                {{ $supplier->present()->telephone }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('suppliers.show', $supplier->id) }}">
                                                {{ $supplier->present()->email }}
                                            </a>
                                        </td>
                                        <td>
                                            @if($supplier->mainContact)
                                                <a href="{{ route('contacts.show', $supplier->mainContact) }}">
                                                    {{ Util::joinName([$supplier->mainContact->first_name, $supplier->mainContact->surname]) }}
                                                </a>
                                            @endif
                                        </td>
                                    @else
                                        <td>{{ $supplier->present()->name }}</td>
                                        <td>{{ $supplier->present()->telephone }}</td>
                                        <td>{{ $supplier->present()->email }}</td>
                                        <td>{{ Util::joinName([$supplier->first_name, $supplier->surname]) }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <nav class="property-list-pagination default-pagination" aria-label="Page navigation example">
                            <span>Showing {{$suppliers->firstItem()}} to {{$suppliers->lastItem()}} of {{$suppliers->total()}} entries</span>
                            {{$suppliers->links()}}
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
