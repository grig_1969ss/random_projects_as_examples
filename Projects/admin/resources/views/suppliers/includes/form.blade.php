<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-md-5 col-lg-4 order-md-1">
            <div class="mb-5 text-center">
                <h6 class="h3">{{$title}}</h6>
                <div class="dropdown-divider"></div>
            </div>
            <div id='error-message'></div>
            <form method="POST"
                  action="{{ $supplier->id ? route('suppliers.update', $supplier) : route('suppliers.store') }}"
                  class="form-horizontal" id="create-supplier">
                {{ csrf_field() }}

                @if($supplier->id)
                    {{ method_field('PUT') }}
                @endif

                <div id="name" class="control-group">
                    <label for="name" class="control-label">Name</label>
                    <div class="form-group controls">
                        <input type="text" name="name" id="name"
                               class="form-control  @error('name') invalid-input @enderror"
                               value="{{ $supplier->name }}">
                        <span id="name" class="help-block invalid-text"></span>
                    </div>
                </div>

                @if (!$supplier->id)
                    <div id="contact-first_name" class="control-group">
                        <label for="contact-first_name" class="control-label">Contact First Name</label>
                        <div class="form-group">
                            <input type="text" name="contact[first_name]" id="contact-first_name"
                                   class="form-control @error('contact.surname') invalid-input @enderror">
                            <span id="contact-first_name" class="help-block invalid-text"></span>
                        </div>
                    </div>
                    <div id="contact-surname" class="control-group">
                        <label for="contact-surname" class="control-label">Contact Surname</label>
                        <div class="form-group">
                            <input type="text" name="contact[surname]" id="contact-surname"
                                   class="form-control @error('contact.surname') invalid-input @enderror">
                            <span id="contact-surname" class="help-block invalid-text"></span>
                        </div>
                    </div>
                @endif

                <div id="address-address_1" class="control-group">
                    <label for="address-address_1" class="control-label">Address Line 1</label>
                    <div class="form-group">
                        <input type="text"
                               name="address[address_1]"
                               id="address-address_1"
                               class="form-control @error('address.address_1') invalid-input @enderror()"
                               value="{{ $address->address_1 }}">
                        <span id="address-address_1" class="help-block"></span>
                    </div>
                </div>

                <div id="address-address_2" class="control-group">
                    <label for="address-address_2" class="control-label">Address Line 2</label>
                    <div class="form-group">
                        <input type="text"
                               name="address[address_2]"
                               id="address-address_2"
                               class="form-control"
                               value="{{ $address->address_2 }}">
                        <span id="address_2" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="town" class="control-group">
                    <label for="town" class="control-label">Town</label>
                    <div class="form-group">
                        <input type="text" name="address[town]" id="town" class="form-control"
                               value="{{ $address->town }}">
                        <span id="town" class="help-block"></span>
                    </div>
                </div>

                <div id="county" class="control-group">
                    <label for="county" class="control-label">County</label>
                    <div class="form-group">
                        <input type="text"
                               name="address[county]"
                               id="county"
                               class="form-control"
                               value="{{ $address->county }}">
                        <span id="county" class="help-block"></span>
                    </div>
                </div>

                <div id="address-post_code" class="control-group">
                    <label for="address-post_code" class="control-label">Post Code</label>
                    <div class="form-group">
                        <input type="text"
                               name="address[post_code]"
                               id="address-post_code"
                               class="form-control  @error('address.post_code') invalid-input @enderror"
                               value="{{ $address->post_code }}">
                        <span id="address-post_code" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="telephone" class="control-group">
                    <label for="telephone" class="control-label">Telephone Number</label>
                    <div class="form-group">
                        <input type="text"
                               name="telephone"
                               id="telephone"
                               class="form-control @error('telephone') invalid-input @enderror"
                               value="{{ $supplier->telephone }}">
                        <span id="telephone" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="fax" class="control-group">
                    <label for="fax" class="control-label">Fax Number</label>
                    <div class="form-group">
                        <input type="text" name="fax" id="fax" class="form-control" value="{{ $supplier->fax }}">
                        <span id="fax" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="email" class="control-group">
                    <label for="email" class="control-label">Email</label>
                    <div class="form-group">
                        <input type="text" name="email" id="email" class="form-control" value="{{ $supplier->email }}">
                        <span id="email" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div class="form-actions">
                    <button id="save-supplier" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
                        Save Changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
