<div class="pagination-replace">
    <form action="{{route('email.actions')}}" method="post" id="buttons-form">
        @csrf
        <input type="hidden" name="action" value="">
        @foreach($emails as $key => $email)
            <a href="{{route('email.detail', $email->id)}}">
                <div class="timeline-item border border-light"
                     @if(!$email->read_at) style="font-weight: 900" @endif>
                    <div class="container-fluid single-item position-relative mail-top-header">
                        <div class="inbox-icons-wrapper">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input name="emails[]" value="{{$email->id}}" class="form-check-input"
                                           type="checkbox">
                                    <span class="form-check-sign"></span>
                                </label>
                            </div>
                            @if($email->starred) <i class="fas fa-star icon mr-2 icon-warning"></i> @endif
                            <i class="fas fa-circle text-success fa-xs icon mr-2 icon-warning"></i>
                            <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle ml-2 mr-0 mt-1 mb-1"
                                 src="{{ gravatar_url($email->from) }}" alt="{{ $email->from }}">
                        </div>
                        <div class="inbox-from-wrapper">
                            @if($email->type->name == 'Sent')
                                To: &nbsp; {{ $email->to }}
                            @elseif($email->from_name)
                                <span>{{ $email->from_name }} ({{ $email->from }})</span>
                            @else
                                <span>{{ $email->from }}</span>
                            @endif
                        </div>

                        <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 inbox-subject-wrapper">
                            <span>{{$email->subject}}</span>
                        </div>

                        <div class="inbox-date-wrapper">
                            @if($email->attachments()->count() > 0)
                                <i class="fas fa-paperclip icon mr-3"></i>
                            @endif
                            <span>
                       @if($email->date->isToday())
                                    {{\Carbon\Carbon::parse($email->date)->format('H:i:s')}}
                                @else
                                    {{\Carbon\Carbon::parse($email->date)->format('d M Y')}}
                                @endif
                    </span>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
    </form>
</div>

