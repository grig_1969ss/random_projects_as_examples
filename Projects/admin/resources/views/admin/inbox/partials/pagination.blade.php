<div class="pr-4 pl-4 flex-fill">
    <div class="d-flex justify-content-between pagination align-items-center">
        <div>
            @php
                $pagination = json_decode($emails->toJson());
            @endphp

            {{ $pagination->from }} - {{ $pagination->to }} of {{ $pagination->total }}
        </div>
        <div>
            <i class="fas fa-arrow-left pl-3 pr-1 icon  @if($pagination->prev_page_url) prev-page @endif " data-url="{{ $pagination->prev_page_url }}&emails={{request()->get('emails')}}"></i>
            <i class="fas fa-arrow-right pr-3 icon @if($pagination->next_page_url) next-page @endif" data-url="{{ $pagination->next_page_url }}&emails={{request()->get('emails')}}"></i>
            <i class="fas fa-cog pl-3 icon"></i>
        </div>
    </div>
</div>
