<div class="col-lg-9 pr-lg-4 right-side">
    <div class="timeline-item top-item shadow-sm border border-light">
        <div class="container-fluid position-relative mail-top-header mb-1 mt-1">
            <div class="d-flex align-items-center">
                <ul class="flex-row list-style-none mr-4 d-none d-sm-flex">
                    <li class="mr-3">
                        <div class="form-check mt-0">
                            <label class="form-check-label pl-0 mr-3">
                                <input class="form-check-input checkbox-toggle" type="checkbox"><span class="form-check-sign"></span>&nbsp;
                            </label>
                        </div>
                    </li>
                    <li class="mr-3">
                        <i id="as-important" class="fas fa-exclamation-circle icon"></i>
                    </li>
                    <li class="mr-3">
                        <i id="as-trash" class="fas fa-trash icon"></i>
                    </li>
                </ul>
            </div>

            <div class="top-header_pagination">
                @include('admin.inbox.partials.pagination')
            </div>
        </div>
    </div>
    @include('admin.inbox.partials.emails_table')
</div>
