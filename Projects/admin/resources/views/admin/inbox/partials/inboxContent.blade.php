<div class="section section-md">
    <div class="container-fluid">
        @if(request()->route()->getName() === 'inbox.index')
            <p><a class="btn btn-outline-primary"
                  href="{{route('inbox.index')}}?emails={{$nextType}}">
                    {{$nextType === 'my' ? 'Show My Emails' : 'Show All Emails'}}
                </a>
            </p>
        @endif
        <div class="row justify-content-between">
            <aside class="col-12 col-lg-3 mt-3 mt-lg-0 main_left">
                <div class="card shadow-soft border-soft p-3 inboxes">
                    <a class="btn compose mr-2 mb-2 btn-pill btn-outline-primary" href="{{route('emails.create')}}?property_id={{request()->segment(2)}}">NEW
                        EMAIL</a>
                    <ul class="list-group list-group-flush bg-white shadow-sm border border-soft">
                        <li data-type="inbox" data-property="{{request()->segment(2)}}"
                            data-emails="{{request()->get('emails')}}"
                            class="list-group-item d-flex justify-content-between align-items-center border-0 menu-option active-inbox">
                                <span class="text-primary"><i
                                        class="fas fa-inbox icon mr-3"></i>Inbox</span>
                            @if($unreadCount['inbox'] > 0)
                                <div class="email-count">{{ $unreadCount['inbox'] }}</div>
                            @endif
                        </li>
                        <li data-type="draft" data-emails="{{request()->get('emails')}}" data-property="{{request()->segment(2)}}"
                            class="list-group-item d-flex justify-content-between align-items-center border-0 menu-option">
                                <span class="text-primary"><i
                                        class="far fa-envelope-open icon mr-3"></i>Draft</span>
                            @if($unreadCount['draft'] > 0)
                                <div class="email-count">{{ $unreadCount['draft'] }}</div>
                            @endif
                        </li>
                        <li data-type="sent" data-emails="{{request()->get('emails')}}" data-property="{{request()->segment(2)}}"
                            class="list-group-item d-flex justify-content-between align-items-center border-0 menu-option">
                                <span class="text-primary"><i
                                        class="fas fa-paper-plane icon mr-3"></i>Sent Mail</span>
                        </li>
                        <li data-type="archived" data-emails="{{request()->get('emails')}}" data-property="{{request()->segment(2)}}"
                            class="list-group-item d-flex justify-content-between align-items-center border-0 menu-option">
                                <span class="text-primary"><i
                                        class="fas fa-archive icon mr-3"></i>Archived</span>
                            @if($unreadCount['archived'] > 0)
                                <div class="email-count">{{ $unreadCount['archived'] }}</div>
                            @endif
                        </li>
                        <li data-type="trash" data-emails="{{request()->get('emails')}}" data-property="{{request()->segment(2)}}"
                            class="list-group-item d-flex justify-content-between align-items-center border-0 menu-option">
                                <span class="text-primary"><i
                                        class="fas fa-trash icon mr-3"></i>Trash</span>
                            @if($unreadCount['trash'] > 0)
                                <div class="email-count">{{ $unreadCount['trash'] }}</div>
                            @endif
                        </li>
                    </ul>
                </div>
            </aside>

            @include('admin.inbox.partials.emailsList')

        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('js/inbox.js')}}"></script>
@endpush
