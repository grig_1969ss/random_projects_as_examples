@extends('layouts_new.app', ['pageTitle' => 'Compose Email', 'pageNav' => 'inbox', 'page' => 'InboxCompose'])

@section('container')
    <form method="POST" id="email-create-from" action="{{ route('emails.store') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="is_draft" class="is-draft-input" value="0">
        <input type="hidden" name="property_id" value="{{request()->get('property_id')}}">
        @if(request()->route()->parameter('email'))
            <input type="hidden" name="previous_id" value="{{request()->route()->parameter('email')->id}}">
            <input name="previous_content" type="hidden" value="{{request()->route()->parameter('email')->html_body}}">
        @endif

        <div class="col-12 email-create">
            <div class="blog-card">
                <div class="card shadow-soft border-soft p-2 p-md-3 p-lg-5 first">
                    <div class="card-header">
                        <h3>{{ !$email->id ? 'Compose' : 'Reply To' }} Email</h3>
                    </div>
                    @if($email->client_id)
                        <input type="hidden" name="client_id" value="{{ $email->client_id }}">
                    @endif
                    <div class="card-body pt-0">

                        <div class="form-group">
                            <div class="input-group @error('to') error-block  @enderror ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input class="form-control" placeholder="TO" type="email" name="to" required
                                       value="{{ old('to', Util::replyTo($subject, $email) ) }}">
                            </div>
                            @error('to')
                            <span id="to" class="help-block error-message"> {{ $errors->first('to') }}</span>
                            @enderror
                        </div>
                        <div class="form-group ">
                            <div class="input-group @error('cc') error-block  @enderror ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input
                                    class="form-control" placeholder="CC"
                                    type="email" name="cc"
                                    value="{{ old('cc', request()->all ? implode(',',$toCc) : '' ) }}">
                            </div>
                            @error('cc')
                            <p class="help-block error-message"> {{ $errors->first('cc') }}</p>
                            @enderror
                        </div>
                        @php
                            $subjectType = $subject ?? 'Re';
                        @endphp
                        <div class="form-group">
                            <input
                                class="form-control @error('subject') error-block  @enderror"
                                placeholder="Subject" required
                                type="text" name="subject"
                                value="{{ old('subject', $email->id ? ($subjectType.': '.$email->subject) : '') }}"
                            >
                            @error('subject')
                            <p class="help-block error-message"> {{ $errors->first('subject') }}</p>
                            @enderror
                        </div>
                        <div class="form-group ">
                            <textarea class="form-control @error('body') error-block  @enderror "
                                      id="body" placeholder="Enter your message..."
                                      rows="3"
                                      name="body"
                                      required
                            >{{ old('body') }}</textarea>
                            @error('body')
                            <p class="help-block error-message"> {{ $errors->first('body') }}</p>
                            @enderror
                        </div>

                    </div>

                    <h6>{!! Util::replyEmailTime($email) !!}</h6>

                    <p>
                        @include ('admin.emails.partials.iframe')
                    </p>

                    <div class="row">
                        <input type="hidden" name="attachment_ids_base_64"
                               value="{{Util::attachmentsAsString($email)}}">
                    </div>

                </div>
            </div>
            <!-- End of Contact Card -->
        </div>

        <div class="col-12">
            <!-- Contact Card -->
            <div class="blog-card">
                <div class="card shadow-soft border-soft p-2 p-md-3 p-lg-5">
                    <div class="card-header attachments">
                        <h3>Add attachment</h3>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <!-- <div class="col-3"></div> -->
                            <div class="col-12">
                                <div class="shadow-soft border-light pt-3 pr-5 pb-3 pl-4 mb-2">
                                    <div class="row d-flex align-items-center">
                                        <div class="row col-lg-9 mb-3 mb-lg-0">
                                            <div class="col-lg-4 col-md-6" style="display: none">
                                                <input class="form-control" disabled="disabled" type="text"/>
                                            </div>
                                            <div class="custom-file col-lg-3 col-md-3">
                                                <input name="attachments[]" type="file" class="custom-file-input">
                                                <label class="custom-file-label">Choose file</label>
                                            </div>
                                        </div>

                                        <div class="col-12 col-lg-2">
                                            <button class="btn mr-2 mb-2 btn-pill btn-outline-danger delete-btn"
                                                    type="button">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="shadow-soft border-light pt-3 pr-5 pb-3 pl-4 mb-2 base-file-row sr-only">
                                    <div class="row d-flex align-items-center">
                                        <div class="row col-lg-9 mb-3 mb-lg-0">
                                            <div class="col-lg-4 col-md-6" style="display: none">
                                                <input class="form-control" disabled="disabled" type="text"/>
                                            </div>
                                            <div class="custom-file col-lg-3 col-md-3">
                                                <input name="attachments[]" type="file" class="custom-file-input">
                                                <label class="custom-file-label">Choose file</label>
                                            </div>
                                        </div>

                                        <div class="col-12 col-lg-2">
                                            <button class="btn mr-2 mb-2 btn-pill btn-outline-danger delete-btn"
                                                    type="button">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class=" shadow-soft border-light pt-3 pr-5 pb-3 pl-4">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-12">
                                            <button class="btn mr-2 mb-2 btn-pill btn-outline-gray add-file-btn"
                                                    type="button">Add File
                                            </button>

                                            <button class="btn mr-2 mb-2 btn-pill btn-outline-gray"
                                                    id="attach-email-as-pdf" data-attaches-name=""
                                                    type="button">Attach Email
                                                <span class="spinner-border sr-only spinner-border-sm" role="status"
                                                      aria-hidden="true"></span>
                                            </button>
                                            <input type="hidden" name="attached_pdf_name" value="{{old('attached_pdf_name')}}">
                                            <span
                                                class="badge badge-pill badge-secondary sr-only text-uppercase">{{old('attached_pdf_name')}}</span>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="card-footer mx-4 px-0 pt-0">
                        <button class="btn mr-2 mb-2 btn-outline-primary save-draft-btn" type="button">Save as draft
                        </button>
                        <button id="email-create-from-button" class="btn mr-2 mb-2 btn-outline-success" type="submit">Send</button>
                        <a href="{{route('inbox.index')}}" class="btn mr-2 mb-2 btn-white float-right">Back</a>
                    </div>
                </div>
            </div>
            <!-- End of Contact Card -->
        </div>
    </form>


    <div class="modal fade" id="emailAsPdf" tabindex="-1" role="dialog" aria-labelledby="modal-default"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-default">Attach a email as a PDF file to the sending
                        email</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="" id="email-pdf-preview-container">

                        <div class="">
                            <div id="replace-property-dropdown">
                                <div></div>
                            </div>
                            <div id="replace-email-dropdown">
                                <div></div>
                            </div>
                        </div>

                        <div id="pdf-container" class="text-center">
                            <span class="spinner-border spinner-border-sm sr-only"
                                  role="status" aria-hidden="true" style="width: 7rem;height: 7rem">

                            </span>
                            @include('layouts_new.partials.components.pdf.container')
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-danger" data-dismiss="modal">Close</button>
                    <button id="attach-chosen-pdf" type="button" class="btn btn-sm btn-secondary ml-auto">Attach
                    </button>
                </div>
            </div>
        </div>
    </div>






    {{--<div class="modal fade" id="emailPreviewModal" tabindex="-1" role="dialog" aria-labelledby="modal-default"--}}
    {{--aria-hidden="true">--}}
    {{--<div class="modal-dialog modal-dialog-centered" role="document">--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<h6 class="modal-title" id="modal-title-default">Email PDF Preview</h6>--}}
    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--<span aria-hidden="true">×</span>--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--<div id="email-attach-preview">--}}
    {{--<div>--}}
    {{--@include('layouts_new.partials.components.pdf.container')--}}
    {{--</div>--}}
    {{--<div id="replace">--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal-footer">--}}
    {{--<button type="button" class="btn btn-link text-danger ml-auto" data-dismiss="modal">Close</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}





    @include('layouts_new.partials.components.pdf.viewer')
@endsection

@push('js')
    <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
    <script src="{{asset('js/pdf.js')}}"></script>
    <script src="{{ asset('js/inbox.js')}}"></script>
@endpush

{{--We will use this in multiple places, but for now we will do as follows;--}}
{{--Have a button next to ‘Add attachment’ on emails with the label ‘Attach email’
that when pressed shows a modal asking for ‘Property’ (a dropdown (with the current property selected by default if applicable))
 and when property is selected, ‘Email’ (a dropdown list of emails from that property (date dd/mm/yy hh:mm and subject).
 When email is selected, show a preview. And an attach button. When attach is pressed, show something on the email to show it is attached.--}}
{{--11:38--}}
{{--When the email sends, pdf the selected email and attach to sending email.--}}
