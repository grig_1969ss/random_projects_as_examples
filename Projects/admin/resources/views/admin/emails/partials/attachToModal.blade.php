<div class="modal fade " id="modal-attach-to" tabindex="-1" role="dialog" aria-labelledby=modal-attach-to"
     aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close ml-auto" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5" id="type-crud"><div></div></div>
                    <div class="col-7" id="type-pdf"><div></div></div>
                </div>
            </div>
        </div>
    </div>
</div>
