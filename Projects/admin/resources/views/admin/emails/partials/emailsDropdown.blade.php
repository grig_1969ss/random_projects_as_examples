<div class="form-group emails-dropdown">
    <label for="emails-inbox">Emails</label>
    <select class="form-control select2" name="email" id="emails-inbox">
        <option disabled selected value="">Select...</option>
        @foreach($emails as $email)
            <option
                value="{{$email->id}}">
               {{Util::emailName($email)}}
            </option>
        @endforeach
    </select>
</div>
