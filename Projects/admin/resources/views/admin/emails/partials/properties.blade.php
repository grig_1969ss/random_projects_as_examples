<div class="form-group property-dropdown">
    <label for="property-inbox">Property</label>
    <input type="hidden" id="property_exists" data-property="{{request()->get('property')}}">
    <select class="form-control select2" name="property" id="property-inbox">
        <option disabled selected value="">Select...</option>
        @foreach($properties as $property)
            <option @if(request()->get('property') == $property->id) selected @endif
            value="{{$property->id}}">
                {{Util::propertyName($property)}}
            </option>
        @endforeach
    </select>
</div>
