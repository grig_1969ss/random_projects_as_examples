<div class="email-iframe @if($htmlBody == '') sr-only @endif">
    <iframe src="{{route('email.detail', ['email' => request()->segment(2)])}}?iframe=1" id="email-iframe"></iframe>
</div>

