<div class="modal fade" id="modal-rediarise-email" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Re Diarise</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{route('email-adddiarise')}}" method="post" id="re-diarise-email">
                @csrf
                <div class="modal-body">
                    @if($email->diaries()->count())
                        <input type="hidden" name="update" value="rediarise">
                    @endif
                    <input type="hidden" name="email" value="{{$email->id}}">
                    <div id="date" class="form-group control-group">
                        <small class="d-block font-weight-normal mb-3">Diary Date</small>
                        <div class="form-group">
                            <input name="diary_date" class="form-control datepicker" type="text" autocomplete="off">
                        </div>
                        <span id="date" class="help-block"></span>
                    </div>
                    <div id="diarise_note" class="form-group control-group">
                        <label for="diarise-note">Note</label>
                        <div>
                        <textarea class="form-control" name="diarise_note" id="diarise-note" cols="10"
                                  rows="3"></textarea>
                        </div>
                        <span id="diarise_note" class="help-block"></span>
                    </div>
                </div>
                @include('partials.modal_footer', array('buttonName' => 'Save'))
            </form>
        </div>
    </div>
</div>
