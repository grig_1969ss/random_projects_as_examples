@extends('layouts_new.app', ['pageTitle' => 'Show Email', 'pageNav' => 'inbox', 'page' => 'ShowEmail'])
@push('css')
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
@endpush

@section('container')
    <div class="section section-sm" id="email_details">
        <div class="container-fluid">
            <div class="">
                <div class="col-12 mb-5">
                    <div>
                        <h5 class="font-weight-bold">{{$email->subject}}</h5>
                    </div>
                    <hr>
                    <?php ?>
                    <div class="row">
                        <div style="font-weight: 300">
                            <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-0"
                                 src="{{ gravatar_url($email->from) }}" alt="{{ $email->from }}">

                            <span>From: &nbsp; {{$email->from}} on {{ Date::dmYHi($email->created_at) }}  </span>

                        </div>
                        @if($email->email_type_id == 1)
                            <a type="button"
                               href="{{ route('emails.reply', $email) }}?property_id={{$email->property_id}}"
                               class="btn mr-2 ml-auto mb-2 btn-outline-success">Reply
                            </a>

                            <a type="button"
                               href="{{ route('emails.reply', $email) }}?all=true&property_id={{$email->property_id}}"
                               class="btn mr-2 mb-2 btn-outline-primary">Reply To All
                            </a>

                            <a type="button"
                               href="{{ route('emails.reply', $email) }}?type=fw&property_id={{$email->property_id}}"
                               class="btn mr-2 mb-2 btn-outline-default">Forward
                            </a>
                        @endif

                        @if($email->email_type_id == 2)
                            <a type="button"
                               href="{{ route('emails.reply', $email) }}?type=fw&property_id={{$email->property_id}}"
                               class="btn mr-2 mb-2 btn-outline-default ml-auto">Forward
                            </a>
                        @endif

                        @if($email->archived_at)
                            <a type="button" href="{{ route('emails.archive', $email) }}?type=unarchive"
                               class="btn mr-2 mb-2 btn-outline-warning ml-auto">UnArchive
                            </a>
                        @else
                            <a type="button" href="{{ route('emails.archive', $email) }}"
                               class="btn mr-2 mb-2 btn-outline-warning ml-auto">Archive
                            </a>
                        @endif

                        <button type="button" data-target="#modal-default" data-toggle="modal"
                                class="btn mr-2 mb-2 btn-outline-primary">{{!$email->property_id ? 'Link email': 'Re-Link'}}
                        </button>

                        <a href="{{route('unread', ['email' => $email->id])}}" type="button"
                           class="btn mr-2 mb-2 btn-outline-tertiary">Mark as Unread
                        </a>

                        @if($email->property_id)
                            <button type="button" class="btn mr-2 mb-2 btn-outline-gray" data-toggle="modal"
                                    data-target="#modal-rediarise-email">
                                {{$email->diaries()->count() ? 'Re Diarise' : 'Add Diary'}}
                            </button>
                        @endif

                    </div>
                    <div style="font-weight: 400"
                         class="mb-5 mt-4">
                         @include ('admin.emails.partials.iframe')
                    </div>

                    <div class="row">
                        @foreach($attachments as $attachment)
                            <a class="col-md-2 btn mr-2 mb-2 btn-outline-primary"
                               href="{{route('attachments.download',$attachment->id)}}"
                               target="_blank">
                                <i class="fas fa-paperclip"></i>
                                {{$attachment->name}}
                            </a>
                        @endforeach
                    </div>

                    <hr>

                    <div class="mt-3">
                        @if($email->property_id)
                            <div aria-label="breadcrumb mt-3 btn mr-2 ">
                                <ol class="breadcrumb breadcrumb-primary breadcrumb-transparent">
                                    @if($email->client)
                                        <li class="breadcrumb-item"><a href="#">{{$email->client->client_name}}</a></li>
                                    @endif
                                    <li class="breadcrumb-item"><a
                                            href="{{route('properties.show',$email->property->id)}}">{{$email->property->property_name}}</a>
                                    </li>
                                </ol>
                            </div>
                        @endif

                        <div class="ml-auto row">
                            <a type="button" href="{{route('inbox.index')}}"
                               class="col-md-2 btn mr-2 mb-2 btn-white">Back
                            </a>

                            <button type="button" data-target="#modal-default" data-toggle="modal"
                                    class="col-md-2 btn mr-2 ml-auto mb-2 btn-outline-primary">{{!$email->property_id ? 'Link email': 'Re-Link'}}
                            </button>
                        </div>
                    </div>

                    {{--Modal--}}
                    <div class="modal fade" id="modal-default" role="dialog"
                         aria-labelledby="modal-default"
                         aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h6 class="modal-title" id="modal-title-default">Link email to property</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <form action="{{route('link.email')}}" method="post" id="link-email-to-property">
                                    @csrf
                                    <input type="hidden" name="email" value="{{request()->segment('2')}}">
                                    <div class="modal-body">

                                        @include('admin.emails.partials.properties')

                                        <div class="row attachments-table sr-only">
                                            <table>
                                                @foreach($attachments as $key => $attachment)
                                                    <tr class="mt-2 attachment-row">
                                                        <td>
                                                            <a href="{{route('attachments.download',$attachment->id)}}"
                                                               data-unique="{{str_replace('.','-',$attachment->name.$key)}}"
                                                               class="btn mr-2 btn-link text-primary btn-icon {{ preg_replace('/[^\w]/', '_', $attachment->name.$key) }}">
                                                                <span class="btn-inner-icon"><i
                                                                        class="fas fa-paperclip"></i></span>
                                                                <span
                                                                    class="btn-inner-text font-weight-bold">{{$attachment->name}}</span>
                                                            </a>
                                                            @if(request()->segment(1) === 'inbox')
                                                                <input type="hidden" name="attachments[]"
                                                                       value="{{$attachment->id}}">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="note_type_replace"></div>
                                                        </td>
                                                        <td>
                                                            @include('documents.includes.documentType')
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </table>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-sm btn-secondary">
                                            Save
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.emails.partials.attachToModal')
    @include('admin.emails.partials.reDiariseEmail')
@endsection

@push('js')
    <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
    <script src="{{asset('js/pdf.js')}}"></script>
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).ready(function () {
                $(".select2").select2();
            });
        });
    </script>
@endpush
