<form action="{{ route($entity->documentStoreRoute, $entity) }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="uik-form-input-group__vertical">
        <div>
            <span class="uik-content-title__wrapper">Name</span>
            <div class="uik-input__inputWrapper">
                <input type="text" name="name" class="uik-input__input">
            </div>
        </div>
        <div>
            <span class="uik-content-title__wrapper">Type</span>
            <div class="uik-input__inputWrapper">
                <select name="document_type_id" id="document_type_id" class="uik-input__input">
                    <option value="">Select...</option>
                    @foreach($types as $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div>
            <span class="uik-content-title__wrapper">File</span>
            <div class="input-group file-wrapper">
                <input type="text" class="form-control">
                <div class="input-group-append">
                    <button type="button" class="input-group-text select-file">Select file</button>
                </div>
            </div>
            <input type="file" name="file" class="inputfile" />
        </div>
    </div>

    <div class="text-right pt-3">
        <input type="submit" class="uik-btn__base uik-btn__success" value="Save">
    </div>
</form>
