@extends('layouts_new.app',[
//TODO: this makes issue in console. (handle in js)
'page' => 'SwitchClient',
'pageTitle' => 'Switch Client'
])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">

            <!-- End of Title -->
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <div id="add-page" class="col-lg-9 pr-lg-4 new-doc-form">
                            <div>
                                <div class="mb-5 text-center">
                                    <p>Choose current client</p>
                                </div>
                                <span class="clearfix"></span>
                                <form action="{{route('save.current.client')}}" method="post" class="offset-md-4">
                                    @csrf
                                    <input type="hidden" name="m" value="DocumentType">
                                    <div class="row">

                                        <div class="col-md-8 col-lg-8 col-sm-12 form-group">
                                            <div class="form-group">
                                                <select required="" name="current_client" class="custom-select mr-sm-2">
                                                    <option value="" disabled="" selected="">Choose...</option>
                                                    @foreach($clients as $client)
                                                        <option
                                                            @if($user->client_id == $client->id) selected @endif
                                                        value="{{$client->id}}">{{$client->client_name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <button type="submit" class="btn mr-2 mb-2 btn-outline-success">Save</button>
                                    <a href="{{route('account.settings')}}" class="go-back btn mr-2 mb-2 btn-outline-dark"
                                       type="button">Back</a>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Grid row -->


            <!-- /Grid row -->

        </div>
    </div>





@endsection
