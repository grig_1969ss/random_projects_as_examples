@extends('layouts.app')

@section('content')

    <div id="edit-page" class="col-12 ">
        <div>
            <span class="clearfix"></span>
            <form action="{{route('type.update')}}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PUT') }}
                <input type="hidden" name="m" value="User">
                <input type="text" name="id" value="{{$type->id}}" hidden>
                <div class="row">
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="name" type="text" placeholder="Name" class="form-control"
                                   value="{{$type->name}}"/>
                        </div>
                    </div>

                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="username" type="text" placeholder="Username"
                                   value="{{$type->username}}" class="form-control"/>
                        </div>
                    </div>

                </div>

                <div class="row">
                    @foreach(userInputValues() as $inputValue)
                        <div class="col-4 form-group">
                            <div class="form-group">
                                <input required
                                       @foreach($inputValue as $key => $value)
                                       {{$key}}="{{$value}}" value="{{$type->$value}}"
                                       @endforeach
                                       class="form-control"/>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-4">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-4 form-group">
                        <div class="form-group">
                            @php $clients =  $type->clients()->pluck('id')->toArray() @endphp
                            <select multiple required name="client_id[]" class="custom-select mr-sm-2">
                                @foreach(\App\Models\Client::get() as $client)
                                    <option @if(in_array($client->id,$clients)) selected
                                            @endif value="{{$client->id}}">{{$client->client_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select required name="region_id" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Region</option>
                                @foreach(\App\Models\Region::get() as $region)
                                    <option @if($region->id == $type->region_id) selected
                                            @endif  value="{{$region->id}}">{{$region->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select required name="read_only" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Read Only</option>
                                <option @if($type->read_only == 1) selected @endif value="1">Yes</option>
                                <option @if($type->read_only == 0) selected @endif  value="0">No</option>
                            </select>
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn mr-2 mb-2 btn-outline-success">Update Changes</button>
                <a href="{{route('user.details', request()->id)}}" class="go-back btn mr-2 mb-2 btn-outline-dark"
                   type="button">Back</a>

            </form>

        </div>
    </div>






@endsection
