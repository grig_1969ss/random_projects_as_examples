<div id="index-table">
    <div class="row">
        <div class="col-12 mb-4 d-flex justify-content-between">
            <div>
                <h5>{{displayName($data['currentModel'])}}</h5>
            </div>
            <div class="d-none d-md-block">
                <button id="new-type"
                        class="btn mb-2 mr-2 btn-secondary animate-right-3" type="button">
                    Add {{displayName($data['currentModel'])}}
                </button>
            </div>
        </div>
    </div>

    <table class="table list-table table-responsive">
        <thead>
        <tr>
            <th style="width: 1%">
                @include('utils.checkbox', ['checkboxName' => ''])
            </th>
            <th>#</th>
            @foreach($data['columns'] as $column)
                <th>{{displayColumn($column)}}</th>
            @endforeach
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data['rows'] as $row)
            <tr class="clickable_false" data-url="/">
                <td>@include('utils.checkbox', ['checkboxName' => ''])</td>
                @foreach($row->toArray() as $key => $value)
                    <td>{!! filterValue($key, $value) !!}</td>
                @endforeach
                <td>
                    <div class="d-flex">
                        @if($data['currentModel'] == "User")
                            <a href="{{route('user.details',['id' => $row->id])}}"><i  data-type="{{$data['currentModel']}}" class="fas fa-eye mr-2" data-placement="top" title=""
                               data-id="{{$row->id}}"
                               data-original-title="Details"></i></a>
                        @endif
                        <i data-type="{{$data['currentModel']}}" class="fas fa-edit mr-3 edit" data-toggle="tooltip" data-placement="top" title=""
                           data-id="{{$row->id}}" data-original-title="Edit"></i>
                        @if(is_null($row->deleted_at))
                            <i data-type="{{$data['currentModel']}}" class="fas fa-trash text-warning mr-2 archive-type" data-placement="top" title=""
                               data-toggle="modal" data-target="#modal-default" data-id="{{$row->id}}"
                               data-original-title="Archive"></i>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>


    <div id="edit-part">

    </div>

    <main class="archive-modal sr-only">

        <!-- Section -->
        <div class="section section-lg">
            <div class="container-fluid">
                <div class="row mt-4">
                    <div class="col-md-4">
                        <div class="modal fade" id="modal-default" tabindex="-1" role="dialog"
                             aria-labelledby="modal-default" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h6 class="modal-title" id="modal-title-default">Archive</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form action="{{route('type.delete')}}" method="post">
                                        <div class="modal-body">
                                            <p>
                                                Do you want to archive this document type?
                                            </p>
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <input id="delete-id" type="hidden" name="id" value="">
                                            <input id="delete-type" type="hidden" name="m" value="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-sm btn-secondary">Archive</button>
                                            <button type="button" class="btn btn-link text-danger ml-auto"
                                                    data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- End of Modal Content -->
                    </div>

                </div>
            </div>
        </div>
        <!-- End of Section -->
    </main>

</div>

@include('admin.account.settings.add')

