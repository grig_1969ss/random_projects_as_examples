<div id="edit-page" class="col-12 ">
    <div>
        <div class="mb-5 text-center">
            <h3>Edit</h3>
            <p>Edit {{displayName($model)}} and save</p>
        </div>
        <span class="clearfix"></span>
        <form action="{{route('type.update')}}" method="post" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}

            <input type="hidden" name="m" value="{{$model}}">
            <input type="text" name="id" value="{{$type->id}}" hidden>

            <div class="row">
                @if($type->name)
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="name" type="text" placeholder="Name" class="form-control"
                                   value="{{$type->name}}"/>
                        </div>
                    </div>
                @endif

                @if(showThisField( $model, 'username' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="username" type="text" placeholder="Username"
                                   value="{{$type->username}}" class="form-control"/>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'attach_to_email' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="attach_to_email" class="custom-select mr-sm-2">
                                <option value="" disabled>Attach To Email</option>
                                <option value="1" @if($type->attach_to_email == 1) selected @endif >Yes</option>
                                <option value="0" @if($type->attach_to_email == 0) selected @endif >No</option>
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'claim_type_id' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="claim_type_id" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Claim Types</option>
                                @foreach(\App\Models\ClaimType::get() as $claim)
                                    <option @if($type->claim_type_id === $claim->id) selected
                                            @endif value="{{$claim->id}}">{{$claim->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf


                @if(showThisField( $model, 'template_type_id' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="template_type_id" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Template Types</option>
                                @foreach(\App\Models\EmailTemplateType::get() as $template)
                                    <option @if($type->template_type_id === $template->id) selected
                                            @endif value="{{$template->id}}">{{$template->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'service_charge' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="service_charge" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Service Charge</option>
                                <option @if($type->service_charge == 1) selected @endif value="1">Yes</option>
                                <option @if($type->service_charge == 0) selected @endif value="0">No</option>
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'button' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="button" type="text" placeholder="Button" value="{{$type->button}}"
                                   class="form-control"/>
                        </div>
                    </div>
                @endIf

            </div>

            @if(showThisField( $model, 'subject' ))
                <div class="col-6 form-group">
                    <div class="form-group">
                        <input required name="subject" value="{{$type->subject}}" type="text" placeholder="Subject"
                               class="form-control"/>
                    </div>
                </div>
            @endIf


            @if(showThisField( $model, 'body' ))
                <div class="col-6 form-group">
                    <div class="form-group">
                        <textarea required name="body" type="text" placeholder="Body"
                                  class="form-control">{{$type->body}}</textarea>
                    </div>
                </div>
            @endIf


            <div class="row">
                @if(showThisField( $model, 'currency_code' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="currency_code" type="text" placeholder="Currency Code"
                                   value="{{$type->currency_code}}" class="form-control"/>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'symbol' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="symbol" type="text" placeholder="Symbol" value="{{$type->symbol}}"
                                   class="form-control"/>
                        </div>
                    </div>
                @endIf

            </div>

            <div class="row">
                @foreach(userInputValues() as $inputValue)
                    @if(showThisField( $model, $inputValue['name'] ))
                        <div class="col-4 form-group">
                            <div class="form-group">
                                <input required
                                       @foreach($inputValue as $key => $value)
                                       {{$key}}="{{$value}}" value="{{$type->$value}}"
                                       @endforeach
                                       class="form-control"/>
                            </div>
                        </div>
                    @endIf
                @endforeach

                @if(showThisField( $model, 'avatar' ))
                    <div class="col-4">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                @endIf

            </div>

            <div class="row">
                @if(showThisField( $model, 'client_id' ))
                    <div class="col-4 form-group">
                        <div class="form-group">
                            @php $clients =  $type->clients()->pluck('id')->toArray() @endphp
                            <select multiple required name="client_id[]" class="custom-select mr-sm-2">
                                @foreach(\App\Models\Client::get() as $client)
                                    <option @if(in_array($client->id,$clients)) selected
                                            @endif value="{{$client->id}}">{{$client->client_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'region_id' ))
                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select required name="region_id" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Region</option>
                                @foreach(\App\Models\Region::get() as $region)
                                    <option @if($region->id == $type->region_id) selected
                                            @endif  value="{{$region->id}}">{{$region->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $model, 'read_only' ))
                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select required name="read_only" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Read Only</option>
                                <option @if($type->read_only == 1) selected @endif value="1">Yes</option>
                                <option @if($type->read_only == 0) selected @endif  value="0">No</option>
                            </select>
                        </div>
                    </div>
                @endIf
            </div>


            <button type="submit" class="btn mr-2 mb-2 btn-outline-success">Update Changes</button>
            <button class="go-back btn mr-2 mb-2 btn-outline-dark" type="button">Back</button>

        </form>

    </div>
</div>



