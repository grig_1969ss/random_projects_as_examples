<div id="add-page" class="col-12  new-doc-form sr-only">
    <div>
        <div class="mb-5 text-center">
            <p>Add {{displayName($data['currentModel'])}} and save</p>
        </div>
        <span class="clearfix"></span>
        <form action="{{route('type.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="m" value="{{$data['currentModel']}}">
            <div class="row">

                @if($data['currentModel'] !== 'EmailTemplate')
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="name" type="text" placeholder="Name" class="form-control"/>
                        </div>
                    </div>
                @endif

                @if(showThisField( $data['currentModel'], 'username' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="username" type="text" placeholder="Username" class="form-control"/>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'subject' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="subject" type="text" placeholder="Subject" class="form-control"/>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'body' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <textarea required name="body" type="text" placeholder="Body" class="form-control"></textarea>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'attach_to_email' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="attach_to_email" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Attach To Email</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'claim_type_id' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="claim_type_id" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Claim Types</option>
                                @foreach(\App\Models\ClaimType::cursor() as $claim)
                                    <option value="{{$claim->id}}">{{$claim->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf

                    @if(showThisField( $data['currentModel'], 'template_type_id' ))
                        <div class="col-6 form-group">
                            <div class="form-group">
                                <select required name="template_type_id" class="custom-select mr-sm-2">
                                    <option value="" disabled selected>Template Type</option>
                                    @foreach(\App\Models\EmailTemplateType::cursor() as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endIf

                @if(showThisField( $data['currentModel'], 'service_charge' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <select required name="service_charge" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Service Charge</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'button' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="button" type="text" placeholder="Button" class="form-control"/>
                        </div>
                    </div>
                @endIf

            </div>

            <div class="row">
                @if(showThisField( $data['currentModel'], 'currency_code' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="currency_code" type="text" placeholder="Currency Code"
                                   class="form-control"/>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'symbol' ))
                    <div class="col-6 form-group">
                        <div class="form-group">
                            <input required name="symbol" type="text" placeholder="Symbol" class="form-control"/>
                        </div>
                    </div>
                @endIf
            </div>


            <div class="row">
                @foreach(userInputValues() as $inputValue)
                    @if(showThisField( $data['currentModel'], $inputValue['name'] ))
                        <div class="col-4 form-group">
                            <div class="form-group">
                                <input required
                                       @foreach($inputValue as $key => $value)
                                       {{$key}}="{{$value}}"
                                       @endforeach
                                       class="form-control"/>
                            </div>
                        </div>
                    @endIf
                @endforeach

                @if(showThisField( $data['currentModel'], 'avatar' ))
                    <div class="col-4">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="avatar" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                @endIf
            </div>

            <div class="row">
                @if(showThisField( $data['currentModel'], 'client_id' ))
                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select multiple required name="client_id[]" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Clients</option>
                                @foreach(\App\Models\Client::get() as $claim)
                                    <option value="{{$claim->id}}">{{$claim->client_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'region_id' ))
                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select required name="region_id" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Region</option>
                                @foreach(\App\Models\Region::get() as $claim)
                                    <option value="{{$claim->id}}">{{$claim->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endIf

                @if(showThisField( $data['currentModel'], 'read_only' ))
                    <div class="col-4 form-group">
                        <div class="form-group">
                            <select required name="read_only" class="custom-select mr-sm-2">
                                <option value="" disabled selected>Read Only</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                @endIf
            </div>


            <button type="submit" class="btn mr-2 mb-2 btn-outline-success">Save</button>
            <button class="go-back btn mr-2 mb-2 btn-outline-dark" type="button">Back</button>

        </form>

    </div>
</div>


