@extends('layouts_new.app',['page' => 'Settings Crud', 'pageTitle' => 'Settings'])

@section('container')
    <div class="section section-md">
        <div class="container-fluid">
            <div class="row justify-content-between">

                @if ($errors->any())
                    <div class="alert alert-danger text-center">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{$errors->first()}}
                    </div>
                @endif

                <aside class="col-12 col-lg-3 mt-3 mt-lg-0 main_left">
                    <div class="card shadow-soft border-soft p-3 mt-4"><h5 class="mb-4">Settings</h5>
                        <p>Use this screen to update the app settings, such as drop-down options and user settings.</p>
                        <ul class="list-group list-group-flush bg-white shadow-sm border border-soft">
                            @foreach(settingsMenu() as $key => $menu)
                                <li data-type="{{$key}}"
                                    class="{{$key == $data['currentModel'] ? 'active-menu' : '' }}  list-group-item d-flex justify-content-between align-items-center border-0 setting-menu">
                                    <a
                                        href="?m={{$key}}">{{$menu}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>

                <div class="col-lg-9 pr-lg-4">
                    <div class="my-5">
                        <div class="mb-5">
                            <div class="spinners text-center sr-only">
                                <div class="spinner spinner-bounce-bottom"></div>
                            </div>
                            @include('admin.account.settings.indexTable')
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{ asset('js/custom.js') }}"></script>
@endpush
