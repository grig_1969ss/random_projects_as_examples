<?php $clientId = isset($staff->client_id) ? 1 : 0; ?>

<fieldset>
    <div id='error-message'></div>
    {{ Form::open(['route' => ($staff->id ? ['staff.update', $staff] : 'staff.store'), 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => 'save-staff']) }}
    <div id="name" class="control-group">
        {{ Form::label('name', 'Name', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('name',isset($staff)?$staff->name:null, array('class' => 'form-control')) }}
            <span id="name" class="help-block"></span>
        </div>
    </div>
{{--    <div id="username" class="control-group">--}}
{{--        {{ Form::label('username', 'Username', array('class' => 'control-label')) }}--}}
{{--        <div class="form-group controls">--}}
{{--            {{ Form::text('username',isset($staff)?$staff->username:null, array('class' => 'form-control')) }}--}}
{{--            <span id="username" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div id="email" class="control-group">
        {{ Form::label('email', 'Email', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('email',isset($staff)?$staff->email:null, array('class' => 'form-control')) }}
            <span id="email" class="help-block"></span>
        </div>
    </div>
    @if(!isset($staff->id))
        <div id="password" class="control-group">
            {{ Form::label('password', 'Password', array('class' => 'control-label')) }}
            <div class="form-group controls">
                {{ Form::input('password', 'password', isset($staff)?$staff->password:null, array('class' => 'form-control')) }}
                <span id="password" class="help-block"></span>
            </div>
        </div>
        <div id="confirm_password" class="control-group">
            {{ Form::label('confirm_password', 'Confirm Password', array('class' => 'control-label')) }}
            <div class="form-group controls">
                {{ Form::input('password', 'confirm_password', isset($staff)?$staff->confirm_password:null, array('class' => 'form-control')) }}
                <span id="confirm_password" class="help-block"></span>
            </div>
        </div>
    @endif
    <div id="job_title" class="control-group">
        {{ Form::label('job_title', 'Job Title', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::text('job_title',isset($staff)?$staff->job_title:null, array('class' => 'form-control')) }}
            <span id="job_title" class="help-block"></span>
        </div>
    </div>
    <div id="avatar" class="control-group">
        {{ Form::label('avatar', 'Avatar', array('class' => 'control-label')) }}
        <input id="avatar" name="avatar" type="file" style="display:none">
        <div class="form-group controls">
            <input id='input' name="avatar" class="form-control" type="text" onclick="$('input[id=avatar]').click()"/>
            <a class="btn" onclick="$('input[id=avatar]').click()">Browse</a>
            <span id="avatar" class="help-block">Select a file to upload</span>
        </div>
    </div>
    <div id="role_id" class="control-group">
        {{ Form::label('role_id', 'Role', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::select('role_id', $rolesDropDown, $staff->id ? $staff->roles()->first()->id : null, array('class' => 'form-control add-new-option')) }}
            <span id="role_id" class="help-block"></span>
        </div>
    </div>
    <div id="client_id" class="control-group" style='display:block'>
        {{ Form::label('client_id', 'Client', array('class' => 'control-label')) }}
        <div class="form-group controls">
            {{ Form::select('client_id[]', $clientsDropDown, $staff->clients()->pluck('id')->toArray(), ['multiple'=>'multiple','class' => 'form-control']  ) }}
            <span id="client_id" class="help-block"></span>
        </div>
    </div>
    <div id="verified" class="control-group">
        {{ Form::label('verified', 'Verified', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::checkbox('verified', '1', $staff->verified)}}
            <span id="verified" class="help-block"></span>
        </div>
    </div>
    <div id="disabled" class="control-group">
        {{ Form::label('disabled', 'Disabled', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::checkbox('disabled', '1', $staff->disabled)}}
            <span id="disabled" class="help-block"></span>
        </div>
    </div>
    @if(isset($staff->id))
        {{ Form::hidden('id', $staff->id) }}
    @endif
    {{ Form::hidden('is_client', $clientId, array('id' => 'is_client')) }}
<!-- submit button -->
    <div class="form-actions">
        <button id="save-staff" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
            Save Changes
        </button>
    </div>
    {{ Form::close()}}
</fieldset>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
