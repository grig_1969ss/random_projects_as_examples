@extends('layouts_new.app', ['pageTitle' => 'Staff', 'pageNav' => 'settings', 'page' => 'StaffDetail'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">{{ $staff->name }}</h5>
                    </div>
                    <div class="dropdown">
                        <div class="btn-group mr-2 mb-2">
                            <a href="{{route('staff.edit', $staff->id)}}" class="btn add-property btn-outline-primary">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div class="col-md-12">

                        <div class="row pt-3 align-items-center">
                            <div class="col-sm-3">
                                <small class="text-uppercase text-muted">Username</small>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <p class="mb-0">{{ $staff->username }}</p>
                            </div>
                        </div>

                        <div class="row pt-3 align-items-center">
                            <div class="col-sm-3">
                                <small class="text-uppercase text-muted">Email</small>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <p class="mb-0">{{ $staff->email }}</p>
                            </div>
                        </div>

                        <div class="row pt-3 align-items-center">
                            <div class="col-sm-3">
                                <small class="text-uppercase text-muted">Job Title</small>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <p class="mb-0">{{ $staff->job_title}}</p>
                            </div>
                        </div>

                        <div class="row pt-3 align-items-center">
                            <div class="col-sm-3">
                                <small class="text-uppercase text-muted">Role</small>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <p class="mb-0">{{ $staff->role_name}}</p>
                            </div>
                        </div>

                        <div class="row pt-3 align-items-center">
                            <div class="col-sm-3">
                                <small class="text-uppercase text-muted">Clients</small>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <p class="mb-0">
                                    @foreach($staff->clients as $client)
                                        {{ $client->client_name}}<br>
                                    @endforeach
                                </p>
                            </div>
                        </div>

                        <div class="row pt-3 align-items-center">
                            <div class="col-sm-3">
                                <small class="text-uppercase text-muted">Added</small>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <p class="mb-0">
                                    {{ \App\Helpers\Date::dmYHi($staff->created_at) }}
                                    <br>by {{ $staff->creator_name }}
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $staff->name }}</h2>
                        <ul class="data-header-actions">
                            <li><a href="/settings/staff/{{ $staff->id }}/edit" class="btn btn-alt">Edit</a></li>
                        </ul>
                    </header>

                    <section>
                        <dl class="dl-horizontal">
                            <dt>Username</dt>
                            <dd>{{ $staff->username }}</dd>
                            <dt>Email</dt>
                            <dd>{{ $staff->email }}</dd>
                            <dt>Job Title</dt>
                            <dd>{{ $staff->job_title}}</dd>
                            <dt>&nbsp;</dt>
                            <dd>&nbsp;</dd>
                            <dt>Role</dt>
                            <dd>{{ $staff->role_name}}</dd>
                            <dt>Clients</dt>
                            @foreach($staff->clients as $client)
                                <dd>{{ $client->client_name}}</dd>
                            @endforeach
                            <dt>&nbsp;</dt>
                            <dd>&nbsp;</dd>
                            <dt>Added</dt>
                            <dd>{{ \App\Helpers\Date::dmYHi($staff->created_at) }}
                                <br>by {{ $staff->creator_name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
            <!-- /Data block -->
        </div>
        <!-- /Grid row -->
    </div>
    <!-- /Right (content) side -->
@endsection
