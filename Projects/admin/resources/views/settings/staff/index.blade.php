@extends('layouts_new.app', ['pageTitle' => 'Staff', 'pageNav' => 'settings', 'page' => 'StaffIndex'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Staff</h5>
                    </div>
                    <div class="dropdown">
                        <div class="btn-group mr-2 mb-2">
                            <a  href="{{ route('staff.create') }}" class="btn add-property btn-outline-primary">Add Staff Member</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table id="staff-table" class="datatable table table-striped table-bordered table-hover dataTable table-responsive" style="display: inline-table">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Client</th>
                                <th scope="col">Disabled</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($staffs as $staff)
                                <tr class="odd gradeX">
                                    <td>{{ Html::link('/settings/staff/'.$staff->id, $staff->name) }}</td>
                                    <td>{{ Html::link('/settings/staff/'.$staff->id, $staff->email) }}</td>
                                    <td><a href="{{ route('staff.show', $staff) }}">{{ $staff->present()->client }}</a></td>
                                    <td>
                                        <a href="{{ route('staff.show', $staff) }}">{{ $staff->present()->disabled }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

{{--                        <nav class="property-list-pagination" aria-label="Page navigation example">--}}
{{--                            <span>Showing {{$staffs->firstItem()}} to {{$staffs->lastItem()}} of {{$staffs->total()}} entries</span>--}}
{{--                            {{$staffs->links()}}--}}
{{--                        </nav>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Right (content) side -->
{{--    <div class="content-block" role="main">--}}

{{--        <!-- Grid row -->--}}
{{--        <div class="row">--}}

{{--            <!-- Data block -->--}}
{{--            <article class="span12 data-block">--}}
{{--                <div class="data-container">--}}
{{--                    <header>--}}
{{--                        <h2>Staff</h2>--}}
{{--                        <ul class="data-header-actions">--}}
{{--                            <li class="demoTabs">--}}
{{--                                <a href="{{ route('staff.create') }}" class="btn btn-alt">Add Staff Member</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </header>--}}
{{--                    <table class="datatable table table-striped table-bordered table-hover" id="example">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>Name</th>--}}
{{--                            <th>Email</th>--}}
{{--                            <th>Client</th>--}}
{{--                            <td>Disabled</td>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach ($staffs as $staff)--}}
{{--                            <tr class="odd gradeX">--}}
{{--                                <td>{{ Html::link('/settings/staff/'.$staff->id, $staff->name) }}</td>--}}
{{--                                <td>{{ Html::link('/settings/staff/'.$staff->id, $staff->email) }}</td>--}}
{{--                                <td><a href="{{ route('staff.show', $staff) }}">{{ $staff->present()->client }}</a></td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('staff.show', $staff) }}">{{ $staff->present()->disabled }}</a>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            </article>--}}
{{--            <!-- /Data block -->--}}
{{--        </div>--}}
{{--        <!-- /Grid row -->--}}
{{--    </div>--}}
    <!-- /Right (content) side -->
@endsection
@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
