@extends('layouts_new.app', [
    'pageTitle' => ($staff->id ? 'Edit' : 'New').' Staff Member',
    'pageNav' => 'settings',
    'page' => 'StaffForm'
])

@section('container')
    <div class="container-fluid ">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-4 order-md-1">
                <div class="mb-5 text-center">
                    <h6 class="h3">{{ ($staff->id ? 'Edit' : 'New') }} Staff Member</h6>
                    <div class="dropdown-divider"></div>
                </div>
                @include('settings.staff.includes.form', [
                    'title'      => ($staff->id ? 'Edit' : 'Add').' Staff Member',
                    'restMethod' => ($staff->id ? 'PUT' : 'POST')
                ])
            </div>
        </div>
    </div>
@endsection
