<fieldset>
    <div id='error-message'></div>
    <form method="POST"
          action="{{ $claimSubtype->id ? route('claim-subtypes.update', $claimSubtype) : route('claim-subtypes.store') }}"
          class="form-horizontal">
        @if($claimSubtype->id)
            {{ method_field('PUT') }}
        @endif

        @csrf

        <div id="ctrl-grp-name" class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
                <input type="text" name="name" id="name" class="input-medium" value="{{ $claimSubtype->name }}">
                <span id="help-name" class="help-block"></span>
            </div>

            <label class="control-label" for="claim_type_id">Claim Type</label>
            <div class="controls">
                <select name="claim_type_id" id="claim_type_id" class="input-medium">
                    <option value="">Select...</option>
                    @foreach($claimTypes as $claimType)
                        <option value="{{ $claimType->id }}"
                                {{ $claimSubtype->claim_type_id == $claimType->id ? 'selected' : '' }}>
                            {{ $claimType->name }}
                        </option>
                    @endforeach
                </select>
                <span id="help-name" class="help-block"></span>
            </div>
        </div>

        <!-- submit button -->
        <div class="form-actions">
            <button class="btn btn-large btn-primary" type="submit">Save Changes</button>
        </div>
    </form>
</fieldset>