<fieldset>
    <div id='error-message'></div>
    <form method="POST"
          action="{{ $currency->id ? route('currencies.update', $currency) : route('currencies.store') }}"
          class="form-horizontal">
        @if($currency->id)
            {{ method_field('PUT') }}
        @endif

        @csrf

        <div id="ctrl-grp-name" class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
                <input type="text" name="name" id="name" class="input-medium" value="{{ $currency->name }}">
                <span id="help-name" class="help-block"></span>
            </div>

            <label class="control-label" for="currency_code">Currency code</label>
            <div class="controls">
                <input type="text"
                       name="currency_code"
                       id="currency_code"
                       class="input-medium"
                       value="{{ $currency->currency_code }}">
                <span id="help-name" class="help-block"></span>
            </div>

            <label class="control-label" for="symbol">Symbol</label>
            <div class="controls">
                <input type="text" name="symbol" id="symbol" class="input-medium" value="{{ $currency->symbol }}">
                <span id="help-name" class="help-block"></span>
            </div>
        </div>

        <!-- submit button -->
        <div class="form-actions">
            <button class="btn btn-large btn-primary" type="submit">Save Changes</button>
        </div>
    </form>
</fieldset>