<fieldset>
    <div id='error-message'></div>
    <form method="POST"
          action="{{ $paymentType->id ? route('payment-types.update', $paymentType) : route('payment-types.store') }}"
          class="form-horizontal">
        @if($paymentType->id)
            {{ method_field('PUT') }}
        @endif

        @csrf

        <div id="ctrl-grp-name" class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
                <input type="text" name="name" id="name" class="input-medium" value="{{ $paymentType->name }}">
                <span id="help-name" class="help-block"></span>
            </div>

            <label class="control-label" for="service_charge">Service charge?</label>
            <div class="controls">
                <input type="hidden" name="service_charge" value="0">
                <input type="checkbox"
                       name="service_charge"
                       id="service_charge"
                       value="1"
                        {{ $paymentType->service_charge ? 'checked' : '' }}>
            </div>
        </div>

        <!-- submit button -->
        <div class="form-actions">
            <button class="btn btn-large btn-primary" type="submit">Save Changes</button>
        </div>
    </form>
</fieldset>