@extends('layouts_new.app', ['pageTitle' => 'Settings', 'pageNav' => 'settings', 'page' => 'SettingsIndex'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

        <?php foreach ($typesArray as $title => $modelArray) { ?>
        <?php foreach ($modelArray as $model => $typeList) { ?>
        <!-- Data block -->
            <article class="span4 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ str_plural($title) }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="#" onclick='addType("{{$model}}", "{{$title}}")' class="btn btn-alt btn-info">Add New</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <table class="table table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Name</th>
                                    @if ($read_only == 0)
                                        <th>&nbsp;</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $first = true;
                                if (isset($typeList)) {
                                    foreach ($typeList as $id => $name) { ?>
                                <tr>
                                    <td>
                                        @if ($read_only == 0)
                                            <a href="#" onclick='editType("{{$model}}", "{{$id}}", "{{$name}}", "{{$title}}")'>
                                                @endif
                                                {{$id}}
                                                @if ($read_only == 0)
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($read_only == 0)
                                            <a href="#" onclick='editType("{{$model}}", "{{$id}}", "{{$name}}", "{{$title}}")'>
                                                @endif
                                                {{$name}}
                                                @if ($read_only == 0)
                                            </a>
                                        @endif
                                    </td>
                                    @if ($read_only == 0)
                                        <td class="toolbar">
                                            <div class="btn-group">
                                                <button onclick='deleteType("{{$model}}", "{{$id}}", "{{$title}}")' class="btn btn-flat"><span class="awe-remove"></span></button>
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                                <?php }
                                } ?>
                                </tbody>
                            </table>
                        </dl>
                    </section>
                </div>
            </article>
            <!-- /Data block -->
            <?php } ?>
            <?php } ?>

        </div>
        <!-- /Grid row -->

        <!-- Div for rendering Add Type Form -->
        <div id='addTypeForm'>
            <div class="modal fade hide" id="addTypeModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3 id='type-form-header'>Add Type</h3>
                </div>
                <div class="modal-body">
                    @include('partials.type_form', ['restMethod' => 'post', 'isDelete' => false, 'formId'=>'add-type'])
                </div>
            </div>

            @include('partials.base_modal')

            <div id='editTypeForm'>
                <div class="modal fade hide" id="editTypeModal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3 id='type-form-header'>Edit Type</h3>
                    </div>
                    <div class="modal-body">
                        @include('partials.type_form', ['restMethod' => 'put', 'isDelete' => false, 'formId'=>'edit-type'])
                    </div>
                </div>
                <div id='deleteTypeForm'>
                    <div class="modal fade hide" id="deleteTypeModal">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h3 id='type-form-header'>Delete Type</h3>
                        </div>
                        <div class="modal-body">
                            @include('partials.type_form', ['restMethod' => 'delete', 'isDelete' => true, 'formId' => 'delete-type'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
