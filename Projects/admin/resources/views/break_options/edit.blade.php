@extends('layouts_new.app', [
    'pageTitle' => ($break->id ? 'Edit' : 'Add').' Break Option',
    'pageNav' => 'property',
    'page' => 'PropertyBreakOption'
])

@section('container')
    @include('break_options.includes.form', [
        'title' => ($break->id ? 'Edit' : 'Add').' Break Option',
    ])
@endsection
