<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-md-5 col-lg-4 order-md-1">
            <div class="mb-5 text-center">
                <h6 class="h3">{{ $title }}</h6>
                <div class="dropdown-divider"></div>
            </div>

            <form method="POST"
                  action="{{ $break->id ? route('breaks.update', $break) : route('properties.breaks.store', $property) }}"
                  class="form-horizontal"
                  id="create-break-option">

                {{ csrf_field() }}

                @if($break->id)
                    {{ method_field('PUT') }}
                @endif

                <div id='error-message'></div>

                <div id="break_clause_type_id" class="control-group">
                    {{ Form::label('break_clause_type_id', 'Break Clause', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        {{ Form::select('break_clause_type_id', $breakClauseTypeDropdown, $break->break_clause_type_id, array('id'=>'break_clause_type_id', 'class' => 'form-control')) }}
                        <span id="break_clause_type_id" class="help-block"></span>
                    </div>
                </div>


                <div id="break_date" class="control-group">
                    {{ Form::label('break_date', 'Break Option Date', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        <div class="input-append">
                            {{ Form::text('break_date',Date::dmy($break->break_date), array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                            <span class="add-on"><i class="awe-calendar"></i></span>
                        </div>
                        <span id="break_date" class="help-block"></span>
                    </div>
                </div>

                <div id="notice_period" class="control-group">
                    {{ Form::label('notice_period', 'Notice Date', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        <div class="input-append">
                            {{ Form::text('notice_period',Date::dmy($break->notice_period), array('class' => 'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off')) }}
                            <span class="add-on"><i class="awe-calendar"></i></span>
                        </div>
                        <span id="notice_period" class="help-block"></span>
                    </div>
                </div>

                <div id="number_of_days" class="control-group">
                    {{ Form::label('number_of_days', 'No. of Days', array('class' => 'control-label')) }}
                    <div class="form-group controls">
                        {{ Form::text('number_of_days',$break->number_of_days, array('class' => 'form-control')) }}
                        <span id="number_of_days" class="help-block"></span>
                    </div>
                </div>

                <div id="break_clause" class="control-group">
                    <label class="control-label" for="break_clause">Details</label>
                    <div class="form-group controls">
                        <textarea id="break_clause" class="form-control" name="break_clause"
                                  rows="3">{{ $break->break_clause }}</textarea>
                        <span id="break_clause" class="help-block"></span>
                    </div>
                </div>

                <div class="form-actions">
                    <button id="add-break" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
                        Save Changes
                    </button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
