<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ site_title($pageTitle) }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="description" content="{{ config('app.meta_description') }}">
    <meta name="author" content="Up Technology Ltd"><!-- Favicon -->
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('dist/img/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('dist/img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('dist/img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('dist/img/favicon/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('dist/img/favicon/safari-pinned-tab.svg')}}" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="{{ mix('/css/all.css') }}" rel="stylesheet">
</head>
<body>



@yield('content')

<script src="{{asset('/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('dist/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dist/vendor/headroom/headroom.min.js')}}"></script><!-- Vendor JS -->
<script src="{{asset('dist/vendor/onscreen/onscreen.min.js')}}"></script>
<script src="{{asset('dist/vendor/nouislider/js/nouislider.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('dist/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('dist/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('dist/vendor/owl-carousel/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('dist/vendor/parallax/jarallax.min.js')}}"></script>
<script src="{{asset('dist/vendor/smooth-scroll/smooth-scroll.polyfills.min.js')}}"></script>
<script src="{{asset('dist/vendor/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('dist/vendor/countdown/jquery.countdown.min.js')}}"></script>
<script src="{{asset('dist/vendor/prism/prism.js')}}"></script><!-- pixel JS -->
<script src="{{asset('dist/js/pixel.js')}}"></script>


</body>
</html>
