<html lang="en" data-reactroot="">
<head>
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta charset="utf-8">
    <title>{{ config('app.name') }}</title>
    <meta content="website" property="og:type">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('css/style.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('dist/css/pixel.css') }}" rel="stylesheet">
    <link async="" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</head>
<body>
<div id="root">
    <div class="uik-App__app">
        @yield('content')
    </div>
</div>

<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
</body>
</html>
