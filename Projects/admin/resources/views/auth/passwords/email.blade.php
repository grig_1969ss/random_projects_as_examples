@extends('auth.app',['pageTitle' => 'passwordReset'])

@section('content')
    <div class="container-fluid">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('status') }}
            </div>
        @endif

        <div class="row justify-content-center text-center py-6 mb-2">
            <div class="col-lg-8 col-xl-8">
                <h1 class="display-2 font-weight-light mb-3">{{ __('Reset Password') }}</span>
                </h1>
                <form class="row d-flex align-items-top mt-3" method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="col-8 col-sm-6 col-xl-8 offset-sm-2">
                        <div class="form-group">
                            <div
                                @error('email') style="border: 1px solid #fa5252" @enderror
                            class="d-flex flex-row mt-5 justify-content-center">
                                <div class="input-group">
                                    <input class="form-control form-control-xl border-0" name="email"
                                           placeholder="Your email address" type="email" value="{{ old('email') }}">
                                </div>
                            </div>
                            @error('email')
                            <span class="invalid-feedback" style="display: block">
                                  <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="mt-4">
                                <button type="submit"
                                        class="btn btn-block mr-2 mb-2 btn-outline-primary">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
