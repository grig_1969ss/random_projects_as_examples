@extends('auth.app', ['pageTitle' => 'resetPassword'])

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5 col-lg-4 order-md-1 py-6">
                <div>
                    <div class="mb-5 text-center">
                        <h6 class="h3">Reset Password</h6>
                    </div>
                    <span class="clearfix"></span>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-user"></i></span>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ $email ?? old('email') }}" required autocomplete="email"
                                       autofocus>

                            </div>
                            @error('email')
                            <span class="invalid-feedback" style="display: block">
                                  <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>
                                </div>
                                <input class="form-control" name="password" placeholder="Password" type="password"
                                       required="">
                            </div>
                            @error('password')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                   <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>
                                </div>
                                <input id="password-confirm" name="password_confirmation" type="password"
                                       class="form-control"
                                       placeholder="Confirm password" required="">
                            </div>
                        </div>
                        <div class="mt-4">
                            <button class="btn btn-block mr-2 mb-2 btn-outline-primary"
                                    type="submit">{{ __('Reset Password') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
