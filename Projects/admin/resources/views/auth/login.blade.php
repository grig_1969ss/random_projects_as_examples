@extends('auth.app', ['pageTitle' => 'Login'])
@section('content')
    <main>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <span class="alert-inner--icon"></span>
                <span class="alert-inner--text">{{$errors->first()}}</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        @endif


        <section class="min-vh-100 d-flex align-items-center">
            <div class="container">
                <div class="row align-items-center justify-content-center">

                    <div class="col-8 col-md-6 col-lg-7 offset-md-1 order-md-2"><!-- Image --> <img
                            src="{{asset('dist/img/pages/signin.svg')}}" alt="..." class="img-fluid"></div>
                    <div class="col-12 col-md-5 col-lg-4 order-md-1">
                        <div>
                            <div class="btn-wrapper mt-4 mt-md-0 text-center">
                                <img class="login-logo" src="{{asset('/img/moved/icons/dark.svg')}}" alt="CAPA Logo">
                            </div>
                            <div class="text-center text-md-center mt-4 mb-5 ">
                                <h1>Sign in</h1>
                            </div>
                            <span class="clearfix"></span>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i
                                                    class="far fa-user"></i></span></div>
                                        <input
                                            name="email"
                                            type="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            id="input-email"
                                            placeholder="Enter email"
                                            value="{{ old('email') }}"
                                            required>
                                        @error('email')
                                        <p class="">{{ $errors->first('email') }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i
                                                    class="fas fa-unlock-alt"></i></span></div>
                                        <input
                                            name="password"
                                            class="form-control"
                                            placeholder="Password"
                                            type="password"
                                            required>
                                        @error('password')
                                        <p class="">{{ $errors->first('password') }}</p>
                                        @enderror

                                        <div class="input-group-append"><span class="input-group-text"><i
                                                    class="far fa-eye"></i></span></div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-block btn-secondary">Sign in</button>
                                </div>
                            </form>


                            <div class="d-block d-sm-flex justify-content-between align-items-center mt-4">
                                <span>
                                <div><a href="{{ route('password.request') }}"
                                        class="small text-right">Lost password?</a></div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- Core -->

@endsection
