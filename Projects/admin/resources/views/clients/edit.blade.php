@extends('layouts_new.app',[
    'pageTitle' => ($client->id ? 'Edit' : 'Add').' Client',
    'pageNav'   => 'client',
    'page'      => 'ClientNew',
])

@section('container')
    @include('clients.includes.form', [
                              'title'      => ($client->id ? 'Edit' : 'New').' Client',
                              'restMethod' => $client->id ? 'put' : 'post',
                              'isModal'    => false,
                              'route'      => $client->id ? ['clients.update', $client] : 'clients.store'
    ])
@endsection
