@extends('layouts_new.app', ['pageTitle' => 'Clients', 'pageNav' => 'client', 'page' => 'ClientsIndex'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Clients</h5>
                    </div>
                    <div class="dropdown">
                        @if(!$read_only)
                            <div class="btn-group mr-2 mb-2">
                                <a href="{{route('clients.create')}}" class="btn add-property btn-outline-primary">Add Client</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tbody>
                            @foreach ($clients as $client)
                                <tr>
                                    <td>{{ Html::link('/clients/'.$client->id, $client->client_name ? $client->client_name :'No Name Given'	) }}</td>
                                    <td>{{ Html::link('/clients/'.$client->id, $client->telephone ? $client->telephone :'No Telephone Given') }}</td>
                                    <td>{{ Html::link('/clients/'.$client->id, $client->email ? $client->email :'No Email Given') }}</td>
                                    <td>{{ Html::link('/contacts/'.$client->contact_id, Util::joinName(array( $client->first_name, $client->surname ))) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <nav class="property-list-pagination default-pagination" aria-label="Page navigation example">

                            @if($clients->total())
                                <span>Showing {{$clients->firstItem()}} to {{$clients->lastItem()}} of {{$clients->total()}} entries</span>
                            @endif

                            {{$clients->links()}}
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


