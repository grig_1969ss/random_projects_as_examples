@extends('layouts_new.app', ['pageTitle' => 'Client', 'pageNav' => 'client', 'page' => 'ClientsDetail'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <div class="row">


                <div class="col-6">

                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div class="row">
                                <div class="navbar-brand">
                                    <img src="{{asset('/storage/client_logos/'.$client->logo)}}" alt="">
                                </div>
                                <h5 class="font-weight-bold">{{ $client->client_name }}</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a href="{{ route('clients.edit', $client) }}"
                                       class="btn add-property btn-outline-primary">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-3 align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Client Name</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">{{ $client->client_name }}</p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Address</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ $client->address->address_1 }}<br>
                                {{ $client->address->address_2 }}<br>
                                {{ $client->address->town }}<br>
                                {{ $client->address->county }}<br>
                                {{ $client->address->post_code }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Telephone</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($client->telephone) }}<br>
                                {{ Util::showValue($client->mobile) }}
                            </p>
                        </div>
                    </div>

                    @if($client->fax)
                        <div class="row align-items-center">
                            <div class="col-5">
                                <small class="text-uppercase text-muted">Fax</small>
                            </div>
                            <div class="col-7 pl-0">
                                <p class="mb-0">
                                    {{ $client->fax }}
                                </p>
                            </div>
                        </div>
                    @endif

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Email</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($client->email) }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Currency</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ $client->currency ? $client->currency->name : '-' }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Added</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{Date::dmYHi($client->created_at) }}
                                by {{ $client->user->name }}
                            </p>
                        </div>
                    </div>

                </div>
                <div class="col-6">
                    <!-- Title  -->
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">Contacts</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a href="{{ route('clients.contacts.create', $client) }}"
                                       class="btn add-property btn-outline-primary">Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-5">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Contact Name</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($client->contact as $contact)
                                        <?php $contactUrl = '/contacts/'.$contact->id; ?>
                                        <tr>
                                            <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->telephone) }} </td>
                                            <td>{{ Util::link($contactUrl, $contact->email) }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('partials.show_properties', [
                            'id' => $client->id,
                            'type'=> 'client',
                            'showClients' => true,
                            'header' => 'Client'
            ])


        </div>
    </div>




    <div class="content-block" role="main">
        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $client->client_name }}</h2>
                        <ul class="data-header-actions">
                            @if(!$read_only)
                                <li><a href="{{ route('clients.edit', $client) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>

                    <section>
                        <dl class="dl-horizontal">
                            <dt>Client Name</dt>
                            <dd>{{ $client->client_name }}</dd>
                            <dt>Address</dt>
                            <dd>{{ $client->address->address_1 }}</dd>
                            <dd>{{ $client->address->address_2 }}</dd>
                            <dd>{{ $client->address->town }}</dd>
                            <dd>{{ $client->address->county }}</dd>
                            <dd>{{ $client->address->post_code }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ Util::showValue($client->telephone) }}</dd>
                            <dd>{{ Util::showValue($client->mobile) }}</dd>
                            @if($client->fax)
                                <dt>Fax</dt>
                                <dd>{{ $client->fax }}</dd>
                            @endif
                            <dt>Email</dt>
                            <dd>{{ Util::showValue($client->email) }}</dd>
                            <dt>Currency</dt>
                            <dd>{{ $client->currency ? $client->currency->name : '-' }}</dd>
                            {{--<dt>Administrator</dt>
                            <dd>{{ $client->present()->isAdmin }}</dd>
                            <dt>Administrator Name</dt>
                            <dd>
                                @if($client->administrator)
                                    <a href="{{ route('administrators.show', $client->administrator) }}">
                                        {{ $client->administrator->name }}
                                    </a>
                                @else
                                    -
                                @endif
                            </dd>--}}
                            <dt>Added</dt>
                            <dd>{{Date::dmYHi($client->created_at) }}
                                by {{ $client->user->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
            <!-- /Data block -->

            <!-- Data block -->
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Contacts</h2>
                        <ul class="data-header-actions">
                            @if(!$read_only)
                                <li>
                                    <a href="{{ route('clients.contacts.create', $client) }}" class="btn btn-alt">
                                        Add
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </header>

                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Telephone</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($client->contact as $contact)
                            <?php $contactUrl = '/contacts/'.$contact->id; ?>
                            <tr class="odd gradeX">
                                <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->telephone) }} </td>
                                <td>{{ Util::link($contactUrl, $contact->email) }} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->

        </div>
        <!-- /Grid row -->

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <!-- Render Show Properties Partial -->
                @include('partials.show_properties', [
                    'id'          => $client->id,
                    'type'        => 'client',
                    'showClients' => false,
                    'header'      => 'Managing Agent'
                ])
            </article>
            <!-- /Data block -->

        </div>
        <div class="row">
            <!-- /Grid row -->
            <!-- Data block -->
            <article class="span6 data-block todo-block">
                <!-- Render Show Diaries Partial -->
                @include('partials.show_diary')
            </article>
            <!-- /Data block -->

            <!-- Grid row -->
            <div class="row">
                <!-- Render Show Notes Partial -->
                <article class="span6 data-block">
                    @include('partials.show_notes', ['id' => $client->id])
                </article>
            </div>
            <!-- Render Hidden Add Notes Partial -->
            @include('partials.add_comment', [
                'id' => $client->id,
                'type' => 'clients',
                'route' => ['clients.notes.store', $client]
            ])
        </div>
        <!-- /Grid row -->

    </div>
@endsection

