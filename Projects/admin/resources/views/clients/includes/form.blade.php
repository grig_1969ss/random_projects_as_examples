<?php $clientId = isset($client) ? $client->id : null ?>
<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-md-5 col-lg-4 order-md-1">
            <div class="mb-5 text-center">
                <h6 class="h3">{{$title}}</h6>
                <div class="dropdown-divider"></div>
            </div>
            <div id='error-message'></div>
            {{ Form::open(['route' => $route, 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => 'create-client','enctype'=>'multipart/form-data']) }}

            <div id="client_name" class="control-group">
                <label for="client_name" class="control-label">Client Name</label>
                <div class="form-group controls">
                    <input type="text" name="client_name" id="client_name"
                           class="form-control  @error('client_name') invalid-input @enderror"
                           value="{{ isset($client)?$client->client_name:null }}">
                    <span id="client_name" class="help-block invalid-text"></span>
                </div>
            </div>

            @if(!$clientId)
                <div id="contact_firstname" class="control-group">
                    <label for="contact_firstname" class="control-label">Contact FirstName</label>
                    <div class="form-group controls">
                        <input type="text" name="contact_firstname" id="contact_firstname"
                               class="form-control  @error('contact_firstname') invalid-input @enderror"
                               value="{{ isset($client->contact->first_name)?$client->contact->first_name.' '.$client->contact->surname:null }}">
                        <span id="contact_firstname" class="help-block invalid-text"></span>
                    </div>
                </div>
                <div id="contact_surname" class="control-group">
                    <label for="contact_surname" class="control-label">Contact Surname</label>
                    <div class="form-group controls">
                        <input type="text" name="contact_surname" id="contact_surname"
                               class="form-control  @error('contact_surname') invalid-input @enderror"
                               value="{{ isset($client->contact->first_name)?$client->contact->first_name.' '.$client->contact->surname:null }}">
                        <span id="contact_surname" class="help-block invalid-text"></span>
                    </div>
                </div>
            @endif

            <div id="address_1" class="control-group">
                <label for="address_1" class="control-label">Address Line 1</label>
                <div class="form-group controls">
                    <input type="text" name="address_1" id="address_1"
                           class="form-control  @error('address_1') invalid-input @enderror"
                           value="{{ isset($address)?$address->address_1:null }}">
                    <span id="address_1" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="address_2" class="control-group">
                <label for="address_2" class="control-label">Address Line 2</label>
                <div class="form-group controls">
                    <input type="text" name="address_2" id="address_2"
                           class="form-control  @error('address_2') invalid-input @enderror"
                           value="{{ isset($address)?$address->address_2:null }}">
                    <span id="address_2" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="town" class="control-group">
                <label for="town" class="control-label">Town</label>
                <div class="form-group controls">
                    <input type="text" name="town" id="town"
                           class="form-control  @error('town') invalid-input @enderror"
                           value="{{ isset($address)?$address->town:null }}">
                    <span id="town" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="county" class="control-group">
                <label for="county" class="control-label">County</label>
                <div class="form-group controls">
                    <input type="text" name="county" id="county"
                           class="form-control  @error('county') invalid-input @enderror"
                           value="{{ isset($address)?$address->county:null }}">
                    <span id="county" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="post_code" class="control-group">
                <label for="post_code" class="control-label">Post Code</label>
                <div class="form-group controls">
                    <input type="text" name="post_code" id="post_code"
                           class="form-control  @error('post_code') invalid-input @enderror"
                           value="{{ isset($address)?$address->post_code:null }}">
                    <span id="post_code" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="telephone" class="control-group">
                <label for="telephone" class="control-label">Telephone Number</label>
                <div class="form-group controls">
                    <input type="text" name="telephone" id="telephone"
                           class="form-control  @error('telephone') invalid-input @enderror"
                           value="{{ isset($client)?$client->telephone:null }}">
                    <span id="telephone" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="fax" class="control-group">
                <label for="fax" class="control-label">Fax Number</label>
                <div class="form-group controls">
                    <input type="text" name="fax" id="fax"
                           class="form-control  @error('fax') invalid-input @enderror"
                           value="{{ isset($client)?$client->fax:null }}">
                    <span id="fax" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="email" class="control-group">
                <label for="email" class="control-label">Email</label>
                <div class="form-group controls">
                    <input type="email" name="email" id="email"
                           class="form-control  @error('email') invalid-input @enderror"
                           value="{{ isset($client)?$client->email:null }}">
                    <span id="email" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="currency" class="control-group">
                <label for="currency" class="control-label">Currency</label>
                <div class="form-group controls">
                    <select name="currency_id" id="currency_id" class="form-control">
                        <option>Select...</option>
                        @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}"
                                {{ isset($client) && $client->currency_id == $currency->id ? 'selected' : '' }}>
                                {{ $currency->name }}
                            </option>
                        @endforeach
                    </select>
                    <span id="currency" class="help-block invalid-text"></span>
                </div>
            </div>

            <div id="avatar" class="control-group">
                {{ Form::label('avatar', 'Avatar', array('class' => 'control-label')) }}
                <input id="avatar" name="logo" type="file" style="display:none">
                <div class="form-group controls">
                    <input id='input' class="form-control" type="text" value="{{$client->logo ?? ''}}"
                           onclick="$('input[id=avatar]').click()"/>
                    <a class="btn" onclick="$('input[id=avatar]').click()">Browse</a>
                    <span id="avatar" class="help-block">Select a file to upload</span>
                </div>
            </div>


            @if ($isModal)
                {{ Form::hidden('isModal', $isModal) }}
            @endif
            {{ Form::hidden('id', $clientId) }}
            {{ Form::hidden('address_id', isset($client)?$client->address_id:null)}}


            <div class="form-actions">
                <button id="save-client" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
                    Save Changes
                </button>
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>
@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
