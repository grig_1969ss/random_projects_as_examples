@extends('layouts_new.app')

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Teams</h5>
                    </div>
                    <div class="dropdown">
                        <div class="btn-group mr-2 mb-2">
                            <a class="btn add-property btn-outline-primary">Add Team</a>
                        </div>
                        <div class="btn-group mr-2 mb-2">
                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option>All teams</option>
                                <option>Active</option>
                                <option>Disposed</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Client</th>
                                <th scope="col">Owner</th>
                                <th scope="col">Users</th>
                                <th scope="col">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>McDonalds UK</td>
                                <td>Steve Dodsworth</td>
                                <td>4</td>
                                <td>Active</td>
                            </tr>
                            </tbody>
                        </table>

                        <nav class="property-list-pagination" aria-label="Page navigation example">
                            <span>Showing 1 to 1 of 1 entries</span>
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
