@extends('layouts_new.app', ['page'=>'page'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Properties</h5>
                    </div>
                    <div class="dropdown">
                        <div class="btn-group mr-2 mb-2">
                            <a class="btn add-property btn-outline-primary">Add Property</a>
                        </div>
                        <div class="btn-group mr-2 mb-2">
                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option>All properties</option>
                                <option>Active</option>
                                <option>Disposed</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Property</th>
                                <th scope="col">Region</th>
                                <th scope="col">Property Ref</th>
                                <th scope="col">Post Code</th>
                                <th scope="col">Client</th>
                                <th scope="col">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>0026 Maidstone, Week Street</td>
                                <td>Mcdonalds-South</td>
                                <td>26</td>
                                <td>ME14 1RL</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1427 Dartford, Princes Road</td>
                                <td>Mcdonalds-South</td>
                                <td>1427</td>
                                <td>DA1 1YT</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1443 Swindon, Bridgemead</td>
                                <td>Mcdonalds-South</td>
                                <td>1443</td>
                                <td>SN5 7FQ</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1451 Rotherham, Parkgate</td>
                                <td>Mcdonalds-North</td>
                                <td>1451</td>
                                <td>S60 1SG</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1452 Stoke, Norton Park</td>
                                <td>Mcdonalds-Mcopco</td>
                                <td>1452</td>
                                <td>ST1 6AX</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1458 Poole, Mannings Heath</td>
                                <td>Mcdonalds-South</td>
                                <td>1458</td>
                                <td>BH12 4QY</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1461 Crewe, Dunwoody</td>
                                <td>Mcdonalds-North</td>
                                <td>1461</td>
                                <td>CW1 3AW</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1502 Lancaster, Caton Road</td>
                                <td>Mcdonalds-North</td>
                                <td>1502</td>
                                <td>LA1 3PE</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1530 St Ives, Bramley Road</td>
                                <td>Mcdonalds-South</td>
                                <td>1530</td>
                                <td>PE27 4AE</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            <tr>
                                <td>1130 Bishop Stortford</td>
                                <td>-</td>
                                <td>#1130</td>
                                <td>-</td>
                                <td>McDonalds UK</td>
                                <td>Active</td>
                            </tr>
                            </tbody>
                        </table>

                        <nav class="property-list-pagination" aria-label="Page navigation example">
                            <span>Showing 1 to 10 of 1,351 entries</span>
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
