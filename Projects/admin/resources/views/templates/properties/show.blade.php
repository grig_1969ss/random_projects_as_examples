@extends('layouts_new.app', ['pageTitle' => 'propertyShow', 'pageNav' => 'property', 'page' => ''])

@section('container')
    <div class="section section-sm" style="padding-top: 1rem;">
        <div id="template-page" class="container-fluid">
            <!-- Title  -->
            <div class="col-12 mb-3 d-flex justify-content-between">
                <div>
                    <h5 class="font-weight-bold">0587 Fleet Service Area</h5>
                </div>
                <div class="d-none d-md-block">
                    <button class="btn btn-sm btn-danger animate-hover mr-3">Edit</button>
                    <button class="btn btn-sm btn-primary animate-hover mr-3">Upload</button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-angle-down dropdown-arrow"></i>
                            <span>Actions</span>
                        </button>

                        <div class="dropdown-menu" id="action-dropdown" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 31px, 0px);">
                            <a class="dropdown-item" href="#">Add Service Charge</a>
                            <a class="dropdown-item" href="#">Add Insurance</a>
                            <a class="dropdown-item" href="#">Add Approval</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Duplicate</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Dispose</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <!-- Tab -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                           href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Property</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                           href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
                        <a class="nav-item nav-link" id="nav-rent-tab" data-toggle="tab"
                           href="#nav-rent" role="tab" aria-controls="nav-rent" aria-selected="false">Rent</a>
                        <a class="nav-item nav-link" id="nav-rates-tab" data-toggle="tab"
                           href="#nav-rates" role="tab" aria-controls="nav-rates" aria-selected="false">Rates</a>
                        <a class="nav-item nav-link" id="nav-lease-tab" data-toggle="tab"
                           href="#nav-lease" role="tab" aria-controls="nav-lease" aria-selected="false">Lease</a>
                        <a class="nav-item nav-link" id="nav-documents-tab" data-toggle="tab"
                           href="#nav-documents" role="tab" aria-controls="nav-documents" aria-selected="false">Documents</a>
                           <a class="nav-item nav-link" id="nav-emails-tab" data-toggle="tab"
                           href="#nav-emails" role="tab" aria-controls="nav-emails" aria-selected="false">Emails</a>
                    </div>
                </nav>
                <div class="tab-content pt-3 pl-3 pr-3" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row pt-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Property Address</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">0587 Fleet Service Area</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Property Ref</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">587</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Property Type</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">Special</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Address</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">Fleet Services Area<br>
                                    M3 Motorway Northbound<br>
                                    Hartley Whitney<br>
                                    Hampshire<br>
                                    GU51 1AA</p>
                                    </div>
                                </div>

                                <div class="row pt-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Region Name</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">Mcdonalds - Mcopco</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Region Manager</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">General Info</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Inside Landlord &
                                    Tenant Act</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">&nbsp;</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Floor Area Net Trading</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">Sq Ft</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-5">
                                        <small class="text-uppercase text-muted">Tenure</small>
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="mb-0">NR</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                         aria-labelledby="nav-contact-tab">
                         <div class="row">
                            <div class="col-md-12">
                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Client Surveyor</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">Hannah Ford</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Client Admin</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">Penné Houghton</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Capa Admin</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">Rosemary Crawley</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Store Contact</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Store Telephone</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">01252-815580</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Client Name</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">McDonalds UK</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Managing Agent</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Managing Agent Contact</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Landlord</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row py-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted"Landlord Contact></small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade show" id="nav-rent" role="tabpanel" aria-labelledby="nav-rent-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row pt-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Completion Date</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Rent Commencement Date</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Current Rent</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row pt-2 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Rent Payment Frequency</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Rent Payment Frequency...</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Rent Payment Dates</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Deposit</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Service Charge</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">Yes</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Lease Start</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Expiry Date</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Rent Review Date</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>



                    <div class="tab-pane fade show" id="nav-rates" role="tabpanel" aria-labelledby="nav-rates-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row pt-3 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Current Rateable Value</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Proposed Rateable Value</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Agreed Rateable Value</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row pt-2 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">User Type</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted">Notes</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="nav-lease" role="tabpanel" aria-labelledby="nav-lease-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Lease Notice Date</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Guarantor</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Lease Comments</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Owner Type</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">M</p>
                                    </div>
                                </div>

                                <h5 class="mt-3">ALIENATION</h5>
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Assignment</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Subletting</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-8">-</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <h5>ALTERATIONS</h5>
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Non Structural</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row pt-2 align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Structural</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Landlord Consent</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Schedule of Condition</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-8">-</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <h5>USER CLAUSE</h5>
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Restrictions</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">User Type</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Initial Concessions</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-8">-</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <h5>REPAIR</h5>
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Repair Classification</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Repair Obligations</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <h5>EXT. DECORATION</h5>
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Frequency</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">First Due</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Next Due</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Final Year</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <h5>INT. DECORATION</h5>
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Frequency</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">First Due</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Next Due</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <small class="text-uppercase text-muted ml-5">Final Year</small>
                                    </div>
                                    <div class="col-sm-9 pl-0">
                                        <p class="mb-0">-</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-documents" role="tabpanel"
                         aria-labelledby="nav-documents-tab">
                        <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed.
                            Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident
                            chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod
                            Pinterest in do umami readymade swag.</p>
                        <p>Day handsome addition horrible sensible goodness two contempt. Evening for
                            married his account removal. Estimable me disposing of be moonlight cordially
                            curiosity.</p>
                    </div>
                    <div class="tab-pane fade" id="nav-emails" role="tabpanel"
                         aria-labelledby="nav-emails-tab">
                        <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed.
                            Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident
                            chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod
                            Pinterest in do umami readymade swag.</p>
                        <p>Day handsome addition horrible sensible goodness two contempt. Evening for
                            married his account removal. Estimable me disposing of be moonlight cordially
                            curiosity.</p>
                    </div>
                </div>
                <!-- End of tab -->
            </div>

            <div class="row mt-4">
                <div class="col-lg-3">
                    <!-- Tab Nav -->
                    <ul class="nav nav-pills square nav-fill flex-column vertical-tab" id="tab12" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab-3" data-toggle="tab" href="#tab-14" role="tab" aria-controls="tab-14" aria-selected="true"><span class="icon-primary d-block"><!-- <i class="far fa-sun"></i> -->Service Charge</span></a>
                        </li>
                        <li class="nav-item"><a class="nav-link" id="profile-tab-3" data-toggle="tab" href="#tab-15" role="tab" aria-controls="tab-15" aria-selected="false"><span class="icon-secondary d-block"><!-- <i class="far fa-sun"></i> -->Insurances</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab-3" data-toggle="tab" href="#tab-16" role="tab" aria-controls="tab-16" aria-selected="false"><span class="icon-tertiary d-block"><!-- <i class="far fa-sun"></i> -->Approvals</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab-3" data-toggle="tab" href="#tab-16" role="tab" aria-controls="tab-16" aria-selected="false"><span class="icon-info d-block"><!-- <i class="far fa-sun"></i> -->Rent Review</span></a>
                        </li>
                        <li class="nav-item" style="display: none;">
                            <a class="nav-link" id="contact-tab-3" data-toggle="tab" href="#tab-16" role="tab" aria-controls="tab-16" aria-selected="false"><span class="icon-warning d-block"><!-- <i class="far fa-sun"></i> -->Maintenance</span></a>
                        </li>
                        <li class="nav-item" style="display: none;">
                            <a class="nav-link" id="contact-tab-3" data-toggle="tab" href="#tab-16" role="tab" aria-controls="tab-16" aria-selected="false"><span class="icon-dark d-block"><!-- <i class="far fa-sun"></i> -->Break Options</span></a>
                        </li>
                    </ul>
                <!-- End of Tab Nav -->
                </div>
                <div class="col-lg-9">
                    <!-- Tab Content -->
                    <div class="card">
                        <div class="card-body py-0 pl-0 pr-0">
                            <div class="tab-content" id="tabcontent"><div class="tab-pane fade show active" id="tab-14" role="tabpanel" aria-labelledby="tab-14">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Tab -->
                                        <nav>
                                            <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-2019-tab" data-toggle="tab" href="#nav-2019" role="tab" aria-controls="nav-2019" aria-selected="true">2019</a>
                                                <a class="nav-item nav-link" id="nav-2018-tab" data-toggle="tab" href="#nav-2018" role="tab" aria-controls="nav-2018" aria-selected="false">2018</a>
                                                <a class="nav-item nav-link" id="nav-2017-tab" data-toggle="tab" href="#nav-2017" role="tab" aria-controls="nav-2017" aria-selected="false">2017</a>
                                            </div>
                                        </nav>
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-2019" role="tabpanel" aria-labelledby="nav-2019-tab">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Budget Y/E</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£247,371.04</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Actual Y/E</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£246,758.11</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">WIP Amount</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£ - </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Qrt Payment (Budget)</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£61,842.76</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Qrt Payment (Actual)</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£61,689.53</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Balancing S/C</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£-612.93</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Annual Budget 2019</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£247,371.04</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Annual Budget 2018</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">£237,084.05</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row pt-3 align-items-center">
                                                            <div class="col-sm-5">
                                                                <small class="text-uppercase text-muted">Budget % increase</small>
                                                            </div>
                                                            <div class="col-sm-6 pl-0">
                                                                <p class="mb-0">4.34%</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-2018" role="tabpanel" aria-labelledby="nav-2018-tab">
                                                <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag.</p>
                                                <p>Day handsome addition horrible sensible goodness two contempt. Evening for married his account removal. Estimable me disposing of be moonlight cordially curiosity.</p>
                                            </div>
                                            <div class="tab-pane fade" id="nav-2017" role="tabpanel" aria-labelledby="nav-2017-tab">
                                                <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag.</p>
                                                <p>Day handsome addition horrible sensible goodness two contempt. Evening for married his account removal. Estimable me disposing of be moonlight cordially curiosity.</p>
                                            </div>
                                        </div>
                                        <!-- End of tab -->
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-15" role="tabpanel" aria-labelledby="tab-15"><p>Photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag.</p>
                                <p>Day handsome addition horrible sensible goodness two contempt. Evening for married his account removal. Estimable me disposing of be moonlight cordially curiosity.</p>
                            </div>
                            <div class="tab-pane fade" id="tab-16" role="tabpanel" aria-labelledby="tab-16">
                                <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag.</p><p>Day handsome addition horrible sensible goodness two contempt. Evening for married his account removal. Estimable me disposing of be moonlight cordially curiosity.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- End of Tab Content -->
                </div>
            </div>

            <div class="row mt-4">
                <!-- Timeline -->
                <div class="timeline timeline-five px-3 px-sm-0">
                    <!-- Item 1 -->
                    <div class="row">
                        <!-- timeline item 1 center image & middle line -->
                        <div class="col-auto text-center flex-column d-none d-sm-flex">
                            <div class="row h-50">
                                <div class="col">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                            <span class="m-3 avatar-separator">
                        <img class="img-fluid rounded-circle" src="../../img/team/8.jpg" alt="avatar">
                    </span>
                            <div class="row h-50">
                                <div class="col middle-line">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                        </div>
                        <!-- Timeline item 1 content-->
                        <div class="col-12 col-lg-12 col-xl-11 my-4">
                            <div class="card shadow-sm bw-md border-primary text-primary">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-4">
                                        <span class="font-small">
                                            <a href="#">
                                                <img class="avatar-sm img-fluid rounded-circle mr-2" src="../../img/team/9.jpg" alt="avatar">
                                                <span class="font-weight-bold">John Smith</span>
                                            </a>
                                            <span class="ml-2">2 min ago</span>
                                        </span>
                                        <div>
                                            <button class="btn btn-link text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Report comment">
                                                <i class="far fa-flag"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <p class="card-text">Contacted client to discuss email regarding service charges, they will send in a letter covering the required information and also forward a copy to the franchisee. </p>
                                    <button class="btn btn-sm btn-primary collapsed" type="button"
                                            data-target="#t1_details" data-toggle="collapse"
                                            aria-expanded="false">Show Details <i
                                            class="fas fa-angle-down toggle-arrow ml-1"></i></button>
                                    <div class="collapse" id="t1_details" style="">
                                        <div class="p-2 mt-3 text-monospace">
                                            <div>08:30 - 09:00 Breakfast in Town</div>
                                            <div>09:00 - 10:30 Attend a team meeting</div>
                                            <div>10:30 - 10:45 Research on new technologies and tools</div>
                                            <div>10:45 - 12:00 It’s a good idea to review the day’s work</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 2 -->
                    <div class="row">
                        <!-- timeline item 2 center image & middle line -->
                        <div class="col-auto text-center flex-column d-none d-sm-flex">
                            <div class="row h-50">
                                <div class="col middle-line">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                            <span class="m-3 avatar-separator">
                        <img class="img-fluid rounded-circle" src="../../img/team/10.jpg" alt="avatar">
                    </span>
                            <div class="row h-50">
                                <div class="col middle-line">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                        </div>
                        <!-- Timeline item 2 content -->
                        <div class="col-12 col-lg-12 col-xl-11 my-4">
                            <div class="card shadow-sm bw-md border-secondary text-success">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-4">
                                        <span class="font-small">
                                            <a href="#">
                                                <img class="avatar-sm img-fluid rounded-circle mr-2" src="../../img/team/9.jpg" alt="avatar">
                                                <span class="font-weight-bold">John Doe</span>
                                            </a>
                                            <span class="ml-2">2 min ago</span>
                                        </span>
                                        <div>
                                            <button class="btn btn-link text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Report comment">
                                                <i class="far fa-flag"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <p class="card-text">Stay organized, reduce stress, and accomplish personal
                                        and business goals with a daily schedule template. It’s a simple yet
                                        effective time-management tool for any daily activity, whether you’re
                                        managing a busy work
                                        schedule, academic assignments or family chores. </p>
                                    <button class="btn btn-sm btn-secondary" type="button"
                                            data-target="#t2_details" data-toggle="collapse">Show Details <i
                                            class="fas fa-angle-down toggle-arrow ml-1"></i></button>
                                    <div class="collapse" id="t2_details">
                                        <div class="p-2 mt-3 text-monospace">
                                            <div>08:30 - 09:00 Breakfast in Town</div>
                                            <div>09:00 - 10:30 Attend a team meeting</div>
                                            <div>10:30 - 10:45 Research on new technologies and tools</div>
                                            <div>10:45 - 12:00 It’s a good idea to review the day’s work</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Timeline Item 3 -->
                    <div class="row">
                        <!-- timeline item 3 center image & middle line -->
                        <div class="col-auto text-center flex-column d-none d-sm-flex">
                            <div class="row h-50">
                                <div class="col middle-line">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                            <span class="m-3 avatar-separator">
                        <img class="img-fluid rounded-circle" src="../../img/team/9.jpg" alt="avatar">
                    </span>
                            <div class="row h-50">
                                <div class="col middle-line">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                        </div>
                        <!-- Timeline item 3 content -->
                        <div class="col-12 col-lg-12 col-xl-11 my-4">
                            <div class="card shadow-sm bw-md border-tertiary text-tertiary">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-4">
                                        <span class="font-small">
                                            <a href="#">
                                                <img class="avatar-sm img-fluid rounded-circle mr-2" src="../../img/team/9.jpg" alt="avatar">
                                                <span class="font-weight-bold">John Doe</span>
                                            </a>
                                            <span class="ml-2">2 min ago</span>
                                        </span>
                                        <div>
                                            <button class="btn btn-link text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Report comment">
                                                <i class="far fa-flag"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <p class="card-text">Stay organized, reduce stress, and accomplish personal
                                        and business goals with a daily schedule template. It’s a simple yet
                                        effective time-management tool for any daily activity, whether you’re
                                        managing a busy work
                                        schedule, academic assignments or family chores. </p>
                                    <button class="btn btn-sm btn-tertiary" type="button"
                                            data-target="#t3_details" data-toggle="collapse">Show Details <i
                                            class="fas fa-angle-down toggle-arrow ml-1"></i></button>
                                    <div class="collapse" id="t3_details">
                                        <div class="p-2 mt-3 text-monospace">
                                            <div>08:30 - 09:00 Breakfast in Town</div>
                                            <div>09:00 - 10:30 Attend a team meeting</div>
                                            <div>10:30 - 10:45 Research on new technologies and tools</div>
                                            <div>10:45 - 12:00 It’s a good idea to review the day’s work</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Timeline -->
            </div>
        </div>
    </div>
@endsection
