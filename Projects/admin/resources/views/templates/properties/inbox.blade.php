@extends('layouts_new.app', ['page' => 'Inbox'])

@section('container')
    <div class="section section-md">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 mb-4 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Inbox</h5>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">

                <aside class="col-12 col-lg-3 mt-3 mt-lg-0 main_left">
                    <div class="card shadow-soft border-soft p-3">
                        <a class="btn compose mr-2 mb-2 btn-pill btn-outline-primary" href="{{route('emails.create')}}">COMPOSE</a>
                        <ul class="list-group list-group-flush bg-white shadow-sm border border-soft">
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-inbox icon mr-3"></i></i>Inbox</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="far fa-envelope-open icon mr-3"></i>Draft</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-paper-plane icon mr-3"></i>Sent Mail</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-trash icon mr-3"></i>Trash</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card shadow-soft border-soft p-3 mt-4">
                        <ul class="list-group list-group-flush bg-white shadow-sm border border-soft">
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-circle text-success fa-xs icon mr-3"></i></i>Tag 1</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-circle text-warning fa-xs icon mr-3"></i>Tag 2</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-circle icon text-info fa-xs mr-3"></i>Tag 3</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0">
                                <a href="http://admin.capa-new.loc/diary" class="text-primary"><i
                                        class="fas fa-circle icon text-primary fa-xs mr-3"></i>Tag 4</a>
                            </li>
                        </ul>
                    </div>
                </aside>

                <div class="col-lg-9 pr-lg-4">
                    <div class="timeline-item border border-light top-item">
                        <div class="container-fluid position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign check-all"></span>
                                    </label>
                                </div>
                                <i class="fas fa-redo icon mr-3 "></i>
                                <i class="fas fa-archive icon mr-3"></i>
                                <i class="fas fa-exclamation-circle icon mr-3"></i>
                                <i class="fas fa-folder icon mr-3"></i>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <div class="input-group p-0 m-0">

                                    <input class="form-control" placeholder="Search..." type="text">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                    </div>
                                </div>

                            </div>
                            <div class="top-header_pagination">
                                <span>1-10 of 36</span>
                                <i class="fas   fa-arrow-left icon mr-3 "></i>
                                <i class="fas  fa-arrow-right icon mr-3"></i>
                                <i class="fas fa-cog icon mr-3"></i>
                            </div>
                        </div>
                    </div>

                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item  border border-light">
                        <div class="container-fluid single-item position-relative mail-top-header">
                            <div class="icons-wrapper">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <i class="fas fa-star icon mr-3 icon-warning"></i>
                                <i class="fas fa-circle text-success fa-xs icon mr-3 icon-warning"></i>
                                <img class="avatar-lg avatar-sm-lg img-fluid rounded-circle mr-3"
                                     src="{{asset('img/moved/assets/girl.jpg')}}" alt="avatar">

                                <span>Google Inc</span>
                            </div>

                            <div class="d-flex align-items-center mt-0 ml-1 ml-sm-0 mt-sm-0 search-block">
                                <span>Adwords Keywords Research For Beginners</span>

                            </div>
                            <div class="top-header_pagination">
                                <i class="fas fa-paperclip icon mr-3"></i>
                                <span>7:24 AM</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection
