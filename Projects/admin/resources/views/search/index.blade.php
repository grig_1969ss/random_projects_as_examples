@extends('layouts_new.app', ['pageTitle' => 'Search', 'pageNav' => 'Search', 'page' => 'Search'])

@section('container')
    <div class="section section-md">
        <div class="container-fluid">
            <form action="" id="searchForm">
                <div class="row">
                    <div class="form-group col-4">
                        <select name="type" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option disabled selected>Select...</option>
                            @foreach($searchables as $searchable)
                                <option
                                    value="{{$searchable}}" {{$searchable == $type ? 'selected' : ''}}>{{$searchable}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-4">
                        <input name="searchBy" type="text" placeholder="" class="form-control"
                               value="{{$searchBy ?? '' }}">
                    </div>
                    <div class="form-group col-4">
                        <button class="btn btn-primary btn-loading-overlay mr-2 mb-2" type="submit" id="search_button"
                                disabled>
                                <span class="spinner">
                                    <span class="spinner-border spinner-border-sm" role="status"
                                          aria-hidden="true"></span>
                                </span>
                            <span class="ml-1 btn-inner-text">Search</span>
                        </button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Result >></th>
                                <th scope="col">Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><b>Total</b></td>
                                <td><b>{{$result->count()}}</b></td>
                            </tr>
                            @if(!is_array($result))
                                @foreach($result->groupByType() as $type => $typeResult)
                                    @foreach($typeResult as $row)
                                        <tr>
                                            <td><a href="{{$row->url}}">{{$row->title}}</a></td>
                                            <td>{{$type}}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('js')

@endpush
