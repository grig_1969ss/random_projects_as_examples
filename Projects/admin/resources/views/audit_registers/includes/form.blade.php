<fieldset>
    <div id='error-message'></div>

    <form method="POST"
          action="{{ $auditRegister->id ? route('audit-registers.update', $auditRegister) : route('audit-registers.store') }}"
          class="form-horizontal"
          id="create-audit-register">
        {{ csrf_field() }}

        @if($auditRegister->id)
            {{ method_field('PUT') }}
        @endif

        <div id="client_id" class="control-group">
            <label for="client_id" class="control-label">Client</label>
            <div class="controls">
                <select name="client_id" class="input-large" id="client_id">
                    <option value="">Select...</option>
                    @foreach($clients as $client)
                        <option value="{{ $client->id }}"
                                {{ $client->id == $auditRegister->client_id ? 'selected' : '' }}>
                            {{ $client->client_name }}
                        </option>
                    @endforeach
                </select>
                <span id="client_id" class="help-block"></span>
            </div>
        </div>

        <div id="audit_name" class="control-group">
            <label for="audit_name" class="control-label">Audit Name</label>
            <div class="controls">
                <input type="text"
                       name="audit_name"
                       id="audit_name"
                       class="input-xlarge"
                       value="{{ $auditRegister->audit_name }}">
                <span id="audit_name" class="help-block"></span>
            </div>
        </div>

        <div class="control-group">
            <label for="audit_start_date" class="control-label">Audit Period</label>
            <div class="controls">
                From
                <div class="input-append" id="audit_start_date">
                    <input type="text"
                           name="audit_start_date"
                           id="audit_start_date"
                           class="datepicker input-small"
                           data-date-format="dd/mm/yyyy"
                           value="{{ $auditRegister->present()->auditStartDate('') }}">
                    <span class="add-on"><i class="'awe-calendar"></i></span>
                </div>
                to
                <div class="input-append">
                    <input type="text"
                           name="audit_end_date"
                           id="audit_end_date"
                           class="datepicker input-small"
                           data-date-format="dd/mm/yyyy"
                           value="{{ $auditRegister->present()->auditEndDate('') }}">
                    <span class="add-on"><i class="awe-calendar"></i></span>
                </div>
                <span id="audit_start_date" class="help-block"></span>
                <span id="audit_end_date" class="help-block"></span>
            </div>
        </div>

        <div id="audit_number" class="control-group">
            <label for="audit_number" class="control-label">Audit Number</label>
            <div class="controls">
                <input type="text"
                       name="audit_number"
                       id="audit_number"
                       class="input-xlarge"
                       value="{{ $auditRegister->audit_number }}">
                <span id="audit_number" class="help-block"></span>
            </div>
        </div>

        <div id="administration" class="control-group">
            <label for="administration" class="control-label">Administration</label>
            <div class="controls">
                <input type="hidden" name="administration" value="0">
                <input type="checkbox"
                       name="administration"
                       id="administration"
                       value="1"
                        {{ $auditRegister->administration ? 'checked' : '' }}>
            </div>
        </div>

        <div class="form-actions">
            <button id="save-audit-register" class="btn btn-large btn-primary" type="submit">
                Save Changes
            </button>
        </div>
    </form>
</fieldset>

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
