@extends('layouts_new.app',
    ['pageTitle' => 'Audit Registers', 'pageNav' => 'audit-register', 'page' => 'AuditRegistersIndex'])

@section('container')
    <!-- Right (content) side -->
    <div class="content-block" role="main">

        <!-- Grid row -->
        <div class="row">

            <!-- Data block -->
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Audit Registers</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li class="demoTabs">
                                    <a href="{{ route('audit-registers.create') }}" class="btn btn-alt">
                                        Add Audit Register
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </header>
                    <table id="example-2" class="datatable table table-striped table-bordered table-hover dataTable"
                           aria-describedby="example-2_info" style="width: 880px;">
                        <thead>
                        <tr>
                            <th>Client</th>
                            <th>Audit Name</th>
                            <th>Audit Period</th>
                            <th>Audit Number</th>
                            <th>Administration</th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach ($auditRegisters as $auditRegister)
                            <tr class="odd gradeX">
                                @if (!$read_only)
                                    <td>
                                        <a href="{{ route('audit-registers.show', $auditRegister) }}">
                                            {{ $auditRegister->present()->client }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('audit-registers.show', $auditRegister) }}">
                                            {{ $auditRegister->audit_name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('audit-registers.show', $auditRegister) }}">
                                            {{ $auditRegister->present()->period }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('audit-registers.show', $auditRegister) }}">
                                            {{ $auditRegister->audit_number }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('audit-registers.show', $auditRegister) }}">
                                            {{ $auditRegister->present()->administration }}
                                        </a>
                                    </td>
                                @else
                                    <td>{{ $auditRegister->present()->client }}</td>
                                    <td>{{ $auditRegister->audit_name }}</td>
                                    <td>{{ $auditRegister->present()->period }}</td>
                                    <td>{{ $auditRegister->audit_number }}</td>
                                    <td>{{ $auditRegister->present()->administration }}</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- /Data block -->
        </div>
        <!-- /Grid row -->
    </div>
    <!-- /Right (content) side -->
@endsection

@push('js')
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
