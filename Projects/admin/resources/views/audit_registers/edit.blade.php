@extends('layouts_new.app', [
    'pageTitle' => ($auditRegister->id ? 'Edit' : 'Add').' Audit Register',
    'pageNav'   => 'auditRegister',
    'page'      => 'AuditRegisterNew'
])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $auditRegister->id ? 'Edit' : 'New' }} Audit Register</h2>
                    </header>
                    <div class="span8">
                        @include('audit_registers.includes.form')
                    </div>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush
