@extends('layouts_new.app',
    ['pageTitle' => 'Audit Register', 'pageNav' => 'auditRegister', 'page' => 'AuditRegistersDetail'])

@section('container')
    <div class="content-block" role="main">
        <div class="row">
            <article class="span12 data-block">
                <div class="data-container">
                    <header>
                        <h2>Audit Register #{{ $auditRegister->audit_number }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="{{ route('audit-registers.edit', $auditRegister) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Client</dt>
                            <dd>
                                @if($auditRegister->client)
                                    <a href="{{ route('clients.show', $auditRegister->client) }}">
                                        {{ $auditRegister->present()->client }}
                                    </a>
                                @else
                                    -
                                @endif
                            </dd>
                            <dt>Audit Name</dt>
                            <dd>{{ $auditRegister->audit_name }}</dd>
                            <dt>Audit Period</dt>
                            <dd>{{ $auditRegister->present()->period }}</dd>
                            <dt>Audit Number</dt>
                            <dd>{{ $auditRegister->audit_number }}</dd>
                            <dt>Administration</dt>
                            <dd>{{ $auditRegister->present()->administration }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($auditRegister->created_at) }}
                                by {{ optional($auditRegister->createdBy)->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
