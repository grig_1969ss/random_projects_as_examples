@extends('layouts_new.app', ['pageTitle' => 'Managing Agents', 'pageNav' => 'agent', 'page' => 'AgentsIndex'])

@section('container')
    <div class="section section-sm">
        <div class="container-fluid">

            <div class="container-fluid">
                <!-- Title  -->
                <div class="row">
                    <div class="col-12 mb-4 d-flex justify-content-between">
                        <div>
                            <h5 class="font-weight-bold">Managing Agents</h5>
                        </div>
                        <div class="dropdown">
                            @if ($read_only == 0)
                                <div class="btn-group mr-2 mb-2">
                                    <a href="{{route('agents.create')}}" class="btn add-property btn-outline-primary">Add Managing Agent</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- End of Title -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-5">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Telephone</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Main Contact</th>
                                    <th scope="col">Property</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($agents as $agent)
                                    <tr>
                                        @if ($read_only == 0)
                                            <td>
                                                <a href="{{ route('agents.show', $agent->id) }}">
                                                    {{ $agent->present()->agentName }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('agents.show', $agent->id) }}">
                                                    {{ $agent->present()->telephone }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('agents.show', $agent->id) }}">
                                                    {{ $agent->present()->email }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('contacts.show', $agent->contact_id) }}">
                                                    {{ Util::joinName([$agent->first_name, $agent->surname]) }}
                                                </a>
                                            </td>
                                            <td>
                                                @if($agent->property)
                                                    <a href="{{ route('properties.show', $agent->property_id) }}">
                                                        {{ $agent->property }}
                                                    </a>
                                                @else
                                                    No Properties Exist
                                                @endif
                                            </td>
                                        @else
                                            <td>{{ $agent->present()->agentName }}</td>
                                            <td>{{ $agent->present()->telephone }}</td>
                                            <td>{{ $agent->present()->email }}</td>
                                            <td>{{ Util::joinName([$agent->first_name, $agent->surname]) }}</td>
                                            <td>{{ $agent->property ? $agent->property :'No Properties Exist' }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <nav class="property-list-pagination default-pagination" aria-label="Page navigation example">
                                <span>Showing {{$agents->firstItem()}} to {{$agents->lastItem()}} of {{$agents->total()}} entries</span>
                                {{$agents->links()}}
                            </nav>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

