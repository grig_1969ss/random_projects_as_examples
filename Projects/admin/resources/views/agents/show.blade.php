@extends('layouts_new.app', ['pageTitle' => 'Managing Agent', 'pageNav' => 'agent', 'page' => 'AgentsDetail'])

@section('container')

    <div class="section section-sm">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">{{ $agent->agent_name }}</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a href="{{ route('agents.edit', $agent) }}"
                                       class="btn add-property btn-outline-primary">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-3 align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Managing Agent/Landlord</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">{{ $agent->agent_name }}</p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Address</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ $agent->address->address_1 }}<br>
                                {{ $agent->address->address_2 }}<br>
                                {{ $agent->address->town }}<br>
                                {{ $agent->address->county }}<br>
                                {{ $agent->address->post_code }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Telephone</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($agent->telephone) }}<br>
                                {{ Util::showValue($agent->mobile) }}
                            </p>
                        </div>
                    </div>

                    @if($agent->fax)
                        <div class="row align-items-center">
                            <div class="col-5">
                                <small class="text-uppercase text-muted">Fax</small>
                            </div>
                            <div class="col-7 pl-0">
                                <p class="mb-0">
                                    {{ $agent->fax }}
                                </p>
                            </div>
                        </div>
                    @endif

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Email</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Util::showValue($agent->email) }}
                            </p>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-5">
                            <small class="text-uppercase text-muted">Added</small>
                        </div>
                        <div class="col-7 pl-0">
                            <p class="mb-0">
                                {{ Date::dmYHi($agent->created_at) }}
                                by {{ $agent->user->name }}
                            </p>
                        </div>
                    </div>

                </div>
                <div class="col-6">
                    <!-- Title  -->
                    <div class="row">
                        <div class="col-12 mb-4 d-flex justify-content-between">
                            <div>
                                <h5 class="font-weight-bold">Contacts</h5>
                            </div>
                            <div class="dropdown">
                                <div class="btn-group mr-2 mb-2">
                                    <a href="{{ route('agents.contacts.create', $agent) }}"
                                       class="btn add-property btn-outline-primary">Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-5">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Contact Name</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($agent->contacts as $contact)
                                        <?php $contactUrl = '/contacts/'.$contact->id; ?>
                                        <tr>
                                            <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                            <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('partials.show_properties', [
                          'id'          => $agent->id,
                          'type'        => 'agent',
                          'showClients' => true,
                          'header'      => 'Managing Agent'
            ])


        </div>
    </div>



    <div class="content-block" role="main">
        <div class="row">
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>{{ $agent->agent_name }}</h2>
                        <ul class="data-header-actions">
                            @if ($read_only == 0)
                                <li><a href="{{ route('agents.edit', $agent) }}" class="btn btn-alt">Edit</a></li>
                            @endif
                        </ul>
                    </header>
                    <section>
                        <dl class="dl-horizontal">
                            <dt>Managing Agent/Landlord</dt>
                            <dd>{{ $agent->agent_name }}</dd>
                            <dt>Address</dt>
                            <dd>{{ $agent->address->address_1 }}</dd>
                            <dd>{{ $agent->address->address_2 }}</dd>
                            <dd>{{ $agent->address->town }}</dd>
                            <dd>{{ $agent->address->county }}</dd>
                            <dd>{{ $agent->address->post_code }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ Util::showValue($agent->telephone) }}</dd>
                            <dd>{{ Util::showValue($agent->mobile) }}</dd>
                            @if($agent->fax)
                                <dt>Fax</dt>
                                <dd>{{ $agent->fax }}</dd>
                            @endif
                            <dt>Email</dt>
                            <dd>{{ Util::showValue($agent->email) }}</dd>
                            <dt>Added</dt>
                            <dd>{{ Date::dmYHi($agent->created_at) }}
                                by {{ $agent->user->name }}</dd>
                        </dl>
                    </section>
                </div>
            </article>
            <article class="span6 data-block">
                <div class="data-container">
                    <header>
                        <h2>Contacts</h2>
                        @if(!$read_only)
                            <ul class="data-header-actions">
                                <li>
                                    <a href="{{ route('agents.contacts.create', $agent) }}" class="btn btn-alt">Add</a>
                                </li>
                            </ul>
                        @endif
                    </header>
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Telephone</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($agent->contacts as $contact)
                            <?php $contactUrl = '/contacts/'.$contact->id; ?>
                            <tr class="odd gradeX">
                                <td>{{ Util::link($contactUrl, $contact->first_name.' '.$contact->surname) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->telephone) }}</td>
                                <td>{{ Util::link($contactUrl, $contact->email) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>
        </div>

        <div class="row">
            <article class="span12 data-block">
                @include('partials.show_properties', [
                    'id'          => $agent->id,
                    'type'        => 'agent',
                    'showClients' => true,
                    'header'      => 'Client'
                ])
            </article>
        </div>

        <div class="row">
            <article class="span12 data-block">
                @include('partials.show_notes', ['id' => $agent->id])
            </article>

            @include('partials.add_comment', [
                'id'    => $agent->id,
                'type'  => 'agents',
                'route' => ['agents.notes.store', $agent]
            ])

        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
    <script src="{{ asset("js/moved/plugins/dataTables/jquery.datatables.min.js") }}"></script>
@endpush
