@extends('layouts_new.app', ['pageTitle' => ($agent->id ? 'Edit' : 'Add').' Managing Agent', 'pageNav' => 'agent', 'page' => 'AgentNew'])

@section('container')
    @include('agents.includes.form', [
                            'title'      => ($agent->id ? 'Edit' : 'New').' Managing Agent',
                            'restMethod' => $agent->id ? 'put' : 'post',
                            'isModal'    => false,
                            'route'      => $agent->id ? ['agents.update', $agent] : 'agents.store'
    ])
@endsection
