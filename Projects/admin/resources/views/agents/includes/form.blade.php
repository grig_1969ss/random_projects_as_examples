<?php $agentId = isset($agent->id) ? $agent->id : null ?>

<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12  order-md-1">
{{--            col-md-5 col-lg-4--}}
            <div class="mb-5 text-center">
                <h6 class="h3">{{$title}}</h6>
                <div class="dropdown-divider"></div>
            </div>
            <div id='error-message'></div>
            {{ Form::open(['route' => $route, 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => 'create-agent']) }}

                <div id="agent_name" class="control-group">
                    <label for="name" class="control-label">Managing Agent Name</label>
                    <div class="form-group controls">
                        <input type="text" name="agent_name" id="agent_name"
                               class="form-control  @error('agent_name') invalid-input @enderror"
                               value="{{ $agent ? $agent->agent_name : null }}">
                        <span id="agent_name" class="help-block invalid-text"></span>
                    </div>
                </div>


                @if(!$agentId)
                    <div id="contact_firstname" class="control-group">
                        <label for="contact_firstname" class="control-label">Contact FirstName</label>
                        <div class="form-group controls">
                            <input type="text" name="contact_firstname" id="contact_firstname"
                                   class="form-control  @error('contact_firstname') invalid-input @enderror"
                                   value="{{ isset($agent->contact->first_name)?$agent->contact->first_name.' '.$agent->contact->surname: null  }}">
                            <span id="contact_firstname" class="help-block invalid-text"></span>
                        </div>
                    </div>
                    <div id="contact_surname" class="control-group">
                        <label for="contact_surname" class="control-label">Contact Surname</label>
                        <div class="form-group controls">
                            <input type="text" name="contact_surname" id="contact_surname"
                                   class="form-control  @error('contact_surname') invalid-input @enderror"
                                   value="{{ isset($agent->contact->first_name)?$agent->contact->first_name.' '.$agent->contact->surname:null }}">
                            <span id="contact_surname" class="help-block invalid-text"></span>
                        </div>
                    </div>
                @endif

                <div id="address_1" class="control-group">
                    <label for="address_1" class="control-label">Address Line 1</label>
                    <div class="form-group controls">
                        <input type="text" name="address_1" id="address_1"
                               class="form-control  @error('address_1') invalid-input @enderror"
                               value="{{ isset($address)?$address->address_1:null }}">
                        <span id="address_1" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="address_2" class="control-group">
                    <label for="address_2" class="control-label">Address Line 2</label>
                    <div class="form-group controls">
                        <input type="text" name="address_2" id="address_2"
                               class="form-control  @error('address_2') invalid-input @enderror"
                               value="{{ isset($address)?$address->address_2:null }}">
                        <span id="address_2" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="town" class="control-group">
                    <label for="town" class="control-label">Town</label>
                    <div class="form-group controls">
                        <input type="text" name="town" id="town"
                               class="form-control  @error('town') invalid-input @enderror"
                               value="{{ isset($address)?$address->town:null }}">
                        <span id="town" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="county" class="control-group">
                    <label for="county" class="control-label">County</label>
                    <div class="form-group controls">
                        <input type="text" name="county" id="county"
                               class="form-control  @error('county') invalid-input @enderror"
                               value="{{ isset($address)?$address->county:null }}">
                        <span id="county" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="post_code" class="control-group">
                    <label for="post_code" class="control-label">Post Code</label>
                    <div class="form-group controls">
                        <input type="text" name="post_code" id="post_code"
                               class="form-control  @error('post_code') invalid-input @enderror"
                               value="{{ isset($address)?$address->post_code:null }}">
                        <span id="post_code" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="telephone" class="control-group">
                    <label for="telephone" class="control-label">Telephone Number</label>
                    <div class="form-group controls">
                        <input type="text" name="telephone" id="telephone"
                               class="form-control  @error('telephone') invalid-input @enderror"
                               value="{{ isset($agent)?$agent->telephone:null }}">
                        <span id="telephone" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="fax" class="control-group">
                    <label for="fax" class="control-label">Fax Number</label>
                    <div class="form-group controls">
                        <input type="text" name="fax" id="fax"
                               class="form-control  @error('fax') invalid-input @enderror"
                               value="{{ isset($agent)?$agent->fax:null }}">
                        <span id="fax" class="help-block invalid-text"></span>
                    </div>
                </div>

                <div id="email" class="control-group">
                    <label for="email" class="control-label">Email</label>
                    <div class="form-group controls">
                        <input type="email" name="email" id="email"
                               class="form-control  @error('email') invalid-input @enderror"
                               value="{{ isset($agent)?$agent->email:null }}">
                        <span id="email" class="help-block invalid-text"></span>
                    </div>
                </div>

                @if ($isModal)
                    {{ Form::hidden('isModal', $isModal) }}
                @endif
                {{ Form::hidden('id', $agentId) }}
                {{ Form::hidden('address_id', isset($agent)?$agent->address_id:null) }}


                <div class="form-actions">
                    <button id="save-agent" class="btn btn-block mr-2 mb-2 btn-outline-primary" type="submit">
                        Save Changes
                    </button>
                </div>
            {{ Form::close()}}
        </div>
    </div>
</div>
@push('js')
    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>
@endpush




{{--<fieldset>--}}
{{--    <div id='error-message'></div>--}}
{{--    {{ Form::open(['route' => $route, 'method' => $restMethod, 'class' => 'form-horizontal', 'id' => 'create-agent']) }}--}}
{{--    <div id="agent_name" class="control-group">--}}
{{--        {{ Form::label('agent_name', 'Managing Agent Name', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('agent_name',isset($agent)?$agent->agent_name:null, array('class' => 'input-medium')) }}--}}
{{--            <span id="agent_name" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @if (!$agentId)--}}
{{--        <div id="contact_firstname" class="control-group">--}}
{{--            {{ Form::label('contact_firstname', 'Contact Firstname', array('class' => 'control-label')) }}--}}
{{--            <div class="controls">--}}
{{--                {{ Form::text('contact_firstname',isset($agent->contact->first_name)?$agent->contact->first_name.' '.$agent->contact->surname:null, array('class' => 'input-medium')) }}--}}
{{--                <span id="contact_firstname" class="help-block"></span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div id="contact_surname" class="control-group">--}}
{{--            {{ Form::label('contact_surname', 'Contact Surname', array('class' => 'control-label')) }}--}}
{{--            <div class="controls">--}}
{{--                {{ Form::text('contact_surname',isset($agent->contact->first_name)?$agent->contact->first_name.' '.$agent->contact->surname:null, array('class' => 'input-medium')) }}--}}
{{--                <span id="contact_surname" class="help-block"></span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}
{{--    <div id="address_1" class="control-group">--}}
{{--        {{ Form::label('address_1', 'Address Line 1', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('address_1',isset($address)?$address->address_1:null, array('class' => 'input-xlarge')) }}--}}
{{--            <span id="address_1" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="address_2" class="control-group">--}}
{{--        {{ Form::label('address_2', 'Address Line 2', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('address_2',isset($address)?$address->address_2:null, array('class' => 'input-xlarge')) }}--}}
{{--            <span id="address_2" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="town" class="control-group">--}}
{{--        {{ Form::label('town', 'Town', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('town',isset($address)?$address->town:null, array('class' => 'input-medium')) }}--}}
{{--            <span id="town" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="county" class="control-group">--}}
{{--        {{ Form::label('county', 'County', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('county',isset($address)?$address->county:null, array('class' => 'input-medium')) }}--}}
{{--            <span id="county" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="post_code" class="control-group">--}}
{{--        {{ Form::label('post_code', 'Post Code', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('post_code',isset($address)?$address->post_code:null, array('class' => 'input-small')) }}--}}
{{--            <span id="post_code" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="telephone" class="control-group">--}}
{{--        {{ Form::label('telephone', 'Telephone Number', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('telephone',isset($agent)?$agent->telephone:null, array('class' => 'input-medium')) }}--}}
{{--            <span id="telephone" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="fax" class="control-group">--}}
{{--        {{ Form::label('fax', 'Fax Number', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('fax',isset($agent)?$agent->fax:null, array('class' => 'input-medium')) }}--}}
{{--            <span id="fax" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="email" class="control-group">--}}
{{--        {{ Form::label('email', 'Email', array('class' => 'control-label')) }}--}}
{{--        <div class="controls">--}}
{{--            {{ Form::text('email',isset($agent)?$agent->email:null, array('class' => 'input-large')) }}--}}
{{--            <span id="email" class="help-block"></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @if ($isModal)--}}
{{--        {{ Form::hidden('isModal', $isModal) }}--}}
{{--    @endif--}}
{{--    {{ Form::hidden('id', $agentId) }}--}}
{{--    {{ Form::hidden('address_id', isset($agent)?$agent->address_id:null) }}--}}
{{--<!-- submit button -->--}}
{{--    <div class="form-actions">--}}
{{--        <button id="save-agent" class="btn btn-large btn-primary" type="submit">--}}
{{--            Save Changes--}}
{{--        </button>--}}
{{--    </div>--}}
{{--    {{ Form::close()}}--}}
{{--</fieldset>--}}

{{--@push('js')--}}
{{--    <script src="{{ asset('js/moved/libs/jquery.form.js') }}"></script>--}}
{{--@endpush--}}
