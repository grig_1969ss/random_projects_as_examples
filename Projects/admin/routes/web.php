<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function () {
    $data = \App\Models\ServiceCharge::select('budget_quarterly_payment')->cursor();
    $results = [];
    foreach ($data as $d) {
        if (! is_numeric($d->budget_quarterly_payment) && $d->budget_quarterly_payment != '' && ! is_null($d->budget_quarterly_payment) && ! empty($d->budget_quarterly_payment)) {
            $results[] = $d->budget_quarterly_payment;
        }
    }

    dd($results);
});

Route::middleware(['auth', 'client'])->namespace('Admin')->domain('admin.'.config('app.host'))->group(function () {
    Route::redirect('/', '/properties', 301);

    //Diary
    Route::middleware(['diary'])->group(function () {
        Route::resource('diary', 'DiaryController'); //->only(['index'])
        Route::get('diary/typeId/{id}', 'DiaryController@index')->name('diary.type');
    });

    Route::post('property/alert', 'AlertController@store')->name('property.alert.create');
    Route::delete('property/alert-delete', 'AlertController@destroy');

    Route::resource('properties', 'PropertyController');
    Route::get('overview/{date?}', 'PropertyController@overview')->name('properties.overview');
    Route::get('properties/{property}/duplicate', 'PropertyController@duplicate')->name('properties.duplicate');
    Route::post('properties/{property}/dispose', 'PropertyController@dispose')->name('properties.dispose');
    Route::resource('properties.payments', 'PaymentController');
    Route::resource('properties.utilities', 'UtilityController');
    Route::resource('properties.rents', 'RentController')->only(['create', 'store']);
    Route::resource('properties.documents', 'PropertyDocumentController'); /*->only('store')*/

    Route::resource('properties.service-charges', 'ServiceChargeController');
    Route::resource('properties.rent-reviews', 'RentReviewController')->only(['create', 'store']);
    Route::resource('properties.breaks', 'BreakController')->only(['create', 'store']);
    Route::resource('properties.client-queries', 'ClientQueryController')->only(['create', 'store']);
    Route::resource('properties.maintenances', 'MaintenanceController')->only(['create', 'store']);
    Route::resource('properties.notes', 'NoteController')->only('store');
    Route::resource('properties.insurances', 'InsuranceController')->only(['create', 'store']);
    Route::get('property/search', 'PropertyController@search');
    Route::post('property/rediarise', 'PropertyController@reDiarise')->name('rediarise');
    Route::get('property/filter-note', 'PropertyController@filterNote');
    Route::get('property/load-template-data/{id}', 'PropertyController@loadTemplateData');
    Route::get('property/attach-to/{id}', 'PropertyController@attachTo');

    Route::middleware(['agent'])->group(function () {
        Route::get('ajax/agents/{agent}/contacts/select', 'AgentController@getContactsSelectPartial')
            ->name('ajax.agents.contacts.select');
        Route::resource('agents', 'AgentController');

        Route::get('ajax/agents/{agent}/contacts/create', 'ContactController@ajaxCreate')
            ->name('ajax.agents.contacts.create');
        Route::resource('agents.contacts', 'ContactController')->only(['create', 'store']);
        Route::resource('agents.notes', 'NoteController')->only('store');
    });

    Route::resource('landlords', 'LandlordController');
    Route::resource('landlords.contacts', 'ContactController')->only(['create', 'store']);
    Route::resource('landlords.notes', 'NoteController')->only('store');

    Route::get('clients/{client}/audit-registers-partial', 'ClientController@viewAuditRegistersPartial');
    Route::resource('clients', 'ClientController');
    Route::resource('clients.contacts', 'ContactController')->only(['create', 'store']);
    Route::resource('clients.notes', 'NoteController')->only('store');

    Route::resource('contacts', 'ContactController');
    Route::resource('contacts.notes', 'NoteController')->only('store');

    Route::resource('payments', 'PaymentController')->only(['index', 'edit', 'update', 'destroy']);
    Route::resource('utilities', 'UtilityController')->only(['index', 'edit', 'update', 'destroy']);
    Route::resource('payments.documents', 'DocumentController')->only('store');
    Route::get('ajax/payments/{payment}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.payments.documents.create');

    Route::resource('rents', 'RentController')->only(['edit', 'update', 'destroy']);

    Route::resource('service-charges', 'ServiceChargeController');
    Route::resource('service-charges.documents', 'DocumentController')->only('store')->parameters([
        'service-charges' => 'serviceCharge',
    ]);
    Route::get('ajax/service-charges/{serviceCharge}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.service-charges.documents.create');

    Route::resource('rent-reviews', 'RentReviewController')->only(['edit', 'update']);
    Route::resource('rent-reviews.documents', 'DocumentController')->only('store')->parameters([
        'rent-reviews' => 'rentReview',
    ]);
    Route::get('ajax/rent-reviews/{rentReview}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.rent-reviews.documents.create');

    Route::resource('breaks', 'BreakController')->only(['edit', 'update', 'destroy']);
    Route::resource('breaks.documents', 'DocumentController')->only('store');
    Route::get('ajax/breaks/{break}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.breaks.documents.create');

    Route::resource('client-queries', 'ClientQueryController')->only(['edit', 'update']);
    Route::resource('client-queries.documents', 'DocumentController')->only('store')->parameters([
        'client-queries' => 'clientQuery',
    ]);
    Route::get('ajax/client-queries/{clientQuery}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.client-queries.documents.create');

    Route::resource('insurances', 'InsuranceController')->only(['edit', 'update', 'destroy']);
    Route::resource('insurances.documents', 'DocumentController')->only('store');
    Route::get('ajax/insurances/{insurance}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.insurances.documents.create');

    Route::resource('maintenances', 'MaintenanceController')->only(['edit', 'update']);
    Route::resource('maintenances.photos', 'MaintenancePhotoController')->only('store');
    Route::resource('maintenances.documents', 'DocumentController')->only('store');
    Route::get('ajax/maintenances/{maintenance}/documents/create', 'DocumentController@ajaxCreate')
        ->name('ajax.maintenances.documents.create');

    Route::resource('documents', 'DocumentController');

    Route::get('ajax/suppliers/{supplier}/contacts/select', 'SupplierController@getContactsSelectPartial')
        ->name('ajax.suppliers.contacts.select');
    Route::resource('suppliers', 'SupplierController');

    Route::get('ajax/suppliers/{supplier}/contacts/create', 'ContactController@ajaxCreate')
        ->name('ajax.suppliers.contacts.create');
    Route::resource('suppliers.contacts', 'ContactController')->only(['create', 'store']);
    Route::resource('suppliers.notes', 'NoteController')->only('store');

    Route::resource('administrators', 'AdministratorController');
    Route::resource('administrators.contacts', 'ContactController')->only(['create', 'store']);

    Route::resource('audit-registers', 'AuditRegisterController');

    Route::resource('claims', 'ClaimController');
    Route::resource('claims.notes', 'NoteController')->only('store');

    Route::get('claim-types/{claimType}/subtypes-partial', 'ClaimTypeController@viewSubtypesPartial');

    Route::resource('auditors', 'AuditorController');

    Route::resource('settings', 'SettingsController')->only('index');
    Route::post('settings/type', 'SettingsController@postType')->name('settings.type');
    Route::put('settings/type', 'SettingsController@updateType')->name('settings.update-type');
    Route::delete('settings/type', 'SettingsController@deleteType')->name('settings.delete-type');

    Route::resource('settings/currencies', 'CurrencyController');
    Route::get('settings/currencies/{currency}/delete', 'CurrencyController@delete');

    Route::resource('settings/payment-types', 'PaymentTypeController');
    Route::get('settings/payment-types/{paymentType}/delete', 'PaymentTypeController@delete');

    Route::resource('settings/claim-subtypes', 'ClaimSubtypeController');
    Route::get('settings/claim-subtypes/{claimSubtype}/delete', 'ClaimSubtypeController@delete');

    Route::resource('settings/staff', 'SettingsStaffController')->except('destroy')->middleware('superAdmin');

    Route::get('index-table', 'SettingsController@indexTable');
    Route::get('show-type/{id}', 'SettingsController@showType');
    Route::post('type-store', 'SettingsController@storeType')->name('type.store');
    Route::put('type-update', 'SettingsController@updateType')->name('type.update');
    Route::delete('type-delete', 'SettingsController@destroyType')->name('type.delete');

    // issue with this routes
    Route::middleware(['superAdmin'])->group(function () {
        Route::get('/account/settings', 'SettingsController@index')->name('account.settings');
        Route::get('/account/settings/user/{id}', 'SettingsController@userDetails')->name('user.details');
        Route::get('/account/settings/user/edit/{id}', 'SettingsController@userDetailsEdit')->name('user.details.edit');
    });
    Route::get('account/switch-client', 'SwitchClientController@switchClient')->name('switch.client');
    Route::post('account/settings/switch-client', 'SwitchClientController@saveSwitchClient')->name('save.current.client');

    // REPORTS
    Route::middleware(['report'])->group(function () {
        Route::get('reports/new/{type}', 'ReportController@getCommonTable');
        Route::get('reports/download/zip', 'ReportController@downloadAsZip')->name('reports.download.zip');

        Route::get('reports/master-spreadsheet', 'ReportController@getMasterSpreadsheet')
            ->name('reports.master-spreadsheet');
        Route::get('reports/rent-review', 'ReportController@getRentReview')->name('reports.rent-review');
        Route::get('reports/rent-review-csv', 'ReportCsvController@getRentReviewCSV')->name('reports.rent-review-csv');
        Route::get('reports/lease-expiry', 'ReportController@getLeaseExpiry')->name('reports.lease-expiry');

        Route::get('reports/lease-expiry-csv', 'ReportCsvController@getLeaseExpiryCSV')->name('reports.lease-expiry-csv');
        Route::get('reports/rateable-value', 'ReportController@getRateableValue')->name('reports.rateable-value');
        Route::get('reports/rateable-value-csv', 'ReportCsvController@getRateableValueCSV')
            ->name('reports.rateable-value-csv');
        Route::get('reports/break-option', 'ReportController@getBreakOption')->name('reports.break-option');
        Route::get('reports/break-option-csv', 'ReportCsvController@getBreakOptionCSV')->name('reports.break-option-csv');
        Route::get('reports/condition', 'ReportController@getCondition')->name('reports.condition');
        Route::get('reports/condition-csv', 'ReportCsvController@getConditionCSV')->name('reports.condition-csv');
        Route::get('reports/document-checklist', 'ReportController@getDocumentChecklist')
            ->name('reports.document-checklist');
        Route::get('reports/document-checklist-csv', 'ReportCsvController@getDocumentChecklistCSV')
            ->name('reports.document-checklist-csv');
        Route::get('reports/maintenance', 'ReportController@getMaintenance')->name('reports.maintenance');
        Route::get('reports/maintenance-csv', 'ReportCsvController@getMaintenanceCSV')->name('reports.maintenance-csv');
        Route::get('reports/rental-liability', 'ReportController@getRentalLiability')->name('reports.rental-liability');
        Route::get('reports/rental-liability-csv', 'ReportCsvController@getRentalLiabilityCSV')
            ->name('reports.rental-liability-csv');
        Route::get('reports/budget', 'ReportController@getBudget')->name('reports.budget');
        Route::get('reports/annual-service-charge-csv', 'ReportCsvController@getAnnualServiceChargeCSV')
            ->name('reports.annual-service-charge-csv');
        Route::get('reports/actual', 'ReportController@getActual')->name('reports.actual');
        Route::get('reports/missing/{type}', 'ReportController@getMissing')->name('reports.missing');
        Route::get('reports/missingrecs/{type}', 'ReportController@getMissing')->name('reports.missingrecs');
        Route::get('reports/missing-payments-csv/{type}', 'ReportCsvController@getMissingPaymentsCSV')
            ->name('reports.missing-payments-csv');
        Route::get('reports/service-charge', 'ReportController@allRecentChargesForPropertiesReport')
            ->name('reports.service-charge');
        Route::get('reports/service-charge-csv', 'ReportCsvController@allRecentChargesForPropertiesCSV')
            ->name('reports.service-charge-csv');
        Route::get('reports/on-account', 'ReportController@quarterlyServiceChargeReport')->name('reports.on-account');
        Route::get('reports/quarter-budget-csv', 'ReportCsvController@getQuarterBudgetCSV')
            ->name('reports.quarter-budget-csv');
        Route::get('reports/get-document-tables', 'ReportController@getDocumentTables')
            ->name('reports.get-document-tables');
        Route::get('reports/approvals', 'ReportController@getApprovals')->name('reports.approvals');
        Route::get('reports/approvals-pdf', 'ReportController@getApprovalsPDF')->name('reports.approvals-pdf');
        Route::get('reports/properties-pdf', 'ReportController@getPropertiesPDF')->name('reports.properties-pdf');
        Route::get('reports/approvals-by-property', 'ReportController@getApprovalsByProperty')
            ->name('reports.approvals-by-property');
        Route::get('reports/approvals-by-property-csv', 'ReportCsvController@getApprovalsByPropertyCSV')
            ->name('reports.approvals-by-property-csv');
        Route::get('reports/properties', 'ReportController@getProperties')->name('reports.properties');
        Route::get('reports/invoices-on-hold', 'ReportController@getInvoicesOnHold')->name('reports.invoices-on-hold');
        Route::get('reports/invoices-on-hold-csv', 'ReportCsvController@getInvoicesOnHoldCSV')
            ->name('reports.invoices-on-hold-csv');
        Route::get('reports/claims', 'ReportController@getClaims')->name('reports.claims');
        Route::get('reports/claims-csv', 'ReportCsvController@getClaimsCSV')
            ->name('reports.claims-csv');
        Route::get('reports/insurance-renewal-dates', 'ReportController@getInsuranceRenewalDates')
            ->name('reports.insurance-renewal-dates');
        Route::get('reports/insurance-renewal-dates-csv', 'ReportCsvController@getInsuranceRenewalDatesCSV')
            ->name('reports.insurance-renewal-dates-csv');
        Route::get('reports/service-charge-annual-budget', 'ReportController@getServiceChargeAnnualBudget')
            ->name('reports.service-charge-annual-budget');
        Route::get('reports/service-charge-annual-budget-csv', 'ReportCsvController@getServiceChargeAnnualBudgetCSV')
            ->name('reports.service-charge-annual-budget-csv');
        Route::get('reports/insurance-annual-budget', 'ReportController@getInsuranceAnnualBudget')
            ->name('reports.insurance-annual-budget');
        Route::get('reports/insurance-annual-budget-csv', 'ReportCsvController@getInsuranceAnnualBudgetCSV')
            ->name('reports.insurance-annual-budget-csv');
    });

    // Inbox
    Route::middleware(['inbox'])->group(function () {
        Route::get('inbox', 'InboxController@index')->name('inbox.index');
        Route::get('inbox/load-crud', 'InboxController@loadCrud');
        Route::get('inbox/iframe/data', 'InboxController@detail');

        Route::post('inbox/client-properties', 'InboxController@linkEmail')->name('link.email');
        Route::post('inbox/email-actions', 'InboxController@emailActions')->name('email.actions');
        Route::get('ajax/inbox', 'InboxController@ajaxInbox')->name('ajax.inbox');
        Route::get('ajax/load-inbox', 'InboxController@ajaxLoadInbox')->name('ajax.load-inbox');
        Route::get('inbox/pdf-preview/{name}', 'DocumentController@inboxPdfPreview');
        Route::get('inbox/mark-as-unread/{email}', 'DocumentController@markAsUnread')->name('unread');
        Route::get('inbox/{email}', 'InboxController@detail')->name('email.detail');
    });

    // Emails
    Route::resource('emails', 'EmailController')->only(['create', 'store']);
    Route::get('emails/attach-pdf/properties', 'EmailController@pdfProperties');
    Route::get('emails/attach-pdf/emails', 'EmailController@pdfEmails');
    Route::get('emails/attach-pdf/preview', 'EmailController@generatePreviewFile');
    Route::get('emails/{email}/reply', 'EmailController@reply')->name('emails.reply');
    Route::get('emails/{email}/archive', 'EmailController@archive')->name('emails.archive');
    Route::post('emails/email/adddiarise', 'EmailController@emailAddDiarise')->name('email-adddiarise');

    // Notes
    Route::resource('issues.notes', 'NoteController')->only(['create', 'store']);

    // Documents
    Route::resource('issues.documents', 'DocumentController')->only(['create', 'store']);
    Route::resource('documents', 'DocumentController')->only('show');
    Route::post('documents/docs/delete', 'DocumentController@docsDelete')->name('docs-delete');

    //Attachment
    Route::get('attachments/download/{id}', 'AttachmentController@download')->name('attachments.download');

    //search
    Route::get('global/search', 'SearchController@globalSearch');
    Route::get('search/', 'SearchController@search')->name('search');
});

//Auth routes
Auth::routes(['register' => false]);
