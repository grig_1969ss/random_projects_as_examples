<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->domain('api.'.config('app.host'))->group(function () {
    Route::post('inbound-mail/'.config('services.postmark.url_secret'), 'InboundMailController@inbound');
    Route::post('inbound-mail-status/'.config('services.postmark.url_secret'), 'InboundMailController@mailStatus');
    Route::get('test-inbound-mail', 'InboundMailController@testInbound');
    Route::post('mail-delivery/'.config('services.postmark.url_secret'), 'EmailController@processMailDelivery');
});
