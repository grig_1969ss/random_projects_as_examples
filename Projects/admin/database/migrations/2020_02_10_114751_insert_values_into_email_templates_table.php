<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertValuesIntoEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $state = ['template_type_id' => 1, 'subject' =>  $this->realEstateSubject, 'body' =>  $this->realEstateBody];
        $franchiseeOnly = ['template_type_id' => 2, 'subject' =>  $this->franchiseeOnlySubject, 'body' =>  $this->franchiseeOnlyBody];
        $reconciliation = ['template_type_id' => 3, 'subject' =>  $this->reconciliationSubject, 'body' =>  $this->reconciliationBody];

        DB::table('email_templates')->insert($state);
        DB::table('email_templates')->insert($franchiseeOnly);
        DB::table('email_templates')->insert($reconciliation);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('email_templates')->whereIn('id', [1, 2, 3])->delete();
    }

    private $realEstateSubject = '#{{ property_reference }} ({{ property_name }}) Change Form';

    private $realEstateBody =
        'Hi Real Estate,<br>
Please see below change form details for the above property:<br>
DETAILS OF CHANGE: {{ increase_decrease }} IN SERVICE CHARGE<br>
STORE NAME: {{ property_name }}<br>
STORE NUMBER: #{{ property_reference }}<br>
CURRENT SERVICE CHARGE: {{ currency_symbol }}{{ previous_annual_budget }} NET PER ANNUM ({{ currency_symbol }}{{ previous_quarterly_budget }} NET Quarterly)<br>
NEW SERVICE CHARGE (DUE FROM {{ previous_year end + 1 month (Formatted as "JANUARY 2019" }}): {{ currency_symbol }}{{ current_annual_budget }} NET PER ANNUM ({{ currency_symbol }}{{ current_quarterly_budget }} NET Quarterly)<br>
INCREASE SERVICE CHARGE ON \'SERVICE CHARGE SCHEDULE\':PLEASE AMEND IMMEDIATELY (IN TIME FOR {{ previous_year end + 6 months (Formatted as "June 2019" }}).<br>
Please pay uplift for [[ Quarter ]] of {{ currency_symbol }}[[ Uplift Amount ]]';

    private $franchiseeOnlySubject = 'New Service charge budget';

    private $franchiseeOnlyBody =
        'Good afternoon {{ franchisee_name }},<br>
Please find attached a copy of the Landlord’s Service Charge budget for the year ending {{ current_year_end }} for the above property.<br>
The overall budget for the forthcoming year has {{ increased_decreased }} from previous.<br>
This has resulted in an annual net figure of ({{ currency_symbol }}{{ current_annual_budget }} net p/a), please accrue for this figure, previous on account amount was (£{{ previous_annual_budget }} net p/a). All costs are detailed within the estimated expenditure table.<br>
I hope that the above and the attached provide all of the information you require but should you have any further questions please do not hesitate to contact me.<br>
Kind regards<br>
Rosemary';

    private $reconciliationSubject =
        '#{{ property_reference }} Landlord Certificate';

    private $reconciliationBody =
        'Dear {{ franchisee_name }},<br>
Please find attached a copy of the Landlord’s certificate for year ending {{ current_year_end }} for the above property.<br>
Having checked their reconciliation, it is in line with the industry recognised Code of Practice and we have a contractual obligation to contribute. The reconciled account is over the budgeted amount and has resulted in a balancing charge for the sum of £{{ current_annual_budget }} net being due. Details of all increases are explained within the attached reconciliation.I hope that the above and the attached provide all of the information you require but should you have any further questions please do not hesitate to contact me.';
}
