<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecdUpdatedColumnsToServiceChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_charges', function (Blueprint $table) {
            $table->boolean('budget_details_recd')->after('apportionment_type_id')->default(0);
            $table->boolean('franchisee_updated')->after('budget_details_recd')->default(0);
            $table->boolean('real_estate_updated')->after('franchisee_updated')->default(0);

            $table->boolean('annual_reconciliation_recd')->after('real_estate_updated')->default(0);
            $table->boolean('annual_franchisee_updated')->after('annual_reconciliation_recd')->default(0);
            $table->boolean('annual_real_estate_updated')->after('annual_franchisee_updated')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_charges', function (Blueprint $table) {
            $table->dropColumn('budget_details_recd');
            $table->dropColumn('franchisee_updated');
            $table->dropColumn('real_estate_updated');

            $table->dropColumn('annual_reconciliation_recd');
            $table->dropColumn('annual_franchisee_updated');
            $table->dropColumn('annual_real_estate_updated');
        });
    }
}
