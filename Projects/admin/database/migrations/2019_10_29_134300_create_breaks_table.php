<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breaks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable()->index('property_id');
            $table->integer('diary_id')->nullable()->index('diary_id');
            $table->dateTime('break_date')->nullable();
            $table->string('number_of_days')->nullable();
            $table->text('break_clause', 65535)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->integer('archived')->default(0);
            $table->integer('break_clause_type_id')->nullable()->index('break_clause_type_id');
            $table->dateTime('notice_period')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('breaks');
    }
}
