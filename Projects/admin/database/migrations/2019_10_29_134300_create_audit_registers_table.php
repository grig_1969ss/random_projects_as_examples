<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuditRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('audit_name');
            $table->date('audit_start_date');
            $table->date('audit_end_date');
            $table->string('audit_number');
            $table->boolean('administration')->default(0);
            $table->integer('client_id')->unsigned()->index('audit_registers_client_id_foreign');
            $table->integer('created_by')->unsigned()->nullable()->index('audit_registers_created_by_foreign');
            $table->integer('updated_by')->unsigned()->nullable()->index('audit_registers_updated_by_foreign');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_registers');
    }
}
