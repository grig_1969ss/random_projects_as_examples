<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable()->index('property_id');
            $table->integer('diary_id')->nullable()->index('diary_id');
            $table->string('percentage_apportion')->nullable();
            $table->integer('apportionment_type_id')->nullable()->index('apportionment_type_id');
            $table->date('year_end')->nullable();
            $table->boolean('exclusions')->nullable();
            $table->string('year_end_cost_per_sq_ft')->nullable();
            $table->string('balance_service_charge')->nullable();
            $table->string('service_charge_budget_differential')->nullable();
            $table->dateTime('service_charge_commencement_date')->nullable();
            $table->string('agent_contributions')->nullable();
            $table->string('potential_savings_against_budget')->nullable();
            $table->string('amount_invoiced')->nullable();
            $table->dateTime('date_invoiced')->nullable();
            $table->string('amount_received')->nullable();
            $table->dateTime('date_received')->nullable();
            $table->string('budget_year_end')->nullable();
            $table->text('wip', 65535)->nullable();
            $table->text('lease_comments', 65535)->nullable();
            $table->text('charge_details', 65535)->nullable();
            $table->boolean('invoiced')->nullable();
            $table->string('budget_quarterly_payment')->nullable();
            $table->string('actual_quarterly_payment')->nullable();
            $table->string('wip_amount')->nullable();
            $table->string('actual_year_end')->nullable();
            $table->string('actual_against_budget')->nullable();
            $table->boolean('confirmed')->nullable();
            $table->string('potential_budget')->nullable();
            $table->decimal('annual_budget')->nullable();
            $table->string('current_contribution')->nullable();
            $table->string('forcast_contribution_year1')->nullable();
            $table->string('current_turnover')->nullable();
            $table->string('forcast_turnover_year1')->nullable();
            $table->string('forcast_turnover_year2')->nullable();
            $table->string('forcast_contribution_year2')->nullable();
            $table->binary('notes', 65535)->nullable();
            $table->integer('service_charge_status_id')->nullable()->index('service_charge_status_id');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('vat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_charges');
    }
}
