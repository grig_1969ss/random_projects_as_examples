<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('from');
            $table->string('from_name');
            $table->string('subject');
            $table->string('message_id');
            $table->dateTime('date');
            $table->text('text_body');
            $table->text('html_body');
            $table->text('stripped_text_reply');
            $table->boolean('starred')->default(false);
            $table->bigInteger('email_type_id')->unsigned()->nullable();
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('email_type_id')->references('id')->on('email_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
