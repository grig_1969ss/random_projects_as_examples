<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200)->nullable();
            $table->string('username', 60)->default('')->index();
            $table->string('password', 60)->index();
            $table->string('remember_token', 100)->nullable();
            $table->string('email')->index();
            $table->string('job_title')->nullable();
            $table->binary('avatar')->nullable();
            $table->boolean('verified')->default(false);
            $table->boolean('disabled')->default(false);
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->integer('region_id')->nullable();
            $table->boolean('read_only')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
