<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_type_id')->nullable()->index('property_type_id');
            $table->integer('client_id')->nullable()->index('client_id');
            $table->integer('landlord_id')->nullable()->index('landlord_id');
            $table->integer('agent_id')->nullable()->index('agent_id');
            $table->integer('agent_contact_id')->unsigned()->nullable()->index('properties_agent_contact_id_foreign');
            $table->integer('owner_type_id')->unsigned()->nullable()->index('properties_owner_type_id_foreign');
            $table->string('reference')->nullable();
            $table->string('property_name')->nullable();
            $table->string('store_contact')->nullable();
            $table->string('store_telephone')->nullable();
            $table->integer('property_status_id')->nullable()->index('property_status_id');
            $table->integer('address_id')->nullable()->index('address_id');
            $table->string('floor_area_net_trading')->nullable();
            $table->float('rateable_value', 10, 0)->nullable();
            $table->float('proposed_rateable_value', 10, 0)->nullable();
            $table->float('agreed_rateable_value', 10, 0)->nullable();
            $table->integer('appeal_type_id')->nullable()->index('appeal_type_id');
            $table->binary('rv_notes', 65535)->nullable();
            $table->integer('tenure_type_id')->nullable()->index('tenure_type_id');
            $table->dateTime('purchase_date')->nullable();
            $table->dateTime('commencement_date')->nullable();
            $table->float('purchase_price', 10, 0)->nullable();
            $table->float('book_value', 10, 0)->nullable();
            $table->float('freehold_value', 10, 0)->nullable();
            $table->float('rental_value', 10, 0)->nullable();
            $table->float('book_rental_value', 10, 0)->nullable();
            $table->float('est_repairs_cost', 10, 0)->nullable();
            $table->float('repair_liability', 10, 0)->nullable();
            $table->dateTime('date_last_inspected')->nullable();
            $table->dateTime('lease_start_date')->nullable();
            $table->integer('break_clause_type_id')->nullable()->index('break_clause_type_id');
            $table->boolean('undergoing_refit')->nullable()->default(0);
            $table->boolean('has_service_charge')->nullable()->default(0);
            $table->boolean('has_break_options')->default(1);
            $table->boolean('has_client_queries')->default(1);
            $table->boolean('has_maintenances')->default(1);
            $table->boolean('has_rent_reviews')->default(1);
            $table->boolean('has_notes')->default(1);
            $table->binary('lease_comments', 65535)->nullable();
            $table->string('lease_term')->nullable();
            $table->dateTime('lease_expiry_date')->nullable();
            $table->dateTime('rent_review_date')->nullable();
            $table->dateTime('erv_date')->nullable();
            $table->string('initial_concessions')->nullable();
            $table->float('erv_amount', 10, 0)->nullable();
            $table->dateTime('lease_notice_dates')->nullable();
            $table->string('guarantor')->nullable();
            $table->string('tenant_break_option')->nullable();
            $table->string('agent_break_option')->nullable();
            $table->integer('break_notice_period')->nullable();
            $table->dateTime('notice_dates')->nullable();
            $table->string('annual_rent')->nullable();
            $table->string('rent_year_2')->nullable();
            $table->string('rent_year_3')->nullable();
            $table->string('rent_year_4')->nullable();
            $table->string('rent_year_5')->nullable();
            $table->string('payment_frequency')->nullable();
            $table->integer('rent_payment_frequency_id')->nullable()->index('rent_payment_frequency_id');
            $table->string('payment_dates')->nullable();
            $table->float('deposit', 10, 0)->nullable();
            $table->float('rent_paid', 10, 0)->nullable();
            $table->string('insurance_supplier')->nullable();
            $table->integer('disposal_type_id')->nullable()->index('disposal_type_id');
            $table->dateTime('disposal_date')->nullable();
            $table->integer('alienation_type_id')->nullable()->index('alienation_type_id');
            $table->boolean('concessions')->nullable();
            $table->binary('alienation_note', 65535)->nullable();
            $table->binary('alteration_note', 65535)->nullable();
            $table->boolean('non_structural')->nullable();
            $table->boolean('structural')->nullable();
            $table->boolean('landlord_consent')->nullable();
            $table->boolean('schedule_condition')->nullable();
            $table->integer('restrictions')->nullable();
            $table->integer('user_type_id')->nullable()->index('user_type_id');
            $table->binary('user_clause_note', 65535)->nullable();
            $table->binary('initial_concessions_note', 65535)->nullable();
            $table->integer('subletting_type_id')->nullable()->index('subletting_type_id');
            $table->string('ext_frequency')->nullable();
            $table->dateTime('ext_first_due')->nullable();
            $table->dateTime('ext_next_due')->nullable();
            $table->boolean('ext_final_year')->nullable();
            $table->string('int_frequency')->nullable();
            $table->dateTime('int_first_due')->nullable();
            $table->dateTime('int_next_due')->nullable();
            $table->boolean('int_final_year')->nullable();
            $table->integer('repair_classification_type_id')->nullable()->index('repair_classification_type_id');
            $table->binary('repair_obligations_note', 65535)->nullable();
            $table->boolean('share')->nullable();
            $table->boolean('auto_service_charge')->nullable();
            $table->text('general_info', 65535)->nullable();
            $table->boolean('drive_thru')->default(0);
            $table->integer('store_type_id')->unsigned()->nullable()->index('properties_store_type_id_foreign');
            $table->date('open_date')->nullable();
            $table->string('client_surveyor')->nullable();
            $table->string('client_admin')->nullable();
            $table->string('capa_admin')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->integer('region_id')->nullable();
            $table->integer('inside_landlord')->default(-1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
