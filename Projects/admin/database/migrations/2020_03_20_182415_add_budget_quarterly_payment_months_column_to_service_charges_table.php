<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBudgetQuarterlyPaymentMonthsColumnToServiceChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_charges', function (Blueprint $table) {
            $table->dropColumn('balance_service_charge_monthly');
            $table->integer('budget_quarterly_payment_months')->default(3)->after('budget_quarterly_payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_charges', function (Blueprint $table) {
            $table->boolean('balance_service_charge_monthly')->default(0)->after('balance_service_charge')->nullable();
            $table->dropColumn('budget_quarterly_payment_months');
        });
    }
}
