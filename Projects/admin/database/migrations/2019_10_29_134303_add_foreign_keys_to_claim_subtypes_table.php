<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClaimSubtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_subtypes', function (Blueprint $table) {
            $table->foreign('claim_type_id')->references('id')->on('claim_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_subtypes', function (Blueprint $table) {
            $table->dropForeign('claim_subtypes_claim_type_id_foreign');
            $table->dropForeign('claim_subtypes_created_by_foreign');
            $table->dropForeign('claim_subtypes_updated_by_foreign');
        });
    }
}
