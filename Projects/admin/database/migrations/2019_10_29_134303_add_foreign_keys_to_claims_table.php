<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function (Blueprint $table) {
            $table->foreign('audit_register_id')->references('id')->on('audit_registers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('auditor_id')->references('id')->on('auditors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('claim_status_id')->references('id')->on('claim_statuses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('claim_subtype_id')->references('id')->on('claim_subtypes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('claim_type_id')->references('id')->on('claim_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('client_id')->references('id')->on('clients')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims', function (Blueprint $table) {
            $table->dropForeign('claims_audit_register_id_foreign');
            $table->dropForeign('claims_auditor_id_foreign');
            $table->dropForeign('claims_claim_status_id_foreign');
            $table->dropForeign('claims_claim_subtype_id_foreign');
            $table->dropForeign('claims_claim_type_id_foreign');
            $table->dropForeign('claims_client_id_foreign');
            $table->dropForeign('claims_created_by_foreign');
            $table->dropForeign('claims_currency_id_foreign');
            $table->dropForeign('claims_updated_by_foreign');
        });
    }
}
