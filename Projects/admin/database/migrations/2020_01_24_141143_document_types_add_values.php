<?php

use Illuminate\Database\Migrations\Migration;

class DocumentTypesAddValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = \Carbon\Carbon::now();

        \App\Models\DocumentType::insert(
            ['id' => 41, 'name' => 'Service Charge Budget', 'created_at' => $now, 'updated_at' => $now]
        );

        \App\Models\DocumentType::insert(
            ['id' => 42, 'name' => 'Service Charge Annual Rec', 'created_at' => $now, 'updated_at' => $now]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\DocumentType::whereIn('id', [41, 42])->orWhere('id', 42)->forceDelete();
    }
}
