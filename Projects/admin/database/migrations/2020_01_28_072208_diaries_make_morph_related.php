<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class DiariesMakeMorphRelated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diaries', function ($table) {
            $table->integer('diaryable_id')->nullable()->index('diaryable_id')->after('id');
            $table->string('diaryable_type')->nullable()->after('diaryable_id');
            $table->text('comments')->nullable()->after('diaryable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diaries', function ($table) {
            $table->dropIndex('diaryable_id');
            $table->dropColumn('diaryable_id');
            $table->dropColumn('diaryable_type');
            $table->dropColumn('comments');
        });
    }
}
