<?php

use App\Models\EmailType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFolderColumnToEmailTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_types', function (Blueprint $table) {
            $table->string('folder', 50)->nullable()->after('name');
        });

        $types = [
            ['id' => 1, 'name' => 'New', 'folder' => 'Inbox'],
            ['id' => 2, 'name' => 'Sent', 'folder' => 'Sent'],
            ['id' => 3, 'name' => 'Draft', 'folder' => 'Draft'],
            ['id' => 4, 'name' => 'Scheduled', 'folder' => 'Scheduled'],

        ];

        foreach ($types as $type) {
            EmailType::where('id', $type['id'])->update(['name' => $type['name'], 'folder' => $type['folder']]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_types', function (Blueprint $table) {
            $table->dropColumn('folder')->unsigned()->nullable()->after('property_id');
        });
    }
}
