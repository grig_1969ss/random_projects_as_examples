<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCapaOwnerIdAndClientOwnerIdColumnsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->integer('capa_owner_id')->unsigned()->nullable()->after('id');
            $table->integer('client_owner_id')->unsigned()->nullable()->after('capa_owner_id');

            $table->foreign('capa_owner_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('client_owner_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign('properties_capa_owner_id_foreign');
            $table->dropForeign('properties_client_owner_id_foreign');
            $table->dropColumn('capa_owner_id');
            $table->dropColumn('client_owner_id');
        });
    }
}
