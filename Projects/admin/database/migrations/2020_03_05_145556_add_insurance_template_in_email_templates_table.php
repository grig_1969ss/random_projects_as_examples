<?php

use Illuminate\Database\Migrations\Migration;

class AddInsuranceTemplateInEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->insert([
            'template_type_id' => 2,
            'created_by' => 1,
            'updated_by' => 1,
            'subject' => '#{{ property_reference }} ({{ property_name }}) Landlord Insurance',
            'body' => 'Dear {{ franchisee_name }},

Please find attached details of the Landlords insurance for the period {{ period }}.

We have a copy of the Landlords insurance certificate on file.

Your premium for the next insurance period is {{ currency_symbol }}{{ insurance_premium }}. This is in line with current costs and represents a {{ percentage_increase_decrease }}% {{ increase_decrease }}.

The main points to note are the level of buildings cover has increased by [[ £amount]]

Your sincerely

McDonalds Service Charge Management Team
Please reply to Email  {{ client_inbox_email }}',

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('email_templates')->where('id', 4)->delete();
    }
}
