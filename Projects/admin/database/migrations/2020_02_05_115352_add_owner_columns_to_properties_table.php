<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOwnerColumnsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->integer('capa_owner2_id')->nullable()->unsigned()->after('capa_owner_id');
            $table->integer('client_surveyor_id')->nullable()->unsigned()->after('client_owner_id');

            $table->foreign(['capa_owner2_id'])
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign(['client_surveyor_id'])
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign(['capa_owner2_id']);
            $table->dropForeign(['client_surveyor_id']);
            $table->dropColumn('capa_owner2_id');
            $table->dropColumn('client_surveyor_id');
        });
    }
}
