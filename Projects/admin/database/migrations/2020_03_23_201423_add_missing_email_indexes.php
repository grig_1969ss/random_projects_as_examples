<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMissingEmailIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attachment_details', function (Blueprint $table) {
            $table->index('email_id');
            $table->index('content_type');
            $table->index('contentid');
            $table->index('deleted_at');
        });

        Schema::table('emails', function (Blueprint $table) {
            $table->index('subject');
            $table->index('message_id');
            $table->index('read_at');
            $table->index('read_by');
            $table->index('starred');
            $table->index('important');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attachment_details', function (Blueprint $table) {
            $table->dropIndex('attachment_details_email_id_index');
            $table->dropIndex('attachment_details_content_type_index');
            $table->dropIndex('attachment_details_contentid_index');
            $table->dropIndex('attachment_details_deleted_at_index');
        });

        Schema::table('emails', function (Blueprint $table) {
            $table->dropIndex('emails_subject_index');
            $table->dropIndex('emails_message_id_index');
            $table->dropIndex('emails_read_at_index');
            $table->dropIndex('emails_read_by_index');
            $table->dropIndex('emails_starred_index');
            $table->dropIndex('emails_important_index');
        });
    }
}
