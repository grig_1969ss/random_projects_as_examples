<?php

use Illuminate\Database\Migrations\Migration;

class AddPropertyValueToDiaryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = now();

        \App\Models\DiaryType::insert([
            'name' => 'Property',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\DiaryType::where('name', 'Property')->delete();
    }
}
