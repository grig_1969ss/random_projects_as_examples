<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ClientsTableAddTypeBooleans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function ($table) {
            $table->boolean('hide_service_charge')->default(0)->after('administrator_id');
            $table->boolean('hide_break_options')->default(0)->after('hide_service_charge');
            $table->boolean('hide_client_queries')->default(0)->after('hide_break_options');
            $table->boolean('hide_maintenance')->default(0)->after('hide_client_queries');
            $table->boolean('hide_rent_reviews')->default(0)->after('hide_maintenance');
            $table->boolean('hide_lease')->default(0)->after('hide_rent_reviews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function ($table) {
            $table->dropColumn('hide_service_charge');
            $table->dropColumn('hide_break_options');
            $table->dropColumn('hide_client_queries');
            $table->dropColumn('hide_maintenance');
            $table->dropColumn('hide_rent_reviews');
            $table->dropColumn('hide_lease');
        });
    }
}
