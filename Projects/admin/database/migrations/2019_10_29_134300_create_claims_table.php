<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('claim_number')->nullable();
            $table->string('supplier_number')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('invoice_number')->nullable();
            $table->date('invoice_date')->nullable();
            $table->decimal('original_net_value')->nullable();
            $table->decimal('vat')->nullable();
            $table->decimal('claim_adjustment')->nullable();
            $table->decimal('cancelled_claim')->nullable();
            $table->date('client_date')->nullable();
            $table->string('po_number')->nullable();
            $table->string('property_number')->nullable();
            $table->string('property_address')->nullable();
            $table->string('supplier_contact')->nullable();
            $table->integer('client_id')->unsigned()->nullable()->index('claims_client_id_foreign');
            $table->integer('audit_register_id')->unsigned()->nullable()->index('claims_audit_register_id_foreign');
            $table->integer('auditor_id')->unsigned()->nullable()->index('claims_auditor_id_foreign');
            $table->integer('claim_type_id')->unsigned()->nullable()->index('claims_claim_type_id_foreign');
            $table->integer('claim_subtype_id')->unsigned()->nullable()->index('claims_claim_subtype_id_foreign');
            $table->integer('currency_id')->unsigned()->nullable()->index('claims_currency_id_foreign');
            $table->integer('claim_status_id')->unsigned()->nullable()->index('claims_claim_status_id_foreign');
            $table->integer('created_by')->unsigned()->nullable()->index('claims_created_by_foreign');
            $table->integer('updated_by')->unsigned()->nullable()->index('claims_updated_by_foreign');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claims');
    }
}
