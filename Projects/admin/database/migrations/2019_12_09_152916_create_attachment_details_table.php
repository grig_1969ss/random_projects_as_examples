<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
                    CREATE TABLE `attachment_details` (
                    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `email_id` INT(10) UNSIGNED NOT NULL,
                    `name` VARCHAR(191) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                    `content` LONGTEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
                    `content_type` VARCHAR(191) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
                    `content_length` INT(10) UNSIGNED NOT NULL,
                    `created_at` TIMESTAMP NULL DEFAULT NULL,
                    `updated_at` TIMESTAMP NULL DEFAULT NULL,
                    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                COLLATE='utf8mb4_unicode_ci'
                ENGINE=InnoDB
                ;
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachment_details');
    }
}
