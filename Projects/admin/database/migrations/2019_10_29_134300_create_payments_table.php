<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable()->index('property_id');
            $table->integer('service_charge_id')->unsigned()->nullable()->index('payments_service_charge_id_foreign');
            $table->integer('supplier_id')->unsigned()->nullable()->index('payments_supplier_id_foreign');
            $table->integer('payment_type_id')->unsigned()->nullable()->index('payments_payment_type_id_foreign');
            $table->integer('diary_id')->unsigned()->nullable()->index('payments_diary_id_foreign');
            $table->integer('contact_id')->unsigned()->nullable()->index('payments_contact_id_foreign');
            $table->dateTime('payment_date')->nullable();
            $table->float('gross_invoice_amount', 10, 0)->nullable();
            $table->float('tax_amount')->nullable();
            $table->string('payment_ref')->nullable();
            $table->date('date_approved')->nullable();
            $table->string('period')->nullable();
            $table->integer('quarter')->nullable();
            $table->boolean('hold')->default(0);
            $table->text('note', 65535)->nullable();
            $table->text('description', 65535)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
