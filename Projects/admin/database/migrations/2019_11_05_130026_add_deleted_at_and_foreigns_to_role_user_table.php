<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedAtAndForeignsToRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $table->softDeletes();

            $table->foreign('role_id', 'role_id_foreign')->references('id')->on('roles')
                ->onDelete('cascade');
            $table->foreign('user_id', 'user_id_foreign')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropForeign('role_id_foreign');
            $table->dropForeign('user_id_foreign');
        });
    }
}
