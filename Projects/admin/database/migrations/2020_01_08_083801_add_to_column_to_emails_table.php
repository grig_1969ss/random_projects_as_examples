<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddToColumnToEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emails', function ($table) {
            $table->string('to')->after('property_id');
            $table->integer('created_by')->unsigned()->nullable()->after('sent_at');
            $table->string('message_id')->nullable()->change();

            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emails', function ($table) {
            $table->dropColumn('to');
            $table->dropForeign('emails_created_by_foreign');
            $table->dropColumn('created_by');
        });
    }
}
