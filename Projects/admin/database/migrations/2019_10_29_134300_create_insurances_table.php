<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->index('insurances_property_id_foreign');
            $table->integer('diary_id')->unsigned()->nullable()->index('insurances_diary_id_foreign');
            $table->integer('covered_by')->unsigned()->nullable()->index('insurances_covered_by_foreign');
            $table->boolean('block_policy')->default(0);
            $table->decimal('insurance_premium')->nullable();
            $table->integer('loss_of_rent')->nullable();
            $table->boolean('terrorism_cover')->default(0);
            $table->decimal('terrorism_premium')->nullable();
            $table->integer('plate_glass')->unsigned()->nullable()->index('insurances_plate_glass_foreign');
            $table->boolean('tenant_reimburses')->default(0);
            $table->boolean('valuation_fee_payable')->default(0);
            $table->string('period_of_insurance')->nullable();
            $table->string('insurer')->nullable();
            $table->date('insurance_renewal_date')->nullable();
            $table->text('notes', 65535)->nullable();
            $table->integer('created_by')->unsigned()->nullable()->index('insurances_created_by_foreign');
            $table->integer('updated_by')->unsigned()->nullable()->index('insurances_updated_by_foreign');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insurances');
    }
}
