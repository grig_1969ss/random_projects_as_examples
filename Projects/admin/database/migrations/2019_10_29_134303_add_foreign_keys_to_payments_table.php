<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->foreign('contact_id')->references('id')->on('contacts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('diary_id')->references('id')->on('diaries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('payment_type_id')->references('id')->on('payment_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payments_contact_id_foreign');
            $table->dropForeign('payments_diary_id_foreign');
            $table->dropForeign('payments_payment_type_id_foreign');
            $table->dropForeign('payments_supplier_id_foreign');
        });
    }
}
