<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimSubtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_subtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('claim_type_id')->unsigned()->index('claim_subtypes_claim_type_id_foreign');
            $table->integer('created_by')->unsigned()->nullable()->index('claim_subtypes_created_by_foreign');
            $table->integer('updated_by')->unsigned()->nullable()->index('claim_subtypes_updated_by_foreign');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_subtypes');
    }
}
