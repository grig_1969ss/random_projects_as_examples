<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->index('user_id');
            $table->dateTime('expiry_date');
            $table->string('link_salt', 200)->nullable();
            $table->string('encrypted_id', 200)->nullable();
            $table->string('unique_id', 200)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('link_data');
    }
}
