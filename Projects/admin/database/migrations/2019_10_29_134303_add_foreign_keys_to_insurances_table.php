<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurances', function (Blueprint $table) {
            $table->foreign('covered_by')->references('id')->on('insurance_parties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('diary_id')->references('id')->on('diaries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('plate_glass')->references('id')->on('insurance_parties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('property_id')->references('id')->on('properties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurances', function (Blueprint $table) {
            $table->dropForeign('insurances_covered_by_foreign');
            $table->dropForeign('insurances_created_by_foreign');
            $table->dropForeign('insurances_diary_id_foreign');
            $table->dropForeign('insurances_plate_glass_foreign');
            $table->dropForeign('insurances_property_id_foreign');
            $table->dropForeign('insurances_updated_by_foreign');
        });
    }
}
