<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaintenancePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('maintenance_id')->nullable()->index('maintenance_id');
            $table->string('document_name')->nullable();
            $table->string('document_file')->nullable();
            $table->string('content_type')->nullable();
            $table->boolean('archived')->nullable()->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maintenance_photos');
    }
}
