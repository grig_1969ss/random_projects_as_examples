<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('archived')->default(0);
            $table->integer('created_by')->unsigned()->nullable()->index('claim_statuses_created_by_foreign');
            $table->integer('updated_by')->unsigned()->nullable()->index('claim_statuses_updated_by_foreign');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_statuses');
    }
}
