<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRentReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable()->index('property_id');
            $table->string('break_option')->nullable();
            $table->dateTime('date')->nullable();
            $table->string('rent')->nullable();
            $table->string('arbitrator_or_expert')->nullable();
            $table->boolean('time_of_essence')->nullable();
            $table->boolean('interest')->nullable();
            $table->string('rent_review_pattern')->nullable();
            $table->string('notice_by')->nullable();
            $table->string('notice_period')->nullable();
            $table->string('notice_amount')->nullable();
            $table->boolean('turnover_rent')->nullable();
            $table->dateTime('notice_dates')->nullable();
            $table->string('no_floors')->nullable();
            $table->string('itza')->nullable();
            $table->string('floors_occupied')->nullable();
            $table->string('gross_area')->nullable();
            $table->string('net_area')->nullable();
            $table->integer('bank_id')->nullable()->index('bank_id');
            $table->string('base_rate')->nullable();
            $table->string('turnover_above')->nullable();
            $table->string('base_rent')->nullable();
            $table->string('turnover')->nullable();
            $table->binary('notes', 65535)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->integer('floor_areas_confirmed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rent_reviews');
    }
}
