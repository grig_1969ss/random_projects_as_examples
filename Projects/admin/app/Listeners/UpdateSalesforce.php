<?php

namespace App\Listeners;

use App\Services\SalesforceService;

class UpdateSalesforce
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\NewCasePayment',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\NewAuxiliaryCost',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\EditCase',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\UpdateCaseStatus',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\NewIssueTerms',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\NewLegalNotice',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\NewPrintLetter',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );

        $events->listen(
            'App\Events\EditClient',
            'App\Listeners\UpdateSalesforce@updateSalesforce'
        );
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function updateSalesforce($event)
    {
        $salesforce = new SalesforceService();
        $clientId = null;
        $caseId = null;

        if (isset($event->case)) {
            $clientId = $event->case->client->id;
            $caseId = $event->case->id;
        } elseif (isset($event->client)) {
            $clientId = $event->client->id;
        }

        $salesforce->pushToSalesforce($caseId, $clientId);
    }
}
