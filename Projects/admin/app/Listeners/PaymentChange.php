<?php

namespace App\Listeners;

class PaymentChange
{
    /**
     * Subscribe multiple events to this listener.
     *
     * @param $events
     *
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\UpdateCaseStatus',
            'App\Listeners\PaymentChange@updateStatus'
        );
    }

    /**
     * Handle the event if it's a new case payment.
     *
     * @param $event
     *
     * @return void
     */
    public function updateStatus($event)
    {
        $case = $event->case;

        // if (optional($case->status)->closed) {
        //     $case->closed_date = date('Y-m-d H:i:s');
        //     $case->update();
        // }
    }
}
