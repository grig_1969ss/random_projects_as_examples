<?php

namespace App\Listeners;

use App\Models\CaseCall;

class CreateActivity
{
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\NewLegalNotice',
            'App\Listeners\CreateActivity@onLegalNoticeEmailSent'
        );

        $events->listen(
            'App\Events\NewIssueTerms',
            'App\Listeners\CreateActivity@onTermsIssued'
        );

        $events->listen(
            'App\Events\CasePayment',
            'App\Listeners\CreateActivity@onPaymentCreated'
        );

        $events->listen(
            'App\Events\LegalActionSent',
            'App\Listeners\CreateActivity@onLegalActionSent'
        );

        $events->listen(
            'App\Events\AuxiliaryActionSent',
            'App\Listeners\CreateActivity@onAuxiliaryActionSent'
        );

        $events->listen(
            'App\Events\NewInboundEmail',
            'App\Listeners\CreateActivity@onNewInboundEmail'
        );

        $events->listen(
            'App\Events\NewOutboundEmail',
            'App\Listeners\CreateActivity@onNewOutboundEmail'
        );
    }

    public function onLegalNoticeEmailSent($event)
    {
        CaseCall::create([
            'call_type_id' => 16,
            'note'         => 'Legal notice email sent.',
            'case_id'      => $event->case->id,
        ]);
    }

    public function onTermsIssued($event)
    {
        CaseCall::create([
            'call_type_id' => 17,
            'note'         => 'Terms issued.',
            'case_id'      => $event->case->id,
        ]);
    }

    public function onPaymentCreated($event)
    {
        CaseCall::create([
            'call_type_id' => 18,
            'note'         => 'Payment created.',
            'case_id'      => $event->case->id,
        ]);
    }

    public function onLegalActionSent($event)
    {
        CaseCall::create([
            'call_type_id' => 17,
            'note'         => "Legal action {$event->type} sent.",
            'case_id'      => $event->case->id,
        ]);
    }

    public function onAuxiliaryActionSent($event)
    {
        CaseCall::create([
            'call_type_id' => 17,
            'note'         => 'Auxiliary action sent.',
            'case_id'      => $event->case->id,
        ]);
    }

    public function onNewInboundEmail($event)
    {
        $email = $event->email;
        $url = route('email.detail', $email);

        CaseCall::create([
            'call_type_id' => 20,
            'note'         => "<a href=\"{$url}\">{$email->subject}</a>",
            'case_id'      => $email->tag,
        ]);
    }

    public function onNewOutboundEmail($event)
    {
        $email = $event->email;
        $url = route('email.detail', $email);

        CaseCall::create([
            'call_type_id' => 21,
            'note'         => "<a href=\"{$url}\">{$email->subject}</a>",
            'case_id'      => $email->tag,
        ]);
    }
}
