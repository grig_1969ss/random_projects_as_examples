<?php

namespace App\Listeners;

use App\Models\Insurance;
use App\Models\ServiceCharge;

class DocumentUploadListener
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->entity instanceof Insurance) {
            $event->entity->update(['cover_note_received' => 1]);
        }

        if ($event->entity instanceof ServiceCharge) {
            if ($event->request->document_type_id == 41) {
                $event->entity->update(['budget_details_recd' => 1]);
            } elseif ($event->request->document_type_id == 42) {
                $event->entity->update(['annual_reconciliation_recd' => 1]);
            }
        }
    }
}
