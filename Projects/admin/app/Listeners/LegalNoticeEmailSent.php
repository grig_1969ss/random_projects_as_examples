<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Facades\Storage;

class LegalNoticeEmailSent
{
    public function __construct()
    {
        //
    }

    public function handle(MessageSent $event)
    {
        if (isset($event->message->pdfPath)) {
            $pdfPath = $event->message->pdfPath;

            Storage::delete($pdfPath);
        }
    }
}
