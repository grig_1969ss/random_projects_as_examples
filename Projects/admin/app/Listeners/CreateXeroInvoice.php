<?php

namespace App\Listeners;

use App\Events\CaseCreated;
use App\Helpers\PaymentHelper;
use App\Helpers\XeroHelper;

class CreateXeroInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\NewCasePayment',
            'App\Listeners\CreateXeroInvoice@handle'
        );

        $events->listen(
            'App\Events\NewAuxiliaryCost',
            'App\Listeners\CreateXeroInvoice@handle'
        );
    }

    /**
     * Handle the event.
     *
     * @param CaseCreated $event
     *
     * @return void
     */
    public function handle($event)
    {
        if ($event->isAux) {
            $isAux = true;
            $commission = $event->payment->amount;
            $isMinCharge = false;
        } else {
            $isAux = false;
            $paymentHelper = new PaymentHelper();
            $commission = $paymentHelper->payment($event->payment);
            $dqFee = $event->case->dqFeeTotal;
            $minCharge = $event->case->getCaseMinChargeAttribute(false);
            $isMinCharge = $dqFee <= $minCharge && $commission <= $minCharge;
        }

        $xeroHelper = new XeroHelper($event->case, $event->payment, $isAux, $isMinCharge, $commission);

        if ((
                ! $isAux
                && $event->payment->description == 'Principal'
                && $commission > 0
            )
            || $isAux) {
            $xeroHelper->buildInvoice();
        }
    }
}
