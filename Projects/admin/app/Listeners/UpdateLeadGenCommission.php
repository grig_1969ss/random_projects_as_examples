<?php

namespace App\Listeners;

use App\Events\NewCasePayment;

class UpdateLeadGenCommission
{
    public function handle(NewCasePayment $event)
    {
        $case = $event->case;
        $payment = $event->payment;
        $check1 = ($payment->description == 'Principal');

        $check2 = ($case->casePayments()
                ->where('description', 'Principal')
                ->where('id', '<>', $payment->id)
                ->count() == 0);

        $check3 = ($case->lead_gen_commission);

        $check4 = ($case->client
                ->cases()
                ->whereDate('created_at', $case->created_at->format('Y-m-d'))
                ->where('id', '<>', $case->id)
                ->whereHas('casePayments')
                ->count() == 0);

        if ($check1 && $check2 && $check3 && $check4) {
            $case->update([
                'lead_gen_commission_due'      => true,
                'lead_gen_commission_due_date' => $payment->date,
            ]);
        }
    }
}
