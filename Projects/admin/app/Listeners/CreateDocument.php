<?php

namespace App\Listeners;

use App\Models\CaseCall;
use App\Services\PDFService;
use Illuminate\Support\Facades\Storage;

class CreateDocument
{
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\NewIssueTerms',
            'App\Listeners\CreateDocument@onTermsIssued'
        );
    }

    public function onTermsIssued($event)
    {
        $case = $event->case;
        $data = $event->data;
        $data['name'] = $data['user']->name;

        $view = ($case->client->agreedTerms == 0)
            ? 'admin.cases.issueTermsMail.new_client_email'
            : 'admin.cases.issueTermsMail.existing_client_email';

        $service = new PDFService();
        $result = $service->storePDF($view, $data, 'Issue_Terms_Email_'.$case->id, 'issue_terms', []);

        $document = $case->documents()->create([
            'name'             => $result[1],
            'file_name'        => $result[0],
            'mime_type'        => 'application/pdf',
            'file_size'        => Storage::size($result[0]),
            'document_type_id' => 4,
        ]);

        $documentUrl = route('documents.show', $document);

        CaseCall::create([
            'call_type_id' => 19,
            'note'         => "<a href=\"{$documentUrl}\">{$result[1]}</a> sent to {$data['user']->email}",
            'case_id'      => $event->case->id,
        ]);
    }
}
