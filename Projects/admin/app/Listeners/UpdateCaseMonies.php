<?php

namespace App\Listeners;

use App\Helpers\CaseMoneyCalculationsHelper;
use App\Models\CaseMoney;

class UpdateCaseMonies
{
    /**
     * Subscribe multiple events to this listener.
     *
     * @param $events
     *
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\NewCasePayment',
            'App\Listeners\UpdateCaseMonies@addCaseMoney'
        );

        $events->listen(
            'App\Events\NewCaseInvoice',
            'App\Listeners\UpdateCaseMonies@updateCaseMonies'
        );

        $events->listen(
            'App\Events\CasePaymentUpdated',
            'App\Listeners\UpdateCaseMonies@updateCaseMonies'
        );
    }

    /**
     * Handle the event if it's a new case payment.
     *
     * @param $event
     *
     * @return void
     */
    public function addCaseMoney($event)
    {
        // if ($event->case->casePayments()->count() == 0 || $event->case->casePayments()->where('description', 'Principal')->count() == 1) {
        //     $firstPayment = true;
        // } else {
        //     $firstPayment = false;
        // }
        //
        // $this->storeCaseMoney($event->case, $firstPayment, $event->payment);
    }

    /**
     * Handle the event if all case money entries for a case need updated, e.g. when there's a new case invoice.
     *
     * @param $event
     *
     * @return void
     */
    public function updateCaseMonies($event)
    {
        //$event->case->caseMonies()->delete();
        //CaseMoney::createFromCase($event->case);
        //$casePayments = $event->case->casePayments()->orderBy('id', 'asc')->get();

        // $latestCaseMoney = CaseMoney::where('case_id', $event->case->id)->whereNotNull('case_payment_id')->latest()->orderBy('id', 'desc')->first();
        //
        // if ($latestCaseMoney) {
        //     $this->storeCaseMoney($latestCaseMoney->case, false, $event->case->casePayments()->latest()->orderBy('id', 'desc')->first());
        // }

        /*foreach ($casePayments as $payment) {
            if ($counter == 0 && $payment->description == 'Principal') {
                $firstPayment = true;
            } else {
                $firstPayment = false;
            }

            $this->storeCaseMoney($event->case, $firstPayment, $payment, true);

            $counter++;
        }*/
    }

    /**
     * Store the new case money.
     *
     * @param $event
     * @param bool $firstPayment
     * @param $payment
     *
     * @return void
     */
    public function storeCaseMoney($case, $firstPayment, $payment)
    {
        // $caseMoneyCalculationsHelper = new CaseMoneyCalculationsHelper($case, $firstPayment, $payment);
        //
        // CaseMoney::create($caseMoneyCalculationsHelper->calculateMonetaryValues());
    }
}
