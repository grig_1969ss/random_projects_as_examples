<?php

namespace App\Listeners;

use App\Models\AccountType;

class ChangeClientAccountType
{
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\NewCasePayment',
            'App\Listeners\ChangeClientAccountType@onNewCasePayment'
        );

        $events->listen(
            'App\Events\CaseClosed',
            'App\Listeners\ChangeClientAccountType@onCaseClosed'
        );
    }

    public function onNewCasePayment($event)
    {
        $case = $event->case;

        if ($client = $case->client) {
            if (in_array($client->account_type_id, $this->getAccountTypesNewPayment())) {
                $client->update([
                    'account_type_id' => AccountType::EXISTING_ACTIVE,
                ]);
            }
        }
    }

    public function onCaseClosed($event)
    {
        $case = $event->case;

        if ($case->casePayments()->count() == 0 && $client = $case->client) {
            if (in_array($client->account_type_id, $this->getAccountTypesCaseClosed())) {
                $client->update([
                    'account_type_id' => AccountType::NEW_NOT_PAID_OUT,
                ]);
            }
        }
    }

    protected function getAccountTypesNewPayment()
    {
        return $accountTypes = [
            AccountType::NEW_OUTBOUND,
            AccountType::NEW_INBOUND,
            AccountType::NEW_DIVISION_OF_GROUP,
            AccountType::EXISTING_LOST,
            AccountType::NEW_NOT_PAID_OUT,
        ];
    }

    protected function getAccountTypesCaseClosed()
    {
        return $accountTypes = [
            AccountType::NEW_OUTBOUND,
            AccountType::NEW_INBOUND,
            AccountType::NEW_DIVISION_OF_GROUP,
        ];
    }
}
