<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DocumentUpload
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $entity;
    public $request;

    public function __construct($entity, $request)
    {
        $this->entity = $entity;
        $this->request = $request;
    }
}
