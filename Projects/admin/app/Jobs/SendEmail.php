<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $email;
    public $emailId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $emailId)
    {
        $this->email = $email;
        $this->emailId = $emailId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this->email['Attachments']);

        Mail::send([], [], function ($message) {
            $message->from($this->email['From']);

            if (isset($this->email['ReplyTo'])) {
                $message->replyTo($this->email['ReplyTo']);
            }

            $message->to($this->email['To']);

            if (! empty($this->email['Cc'])) {
                $message->cc($this->email['Cc']);
            }

            $message->subject($this->email['Subject']);
            $message->setBody($this->email['HtmlBody'], 'text/html');

            foreach ($this->email['Attachments']['files'] as $attachment) {
                $message->attachData(
                    base64_decode($attachment['Content']),
                    $attachment['Name'],
                    ['mime' => $attachment['ContentType']]
                );
            }

            foreach ($this->email['Attachments']['base_64'] as $attachment) {
                $message->attachData(
                    base64_decode($attachment['content']),
                    $attachment['name'],
                    ['mime' => $attachment['content_type']]
                );
            }

            if (isset($this->email['Attachments']['attached_pdf'])) {
                $message->attach(storage_path('app/emails_as_pdf/'.$this->email['Attachments']['attached_pdf']));
            }

            $swiftMessage = $message->getSwiftMessage();

            $headers = $swiftMessage->getHeaders();
            $headers->addTextHeader('X-PM-Tag', $this->emailId);
        });

        if (isset($this->email['Attachments']['attached_pdf'])) {
            $path = 'emails_as_pdf/'.$this->email['Attachments']['attached_pdf'];
            Storage::delete($path);
        }
    }
}
