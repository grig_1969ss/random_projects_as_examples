<?php

namespace App\Jobs;

use App\Models\Email;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ProcessInbound implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $inbound;
    protected $options;

    public function __construct($inbound, $options)
    {
        $this->inbound = $inbound;
        $this->options = $options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->storeEmail();

        if ($this->inbound->HasAttachments()) {
            $this->storeAttachments($email);
        }
    }

    private function storeEmail()
    {
        $email = Email::create([
            'from'                => $this->getFromEmail(),
            'from_name'           => $this->getFromName(),
            'subject'             => $this->getSubject(),
            'message_id'          => $this->inbound->MessageID(),
            'date'                => $this->getDate(),
            'text_body'           => $this->getBody(),
            'html_body'           => $this->inbound->HtmlBody(),
            'stripped_text_reply' => $this->inbound->StrippedTextReply(),
        ]);

        return $email;
    }

    private function getFromEmail()
    {
        if ($this->options['forwarded'] === false) {
            return $this->inbound->FromEmail();
        }

        $matches = $this->matchEmail();

        if ($matches) {
            return $matches[1];
        }

        return $this->inbound->FromEmail();
    }

    private function getFromName()
    {
        if ($this->options['forwarded'] === false) {
            return $this->inbound->FromName();
        }

        if (preg_match('/From: "(.*?)"/', $this->inbound->TextBody(), $name)) {
            return implode(' ', array_reverse(explode(', ', $name[1])));
        }

        return $this->inbound->FromName();
    }

    private function getDate()
    {
        if ($this->options['forwarded'] === false) {
            return Carbon::now();
        }

        if (preg_match('/Date: (.*?) at (\d+:\d+:\d+) (.*)\n/', $this->inbound->TextBody(), $date)) {
            $year = $date['1'];
            $time = $date['2'];

            return Carbon::createFromFormat('d F Y H:i:s', $year.' '.$time);
        }

        return Carbon::now();
    }

    private function getSubject()
    {
        if ($this->options['forwarded'] === true) {
            return str_replace('Fwd: ', '', $this->inbound->Subject());
        }

        return $this->inbound->Subject();
    }

    private function getBody()
    {
        $matches = $this->matchEmail();

        if ($matches) {
            $email = '('.$matches[1].')';

            return preg_replace('/(<.*?>)/', $email, $this->inbound->TextBody());
        }

        return $this->inbound->TextBody();
    }

    private function matchEmail()
    {
        preg_match('/<(.*?)>/', $this->inbound->TextBody(), $email);

        return $email ?: false;
    }

    private function storeAttachments(Email $email)
    {
        foreach ($this->inbound->Attachments() as $attachment) {
            $fileName = str_replace(' ', '_', time().'_'.$attachment->Name);
            $path = 'email_attachments/'.$fileName;

            Storage::put($path, base64_decode($attachment->Content));

            $email->documents()->create([
                'name' => substr($attachment->Name, 0, strrpos($attachment->Name, '.')),
                'file_path' => $path,
                'mime_type' => $attachment->ContentType,
                'file_size' => $attachment->ContentLength,
            ]);
        }
    }
}
