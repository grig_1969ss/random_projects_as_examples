<?php

namespace App\Jobs;

use App\Models\Role;
use App\Repositories\UserRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RegisterUser extends Job
{
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $password;
    protected $role;

    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct($firstName, $lastName, $email, $password, $role = 'web')
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
    }

    /**
     * @param UserRepository $userRepository
     * @return mixed
     */
    public function handle(UserRepository $userRepository)
    {
        $this->validate();

        return DB::transaction(function () use ($userRepository) {
            $user = $userRepository->create([
                'name' => $this->firstName.' '.$this->lastName,
                'email' => $this->email,
                'password' => bcrypt($this->password),
            ]);

            $this->attachRole($user);

            return $user;
        });
    }

    protected function attachRole($user)
    {
        $role = Role::whereName($this->role)->firstOrFail();

        return $user->attachRole($role);
    }

    private function validate()
    {
        $validator = Validator::make(
            [
                'email' => $this->email,
            ],
            [
                'email' => 'required|email|unique:users',
            ]);

        if ($validator->fails()) {
            exit($validator->errors()->first()."\n");
        }

        return true;
    }
}
