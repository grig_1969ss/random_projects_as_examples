<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

trait CreateAndUpdateTrait
{
    public static function bootCreateAndUpdateTrait()
    {
        static::creating(function ($model) {
            if (Auth::check()) {
                $model->created_by = Auth::user()->id;
                $model->updated_by = Auth::user()->id;
            }
        });
        static::updating(function ($model) {
            if (Auth::check()) {
                $model->updated_by = Auth::user()->id;
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    // from model name, get columns and values for columns, then show in table
    public function modelColumnRows($requestModel, $id = null)
    {
        $currentModel = $requestModel->m ?? $this->defaultType;

        $model = classObject($currentModel);
        $columnsInTable = Schema::getColumnListing($model->table);

        $columns = $this->filterColumns($columnsInTable);

        $rows = $model::withTrashed()->cursor();

        // filter by id if isset
        if ($id) {
            $rows = $model::withTrashed()->cursor()->where('id', $id);
        }

        return [
            'columns' => $columns,
            'rows' => $rows,
            'currentModel' => $currentModel,
        ];
    }

    // delete column from view table, keep in fillable for assignment
    private function filterColumns($columns)
    {
        $columnsToDelete = ['id', 'password', 'remember_token'];

        foreach ($columnsToDelete as $column) {
            if (($key = array_search($column, $columns)) !== false) {
                unset($columns[$key]);
            }
        }

        return $columns;
    }

    // call dynamic repository
    public function modelRepository($model)
    {
        $repository = 'App\Repositories\\'.$model.'Repository';

        $this->dynamicRepository = app()->make($repository);
    }

    // call dynamic request
    public function modelRequest($model)
    {
        $model = $model ?? $this->defaultType;

        $request = 'App\Http\Requests\\'.$model.'Request';

        return app()->make($request);
    }

    // requests are dynamic, so get rules dynamically
    public function validateRequest($request)
    {
        $requestArray = $request->all();

        $dynamicRequest = $this->modelRequest($request['m']);

        $rules = $dynamicRequest->rules();

        // e.g. email rule is different
        $rules = $this->specificRules($request, $rules);

        Validator::make($requestArray,
            $rules
        );

        return $requestArray;
    }

    // some rules can be different from the rest
    private function specificRules($request, $rules)
    {
        if ($request['m'] == 'User') {
            $rules['email'] = 'required|email|unique:users'.',id,'.$request['id'];
        }

        return $rules;
    }

    // some fields can not be just saved from request, we need to modify
    public function specificFields($request)
    {
        if ($request['m'] == 'User') {
            $request['password'] = $this->generatePassword();

            if (isset($request['avatar'])) {
                $path = $request['avatar']->getRealPath();
                $logo = file_get_contents($path);
                $base64 = 'data:image/png;base64,'.base64_encode($logo);

                $request['avatar'] = $base64;
            }
        }

        return $request;
    }

    // belongs to many
    public function attachRelations($request, $model)
    {
        if ($request['m'] == 'User') {
            $model->clients()->sync($request['client_id']);
        }
    }

    // when we create user, we need password to be generated
    private function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return bcrypt($result);
    }
}
