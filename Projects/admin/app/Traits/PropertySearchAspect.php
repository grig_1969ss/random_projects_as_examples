<?php

namespace App\Traits;

use App\Models\Property;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Spatie\Searchable\SearchAspect;

class PropertySearchAspect extends SearchAspect
{
    /*
     * This method only for Property. Used for custom where statement
    */
    public function getResults(string $term): Collection
    {
        return Property::where('property_name', 'LIKE', '%'.$term.'%')
            ->where('client_id', auth()->user()->client_id)
            ->get();
    }
}
