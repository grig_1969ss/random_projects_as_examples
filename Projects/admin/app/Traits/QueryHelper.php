<?php

namespace App\Traits;

trait QueryHelper
{
    public function scopeFilterByDate($query, $dateField, $date = 'CURDATE()', $interval = 0, $intervalType = 'MONTH')
    {
        return $query->whereRaw("DATE_ADD({$date},INTERVAL {$interval} {$intervalType}) >= {$dateField}");
    }
}
