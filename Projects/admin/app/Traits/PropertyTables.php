<?php

namespace App\Traits;

use App\Helpers\Util;
use App\Models\ServiceCharge;
use Carbon\Carbon;

trait PropertyTables
{
    public function getServiceChargesAsTable($serviceCharges)
    {
        if (empty($serviceCharges)) {
            return;
        }

        $yearRow = $yearEndRow = $methodOfApportionmentRow = $apportionmentRow = $annualBudgetRow =
        $quarterlyPaymentRow = $serviceChargeReconciliationRow = $totalCost = $budgetUplift =
        $budgetDetailsRecdRow = $franchiseeUpdatedRow = $realEstateUpdatedRow = $annualReconciliationRecdRow =
        $annualFranchiseeUpdatedRow = $annualRealEstateUpdatedRow = $yearsExists = $policy = $diary = [];

        $yearRow[] = 'Year';
        $yearEndRow[] = 'Year end';
        $methodOfApportionmentRow[] = 'Method of Apportionment';
        $apportionmentRow[] = 'Apportionment';
        $annualBudgetRow[] = 'Annual Budget';
        $quarterlyPaymentRow[] = 'Quarterly Payment';
        $serviceChargeReconciliationRow[] = 'Service Charge Reconciliation';
        $totalCost[] = 'Total Cost';
        $budgetUplift[] = 'Budget Uplift';
        $budgetDetailsRecdRow[] = 'Budget Details Recd';
        $annualReconciliationRecdRow[] = 'Annual Reconciliation Recd';
        $diary[] = 'Diary';
        $policy[] = 'Documents';

        $i = 0;
        foreach ($serviceCharges as $key => $charge) {
            $yearRow[] = $charge->present()->year;
            $yearEndRow[] = Carbon::parse($charge->year_end)->format('d/m/Y');
            $methodOfApportionmentRow[] = $charge->apportionmentType ? Util::showValue($charge->apportionmentType->name) : '-';
            $apportionmentRow[] = Util::showValue($charge->percentage_apportion);
            $annualBudgetRow[] = $charge->present()->formatNumber($charge->annual_budget);
            $quarterlyPaymentRow[] = $charge->present()->formatNumber($charge->budget_quarterly_payment);
            $serviceChargeReconciliationRow[] = $charge->present()->formatNumber($charge->balance_service_charge);
            $totalCost[] = $charge->present()->totalCost;
            $budgetUplift[] = $this->calculateBudgetValue($charge->year_end, $charge->property_id);

            $budgetDetailsRecdRow[] = [
                'Budget Details Recd' => $charge->present()->formatBooleanShort($charge->budget_details_recd),
                'Franchisee Updated' => $charge->present()->formatBooleanShort($charge->franchisee_updated),
                'Real Estate Updated' => $charge->present()->formatBooleanShort($charge->real_estate_updated),
            ];
            $annualReconciliationRecdRow[] = [
                'Annual Reconciliation Recd' => $charge->present()->formatBooleanShort($charge->annual_reconciliation_recd),
                'Franchisee Updated' => $charge->present()->formatBooleanShort($charge->annual_franchisee_updated),
                // 'Real Estate Updated' => $charge->present()->formatBooleanShort($charge->annual_real_estate_updated),
            ];

            $yearsExists[] = Carbon::parse($charge->year_end)->format('Y');

            $diary[] = $charge->present()->dateDiary($charge->diary) ?? 'Overdue';

            $policy[] = $charge->present()->policy() ?? '';
            $i++;
        }

        // Quarterly Payment ?  Apportionment ? Service Charge Reconciliation? Total Cost?

        return compact('yearRow', 'yearEndRow', 'methodOfApportionmentRow', 'apportionmentRow',
            'annualBudgetRow', 'quarterlyPaymentRow', 'serviceChargeReconciliationRow', 'totalCost', 'budgetUplift',
            'budgetDetailsRecdRow', 'annualReconciliationRecdRow', 'yearsExists', 'diary', 'policy');
    }

    public function generatePayments($payments)
    {
        if ($payments->isEmpty()) {
            return;
        }

        $headers = $rows = [];

        $headers = [
            '#', 'Period', 'Description', 'Supplier', 'Type', 'Invoice Date', 'Approved Date',
            'VAT Amount', 'Gross Amount', 'Hold?', 'Notes', 'Year',
        ];

        foreach ($payments as $key => $payment) {
            $supplierRoute = '#'; // checked
            if (! is_null($payment->contact)) {
                // check relation here as well, because `route` throws an issue even if this if statement is not reached
                $supplierRoute = route('suppliers.show', $payment->supplier ?? false);
            }

            $rows[] = [
                $payment->id,
                $payment->present()->period,
                $payment->description ?? '-',
                '<a href="'.$supplierRoute.'">'.$payment->present()->supplier.'</a>',
                $payment->present()->paymentType,
                $payment->present()->paymentDate ?? '-',
                $payment->present()->dateApproved,
                $payment->present()->taxAmount,
                $payment->present()->grossInvoiceAmount,
                $payment->present()->hold,
                $payment->note,
                Carbon::parse($payment->payment_date)->format('Y'),
                $payment->present()->policy(),
            ];
        }

        return compact('headers', 'rows');
    }

    /**
     * ( Annual Budget (Current Year) - Annual Budget (Previous Year) ) / Annual Budget (Previous Year)
     * So if a is `Annual Budget (Current Year)` and b is `Annual Budget (Previous Year)` then (a-b)/b
     * If there is no previous year, then 'N/A' would be the value.
     */
    private function calculateBudgetValue($currentYear, $property_id)
    {
        $currentYear = Carbon::parse($currentYear)->format('Y');
        $AnnualBudgetCurrentYear = ServiceCharge::whereRaw('year(`year_end`) = ?', $currentYear)->where('property_id', '=', $property_id)->first();
        $AnnualBudgetPreviousYear = ServiceCharge::whereRaw('year(`year_end`) = ?', [date('Y', strtotime("$AnnualBudgetCurrentYear->year_end -1 year"))])->where('property_id', '=', $property_id)->first();
        //TODO: check if not 0, do what?
        if ($AnnualBudgetPreviousYear && $AnnualBudgetPreviousYear->annual_budget) {
            // show in percentage, not as number
            $number = (
                    ($AnnualBudgetCurrentYear->annual_budget - $AnnualBudgetPreviousYear->annual_budget)
                    / $AnnualBudgetPreviousYear->annual_budget)
                * 100;

            return number_format($number, 2).'%';
        }

        return 'N/A';
    }

    public function generateInsurances($property, $returnInsurances = false)
    {
        $insurances = $property->insurances;
        $insuranceArray = [];

        $yearRow = $coveredByRow = $insurancePremiumRow = $insurerRow = $lossOfRentRow =
        $terrorismCoveredRow = $terrorismPremiumRow = $periodOfInsuranceRow =
        $renewalDateRaw = $notesRow = $noteReceivedRow = $diaryRow = $yearsExists = $policy = [];

        $yearRow[] = 'Year';
        $coveredByRow[] = 'Covered By';
        $insurancePremiumRow[] = 'Insurance Premium';
        $insurerRow[] = 'Insurer';
        //  $annualPremium[] = 'Annual Premium';
        $lossOfRentRow[] = 'Loss of Rent';
        $terrorismCoveredRow[] = 'Terrorism Covered';
        $terrorismPremiumRow[] = 'Terrorism Premium';
        $periodOfInsuranceRow[] = 'Period of Insurance';
        $renewalDateRaw[] = 'Renewal Date'; //
        $noteReceivedRow[] = 'Schedule Received'; //
        $notesRow[] = 'Note';
        $diaryRow[] = 'Diary';
        $policy[] = 'Policy';

        foreach ($insurances as $key => $insurance) {

            // we have to show 4 reports in show screen, but there could be 2 2020 for example
            // so we need something like group by to get real 4 not repeated years for overview page
            // because this function is used for both
            if ($key >= self::REPORT_LIMIT) {
                $yearsExists[] = Carbon::parse($insurance->insurance_renewal_date)->format('Y');
                continue;
            }
            if ($returnInsurances) {
                $insuranceArray[] = $insurance;
                continue;
            }

            $yearRow[] = $insurance->present()->year;
            $coveredByRow[] = $insurance->present()->coveredBy;
            $insurancePremiumRow[] = $insurance->present()->insurancePremium;
            $insurerRow[] = $insurance->insurer ?? '-';
            //  $annualPremium[] = '-';
            $lossOfRentRow[] = $insurance->present()->lossOfRent;
            $terrorismCoveredRow[] = $insurance->present()->terrorismCover;
            $terrorismPremiumRow[] = $insurance->present()->terrorismPremium;
            $periodOfInsuranceRow[] = $insurance->period_of_insurance ?? '-';
            $renewalDateRaw[] = Carbon::parse($insurance->insurance_renewal_date)->format('d/m/Y') ?? '-';
            $noteReceivedRow[] = [
                'schedule' => $insurance->present()->coverNoteReceived,
                'franchisee_updated' => $insurance->franchisee_updated,
            ];
            $notesRow[] = $insurance->notes ?? '-';
            $diaryRow[] = $insurance->present()->dateDiary($insurance->diary) ?? 'Overdue';
            $yearsExists[] = Carbon::parse($insurance->insurance_renewal_date)->format('Y');

            $policy[] = $insurance->present()->policy;
        }

        // when we need only max 4 insurances and not table
        if ($returnInsurances) {
            return $insuranceArray;
        }

        return compact('yearRow', 'coveredByRow', 'insurancePremiumRow', 'insurerRow', 'lossOfRentRow', 'terrorismCoveredRow', 'terrorismPremiumRow', 'periodOfInsuranceRow', 'renewalDateRaw', 'noteReceivedRow', 'notesRow', 'diaryRow', 'yearsExists', 'policy');
    }

    public function generateUtilities($property)
    {
        $rows = [];

        $headers = ['Period', 'Utility Type', 'Invoice Number', 'Date From / To', 'Opening Read', 'Closing Read', 'Usage', 'Amount Net'];

        foreach ($property->utilities as $utility) {
            $rows[] = [
                $utility->id,
                $utility->period,
                $utility->payment->name ?? '-',
                $utility->invoice_number,
                $utility->date_from_to,
                $utility->opening_read,
                $utility->closing_read,
                $this->getUsage($utility),
                $this->getAmountNet($utility),
            ];
        }

        return compact('headers', 'rows');
    }

    private function getUsage($utility)
    {
        //‘Closing Read’ minus ‘Opening Read’
        return $utility->closing_read - $utility->opening_read;
    }

    private function getAmountNet($utility)
    {
        $symbol = auth()->user()->client->currency->symbol ?? '';

        return $symbol.number_format($utility->amount_net, 2);
    }
}
