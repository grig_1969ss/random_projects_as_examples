<?php

namespace App\Traits;

use App\Helpers\Date;
use App\Models\Diary;

trait UpdateDiary
{
    public function updateDiaryEntry($date, $entity, $type, $title)
    {
        $date = Date::toMysql($date);
        $deleteDiary = false;
        $diaryId = null;

        if ($date) {
            $diary = $entity->diary;

            if ($diary) {
                if ($diary->date != $date) {
                    $diary->update(compact('date'));
                }
            } else {
                $diary = Diary::create([
                    'property_id'   => $entity->property->id,
                    'diary_type_id' => $type,
                    'diary_title'   => $title,
                    'date'          => $date,
                    'status'        => 0,
                ]);
            }

            $diaryId = $diary->id;
        } else {
            $diary = $entity->diary;

            if ($diary) {
                $deleteDiary = true;
            }
        }

        return [$diaryId, $deleteDiary];
    }
}
