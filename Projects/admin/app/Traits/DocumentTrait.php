<?php

namespace App\Traits;

use App\Helpers\Util;
use App\Models\Document;

trait DocumentTrait
{
    public function documents()
    {
        return $this->morphMany(Document::class, 'docable')->where('archived', false)->orderByDesc('created_at');
    }

    public function addDocumentNote(Document $document, $comments = null, $objectType = 'properties')
    {
        Util::addNote(
            $this->property->id,
            $objectType,
            6,
            $comments ? $comments : "Document {$document->document_name} Uploaded to {$this->modelNameHuman} #{$this->id}"
        );
    }
}
