<?php

namespace App\Traits;

use App\Helpers\Date;
use App\Models\ApportionmentType;
use App\Models\AttachmentDetails;
use App\Models\Email;
use App\Models\ServiceCharge;
use App\Models\ServiceChargeStatus;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

trait HelperTrait
{
    public function unreadCount($type)
    {
        return [
            'inbox' => Email::getUnreadInboxCount($type),
            'draft' => Email::getUnreadDraftCount($type),
            'archived' => Email::getUnreadArchivedCount($type),
            'trash' => Email::getUnreadTrashCount($type),
        ];
    }

    public function serviceChargeCreate()
    {
        $serviceCharge = new ServiceCharge();
        $apportionmentTypesDropdown = (new ApportionmentType())->toDropdown();
        $serviceChargeStatusesDropdown = (new ServiceChargeStatus())->toDropdown();

        return compact('serviceCharge', 'apportionmentTypesDropdown', 'serviceChargeStatusesDropdown');
    }

    public function attachToDropdown($property, $addNew = false)
    {
        if (! $property) {
            return false;
        }
        $hideApprovals = Auth::user()->client->hide_approvals;
        $serviceChargeObj = new ServiceCharge();
        $charges = $serviceChargeObj->getAllServiceChargesByPropertyId($property->id, false) ?? [];
        $insurances = $property->insurances ?? [];
        $payments = ! $hideApprovals && $property->payment ? $property->payment : [];
        $dropdown = [
            'properties' => ['items' => [], 'value' => false, 'label' => 'Property', 'url' => '/properties/'],
            'service-charges' => ['items' => [], 'value' => false, 'label' => 'ServiceCharge', 'url' => '/service-charges/'],
            'insurance' => ['items' => [], 'value' => 10, 'label' => 'Insurance', 'url' => '/insurances/'],
            'approvals' => ['items' => [], 'value' => false, 'label' => 'Payment', 'url' => '/payments/'],
        ];

        $dropdown['properties']['items'] = [
            'id' => $property->id,
            'value' => 'Property (#'.str_pad($property->reference, 4, '0', STR_PAD_LEFT).' '.$property->property_name.')',
        ];

        if ($addNew) {
            // add new options
            $dropdown['service-charges']['items'][] = ['id' => 'service charge', 'value' => 'Add new service charge'];
            $dropdown['insurance']['items'][] = ['id' => 'insurance', 'value' => 'Add new insurance'];
            $dropdown['approvals']['items'][] = ['id' => 'approval', 'value' => 'Add new approval'];
//            $dropdown = [
//                'approvals' => ['items' => [], 'value' => false, 'label' => 'Approval', 'url' => '/payments/'],
//            ];
        }

        foreach ($charges as $charge) {
            $dropdown['service-charges']['items'][] = [
                'id' => $charge->id,
                'value' => Date::Y($charge->year_end),
            ];
        }

        foreach ($insurances as $insurance) {
            $dropdown['insurance']['items'][] = [
                'id' => $insurance->id,
                'value' => Date::Y($insurance->insurance_renewal_date),
            ];
        }

        foreach ($payments as $payment) {
            $type = $payment->paymentType ? ' - '.$payment->paymentType->name : '';
            $dropdown['approvals']['items'][] = [
                'id' => $payment->id,
                'value' => $payment->period.$type,
            ];
        }
        if ($hideApprovals) {
            Arr::forget($dropdown, 'approvals');
        }

        return $dropdown;
    }

    /**
     * @param $property
     * @param $selected
     * @param $modalSave
     * @param $data
     * @return bool
     */
    public function selectData($property, $selected, $modalSave, &$data, $model)
    {
        if (! $modalSave) {
            return false;
        }

        $addNew = true;
        $documentDropdown = $this->attachToDropdown($property, true);
        $select = view('documents.includes.attachToDropdown', compact('documentDropdown', 'addNew'))->toHtml();

        if ($modalSave) {
            $data['model'] = [
                'unique' => preg_replace('/[^\w]/', '_', $modalSave),
                'attachTo' => json_encode($select),
                'selected' => $selected,
                'modelClass' => $model,
            ];
        }
    }

    public function replaceCidToBase64($htmlBody, $email)
    {
        preg_match_all('/src="cid:(.*?)"/', $htmlBody, $names);

        $attachments = AttachmentDetails::select('content', 'content_type', 'contentid')
            ->where('email_id', $email->id)
            ->where('deleted_at', null)
            ->where(function ($query) {
                $query->where('contentid', '!=', '')
                    ->orWhereNotNull('contentid');
            })
            ->get();

        foreach ($names[1] as $contentid) {
            $attachment = $attachments->firstWhere('contentid', $contentid);

            if (empty($attachment)) {
                $htmlBody = str_replace('src="cid:'.$contentid.'"', '', $htmlBody);
                continue;
            }

            $src = 'src="'.'data:'.$attachment->content_type.';base64, '.$attachment->content.'"';

            $newSrc = 'src="cid:'.$contentid.'"';

            $htmlBody = str_replace($newSrc, $src, $htmlBody);
        }

        return $htmlBody;
    }

    public function cleanBody($html)
    {
        $html = str_replace('<script>', '[scriptblocked]', $html);
        $html = str_replace('</script>', '[/scriptblocked]', $html);

        $html = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', '', $html);
        // $html = nl2br(preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', '', $html));

        return $html;
    }
}
