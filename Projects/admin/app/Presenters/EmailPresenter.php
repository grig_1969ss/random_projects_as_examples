<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class EmailPresenter extends Presenter
{
    public function date()
    {
        if ($date = $this->entity->date) {
            if ($date->isToday()) {
                return $date->format('h:i A');
            }

            return $date->format('d/m/Y');
        }
    }

    public function dateAlt()
    {
        if ($date = $this->entity->date) {
            return $date->format('d/m/Y H:i');
        }
    }
}
