<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class DocumentPresenter extends Presenter
{
    public function fileSize()
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $bytes = max($this->entity->file_size, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);

        return round($bytes, 2).' '.$units[$pow];
    }
}
