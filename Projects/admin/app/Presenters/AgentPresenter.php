<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class AgentPresenter extends Presenter
{
    public function agentName()
    {
        if ($this->entity->agent_name) {
            return $this->entity->agent_name;
        }

        return 'No Name Given';
    }

    public function telephone()
    {
        if ($this->entity->telephone) {
            return $this->entity->telephone;
        }

        return 'No Telephone Given';
    }

    public function email()
    {
        if ($this->entity->email) {
            return $this->entity->email;
        }

        return 'No Email Given';
    }
}
