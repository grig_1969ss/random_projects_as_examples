<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class SupplierPresenter extends Presenter
{
    public function name()
    {
        if ($this->entity->name) {
            return $this->entity->name;
        }

        return 'No Name Given';
    }

    public function telephone()
    {
        if ($this->entity->telephone) {
            return $this->entity->telephone;
        }

        return 'No Telephone Given';
    }

    public function email()
    {
        if ($this->entity->email) {
            return $this->entity->email;
        }

        return 'No Email Given';
    }
}
