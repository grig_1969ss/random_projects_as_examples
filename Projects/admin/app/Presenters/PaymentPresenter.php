<?php

namespace App\Presenters;

class PaymentPresenter extends PropertyRelatedPresenter
{
    public function paymentDate($returnIfNull = '-')
    {
        return $this->formatDate('payment_date', 'Y-m-d H:i:s', $returnIfNull);
    }

    public function dateApproved($returnIfNull = '-')
    {
        return $this->formatCarbonDate('date_approved', $returnIfNull);
    }

    public function grossInvoiceAmount()
    {
        return $this->formatNumber($this->entity->gross_invoice_amount);
    }

    public function taxAmount()
    {
        return $this->formatNumber($this->entity->tax_amount);
    }

    public function serviceCharge()
    {
        if ($this->entity->serviceCharge) {
            return $this->entity->serviceCharge->present()->yearEnd;
        }

        return '-';
    }

    public function supplier()
    {
        if ($this->entity->supplier) {
            return $this->entity->supplier->name;
        }

        return '-';
    }

    public function paymentType()
    {
        if ($this->entity->paymentType) {
            return $this->entity->paymentType->name;
        }

        return '-';
    }

    public function hold()
    {
        return $this->formatBoolean($this->entity->hold);
    }

    public function contact()
    {
        $contact = $this->entity->contact;

        if ($contact) {
            return $contact->first_name.' '.$contact->surname;
        }

        return '-';
    }

    public function period()
    {
        if (! $this->entity->period) {
            return '-';
        }

        return '<span class="show-on-hover"><a href="'.route('payments.edit', $this->entity->id).'?property='.request('property')->id.'">'.$this->entity->period.' <i class="fa fa-edit" style="display: none"></i></a></span>';
    }

    public function policy($report = false, $onlyDocument = false)
    {
        $document = $this->document->where('archived', '!=', 1)->first();

        if ($onlyDocument) {
            return $document;
        }

        if ($document) {
            $deleteIcon = ! $report ? '<span class="fa fa-trash delete-policy-document"  data-document_id="'.$document->id.'" ></span>' : '';

            return '<a href="'.route('documents.show', $document->id).'" target="_blank">
                        <i class="fas fa-paperclip"></i>
                    </a> '.$deleteIcon;
        }

        return '';
    }
}
