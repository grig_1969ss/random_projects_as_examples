<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class ContactPresenter extends Presenter
{
    public function nameEmail()
    {
        $string = '';

        if ($this->entity->first_name) {
            $string .= $this->entity->first_name;

            if ($this->entity->surname) {
                $string .= ' '.$this->entity->first_name;
            }

            if ($this->entity->email) {
                $string .= " ({$this->entity->email})";
            }
        } else {
            $string = 'N/A';
        }

        return $string;
    }
}
