<?php

namespace App\Presenters;

use Carbon\Carbon;

class InsurancePresenter extends PropertyRelatedPresenter
{
    public function renewalDate($default = '-')
    {
        $date = $this->entity->insurance_renewal_date;

        if ($date) {
            return $date->format('d/m/Y');
        }

        return $default;
    }

    public function coveredBy()
    {
        $coveredBy = $this->entity->coveredBy;

        if ($coveredBy) {
            return $coveredBy->name;
        }

        return '-';
    }

    public function blockPolicy()
    {
        return $this->formatBoolean($this->entity->block_policy);
    }

    public function insurancePremium()
    {
        return $this->formatNumber($this->entity->insurance_premium);
    }

    public function lossOfRent()
    {
        $lossOfRent = $this->entity->loss_of_rent;

        if ($lossOfRent) {
            return $lossOfRent.' years';
        }

        return '-';
    }

    public function terrorismCover()
    {
        return $this->formatBoolean($this->entity->terrorism_cover);
    }

    public function terrorismPremium()
    {
        return $this->formatNumber($this->entity->terrorism_premium);
    }

    public function plateGlass()
    {
        $plateGlass = $this->entity->plateGlass;

        if ($plateGlass) {
            return $plateGlass->name;
        }

        return '-';
    }

    public function tenantReimburses()
    {
        return $this->formatBoolean($this->entity->tenant_reimburses);
    }

    public function valuationFeePayable()
    {
        return $this->formatBoolean($this->entity->valuation_fee_payable);
    }

    public function coverNoteReceived()
    {
        return $this->formatBoolean($this->entity->cover_note_received);
    }

    public function year()
    {
        $diaryDate = null;
        $diary = $this->entity->diaries->first();
        if ($diary) {
            $diaryDate = Carbon::parse($diary->date)->format('d/m/Y');
        }

        return [
            'value' => $this->entity->id,
            'label' => Carbon::parse($this->entity->insurance_renewal_date)->subYear()->format('Y') ?? '-',
            'updateUrl' => route('insurances.edit', $this->entity->id).'?property='.request('property')->id,
            'diaryDate' => $diaryDate,
        ];
    }

    public function policy()
    {
        $docs = '';
        foreach ($this->document as $document) {
            $docs .= '<span class="insurance-pdf-preview" data-href="'.route('documents.show', $document->id).'" target="_blank">'.$document->document_name.' 
                       <i class="fas insurance-spinner sr-only fa-spinner fa-spin"></i></span>
                        <span class="fa fa-trash delete-policy-document"  data-document_id="'.$document->id.'" ></span>
                       <br>'.
                '<a href="'.route('documents.show', $document->id).'" target="_blank"><i class="fa fa-download"></i></a><br>';
        }

        return $docs;
    }

    public function dateDiary($diary)
    {
        if (! $diary || ! $diary->date) {
            return 'Overdue';
        }

        return Carbon::parse($diary->date)->format('d/m/Y');
    }
}
