<?php

namespace App\Presenters;

class PropertyRelatedPresenter extends BasePresenter
{
    public function formatNumber($field, $currency = '&pound;')
    {
        $property = $this->entity->property;

        if ($property) {
            $currency = $property->present()->currencyHtmlCode();
        }

        return parent::formatNumber($field, $currency);
    }

    public function diaryDate($returnIfNull = '-')
    {
        $diary = $this->entity->diary;

        if ($diary) {
            return $diary->date->format('d/m/Y');
        }

        return $returnIfNull;
    }
}
