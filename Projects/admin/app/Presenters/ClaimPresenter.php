<?php

namespace App\Presenters;

class ClaimPresenter extends BasePresenter
{
    public function invoiceDate($returnIfNull = '-')
    {
        return $this->formatCarbonDate('invoice_date', $returnIfNull);
    }

    public function clientDate($returnIfNull = '-')
    {
        return $this->formatCarbonDate('client_date', $returnIfNull);
    }

    public function client($returnIfNull = '-')
    {
        if ($this->entity->client) {
            return $this->entity->client->client_name;
        }

        return $returnIfNull;
    }

    public function auditRegister($returnIfNull = '-')
    {
        if ($this->entity->auditRegister) {
            return $this->entity->auditRegister->audit_name;
        }

        return $returnIfNull;
    }

    public function auditor($returnIfNull = '-')
    {
        if ($this->entity->auditor) {
            return $this->entity->auditor->name;
        }

        return $returnIfNull;
    }

    public function claimType($returnIfNull = '-')
    {
        if ($this->entity->claimType) {
            return $this->entity->claimType->name;
        }

        return $returnIfNull;
    }

    public function claimSubtype($returnIfNull = '-')
    {
        if ($this->entity->claimSubtype) {
            return $this->entity->claimSubtype->name;
        }

        return $returnIfNull;
    }

    public function currency($returnIfNull = '-')
    {
        if ($this->entity->currency) {
            return $this->entity->currency->name;
        }

        return $returnIfNull;
    }

    public function currencyHtmlCode()
    {
        if ($this->entity->currency) {
            return $this->entity->currency->html_code;
        }

        return '&pound;';
    }

    public function originalNetValue()
    {
        return $this->customFormatNumber('original_net_value');
    }

    public function vat()
    {
        return $this->customFormatNumber('vat');
    }

    public function grossValue()
    {
        return $this->customFormatNumber('gross_value');
    }

    public function claimAdjustment()
    {
        return $this->customFormatNumber('claim_adjustment');
    }

    public function cancelledClaim()
    {
        return $this->customFormatNumber('cancelled_claim');
    }

    public function netRecovery()
    {
        return $this->customFormatNumber('net_recovery');
    }

    public function claimStatus($returnIfNull = '-')
    {
        if ($this->entity->claimStatus) {
            return $this->entity->claimStatus->name;
        }

        return $returnIfNull;
    }

    public function customFormatNumber($fieldName)
    {
        $value = $this->entity->{$fieldName} ?? 0;

        return $this->currencyHtmlCode().' '.number_format($value, 2);
    }
}
