<?php

namespace App\Presenters;

class ClientPresenter extends BasePresenter
{
    public function isAdmin()
    {
        return $this->formatBoolean($this->entity->is_administrator);
    }

    public function administrator($returnIfNull = '-')
    {
        if ($this->entity->administrator) {
            return $this->entity->administrator->name;
        }

        return $returnIfNull;
    }
}
