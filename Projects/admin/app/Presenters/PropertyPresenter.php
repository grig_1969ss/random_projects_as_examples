<?php

namespace App\Presenters;

class PropertyPresenter extends BasePresenter
{
    public function currencyHtmlCode()
    {
        $client = $this->entity->client;

        if ($client && $client->currency) {
            return $client->currency->html_code;
        }

        return '&pound;';
    }

    public function formatNumber($field, $currency = '&pound;')
    {
        return parent::formatNumber($field, $this->currencyHtmlCode());
    }

    public function ownerType()
    {
        if ($this->entity->ownerType) {
            return $this->entity->ownerType->name;
        }

        return '-';
    }

    public function address()
    {
        if ($this->entity->address) {
            return $this->entity->address->address_1;
        }

        return '';
    }
}
