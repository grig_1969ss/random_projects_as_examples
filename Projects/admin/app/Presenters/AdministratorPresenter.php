<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class AdministratorPresenter extends Presenter
{
    public function mainContact($returnIfNull = '-')
    {
        $mainContact = $this->entity->mainContact;

        if ($mainContact) {
            return $mainContact->first_name.' '.$mainContact->surname;
        }

        return $returnIfNull;
    }
}
