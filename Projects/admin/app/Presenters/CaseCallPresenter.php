<?php

namespace App\Presenters;

class CaseCallPresenter extends BasePresenter
{
    public function caseCallsType()
    {
        if ($this->entity->call_type_id) {
            if ($this->entity->call_type_id == 1) {
                return 'Client';
            } elseif ($this->entity->call_type_id == 2) {
                return 'Debtor';
            }
        }

        return '-';
    }

    public function caseCallNote()
    {
        if ($this->entity->note) {
            return nl2br($this->entity->note);
        }

        return '-';
    }

    public function addedBy()
    {
        $name = '-';
        $date = '-';

        if ($this->entity->createdBy) {
            $name = $this->entity->createdBy->name;
        }

        if ($this->entity->created_at) {
            $date = $this->entity->created_at->format('d/m/Y H:i');
            $date = $this->entity->created_at->format('d M Y g:ia');
        }

        return $name.' on '.$date;
    }
}
