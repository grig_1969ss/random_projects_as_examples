<?php

namespace App\Presenters;

use App\Helpers\Date;
use Carbon\Carbon;

class ServiceChargePresenter extends PropertyRelatedPresenter
{
    public function actualPayment()
    {
        $serviceCharge = $this->entity;

        if ($serviceCharge->property->client_id == 5) {
            if ($serviceCharge->payments()->count() > 0) {
                return $this->formatNumber($serviceCharge->payments->sum('gross_invoice_amount'));
            }
        } else {
            return $this->formatNumber($serviceCharge->actual_year_end);
        }

        return '-';
    }

    public function totalCost()
    {
        $serviceCharge = $this->entity;

        $cost = (float) $serviceCharge->annual_budget + (float) $serviceCharge->balance_service_charge;

        return $this->formatNumber($cost);
    }

    public function yearEnd()
    {
        return $this->formatDate('year_end', 'Y-m-d');
    }

    public function year()
    {
        if (! $this->entity->year_end) {
            return '-';
        }

        $diaryDate = null;
        $diary = $this->entity->diaries->first();
        if ($diary) {
            $diaryDate = Carbon::parse($diary->date)->format('d/m/Y');
        }

        return [
            'value' => $this->entity->id,
            'label' => Date::Y($this->entity->year_end),
            'updateUrl' => route('service-charges.edit', $this->entity->id).'?property='.request('property')->id,
            'diaryDate' => $diaryDate,
        ];
    }

    public function policy()
    {
        $docs = '';
        foreach ($this->document as $document) {
            $docs .= '<a href="'.route('documents.show', $document->id).'" target="_blank">'.$document->document_name.'</a>
             <span class="fa fa-trash delete-policy-document"  data-document_id="'.$document->id.'" ></span>
            <br>';
        }

        return $docs;
    }

    public function dateDiary($diary)
    {
        if (! $diary || ! $diary->date) {
            return 'Overdue';
        }

        return Carbon::parse($diary->date)->format('d/m/Y');
    }
}
