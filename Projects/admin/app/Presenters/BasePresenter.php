<?php

namespace App\Presenters;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;

class BasePresenter extends Presenter
{
    public function formatNumber($field, $currency = '&pound;')
    {
        if (! $field) {
            return '-';
        } elseif (is_numeric($field)) {
            $field = str_replace(['£', ','], '', $field);

            return is_numeric($field) ? $currency.number_format($field, 2) : $field;
        } else {
            return $field;
        }
    }

    public function formatBoolean($field)
    {
        return $field ? 'Yes' : 'No';
    }

    public function formatBooleanShort($field)
    {
        return $field ? 'Y' : 'N';
    }

    public function createdBy()
    {
        if ($this->entity->createdBy) {
            return $this->entity->createdBy->name;
        }

        return '-';
    }

    public function formatDate($fieldName, $originalFormat, $returnIfNull = '-')
    {
        $field = $this->entity->{$fieldName};

        if ($field) {
            return Carbon::createFromFormat($originalFormat, $this->entity->{$fieldName})->format('d/m/Y');
        }

        return $returnIfNull;
    }

    public function formatCarbonDate($fieldName, $returnIfNull = '-')
    {
        $field = $this->entity->{$fieldName};

        if ($field) {
            return $field->format('d/m/Y');
        }

        return $returnIfNull;
    }

    public function createdAt()
    {
        if ($this->entity->created_at) {
            return $this->entity->created_at->format('d/m/Y H:i');
        }

        return '-';
    }
}
