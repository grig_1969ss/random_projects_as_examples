<?php

namespace App\Presenters;

class AuditRegisterPresenter extends BasePresenter
{
    public function auditStartDate($returnIfNull = '-')
    {
        return $this->formatCarbonDate('audit_start_date', $returnIfNull);
    }

    public function auditEndDate($returnIfNull = '-')
    {
        return $this->formatCarbonDate('audit_end_date', $returnIfNull);
    }

    public function client($returnIfNull = '-')
    {
        if ($this->entity->client) {
            return $this->entity->client->client_name;
        }

        return $returnIfNull;
    }

    public function period($returnIfNull = '-')
    {
        $start = $this->entity->audit_start_date;
        $end = $this->entity->audit_end_date;

        if ($start && $end) {
            return $start->format('d/m/Y').' - '.$end->format('d/m/Y');
        }

        return $returnIfNull;
    }

    public function administration()
    {
        return $this->formatBoolean($this->entity->administration);
    }
}
