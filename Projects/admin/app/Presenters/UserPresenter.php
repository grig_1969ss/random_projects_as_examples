<?php

namespace App\Presenters;

class UserPresenter extends BasePresenter
{
    public function client()
    {
        if ($this->entity->client) {
            return $this->entity->client->client_name;
        }

        return '-';
    }

    public function disabled()
    {
        return $this->formatBoolean($this->entity->disabled);
    }
}
