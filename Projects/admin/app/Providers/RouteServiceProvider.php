<?php

namespace App\Providers;

use App\Models\Administrator;
use App\Models\Agent;
use App\Models\Breaks;
use App\Models\Claim;
use App\Models\Client;
use App\Models\ClientQuery;
use App\Models\Contact;
use App\Models\Insurance;
use App\Models\Landlord;
use App\Models\Maintenance;
use App\Models\Payment;
use App\Models\Property;
use App\Models\RentReview;
use App\Models\ServiceCharge;
use App\Models\Supplier;
use App\Models\Utility;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::model('agent', Agent::class);
        Route::model('client', Client::class);
        Route::model('landlord', Landlord::class);
        Route::model('property', Property::class);
        Route::model('contact', Contact::class);
        Route::model('serviceCharge', ServiceCharge::class);
        Route::model('rentReview', RentReview::class);
        Route::model('break', Breaks::class);
        Route::model('clientQuery', ClientQuery::class);
        Route::model('maintenance', Maintenance::class);
        Route::model('insurance', Insurance::class);
        Route::model('supplier', Supplier::class);
        Route::model('payment', Payment::class);
        Route::model('administrator', Administrator::class);
        Route::model('claim', Claim::class);
        Route::model('utility', Utility::class);

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
