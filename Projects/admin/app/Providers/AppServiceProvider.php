<?php

namespace App\Providers;

use App\Models\Currencies;
use App\Models\Diary;
use App\Models\Email;
use App\Models\Payment;
use App\Models\Property;
use App\Models\PropertyQuery;
use App\Observers\CurrencyObserver;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts_new.app', function ($view) {
            if (auth()->check()) {
                $view->with('propertyCount', Property::countProperties())
                    ->with('clientCount', PropertyQuery::countPropertyQueries())
                    ->with('diaryCount', Diary::getAllDiariesDueTodayOrOverdue())
                    ->with('activeMaintenanceCount', Property::countPropertiesWithActiveMaintenance())
                    ->with('rentReviewDueCount', Property::countPropertiesDueRentReview())
                    ->with('leaseExpiryCount', Property::countPropertiesDueLeaseExpiry())
                    ->with('approvalsCount', Payment::whereHas('property', function ($query) {
                        $query->where('properties.client_id', auth()->user()->client_id);
                    })->count())
                    ->with('refitCount', Property::countPropertiesUndergoingRefit())
                    ->with('allEmails', Email::inbox('all', true)->filterByCurrentClient()->count())
                    ->with('myEmails', Email::inbox('my', true)->filterByCurrentClient()->count());
            }
        });

        Blade::directive('badge', function ($count) {
            return '<?php if ('.$count.'  > 0) {
                echo "<span class=\'badge badge-info\'>'.$count.'</span>";
            } ?>';
        });

        Currencies::observe(CurrencyObserver::class);

        $url = request()->getHost();
        $urlExploded = explode('.', $url);

        view()->share('_SUBDOMAIN', $urlExploded[0]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment(['staging', 'production'])) {
            $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
            $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
        }
    }
}
