<?php

namespace App\Services;

use App\Helpers\Currency;
use App\Helpers\Reports\Approval;
use App\Helpers\Reports\ClaimReport;
use App\Helpers\Reports\InvoicesOnHold;
use App\Helpers\Reports\MasterSpreadsheet;
use App\Helpers\Reports\RentReview;
use App\Models\Document;
use App\Models\Payment;
use App\Models\Property;
use App\Models\Rent;
use App\Repositories\PropertyRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReportService
{
    public function getMasterSpreadsheet()
    {
        $property = new Property();

        $properties = (new PropertyRepository())->getAllPropertiesDueLeaseExpiry(null);
        $props[0] = MasterSpreadsheet::reportHeaders();
        $i = 1;

        foreach ($properties as $property) {
            $helper = new MasterSpreadsheet($property);
            $props[$i] = $helper->fillPropertyData();

            $i++;
        }

        $this->prepareCSV($props);
    }

    public function getRentReview($inputs)
    {
        $helper = new RentReview($inputs);

        return [$helper->properties, $helper->order, $helper->viewDate];
    }

    public function getRentReviewCSV($inputs)
    {
        $helper = new RentReview($inputs, true);
        $data = $helper->toCSV();

        $this->prepareCSV($data);
    }

    public function getApprovals($input)
    {
        $helper = new Approval($input);

        return $helper->toArray();
    }

    public function getApprovalsPDF($input)
    {
        $helper = new Approval($input, true);

        return $helper->toArray();
    }

    public function getApprovalsByProperty($input, $isCSV = false)
    {
        $propertyId = isset($input['property_id']) ? $input['property_id'] : null;

        if ($propertyId) {
            $select = [
                DB::raw("(select property_name from properties where id = {$propertyId} limit 1) as property_name"),
                DB::raw('(select name from suppliers where id = payments.supplier_id limit 1) as supplier'),
                DB::raw("date_format(payment_date, '%d/%m/%Y') as invoice_date"),
                'payment_ref',
                DB::raw('(select name from payment_types where id = payments.payment_type_id limit 1) as payment_type'),
                'period',
                DB::raw('format(gross_invoice_amount, 2) as gross_invoice_amount'),
                DB::raw("date_format(date_approved, '%d/%m/%Y') as approved_date"),
                DB::raw("if(hold = 1, 'Yes', 'No') as held"),
            ];

            if (! $isCSV) {
                $select[] = 'id';
                $select[] = DB::raw("{$propertyId} as property_id");
            }

            $approvals = Payment::where('property_id', $propertyId)->select($select)->orderBy('payment_type')->orderBy('payment_date')->get();
        } else {
            $approvals = collect();
        }

        return $approvals;
    }

    public function getApprovalsByPropertyCSV($input)
    {
        $approvals = $this->getApprovalsByProperty($input, true);

        $headers = [
            'Property Name',
            'Supplier',
            'Supplier Invoice Date',
            'Supplier Invoice Number',
            'Payment Type',
            'Period',
            'Gross Invoice Value',
            'Approved Date',
            'Held',
        ];

        $data = array_merge([$headers], $approvals->toArray());

        $this->prepareCSV($data);
    }

    public function getInvoicesOnHold($input)
    {
        $helper = new InvoicesOnHold($input);

        return $helper->toArray();
    }

    public function getInvoicesOnHoldCSV($input)
    {
        $helper = new InvoicesOnHold($input, true);
        $data = $helper->toCSV();

        $this->prepareCSV($data);
    }

    public function getClaims($input)
    {
        $helper = new ClaimReport($input);

        return [$helper->claims, $helper->order];
    }

    public function getClaimsCSV($input)
    {
        $helper = new ClaimReport($input, true);
        $data = $helper->toCSV();

        $this->prepareCSV($data);
    }

    public function getInsuranceRenewalDates()
    {
        $user = auth()->user();
        $subQuery = '';

        if ($user->client_id) {
            $subQuery = "AND p.client_id = $user->client_id";
        }

        $sql = "SELECT p.id,
                       p.property_name,
                       p.reference,
                       DATE_FORMAT(MAX(i.insurance_renewal_date), '%d/%m/%Y') as insurance_renewal_date
                FROM properties AS p
                JOIN insurances AS i ON i.property_id = p.id
                WHERE true
                      $subQuery
                GROUP BY p.id
                ORDER BY MAX(i.insurance_renewal_date), property_name, property_name;";

        return DB::select($sql);
    }

    public function getInsuranceRenewalDatesCSV()
    {
        $headers = [['Property ID', 'Ref', 'Property', 'Renewal Date']];
        $data = $this->getInsuranceRenewalDates();
        $data = array_merge($headers, $data);

        $this->prepareCSV($data);
    }

    public function getServiceChargeAnnualBudget($clientId)
    {
        $subQuery = '';

        if ($clientId) {
            $subQuery = "AND p.client_id = $clientId";
        }

        $sql = "SELECT p.id,
                       p.reference                                               as property_ref,
                       property_name,
                       (select name from owner_types where id = p.owner_type_id) as owner_type,
                       date_format(sc.year_end, '%d/%m/%Y')                      as renewal_date,
                       format(sc.annual_budget, 2)                               as annual_budget
                FROM properties p
                         JOIN service_charges sc ON p.id = sc.property_id
                where true
                  {$subQuery}
                  AND sc.deleted_at is null
                ORDER BY property_ref, sc.year_end desc;";

        $result = DB::select($sql);
        $result = collect($result)->groupBy('id');
        $data = [];

        foreach ($result as $items) {
            $_items = $items->take(2);

            foreach ($_items as $item) {
                $data[] = $item;
            }
        }

        return $data;
    }

    public function getServiceChargeAnnualBudgetCSV($clientId)
    {
        $headers = [['Property ID', 'Property Ref', 'Property Name', 'Owner Type', 'Renewal Date', 'Annual Budget']];
        $data = $this->getServiceChargeAnnualBudget($clientId);
        $data = array_merge($headers, $data);

        $this->prepareCSV($data);
    }

    public function getInsuranceAnnualBudget($clientId)
    {
        $subQuery = '';

        if ($clientId) {
            $subQuery = "AND p.client_id = $clientId";
        }

        $sql = "SELECT p.id,
                       p.reference                                               as property_ref,
                       property_name,
                       (select name from owner_types where id = p.owner_type_id) as owner_type,
                       date_format(i.insurance_renewal_date, '%d/%m/%Y')         as renewal_date,
                       format(i.insurance_premium, 2)                            as insurance_premium
                FROM properties p
                         JOIN insurances i ON p.id = i.property_id
                where true
                  {$subQuery}
                  AND i.deleted_at is null
                  and i.insurance_renewal_date is not null
                ORDER BY property_ref, i.insurance_renewal_date desc;";

        $result = DB::select($sql);
        $result = collect($result)->groupBy('id');
        $data = [];

        foreach ($result as $items) {
            $_items = $items->take(2);

            foreach ($_items as $item) {
                $data[] = $item;
            }
        }

        return $data;
    }

    public function getInsuranceAnnualBudgetCSV($clientId)
    {
        $headers = [
            ['Property ID', 'Property Ref', 'Property Name', 'Owner Type', 'Renewal Date', 'Insurance Premium'],
        ];

        $data = $this->getInsuranceAnnualBudget($clientId);
        $data = array_merge($headers, $data);

        $this->prepareCSV($data);
    }

    protected function prepareCSV($data)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=report.csv');

        $fp = fopen('php://output', 'w');

        foreach ($data as $item) {
            if (! is_array($item)) {
                $item = (array) $item;
            }

            fputcsv($fp, $item);
        }

        exit;
    }

    // TODO REMOVE
    public static function getPropertiesRentAndSOC($properties)
    {
        // get current rents
        $rents = Rent::getCurrentRents();

        // get schedules
        $schedules = Document::getDocumentsByType(7);

        // set rents fore each property
        foreach ($properties as $property) :
            foreach ($rents as $rent) :
                if ($property->id == $rent->property_id) :
                    $property->current_rent = $rent->rent;
        endif;
        endforeach;

        foreach ($schedules as $schedule) :
                if ($property->id == $schedule->property_id) :
                    $property->schedule_of_condition_link = Storage::url($schedule->document_file);
        $property->schedule_of_condition = $schedule->document_name;
        endif;
        endforeach;

        // check they are set if not then show -
        $property->current_rent = isset($property->current_rent) ? Currency::gbp($property->current_rent) : '-';
        $property->schedule_of_condition_link = isset($property->schedule_of_condition_link) ? $property->schedule_of_condition_link : "/properties/{$property->id}";
        $property->schedule_of_condition = isset($property->schedule_of_condition) ? $property->schedule_of_condition : '-';
        $property->rateable_value = $property->rateable_value ? Currency::gbp($property->rateable_value) : '-';
        endforeach;

        return $properties;
    }
}
