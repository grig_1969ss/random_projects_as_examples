<?php

namespace App\Services;

use App\Helpers\Date;

class PropertyService
{
    private $input;

    public function __construct($input)
    {
        $this->input = $input;
    }

    public function formatInput()
    {
        $this->input['date_last_inspected'] = Date::toMysql($this->input['date_last_inspected']);
        $this->input['lease_start_date'] = Date::toMysql($this->input['lease_start_date']);
        $this->input['lease_expiry_date'] = Date::toMysql($this->input['lease_expiry_date']);
        $this->input['rent_review_date'] = Date::toMysql($this->input['rent_review_date']);
        $this->input['lease_notice_dates'] = Date::toMysql($this->input['lease_notice_dates']);
        $this->input['erv_date'] = Date::toMysql($this->input['erv_date']);
        $this->input['purchase_date'] = Date::toMysql($this->input['purchase_date']);
        $this->input['commencement_date'] = Date::toMysql($this->input['commencement_date']);
        $this->input['ext_first_due'] = Date::toMysql($this->input['ext_first_due']);
        $this->input['int_first_due'] = Date::toMysql($this->input['int_first_due']);
        $this->input['ext_next_due'] = Date::toMysql($this->input['ext_next_due']);
        $this->input['int_next_due'] = Date::toMysql($this->input['int_next_due']);
        $this->input['concessions'] = $this->formatBool('concessions');
        $this->input['share'] = $this->formatBool('share');
        $this->input['non_structural'] = $this->formatBool('non_structural');
        $this->input['structural'] = $this->formatBool('structural');
        $this->input['landlord_consent'] = $this->formatBool('landlord_consent');
        $this->input['restrictions'] = $this->formatBool('restrictions');
        $this->input['ext_final_year'] = $this->formatBool('ext_final_year');
        $this->input['int_final_year'] = $this->formatBool('int_final_year');
        $this->input['auto_service_charge'] = $this->formatBool('auto_service_charge');
        $this->input['schedule_condition'] = $this->formatBool('schedule_condition');
        $this->input['undergoing_refit'] = $this->formatBool('undergoing_refit');
        $this->input['has_service_charge'] = $this->formatBool('has_service_charge');

        return $this->input;
    }

    private function formatBool($key)
    {
        if (! isset($this->input[$key])) {
            return 0;
        }

        return $this->input[$key];
    }
}
