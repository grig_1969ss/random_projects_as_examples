<?php

namespace App\Services;

use App\Helpers\Util;
use App\Jobs\SendEmail;
use App\Models\AttachmentDetails;
use App\Models\Document;
use App\Models\Email;
use App\Models\EmailEvent;
use App\Traits\HelperTrait;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EmailService
{
    use HelperTrait;

    public function sendEmail($request, $files, $user, $createSentEmail = true)
    {
        if (! $request->input('is_draft')) {
            if ($createSentEmail) {
                $emailId = $this->saveEmail($request, 2)->id;
            } else {
                $emailId = null;
            }

            $this->doSend($request, $user, $files, $createSentEmail, $emailId);
        } else {
            $this->saveEmail($request, 3);
        }
    }

    public function doSend($request, $user, $files, $createSentEmail, $id = null)
    {
        $hashids = new Hashids('', 0, config('services.hashid.library'));

        $from_address = auth()->user()->client->inbound_email;
        $replyto_address = $from_address;

        $newBody = Util::bodyWithSignature($request, $user);

        $previousContent = $this->previousContent($request);

        $email = Email::find($request->previous_id);

        $replyTime = Util::replyEmailTime($email);

        $data = [
            'From' => $from_address,
            'ReplyTo' => $replyto_address,
            'To' => $request['to'],
            'Subject' => $request['subject'],
            'HtmlBody' => $newBody.'<br><br><br>'.$replyTime.$previousContent,
            'TrackOpens' => true,
        ];

        $request['cc'] = str_replace(';', ',', $request['cc']);
        $request['cc'] = preg_replace('/\s+/', '', $request['cc']);

        if (! empty($request['cc'])) {
            $data['Cc'] = explode(',', $request['cc']);
        }

        $data['Attachments']['base_64'] = [];

        // modal
        if ($request->attachment_ids) {
            $data['Attachments']['base_64'] = $this->getFilesDocument($request->attachment_ids);
        }

        if ($request->attachment_ids_base_64) {
            $data['Attachments']['base_64'] = $this->duplicateFromTable($request->attachment_ids_base_64);
        }

        // generated email from body_html
        if ($request->attached_pdf_name) {
            $data['Attachments']['attached_pdf'] = $request->attached_pdf_name;
        }

        $data['Attachments']['files'] = $files;

        $job = (new SendEmail($data, $id));

        dispatch($job);
    }

    public function saveEmail(Request $request, $typeId, $messageId = null)
    {
        $htmlBody = Util::bodyWithSignature($request, auth()->user(), true);

        $previousContent = $this->previousContent($request);

        $email = Email::find($request->previous_id);

        $replyTime = Util::replyEmailTime($email);

        $propertyId = $request->template_property ?? $request->get('property_id');

        $email = Email::create([
            'property_id' => $propertyId,
            'client_id' => auth()->user()->client_id ?? null,
            'from' => auth()->user()->client->inbound_email,
            'from_name' => 'CAPA ('.auth()->user()->client->client_name.')',
            'to' => $request->input('to'),
            'cc' => $request->input('cc'),
            'subject' => $request->input('subject'),
            'message_id' => $messageId,
            'date' => now(),
            'text_body' => $request->input('body'),
            'html_body' => $request->input('body').$htmlBody.'<br><br><br>'.$replyTime.$previousContent,
            'stripped_text_reply' => '',
            'email_type_id' => $typeId,
        ]);

        $data = [
            'email_id' => $email->id,
            'type' => 'Outbound',
        ];
        EmailEvent::insert($data);
        $this->saveAttachments($request, $email);

        return $email;
    }

    private function previousContent($request)
    {
        if (! isset($request->previous_content)) {
            return '';
        }

        $previousEmail = Email::find($request->previous_id);
        $previousContent = $this->cleanBody($request->previous_content);
        $htmlBody = $this->replaceCidToBase64($previousContent, $previousEmail);

        return $htmlBody;
    }

    public function saveAttachments(Request $request, Email $sentEmail)
    {
        if (! ($request->hasFile('attachments') || $request->attachment_ids || $request->attachment_ids_base_64 || $request->attached_pdf_name)) {
            return false;
        }

        // e.g. from s/c email create modal
        if ($request->attachment_ids) {
            $files = $this->getFilesDocument($request->attachment_ids, $sentEmail->id);

            AttachmentDetails::insert($files);
        }

        // forward,reply...
        if ($request->attachment_ids_base_64) {
            $files = $this->duplicateFromTable($request->attachment_ids_base_64, $sentEmail->id);

            AttachmentDetails::insert($files);
        }

        // forward,reply...
        if ($request->attached_pdf_name) {
            $files = $this->generatedPdf($request->attached_pdf_name, $sentEmail);

            AttachmentDetails::insert($files);
        }

        $uploadedFiles = $request->file('attachments');

        if (! $uploadedFiles) {
            return true;
        }

        $files = [];
        foreach ($uploadedFiles as $uploadedFile) {
            $temp = [
                'email_id' => $sentEmail->id,
                'name' => $uploadedFile->getClientOriginalName(),
                'content' => base64_encode(File::get($uploadedFile)),
                'content_type' => $uploadedFile->getMimeType(),
                'content_length' => strlen(base64_encode(File::get($uploadedFile))),
            ];

            $files[] = $temp;
        }

        AttachmentDetails::insert($files);
    }

    private function generatedPdf($path, $sentEmail)
    {
        $fullPath = storage_path('/app/emails_as_pdf/').$path;
        $mimeType = File::mimeType($fullPath);
        $content = File::get($fullPath);
        $now = now();

        return [
            'email_id' => $sentEmail->id,
            'name' => $path,
            'content' => base64_encode($content),
            'content_type' => $mimeType,
            'content_length' => strlen(base64_encode($content)),
            'created_at' => $now,
            'updated_at' => $now,
        ];
    }

    private function duplicateFromTable($idsToDuplicate, $id = null)
    {
        $ids = explode(',', $idsToDuplicate);

        $attachmentsToDuplicate = AttachmentDetails::whereIn('id', $ids)->cursor();
        $files = [];

        foreach ($attachmentsToDuplicate as $attachment) {
            $now = now();
            $content = $attachment->content;
            $files[] = [
                'email_id' => $id,
                'name' => $attachment->name,
                'content' => $content,
                'content_type' => $attachment->content_type,
                'content_length' => strlen($content),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        return $files;
    }

    private function getFilesDocument($attachment_ids, $id = null)
    {
        $docIds = explode(',', $attachment_ids);
        $documents = Document::whereIn('id', $docIds)->cursor();
        $files = [];
        foreach ($documents as $document) {
            $now = now();
            $content = Storage::disk('s3')->get($document->document_file);
            $content = base64_encode($content);
            $files[] = [
                'email_id' => $id,
                'name' => $document->document_name,
                'content' => $content,
                'content_type' => $document->content_type,
                'content_length' => strlen($content),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        return $files;
    }
}
