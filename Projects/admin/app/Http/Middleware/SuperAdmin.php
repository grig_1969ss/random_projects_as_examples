<?php

namespace App\Http\Middleware;

use Closure;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Hide settings and users from nav top right dropdown when not logged in as a super admin.
        if (auth()->user()->hasRole('superAdmin')) {
            return $next($request);
        }

        return redirect('/');
    }
}
