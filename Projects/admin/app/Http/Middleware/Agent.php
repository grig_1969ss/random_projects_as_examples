<?php

namespace App\Http\Middleware;

use Closure;

class Agent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Hide agents when logged in as a client.
        if (auth()->user()->hasRole('client')) {
            return redirect('/');
        }

        return $next($request);
    }
}
