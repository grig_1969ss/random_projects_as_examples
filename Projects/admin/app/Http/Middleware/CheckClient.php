<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckClient
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = auth()->user();
        // check if user logged in as a client, and if so, check if it has assigned clients

        if (! $auth->hasRole('superAdmin')) {
            if (is_null($auth->client_id) || ! $auth->clients()->count()) {
                Auth::logout();

                return redirect('login')->withErrors(['Invalid settings, please contact a super user']);
            }
        }

        //show switch user screen if the selected client_id isn’t in their list of client_user
        if (
            ! in_array($auth->client_id, $auth->clients()->pluck('client_id')->toArray()) &&
            request()->path() !== 'account/switch-client' // prevent endless redirects
        ) {
            return redirect('account/switch-client');
        }

        return $next($request);
    }
}
