<?php

namespace App\Http\Controllers\Api;

use App\Events\NewInboundEmail;
use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Models\AttachmentDetails;
use App\Models\Client;
use App\Models\Email;
use App\Models\EmailEvent;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Postmark\Inbound;

class InboundMailController extends Controller
{
    private $inbound;
    private $hash;
    private $InboundCcFull;

    public function __construct()
    {
        $this->hashids = new Hashids('', 0, config('services.hashid.library'));
        $this->hash = null;
    }

    public function inbound(AttachmentDetails $attachment, Email $email)
    {
        $this->inbound = new Inbound(file_get_contents('php://input'));

        $to = $this->inbound->ToFull();

        array_shift($to);

        $cc = is_array($this->inbound->CcFull()) ? $this->inbound->CcFull() : [];

        foreach ($to as $toEmail) {
            if (is_array($cc)) {
                $cc = array_merge([(array) $toEmail], $cc);
            } else {
                $cc = [(array) $toEmail];
            }
        }

        $this->InboundCcFull = $cc;

        $this->getHash();

        $email->subject = $this->inbound->Subject() ?? '';
        $email->from_name = $this->inbound->FromName() ?? '';
        $email->from = $this->inbound->From() ?? '';
        $email->to = Arr::pluck($this->inbound->ToFull(), 'Email')[0];
        $email->cc = json_encode($cc) ?? '';
        $email->message_id = $this->inbound->MessageID() ?? '';
        $email->body = $this->inbound->TextBody() ?? '';
        $email->html_body = $this->inbound->HtmlBody() == '0' ? nl2br($this->inbound->TextBody()) : $this->inbound->HtmlBody();
        $email->email_type_id = 1;
        $email->stripped_text_reply = $this->inbound->StrippedTextReply() ?? false;
        $email->date = Date::dateOrNow($this->inbound->Date())->format('Y-m-d H:i:s');
        $email->client_id = $this->getClient();
        $email->property_id = $this->hash ? $this->hashids->decode($this->hash)[0] : null;

        $email->save();
        if ($email->id) {
            foreach ($this->inbound->Attachments()->attachments as $attachment) {
                $attach = new AttachmentDetails();
                $attach->email_id = $email->id;
                $attach->name = $attachment->Name;
                $attach->content = $attachment->Content ?? '';
                $attach->content_type = $attachment->ContentType;
                $attach->content_length = $attachment->ContentLength;

                $ContentID = $this->contentId($attachment);
                $attach->contentid = $ContentID;

                $attach->save();
                unset($attach);
            }
        }
        event(new NewInboundEmail($email));

        return ['status' => 'Success', 'message' => 'ok'];
    }

    private function contentId($attachment)
    {
        if (! isset($attachment->ContentID)) {
            return;
        }
        $ContentID = $attachment->ContentID;
        $ContentType = $attachment->ContentType;

        if (substr($ContentType, 0, 5) !== 'image') {
            return;
        }

        if (strpos(strtolower($ContentID), 'outlook.com') !== false) {
            return;
        }

        return $ContentID;
    }

    public function mailStatus(Request $request)
    {
        $emailId = $request->get('Tag');
        $emailType = $request->get('RecordType');

        EmailEvent::insert([
            'email_id' => $emailId,
            'type' => $emailType,
        ]);

        return response()->json('ok', Response::HTTP_OK);
    }

    public function testInbound()
    {
        $url = 'https://api.'.config('app.host').'/inbound-mail/'.config('services.postmark.url_secret');
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        $data = '{ "FromName": "Paul Johnston", "From": "paul@upsoftware.co.uk", "FromFull": { "Email": "paul@upsoftware.co.uk", "Name": "Paul Johnston", "MailboxHash": "" }, "To": "\"CAPA McDonalds\" <mcdonalds@mcd.inbound.capa.uk.com>", "ToFull": [ { "Email": "mcdonalds@mcd.inbound.capa.uk.com", "Name": "CAPA McDonalds", "MailboxHash": "SampleHash" } ], "Cc": "\"First Cc\" <firstcc@postmarkapp.com>, secondCc@postmarkapp.com", "CcFull": [ { "Email": "firstcc@postmarkapp.com", "Name": "First Cc", "MailboxHash": "" }, { "Email": "secondCc@postmarkapp.com", "Name": "", "MailboxHash": "" } ], "Bcc": "\"First Bcc\" <firstbcc@postmarkapp.com>, secondbcc@postmarkapp.com", "BccFull": [ { "Email": "firstbcc@postmarkapp.com", "Name": "First Bcc", "MailboxHash": "" }, { "Email": "secondbcc@postmarkapp.com", "Name": "", "MailboxHash": "" } ], "OriginalRecipient": "yourhash+SampleHash@inbound.postmarkapp.com", "Subject": "Test subject", "MessageID": "73e6d360-66eb-11e1-8e72-a8904824019b", "ReplyTo": "replyto@postmarkapp.com", "MailboxHash": "SampleHash", "Date": "Wed, 1 Jan 2020 16:45:32 -04:00", "TextBody": "This is a test text body.", "HtmlBody": "<html><body><p>This is a test html body.<\/p><\/body><\/html>", "StrippedTextReply": "This is the reply text", "Tag": "TestTag", "Headers": [ { "Name": "X-Header-Test", "Value": "" }, { "Name": "X-Spam-Status", "Value": "No" }, { "Name": "X-Spam-Score", "Value": "-0.1" }, { "Name": "X-Spam-Tests", "Value": "DKIM_SIGNED,DKIM_VALID,DKIM_VALID_AU,SPF_PASS" } ], "Attachments": [ { "Name": "test.txt", "Content": "VGhpcyBpcyBhdHRhY2htZW50IGNvbnRlbnRzLCBiYXNlLTY0IGVuY29kZWQu", "ContentType": "text/plain", "ContentLength": 45 } ] }';

        $response = $client->request('POST', $url, ['body' => $data]);
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getBody(), true);

        return $content;
    }

    private function getHash()
    {
        if (is_array($this->inbound->ToFull())) {
            foreach ($this->inbound->ToFull() as $toEmail) {
                $emailAddress = $toEmail->Email;
                $token = explode('-', strtok($emailAddress, '@'));

                if (strpos($emailAddress, config('services.inbound.domain')) !== false) {
                    if (isset($token)) {
                        $this->hash = $token;

                        break;
                    }
                }
            }
        }
    }

    private function getClient()
    {
        if (! $client = $this->getClientDependsOnToOrCC($this->inbound->ToFull(), true)) {
            return $this->getClientDependsOnToOrCC($this->InboundCcFull);
        }

        return $client;
    }

    private function getClientDependsOnToOrCC($ToOrCC, $to = false)
    {
        if (! is_array($ToOrCC)) {
            return;
        }

        if ($to) {
            foreach ($ToOrCC as $to) {
                $belongToClient = Client::where('inbound_email', $to->Email)->first();
                if ($belongToClient) {
                    return $belongToClient->id;
                }
            }

            // from to get the first one
            $ToOrCC = [$ToOrCC[0]];
        }

        $returnClientId = null;
        foreach ($ToOrCC as $key => $cc) {
            $cc = (array) $cc;

            $emailAddress = $cc['Email'];

            if (! $emailAddress) {
                continue;
            }

            if ($returnClientId = optional((new Client)->where('inbound_email', $emailAddress)->first())->id) {
                break;
            }

            $at = strpos($emailAddress, '@');
            $end = strpos($emailAddress, '.inbound.');
            $identifier = substr($emailAddress, $at + 1, $end - $at - 1);

            if (! $identifier) {
                continue;
            }

            $client = Client::where('inbound_identifier', $identifier)->first();

            if ($client) {
                $returnClientId = $client->id;
                break;
            }
        }

        return $returnClientId;
    }
}
