<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessInbound;
use Illuminate\Http\Request;
use Postmark\Inbound;
use Postmark\InboundException;

class EmailController extends Controller
{
    public function inbound(Request $request)
    {
        try {
            $inbound = new Inbound(json_encode($request->all()));

            $this->dispatch(new ProcessInbound($inbound, ['forwarded' => true]));
        } catch (InboundException $e) {
            \Log::debug($e);
        }
    }

    public function processMailDelivery()
    {
        return ['status'=>'Success', 'message'=>'ok'];
    }
}
