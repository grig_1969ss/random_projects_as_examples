<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\BreakRequest;
use App\Models\Breaks;
use App\Models\BreakType;
use App\Models\Diary;
use App\Models\Property;

class BreakController extends Controller
{
    public function create(Property $property)
    {
        $break = new Breaks();
        $breakClauseTypeDropdown = BreakType::getAllBreakTypesForDropdown();
        $title = 'Add Break';

        return view('break_options.edit', compact('break', 'property', 'breakClauseTypeDropdown'));

//        $view = view('properties.includes.break_options.edit', compact('break', 'property', 'breakClauseTypeDropdown'))
//            ->render();
//
//        return response()->json(compact('view', 'title'));
    }

    public function store(Property $property, BreakRequest $request)
    {
        $input = $request->all();
        $input['break_date'] = Date::toMysql($input['break_date']);
        $input['notice_period'] = Date::toMysql($input['notice_period']);
        $input['break_clause'] = $input['break_clause'] != '' ? $input['break_clause'] : '-';

        $diary = Diary::create([
            'property_id'   => $property->id,
            'diary_type_id' => 5,
            'diary_title'   => 'Break Option',
            'date'          => $input['break_date'],
            'status'        => 0,
        ]);

        $input['diary_id'] = $diary->id;

        $property->breaks()->create($input);

        Util::addNote(
            $property->id,
            'properties',
            5,
            $input['break_clause'] != '' ? $input['break_clause'] : 'Break was Added'
        );

        $data = [
            'id'         => $property->id,
            'status'     => 'ok',
            'controller' => 'properties',
            'modal'      => 'div#breakModalAdd',
            'selector'   => 'table#break-table tbody',
        ];

        return response()->json($data);
    }

    public function edit(Breaks $break)
    {
        $breakClauseTypeDropdown = BreakType::getAllBreakTypesForDropdown();
        $title = 'Edit Break';

        return view('break_options.edit', compact('break', 'breakClauseTypeDropdown'));

//        $view = view('properties.includes.break_options.edit', compact('break', 'breakClauseTypeDropdown'))
//            ->render();
//
//        return response()->json(compact('view', 'title'));
    }

    public function update(Breaks $break, BreakRequest $request)
    {
        $input = $request->all();
        $input['break_clause'] = $input['break_clause'] != '' ? $input['break_clause'] : '-';
        $input['break_date'] = Date::toMysql($input['break_date']);
        $input['notice_period'] = Date::toMysql($input['notice_period']);

        $break->update($input);

        Util::addNote(
            $break->property_id,
            'properties',
            5,
            $input['break_clause'] != '' ? $input['break_clause'] : 'Break was Edited'
        );

        if (isset($input['diary_id'])) {
            $diary = Diary::find($input['diary_id']);

            if ($diary) {
                $diary->update([
                    'date' => $input['break_date'],
                ]);
            }
        }

        $data = [
            'id'         => $break->property_id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function destroy(Breaks $break)
    {
        $break->update([
            'archived' => true,
        ]);

        return redirect(route('properties.show', $break->property))->with('success', 'Break Deleted.');
    }
}
