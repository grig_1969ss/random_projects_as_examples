<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\Property;
use App\Models\Supplier;
use App\Traits\HelperTrait;
use App\Traits\UpdateDiary;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    use UpdateDiary, HelperTrait;

    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->read_only;
        $paymentObj = new Payment();
        $payments = $paymentObj->getPayments();

        unset($paymentObj);

        return view('payments.index', compact('payments', 'read_only'));
    }

    public function create(Property $property)
    {
        $payment = new Payment();
        $route = route('properties.payments.store', $property);
        $serviceCharges = $property->serviceCharge()->orderBy('year_end')->get();
        $suppliers = Supplier::orderBy('name')->select(['id', 'name'])->get();
        $paymentTypes = PaymentType::orderBy('name')->get();
        $title = 'Add Approval';
        $quarters = [1, 2, 3, 4];
        $contacts = collect();
        $contactId = null;

        return view('payments.edit', compact(
            'payment',
            'property',
            'route',
            'serviceCharges',
            'quarters',
            'suppliers',
            'paymentTypes',
            'title',
            'contacts',
            'contactId'
        ));
    }

    public function store(Property $property, PaymentRequest $request)
    {
        $input = $request->all();
        $input['payment_date'] = Date::toMysql($input['payment_date']);
        $input['date_approved'] = Date::toMysql($input['date_approved']);

        if ($input['diary_date']) {
            $diary = $property->createDiaryEntry(6, 'Approval', $input['diary_date']);
            $input['diary_id'] = $diary->id;
        }

        $payment = $property->payment()->create($input);

        Util::addNote($payment->id, 'properties', 7, 'Approval was Added');

        $data = [
            'id'         => $property->id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        $this->selectData($property, $payment->id, $request->modal_save, $data, 'Payment');

        return response()->json($data);
    }

    public function edit(Payment $payment)
    {
        $route = route('payments.update', $payment);
        $property = $payment->property;
        $serviceCharges = $property->serviceCharge()->orderBy('year_end')->get();
        $suppliers = Supplier::orderBy('name')->select(['id', 'name'])->get();
        $paymentTypes = PaymentType::orderBy('name')->get();
        $title = 'Edit Approval';
        $quarters = [1, 2, 3, 4];
        $contacts = $payment->supplier ? $payment->supplier->contacts : collect();
        $contactId = $payment->contact_id;

        return view('payments.edit', compact(
            'payment',
            'property',
            'route',
            'serviceCharges',
            'quarters',
            'suppliers',
            'paymentTypes',
            'title',
            'contacts',
            'contactId'
        ));
    }

    public function update(Payment $payment, PaymentRequest $request)
    {
        $input = $request->all();
        $input['payment_date'] = Date::toMysql($input['payment_date']);
        $input['date_approved'] = Date::toMysql($input['date_approved']);
        $date = $input['diary_date'];

        [$input['diary_id'], $deleteDiary] = $this->updateDiaryEntry($date, $payment, 6, 'Approval');

        $payment->update($input);

        if ($deleteDiary) {
            $payment->diary->delete();
        }

        Util::addNote($payment->id, 'properties', 7, 'Approval was Edited');

        $data = [
            'id'         => $payment->property_id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function destroy(Payment $payment)
    {
        $property = $payment->property;

        try {
            $payment->delete();
        } catch (\Exception $e) {
            Log::debug($e);

            return;
        }

        return redirect(route('properties.show', $property));
    }
}
