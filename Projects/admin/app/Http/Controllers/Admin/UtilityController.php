<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\UtilityRequest;
use App\Models\PaymentType;
use App\Models\Property;
use App\Models\Utility;
use Illuminate\Http\Response;

class UtilityController extends Controller
{
    public function create(Property $property)
    {
        $utility = new Utility();
        $title = 'Add Utility';
        $route = route('properties.utilities.store', $property);
        $properties = $this->propertiesForClient();
        $payments = PaymentType::cursor();

        return view('utilities.edit', compact(
            'utility',
            'route',
            'title',
            'properties',
            'payments'
        ));
    }

    private function propertiesForClient()
    {
        return Property::where('client_id', auth()->user()->client_id)
            ->select('id', 'client_id', 'property_name')->cursor();
    }

    public function store(UtilityRequest $request)
    {
        Utility::create($request->all());

        return response()->json([
            'id' => $request->property_id,
            'controller' => 'properties',
        ], Response::HTTP_OK);
    }

    public function edit(Utility $utility)
    {
        $route = route('utilities.update', $utility);
        $property = $utility->property;
        $title = 'Edit Utility';
        $payments = PaymentType::cursor();

        return view('utilities.edit', compact(
            'utility',
            'route',
            'title',
            'property',
            'payments'
        ));
    }

    public function update(Utility $utility, UtilityRequest $request)
    {
        $utility->update($request->all());

        Util::addNote($utility->id, 'utilities', 11, 'Utility was Edited');

        $data = [
            'id'         => $utility->property_id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }
}
