<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\Address;
use App\Models\Administrator;
use App\Models\Claim;
use App\Models\Client;
use App\Models\Contact;
use App\Models\NoteType;
use App\Queries\PropertiesByCompany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->isClient();
        $clients = Client::getClients();

        return view('clients.index', compact('clients', 'read_only'));
    }

    public function create()
    {
        $client = new Client();
        $address = new Address();
        $currencies = DB::table('currencies')->get()->toArray();
        //$currencies = Currency::all()->toArray();
        $administrators = Administrator::orderBy('name')->select(['id', 'name'])->get();

        return view('clients.edit', compact('client', 'address', 'currencies', 'administrators'));
    }

    public function store(ClientRequest $request)
    {
        $input = $request->except('_token');
        $userId = $this->user->id;

        if (isset($input['isModal'])) {
            $isModal = $input['isModal'];

            unset($input['isModal']);
        } else {
            $isModal = false;
        }

        $contactFirstname = $input['contact_firstname'];

        unset($input['contact_firstname']);

        $contactSurname = $input['contact_surname'];

        unset($input['contact_surname']);

        $clientId = Util::saveFormValues($input, null, Client::class, true);

        Util::addNote($clientId, 'clients', 1, 'Client was Added');

        // save contact name
        $contactData = [
            'first_name' => $contactFirstname,
            'surname' => $contactSurname,
            'contactable_id' => $clientId,
            'contactable_type' => 'App\\Models\\Client',
            'created_by' => $userId,
            'updated_by' => $userId,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $contactObj = new Contact();
        $contactObj->saveData($contactData);

        unset($contactObj);

        if ($isModal) {
            $html = "<option value='{$clientId}'>{$input['client_name']}</option>";

            $data = [
                'id' => $clientId,
                'status' => 'ok',
                'html' => $html,
                'modal' => 'div#addClientModal',
                'selector' => 'select#client_id',
                'type' => 'addClientModal',
            ];
        } else {
            $data = [
                'id' => $clientId,
                'status' => 'ok',
                'controller' => 'clients',
            ];
        }

        if ($request->logo) {
            $logo = $request->file('logo');
            $name = $logo->getClientOriginalName().'_'.uniqid().'.'.$logo->getClientOriginalExtension();

            // save in table
            Client::where('id', $clientId)->update(['logo' => $name]);

            // save in folder
            $logo->storeAs(
                'public/client_logos', $name
            );
        }

        return response()->json($data);
    }

    public function show(Client $client)
    {
        $read_only = $this->user->isClient();
        $status = request()->get('status');

        $client = Client::with(['contact', 'user'])->find($client->id);
        $noteTypes = (new NoteType)->toDropdown();
        $notes = Util::getAllNotesByObjectIdAndTypeName($client->id, 'clients');
        $diaries = Util::getAllDiariesByClientId($client->id);
        $properties = (new PropertiesByCompany)->get('client', $client->id, $status);

        return view('clients.show', compact('client', 'noteTypes', 'notes', 'diaries', 'properties', 'read_only'));
    }

    public function edit(Client $client)
    {
        $address = $client->address;
        $currencies = DB::table('currencies')->get();
        $administrators = Administrator::orderBy('name')->select(['id', 'name'])->get();

        return view('clients.edit', compact('client', 'address', 'currencies', 'administrators'));
    }

    public function update(Client $client, ClientRequest $request)
    {
        $input = $request->except(['_method', '_token']);
        $id = Util::saveFormValues($input, $client->id, Client::class, true);

        Util::addNote($id, 'clients', 1, 'Client was Edited');

        $data = [
            'id' => $id,
            'status' => 'ok',
            'controller' => 'clients',
        ];

        if ($request->logo) {
            $logo = $request->file('logo');
            $name = $logo->getClientOriginalName().'_'.uniqid().'.'.$logo->getClientOriginalExtension();
            $path = 'public/client_logos';
            $logoOldName = $client->logo;

            $client->logo = $name;
            $client->save();

            // save in folder
            $logo->storeAs(
                $path, $name
            );

            //delete old image
            Storage::delete($path.'/'.$logoOldName);
        }

        return response()->json($data);
    }

    public function viewAuditRegistersPartial(Client $client)
    {
        $auditRegisters = $client->auditRegisters;
        $claim = new Claim();
        $view = view('claims.includes.audit_registers_partial', compact('auditRegisters', 'claim'))->render();

        return response()->json(compact('view'));
    }
}
