<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Document;
use App\Models\Property;
use App\Models\Supplier;
use App\Traits\PropertySearchAspect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Searchable\Search;

class SearchController extends Controller
{
    const PAGINATION_COUNT = 10;

    private $searchables = [
        'Property',
        //        'Client',
        'Agent',
        'Contact',
        'Document',
        'Supplier',
    ];

    private $searchableFields = [
        'Property' => [
            'property_name',
        ],
        //        'Client' => [
        //            'client_name',
        //            'email',
        //        ],
        'Agent' => [
            'agent_name',
        ],
        'Contact' => [
            'first_name',
            'surname',
            'email',
        ],
        'Document' => [
            'document_name',
        ],
        'Supplier' => [
            'name',
            'email',
        ],
    ];

    public function globalSearch(Request $request)
    {
        $search = $request->search;
        $authClientId = Auth::user()->client_id;

        $properties = Property::where('property_name', 'LIKE', '%'.$search.'%')->where('client_id', $authClientId)
            ->select('id', 'property_name as name', DB::raw("'property' as result_type"), DB::raw("'properties' as route"))
            ->limit(self::PAGINATION_COUNT); // 6 -> 6

//        $clients = Client::where('client_name', 'LIKE', '%'.$search.'%')
//            ->orWhere('email', 'LIKE', '%'.$search.'%')
//            ->select('id', 'client_name as name', DB::raw("'client' as result_type"), DB::raw("'clients' as route"))
//            ->limit(self::PAGINATION_COUNT);

        $agents = Agent::where('agent_name', 'LIKE', '%'.$search.'%')
            ->select('id', 'agent_name as name', DB::raw("'agent' as result_type"), DB::raw("'agents' as route"))
            ->limit(self::PAGINATION_COUNT);

        $contacts = Contact::where(function ($query) use ($search) {
            $query->where(DB::raw("concat(`first_name`,' ',`surname`)"), 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%');
        })
            ->select('id', DB::raw("concat(`first_name`,' ', `surname`) as `name`"), DB::raw("'contact' as result_type"), DB::raw("'contacts' as route"))
            ->limit(self::PAGINATION_COUNT);

        $documents = Document::where('document_name', 'LIKE', '%'.$search.'%')
            ->select('id', 'document_name as name', DB::raw("'document' as result_type"), DB::raw("'documents' as route"))
            ->limit(self::PAGINATION_COUNT);

        $suppliers = Supplier::where(function ($query) use ($search) {
            $query->where('name', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%');
        })
            ->select('id', 'name', DB::raw("'supplier' as result_type"), DB::raw("'suppliers' as route"))
            ->limit(self::PAGINATION_COUNT);

        $properties = $properties//->union($clients)
        ->union($agents)
            ->union($contacts)
            ->union($documents)
            ->union($suppliers)
            ->paginate(self::PAGINATION_COUNT);

        return json_encode(view('utils.globalSearch', compact('properties', 'search'))->toHtml());
    }

    public function search(Request $request)
    {
        $prefix = 'App\Models\\';
        $searchables = $this->searchables;
        $result = [];

        // dd($request->searchBy);

        if ($request->searchBy) {
            if ($request->type) {
                // get certain model for search
                $this->searchables = [$request->type];
            }

            $searchResults = (new Search());

            foreach ($this->searchables as $searchable) {
                if ($searchable === 'Property') {
                    $searchResults = $searchResults->registerAspect(PropertySearchAspect::class);
                } else {
                    $searchResults = $searchResults->registerModel($prefix.$searchable, $this->searchableFields[$searchable]);
                }
            }
            $result = $searchResults->search($request->searchBy);
        }

        return view('search.index', $request->all())
            ->with([
                'searchables' => $searchables,
                'result' => $result,
                'type' => $request->type,
            ]);
    }
}
