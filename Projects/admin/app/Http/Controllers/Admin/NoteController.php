<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequest;
use App\Models\Note;
use App\Models\ObjectType;

class NoteController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function store($entity, NoteRequest $request)
    {
        $input = $request->all();

        $objectTypeObj = new ObjectType();
        $objectType = $objectTypeObj->getObjectByName($input['object_type']);
        unset($objectTypeObj);

        $input['object_type_id'] = $request->object_type_id ?? $objectType->id;
        $input['object_id'] = $request->object_id ?? $entity->id;

        $note = Note::create($input);

        $createdAt = Date::dmYHi($note->created_at);

        $html = "<p>
					{$createdAt} -
					<strong>{$this->user->name}</strong>
					-
					<code>{$note->noteType->name}</code>
					<br>
					{$input['note_text']}
					</p>";

        $data = [
            'id' => $note->id,
            'status' => 'ok',
            'html' => $html,
            'modal' => 'div#noteModal',
            'selector' => 'div#notes-section'.'.'.$this->noteTypes[$input['note_type_id']],
            'note_type' => isset($input['note_type_id']) ? $this->noteTypes[$input['note_type_id']] : '',
        ];

        return response()->json($data);
    }

    private $noteTypes = [
        1 => 'general',
        2 => 'service_charge',
        10 => 'insurance',
        11 => 'utility',
    ];
}
