<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlertRequest;
use App\Models\Alert;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AlertController extends Controller
{
    public function store(AlertRequest $request)
    {
        Alert::create($request->all());

        return response()->json(['refresh' => true], Response::HTTP_OK);
    }

    public function destroy(Request $request)
    {
        Alert::find($request->id)->update(['active' => 0]);

        return response('ok', Response::HTTP_OK);
    }
}
