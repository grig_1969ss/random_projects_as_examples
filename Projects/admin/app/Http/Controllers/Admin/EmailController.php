<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;
use App\Models\DiaryType;
use App\Models\Email;
use App\Services\EmailService;
use App\Traits\HelperTrait;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;

class EmailController extends Controller
{
    use HelperTrait;

    public function create()
    {
        $email = new Email();

        $subject = null;
        $htmlBody = '';

        return view('admin.emails.create', compact('email', 'subject', 'htmlBody'));
    }

    public function store(EmailRequest $request)
    {
        $files = [];

        if ($request->hasFile('attachments')) {
            foreach ($request->file('attachments') as $file) {
                $temp = [
                    'Name' => $file->getClientOriginalName(),
                    'Content' => base64_encode(File::get($file)),
                    'ContentType' => $file->getMimeType(),
                ];

                $files[] = $temp;
            }
        }

        $emailService = new EmailService();

        $emailService->sendEmail($request, $files, auth()->user());

        // set dots true after send from modal
        if ($request->budget_type) {
            $Model = app('App\Models\\'.$request->modal_model);
            $Model::find($request->service_charge)->update([$this->dotButtons[$request->budget_type] => 1]);
        }

        if ($request->ajax()) {
            return response()->json(['refresh' => true], Response::HTTP_OK);
        }

        return redirect(route('inbox.index'));
    }

    private $dotButtons = [
        1 => 'real_estate_updated',
        2 => 'franchisee_updated',
        3 => 'annual_franchisee_updated',
        4 => 'franchisee_updated',
    ];

    public function reply(Email $email)
    {
        $toCc = [];
        if (request()->all && $email->cc) {
            // when multiple emails
            if (gettype(json_decode($email->cc)) == 'array') {
                foreach (json_decode($email->cc) as $cc) {
                    $toCc[] = $cc->Email;
                }
            } else {
                // when 1 email
                $toCc[] = $email->cc;
            }
        }

        $subject = null;
        if (request()->get('type') == 'fw') {
            $subject = 'FW';
        }

        $htmlBody = $this->cleanBody($email->html_body);
        $htmlBody = $this->replaceCidToBase64($htmlBody, $email);

        return view('admin.emails.create', compact('email', 'toCc', 'subject', 'htmlBody'));
    }

    public function archive(Email $email)
    {
        if (request()->get('type') === 'unarchive') {
            $email->archived_at = null;
            $email->save();
        } else {
            $email->archived_at = now();
            $email->save();
        }

        return back();
    }

    public function pdfProperties()
    {
        $properties = auth()->user()->client->property;

        $propertiesDropdown = json_encode(
            view('admin.emails.partials.properties', compact(
                'properties'
            ))->toHtml()
        );

        return response(['propertiesDropdown' => $propertiesDropdown], Response::HTTP_OK);
    }

    public function pdfEmails(Request $request)
    {
        $emails = Email::where('property_id', $request->property)->get();

        $emailsDropdown = json_encode(
            view('admin.emails.partials.emailsDropdown', compact(
                'emails'
            ))->toHtml()
        );

        return response(['emailsDropdown' => $emailsDropdown], Response::HTTP_OK);
    }

    public function generatePreviewFile(Request $request)
    {
        $pathToFolder = storage_path('app/emails_as_pdf/');

        // create folder if missing, because PDF package below can not create it
        if (! File::exists($pathToFolder)) {
            File::makeDirectory($pathToFolder);
        }

        $email = Email::find($request->email);
        $name = $clean_code = preg_replace('/[^\w]/', '_', $email->subject);
        $name = $email->id.'_'.mb_substr($name, 0, 20).'..._'.'.pdf';

        PDF::loadHTML($email->html_body)
            ->setPaper('a4', 'landscape')
            ->setOptions([
                'margin-top', 0,
                'margin-left', 12.7,
                'margin-right', 12.7,
                'margin-bottom', 6,
            ])
            ->save($pathToFolder.$name);

        $data = [
            'route' => 'inbox/pdf-preview/'.$name,
            'name' => $name,
        ];

        return response($data, Response::HTTP_OK);
    }

    public function emailAddDiarise(Request $request)
    {
        $email = Email::find($request->email);
        $diaryType = DiaryType::where('name', 'Email')->first()->id;

        $input['property_id'] = $email->property_id;
        $input['diary_title'] = 'Email';
        $input['status'] = 0;
        $input['diary_type_id'] = $diaryType;
        $input['comments'] = $request->diarise_note;
        $input['date'] = Carbon::parse($request->diary_date)->format('Y-m-d H:i:s');

        if (! isset($request->update)) {
            $diaryId = $email->diaries()->create($input)->id;
        } else {
            $diary = $email->diaries()->first();
            $diary->update($input);
            $diaryId = $diary->id;
        }

        $email->update(['diary_id' => $diaryId]);

        $data = [
            'id' => $email->id,
            'status' => 'ok',
            'controller' => 'inbox',
        ];

        return response($data, Response::HTTP_OK);
    }
}
