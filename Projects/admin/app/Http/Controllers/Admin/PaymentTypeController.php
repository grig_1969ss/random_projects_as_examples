<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentTypeRequest;
use App\Models\PaymentType;

class PaymentTypeController extends Controller
{
    public function create()
    {
        $paymentType = new PaymentType();
        $view = view('settings.payment_types.edit', compact('paymentType'))->render();
        $title = 'Add Approval Type';

        return response()->json(compact('view', 'title'));
    }

    public function store(PaymentTypeRequest $request)
    {
        PaymentType::create($request->all());

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }

    public function edit(PaymentType $paymentType)
    {
        $view = view('settings.payment_types.edit', compact('paymentType'))->render();
        $title = 'Edit Payment Type';

        return response()->json(compact('view', 'title'));
    }

    public function update(PaymentType $paymentType, PaymentTypeRequest $request)
    {
        $paymentType->update($request->all());

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }

    public function delete(PaymentType $paymentType)
    {
        $view = view('settings.payment_types.delete', compact('paymentType'))->render();
        $title = 'Are you sure you want to delete this Approval Type?';

        return response()->json(compact('view', 'title'));
    }

    public function destroy(PaymentType $paymentType)
    {
        $paymentType->delete();

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }
}
