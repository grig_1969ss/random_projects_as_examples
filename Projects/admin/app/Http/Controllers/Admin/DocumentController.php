<?php

namespace App\Http\Controllers\Admin;

use App\Events\DocumentUpload;
use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentRequest;
use App\Models\Document;
use App\Models\DocumentType;
use App\Models\Email;
use App\Models\Payment;
use App\Models\Property;
use App\Models\ServiceCharge;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    /**
     * @param Property|ServiceCharge $entity
     * @param DocumentRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store($entity, DocumentRequest $request)
    {
        $input = $request->all();
        $timestamp = time();

        $request->file('document_file')->storeAs($timestamp, $request->get('document_file'), ['disk' => 's3']);

        $input['document_name'] = $request->get('document_file');
        $input['document_file'] = $timestamp.'/'.$request->get('document_file');
        $input['content_type'] = $request->file('document_file')->getMimeType();

        event(new DocumentUpload($entity, $request));

        $document = $entity->documents()->create($input);

        $objectType = ($entity instanceof Payment) ? 'payments' : 'properties';

        $entity->addDocumentNote($document, Arr::get($input, 'comments'), $objectType);

        $data = [
            'id' => $entity->property->id,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function show(Document $document)
    {
        return Storage::disk('s3')->download($document->document_file);
    }

    public function destroy(Document $document)
    {
        $document->archive();

        Storage::disk('s3')->delete($document->document_file);

        if ($document->docable->getMorphClass() == Property::class) {
            return redirect(route($document->docable->showRoute, $document->docable));
        } else {
            $property = $document->docable->property;

            return redirect(route($property->showRoute, $property));
        }
    }

    public function docsDelete(Request $request)
    {
        Document::find($request->deleting_document)->update(['archived' => 1]);

        return back();
    }

    public function ajaxCreate($entity)
    {
        $documentTypes = DocumentType::orderBy('name')->select(['id', 'name'])->get();
        $view = view('documents.includes.edit', compact('entity', 'documentTypes'))->render();
        $title = 'Upload Document';

        return response()->json(compact('view', 'title'));
    }

    public function inboxPdfPreview($name)
    {
        $path = 'emails_as_pdf/'.$name;

        return Storage::download($path);
    }

    public function markAsUnread(Email $email)
    {
        $email->update(['read_at' => null]);

        return redirect('inbox');
    }
}
