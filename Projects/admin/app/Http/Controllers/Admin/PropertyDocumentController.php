<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DocumentRequest;
use App\Models\Insurance;
use App\Models\Property;
use App\Models\ServiceCharge;

class PropertyDocumentController extends DocumentController
{
    /**
     * @param Property|ServiceCharge|Insurance $entity
     * @param DocumentRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store($entity, DocumentRequest $request)
    {
        $input = $request->all();

        $timestamp = time();

        $request->file('document_file')->storeAs($timestamp, $request->get('document_file'), ['disk' => 's3']);

        $input['document_name'] = $request->get('document_file');
        $input['document_file'] = $timestamp.'/'.$request->get('document_file');
        $input['content_type'] = $request->file('document_file')->getMimeType();

        $document = $entity->document()->create($input);

        $entity->addDocumentNote($document, (isset($input['comments']) ? $input['comments'] : null));

        $read_only = $this->user->read_only;
        $html = view('properties.includes.single_document', compact('document', 'read_only'))->render();

        $data = [
            'id' => $entity->id,
            'status' => 'ok',
            'modal' => 'div#documentModal',
            'selector' => 'div#documents-section',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }
}
