<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Claim;
use App\Models\ClaimType;

class ClaimTypeController extends Controller
{
    public function viewSubtypesPartial(ClaimType $claimType)
    {
        $claimSubtypes = $claimType->claimSubtypes;
        $claim = new Claim();
        $view = view('claims.includes.claim_subtypes_partial', compact('claimSubtypes', 'claim'))->render();

        return response()->json(compact('view'));
    }
}
