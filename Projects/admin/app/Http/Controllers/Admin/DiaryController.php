<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\DiaryRequest;
use App\Models\Diary;
use Carbon\Carbon;
use Illuminate\Http\Response;

class DiaryController extends Controller
{
    public function index($id = null)
    {
        $diaries = Diary::getAllActiveDiaries($id);
        $diaries = Util::sortDiariesList($diaries);

        return view('diary.index', compact('diaries'));
    }

    public function store(DiaryRequest $request)
    {
        $model = app('App\Models\\'.$request->attach_to_type[0]);
        $entity = $model->find($request->note_category);

        $input['comments'] = $request->alert_comments;
        $input['date'] = Carbon::parse($request->datetime)->format('Y-m-d H:i:s');

        $entity->diaries()->create($input);

        $data = [
            'id' => $request->property,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response($data, Response::HTTP_OK);
    }
}
