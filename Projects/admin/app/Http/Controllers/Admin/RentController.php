<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Http\Requests\RentRequest;
use App\Models\Property;
use App\Models\Rent;

class RentController extends Controller
{
    public function create(Property $property)
    {
        $rent = new Rent();
        $action = ['properties.rents.store', $property];
        $view = view('partials.rent-form', compact('rent', 'action', 'property'))->render();

        $data = [
            'body'   => $view,
            'header' => 'Add Rent',
        ];

        return response()->json($data);
    }

    public function store(Property $property, RentRequest $request)
    {
        $input = $request->all();
        $input['exp_rent_date'] = Date::toMysql($input['exp_rent_date']);

        $property->rent()->create($input);

        $data = [
            'url' => route('properties.show', $property),
        ];

        $request->session()->flash('success', 'Rent Added.');

        return response()->json($data);
    }

    public function edit(Rent $rent)
    {
        $property = $rent->property;
        $action = ['rents.update', $rent];
        $view = view('partials.rent-form', compact('rent', 'action', 'property'))->render();

        $data = [
            'body'   => $view,
            'header' => 'Edit Rent',
        ];

        return response()->json($data);
    }

    public function update(Rent $rent, RentRequest $request)
    {
        $input = $request->all();
        $input['exp_rent_date'] = Date::toMysql($input['exp_rent_date']);

        $rent->update($input);

        $data = [
            'url' => route('properties.show', $rent->property),
        ];

        // Set success message
        $request->session()->flash('success', 'Rent Updated.');

        return response()->json($data);
    }

    public function destroy(Rent $rent)
    {
        $rent->update([
            'archived' => true,
        ]);

        return redirect(route('properties.show', $rent->property))->with('success', 'Rent Deleted.');
    }
}
