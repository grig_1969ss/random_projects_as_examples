<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentTypeRequest;
use App\Models\User;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    use CreateAndUpdateTrait;

    private $defaultType = 'DocumentType';

    protected $dynamicRepository;

    public function __construct()
    {
        if ($model = request()->get('m')) {
            $this->modelRepository($model);
        }
    }

    // at first we will return doc typesi
    public function index(Request $request)
    {
        $data = $this->modelColumnRows($request);

        return view('admin.account.settings.index', compact('data'));
    }

    // at first we will return doc types
    public function indexTable(Request $request)
    {
        $data = $this->modelColumnRows($request);

        return json_encode(view('admin.account.settings.indexTable', compact('data'))->toHtml());
    }

    public function showType($id)
    {
        $model = request()->m ?? $this->defaultType;

        $this->modelRepository($model);

        $type = $this->dynamicRepository->find($id);

        return json_encode(view('admin.account.settings.edit', compact('type', 'model'))->toHtml());
    }

    public function createDocType(DocumentTypeRequest $request)
    {
        // this one we show with js hidden form
    }

    public function storeType(Request $request)
    {
        $requestArray = $this->validateRequest($request);

        $request = array_merge($requestArray, ['created_by' => auth()->user()->id]);

        $request = $this->specificFields($request);

        $model = $this->dynamicRepository->create($request);

        $this->attachRelations($request, $model);

        return back();
    }

    public function updateType(Request $request)
    {
        $requestArray = $this->validateRequest($request);

        // also set who has updated
        $request = array_merge($requestArray, ['updated_by' => auth()->user()->id]);

        $request = $this->specificFields($request);

        $model = $this->dynamicRepository->update($request, $request['id']);

        $this->attachRelations($request, $model);

        return back();
    }

    public function destroyType(Request $request)
    {
        $requestId = $request->id;

        $this->dynamicRepository->update(['archived' => true], $requestId);

        $this->dynamicRepository->delete($requestId);

        return back();
    }

    public function userDetails($id)
    {
        $user = User::findOrFail($id);

        $data = $this->modelColumnRows(request()->merge(['m' => 'User']), $id);

        return view('admin.account.settings.user.details', compact('user', 'data'));
    }
}
