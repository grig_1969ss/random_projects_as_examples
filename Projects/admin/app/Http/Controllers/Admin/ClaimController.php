<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClaimRequest;
use App\Models\Auditor;
use App\Models\Claim;
use App\Models\ClaimStatus;
use App\Models\ClaimType;
use App\Models\Client;
use App\Models\Currency;
use App\Models\NoteType;

class ClaimController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->read_only;
        $claims = Claim::all();

        return view('claims.index', compact('claims', 'read_only'));
    }

    public function create()
    {
        $claim = new Claim();
        $clients = Client::select(['id', 'client_name'])->get();
        $auditRegisters = collect();
        $auditors = Auditor::select(['id', 'name'])->get();
        $claimTypes = ClaimType::select(['id', 'name'])->get();
        $claimSubtypes = collect();
        $currencies = Currency::select(['id', 'name'])->get();
        $claimStatuses = ClaimStatus::select(['id', 'name'])->get();
        $claimNumber = Claim::generateClaimNumber();

        return view('claims.edit', compact(
            'claim',
            'clients',
            'auditRegisters',
            'auditors',
            'claimTypes',
            'claimSubtypes',
            'currencies',
            'claimStatuses',
            'claimNumber'
        ));
    }

    public function store(ClaimRequest $request)
    {
        $input = $request->all();
        $input['invoice_date'] = Date::toMysql($input['invoice_date']);
        $input['client_date'] = Date::toMysql($input['client_date']);
        $claim = Claim::create($input);

        Util::addNote($claim->id, 'claims', 9, 'Claim was Added');

        $data = [
            'id'         => $claim->id,
            'status'     => 'ok',
            'controller' => 'claims',
        ];

        return response()->json($data);
    }

    public function show(Claim $claim)
    {
        $read_only = $this->user->read_only;
        $noteTypes = (new NoteType)->toDropdown();
        $notes = Util::getAllNotesByObjectIdAndTypeName($claim->id, 'claims');

        return view('claims.show', compact('claim', 'read_only', 'noteTypes', 'notes'));
    }

    public function edit(Claim $claim)
    {
        $clients = Client::select(['id', 'client_name'])->get();
        $auditors = Auditor::select(['id', 'name'])->get();
        $claimTypes = ClaimType::select(['id', 'name'])->get();
        $currencies = Currency::select(['id', 'name'])->get();
        $claimStatuses = ClaimStatus::select(['id', 'name'])->get();
        $claimNumber = Claim::generateClaimNumber();

        if ($claim->client) {
            $auditRegisters = $claim->client->auditRegisters;
        } else {
            $auditRegisters = collect();
        }

        if ($claim->claimType) {
            $claimSubtypes = $claim->claimType->claimSubtypes;
        } else {
            $claimSubtypes = collect();
        }

        return view('claims.edit', compact(
            'claim',
            'clients',
            'auditRegisters',
            'auditors',
            'claimTypes',
            'claimSubtypes',
            'currencies',
            'claimStatuses',
            'claimNumber'
        ));
    }

    public function update(Claim $claim, ClaimRequest $request)
    {
        $input = $request->all();
        $input['invoice_date'] = Date::toMysql($input['invoice_date']);
        $input['client_date'] = Date::toMysql($input['client_date']);

        $claim->update($input);

        Util::addNote($claim->id, 'claims', 9, 'Claim was Edited');

        $data = [
            'id'         => $claim->id,
            'status'     => 'ok',
            'controller' => 'claims',
        ];

        return response()->json($data);
    }
}
