<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttachmentDetails;

class AttachmentController extends Controller
{
    public function download($id)
    {
        $attachment = AttachmentDetails::where('id', $id)->first();

        $path = public_path($attachment->name);
        $contents = base64_decode($attachment->content);

        //store file temporarily
        file_put_contents($path, $contents);

        //download file and delete it
        return response()->download($path)->deleteFileAfterSend(true);
    }
}
