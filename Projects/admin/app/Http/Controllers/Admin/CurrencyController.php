<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CurrenciesRequest;
use App\Models\Currency;

class CurrencyController extends Controller
{
    public function create()
    {
        $currency = new Currency();
        $view = view('settings.currencies.edit', compact('currency'))->render();
        $title = 'Add Currency';

        return response()->json(compact('view', 'title'));
    }

    public function store(CurrenciesRequest $request)
    {
        Currency::create($request->all());

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }

    public function edit(Currency $currency)
    {
        $view = view('settings.currencies.edit', compact('currency'))->render();
        $title = 'Edit Currency';

        return response()->json(compact('view', 'title'));
    }

    public function update(Currency $currency, CurrenciesRequest $request)
    {
        $currency->update($request->all());

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }

    public function delete(Currency $currency)
    {
        $view = view('settings.currencies.delete', compact('currency'))->render();
        $title = 'Are you sure you want to delete this Currency?';

        return response()->json(compact('view', 'title'));
    }

    public function destroy(Currency $currency)
    {
        $currency->delete();

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }
}
