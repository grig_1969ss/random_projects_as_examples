<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentRequest;
use App\Models\Address;
use App\Models\Agent;
use App\Models\Contact;
use App\Models\NoteType;
use App\Queries\PropertiesByCompany;

class AgentController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->isClient();
        $agents = Agent::getAgents();

        return view('agents.index', compact('agents', 'read_only'));
    }

    public function create()
    {
        $agent = new Agent();
        $address = new Address();

        return view('agents.edit', compact('agent', 'address'));
    }

    public function store(AgentRequest $request)
    {
        $input = $request->except('_token');
        $userId = $this->user->id;

        if (isset($input['isModal'])) {
            $isModal = $input['isModal'];

            unset($input['isModal']);
        } else {
            $isModal = false;
        }

        $contactFirstname = $input['contact_firstname'];

        unset($input['contact_firstname']);

        $contactSurname = $input['contact_surname'];

        unset($input['contact_surname']);

        $agentId = Util::saveFormValues($input, null, Agent::class, true);

        Util::addNote(
            $agentId,
            'agents',
            1,
            'Managing Agent was Added'
        );

        // save contact name
        $contactData = [
            'first_name'       => $contactFirstname,
            'surname'          => $contactSurname,
            'contactable_id'   => $agentId,
            'contactable_type' => 'App\\Models\\Agent',
            'telephone'        => $input['telephone'],
            'fax'              => $input['fax'],
            'email'            => $input['email'],
            'created_by'       => $userId,
            'updated_by'       => $userId,
            'created_at'       => date('Y-m-d H:i:s'),
            'updated_at'       => date('Y-m-d H:i:s'),
        ];

        $contactObj = new Contact();

        $contactObj->saveData($contactData);

        unset($contactObj);

        if ($isModal) {
            $html = "<option value='{$agentId}'>{$input['agent_name']}</option>";

            $data = [
                'id'       => $agentId,
                'status'   => 'ok',
                'html'     => $html,
                'modal'    => 'div#addAgentModal',
                'selector' => 'select#agent_id',
                'type'     => 'addAgentModal',
            ];
        } else {
            $data = [
                'id'         => $agentId,
                'status'     => 'ok',
                'controller' => 'agents',
            ];
        }

        return response()->json($data);
    }

    public function show(Agent $agent)
    {
        $status = request()->get('status');
        $read_only = $this->user->isClient();

        $agent = Agent::with([
            'property',
            'property.address',
            'property.client',
            'property.propertyStatus',
            'contacts',
            'user',
        ])->find($agent->id);

        $properties = (new PropertiesByCompany)->get('agent', $agent->id, $status);
        $noteTypes = (new NoteType)->toDropdown();
        $notes = Util::getAllNotesByObjectIdAndTypeName($agent->id, 'agents');

        return view('agents.show', compact('agent', 'noteTypes', 'notes', 'properties', 'read_only'));
    }

    public function edit(Agent $agent)
    {
        $address = $agent->address;

        return view('agents.edit', compact('agent', 'address'));
    }

    public function update(Agent $agent, AgentRequest $request)
    {
        $input = $request->except(['_token', '_method']);
        $agentId = Util::saveFormValues($input, $agent->id, Agent::class, true);

        Util::addNote(
            $agentId,
            'agents',
            1,
            'Agent was Edited'
        );

        $data = [
            'id'         => $agentId,
            'status'     => 'ok',
            'controller' => 'agents',
        ];

        return response()->json($data);
    }

    public function getContactsSelectPartial(Agent $agent)
    {
        $contacts = $agent->contacts;
        $contactId = null;
        $elementName = 'agent_contact_id';
        $view = view('partials.edit_contacts_partial', compact('contacts', 'contactId', 'elementName'))->render();

        return response()->json(compact('view'));
    }
}
