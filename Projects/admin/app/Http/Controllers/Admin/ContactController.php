<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Models\Agent;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Landlord;
use App\Models\NoteType;

class ContactController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $contacts = Contact::getAll();

        return view('contacts.index', compact('contacts'));
    }

    /**
     * @param Agent|Client|Landlord $entity
     *
     * @return string
     */
    public function create($entity)
    {
        $contact = new Contact();

        return view('contacts.edit', compact('contact', 'entity'));
    }

    /**
     * @param Agent|Client|Landlord $entity
     * @param ContactRequest $request
     * @return mixed
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store($entity, ContactRequest $request)
    {
        if ($entity->contact) {
            $contact = $entity->contact()->create($request->all());
        } else {
            $contact = $entity->contacts()->create($request->all());
        }

        Util::addNote($contact->id, 'contacts', 1, 'Contact was Added');

        if ($request->get('is_ajax')) {
            $contacts = $entity->contacts;
            $contactId = $contact->id;
            $elementName = $entity->contactElementName;
            $view = view('partials.edit_contacts_partial', compact('contacts', 'contactId', 'elementName'))->render();
            $onSuccess = 'addContact';
            $onSuccessParams = [$view, "select#{$elementName}"];

            return response()->json(compact('view', 'onSuccess', 'onSuccessParams'));
        }

        $data = [
            'id' => $contact->id,
            'status' => 'ok',
            'controller' => 'contacts',
        ];

        return response()->json($data);
    }

    public function show(Contact $contact)
    {
        $read_only = $this->user->isClient();
        $noteTypes = (new NoteType)->toDropdown();
        $notes = Util::getAllNotesByObjectIdAndTypeName($contact->id, 'contacts');

        return view('contacts.show', compact('contact', 'noteTypes', 'notes', 'read_only'));
    }

    public function edit(Contact $contact)
    {
        return view('contacts.edit', compact('contact'));
    }

    public function update(ContactRequest $request, Contact $contact)
    {
        $contact->update($request->all());

        Util::addNote($contact->id, 'contacts', 1, 'Contact was Edited');

        $data = [
            'id' => $contact->id,
            'status' => 'ok',
            'controller' => 'contacts',
        ];

        return response()->json($data);
    }

    public function ajaxCreate($entity)
    {
        $route = [$entity->createContactRoute, $entity];
        $restMethod = 'POST';
        $contact = new Contact();
        $isAjax = 1;
        $view = view('contacts.includes.form_inner', compact('route', 'restMethod', 'contact', 'isAjax'))->render();
        $title = 'Add Contact';

        return response()->json(compact('view', 'title'));
    }
}
