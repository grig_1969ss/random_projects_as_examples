<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceChargeRequest;
use App\Models\ApportionmentType;
use App\Models\Diary;
use App\Models\Note;
use App\Models\Property;
use App\Models\ServiceCharge;
use App\Models\ServiceChargeStatus;
use App\Traits\HelperTrait;
use Illuminate\Support\Facades\Log;

class ServiceChargeController extends Controller
{
    use HelperTrait;

    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function create(Property $property = null)
    {
        $data = $this->serviceChargeCreate();

        $serviceCharge = $data['serviceCharge'];
        $apportionmentTypesDropdown = $data['apportionmentTypesDropdown'];
        $serviceChargeStatusesDropdown = $data['serviceChargeStatusesDropdown'];

        return view('service_charges.edit', compact(
            'property',
            'serviceCharge',
            'apportionmentTypesDropdown',
            'serviceChargeStatusesDropdown'
        ));
    }

    public function store(Property $property, ServiceChargeRequest $request)
    {
        $input = $request->except('_token');

        $input['year_end'] = Date::toMysql($input['year_end']);
        $input['service_charge_commencement_date'] = Date::toMysql($input['service_charge_commencement_date']);
        $input['date_invoiced'] = Date::toMysql($input['date_invoiced']);
        $input['date_received'] = Date::toMysql($input['date_received']);

        if (! empty($input['diary_date'])) {
            $input['diary_date'] = Date::toMysql($input['diary_date']);

            $diary = Diary::create([
                'property_id' => $property->id,
                'diary_type_id' => 1,
                'diary_title' => 'Service Charge',
                'date' => $input['diary_date'],
                'status' => 0,
            ]);

            $input['diary_id'] = $diary->id;
        }

        unset($input['diary_date']);

        if (! isset($input['invoiced'])) {
            $input['invoiced'] = 0;
        }
        if (! isset($input['exclusions'])) {
            $input['exclusions'] = 0;
        }
        if (! isset($input['confirmed'])) {
            $input['confirmed'] = 0;
        }
        if (! isset($input['vat'])) {
            $input['vat'] = 0;
        }

        $noteText = $input['notes'];

        $sc = $property->serviceCharge()->create($input);

        Util::addNote(
            $property->id,
            'properties',
            2,
            $noteText != '' ? $noteText : 'Service Charge was Added'
        );

        $data = [
            'id' => $property->id,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        $this->selectData($property, $sc->id, $request->modal_save, $data, 'ServiceCharge');

        return response()->json($data);
    }

    public function edit(ServiceCharge $serviceCharge)
    {
        $apportionmentTypesDropdown = (new ApportionmentType)->toDropdown();
        $serviceChargeStatusesDropdown = (new ServiceChargeStatus)->toDropdown();
        $property = $serviceCharge->property;

        return view(
            'service_charges.edit',
            compact('serviceCharge', 'apportionmentTypesDropdown', 'serviceChargeStatusesDropdown', 'property')
        );
    }

    public function update(ServiceCharge $serviceCharge, ServiceChargeRequest $request)
    {
        $input = $request->except(['_token', '_method']);

        $input['year_end'] = Date::toMysql($input['year_end']);
        $input['service_charge_commencement_date'] = Date::toMysql($input['service_charge_commencement_date']);
        $input['date_invoiced'] = Date::toMysql($input['date_invoiced']);
        $input['date_received'] = Date::toMysql($input['date_received']);
        $input['diary_date'] = Date::toMysql($input['diary_date']);

        if (! isset($input['invoiced'])) {
            $input['invoiced'] = 0;
        }
        if (! isset($input['exclusions'])) {
            $input['exclusions'] = 0;
        }
        if (! isset($input['confirmed'])) {
            $input['confirmed'] = 0;
        }
        if (! isset($input['vat'])) {
            $input['vat'] = 0;
        }

        if (isset($input['diary_id'])) {
            $diary = Diary::find($input['diary_id']);

            $diary->update([
                'date' => $input['diary_date'],
                'updated_by' => $this->user->id,
            ]);
        } elseif (isset($input['diary_date'])) {
            Diary::create([
                'property_id' => $serviceCharge->property_id,
                'diary_type_id' => 1,
                'diary_title' => 'Service Charge',
                'date' => $input['diary_date'],
                'status' => 0,
                'created_by' => $this->user->id,
                'updated_by' => $this->user->id,
            ]);
        }

        unset($input['diary_date']);

        $serviceCharge->update($input);

        $noteText = $input['notes'] != '' ? $input['notes'] : 'Service Charge was Edited';

        if (! Note::noteExists($serviceCharge->property_id, $noteText)) {
            Util::addNote($serviceCharge->property_id, 'properties', 2, $noteText);
        }

        $data = [
            'id' => $serviceCharge->property_id,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function destroy(ServiceCharge $serviceCharge)
    {
        $property = $serviceCharge->property;

        try {
            $serviceCharge->delete();
        } catch (\Exception $e) {
            Log::debug($e);

            return;
        }

        return redirect(route('properties.show', $property));
    }
}
