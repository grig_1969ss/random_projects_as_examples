<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Currency;
use App\Helpers\Date;
use App\Helpers\Reports\CommonTable;
use App\Helpers\Reports\Report;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Document;
use App\Models\Property;
use App\Models\Rent;
use App\Repositories\PropertyRepository;
use App\Services\ReportService;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{
    protected $service;
    protected $user;

    public function __construct()
    {
        $this->service = new ReportService();

        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function getMasterSpreadsheet()
    {
        $this->service->getMasterSpreadsheet();
    }

    public function getRentReview(Request $request)
    {
        $inputs = $request->all();
        $data = $this->service->getRentReview($inputs);
        [$properties, $order, $viewDate] = $data;

        if ($request->ajax()) {
            $markup = view('reports.includes.rent_review_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup, 'order' => $order]);
        } else {
            return view('reports.rent_review', compact('properties', 'order', 'viewDate'));
        }
    }

    public function getCommonTable(Request $request)
    {
        $constants = CommonTable::getConstants($request->type);
        $headers = CommonTable::getHeaders($request->type);
        $method = CommonTable::getData($request->type);
        $dateColumn = CommonTable::getDateColumn($request->type);

        $data = call_user_func([$this, $method]);
        $data = CommonTable::filterByDate($data, $dateColumn, $request->all());
        $data = CommonTable::filterSurveyorId($data, $request->surveyor_id);

        $data = json_encode($data->values()->toArray());

        $surveyors = CommonTable::getSurveyors();

        return view('reports.common_table')->with([
            'constants' => $constants,
            'headers' => json_encode($headers),
            'data' => $data,
            'surveyors' => $surveyors,
        ]);
    }

    public function getLeaseExpiry(Request $request)
    {
        $inputs = $request->all();
        $order = isset($inputs['orderCol']) ? $inputs['orderCol'] : 'lease_expiry_date';

        // if icon is clicked set the month interval to 12
        if (isset($inputs['icon']) && $inputs['icon'] == 1) {
            $interval = 12;
            $date = date('Y-m-d');
            $viewDate = date('d/m/Y');
        } else {
            $interval = 0;
            $date = isset($inputs['date']) ? Date::toMysql($inputs['date']) : null;
            $viewDate = '';
        }

        if (array_key_exists('dateFrom', $inputs) && $inputs['dateFrom'] != '') :
            $inputs['dateFrom'] = DateTime::createFromFormat('d/m/Y', $inputs['dateFrom'])->format('Y-m-d'); else :
            $inputs['dateFrom'] = null;
        endif;

        if (array_key_exists('dateTo', $inputs) && $inputs['dateTo'] != '') :
            $inputs['dateTo'] = DateTime::createFromFormat('d/m/Y', $inputs['dateTo'])->format('Y-m-d'); else :
            $inputs['dateTo'] = null;
        endif;

        // get all properties whose leases are expiring
        $properties = Property::getAllPropertiesDueLeaseExpiry($order, $inputs['dateFrom'], $inputs['dateTo']);

        $properties = self::getPropertiesRentAndSOC($properties);

        if ($request->ajax()) {
            $markup = view('reports.includes.lease_expiry_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        }

        //dd($properties->toArray());

        return view('reports.lease_expiry', compact('properties', 'order', 'viewDate'));
    }

    public function getRateableValue(Request $request)
    {
        $inputs = $request->all();

        $viewDate = Report::setViewDate($inputs);
        $order = Report::addSortQuery($inputs, 'rateable_value');
        $inputs = Report::formatQueryDate($inputs, 'dateFrom');
        $inputs = Report::formatQueryDate($inputs, 'dateTo');

        $property = new PropertyRepository();
        $properties = $property->getAllPropertiesWithRateableValue($order, $inputs['dateFrom'], $inputs['dateTo']);

        $properties = self::getPropertiesRentAndSOC($properties);

        if ($request->ajax()) {
            $markup = view('reports.includes.rateable_value_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        }

        return view('reports.rateable_value', compact('properties', 'order', 'viewDate'));
    }

    public function getBreakOption(Request $request)
    {
        $inputs = $request->all();

        $viewDate = Report::setViewDate($inputs);
        $order = Report::addSortQuery($inputs, 'break_option_date');
        $inputs = Report::formatQueryDate($inputs, 'dateFrom');
        $inputs = Report::formatQueryDate($inputs, 'dateTo');

        $properties = (new PropertyRepository())->getAllPropertiesBreakOptionReview($order, $inputs['dateFrom'], $inputs['dateTo']);

        $properties = self::getPropertiesRentAndSOC($properties);

        if ($request->ajax()) {
            $markup = view('reports.includes.break_option_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup, 'order' => $order]);
        }

        return view('reports.break_option', compact('properties', 'order', 'viewDate'));
    }

    public function getCondition(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'property_name');

        $properties = (new PropertyRepository())->getAllPropertiesWithSchedule($order);

        if ($request->ajax()) {
            $markup = view('reports.includes.condition_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup, 'order' => $order]);
        }

        return view('reports.condition', compact('properties', 'order'));
    }

    public function getDocumentChecklist(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'property_name');
        $date = Report::addDateQuery($inputs);

        $properties = (new PropertyRepository())->getAllPropertiesDocumentChecklist($order, $date);

        if ($request->ajax()) {
            $markup = view('reports.includes.document_checklist_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        }

        return view('reports.document_checklist', compact('properties', 'order'));
    }

    public function getMaintenance()
    {
        $properties = (new PropertyRepository())->getAllPropertiesWithActiveMaintenance();

        return view('reports.maintenance', compact('properties'));
    }

    public function getRentalLiability(Request $request)
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');

        $properties = Property::getRentalLiabilityAll($order);

        if ($request->ajax()) {
            $markup = view('reports.includes.rental_liability_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        }

        return view('reports.rental_liability', compact('properties', 'order'));
    }

    public function getBudget(Request $request)
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $date = Report::addDateQuery($inputs);

        $properties = Property::getAllPropertiesForServiceChargeReport('Budget', $order, $date);

        if ($request->ajax()) {
            $markup = view('reports.includes.annual_service_charge_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        }

        $type = 'Budget';

        return view('reports.annual_service_charge', compact('properties', 'type', 'order'));
    }

    public function getActual(Request $request)
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $date = Report::addDateQuery($inputs);

        $properties = Property::getAllPropertiesForServiceChargeReport('Actual', $order, $date);

        if ($request->ajax()) {
            $markup = view('reports.includes.annual_service_charge_table', compact('properties', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        } else {
            $type = 'Actual';

            return view('reports.annual_service_charge', compact('properties', 'type', 'order'));
        }
    }

    public function getMissing(Request $request, $type = 'Budget')
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $type = strtolower($type) == 'budget' ? 'Budget' : 'Actual';
        $date = Report::addDateQuery($inputs);

        $properties = (new PropertyRepository())->getAllPropertiesMissingPayments($type, $order, $date);

        if ($request->ajax()) {
            $markup = view(
                'reports.includes.missing_payment_table',
                [
                    'properties' => $properties,
                    'type' => "{$type}",
                    'order' => $order,
                ]
            )->render();

            return response()->json(['result' => 'OK', 'params' => $inputs, 'markup' => $markup]);
        }

        return view('reports.missing_payments')->with(
            [
                'properties' => $properties,
                'type' => "{$type}",
                'order' => $order,
            ]
        );
    }

    public function allRecentChargesForPropertiesReport()
    {
        $serviceCharges = Property::getAllRecentServiceCharges();

        return view('reports.recent_service_charges', compact('serviceCharges'));
    }

    public function quarterlyServiceChargeReport(Request $request)
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $date = isset($inputs['date'])
            ? Date::toMysql(date('d/m/Y', strtotime('31 Dec '.$inputs['date'])))
            : Date::toMysql(date('d/m/Y', strtotime('31 Dec '.date('Y', strtotime('+1 Year')))));

        $lastYearDate = Date::toMysql(date('d/m/Y', strtotime($date.'-1 Year')));

        $yearsRange = Report::yearsRange();

        $properties = Property::getAllPropertiesForQuarterServiceChargeReport('Budget', $order, $date);

        $quarter_total = $this->getQuarterTotal($properties);

        $properties = $this->mergeProperties($properties, $properties);

        if ($request->ajax()) {
            $last_year_quarter_total = $quarter_total;

            $markup = view(
                'report.quarterly-service-charge-table',
                compact('properties', 'order', 'quarter_total', 'last_year_quarter_total')
            )->render();

            return response()->json([
                'result' => 'OK',
                'params' => $inputs,
                'markup' => $markup,
            ]);
        }

        return view('reports.quarterly_service_charge')->with(
            [
                'properties' => $properties,
                'type' => 'onaccount',
                'order' => $order,
                'yearsRange' => $yearsRange,
                'quarter_total' => $quarter_total,
                'last_year_quarter_total' => $quarter_total,
            ]
        );
    }

    public function getDocumentTables(Request $request)
    {
        $documentObj = new Document();
        $documentTables = $documentObj->getAllDocumentsByPropertyId($request->get('propertyId'));

        unset($documentObj);

        $markup = view('partials._show-documents-modal', compact('documentTables'))->render();

        return response()->json([
            'status' => 'ok',
            'markup' => $markup,
        ]);
    }

    public static function getPropertiesRentAndSOC($properties)
    {
        $rents = Rent::getCurrentRents();
        $schedules = Document::getDocumentsByType(7);

        foreach ($properties as $property) {
            foreach ($rents as $rent) {
                if ($property->id == $rent->property_id) {
                    $property->current_rent = $rent->rent;
                }
            }

            foreach ($schedules as $schedule) {
                if ($property->id == $schedule->property_id) {
                    $property->schedule_of_condition_link = Storage::url($schedule->document_file);
                    $property->schedule_of_condition = $schedule->document_name;
                }
            }

            // check they are set if not then show -
            $property->current_rent = isset($property->current_rent) ? Currency::gbp($property->current_rent) : '-';
            $property->schedule_of_condition_link = isset($property->schedule_of_condition_link) ? $property->schedule_of_condition_link : "/properties/{$property->id}";
            $property->schedule_of_condition = isset($property->schedule_of_condition) ? $property->schedule_of_condition : '-';
            $property->rateable_value = $property->rateable_value ? Currency::gbp($property->rateable_value) : '-';
        }

        return $properties;
    }

    public function getApprovalsDataTAble()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        $data = $this->service->getApprovals([]);

        return $data['approvals']->map(function ($approval) use ($currency) {
            $approval->formatted_amount = $currency.' '.number_format($approval->amount, 2);
            $approval->attached_document = $approval->present()->policy(true);

            return $approval;
        });
    }

    public function getWeeklyRentInvoicesDataTAble()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';
        $data = $this->service->getApprovals([]);

        return $data['approvals']->where('payment_type_id', 2)->map(function ($approval) use ($currency) {
            $approval->formatted_amount = $currency.' '.number_format($approval->amount, 2);
            $approval->attached_document = $approval->present()->policy(true);

            return $approval;
        });
    }

    public function getRentApprovalsDataTAble()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';
        $data = $this->rentApprovals();

        return $data->map(function ($approval) use ($currency) {
            $approval->formatted_amount = $currency.' '.number_format($approval->amount, 2);
            $approval->attached_document = $approval->present()->policy(true);

            return $approval;
        });
    }

    private function rentApprovals()
    {
        $data = $this->service->getApprovals([]);

        return $data['approvals']->where('payment_type_id', '!=', 2);
    }

    public function getApprovals(Request $request)
    {
        $input = $request->all();
        $data = $this->service->getApprovals($input);
        $approvals = $data['approvals'];
        $order = $data['order'];
        $viewDate = $data['viewDate'];
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        if ($request->ajax()) {
            $view = view('reports.includes.approvals_table', compact('approvals', 'order', 'currency'))->render();

            return response()->json(['result' => 'OK', 'params' => $input, 'markup' => $view, 'order' => $order]);
        } else {
            $clients = Client::orderBy('client_name')->get(['id', 'client_name']);

            return view('reports.approvals', compact('viewDate', 'approvals', 'order', 'clients', 'currency'));
        }
    }

    public function getPropertiesDataTable()
    {
        return auth()->user()->client->property->map(function ($property) {
            $property->agent_name = Util::showAgent($property);
            $property->client_surveyor_name = $property->clientSurveyor->name ?? '-';
            $property->client_admin_name = $property->clientOwner->name ?? '-';

            return $property;
        });
    }

    public function getProperties()
    {
        $properties = auth()->user()->client->property;
        $viewDate = '';

        return view('reports.properties', compact('properties', 'viewDate'));
    }

    public function getApprovalsPDF(Request $request)
    {
        $input = $request->all();
        $data = $this->service->getApprovalsPDF($input);
        $approvals = $data['approvals'];
        $order = $data['order'];
        $viewDate = $data['viewDate'];
        $currency = optional(auth()->user()->client->currency)->html_code;
        $dateFrom = $input['dateFrom'];
        $dateTo = $input['dateTo'];

        $pdf = PDF::loadView('reports.includes.approvals_pdf', compact('approvals', 'currency', 'dateFrom', 'dateTo'))
            ->setPaper('a4', 'landscape')
            ->setOptions([
                'margin-top', 0,
                'margin-left', 12.7,
                'margin-right', 12.7,
                'margin-bottom', 6,
            ]);

        if (config('app.debug') == false) {
            return $pdf->download('ApprovalsReport.pdf');
        } else {
            return view('reports.includes.approvals_pdf', compact('approvals', 'currency', 'dateFrom', 'dateTo'));
        }
    }

    public function getApprovalsByProperty(Request $request)
    {
        $input = $request->all();
        $approvals = $this->service->getApprovalsByProperty($input);
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';
        $order = null;

        if ($request->ajax()) {
            $view = view('reports.includes.approvals_by_property_table', compact('approvals', 'order', 'currency'))
                ->render();

            return response()->json(['result' => 'OK', 'params' => $input, 'markup' => $view]);
        } else {
            if ($this->user->client) {
                $properties = $this->user
                    ->client
                    ->property()
                    ->select(['id', 'reference'])
                    ->whereNotNull('reference')
                    ->where('reference', '<>', '')
                    ->orderBy('reference')->get();
            } else {
                $properties = Property::select(['id', 'reference'])
                    ->whereNotNull('reference')
                    ->where('reference', '<>', '')
                    ->orderBy('reference')->get();
            }

            return view('reports.approvals_by_property', compact('properties', 'approvals', 'order', 'currency'));
        }
    }

    public function getInvoicesOnHoldDataTable()
    {
        $data = $this->service->getInvoicesOnHold([]);

        return $data['approvals']->map(function ($approval) {
            $approval->property_reference = $approval->property->reference;
            $approval->supplier_name = $approval->present()->supplier;
            $approval->contact_name = $approval->present()->contact;
            $approval->date_payment = $approval->present()->paymentDate;
            $approval->invoice_amount = $approval->present()->grossInvoiceAmount;
            $approval->amount_tax = $approval->present()->taxAmount;
            $approval->type_of_payment = $approval->present()->paymentType;
            $approval->date_diary = $approval->present()->diaryDate;

            return $approval;
        });
    }

    public function getAnnualServiceChargeDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        $data = auth()->user()->client->property->map(function ($property) use ($currency) {
            [$property, $month] = CommonTable::checkLast4YearsSC($property, $currency, false);

            $property->month = $month;

            return $property;
        });

        return $data;
    }

    public function getMissingLettersToFranchiseeDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';
        $data = auth()->user()->client->property->map(function ($property) use ($currency) {
            $property = CommonTable::checkLast4YearsSC($property, $currency, false, true);

            $property->prev_month = Carbon::parse(Carbon::now())->subMonthNoOverflow()->format('F');

            return $property;
        });

        return $data;
    }

    public function getMissingRealEstateChangeDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';
        $data = auth()->user()->client->property->map(function ($property) use ($currency) {
            $property = CommonTable::checkLast4YearsSC($property, $currency, false, true);

            $property->prev_month = Carbon::parse(Carbon::now())->subMonthNoOverflow()->format('F');

            return $property;
        });

        return $data;
    }

    public function getServiceChargeAndInsuranceRenewalDatesDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';
        $data = auth()->user()->client->property->map(function ($property) use ($currency) {
            $property = CommonTable::checkLast4YearsSC($property, $currency, false, true);

            $property->prev_month = Carbon::parse(Carbon::now())->subMonthNoOverflow()->format('F');

            return $property;
        });

        return $data;
    }

    public function getMissingServiceChargeBudgetsDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        $data = auth()->user()->client->property->where('has_service_charge', 1)->map(function ($property) use ($currency) {
            [$property, $month] = CommonTable::checkLast4YearsSC($property, $currency, false, true);

            if (! $property) {
                return false;
            }

            $property->month = $month;
            $property->main_contact = $property->capaOwner->name ?? '-';

            return $property;
        })->reject(function ($property) {
            return $property === false;
        });

        return $data;
    }

    public function getMissingServiceChargeReconciliationsDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        $data = auth()->user()->client->property->where('has_service_charge', 1)->map(function ($property) use ($currency) {
            [$property, $month] = CommonTable::checkLast4YearsSC($property, $currency, true, true);

            if (! $property) {
                return false;
            }

            $property->month = $month;
            $property->main_contact = $property->capaOwner->name ?? '-';

            return $property;
        })->reject(function ($property) {
            return $property === false;
        });

        return $data;
    }

    public function getAnnualInsuranceDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        $data = auth()->user()->client->property->map(function ($property) use ($currency) {
            [$property, $month] = CommonTable::checkLast4YearsIns($property, $currency);

            $property->month = $month;

            return $property;
        });

        return $data;
    }

    public function getMissingInsuranceDataTable()
    {
        $currency = optional(optional(auth()->user()->client)->currency)->html_code ?? '&pound;';

        $data = auth()->user()->client->property->map(function ($property) use ($currency) {
            [$property, $month] = CommonTable::checkLast4YearsIns($property, $currency, true);

            if (! $property) {
                return false;
            }

            $property->month = $month;
            $property->main_contact = $property->capaOwner->name ?? '-';

            return $property;
        })->reject(function ($property) {
            return $property === false;
        });

        return $data;
    }

    public function getInvoicesOnHold(Request $request)
    {
        $input = $request->all();
        $data = $this->service->getInvoicesOnHold($input);
        $approvals = $data['approvals'];
        $order = $data['order'];

        if ($request->ajax()) {
            $view = view('reports.includes.invoices_on_hold_table', compact('approvals', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $input, 'markup' => $view, 'order' => $order]);
        } else {
            return view('reports.invoices_on_hold', compact('approvals', 'order'));
        }
    }

    public function getClaims(Request $request)
    {
        $input = $request->all();
        $data = $this->service->getClaims($input);
        [$claims, $order] = $data;

        if ($request->ajax()) {
            $view = view('reports.includes.claims_table', compact('claims', 'order'))->render();

            return response()->json(['result' => 'OK', 'params' => $input, 'markup' => $view, 'order' => $order]);
        } else {
            return view('reports.claims', compact('claims', 'order'));
        }
    }

    public function getInsuranceRenewalDates()
    {
        $data = $this->service->getInsuranceRenewalDates();

        return view('reports.insurance_renewal_dates', compact('data'));
    }

    public function getServiceChargeAnnualBudget(Request $request)
    {
        $clientId = null;

        if ($request->has('client_id')) {
            $clientId = $request->input('client_id');
        } else {
            $clientId = auth()->user()->client_id;
        }

        $data = $this->service->getServiceChargeAnnualBudget($clientId);
        $clients = Client::select(['id', 'client_name'])->orderBy('client_name')->get();

        return view('reports.service_charge_annual_budget', compact('data', 'clients', 'clientId'));
    }

    public function getInsuranceAnnualBudget(Request $request)
    {
        $clientId = null;

        if ($request->has('client_id')) {
            $clientId = $request->input('client_id');
        } else {
            $clientId = auth()->user()->client_id;
        }

        $data = $this->service->getInsuranceAnnualBudget($clientId);
        $clients = Client::select(['id', 'client_name'])->orderBy('client_name')->get();

        return view('reports.insurance_annual_budget', compact('data', 'clients', 'clientId'));
    }

    private function getQuarterTotal($properties)
    {
        $total = 0;
        foreach ($properties as $property) {
            if ($property->budget_quarterly_payment) {
                $total = $total + str_replace(',', '', $property->budget_quarterly_payment);
            }
        }

        return $total;
    }

    private function mergeProperties($properties, $lastYearProperties)
    {
        $merged = [];
        foreach ($properties as $property) {
            $merged[$property->id] = [
                'ref' => $property->reference,
                'property_name' => $property->property_name,
                'current_year_payment' => $property->budget_quarterly_payment,
            ];
            foreach ($lastYearProperties as $lastYearProperty) {
                if (array_key_exists($lastYearProperty->id, $merged)) {
                    $merged[$lastYearProperty->id]['last_year_payment'] = $lastYearProperty->budget_quarterly_payment;
                } else {
                    $merged[$lastYearProperty->id] = [
                        'ref' => $lastYearProperty->reference,
                        'property_name' => $lastYearProperty->property_name,
                        'current_year_payment' => '',
                        'last_year_payment' => $lastYearProperty->budget_quarterly_payment,
                    ];
                }
            }
        }

        return $merged;
    }

    public function downloadAsZip()
    {
        $path = storage_path('app/invoices');
        if (! File::exists($path)) {
            File::makeDirectory($path);
        }
        if (! File::exists($path.'/invoices.zip')) {
            File::put($path.'/invoices.zip', '');
        }

        $approvals = $this->rentApprovals();

        $zipFile = $path.'/invoices.zip';
        $zip = new \ZipArchive();
        $zip->open($zipFile, \ZipArchive::CREATE | \ZIPARCHIVE::OVERWRITE);

        $storage = storage_path('app');

        foreach ($approvals as $key => $approval) {
            $file = $approval->present()->policy(true, true);

            if (! $file) {
                continue;
            }

            $filePath = $storage.'/'.$file->document_file;

            if (! File::exists($filePath)) {
                // if file does not exist
                continue;
            }

            // cut folder name to avoid making folders in zip then bind time to each file
            $fileNameInZip = time().'_'.(explode('/', $file->document_file)[1]);

            $zip->addFile($filePath, $fileNameInZip);
        }

        $zip->close();

        //TODO: copy to s3, change download from there
        // $inputStream = Storage::disk('local')->getDriver()->readStream($zipFile);
        // $destination = Storage::disk('s3')->getDriver()->getAdapter()->getPathPrefix().'/zip_invoices/';
        //Storage::disk('s3')->getDriver()->putStream($destination, $inputStream);

        //delete local zip file
        // Storage::delete('invoices/invoices.zip');

        return response()->download($zipFile);
    }
}
