<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdministratorRequest;
use App\Models\Address;
use App\Models\Administrator;

class AdministratorController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->read_only;
        $administrators = Administrator::select(['id', 'name'])->get();

        return view('administrators.index', compact('administrators', 'read_only'));
    }

    public function create()
    {
        $administrator = new Administrator();
        $address = new Address();

        return view('administrators.edit', compact('administrator', 'address'));
    }

    public function store(AdministratorRequest $request)
    {
        $input = $request->all();
        $address = Address::create($input['address']);
        $input['address_id'] = $address->id;
        $administrator = Administrator::create($input);

        $administrator->contacts()->create($input['contact']);

        $data = [
            'id'         => $administrator->id,
            'status'     => 'ok',
            'controller' => 'administrators',
        ];

        return response()->json($data);
    }

    public function show(Administrator $administrator)
    {
        $read_only = $this->user->read_only;

        return view('administrators.show', compact('administrator', 'read_only'));
    }

    public function edit(Administrator $administrator)
    {
        $address = $administrator->address;

        return view('administrators.edit', compact('administrator', 'address'));
    }

    public function update(Administrator $administrator, AdministratorRequest $request)
    {
        $address = $request->get('address');
        if ($address) {
            $administrator->address->update($address);
        }

        $administrator->update($request->all());

        $data = [
            'id'         => $administrator->id,
            'status'     => 'ok',
            'controller' => 'administrators',
        ];

        return response()->json($data);
    }
}
