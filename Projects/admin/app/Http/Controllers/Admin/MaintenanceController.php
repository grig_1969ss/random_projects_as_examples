<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenanceRequest;
use App\Models\Diary;
use App\Models\Maintenance;
use App\Models\Property;

class MaintenanceController extends Controller
{
    public function create(Property $property)
    {
        $maintenance = new Maintenance();

        return view('maintenances.edit', compact('maintenance', 'property'));
    }

    public function store(Property $property, MaintenanceRequest $request)
    {
        $input = $request->all();
        $input['notice_date'] = Date::toMysql($input['notice_date']);
        $input['diary_date'] = Date::toMysql($input['diary_date']);

        if (! isset($input['resolved'])) {
            $input['resolved'] = 0;
        }

        $diary = Diary::create([
            'property_id'   => $property->id,
            'diary_type_id' => 4,
            'diary_title'   => 'Maintenance',
            'date'          => $input['diary_date'],
            'status'        => 0,
        ]);

        $input['diary_id'] = $diary->id;

        $property->maintenance()->create($input);
        Util::addNote($property->id, 'properties', 4, 'Maintenance was Added');

        $data = [
            'id'         => $property->id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function edit(Maintenance $maintenance)
    {
        return view('maintenances.edit', compact('maintenance'));
    }

    public function update(Maintenance $maintenance, MaintenanceRequest $request)
    {
        $input = $request->all();
        $input['notice_date'] = Date::toMysql($input['notice_date']);
        $input['diary_date'] = Date::toMysql($input['diary_date']);

        if (! isset($input['resolved'])) {
            $input['resolved'] = 0;
        }

        $diary = Diary::find($input['diary_id']);

        if ($diary) {
            $diary->update([
                'date' => $input['diary_date'],
            ]);
        }

        $maintenance->update($input);
        Util::addNote($maintenance->property_id, 'properties', 4, 'Maintenance was Edited');

        $data = [
            'id'         => $maintenance->property_id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }
}
