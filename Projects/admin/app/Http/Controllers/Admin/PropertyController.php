<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyDisposalRequest;
use App\Http\Requests\PropertyRequest;
use App\Http\Requests\ReDiariseRequest;
use App\Models\Address;
use App\Models\Agent;
use App\Models\AlienationType;
use App\Models\AppealType;
use App\Models\Breaks;
use App\Models\BreakType;
use App\Models\Client;
use App\Models\ClientQuery;
use App\Models\Currency;
use App\Models\DiaryType;
use App\Models\DisposalType;
use App\Models\DocumentType;
use App\Models\Email;
use App\Models\EmailTemplate;
use App\Models\Landlord;
use App\Models\Maintenance;
use App\Models\Note;
use App\Models\NoteType;
use App\Models\OwnerType;
use App\Models\Property;
use App\Models\PropertyStatus;
use App\Models\PropertyType;
use App\Models\Region;
use App\Models\Rent;
use App\Models\RentPaymentFrequency;
use App\Models\RentReview;
use App\Models\RepairClassificationType;
use App\Models\ServiceCharge;
use App\Models\SublettingType;
use App\Models\TenureType;
use App\Models\User;
use App\Models\UserType;
use App\Services\PropertyService;
use App\Traits\HelperTrait;
use App\Traits\PropertyTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PropertyController extends Controller
{
    const REPORT_LIMIT = 5;

    use PropertyTables, HelperTrait;

    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $status = request()->get('status');

        $properties = $this->properties($status, false);

        return view('properties.index', compact('properties'));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $status = request()->get('status');

        $properties = $this->properties($status, false, $request->search);

        return json_encode(view('properties.includes.properties_list', compact('properties'))->with([
            'showClients' => true,
            'search' => $search,
        ])->toHtml());
    }

    public function overview($date)
    {
        $year = Carbon::parse($date)->format('Y');

        $years = range($year, $year - 3);

        $properties = $this->properties(null, true);

        $rows = [];

        foreach ($properties as $key => $property) {
            $row = [];

            $row[] = $property->property_name;

            $serviceChargesYears = $this->serviceCharges($property->id, self::REPORT_LIMIT)['yearsExists'];
            $paymentsYears = $this->payments(Property::find($property->id))['yearsExists'];

            $insurancesYears = $this->generateInsurances(Property::find($property->id))['yearsExists'];

            // take last 4 years from start date
            $insurancesYears = $this->filterYears($insurancesYears);
            $paymentsYears = $this->filterYears($paymentsYears);

            $row = $this->sectionYears($years, $serviceChargesYears, $row);
            $row = $this->sectionYears($years, $insurancesYears, $row);
            $row = $this->sectionYears($years, $paymentsYears, $row);

            $rows[] = $row;
        }

        return view('properties.overview', compact('rows', 'years', 'properties'));
    }

    private function filterYears($yearsArray)
    {
        if (! $yearsArray) {
            return [];
        }

        rsort($yearsArray);
        $yearsArray = array_unique($yearsArray);
        $yearsArray = array_slice($yearsArray, 0, self::REPORT_LIMIT);

        return $yearsArray;
    }

    private function sectionYears($years, $sectionYears, $row)
    {
        if (! $sectionYears) {
            $sectionYears = [];
        }

        foreach ($years as $year) {
            if (in_array($year, $sectionYears)) {
                $row[] = 'Y';
            } else {
                $row[] = 'N';
            }
        }

        return $row;
    }

    private function properties($status, $all, $search = null)
    {
        return Property::getPropertiesIndexList($status, $all, $search);
    }

    public function create()
    {
        return $this->commonCreate();
    }

    public function store(PropertyRequest $request)
    {
        $input = (new PropertyService($request->except(['_token', '_method'])))->formatInput();

        $propertyId = Util::saveFormValues($input, null, Property::class, true);

        Util::addNote($propertyId, 'properties', 3, 'Property was Added');

        $data = [
            'id' => $propertyId,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    private function serviceCharges($propertyId, $years5 = self::REPORT_LIMIT, $returnServiceCharges = false)
    {
        $serviceChargeObj = new ServiceCharge();
        $serviceCharges = $serviceChargeObj->getAllServiceChargesByPropertyId($propertyId, $years5);

        // this is e.g. for email modal, we need only limited service charges and not the table
        if ($returnServiceCharges) {
            return $serviceCharges;
        }

        unset($serviceChargeObj);

        return $this->getServiceChargesAsTable($serviceCharges);
    }

    public function show(Property $property)
    {
        if ($property->client_id != auth()->user()->client_id) {
            return redirect('properties');
        }

        $emails = Email::inbox()->filterByCurrentClient()->where('property_id', $property->id)->orderByDesc('date')->paginate(10)->withPath('/ajax/inbox');
        $unreadCount = $this->unreadCount('all');

        $read_only = $this->user->isClient();
        $serviceCharges = $this->serviceCharges($property->id);
        $rentReviews = (new RentReview)->getAllrentReviewsByPropertyId($property->id);
        $breaks = (new Breaks)->getAllbreaksByPropertyId($property->id);
        $documents = $property->document;
        $disposalTypes = (new DisposalType)->toDropdown();
        $noteTypes = (new NoteType)->toDropdown();
        $documentTypes = (new DocumentType)->toDropdown();
        $clientQueries = (new ClientQuery)->getAllClientQueriesByPropertyId($property->id);
        $clientQueryNew = new ClientQuery();
        $maintenance = (new Maintenance())->getAllMaintenaceByPropertyId($property->id);
        $unresolvedMaintenance = (new Maintenance())->getAllUnresolvedMaintenaceByPropertyId($property->id);
        $rents = (new Rent)->getAllByPropertyId($property->id);
        $currentRent = (new Rent)->getCurrentRentByPropertyId($property->id);
        $breakClauseTypeDropdown = BreakType::getAllBreakTypesForDropdown();
        $notes = $this->getNotes($property->id, 'properties');
        $break = new Breaks();
        $insurances = $this->generateInsurances($property);
        $utilities = $this->generateUtilities($property);
        $payments = $this->payments($property);
        $documentDropdown = $this->attachToDropdown($property);
        $alerts = $property->alerts;

        $diaryDate = null;
        $diary = $property->diaries->first();
        if ($diary) {
            $diaryDate = Carbon::parse($diary->date)->format('d/m/Y');
        }

        return view('properties.show')->with([
            'property' => $property,
            'payments' => $payments,
            'insurances' => $insurances,
            'noteTypes' => $noteTypes,
            'notes' => $notes,
            'serviceCharges' => $serviceCharges,
            'rentReviews' => $rentReviews,
            'documentTypes' => $documentTypes,
            'documents' => $documents,
            'disposalTypes' => $disposalTypes,
            'breaks' => $breaks,
            'clientQueries' => $clientQueries,
            'clientQueryNew' => $clientQueryNew,
            'maintenance' => $maintenance,
            'break' => $break,
            'rents' => $rents,
            'currentRent' => $currentRent,
            'breakClauseTypeDropdown' => $breakClauseTypeDropdown,
            'read_only' => $read_only,
            'emails' => $emails,
            'unreadCount' => $unreadCount,
            'documentDropdown' => $documentDropdown,
            'alerts' => $alerts,
            'utilities' => $utilities,
            'diaryDate' => $diaryDate,
        ]);
    }

    private function getNotes($property, $type)
    {
        return Util::getAllNotesByObjectIdAndTypeName($property, $type);
    }

    public function attachTo($id)
    {
        $documentDropdown = $this->attachToDropdown(Property::find($id), true);
        $arrayNames = true;

        return json_encode(view('documents.includes.attachToDropdown', compact('documentDropdown', 'arrayNames'))->toHtml());
    }

    public function filterNote(Request $request)
    {
        $type = $request->type;

        if (isset($request->property)) {
            $notes = $this->getNotes($request->property, 'properties');
        } else {
            $notes = Note::with(['user', 'noteType'])->where([
                'object_id' => $request->selected,
                'object_type_id' => $this->objectTypes[$request->type],
            ])->get();
        }

        return json_encode(view('partials.show_note_section', compact('notes', 'type'))->toHtml());
    }

    private $objectTypes = [
        'Service Charge' => 8,
        'Insurance' => 9,
        'Utility' => 11,
    ];

    private function payments($property)
    {
        $payments = $property->payment()->orderBy('payment_ref')->get();

        return $this->generatePayments($payments);
    }

    public function edit(Property $property)
    {
        return $this->commonCreate($property, true);
    }

    public function update(Property $property, PropertyRequest $request)
    {
        $input = (new PropertyService($request->except(['_token', '_method'])))->formatInput();

        unset($input['break_clause']);

        $propertyId = Util::saveFormValues($input, $property->id, Property::class, true);

        Util::addNote($propertyId, 'properties', 3, 'Property was Edited');

        $data = [
            'id' => $propertyId,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function duplicate(Property $property)
    {
        return $this->commonCreate($property);
    }

    public function dispose(Property $property, PropertyDisposalRequest $request)
    {
        $input = $request->only('disposal_type_id', 'disposal_date');

        //$input = $request->except('_token');
        $input['disposal_date'] = Carbon::parse($input['disposal_date']);
        $input['property_status_id'] = 2;

        Util::saveFormValues($input, $property->id, Property::class);

        $data = [
            'id' => $property->id,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    protected function commonCreate(Property $property = null, $editing = false)
    {
        // TODO: Move to a middleware
        if ($this->user->isClient() || $this->user->read_only) {
            return redirect(route('properties.index'));
        }

        if (! $property) {
            $property = new Property();
            $address = new Address();
            $contacts = collect();
            $contactId = null;
        } elseif (! $editing) {
            $address = $property->address;
            $property->id = null;
            $property->address_id = null;
            $id = null;
            $contacts = $property->agent ? $property->agent->contacts : collect();
            $contactId = $property->agent_contact_id;
        } else {
            $address = $property->address;
            $contacts = $property->agent ? $property->agent->contacts : collect();
            $contactId = $property->agent_contact_id;
        }

        $clientsDropdown = (new Client)->toDropdown();
        $agentsDropdown = (new Agent)->toDropdown();
        $landlordsDropdown = (new Landlord)->toDropdown();
        $regionsDropdown = (new Region)->toDropdown();
        $propertyStatusesArray = (new PropertyStatus)->toDropdown();
        $propertyTypesDropdown = (new PropertyType)->toDropdown();
        $tenureTypesDropdown = (new TenureType)->toDropdown();
        $breakTypesDropdown = (new BreakType)->toDropdown();
        $alienationTypesDropdown = (new AlienationType)->toDropdown();
        $userTypesDropdown = (new UserType)->toDropdown();
        $sublettingTypesDropdown = (new SublettingType)->toDropdown();
        $appealTypesDropdown = (new AppealType)->toDropdown();
        $repairClassificationTypesDropdown = (new RepairClassificationType)->toDropdown();
        $rentPaymentFrequencyDropdown = (new RentPaymentFrequency)->toDropdown();
        $clientId = $this->user->client_id;
        $currencies = Currency::all();
        $ownerTypes = OwnerType::orderBy('name')->get();
        $currencyCode = $this->user->currencyHtmlCode();
        $users = User::cursor();
        $capaOwners2 = User::whereHas('roles', function ($query) {
            $query->where('slug', '!=', 'client');
        })->cursor();

        $view = view('properties.edit', compact(
            'property',
            'address',
            'clientsDropdown',
            'agentsDropdown',
            'landlordsDropdown',
            'regionsDropdown',
            'propertyStatusesArray',
            'propertyTypesDropdown',
            'tenureTypesDropdown',
            'breakTypesDropdown',
            'alienationTypesDropdown',
            'userTypesDropdown',
            'sublettingTypesDropdown',
            'appealTypesDropdown',
            'repairClassificationTypesDropdown',
            'rentPaymentFrequencyDropdown',
            'clientId',
            'currencies',
            'ownerTypes',
            'currencyCode',
            'contacts',
            'contactId',
            'users',
            'capaOwners2'
        ));

        if (isset($id)) {
            $view = $view->with('objectId', $id);
        }

        return $view;
    }

    public function loadTemplateData(Request $request)
    {
        $template = EmailTemplate::where('id', $request->id)->select('subject', 'body')->first()->toArray();

        $template['body'] = $this->templateBody($template['body'], $request);
        $template['body'] = $this->br2nl($template['body']);

        $template['subject'] = $this->templateBody($template['subject'], $request);

        $ccTo = $this->ccToEmail($request);
        $template['to'] = $ccTo['to'];
        $template['cc'] = $ccTo['cc'];

        $attachmentsDropdown = $this->propertyAttachments(Property::find($request->property));

        $template['attachmentsDropdown'] = json_encode(view('properties.includes.service_charges.attachments', compact('attachmentsDropdown'))->toHtml());

        return response($template, Response::HTTP_OK);
    }

    private function propertyAttachments($property)
    {
        $dropdown = [];

        foreach ($property->document as $document) {
            $option = $property->property_name.' - '.$document->document_name;
            $dropdown['Property'][$property->id][$document->id] = $option;
        }

        $serviceCharges = $this->serviceCharges($property->id, self::REPORT_LIMIT, true);
        $insurances = $this->generateInsurances($property, true);

        foreach ($serviceCharges as $sc) {
            foreach ($sc->document as $document) {
                $option = Date::Y($sc->year_end).' - '.$document->document_name;
                $dropdown['ServiceCharge'][$sc->id][$document->id] = $option;
            }
        }

        foreach ($insurances as $insurance) {
            foreach ($insurance->document as $document) {
                $option = Date::Y($insurance->insurance_renewal_date).' - '.$document->document_name;
                $dropdown['Insurance'][$insurance->id][$document->id] = $option;
            }
        }

        // dd($dropdown);

        return $dropdown;
    }

    private function ccToEmail($request)
    {
        $cc = '';
        $to = '';
        if ($request->type === 'Franchisee Updated') {
            $cc = auth()->user()->client->franchise_email;
            $to = Property::find($request->property)->users->first()->email ?? '';
        }

        if ($request->type === 'Real Estate Updated') {
            $to = auth()->user()->client->realestate_email;
        }

        return compact('cc', 'to');
    }

    private function templateBody($template, $request)
    {
        $Model = app('App\Models\\'.$request->model);

        $instance = $Model::find($request->scId);

        $column = ($request->model == 'ServiceCharge') ? 'year_end' : 'insurance_renewal_date';

        $previous = $Model::where('property_id', $request->property)
            ->whereDate($column, '<=', $instance->$column)->orderBy($column, 'DESC')->where('id', '!=', $instance->id)->first();

        $franchisee = Property::find($request->property)->users();

        $increaseDecrease = 'decrease';
        if (! is_null($previous) && ($instance->annual_budget > $previous->annual_budget)) {
            $increaseDecrease = 'increase';
        }

        $previousYearEndPlus1Month = '';
        $previousYearEndPlus6Months = '';
        $previousAnnualBudget = '';
        $previousQuarterlyBudget = '';
        if (is_null($previous)) {
            $increaseDecrease = 'increase';
        } else {
            $previousYearEnd = $previous->$column;
            $previousYearEndPlus1Month = Carbon::parse($previousYearEnd)->addMonthNoOverflow()->format('M Y');
            $previousYearEndPlus6Months = Carbon::parse($previousYearEnd)->addMonths(6)->format('M Y');
            $previousAnnualBudget = $previous->annual_budget ? number_format($previous->annual_budget, 2) : '';
            $previousQuarterlyBudget = $previous->budget_quarterly_payment ? number_format($previous->budget_quarterly_payment, 2) : '';
        }

        $replace = [
            'annual_budget' => number_format($instance->annual_budget, 2),
            'previous_annual_budget' => $previousAnnualBudget,
            'franchisee_name' => $franchisee->first()->name ?? '',
            'current_year_end' => Carbon::parse($instance->$column)->format('d/m/Y'),
            'increased_decreased' => "$increaseDecrease".'d',
            'increase_decrease' => $increaseDecrease,
            'client_email' => auth()->user()->client->inbound_email,
            'currency_symbol' => auth()->user()->client->currency->symbol,
            'current_annual_budget' => number_format($instance->annual_budget, 2),
            'property_reference' => $instance->property->reference,
            'property_name' => Property::find($request->property)->property_name,
            'previous_quarterly_budget' => $previousQuarterlyBudget,
            'current_quarterly_budget' => number_format($instance->budget_quarterly_payment, 2),
            'previous_year end + 1 month (Formatted as "JANUARY 2019"' => $previousYearEndPlus1Month,
            'previous_year end + 6 months (Formatted as "June 2019"' => $previousYearEndPlus6Months,
            'current_user' => auth()->user()->name,
            'period' => $instance->period_of_insurance,
            'insurance_premium' => number_format($instance->insurance_premium, 2),
            'percentage_increase_decrease' => round(abs($this->incDecPercentage($instance, $previous, $request->model))),
            'client_inbox_email' => $instance->property->client->email,
        ];

        $start = '{{';
        $end = '}}';

        $pattern = sprintf(
            '/%s(.+?)%s/ims',
            preg_quote($start, '/'),
            preg_quote($end, '/')
        );

        if (preg_match_all($pattern, $template, $matches)) {
            // list(, $match) = $matches;
            foreach ($matches[1] as $key => $match) {
                $template = str_replace('{{'.$match.'}}', $replace[trim($match)], $template);
            }
        }

        return $template;
    }

    private function incDecPercentage($instance, $previous, $Model)
    {
        if (! $previous) {
            return 100;
        }

        if ($Model == 'ServiceCharge') {
            return '';
        }

        return (floatval($instance->insurance_premium) - floatval($previous->insurance_premium)) / floatval($previous->insurance_premium) * 100;
    }

    public function reDiarise(ReDiariseRequest $request)
    {
        $model = app('App\Models\\'.$request->model)->find($request->modelInstance);

        $diaries = $model->diaries;

        //add space between camelcase words
        $name = preg_replace('/([a-z])([A-Z])/s', '$1 $2', $request->model);
        $diaryTypeId = DiaryType::where('name', $name)->first()->id;

        $diaryId = null;
        if ($diaries->isEmpty()) {
            $diaryId = $model->find($request->modelInstance)->diaries()->create([
                'comments' => $request->diarise_note,
                'property_id' => $request->property_diarie,
                'diary_title' => $request->model,
                'diary_type_id' => $diaryTypeId,
                'status' => 0,
                'date' => Carbon::parse(Carbon::createFromFormat('d/m/Y', $request->diary_date))->format('Y-m-d h:i:s'),
                'updated_at' => now(),
            ]);
        // return response()->json(['refresh' => true], Response::HTTP_OK);
        } else {
            foreach ($diaries as $diary) {
                $diary->update([
                    'comments' => $request->diarise_note,
                    'property_id' => $request->property_diarie,
                    'diary_title' => $request->model,
                    'diary_type_id' => $diaryTypeId,
                    'status' => 0,
                    'date' => Carbon::parse(Carbon::createFromFormat('d/m/Y', $request->diary_date))->format('Y-m-d h:i:s'),
                    'updated_at' => now(),
                ]);
            }
            $diaryId = $diaries->first();
        }

        if ($diaryId) {
            $model->update(['diary_id' => $diaryId->id]);
        }

        Util::addNote($request->modelInstance, $request->object_type, $request->note_type, $request->diarise_note);

        return response()->json(['refresh' => true], Response::HTTP_OK);
    }

    private function br2nl($body)
    {
        $breaks = ['<br />', '<br>', '<br/>'];

        return str_ireplace($breaks, "\r\n", $body);
    }
}
