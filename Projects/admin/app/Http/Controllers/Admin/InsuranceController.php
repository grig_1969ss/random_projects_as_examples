<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\InsuranceRequest;
use App\Models\Insurance;
use App\Models\InsuranceParty;
use App\Models\Property;
use App\Traits\HelperTrait;
use App\Traits\UpdateDiary;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class InsuranceController extends Controller
{
    use UpdateDiary, HelperTrait;

    public function create(Property $property)
    {
        $insurance = new Insurance();
        $insuranceParties = InsuranceParty::all();

        return view('insurances.edit', compact('property', 'insurance', 'insuranceParties'));
    }

    public function store(Property $property, InsuranceRequest $request)
    {
        $input = $request->all();

        if ($input['insurance_renewal_date']) {
            $input['insurance_renewal_date']
                = Carbon::createFromFormat('d/m/Y', $input['insurance_renewal_date'])->format('Y-m-d');
        }

        if ($input['diary_date']) {
            $diary = $property->createDiaryEntry(7, 'Insurance', $input['diary_date']);
            $input['diary_id'] = $diary->id;
        }

        $ins = $property->insurances()->create($input);

        Util::addNote($property->id, 'properties', 8, 'Insurance was Added');

        $data = [
            'id' => $property->id,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        $this->selectData($property, $ins->id, $request->modal_save, $data, 'Insurance');

        return response()->json($data);
    }

    public function edit(Insurance $insurance)
    {
        $property = $insurance->property;
        $insuranceParties = InsuranceParty::all();

        return view('insurances.edit', compact('insurance', 'property', 'insuranceParties'));
    }

    public function update(Insurance $insurance, InsuranceRequest $request)
    {
        $input = $request->all();

        if ($input['insurance_renewal_date']) {
            $input['insurance_renewal_date']
                = Carbon::createFromFormat('d/m/Y', $input['insurance_renewal_date'])->format('Y-m-d');
        }

        $date = $input['diary_date'];
        [$input['diary_id'], $deleteDiary] = $this->updateDiaryEntry($date, $insurance, 7, 'Insurance');

        $insurance->update($input);

        if ($deleteDiary) {
            $insurance->diary->delete();
        }

        Util::addNote($insurance->property_id, 'properties', 8, 'Insurance was Edited');

        $data = [
            'id' => $insurance->property_id,
            'status' => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function destroy(Insurance $insurance)
    {
        $property = $insurance->property;

        try {
            $insurance->delete();
        } catch (\Exception $e) {
            Log::debug($e);

            return;
        }

        return redirect(route('properties.show', $property));
    }
}
