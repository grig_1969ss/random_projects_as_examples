<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuditorRequest;
use App\Models\Auditor;

class AuditorController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->read_only;
        $auditors = Auditor::select(['id', 'name', 'email'])->get();

        return view('auditors.index', compact('auditors', 'read_only'));
    }

    public function create()
    {
        $auditor = new Auditor();

        return view('auditors.edit', compact('auditor'));
    }

    public function store(AuditorRequest $request)
    {
        $input = $request->all();
        $auditor = Auditor::create($input);

        $data = [
            'id'         => $auditor->id,
            'status'     => 'ok',
            'controller' => 'auditors',
        ];

        return response()->json($data);
    }

    public function show(Auditor $auditor)
    {
        $read_only = $this->user->read_only;

        return view('auditors.show', compact('auditor', 'read_only'));
    }

    public function edit(Auditor $auditor)
    {
        return view('auditors.edit', compact('auditor'));
    }

    public function update(Auditor $auditor, AuditorRequest $request)
    {
        $auditor->update($request->all());

        $data = [
            'id'         => $auditor->id,
            'status'     => 'ok',
            'controller' => 'auditors',
        ];

        return response()->json($data);
    }
}
