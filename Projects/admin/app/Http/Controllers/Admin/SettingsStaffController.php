<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StaffRequest;
use App\Models\Client;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SettingsStaffController extends Controller
{
    const PAGINATE_COUNT = 10;

    public function index()
    {
        $staffs = User::get();

        return view('settings.staff.index', compact('staffs'));
    }

    public function create()
    {
        $staff = new User();
        $rolesDropDown = (new Role)->toDropdown();
        $clientsDropDown = (new Client)->toDropdown();

        return view('settings.staff.edit', compact('staff', 'rolesDropDown', 'clientsDropDown'));
    }

    public function store(StaffRequest $request)
    {
        $input = $request->all();

        if (! isset($input['verified'])) {
            $input['verified'] = 0;
        }
        if (! isset($input['disabled'])) {
            $input['disabled'] = 0;
        }

        $input['password'] = Hash::make($input['password']);

        $staff = User::create($input);

        $staff->attachRole($input['role_id']);

        $data = [
            'id' => $staff->id,
            'status' => 'ok',
            'controller' => 'settings/staff',
        ];

        return response()->json($data);
    }

    public function show(User $staff)
    {
        $staff = User::getStaffById($staff->id);

        return view('settings.staff.show', compact('staff'));
    }

    public function edit(User $staff)
    {
        $rolesDropDown = (new Role)->toDropdown();
        $clientsDropDown = (new Client)->toDropdown();

        return view('settings.staff.edit', compact('staff', 'rolesDropDown', 'clientsDropDown'));
    }

    public function update(User $staff, StaffRequest $request)
    {
        $input = $request->all();

        if (! isset($input['verified'])) {
            $input['verified'] = 0;
        }
        if (! isset($input['disabled'])) {
            $input['disabled'] = 0;
        }

        $staff->update($input);
        $staff->detachAllRoles();
        $staff->attachRole($input['role_id']);
        $staff->clients()->sync($input['client_id']);

        if (! $staff->hasRole($input['role_id'])) {
            $staff->attachRole($input['role_id']);
        }

        $data = [
            'id' => $staff->id,
            'status' => 'ok',
            'controller' => 'settings/staff',
        ];

        return response()->json($data);
    }
}
