<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClaimSubtypeRequest;
use App\Models\ClaimSubtype;
use App\Models\ClaimType;

class ClaimSubtypeController extends Controller
{
    public function create()
    {
        $claimSubtype = new ClaimSubtype();
        $claimTypes = ClaimType::all();
        $view = view('settings.claim_subtypes.edit', compact('claimSubtype', 'claimTypes'))->render();
        $title = 'Add Claim Subtype';

        return response()->json(compact('view', 'title'));
    }

    public function store(ClaimSubtypeRequest $request)
    {
        ClaimSubtype::create($request->all());

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }

    public function edit(ClaimSubtype $claimSubtype)
    {
        $claimTypes = ClaimType::all();
        $view = view('settings.claim_subtypes.edit', compact('claimSubtype', 'claimTypes'))->render();
        $title = 'Edit Claim Subtype';

        return response()->json(compact('view', 'title'));
    }

    public function update(ClaimSubtype $claimSubtype, ClaimSubtypeRequest $request)
    {
        $claimSubtype->update($request->all());

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }

    public function delete(ClaimSubtype $claimSubtype)
    {
        $view = view('settings.claim_subtypes.delete', compact('claimSubtype'))->render();
        $title = 'Are you sure you want to delete this Claim Subtype?';

        return response()->json(compact('view', 'title'));
    }

    public function destroy(ClaimSubtype $claimSubtype)
    {
        $claimSubtype->delete();

        return response()->json([
            'redirectUrl' => route('settings.index'),
        ]);
    }
}
