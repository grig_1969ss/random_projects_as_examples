<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SwitchClientController extends Controller
{
    public function switchClient()
    {
        $user = auth()->user();

        $clients = $user->clients()->select('id', 'client_name')->get();

        return view('admin.account.switchClient', compact('user', 'clients'));
    }

    public function saveSwitchClient(Request $request)
    {
        $user = auth()->user();
        if (! in_array($request->current_client, $user->clients()->pluck('client_id')->toArray())) {
            return redirect('account/switch-client');
        }

        $user->client_id = $request->current_client;
        $user->save();

        return redirect('/');
    }
}
