<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\LandlordRequest;
use App\Models\Address;
use App\Models\Contact;
use App\Models\Landlord;
use App\Models\NoteType;
use App\Queries\PropertiesByCompany;

class LandlordController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->isClient();
        $clientId = $this->user->client_id;

        if ($clientId) {
            $landlords = Landlord::getLandlordsByClient($clientId);
        } else {
            $landlords = Landlord::getLandlords();
        }

        return view('landlords.index', compact('read_only', 'landlords'));
    }

    public function create()
    {
        $landlord = new Landlord();
        $address = new Address();

        return view('landlords.edit', compact('landlord', 'address'));
    }

    public function store(LandlordRequest $request)
    {
        $input = $request->except('_token');
        $userId = $this->user->id;

        if (isset($input['isModal'])) {
            $isModal = $input['isModal'];
            unset($input['isModal']);
        } else {
            $isModal = false;
        }

        $contactFirstname = $input['contact_firstname'];

        unset($input['contact_firstname']);

        $contactSurname = $input['contact_surname'];

        unset($input['contact_surname']);

        $landlordId = Util::saveFormValues($input, null, Landlord::class, true);

        Util::addNote(
            $landlordId,
            'landlords',
            1,
            'Landlord was Added'
        );

        // save contact name
        $contactData = [
            'first_name'       => $contactFirstname,
            'surname'          => $contactSurname,
            'contactable_id'   => $landlordId,
            'contactable_type' => 'App\\Models\\Landlord',
            'created_by'       => $userId,
            'updated_by'       => $userId,
            'created_at'       => date('Y-m-d H:i:s'),
            'updated_at'       => date('Y-m-d H:i:s'),
        ];

        $contactObj = new Contact();

        $contactObj->saveData($contactData);

        unset($contactObj);

        if ($isModal) {
            $html = "<option value='{$landlordId}'>{$input['landlord_name']}</option>";

            $data = [
                'id'       => $landlordId,
                'status'   => 'ok',
                'html'     => $html,
                'modal'    => 'div#addLandlordModal',
                'selector' => 'select#landlord_id',
                'type'     => 'addLandlordModal',
            ];
        } else {
            $data = [
                'id'         => $landlordId,
                'status'     => 'ok',
                'controller' => 'landlords',
            ];
        }

        return response()->json($data);
    }

    public function show(Landlord $landlord)
    {
        $read_only = $this->user->isClient();
        $status = request()->get('status');

        $landlord = Landlord::with([
            'property',
            'property.address',
            'property.client',
            'property.propertyStatus',
            'contact',
            'user',
        ])->find($landlord->id);

        $noteTypes = (new NoteType)->toDropdown();
        $notes = Util::getAllNotesByObjectIdAndTypeName($landlord->id, 'landlords');
        $properties = (new PropertiesByCompany)->get('landlord', $landlord->id, $status);

        return view('landlords.show', compact('landlord', 'noteTypes', 'notes', 'properties', 'read_only'));
    }

    public function edit(Landlord $landlord)
    {
        $address = $landlord->address;

        return view('landlords.edit', compact('landlord', 'address'));
    }

    public function update(Landlord $landlord, LandlordRequest $request)
    {
        $input = $request->except(['_token', '_method']);
        $landlordId = Util::saveFormValues($input, $landlord->id, Landlord::class, true);

        Util::addNote(
            $landlordId,
            'landlords',
            1,
            'Landlord was Edited'
        );

        $data = [
            'id'         => $landlordId,
            'status'     => 'ok',
            'controller' => 'landlords',
        ];

        return response()->json($data);
    }
}
