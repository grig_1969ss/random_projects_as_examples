<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttachmentDetails;
use App\Models\Client;
use App\Models\DocumentType;
use App\Models\Email;
use App\Models\Insurance;
use App\Models\InsuranceParty;
use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\Property;
use App\Models\Supplier;
use App\Traits\DocumentTrait;
use App\Traits\HelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class InboxController extends Controller
{
    const PAGINATION_COUNT = 10;
    private $archive = null;

    use DocumentTrait, HelperTrait;

    public function index()
    {
        $type = request()->get('emails');
        $nextType = ($type === 'my') ? 'all' : 'my';

        $emails = Email::inbox($type)->filterByCurrentClient()->orderByDesc('date')
            ->paginate(self::PAGINATION_COUNT)->withPath('/ajax/inbox');

        $unreadCount = $this->unreadCount($type);

        return view(
            'admin.inbox.index',
            compact('emails', 'unreadCount', 'nextType')
        );
    }

    public function detail(Email $email)
    {
        if (! $email->read_at) {
            $email->read_at = Carbon::now();
            $email->read_by = Auth::user()->id;
            $email->update();
        }

        $attachments = AttachmentDetails::select('id', 'name', 'content_type')
            ->where('email_id', $email->id)
            ->where('deleted_at', null)
            ->where(function ($query) {
                $query->where('contentid', '')
                    ->orWhereNull('contentid');
            })
            ->get();

        $properties = [];
        $propertyId = null;
        if ($email->client_id) {
            $properties = $properties = $this->getClientProperties($email->client_id);
            $propertyId = $email->property_id;
        } else {
            $properties = $properties = $this->getClientProperties(auth()->user()->client_id);
        }

        $htmlBody = $this->cleanBody($email->html_body);
        $htmlBody = $this->replaceCidToBase64($htmlBody, $email);

        $htmlBody = preg_replace('/o:href=(.*?)"/', '', $htmlBody);
        $htmlBody = preg_replace('/src="file:(.*?)"/', '', $htmlBody);

        // for iframe data
        if (request()->get('iframe')) {
            return $htmlBody;
        }

        $documentDropdown = $this->attachToDropdown(Property::find($email->property_id));
        $documentTypes = (new DocumentType)->toDropdown();

        return view('admin.emails.email_detail', compact('email', 'htmlBody', 'properties', 'propertyId', 'documentDropdown', 'documentTypes', 'attachments'));
    }

    private function getClientProperties($id)
    {
        // filter properties by client
        return Property::where('properties.client_id', $id)
            ->groupBy('properties.property_name')
            ->cursor();
    }

    public function linkEmail(Request $request)
    {
        $data = $request->all();

        if (isset($data['attachments']) &&
            isset($data['note_category']) &&
            isset($data['document_type_id'])
        ) {
            $this->archiveEmail($data['attach_to_type']);

            $this->saveDocument($data);
        }

        $update = [
            'property_id' => $request->property,
            'client_id' => auth()->user()->client->id,
            'read_at' => null,
            'read_by' => null,
        ];

        $update = array_merge($update, ['archived_at' => $this->archive]);

        Email::where('id', $request->email)
            ->update($update);

        return redirect('/properties/'.$request->property);
    }

    private function saveDocument($data)
    {
        $attachType = $data['attach_to_type'];
        $attachments = $data['attachments'];
        $noteCategory = $data['note_category'];
        $docTypes = $data['document_type_id'];

        // in the list some of attachments can be not chosen (from: start, middle, end)
        // but we need to keep array orders
        foreach ($attachType as $key => $model) {
            if (is_null($model)) {
                if (count($attachments) < count($attachType)) {
                    array_splice($attachments, $key, 0, [null]);
                }
                if (count($noteCategory) < count($attachType)) {
                    array_splice($noteCategory, $key, 0, [null]);
                }
                if (count($docTypes) < count($attachType)) {
                    array_splice($docTypes, $key, 0, [null]);
                }
            }
        }

        $timestamp = time();

        foreach ($attachType as $key => $type) {
            // skip if `Attach to` is not chosen
            if (is_null($type)) {
                continue;
            }

            $document = AttachmentDetails::find($attachments[$key]);
            $model = app('App\Models\\'.$type)->find($noteCategory[$key]);

            $input['document_name'] = $document->name;
            $input['document_file'] = $timestamp.'/'.$document->name;
            $input['content_type'] = $document->content_type;
            $input['document_type_id'] = $docTypes[$key];

            $image = base64_decode($document->content);

            Storage::disk('s3')->put($timestamp.'/'.$document->name, $image);

            $model->document()->create($input);

            if ($this->archive && $model->diary) {
                $model->diary->update(['date' => now()]);
            }
        }
    }

    public function emailActions(Request $request)
    {
        $action = $request->action;
        $emails = $request->emails;

        if ($action === 'important') {
            Email::whereIn('id', $emails)->update(['important' => 1]);
        } elseif ($action === 'trash') {
            Email::whereIn('id', $emails)->delete();
        }

        return back();
    }

    public function ajaxInbox(Request $request)
    {
        $query = Email::orderByDesc('date')->filterByCurrentClient();

        if ($type = $request->input('type')) {
            $query = $query->filterByType($type, $request->emails);
        }

        if ($search = $request->input('search')) {
            $query = $query->search($search);
        }

        $emails = $query
            ->paginate(self::PAGINATION_COUNT, ['*'], 'page', $request->input('page'))
            ->withPath('/ajax/inbox');

        $view = view('admin.inbox.partials.emails_table', compact('emails'))->render();
        $paginationView = view('admin.inbox.partials.pagination', compact('emails'))->render();

        return response()->json(compact('view', 'paginationView'));
    }

    public function ajaxLoadInbox(Request $request)
    {
        $query = Email::filterByCurrentProperty();

        if ($type = $request->input('type')) {
            $query = $query->filterByType($type, $request->emails);
        }

        $emails = $query->filterByCurrentClient()->orderByDesc('date')->paginate(self::PAGINATION_COUNT)->withPath('/ajax/inbox');

        $view = view('admin.inbox.partials.right_side', compact('emails'))->render();

        return response()->json(compact('view'));
    }

    public function loadCrud(Request $request)
    {
        $property = Property::find($request->property);

        if ($request->type == 'service charge') {
            $data = $this->serviceChargeCreate();

            $serviceCharge = $data['serviceCharge'];
            $apportionmentTypesDropdown = $data['apportionmentTypesDropdown'];
            $serviceChargeStatusesDropdown = $data['serviceChargeStatusesDropdown'];

            $title = 'Add Service Charge';
            $restMethod = 'POST';

            return json_encode([
                'crud' => view(
                    'service_charges.includes.form',
                    compact(
                        'property',
                        'serviceCharge',
                        'apportionmentTypesDropdown',
                        'serviceChargeStatusesDropdown',
                        'title',
                        'restMethod'
                    )
                )->toHtml(), 'pdf' => view(
                    'layouts_new.partials.components.pdf.viewer'
                )->toHtml(),
            ]);
        } elseif ($request->type == 'insurance') {
            $insurance = new Insurance();
            $insuranceParties = InsuranceParty::all();

            $title = 'Add Insurance';
            $restMethod = 'POST';

            return json_encode(
                [
                    'crud' => view('insurances.includes.form', compact('property', 'title', 'restMethod', 'property', 'insurance', 'insuranceParties'))->toHtml(),
                    'pdf' => view('layouts_new.partials.components.pdf.viewer')->toHtml(),
                ]
            );
        } elseif ($request->type == 'approval') {
            $payment = new Payment();
            $title = 'Add Approval';
            $contacts = collect();
            $route = route('properties.payments.store', $property);
            $suppliers = Supplier::orderBy('name')->select(['id', 'name'])->get();
            $paymentTypes = PaymentType::orderBy('name')->get();

            return json_encode(
                [
                    'modal' => view('partials.base_modal')->toHtml(),
                    'crud' => view('payments.includes.form', compact('property', 'title', 'payment', 'route', 'suppliers', 'contacts', 'paymentTypes'))->toHtml(),
                    'pdf' => view('layouts_new.partials.components.pdf.viewer')->toHtml(),
                ]
            );
        }
    }

    private function archiveEmail($attachTypes)
    {
        if (empty($attachTypes)) {
            return false;
        }

        // archive email if no one of chosen items is not `Property`!
        if (! in_array('Property', $attachTypes)) {
            $this->archive = now();
        }
    }
}
