<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\RentReviewRequest;
use App\Models\Property;
use App\Models\RentReview;

class RentReviewController extends Controller
{
    public function create(Property $property)
    {
        $rentReview = new RentReview();

        return view('rent_reviews.edit', compact('property', 'rentReview'));
    }

    public function store(Property $property, RentReviewRequest $request)
    {
        $input = $request->all();
        $input['date'] = Date::toMysql($input['date']);
        $input['notice_dates'] = Date::toMysql($input['notice_dates']);

        if (! isset($input['interest'])) {
            $input['interest'] = 0;
        }
        if (! isset($input['time_of_essence'])) {
            $input['time_of_essence'] = 0;
        }
        if (! isset($input['turnover_rent'])) {
            $input['turnover_rent'] = 0;
        }
        if (! isset($input['floor_areas_confirmed'])) {
            $input['floor_areas_confirmed'] = 0;
        }

        $noteText = $input['notes'];

        $property->rentReviews()->create($input);

        Util::addNote(
            $property->id,
            'properties',
            8,
            $noteText != '' ? $noteText : 'Rent Review was Added'
        );

        $data = [
            'id'         => $property->id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function edit(RentReview $rentReview)
    {
        $property = $rentReview->property;

        return view('rent_reviews.edit', compact('rentReview', 'property'));
    }

    public function update(RentReview $rentReview, RentReviewRequest $request)
    {
        $input = $request->all();
        $input['date'] = Date::toMysql($input['date']);
        $input['notice_dates'] = Date::toMysql($input['notice_dates']);

        if (! isset($input['interest'])) {
            $input['interest'] = 0;
        }
        if (! isset($input['time_of_essence'])) {
            $input['time_of_essence'] = 0;
        }
        if (! isset($input['turnover_rent'])) {
            $input['turnover_rent'] = 0;
        }
        if (! isset($input['floor_areas_confirmed'])) {
            $input['floor_areas_confirmed'] = 0;
        }

        $rentReview->update($input);

        $noteText = $input['notes'];

        Util::addNote(
            $rentReview->property_id,
            'properties',
            8,
            $noteText != '' ? $noteText : 'Rent Review was Edited'
        );

        $data = [
            'id'         => $rentReview->property_id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }
}
