<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Currency;
use App\Helpers\Date;
use App\Helpers\Reports\Report;
use App\Helpers\Reports\SCharge;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Repositories\PropertyRepository;
use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportCsvController extends Controller
{
    protected $service;
    protected $user;

    public function __construct()
    {
        $this->service = new ReportService();

        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function getRentReviewCSV(Request $request)
    {
        $this->service->getRentReviewCSV($request->all());
    }

    public function getLeaseExpiryCSV(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs);
        $inputs = Report::formatQueryDate($inputs, 'dateFrom');
        $inputs = Report::formatQueryDate($inputs, 'dateTo');

        $properties = (new PropertyRepository())->getAllPropertiesDueLeaseExpiry($order, $inputs['dateFrom'], $inputs['dateTo']);

        $required_fields = $this->getRequiredFields();

        $properties->transform(function ($item) use ($required_fields) {
            $item->lease_start_date = Date::dmY($item->lease_start_date);
            $item->rent_review_date = Date::dmY($item->rent_review_date);
            $item->lease_expiry_date = Date::dmY($item->lease_expiry_date);
            $item->current_rent = utf8_decode(Currency::gbp($item->current_rent));
            $item->break_option_date = Date::dmY($item->break_option_date);
            $item->notice_date = Date::dmY($item->break_option_notice_period);
            $item->rateable_value = utf8_decode(Currency::gbp($item->rateable_value));
            $item->repair_liability = utf8_decode(Currency::gbp($item->repair_liability));
            $item->branch_no = $item->reference;

            foreach ($required_fields as $field) {
                $items[$field] = $item->$field;
            }

            return collect($items)->only($required_fields);
        });

        $properties->prepend(collect(array_combine($required_fields, $required_fields)));

        return response()->streamDownload(
            function () use ($properties) {
                Report::collectToCSV($properties);
            },
            'report.csv'
        );
    }

    public function getRateableValueCSV(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'notice_dates');
        $inputs = Report::formatQueryDate($inputs, 'dateFrom');
        $inputs = Report::formatQueryDate($inputs, 'dateTo');

        $properties = (new PropertyRepository())->getAllPropertiesWithRateableValue(
            $order,
            $inputs['dateFrom'],
            $inputs['dateTo']
        );

        $required_fields = $this->getRequiredFields();

        $properties->transform(function ($item) use ($required_fields) {
            $item->lease_start_date = Date::dmY($item->lease_start_date);
            $item->rent_review_date = Date::dmY($item->rent_review_date);
            $item->lease_expiry_date = Date::dmY($item->lease_expiry_date);
            $item->current_rent = utf8_decode(Currency::gbp($item->current_rent));
            $item->break_option_date = Date::dmY($item->break_option_date);
            $item->notice_date = Date::dmY($item->break_option_notice_period);
            $item->rateable_value = utf8_decode(Currency::gbp($item->rateable_value));
            $item->repair_liability = utf8_decode(Currency::gbp($item->repair_liability));
            $item->branch_no = $item->reference;

            foreach ($required_fields as $field) {
                $items[$field] = $item->$field;
            }

            return collect($items)->only($required_fields);
        });

        $properties->prepend(collect(array_combine($required_fields, $required_fields)));

        return response()->streamDownload(
            function () use ($properties) {
                Report::collectToCSV($properties);
            },
            'report.csv'
        );
    }

    public function getBreakOptionCSV(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'break_option_date');
        $inputs = Report::formatQueryDate($inputs, 'dateFrom');
        $inputs = Report::formatQueryDate($inputs, 'dateTo');

        $properties = (new PropertyRepository())->getAllPropertiesBreakOptionReview($order, $inputs['dateFrom'], $inputs['dateTo']);

        $required_fields = $this->getRequiredFields();

        $properties->transform(function ($item) use ($required_fields) {
            $item->lease_start_date = Date::dmY($item->lease_start_date);
            $item->rent_review_date = Date::dmY($item->rent_review_date);
            $item->lease_expiry_date = Date::dmY($item->lease_expiry_date);
            $item->current_rent = utf8_decode(Currency::gbp($item->current_rent));
            $item->break_option_date = Date::dmY($item->break_option_date);
            $item->notice_date = Date::dmY($item->break_option_notice_period);
            $item->rateable_value = utf8_decode(Currency::gbp($item->rateable_value));
            $item->repair_liability = utf8_decode(Currency::gbp($item->repair_liability));
            $item->branch_no = $item->reference;

            foreach ($required_fields as $field) {
                $items[$field] = $item->$field;
            }

            return collect($items)->only($required_fields);
        });

        $properties->prepend(collect(array_combine($required_fields, $required_fields)));

        return response()->streamDownload(
            function () use ($properties) {
                Report::collectToCSV($properties);
            },
            'report.csv'
        );
    }

    public function getConditionCSV(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'notice_dates');

        $properties = (new PropertyRepository())->getAllPropertiesWithSchedule($order);

        $props[0] = [
            'region_name',
            'reference',
            'property_name',
            'address',
            'document_name',
        ];
        $i = 1;
        foreach ($properties as $property) {
            $props[$i]['region_name'] = $property->region_name;
            $props[$i]['branch_no'] = $property->reference;
            $props[$i]['property_name'] = $property->property_name;
            $props[$i]['address'] = $property->address;
            $props[$i]['schedule_of_condition'] = $property->document_name;

            $i++;
        }

        return response()->streamDownload(
            function () use ($props) {
                $fp = fopen('php://output', 'w');
                foreach ($props as $prop) {
                    fputcsv($fp, $prop);
                }
            },
            'report.csv'
        );
    }

    public function getDocumentChecklistCSV(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'property_name');

        $properties = (new PropertyRepository())->getAllPropertiesDocumentChecklist($order);

        $required_fields = [
            'region_name',
            'reference',
            'property_name',
            'lease',
            'sublease',
            'licence_to_assign',
            'licence_for_alterations',
            'rent_review_memorandum',
            'landlord_notices',
            'schedule_of_condition',
            'schedule_of_dilapidations',
            'demise_plan',
            'goad_plan',
        ];

        $properties->transform(function ($item) use ($required_fields) {
            $item->lease = Util::showYesNo($item->lease);
            $item->sublease = Util::showYesNo($item->sublease);
            $item->licence_to_assign = Util::showYesNo($item->licence_to_assign);
            $item->licence_for_alterations = Util::showYesNo($item->licence_for_alterations);
            $item->rent_review_memorandum = Util::showYesNo($item->rent_review_memorandum);
            $item->landlord_notices = Util::showYesNo($item->landlord_notices);
            $item->schedule_of_condition = Util::showYesNo($item->schedule_of_condition);
            $item->schedule_of_dilapidations = Util::showYesNo($item->schedule_of_dilapidations);
            $item->demise_plan = Util::showYesNo($item->demise_plan);
            $item->goad_plan = Util::showYesNo($item->goad_plan);

            foreach ($required_fields as $field) {
                $items[$field] = $item->$field;
            }

            return collect($items)->only($required_fields);
        });

        $properties->prepend(collect(array_combine($required_fields, $required_fields)));

        return response()->streamDownload(
            function () use ($properties) {
                Report::collectToCSV($properties);
            },
            'report.csv'
        );
    }

    public function getMaintenanceCSV()
    {
        $properties = (new PropertyRepository())->getAllPropertiesWithActiveMaintenance();

        $required_fields = [
            'region_name',
            'reference',
            'property_name',
            'resolved',
            'notice_date',
            'address',
        ];

        $properties->transform(function ($item) use ($required_fields) {
            $item->resolved = $item->resolved ? 'Yes' : 'No';
            $item->notice_date = Date::dmY($item->notice_date);

            foreach ($required_fields as $field) {
                $items[$field] = $item->$field;
            }

            return collect($items)->only($required_fields);
        });

        $properties->prepend(collect(array_combine($required_fields, $required_fields)));

        return response()->streamDownload(
            function () use ($properties) {
                Report::collectToCSV($properties);
            },
            'report.csv'
        );
    }

    public function getRentalLiabilityCSV(Request $request)
    {
        $inputs = $request->all();
        $order = Report::addSortQuery($inputs, 'notice_dates');

        $properties = Property::getRentalLiabilityAll($order);

        $props[0] = [
            'region_name',
            'reference',
            'property_name',
            'rent_payment_frequency',
            'rent_0',
            'rent_1',
            'rent_2',
            'rent_3',
            'rent_4',
            'rent_5',
        ];

        $currencyFormatFunction = function ($value, $row) {
            if (isset($row->rent) && isset($row->rent[$value])) {
                $ret = number_format($row->rent[$value]->rent, 2);

                if (isset($row->rent[$value - 1]) && ($row->rent[$value - 1]->rent != 0) && $row->rent[$value]->rent != 0) {
                    $prevCompare = (($row->rent[$value]->rent - $row->rent[$value - 1]->rent) / $row->rent[$value - 1]->rent) * 100;
                    $prevCompare = number_format($prevCompare, 2);
                } else {
                    $prevCompare = '-';
                }

                $ret = 'Exp. '.Date::dmY($row->rent[$value]->exp_rent_date)."\n".utf8_decode('£').$ret.' ('.$prevCompare.'%)';
            } else {
                $ret = '-';
            }

            return $ret;
        };

        $i = 1;
        foreach ($properties as $property) {
            $props[$i]['region_name'] = $property->region_name;
            $props[$i]['branch_no'] = $property->reference;
            $props[$i]['property_name'] = $property->property_name;
            $props[$i]['rent_payment_frequency'] = $property->rent_payment_frequency;

            $j = 0;
            if (isset($property->rent)) {
                foreach ($property->rent as $key => $rent) {
                    $props[$i]['rent_'.$j] = $rent->rent;
                    $j++;
                }
            }

            $i++;
        }

        return response()->streamDownload(
            function () use ($props) {
                $fp = fopen('php://output', 'w');
                foreach ($props as $prop) {
                    fputcsv($fp, $prop);
                }
            },
            'report.csv'
        );
    }

    public function getAnnualServiceChargeCSV(Request $request)
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $date = Report::addDateQuery($inputs);

        $properties = Property::getAllPropertiesForServiceChargeReport('Actual', $order, $date);

        $props[0] = [
            'region_name',
            'reference',
            'property_name',
        ];

        $serviceCharge = null;
        $scDates = SCharge::setDates($properties, $props);
        $props = $scDates['props'];
        $yearArray = $scDates['yearArray'];
        $totalArray = $scDates['totalArray'];
        unset($scDates);

        $i = 1;
        foreach ($properties as $property) {
            $props[$i]['region_name'] = $property->region_name;
            $props[$i]['branch_no'] = $property->reference;
            $props[$i]['property_name'] = $property->property_name;

            $key = 0;
            foreach ($yearArray as $yearKey => $year) {
                $prevCompare = '-';
                $yearEnd = '-';
                $amount = 0;

                if (isset($property->service_charge[$key - 1]) and
                    isset($property->service_charge[$key]) and
                    $property->service_charge[$key - 1]->annual_payment != 0 and
                    $property->service_charge[$key]->annual_payment != 0
                ) {
                    $prevCompare = (($property->service_charge[$key]->annual_payment - $property->service_charge[$key - 1]->annual_payment) / $property->service_charge[$key - 1]->annual_payment) * 100;
                    $prevCompare = number_format($prevCompare, 2);
                }

                if (isset($property->service_charge[$key]) and Date::Y($property->service_charge[$key]->year_end) == $year) {
                    $yearEnd = Date::dmY($property->service_charge[$key]->year_end);
                    $amount = utf8_decode('£').number_format($property->service_charge[$key]->annual_payment, 2);
                    $totalArray[$year] += $property->service_charge[$key]->annual_payment;
                    $key++;
                }

                $amountIndex = $props['0'][3 + (($yearKey) * 3)];
                $compareIndex = $props['0'][4 + (($yearKey) * 3)];
                $yearEndIndex = $props['0'][5 + (($yearKey) * 3)];

                $props[$i][$amountIndex] = ($amount === 0 ? '' : $amount);
                $props[$i][$compareIndex] = $prevCompare;
                $props[$i][$yearEndIndex] = $yearEnd;
            }

            $i++;
        }

        return response()->streamDownload(
            function () use ($props) {
                $fp = fopen('php://output', 'w');
                foreach ($props as $prop) {
                    fputcsv($fp, $prop);
                }
            },
            'report.csv'
        );
    }

    public function getMissingPaymentsCSV(Request $request, $type = 'Budget')
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $type = strtolower($type) == 'budget' ? 'Budget' : 'Actual';
        $date = Report::addDateQuery($inputs);

        $properties = (new PropertyRepository())->getAllPropertiesMissingPayments($type, $order, $date);

        $required_fields = [
            'region_name',
            'reference',
            'property_name',
            'year_end',
            'address',
            'landlord_name',
            'landlord_address',
            'landlord_agent',
            'agent_address',
        ];

        $properties->transform(function ($item) use ($required_fields) {
            $item->year_end = Date::dmY($item->year_end);

            foreach ($required_fields as $field) {
                $items[$field] = $item->$field;
            }

            return collect($items)->only($required_fields);
        });

        $properties->prepend(collect(array_combine($required_fields, $required_fields)));

        return response()->streamDownload(
            function () use ($properties) {
                Report::collectToCSV($properties);
            },
            'report.csv'
        );
    }

    public function allRecentChargesForPropertiesCSV()
    {
        $serviceCharges = Property::getAllRecentServiceCharges(true);

        $props = [];

        $props[0] = ['Property ID', 'Property Name', 'Reference', 'Service Charge Date'];

        foreach ($serviceCharges as $key => $serviceCharge) {
            $props[$key + 1] = $serviceCharge;
        }

        return response()->streamDownload(
            function () use ($props) {
                $fp = fopen('php://output', 'w');
                foreach ($props as $prop) {
                    fputcsv($fp, $prop);
                }
            },
            'service-charges-report.csv'
        );
    }

    public function getQuarterBudgetCSV(Request $request)
    {
        $inputs = $request->all();

        $order = Report::addSortQuery($inputs, 'property_name');
        $date = isset($inputs['date'])
            ? Date::toMysql(date('d/m/Y', strtotime('31 Dec '.$inputs['date'])))
            : Date::toMysql(date('d/m/Y', strtotime('31 Dec '.date('Y', strtotime('+1 Year')))));

        $lastYearDate = Date::toMysql(date('d/m/Y', strtotime($date.'-1 Year')));

        $yearsRange = Report::yearsRange();

        $properties = Property::getAllPropertiesForQuarterServiceChargeReport('Budget', $order, $date);

        $props[0] = [
            'reference',
            'property_name',
            'quarter1',
            'quarter2',
            'quarter3',
            'quarter4',
        ];

        $i = 1;
        foreach ($properties as $property) {
            $props[$i]['branch_no'] = $property->reference;
            $props[$i]['property_name'] = $property->property_name;
            $props[$i]['quarter_1'] = $property->budget_quarterly_payment;
            $props[$i]['quarter_2'] = $property->budget_quarterly_payment;
            $props[$i]['quarter_3'] = $property->budget_quarterly_payment;
            $props[$i]['quarter_4'] = $property->budget_quarterly_payment;

            $i++;
        }

        return response()->streamDownload(
            function () use ($props) {
                $fp = fopen('php://output', 'w');
                foreach ($props as $prop) {
                    fputcsv($fp, $prop);
                }
            },
            'report.csv'
        );
    }

    public function getApprovalsByPropertyCSV(Request $request)
    {
        $input = $request->all();

        $this->service->getApprovalsByPropertyCSV($input);
    }

    public function getInvoicesOnHoldCSV(Request $request)
    {
        $this->service->getInvoicesOnHoldCSV($request->all());
    }

    public function getClaimsCSV(Request $request)
    {
        $this->service->getClaimsCSV($request->all());
    }

    public function getInsuranceRenewalDatesCSV()
    {
        $this->service->getInsuranceRenewalDatesCSV();
    }

    public function getServiceChargeAnnualBudgetCSV(Request $request)
    {
        $this->service->getServiceChargeAnnualBudgetCSV($request->input('client_id'));
    }

    public function getInsuranceAnnualBudgetCSV(Request $request)
    {
        $this->service->getInsuranceAnnualBudgetCSV($request->input('client_id'));
    }

    private function getRequiredFields()
    {
        return [
            'region_name',
            'branch_no',
            'address',
            'lease_start_date',
            'rent_review_date',
            'lease_expiry_date',
            'current_rent',
            'break_option_date',
            'notice_date',
            'rateable_value',
            'landlord_name',
            'landlord_address',
            'landlord_agent',
            'agent_address',
            'repair_liability',
            'schedule_of_condition',
            'repair_obligations_note',
            'user_clause_note',
        ];
    }
}
