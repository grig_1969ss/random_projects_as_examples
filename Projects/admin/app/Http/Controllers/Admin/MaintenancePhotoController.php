<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenancePhotoRequest;
use App\Models\Maintenance;
use App\Models\MaintenancePhoto;

class MaintenancePhotoController extends Controller
{
    public function store(Maintenance $maintenance, MaintenancePhotoRequest $request)
    {
        $timestamp = time();

        $photo = $maintenance->maintenancePhotos()->create([
            'document_name' => $request->get('photo_maintenance'),
            'document_file' => $timestamp.'/'.$request->get('photo_maintenance'),
            'content_type' => $request->file('photo_maintenance')->getMimeType(),
        ]);

        $request->file('photo_maintenance')->storeAs('public/'.$timestamp, $request->get('photo_maintenance'));

        $maintenancePhotoObj = new MaintenancePhoto();
        $photoCount = $maintenancePhotoObj->countAllMaintenacePhotoByMaintenanceId($maintenance->id);
        unset($maintenancePhotoObj);

        $html = view('partials.maintenance-photo-item', [
            'photo' => $photo,
            'i' => $photoCount,
        ])->render();

        $data = [
            'status' => 'ok',
            'html' => $html,
            'maintenanceId' => $maintenance->id,
        ];

        return response()->json($data);
    }
}
