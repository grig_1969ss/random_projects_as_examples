<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SupplierRequest;
use App\Models\Address;
use App\Models\NoteType;
use App\Models\Supplier;

class SupplierController extends Controller
{
    protected $user;

    const PAGINATE_COUNT = 10;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->isClient();
        $suppliers = Supplier::paginate(self::PAGINATE_COUNT);

        return view('suppliers.index', compact('suppliers', 'read_only'));
    }

    public function create()
    {
        $supplier = new Supplier();
        $address = new Address();

        return view('suppliers.edit', compact('supplier', 'address'));
    }

    public function store(SupplierRequest $request)
    {
        $input = $request->all();
        $address = Address::create($input['address']);
        $input['address_id'] = $address->id;
        $supplier = Supplier::create($input);

        $supplier->contacts()->create($input['contact']);

        $supplier->createNote([
            'note_type_id' => 1,
            'note_text'    => 'Supplier was Added',
        ]);

        $data = [
            'id'         => $supplier->id,
            'status'     => 'ok',
            'controller' => 'suppliers',
        ];

        return response()->json($data);
    }

    public function show(Supplier $supplier)
    {
        $read_only = $this->user->isClient();
        $notes = $supplier->getNotes();
        $noteTypes = (new NoteType)->toDropdown();

        return view('suppliers.show', compact('supplier', 'read_only', 'notes', 'noteTypes'));
    }

    public function edit(Supplier $supplier)
    {
        $address = $supplier->address;

        return view('suppliers.edit', compact('supplier', 'address'));
    }

    public function update(Supplier $supplier, SupplierRequest $request)
    {
        $supplier->address->update($request->get('address'));
        $supplier->update($request->all());

        $supplier->createNote([
            'note_type_id' => 1,
            'note_text'    => 'Supplier was Edited',
        ]);

        $data = [
            'id'         => $supplier->id,
            'status'     => 'ok',
            'controller' => 'suppliers',
        ];

        return response()->json($data);
    }

    public function getContactsSelectPartial(Supplier $supplier)
    {
        $contacts = $supplier->contacts;
        $contactId = null;
        $elementName = 'contact_id';
        $view = view('partials.edit_contacts_partial', compact('contacts', 'contactId', 'elementName'))->render();

        return response()->json(compact('view'));
    }
}
