<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientQueryRequest;
use App\Models\ClientQuery;
use App\Models\Diary;
use App\Models\Property;

class ClientQueryController extends Controller
{
    public function create(Property $property)
    {
        $clientQuery = new ClientQuery();

        return view('client_queries.edit', compact('property', 'clientQuery'));
    }

    public function store(Property $property, ClientQueryRequest $request)
    {
        $input = $request->all();
        $input['due_date'] = Date::toMysql($input['due_date']);
        $input['diary_date'] = Date::toMysql($input['diary_date']);

        $diary = Diary::create([
            'property_id'   => $property->id,
            'diary_type_id' => 2,
            'diary_title'   => 'Client Query',
            'date'          => $input['diary_date'],
            'status'        => 0,
        ]);

        $input['diary_id'] = $diary->id;

        $property->clientQueries()->create($input);

        Util::addNote($property->id, 'properties', 4, 'Client Query was Added');

        $data = [
            'id'         => $property->id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }

    public function edit(ClientQuery $clientQuery)
    {
        return view('client_queries.edit', compact('clientQuery'));
    }

    public function update(ClientQuery $clientQuery, ClientQueryRequest $request)
    {
        $input = $request->all();
        $input['due_date'] = Date::toMysql($input['due_date']);
        $input['diary_date'] = Date::toMysql($input['diary_date']);
        $diary = Diary::find($input['diary_id']);

        if ($diary) {
            $diary->update([
                'date' => $input['diary_date'],
            ]);
        }

        $clientQuery->update($input);
        Util::addNote($clientQuery->property_id, 'properties', 4, 'Client Query was Edited');

        $data = [
            'id'         => $clientQuery->property_id,
            'status'     => 'ok',
            'controller' => 'properties',
        ];

        return response()->json($data);
    }
}
