<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuditRegisterRequest;
use App\Models\AuditRegister;
use App\Models\Client;
use Carbon\Carbon;

class AuditRegisterController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $read_only = $this->user->read_only;
        $auditRegisters = AuditRegister::all();

        return view('audit_registers.index', compact('auditRegisters', 'read_only'));
    }

    public function create()
    {
        $auditRegister = new AuditRegister();
        $clients = Client::select(['id', 'client_name'])->get();

        return view('audit_registers.edit', compact('auditRegister', 'clients'));
    }

    public function store(AuditRegisterRequest $request)
    {
        $input = $request->all();
        $input['audit_start_date'] = Carbon::createFromFormat('d/m/Y', $input['audit_start_date'])->toDateString();
        $input['audit_end_date'] = Carbon::createFromFormat('d/m/Y', $input['audit_end_date'])->toDateString();
        $auditRegister = AuditRegister::create($input);

        $data = [
            'id'         => $auditRegister->id,
            'status'     => 'ok',
            'controller' => 'audit-registers',
        ];

        return response()->json($data);
    }

    public function show(AuditRegister $auditRegister)
    {
        $read_only = $this->user->read_only;

        return view('audit_registers.show', compact('auditRegister', 'read_only'));
    }

    public function edit(AuditRegister $auditRegister)
    {
        $clients = Client::select(['id', 'client_name'])->get();

        return view('audit_registers.edit', compact('auditRegister', 'clients'));
    }

    public function update(AuditRegister $auditRegister, AuditRegisterRequest $request)
    {
        $input = $request->all();
        $input['audit_start_date'] = Carbon::createFromFormat('d/m/Y', $input['audit_start_date'])->toDateString();
        $input['audit_end_date'] = Carbon::createFromFormat('d/m/Y', $input['audit_end_date'])->toDateString();

        $auditRegister->update($input);

        $data = [
            'id'         => $auditRegister->id,
            'status'     => 'ok',
            'controller' => 'audit-registers',
        ];

        return response()->json($data);
    }
}
