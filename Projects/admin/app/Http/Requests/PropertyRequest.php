<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'property_name'          => 'required|max:100',
            'client_id'		       	    => 'required',
            'address_1'              => 'required|max:100',
            'address_2'              => 'max:100',
            'town'                   => 'max:100',
            'county'                 => 'max:100',
            'post_code'              => 'max:100',
            'reference'              => 'max:100',
            'floor_area_net_trading' => 'max:100',
            'purchase_price'         => 'max:100',
            'book_value'             => 'max:100',
            'freehold_value'         => 'max:100',
            'repair_liability'       => 'max:100',
            'date_last_inspected'    => 'max:100',
            'lease_start_date'       => 'max:100',
            'lease_expiry_date'      => 'max:100',
            'lease_notice_dates'     => 'max:100',
            'guarantor'              => 'max:100',
            'tenant_break_option'    => 'max:100',
            'agent_break_option'     => 'max:100',
            'notice_dates'           => 'max:100',
            'annual_rent'            => 'max:100',
            'rent_paid'              => 'max:100',
            'insurance_supplier'     => 'max:100',
            'deposit'                => 'max:100',
            'erv_date'               => 'max:100',
            'erv_amount'             => 'max:100',
            'initial_concessions'    => 'max:100',
            'auto_service_charge'	   => 'max:100',
            'property_status_id'     => 'required|integer|max:100',
            'property_type_id'       => 'required|integer|max:100',
            'tenure_type_id'         => 'integer|max:100',
        ];
    }

    public function attributes()
    {
        return [
            'client_id'          => 'client',
            'property_status_id' => 'property status',
            'property_type_id'   => 'property type',
            'tenure_type_id'     => 'tenure type',
        ];
    }
}
