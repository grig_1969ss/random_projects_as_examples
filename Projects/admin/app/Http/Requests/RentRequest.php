<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'exp_rent_date' => 'required|max:100',
            'rent'          => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'exp_rent_date' => 'rent expiry date',
        ];
    }
}
