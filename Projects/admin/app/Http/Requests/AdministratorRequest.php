<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdministratorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $administrator = $this->route('administrator');

        $rules = [
            'name'              => 'required',
            'address.address_1' => 'required',
            'address.post_code' => 'required',
        ];

        if (! $administrator) {
            $rules['contact.first_name'] = 'required';
            $rules['contact.surname'] = 'required';
            $rules['contact.email'] = 'required|email';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'contact.first_name' => 'contact first name',
            'contact.surname'    => 'contact surname',
            'contact.email'      => 'contact email',
            'address.address_1'  => 'address line 1',
            'address.post_code'  => 'post code',
        ];
    }
}
