<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaintenancePhotoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'photo_maintenance' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'photo_maintenance' => 'file',
        ];
    }
}
