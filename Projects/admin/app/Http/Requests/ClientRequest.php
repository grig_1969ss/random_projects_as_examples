<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $client = $this->route('client');

        if ($client) {
            $emailRule = "required|unique:clients,email,{$client->id}";
        } else {
            $emailRule = 'required|email|unique:clients';
        }

        $rules = [
            'client_name' => 'required|max:100',
            'address_1' => 'required|max:100',
            'address_2' => 'max:100',
            'town' => 'required|max:100',
            'county' => 'max:100',
            'post_code' => 'required|max:100',
            'telephone' => 'required|max:100',
            'fax' => 'max:100',
            'email' => $emailRule,
            'logo' => 'nullable',
        ];

        return $rules;
    }
}
