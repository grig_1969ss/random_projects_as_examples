<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaintenanceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'notice_date' => 'required|date_format:d/m/Y',
            'notes'       => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'notice_date' => 'due date',
            'notes'       => 'details',
        ];
    }
}
