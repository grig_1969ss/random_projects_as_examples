<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LandlordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'landlord_name'	=> 'required|max:100',
            'address_1'		   => 'max:100',
            'address_2'		   => 'max:100',
            'town'			       => 'max:100',
            'county'		      => 'max:100',
            'post_code'		   => 'max:100',
            'telephone'		   => 'max:100',
            'fax'			        => 'max:100',
            'email'			      => 'nullable|email',
        ];
    }

    public function attributes()
    {
        return [
            'landlord_name' => 'Landlords Name',
        ];
    }
}
