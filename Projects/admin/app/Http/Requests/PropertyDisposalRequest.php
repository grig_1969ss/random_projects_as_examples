<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyDisposalRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'disposal_type_id' => 'required|integer|max:100',
            'disposal_date'    => 'required|max:100',
        ];
    }

    public function attributes()
    {
        return [
            'disposal_type_id' => 'disposal type',
        ];
    }
}
