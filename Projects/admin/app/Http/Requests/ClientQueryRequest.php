<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientQueryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'due_date'               => 'required|max:100',
            'client_query_status_id' => 'required|max:100',
            'details'                => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'client_query_status_id' => 'client query status',
        ];
    }
}
