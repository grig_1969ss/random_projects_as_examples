<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note_category' => 'required|integer',
            'attach_to_type' => 'array|nullable',
            'datetime' => 'required|date',
            'alert_comments' => 'string|nullable',
        ];
    }

    public function attributes()
    {
        return [
            'note_category' => 'Attach to',
            'datetime' => 'Due Date',
        ];
    }
}
