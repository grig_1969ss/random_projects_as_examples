<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuditRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'audit_name'       => 'required|max:255',
            'audit_start_date' => 'required|date_format:d/m/Y',
            'audit_end_date'   => 'required|date_format:d/m/Y',
            'audit_number'     => 'required|max:255',
            'client_id'        => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'client_id' => 'client',
        ];
    }
}
