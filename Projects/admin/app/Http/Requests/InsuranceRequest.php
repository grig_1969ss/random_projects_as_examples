<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsuranceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'insurance_premium'      => 'numeric|nullable',
            'loss_of_rent'           => 'integer|nullable',
            'terrorism_premium'      => 'numeric|nullable',
            'insurance_renewal_date' => 'date_format:d/m/Y|nullable',
            'diary_date'             => 'date_format:d/m/Y|nullable',
        ];
    }
}
