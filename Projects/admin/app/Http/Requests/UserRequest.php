<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:60',
            //'email' => 'required|email|unique:users',
            'job_title' => 'string|max:255',
            //'avatar' => 'required|email|max:255',
            'client_id' => 'required|array',
            'region_id' => 'integer',
            'read_only' => 'between:0,1',
        ];
    }
}
