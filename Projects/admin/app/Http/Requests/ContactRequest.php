<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $contact = $this->route('contact');

        if ($contact) {
            $emailRule = "required|unique:contacts,email,{$contact->id}";
        } else {
            $emailRule = 'required|email|unique:contacts';
        }

        return [
            'first_name'   => 'required|max:100',
            'surname'      => 'required|max:100',
            'telephone'    => 'max:100',
            'fax'          => 'max:100',
            'email'		      => $emailRule,
            'mobile'       => 'max:100',
        ];
    }
}
