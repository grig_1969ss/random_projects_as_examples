<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'to' => 'required|email',
            'cc' => 'string|nullable',
            'subject' => 'required',
            'body' => 'required',
            'attachment_ids' => 'string|nullable',
            'attached_pdf_name' => 'string|nullable',
        ];
    }
}
