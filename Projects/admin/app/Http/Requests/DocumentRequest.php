<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_file' => 'file|required',
            'document_type_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'document_file' => 'file',
            'document_type_id' => 'document type',
        ];
    }
}
