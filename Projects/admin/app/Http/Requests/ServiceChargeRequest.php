<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceChargeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'percentage_apportion'               => 'max:100',
            'apportionment_type_id'              => 'required|integer|max:100',
            'service_charge_status_id'           => 'required|integer|max:100',
            'exclusions'                         => 'max:100',
            'invoiced'                           => 'max:100',
            'confirmed'                          => 'max:100',
            'budget_quarterly_payment'           => 'numeric|nullable',
            'actual_quarterly_payment'           => 'max:100',
            'year_end'                           => 'required|max:100',
            'year_end_cost_per_sq_ft'            => 'max:100',
            'balance_service_charge'             => 'numeric|nullable',
            'service_charge_budget_differential' => 'max:100',
            'service_charge_commencement_date'   => 'max:100',
            'agent_contributions'                => 'max:100',
            'potential_savings_against_budget'   => 'max:100',
            'amount_invoiced'                    => 'max:100',
            'date_invoiced'                      => 'max:100',
            'amount_received'                    => 'max:100',
            'date_received'                      => 'max:100',
            'budget_year_end'                    => 'max:100',
            'actual_year_end'                    => 'max:100',
            'actual_against_budget'              => 'max:100',
            'potential_budget'                   => 'max:100',
            'annual_budget'                      => 'numeric|nullable',
            'budget_quarterly_payment_months'    => 'required|integer|max:100',
        ];
    }

    public function attributes()
    {
        return [
            'apportionment_type_id'    => 'Method of Apportionment',
            'service_charge_status_id' => 'Service Charge Status',
            'agent_contributions'      => 'Managing Agent Contributions',
        ];
    }
}
