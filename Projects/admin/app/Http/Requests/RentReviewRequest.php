<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RentReviewRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'rent'                 => 'max:100',
            'arbitrator_or_expert' => 'max:100',
            'time_of_essence'      => 'max:100',
            'interest'             => 'max:100',
            'notice_by'            => 'required|max:100',
            'notice_amount'        => 'max:100',
            'notice_period'        => 'max:100',
            'turnover_rent'        => 'max:100',
            'turnover'             => 'max:100',
            'date'                 => 'required|max:100',
            'rent_review_pattern'  => 'max:100',
            'notice_dates'         => 'max:100',
            'no_floors'            => 'max:100',
            'itza'                 => 'max:100',
            'floors_occupied'      => 'max:100',
            'gross_area'           => 'max:100',
            'net_area'             => 'max:100',
            'bank'                 => 'max:100',
            'base_rate'            => 'max:100',
            'turnover_above'       => 'max:100',
            'base_rent'            => 'max:100',
            'notes'                => 'max:100',
        ];
    }

    public function attributes()
    {
        return [
            'arbitrator_or_expert' => 'Arbitrator / Expert',
        ];
    }
}
