<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $supplier = $this->route('supplier');

        $rules = [
            'name'              => 'required',
            'telephone'         => 'required',
            'address.address_1' => 'required',
            'address.post_code' => 'required',
        ];

        if (! $supplier) {
            $rules['contact.first_name'] = 'required';
            $rules['contact.surname'] = 'required';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'contact.first_name' => 'first name',
            'contact.surname'    => 'surname',
            'address.address_1'   => 'address line 1',
            'address.post_code'   => 'post code',
        ];
    }
}
