<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UtilityRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'payment_type' => 'required|integer',
            'property_id' => 'required|integer',
            'period' => 'string|nullable',
            'invoice_number' => 'string|nullable',
            'date_from_to' => 'string|nullable',
            'opening_read' => 'integer|nullable',
            'closing_read' => 'integer|nullable',
            'amount_net' => 'nullable|regex:/^-?\d+(\.\d{1,2})?$/',
            'archived' => 'boolean',
        ];
    }

    public function attributes()
    {
        return [
            'apportionment_type_id' => 'Method of Apportionment',
            'service_charge_status_id' => 'Service Charge Status',
            'agent_contributions' => 'Managing Agent Contributions',
        ];
    }
}
