<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BreakRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'break_clause_type_id' => 'required',
            'break_date'           => 'date_format:d/m/Y',
            'number_of_days'       => 'required',
            'notice_period'        => 'nullable|date_format:d/m/Y',
        ];
    }

    public function attributes()
    {
        return [
            'break_clause_type_id' => 'Break clause',
        ];
    }
}
