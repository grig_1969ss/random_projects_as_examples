<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClaimRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'invoice_date'       => 'nullable|date_format:d/m/Y',
            'original_net_value' => 'nullable|numeric',
            'vat'                => 'nullable|numeric',
            'claim_adjustment'   => 'nullable|numeric',
            'cancelled_claim'    => 'nullable|numeric',
            'client_date'        => 'nullable|date_format:d/m/Y',
        ];
    }
}
