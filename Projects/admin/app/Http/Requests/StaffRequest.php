<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $staff = $this->route('staff');

        if ($staff) {
            //dd($staff->id);
            $emailRules = "required|unique:users,id,{$staff->id}|max:100";
        // $usernameRules = "required|unique:users,username,{$staff->id}|max:100";
        } else {
            $emailRules = 'required|unique:users|max:100';
            $usernameRules = 'required|unique:users|max:100';
        }

        $rules = [
            'name'      => 'required|max:100',
            //  'username'  => $usernameRules,
            'email'     => $emailRules,
            'job_title' => 'max:100',
            'role_id'   => 'required|integer|max:100',
            'client_id' => 'required_if:role_id,2',
            'avatar'    => 'nullable|image',
        ];

        if (! $staff) {
            $rules['password'] = 'required|same:confirm_password|max:100';
            $rules['confirm_password'] = 'required|same:password|max:100';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'role_id'   => 'role',
            'client_id' => 'client',
        ];
    }

    public function messages()
    {
        return [
            'client_id.required_if' => 'The client field is required.',
        ];
    }
}
