<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReDiariseEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'diary_date' => 'required|date',
            'diarise_note' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'diarise_note' => 'Note',
        ];
    }
}
