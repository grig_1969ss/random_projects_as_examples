<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'payment_date'         => 'date_format:d/m/Y',
            'gross_invoice_amount' => 'numeric|nullable',
            'tax_amount'           => 'numeric|nullable',
            'payment_ref'          => 'required|max:100',
            'date_approved'        => 'date_format:d/m/Y|nullable',
        ];
    }

    public function attributes()
    {
        return [
            'payment_ref'   => 'payment reference',
            'date_approved' => 'approved date',
        ];
    }
}
