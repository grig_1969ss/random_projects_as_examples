<?php

namespace App\Repositories;

use App\Models\User;

/**
 * Class UsersRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'username',
        'password',
        'remember_token',
        'email',
        'job_title',
        'avatar',
        'client_id',
        'verified',
        'disabled',
        'deleted',
        'created_by',
        'updated_by',
        'region_id',
        'read_only',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return User::class;
    }

    public function __construct(User $model)
    {
        $this->model = $model;
    }
}
