<?php

namespace App\Repositories;

use App\Models\Permission;

/**
 * Class PermissionsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PermissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'description',
        'model',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Permission::class;
    }
}
