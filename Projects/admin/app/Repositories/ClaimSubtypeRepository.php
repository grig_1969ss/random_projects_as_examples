<?php

namespace App\Repositories;

use App\Models\ClaimSubtype;

/**
 * Class ClaimSubtypesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class ClaimSubtypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'claim_type_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return ClaimSubtype::class;
    }
}
