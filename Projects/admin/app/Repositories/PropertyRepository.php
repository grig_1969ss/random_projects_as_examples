<?php

namespace App\Repositories;

use App\Helpers\Util;
use Illuminate\Support\Facades\DB;

/**
 * Class PropertyRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PropertyRepository
{
    /**
     * Gets all the properties with their rateable value.
     *
     * @return \Illuminate\Support\Collection returns all the properties as objects
     */
    public function getAllPropertiesWithRateableValue($order, $dateFrom = null, $dateTo = null)
    {
        $clientQuery = Util::getClientIdQuery('AND');

        if ($dateFrom) {
            $dateQuery = " AND p.rent_review_date >= '".$dateFrom."' ";
        } else {
            $dateQuery = ' AND TRUE ';
        }

        if ($dateTo) {
            $dateQuery .= " AND p.rent_review_date <= '".$dateTo."' ";
        }

        if ($order === null) {
            $sortQuery = ' ORDER BY property_name';
        } else {
            $sortQuery = ' ORDER BY '.$order.', property_name';
        }

        $propertyQuery = DB::select(
            'SELECT p.id,
                p.property_name,
                p.reference,
                p.general_info,
                p.rent_review_date,
                p.lease_expiry_date,
                p.rateable_value,
                p.repair_liability,
                p.lease_expiry_date,
                p.lease_start_date,
                p.rent_review_date,
                p.repair_obligations_note,
                p.user_clause_note,
                breaks.break_date as break_option_date,
                breaks.notice_period as break_option_notice_period,
                documents.document_name as schedule_of_condition,
                rents.rent as current_rent,
                l.landlord_name,
                la.address_1 as landlord_address_1,
                la.address_2 as landlord_address_2,
                la.town as landlord_town,
                la.county as landlord_county,
                la.post_code as landlord_post_code,
                ag.agent_name as landlord_agent,
                ma.address_1 as agent_address_1,
                ma.address_2 as agent_address_2,
                ma.town as agent_town,
                ma.county as agent_county,
                ma.post_code as agent_post_code,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name
            FROM properties AS p
                LEFT JOIN regions
                    ON regions.id = p.region_id
                   LEFT JOIN addresses AS a
                          ON ( a.id = p.address_id )
                   LEFT JOIN landlords AS l
                    ON ( l.id = p.landlord_id )
                LEFT JOIN addresses AS la
                    ON ( la.id = l.address_id )
                LEFT JOIN agents AS ag
                    ON ( ag.id = p.agent_id )
                LEFT JOIN addresses AS ma
                    ON ( ma.id = ag.address_id )
                LEFT JOIN (
                        SELECT b.*
                        FROM (
                            SELECT DISTINCT property_id
                            FROM breaks) bo
                        JOIN breaks b
                        ON b.id = (
                            SELECT id
                            FROM   breaks bi
                            WHERE  bi.property_id = bo.property_id
                                AND bi.archived != 1
                            ORDER  BY break_date
                            LIMIT  1)
                    ) breaks
                    ON breaks.property_id = p.id
                LEFT JOIN (
                        SELECT d.*
                        FROM (
                            SELECT DISTINCT docable_id
                            FROM documents) do
                            JOIN documents d
                            ON d.id = (
                                SELECT id
                                FROM   documents di
                                WHERE  di.docable_id = do.docable_id
                                    AND di.document_type_id = 7
                                    AND di.archived != 1
                                ORDER  BY created_at desc
                                LIMIT  1)
                        ) documents
                    ON documents.docable_id = p.id AND documents.docable_id = p.id
                LEFT JOIN (
                    SELECT property_id, rent
                        FROM rents
                        WHERE exp_rent_date > NOW()
                        AND archived = 0
                        GROUP BY property_id
                        ORDER BY exp_rent_date
                    ) rents
                ON rents.property_id = p.id
            WHERE p.rateable_value IS NOT NULL'.$clientQuery.$dateQuery.$sortQuery
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);
        $properties = $this->formatPropertyAddresses($propertyCollection, 'agent');
        $properties = $this->formatPropertyAddresses($propertyCollection, 'landlord');

        return $properties;
    }

    /**
     * Gets all the properties with a schedule of maintenance.
     *
     * @return \Illuminate\Support\Collection returns all the properties as objects
     */
    public function getAllPropertiesWithSchedule($order)
    {
        if ($order === null) {
            $sortQuery = ' ORDER BY property_name';
        } else {
            $sortQuery = ' ORDER BY '.$order.', property_name';
        }

        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        $propertyQuery = DB::select(
            'SELECT p.id,
                   p.reference,
                   p.property_name,
                   d.document_name,
                   d.document_file,
                   a.address_1,
                   a.address_2,
                   a.town,
                   a.county,
                   a.post_code,
                   regions.name AS region_name
            FROM   documents AS d,
                   properties AS p
                   LEFT JOIN addresses AS a
                          ON ( a.id = p.address_id )
                   LEFT JOIN regions
                        ON  regions.id = p.region_id
            WHERE  p.id = d.docable_id AND d.docable_type = "App\\\\Models\\\\Property"
               AND d.document_type_id = 7'.$clientQuery.$sortQuery
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);

        return $properties;
    }

    public function getAllPropertiesDueRentReview($order, $dateFrom = null, $dateTo = null)
    {

        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        if ($dateFrom) {
            $dateQuery = " AND p.rent_review_date >= '".$dateFrom."' ";
        } else {
            $dateQuery = ' AND TRUE ';
        }

        if ($dateTo) {
            $dateQuery .= " AND p.rent_review_date <= '".$dateTo."' ";
        }

        if ($order === null) {
            $sortQuery = ' ORDER BY p.rent_review_date, property_name';
        } else {
            $sortQuery = ' ORDER BY '.$order.', property_name';
        }

        $propertyQuery = DB::select(
            'SELECT
                p.id,
                p.property_name,
                p.reference,
                p.general_info,
                p.rent_review_date,
                p.lease_expiry_date,
                p.rateable_value,
                p.repair_liability,
                p.lease_expiry_date,
                p.lease_start_date,
                p.rent_review_date,
                p.repair_obligations_note,
                p.user_clause_note,
                breaks.break_date as break_option_date,
                breaks.notice_period as break_option_notice_period,
                l.landlord_name,
                la.address_1 as landlord_address_1,
                la.address_2 as landlord_address_2,
                la.town as landlord_town,
                la.county as landlord_county,
                la.post_code as landlord_post_code,
                ag.agent_name as landlord_agent,
                ma.address_1 as agent_address_1,
                ma.address_2 as agent_address_2,
                ma.town as agent_town,
                ma.county as agent_county,
                ma.post_code as agent_post_code,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name
            FROM properties AS p
                LEFT JOIN regions
                    ON regions.id = p.region_id
                LEFT JOIN addresses AS a
                    ON ( a.id = p.address_id )
                LEFT JOIN landlords AS l
                    ON ( l.id = p.landlord_id )
                LEFT JOIN addresses AS la
                    ON ( la.id = l.address_id )
                LEFT JOIN agents AS ag
                    ON ( ag.id = p.agent_id )
                LEFT JOIN addresses AS ma
                    ON ( ma.id = ag.address_id )
                LEFT JOIN (
                        SELECT b.*
                        FROM (
                            SELECT DISTINCT property_id
                            FROM breaks) bo
                        JOIN breaks b
                        ON b.id = (
                            SELECT id
                            FROM   breaks bi
                            WHERE  bi.property_id = bo.property_id
                                AND bi.archived != 1
                            ORDER  BY break_date
                            LIMIT  1)
                    ) breaks
                    ON breaks.property_id = p.id
            WHERE p.property_status_id = 1'.$clientQuery.$dateQuery.$sortQuery
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);
        $properties = $this->formatPropertyAddresses($propertyCollection, 'agent');
        $properties = $this->formatPropertyAddresses($propertyCollection, 'landlord');

        return $properties;
    }

    public function getAllPropertiesBreakOptionReview($order, $dateFrom = null, $dateTo = null)
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        if ($dateFrom) {
            $dateQuery = " AND breaks.break_date >= '".$dateFrom."' ";
        } else {
            $dateQuery = ' AND TRUE ';
        }

        if ($dateTo) {
            $dateQuery .= " AND breaks.break_date <= '".$dateTo."' ";
        }

        if ($order === null) {
            $sortQuery = ' ORDER BY breaks.break_date, property_name';
        } else {
            $sortQuery = ' ORDER BY '.$order.', property_name';
        }

        $propertyQuery = DB::select(
            'SELECT
                p.id,
                p.property_name,
                p.reference,
                p.general_info,
                p.rent_review_date,
                p.lease_expiry_date,
                p.rateable_value,
                p.repair_liability,
                p.lease_expiry_date,
                p.lease_start_date,
                p.notice_dates,
                p.break_notice_period,
                p.rent_review_date,
                p.repair_obligations_note,
                p.user_clause_note,
                breaks.break_date as break_option_date,
                breaks.notice_period as break_option_notice_period,
                documents.document_name as schedule_of_condition,
                rents.rent as current_rent,
                l.landlord_name,
                la.address_1 as landlord_address_1,
                la.address_2 as landlord_address_2,
                la.town as landlord_town,
                la.county as landlord_county,
                la.post_code as landlord_post_code,
                ag.agent_name as landlord_agent,
                ma.address_1 as agent_address_1,
                ma.address_2 as agent_address_2,
                ma.town as agent_town,
                ma.county as agent_county,
                ma.post_code as agent_post_code,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name
            FROM properties AS p
                LEFT JOIN regions
                    ON regions.id = p.region_id
                LEFT JOIN addresses AS a
                    ON ( a.id = p.address_id )
                LEFT JOIN landlords AS l
                    ON ( l.id = p.landlord_id )
                LEFT JOIN addresses AS la
                    ON ( la.id = l.address_id )
                LEFT JOIN agents AS ag
                    ON ( ag.id = p.agent_id )
                LEFT JOIN addresses AS ma
                    ON ( ma.id = ag.address_id )
                LEFT JOIN (
                        SELECT b.*
                        FROM (
                            SELECT DISTINCT property_id
                            FROM breaks) bo
                        JOIN breaks b
                        ON b.id = (
                            SELECT id
                            FROM   breaks bi
                            WHERE  bi.property_id = bo.property_id
                                AND bi.archived != 1
                            ORDER  BY break_date
                            LIMIT  1)
                    ) breaks
                    ON breaks.property_id = p.id
                LEFT JOIN (
                        SELECT d.*
                        FROM (
                            SELECT DISTINCT docable_id
                            FROM documents) do
                            JOIN documents d
                            ON d.id = (
                                SELECT id
                                FROM   documents di
                                WHERE  di.docable_id = do.docable_id
                                    AND di.document_type_id = 7
                                    AND di.archived != 1
                                ORDER  BY created_at desc
                                LIMIT  1)
                        ) documents
                    ON documents.docable_id = p.id AND documents.docable_id = p.id
                LEFT JOIN (
                    SELECT property_id, rent
                        FROM rents
                        WHERE exp_rent_date > NOW()
                        AND archived = 0
                        GROUP BY property_id
                        ORDER BY exp_rent_date
                    ) rents
                ON rents.property_id = p.id
            WHERE p.property_status_id = 1
            AND breaks.id IS NOT NULL
                '.$clientQuery.$dateQuery.$sortQuery
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);
        $properties = $this->formatPropertyAddresses($propertyCollection, 'agent');
        $properties = $this->formatPropertyAddresses($propertyCollection, 'landlord');

        return $properties;
    }

    /**
     * Get all properties with expiring or expired leases given the date, the interval and the interval type.
     */
    public function getAllPropertiesDueLeaseExpiry($order, $dateFrom = null, $dateTo = null)
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');
        // get properties whose lease has expired or is due to expire

        if ($dateFrom) {
            $dateQuery = " AND p.lease_expiry_date >= '".$dateFrom."' ";
        } else {
            $dateQuery = ' AND TRUE ';
        }

        if ($dateTo) {
            $dateQuery .= " AND p.lease_expiry_date <= '".$dateTo."' ";
        }

        if ($order === null) {
            $sortQuery = ' ORDER BY property_name';
        } else {
            $sortQuery = ' ORDER BY '.$order.', property_name';
        }

        $propertyQuery = DB::select(
            'SELECT
                p.id,
                p.reference,
                p.property_name,
                p.general_info,
                p.rent_review_date,
                p.lease_expiry_date,
                p.rateable_value,
                p.repair_liability,
                p.lease_expiry_date,
                p.lease_start_date,
                p.rent_review_date,
                p.repair_obligations_note,
                p.user_clause_note,
                breaks.break_date as break_option_date,
                breaks.notice_period as break_option_notice_period,
                documents.document_name as schedule_of_condition,
                rents.rent as current_rent,
                l.landlord_name,
                la.address_1 as landlord_address_1,
                la.address_2 as landlord_address_2,
                la.town as landlord_town,
                la.county as landlord_county,
                la.post_code as landlord_post_code,
                ag.agent_name as landlord_agent,
                ma.address_1 as agent_address_1,
                ma.address_2 as agent_address_2,
                ma.town as agent_town,
                ma.county as agent_county,
                ma.post_code as agent_post_code,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name
            FROM properties AS p
                LEFT JOIN regions
                    ON regions.id = p.region_id
                LEFT JOIN addresses AS a
                    ON ( a.id = p.address_id )
                LEFT JOIN landlords AS l
                    ON ( l.id = p.landlord_id )
                LEFT JOIN addresses AS la
                    ON ( la.id = l.address_id )
                LEFT JOIN agents AS ag
                    ON ( ag.id = p.agent_id )
                LEFT JOIN addresses AS ma
                    ON ( ma.id = ag.address_id )
                LEFT JOIN (
                        SELECT b.*
                        FROM (
                            SELECT DISTINCT property_id
                            FROM breaks) bo
                        JOIN breaks b
                        ON b.id = (
                            SELECT id
                            FROM   breaks bi
                            WHERE  bi.property_id = bo.property_id
                                AND bi.archived != 1
                            ORDER  BY break_date
                            LIMIT  1)
                    ) breaks
                    ON breaks.property_id = p.id
                LEFT JOIN (
                        SELECT d.*
                        FROM (
                            SELECT DISTINCT docable_id
                            FROM documents) do
                            JOIN documents d
                            ON d.id = (
                                SELECT id
                                FROM   documents di
                                WHERE  di.docable_id = do.docable_id
                                    AND di.document_type_id = 7
                                    AND di.archived != 1
                                ORDER  BY created_at desc
                                LIMIT  1)
                        ) documents
                    ON documents.docable_id = p.id AND documents.docable_id = p.id
                LEFT JOIN (
                    SELECT property_id, rent
                        FROM rents
                        WHERE exp_rent_date > NOW()
                        AND archived = 0
                        GROUP BY property_id
                        ORDER BY exp_rent_date
                    ) rents
                ON rents.property_id = p.id
            WHERE p.property_status_id = 1'.$clientQuery.$dateQuery.$sortQuery
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);
        $properties = $this->formatPropertyAddresses($propertyCollection, 'agent');
        $properties = $this->formatPropertyAddresses($propertyCollection, 'landlord');

        return $properties;
    }

    public function getAllPropertiesMissingPayments($type, $order, $date, $interval = 0, $intervalType = 'MONTH')
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        $order = (isset($order)) ? $order.', property_name' : 'property_name';

        $column = ($type == 'Budget') ? 'budget_quarterly_payment' : 'actual_quarterly_payment';

        if ($date) {
            $dateQuery = Util::getDateQuery('sc.year_end', $date, $interval, $intervalType, 'AND');
        } else {
            $dateQuery = '';
        }

        $propertyQuery = DB::select(
            'SELECT p.id,
                       p.property_name,
                       p.reference,
                       sc.year_end,
                       a.address_1,
                       a.address_2,
                       a.town,
                       a.county,
                       a.post_code,
                        l.landlord_name,
                        la.address_1 as landlord_address_1,
                        la.address_2 as landlord_address_2,
                        la.town as landlord_town,
                        la.county as landlord_county,
                        la.post_code as landlord_post_code,
                        ag.agent_name as landlord_agent,
                        ma.address_1 as agent_address_1,
                        ma.address_2 as agent_address_2,
                        ma.town as agent_town,
                        ma.county as agent_county,
                        ma.post_code as agent_post_code,
                       regions.name AS region_name
                FROM   properties AS p
                       LEFT JOIN service_charges AS sc
                              ON ( sc.property_id = p.id )
                       LEFT JOIN addresses AS a
                              ON ( a.id = p.address_id )
                       LEFT JOIN regions
                            ON regions.id = p.region_id
                   LEFT JOIN landlords AS l
                    ON ( l.id = p.landlord_id )
                    LEFT JOIN addresses AS la
                        ON ( la.id = l.address_id )
                    LEFT JOIN agents AS ag
                        ON ( ag.id = p.agent_id )
                    LEFT JOIN addresses AS ma
                        ON ( ma.id = ag.address_id )
                WHERE  sc.id IS NOT NULL
                AND    (('.$column.' IS NULL) OR ('.$column.' = 0))'.$clientQuery.$dateQuery.'
                GROUP BY p.id
            ORDER BY '.$order.';'
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);
        $properties = $this->formatPropertyAddresses($propertyCollection, 'agent');
        $properties = $this->formatPropertyAddresses($propertyCollection, 'landlord');

        return $properties;
    }

    public function getAllPropertiesDocumentChecklist($order, $date = null, $interval = 0, $intervalType = 'MONTH')
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');
        // get properties whose lease has expired or is due to expire
        if ($date) {
            $dateQuery = Util::getDateQuery('p.lease_expiry_date', $date, $interval, $intervalType, 'AND');
        } else {
            $dateQuery = '';
        }

        if ($order === null) {
            $sortQuery = '';
        } else {
            $sortQuery = ' ORDER BY '.$order;
        }

        $propertyQuery = DB::select(
            'SELECT
                p.id,
                p.reference,
                p.property_name,
                p.lease_expiry_date,
                p.rateable_value,
                p.repair_liability,
                p.lease_expiry_date,
                p.lease_start_date,
                p.rent_review_date,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name,
                SUM(CASE WHEN d.document_type_id=1 THEN 1 ELSE 0 END) AS lease,
                SUM(CASE WHEN d.document_type_id=15 THEN 1 ELSE 0 END) AS sublease,
                SUM(CASE WHEN d.document_type_id=16 THEN 1 ELSE 0 END) AS licence_to_assign,
                SUM(CASE WHEN d.document_type_id=17 THEN 1 ELSE 0 END) AS licence_for_alterations,
                SUM(CASE WHEN d.document_type_id=18 THEN 1 ELSE 0 END) AS rent_review_memorandum,
                SUM(CASE WHEN (d.document_type_id=12 AND d.document_type_id=13 AND d.document_type_id=14) THEN 1 ELSE 0 END) AS landlord_notices,
                SUM(CASE WHEN d.document_type_id=7 THEN 1 ELSE 0 END) AS schedule_of_condition,
                SUM(CASE WHEN d.document_type_id=9 THEN 1 ELSE 0 END) AS schedule_of_dilapidations,
                SUM(CASE WHEN d.document_type_id=5 THEN 1 ELSE 0 END) AS demise_plan,
                SUM(CASE WHEN d.document_type_id=6 THEN 1 ELSE 0 END) AS goad_plan
            FROM properties AS p
                LEFT JOIN regions
                    ON regions.id = p.region_id
                LEFT JOIN addresses AS a
                    ON ( a.id = p.address_id )
                LEFT JOIN documents AS d
                    ON (d.docable_id = p.id AND d.docable_type = "App\\\\Models\\\\Property")
            WHERE p.property_status_id = 1'.$clientQuery.$dateQuery.
            ' GROUP BY p.id'.$sortQuery
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);

        return $properties;
    }

    /**
     * Gets all the properties with active maintenance.
     *
     * @return \Illuminate\Support\Collection returns all the properties as objects
     */
    public function getAllPropertiesWithActiveMaintenance($unresolvedOnly = true)
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        $propertyQuery = DB::select(
            'SELECT
                p.id,
                p.reference,
                p.property_name,
                m.notes,
                m.resolved,
                m.notice_date,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name
            FROM maintenance AS m
            LEFT JOIN properties AS p
            LEFT JOIN regions
                ON regions.id = p.region_id
            ON ( p.id = m.property_id )
            LEFT JOIN addresses AS a
            ON ( a.id = p.address_id )
            WHERE p.property_status_id = 1 '.$clientQuery.
            ($unresolvedOnly ? ' AND m.resolved = 0' : '')
        );

        $propertyCollection = collect($propertyQuery);

        $properties = $this->formatPropertyAddresses($propertyCollection);

        return $properties;
    }

    private function formatPropertyAddresses($properties, $type = null)
    {
        $prefix = null;

        switch ($type) {
            case 'landlord':
                $prefix = 'landlord_';
                break;
            case 'agent':
                $prefix = 'agent_';
                break;
        }

        $properties->transform(function ($item, $key) use ($prefix) {
            foreach ($item as $key => $value) {
                if ($key === $prefix.'address_1') {
                    $address = $value;
                }

                $fields = [$prefix.'address_2', $prefix.'town', $prefix.'county', $prefix.'post_code'];
                if (in_array($key, $fields)) {
                    $item->$key = isset($value) ? ', '.$value : $value;
                    $address .= $item->$key;
                }
            }

            $addressKey = $prefix.'address';

            $item->$addressKey = $address;

            return $item;
        });

        return $properties;
    }
}
