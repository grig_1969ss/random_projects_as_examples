<?php

namespace App\Repositories;

use App\Models\Rent;

/**
 * Class RentsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class RentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'rent',
        'exp_rent_date',
        'created_by',
        'updated_by',
        'archived',
        'vat',
        'contracted_rent',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Rent::class;
    }
}
