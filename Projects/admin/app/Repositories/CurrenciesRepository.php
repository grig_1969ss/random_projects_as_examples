<?php

namespace App\Repositories;

use App\Models\Currencies;

/**
 * Class CurrenciesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class CurrenciesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'currency_code',
        'symbol',
        'html_code',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Currencies::class;
    }
}
