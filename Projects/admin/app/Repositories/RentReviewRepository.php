<?php

namespace App\Repositories;

use App\Models\RentReview;

/**
 * Class RentReviewsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class RentReviewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'break_option',
        'date',
        'rent',
        'arbitrator_or_expert',
        'time_of_essence',
        'interest',
        'rent_review_pattern',
        'notice_by',
        'notice_period',
        'notice_amount',
        'turnover_rent',
        'notice_dates',
        'no_floors',
        'itza',
        'floors_occupied',
        'gross_area',
        'net_area',
        'bank_id',
        'base_rate',
        'turnover_above',
        'base_rent',
        'turnover',
        'notes',
        'created_by',
        'updated_by',
        'floor_areas_confirmed',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return RentReview::class;
    }
}
