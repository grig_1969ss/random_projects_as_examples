<?php

namespace App\Repositories;

use App\Models\Session;

/**
 * Class SessionsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class SessionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'last_activity',
        'data',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Session::class;
    }
}
