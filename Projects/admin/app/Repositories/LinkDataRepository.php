<?php

namespace App\Repositories;

use App\Models\LinkData;

/**
 * Class LinkDataRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class LinkDataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'expiry_date',
        'link_salt',
        'encrypted_id',
        'unique_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return LinkData::class;
    }
}
