<?php

namespace App\Repositories;

use App\Models\Document;

/**
 * Class DocumentsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class DocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'docable_id',
        'docable_type',
        'document_type_id',
        'document_name',
        'document_file',
        'content_type',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Document::class;
    }
}
