<?php

namespace App\Repositories;

use App\Models\RoleUser;

/**
 * Class RoleUserRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class RoleUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'role_id',
        'user_id',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return RoleUser::class;
    }
}
