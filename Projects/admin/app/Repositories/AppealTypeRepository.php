<?php

namespace App\Repositories;

use App\Models\AppealType;

/**
 * Class AppealTypesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AppealTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return AppealType::class;
    }
}
