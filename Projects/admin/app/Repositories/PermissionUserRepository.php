<?php

namespace App\Repositories;

use App\Models\PermissionUser;

/**
 * Class PermissionUserRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PermissionUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'permission_id',
        'user_id',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return PermissionUser::class;
    }
}
