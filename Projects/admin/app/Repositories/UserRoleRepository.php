<?php

namespace App\Repositories;

use App\Models\UserRole;

/**
 * Class UserRolesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class UserRoleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return UserRole::class;
    }
}
