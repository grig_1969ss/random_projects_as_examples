<?php

namespace App\Repositories;

use App\Models\LaravelMigrations;

/**
 * Class LaravelMigrationsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class LaravelMigrationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'batch',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return LaravelMigrations::class;
    }
}
