<?php

namespace App\Repositories;

use App\Http\Requests\DocumentTypeRequest;
use App\Models\DocumentType;
use Illuminate\Http\Request;

/**
 * Class DocumentTypeRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class DocumentTypeRepositoryOld extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * @return false|string
     */
    public function index()
    {
        $types = DocumentType::withTrashed()->cursor();

        return json_encode(
            view('admin.account.documentTypes.typesTable')->with('types', $types)
                ->toHtml()
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $type = DocumentType::find($request->id);
        $type->archived = true;
        $type->save();
        $type->delete();

        return back();
    }

    /**
     * @param DocumentTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DocumentTypeRequest $request)
    {
        $name = $request->name;
        $attachToEmail = $request->attach;

        $type = new DocumentType();
        $type->name = $name;
        $type->attach_to_email = $attachToEmail;
        $type->created_by = auth()->user()->id;
        $type->save();

        return back();
    }

    /**
     * @param DocumentTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DocumentTypeRequest $request)
    {
        $id = $request->id;
        $name = $request->name;
        $attachToEmail = $request->attach;

        $type = DocumentType::withTrashed()->find($id);
        $type->name = $name;
        $type->attach_to_email = $attachToEmail;
        $type->updated_by = auth()->user()->id;
        $type->save();

        return back();
    }

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return DocumentType::class;
    }
}
