<?php

namespace App\Repositories;

use App\Models\AuditRegister;

/**
 * Class AuditRegistersRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AuditRegisterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'audit_name',
        'audit_start_date',
        'audit_end_date',
        'audit_number',
        'administration',
        'client_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return AuditRegister::class;
    }
}
