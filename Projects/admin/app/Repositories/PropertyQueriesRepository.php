<?php

namespace App\Repositories;

use App\Models\PropertyQuery;

/**
 * Class PropertyQueriesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PropertyQueriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_date',
        'status_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return PropertyQuery::class;
    }
}
