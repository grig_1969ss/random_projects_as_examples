<?php

namespace App\Repositories;

use App\Models\EmailTemplate;

/**
 * Class DiariesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class EmailTemplateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = ['subject', 'body'];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return EmailTemplate::class;
    }
}
