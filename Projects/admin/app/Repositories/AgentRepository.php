<?php

namespace App\Repositories;

use App\Models\Agent;

/**
 * Class AgentsRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AgentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agent_name',
        'address_id',
        'telephone',
        'fax',
        'email',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Agent::class;
    }
}
