<?php

namespace App\Repositories;

use App\Models\Landlord;

/**
 * Class LandlordsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class LandlordRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'address_id',
        'landlord_name',
        'telephone',
        'fax',
        'email',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Landlord::class;
    }
}
