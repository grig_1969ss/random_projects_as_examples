<?php

namespace App\Repositories;

use App\Models\Claim;

/**
 * Class ClaimsRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class ClaimRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'claim_number',
        'supplier_number',
        'supplier_name',
        'invoice_number',
        'invoice_date',
        'original_net_value',
        'vat',
        'claim_adjustment',
        'cancelled_claim',
        'client_date',
        'po_number',
        'property_number',
        'property_address',
        'supplier_contact',
        'client_id',
        'audit_register_id',
        'auditor_id',
        'claim_type_id',
        'claim_subtype_id',
        'currency_id',
        'claim_status_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Claim::class;
    }
}
