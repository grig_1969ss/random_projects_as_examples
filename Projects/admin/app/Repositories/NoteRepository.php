<?php

namespace App\Repositories;

use App\Models\Note;

/**
 * Class NotesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class NoteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'object_type_id',
        'object_id',
        'note_type_id',
        'note_text',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Note::class;
    }
}
