<?php

namespace App\Repositories;

use App\Models\EmailTemplateType;

/**
 * Class DiariesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class EmailTemplateTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = ['name'];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return EmailTemplateType::class;
    }
}
