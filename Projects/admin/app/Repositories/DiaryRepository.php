<?php

namespace App\Repositories;

use App\Models\Diary;

/**
 * Class DiariesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class DiaryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_type_id',
        'diary_title',
        'date',
        'status',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Diary::class;
    }
}
