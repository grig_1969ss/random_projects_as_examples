<?php

namespace App\Repositories;

use App\Models\ClientQuery;

/**
 * Class ClientQueriesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class ClientQueryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_id',
        'client_query_status_id',
        'due_date',
        'details',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return ClientQuery::class;
    }
}
