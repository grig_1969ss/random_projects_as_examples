<?php

namespace App\Repositories;

use App\Models\InsuranceParty;

/**
 * Class InsurancePartiesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class InsurancePartyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return InsuranceParty::class;
    }
}
