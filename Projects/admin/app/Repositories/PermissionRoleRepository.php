<?php

namespace App\Repositories;

use App\Models\PermissionRole;

/**
 * Class PermissionRoleRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PermissionRoleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'permission_id',
        'role_id',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return PermissionRole::class;
    }
}
