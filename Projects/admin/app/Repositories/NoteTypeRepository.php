<?php

namespace App\Repositories;

use App\Models\NoteType;

/**
 * Class NoteTypesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class NoteTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return NoteType::class;
    }
}
