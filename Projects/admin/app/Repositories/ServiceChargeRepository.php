<?php

namespace App\Repositories;

use App\Models\ServiceCharge;

/**
 * Class ServiceChargesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class ServiceChargeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_id',
        'percentage_apportion',
        'apportionment_type_id',
        'year_end',
        'exclusions',
        'year_end_cost_per_sq_ft',
        'balance_service_charge',
        'service_charge_budget_differential',
        'service_charge_commencement_date',
        'agent_contributions',
        'potential_savings_against_budget',
        'amount_invoiced',
        'date_invoiced',
        'amount_received',
        'date_received',
        'budget_year_end',
        'wip',
        'lease_comments',
        'charge_details',
        'invoiced',
        'budget_quarterly_payment',
        'actual_quarterly_payment',
        'wip_amount',
        'actual_year_end',
        'actual_against_budget',
        'confirmed',
        'potential_budget',
        'annual_budget',
        'current_contribution',
        'forcast_contribution_year1',
        'current_turnover',
        'forcast_turnover_year1',
        'forcast_turnover_year2',
        'forcast_contribution_year2',
        'notes',
        'service_charge_status_id',
        'created_by',
        'updated_by',
        'vat',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return ServiceCharge::class;
    }
}
