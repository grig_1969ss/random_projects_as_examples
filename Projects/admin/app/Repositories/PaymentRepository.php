<?php

namespace App\Repositories;

use App\Models\Payment;

/**
 * Class PaymentsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'service_charge_id',
        'supplier_id',
        'payment_type_id',
        'diary_id',
        'contact_id',
        'payment_date',
        'gross_invoice_amount',
        'tax_amount',
        'payment_ref',
        'date_approved',
        'period',
        'quarter',
        'hold',
        'note',
        'description',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Payment::class;
    }
}
