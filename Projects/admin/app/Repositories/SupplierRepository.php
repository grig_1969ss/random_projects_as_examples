<?php

namespace App\Repositories;

use App\Models\Supplier;

/**
 * Class SuppliersRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class SupplierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'telephone',
        'fax',
        'email',
        'address_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Supplier::class;
    }
}
