<?php

namespace App\Repositories;

use App\Models\ApportionmentType;

/**
 * Class ApportionmentTypesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class ApportionmentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return ApportionmentType::class;
    }
}
