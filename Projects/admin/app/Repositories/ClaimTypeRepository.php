<?php

namespace App\Repositories;

use App\Models\ClaimType;

/**
 * Class ClaimTypesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class ClaimTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'archived',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return ClaimType::class;
    }
}
