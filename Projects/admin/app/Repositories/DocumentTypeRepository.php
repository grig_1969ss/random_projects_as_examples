<?php

namespace App\Repositories;

use App\Models\DocumentType;

/**
 * Class DocumentTypeRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class DocumentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return DocumentType::class;
    }
}
