<?php

namespace App\Repositories;

use App\Models\AlienationType;

/**
 * Class AlienationTypesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AlienationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**.
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return AlienationType::class;
    }
}
