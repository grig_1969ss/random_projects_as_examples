<?php

namespace App\Repositories;

use App\Models\Region;

/**
 * Class RegionsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class RegionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'manager',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Region::class;
    }
}
