<?php

namespace App\Repositories;

use App\Models\Client;

/**
 * Class ClientsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class ClientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_name',
        'address_id',
        'telephone',
        'fax',
        'email',
        'logo',
        'is_administrator',
        'currency_id',
        'administrator_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Client::class;
    }
}
