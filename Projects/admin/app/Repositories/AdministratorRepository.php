<?php

namespace App\Repositories;

use App\Models\Administrator;

/**
 * Class AdministratorsRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AdministratorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Administrator::class;
    }
}
