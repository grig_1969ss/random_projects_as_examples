<?php

namespace App\Repositories;

use App\Models\Breaks;

/**
 * Class BreaksRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class BreaksRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_id',
        'break_date',
        'number_of_days',
        'break_clause',
        'created_by',
        'updated_by',
        'archived',
        'break_clause_type_id',
        'notice_period',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Breaks::class;
    }
}
