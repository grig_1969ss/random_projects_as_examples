<?php

namespace App\Repositories;

use App\Models\MaintenancePhoto;

/**
 * Class MaintenancePhotosRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class MaintenancePhotoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'maintenance_id',
        'document_name',
        'document_file',
        'content_type',
        'archived',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return MaintenancePhoto::class;
    }
}
