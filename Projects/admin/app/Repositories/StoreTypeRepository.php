<?php

namespace App\Repositories;

use App\Models\StoreType;

/**
 * Class StoreTypesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class StoreTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return StoreType::class;
    }
}
