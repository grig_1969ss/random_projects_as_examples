<?php

namespace App\Repositories;

use App\Models\Maintenance;

/**
 * Class MaintenanceRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class MaintenanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_id',
        'notice_date',
        'notes',
        'resolved',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Maintenance::class;
    }
}
