<?php

namespace App\Repositories;

use App\Models\Insurance;

/**
 * Class InsurancesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class InsuranceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_id',
        'diary_id',
        'covered_by',
        'block_policy',
        'insurance_premium',
        'loss_of_rent',
        'terrorism_cover',
        'terrorism_premium',
        'plate_glass',
        'tenant_reimburses',
        'valuation_fee_payable',
        'period_of_insurance',
        'insurer',
        'insurance_renewal_date',
        'notes',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Insurance::class;
    }
}
