<?php

namespace App\Repositories;

use App\Models\PaymentType;

/**
 * Class PaymentTypesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PaymentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'service_charge',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return PaymentType::class;
    }
}
