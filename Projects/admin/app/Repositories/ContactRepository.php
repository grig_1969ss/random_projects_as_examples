<?php

namespace App\Repositories;

use App\Models\Contact;

/**
 * Class ContactsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class ContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contactable_id',
        'contactable_type',
        'first_name',
        'surname',
        'telephone',
        'fax',
        'email',
        'mobile',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Contact::class;
    }
}
