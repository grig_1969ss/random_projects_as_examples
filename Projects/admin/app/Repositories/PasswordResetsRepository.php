<?php

namespace App\Repositories;

use App\Models\PasswordResets;

/**
 * Class PasswordResetsRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PasswordResetsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'token',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return PasswordResets::class;
    }
}
