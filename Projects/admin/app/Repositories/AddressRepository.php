<?php

namespace App\Repositories;

use App\Models\Address;

/**
 * Class AddressesRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'address_1',
        'address_2',
        'town',
        'county',
        'post_code',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Address::class;
    }
}
