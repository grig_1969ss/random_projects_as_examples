<?php

namespace App\Repositories;

use App\Models\PropertyStatus;

/**
 * Class PropertyStatusesRepository.
 * @version October 29, 2019, 3:32 am UTC
 */
class PropertyStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'button',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return PropertyStatus::class;
    }
}
