<?php

namespace App\Repositories;

use App\Models\Auditor;

/**
 * Class AuditorsRepository.
 * @version October 29, 2019, 3:31 am UTC
 */
class AuditorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'created_by',
        'updated_by',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Auditor::class;
    }
}
