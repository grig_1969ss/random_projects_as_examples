<?php

namespace App\Queries;

use App\Models\Property;

class PropertiesByCompany
{
    public function get($type, $id, $status = null)
    {
        $query = Property::leftJoin('property_statuses', 'properties.property_status_id', '=', 'property_statuses.id')
            ->leftJoin('clients', 'clients.id', '=', 'properties.client_id')
            ->leftJoin('agents', 'agents.id', '=', 'properties.agent_id')
            ->leftJoin('landlords', 'landlords.id', '=', 'properties.landlord_id')
            ->leftJoin('addresses', 'addresses.id', '=', 'properties.address_id')
            ->leftJoin('regions', 'properties.region_id', '=', 'regions.id');

        $query->where($this->DbFieldByType($type), '=', $id);

        $user = auth()->user();

        if ($user->client_id) {
            $query->where('properties.client_id', '=', $user->client_id);
        }

        if ($status) {
            $query->where('property_statuses.id', $status);
        }

        $properties = $query->get(
            [
                'properties.id',
                'properties.property_name',
                'properties.reference',
                'addresses.post_code',
                'clients.client_name',
                'agents.agent_name',
                'landlords.landlord_name',
                'property_statuses.name',
                'property_statuses.button',
                'regions.name as region_name',
            ]
        );

        return $properties;
    }

    private function dbFieldByType(string $type)
    {
        switch ($type) {
            case 'client':
                return 'properties.client_id';

            case 'agent':
                return 'properties.agent_id';

            case 'landlord':
                return 'properties.landlord_id';
        }
    }
}
