<?php

namespace App\Models;

use App\Presenters\CaseCallPresenter;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class CaseCall extends BaseModel
{
    use CreateAndUpdateTrait, SoftDeletes, PresentableTrait;

    protected $presenter = CaseCallPresenter::class;

    public $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'call_type_id',
        'note',
        'case_id',
    ];

//    public function case()
//    {
//        return $this->belongsTo(CaseModel::class);
//    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

//    public function actionType()
//    {
//        return $this->belongsTo(ActionType::class, 'call_type_id');
//    }
}
