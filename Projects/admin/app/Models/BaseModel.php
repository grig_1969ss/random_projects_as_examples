<?php

namespace App\Models;

use App\Observers\ModifierStamper;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();

        self::observe(new ModifierStamper());
    }

    public function toDropdown($fieldName = 'name', $idField = 'id', $selectLabel = 'Select...')
    {
        return static::orderBy($fieldName)->get()->pluck($fieldName, $idField)->prepend($selectLabel, '');
    }
}
