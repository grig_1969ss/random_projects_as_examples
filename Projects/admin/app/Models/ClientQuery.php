<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;

/**
 * Class ClientQueries.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property int property_id
 * @property int diary_id
 * @property int client_query_status_id
 * @property string|\Carbon\Carbon due_date
 * @property string details
 * @property int created_by
 * @property int updated_by
 */
class ClientQuery extends BaseModel
{
    use CreateAndUpdateTrait, DocumentTrait;

    public $table = 'client_queries';
    public $documentStoreRoute = 'client-queries.documents.store';
    public $modelNameHuman = 'Client Query';

    public $fillable = [
        'property_id',
        'diary_id',
        'client_query_status_id',
        'due_date',
        'details',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'diary_id' => 'integer',
        'client_query_status_id' => 'integer',
        'due_date' => 'datetime',
        'details' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function clientQueryStatus()
    {
        return $this->belongsTo(ClientQueryStatus::class, 'client_query_status_id');
    }

    public function diary()
    {
        return $this->belongsTo(Diary::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getAllClientQueriesByPropertyId($propertyId)
    {
        return self::with('clientQueryStatus')
            ->where('property_id', '=', $propertyId)
            ->orderBy('due_date', 'desc')
            ->get();
    }

    public function getClientQueryById($id)
    {
        return self::with([
            'diary',
        ])->find($id);
    }
}
