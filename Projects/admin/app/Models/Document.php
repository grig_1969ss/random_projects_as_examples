<?php

namespace App\Models;

use App\Presenters\BasePresenter;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Documents.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int docable_id
 * @property string docable_type
 * @property int document_type_id
 * @property int property_id
 * @property string document_name
 * @property string document_file
 * @property string content_type
 * @property int created_by
 * @property int updated_by
 * @property bool archived
 */
class Document extends BaseModel implements Searchable
{
    use CreateAndUpdateTrait, PresentableTrait;

    public $table = 'documents';
    protected $presenter = BasePresenter::class;

    public $searchableType = 'Document';

    public $fillable = [
        'docable_id',
        'docable_type',
        'document_type_id',
        'document_name',
        'document_file',
        'content_type',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'docable_id' => 'integer',
        'docable_type' => 'string',
        'document_type_id' => 'integer',
        'document_name' => 'string',
        'document_file' => 'string',
        'content_type' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'archived' => 'boolean',
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('documents.show', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->document_name,
            $url
        );
    }

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public static function getDocumentsByType($type)
    {
        $documents = self::where('document_type_id', '=', $type)->get();

        return $documents;
    }

    public function getAllDocumentsByObjectId($objectId, $objectTypeId)
    {
        $documents = self::with(['user', 'document_type'])
            ->where('object_id', '=', $objectId)
            ->where('object_type_id', '=', $objectTypeId)
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        return $documents;
    }

    public function getAllDocumentsByPropertyId($propertyId)
    {
        $documents = self::leftJoin('document_types as dt', 'dt.id', '=', 'documents.document_type_id')
            ->leftJoin('users as u', 'documents.created_by', '=', 'u.id')
            ->where('documents.docable_id', $propertyId)
            ->where('documents.docable_type', 'App\\Models\\Property')
            ->where('documents.archived', false)
            ->orderBy('documents.document_type_id')
            ->select([
                'documents.id',
                'documents.document_name',
                'documents.document_file',
                'documents.created_at',
                'u.name as user_name',
                'dt.name as type_name',
                'documents.document_type_id',
            ])
            ->get();

        foreach ($documents as $document) {
            switch ($document->document_type_id) {
                case 1:
                    $documentTables['Leases'][] = $document;
                    break;
                case 15:
                    $documentTables['Subleases'][] = $document;
                    break;
                case 16:
                    $documentTables['Licence to Assign'][] = $document;
                    break;
                case 17:
                    $documentTables['Licence for Alterations'][] = $document;
                    break;
                case 18:
                    $documentTables['Rent Review Memorandum'][] = $document;
                    break;
                case 12:
                case 13:
                case 14:
                    $documentTables['Landlord Notices'][] = $document;
                    break;
                case 7:
                    $documentTables['Schedule of Conditions'][] = $document;
                    break;
                case 9:
                    $documentTables['Schedule of Dilapidations'][] = $document;
                    break;
                case 5:
                    $documentTables['Demise Plan'][] = $document;
                    break;
                case 6:
                    $documentTables['Goad Plan'][] = $document;
                    break;
                default:
                    break;
            }
        }

        return isset($documentTables) ? $documentTables : [];
    }

    public function getUrl()
    {
        return Storage::url($this->document_file);
    }

    public function archive()
    {
        return $this->update(['archived' => true]);
    }

    public function getTypeNameAttribute()
    {
        if ($this->documentType) {
            return $this->documentType->name;
        }

        return '';
    }

    public function docable()
    {
        return $this->morphTo();
    }

    public function type()
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id');
    }

    public function getExtensionAttribute()
    {
        $array = explode('.', $this->file_path);

        return end($array);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            DB::table('documents')
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = DB::table('documents')
                ->insertGetId($data);
        }

        return $id;
    }

    public function getAllPairs()
    {
        return self::where('archived', '=', 0)->orderBy('name')->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
