<?php

namespace App\Models;

/**
 * Class Addresses.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection administrators
 * @property \Illuminate\Database\Eloquent\Collection suppliers
 * @property string address_1
 * @property string address_2
 * @property string town
 * @property string county
 * @property string post_code
 * @property int created_by
 * @property int updated_by
 */
class Address extends BaseModel
{
    public $table = 'addresses';

    public $fillable = [
        'address_1',
        'address_2',
        'town',
        'county',
        'post_code',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'address_1' => 'string',
        'address_2' => 'string',
        'town' => 'string',
        'county' => 'string',
        'post_code' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function agent()
    {
        return $this->hasOne(Agent::class);
    }

    public function landlord()
    {
        return $this->hasOne(Landlord::class);
    }

    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function property()
    {
        return $this->hasOne(Property::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this->insertGetId($data);
        }

        return $id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function administrators()
    {
        return $this->hasMany(Administrator::class, 'address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function suppliers()
    {
        return $this->hasMany(Supplier::class, 'address_id');
    }
}
