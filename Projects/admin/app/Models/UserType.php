<?php

namespace App\Models;

/**
 * Class UserTypes.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 * @property bool archived
 */
class UserType extends BaseModel
{
    public $table = 'user_types';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'archived' => 'boolean',
    ];

    public function property()
    {
        return $this->hasOne(Property::class);
    }

    public function getAllUserTypes()
    {
        return $this->orderBy('name')->get(['id', 'name']);
    }

    public function getNameByUserTypeId($userTypeId)
    {
        return self::where('id', '=', $userTypeId)->only('name');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function deleteType($id)
    {
        $this->delete($id);
    }

    public function getAllPairs()
    {
        return self::where('archived', '=', 0)->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
