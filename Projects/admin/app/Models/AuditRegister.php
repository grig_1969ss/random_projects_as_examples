<?php

namespace App\Models;

/**
 * Class AuditRegisters.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property \App\Models\Client client
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property \Illuminate\Database\Eloquent\Collection claims
 * @property string audit_name
 * @property string audit_start_date
 * @property string audit_end_date
 * @property string audit_number
 * @property bool administration
 * @property int client_id
 * @property int created_by
 * @property int updated_by
 */
class AuditRegister extends BaseModel
{
    public $table = 'audit_registers';

    public $fillable = [
        'audit_name',
        'audit_start_date',
        'audit_end_date',
        'audit_number',
        'administration',
        'client_id',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'audit_name' => 'string',
        'audit_start_date' => 'date',
        'audit_end_date' => 'date',
        'audit_number' => 'string',
        'administration' => 'boolean',
        'client_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'audit_name' => 'required',
        'audit_start_date' => 'required',
        'audit_end_date' => 'required',
        'audit_number' => 'required',
        'administration' => 'required',
        'client_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claims()
    {
        return $this->hasMany(Claim::class, 'audit_register_id');
    }
}
