<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;

/**
 * Class Breaks.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property int property_id
 * @property int diary_id
 * @property string|\Carbon\Carbon break_date
 * @property string number_of_days
 * @property string break_clause
 * @property int created_by
 * @property int updated_by
 * @property int archived
 * @property int break_clause_type_id
 * @property string|\Carbon\Carbon notice_period
 */
class Breaks extends BaseModel
{
    use CreateAndUpdateTrait, DocumentTrait;

    public $table = 'breaks';
    public $documentStoreRoute = 'breaks.documents.store';
    public $modelNameHuman = 'Break Option';

    public $fillable = [
        'property_id',
        'diary_id',
        'break_date',
        'number_of_days',
        'break_clause',
        'created_by',
        'updated_by',
        'archived',
        'break_clause_type_id',
        'notice_period',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'diary_id' => 'integer',
        'break_date' => 'datetime',
        'number_of_days' => 'string',
        'break_clause' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'archived' => 'integer',
        'break_clause_type_id' => 'integer',
        'notice_period' => 'datetime',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function breakType()
    {
        return $this->belongsTo(BreakType::class, 'break_clause_type_id');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getAllBreaksByPropertyId($propertyId)
    {
        $break = self::with('breakType')
            ->where('property_id', '=', $propertyId)
            ->where('archived', '=', '0')
            ->orderBy('break_date', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();

        return $break;
    }

    public function getByBreakId($id)
    {
        return self::find($id);
    }
}
