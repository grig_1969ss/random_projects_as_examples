<?php

namespace App\Models;

/**
 * Class PermissionUser.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Permission permission
 * @property \App\Models\User user
 * @property int permission_id
 * @property int user_id
 */
class PermissionUser extends BaseModel
{
    public $table = 'permission_user';

    public $fillable = [
        'permission_id',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'permission_id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'permission_id' => 'required',
        'user_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
