<?php

namespace App\Models;

use App\Presenters\PropertyRelatedPresenter;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Rents.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int property_id
 * @property number rent
 * @property string|\Carbon\Carbon exp_rent_date
 * @property int created_by
 * @property int updated_by
 * @property bool archived
 * @property bool vat
 * @property number contracted_rent
 */
class Rent extends BaseModel
{
    use CreateAndUpdateTrait, PresentableTrait;

    public $table = 'rents';
    public $presenter = PropertyRelatedPresenter::class;

    public $fillable = [
        'property_id',
        'rent',
        'exp_rent_date',
        'created_by',
        'updated_by',
        'archived',
        'vat',
        'contracted_rent',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'rent' => 'float',
        'exp_rent_date' => 'datetime',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'archived' => 'boolean',
        'vat' => 'boolean',
        'contracted_rent' => 'float',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getAllByPropertyId($id)
    {
        return self::where('property_id', '=', $id)
            ->where('archived', '=', 0)
            ->orderBy('exp_rent_date')
            ->get();
    }

    public function getById($id)
    {
        return self::find($id);
    }

    public static function getCurrentRents()
    {
        $query = DB::select(
            'SELECT property_id, rent
                FROM rents
                WHERE exp_rent_date > NOW()
                AND archived = 0
                GROUP BY property_id
                ORDER BY exp_rent_date'
        );

        return $query;
    }

    /**
     * Gets the rent id closest to todays date.
     *
     * @param int $id the property id
     */
    public function getCurrentRentByPropertyId($id)
    {
        $query = DB::select(
            'SELECT *
				FROM rents
				WHERE exp_rent_date > NOW()
				AND property_id = ?
				AND archived = 0
				ORDER BY exp_rent_date
				LIMIT 1',
            [$id]
        );

        return (isset($query[0])) ? $query[0] : 0;
    }

    /**
     * Gets all the active rents for rental liability comparison.
     *
     * @return array returns all rents
     */
    public static function getRentalLiabilityAll()
    {
        $sql
            = 'SELECT *
			FROM   rents
			ORDER  BY property_id,
			          exp_rent_date;';

        return DB::select($sql);
    }

    /**
     * Gets all the active rents for rental liability comparison.
     *
     * @return array returns all rents
     */
    public static function getRentalLiabilityByPropertyId($id)
    {
        $sql = '
            SELECT *
            FROM rents
            WHERE rents.property_id='.$id.'
            ORDER  BY property_id, exp_rent_date;';

        return DB::select($sql);
    }
}
