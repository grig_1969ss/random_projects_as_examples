<?php

namespace App\Models;

use App\Presenters\InsurancePresenter;
use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Insurances.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\InsuranceParty coveredBy
 * @property \App\Models\User createdBy
 * @property \App\Models\Diary diary
 * @property \App\Models\InsuranceParty plateGlass
 * @property \App\Models\Property property
 * @property \App\Models\User updatedBy
 * @property int property_id
 * @property int diary_id
 * @property int covered_by
 * @property bool block_policy
 * @property number insurance_premium
 * @property int loss_of_rent
 * @property bool terrorism_cover
 * @property number terrorism_premium
 * @property int plate_glass
 * @property bool tenant_reimburses
 * @property bool valuation_fee_payable
 * @property string period_of_insurance
 * @property string insurer
 * @property string insurance_renewal_date
 * @property string notes
 * @property int created_by
 * @property int updated_by
 */
class Insurance extends BaseModel
{
    use SoftDeletes, CreateAndUpdateTrait, PresentableTrait, DocumentTrait;

    public $table = 'insurances';
    protected $presenter = InsurancePresenter::class;
    protected $dates = ['insurance_renewal_date'];
    public $documentStoreRoute = 'insurances.documents.store';
    public $modelNameHuman = 'Insurance';

    public $fillable = [
        'property_id',
        'franchisee_updated',
        'diary_id',
        'covered_by',
        'block_policy',
        'insurance_premium',
        'loss_of_rent',
        'terrorism_cover',
        'terrorism_premium',
        'plate_glass',
        'tenant_reimburses',
        'valuation_fee_payable',
        'period_of_insurance',
        'insurer',
        'insurance_renewal_date',
        'cover_note_received',
        'notes',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'diary_id' => 'integer',
        'covered_by' => 'integer',
        'block_policy' => 'boolean',
        'insurance_premium' => 'float',
        'loss_of_rent' => 'integer',
        'terrorism_cover' => 'boolean',
        'terrorism_premium' => 'float',
        'plate_glass' => 'integer',
        'tenant_reimburses' => 'boolean',
        'valuation_fee_payable' => 'boolean',
        'period_of_insurance' => 'string',
        'insurer' => 'string',
        'insurance_renewal_date' => 'date',
        'cover_note_received' => 'boolean',
        'notes' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'property_id' => 'required',
        'block_policy' => 'required',
        'terrorism_cover' => 'required',
        'tenant_reimburses' => 'required',
        'valuation_fee_payable' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function coveredBy()
    {
        return $this->belongsTo(InsuranceParty::class, 'covered_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function diary()
    {
        return $this->belongsTo(Diary::class, 'diary_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function plateGlass()
    {
        return $this->belongsTo(InsuranceParty::class, 'plate_glass');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

    public function document()
    {
        return $this->morphMany(Document::class, 'docable')->where('archived', false)->orderByDesc('created_at');
    }

    public function diaries()
    {
        return $this->morphMany(Diary::class, 'diaryable')->orderByDesc('created_at');
    }
}
