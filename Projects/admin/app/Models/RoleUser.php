<?php

namespace App\Models;

/**
 * Class RoleUser.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Role role
 * @property \App\Models\User user
 * @property int role_id
 * @property int user_id
 */
class RoleUser extends BaseModel
{
    public $table = 'role_user';

    public $fillable = [
        'role_id',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'role_id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
