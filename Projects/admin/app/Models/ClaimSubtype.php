<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ClaimSubtypes.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property \App\Models\ClaimType claimType
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property \Illuminate\Database\Eloquent\Collection claims
 * @property string name
 * @property int claim_type_id
 * @property int created_by
 * @property int updated_by
 */
class ClaimSubtype extends BaseModel
{
    use SoftDeletes;

    public $table = 'claim_subtypes';

    public $fillable = [
        'name',
        'claim_type_id',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'claim_type_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'claim_type_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function claimType()
    {
        return $this->belongsTo(ClaimType::class, 'claim_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claims()
    {
        return $this->hasMany(Claim::class, 'claim_subtype_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
