<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Support\Facades\DB;

/**
 * Class Diaries.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection insurances
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property int property_id
 * @property int diary_type_id
 * @property string diary_title
 * @property string|\Carbon\Carbon date
 * @property string status
 * @property int created_by
 * @property int updated_by
 */
class Diary extends BaseModel
{
    use CreateAndUpdateTrait;

    public $table = 'diaries';

    public $fillable = [
        'diaryable_id',
        'diaryable_type',
        'comments',
        'property_id',
        'diary_type_id',
        'diary_title',
        'date',
        'status',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'diaryable_id' => 'integer',
        'diaryable_type' => 'string',
        'comments' => 'string',
        'property_id' => 'integer',
        'diary_type_id' => 'integer',
        'diary_title' => 'string',
        'date' => 'datetime',
        'status' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function diaryType()
    {
        return $this->belongsTo(DiaryType::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function serviceCharge()
    {
        return $this->hasOne(ServiceCharge::class);
    }

    public function clientQuery()
    {
        return $this->hasOne(ClientQuery::class);
    }

    public function maintenance()
    {
        return $this->hasOne(Maintenance::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            DB::table('diaries')
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = DB::table('diaries')
                ->insertGetId($data);
        }

        return $id;
    }

    public static function getAllDiariesByPropertyId($propertyId)
    {
        $diaries = self::with(['user', 'diary_type'])
            ->where('property_id', '=', $propertyId)
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        return $diaries;
    }

    public static function getAllDiariesByClientId($clientId)
    {
        $diaries = DB::select("select * FROM (
			select diaries.*, users.name, diary_types.name as diary_type_name from users, diary_types, properties, diaries
			LEFT JOIN service_charges
			ON (diaries.property_id = service_charges.property_id)
			where diaries.updated_by = users.id
			and service_charges.wip != ''
			and diaries.diary_type_id = 1
			and diaries.property_id = properties.id
			and properties.client_id = ".$clientId.'
			and diaries.diary_type_id = diary_types.id
			and service_charges.id
			order by updated_at desc
			) as sc
			union
			(
			select diaries.*, users.name, diary_types.name as diary_type_name from diaries, users, properties, diary_types
			where diaries.updated_by = users.id
			and diaries.diary_type_id != 1
			and properties.client_id = '.$clientId.'
			and diaries.property_id = properties.id
			and diaries.diary_type_id = diary_types.id
			order by updated_at desc
			);');

        return $diaries;
    }

    public static function getAllActiveDiaries($typeId = null)
    {
        // dd(request()->get('type'));

        $user = auth()->user();
        $where = $whereMy = null;

        // filter by client if client_id is set
        if ($user->client_id) {
            $where = ' AND properties.client_id = "'.$user->client_id.'"';
        }

        if (request()->get('type') == 'my') {
            $whereMy = " AND (capa_owner_id = {$user->id} OR capa_owner2_id = {$user->id}) ";
        }

        $serviceChargesPartial = '
            join service_charges sc on sc.diary_id = d.id
            where d.diary_type_id = 1
            and sc.service_charge_status_id <> 5';

        $clientQueriesPartial = '
            join client_queries cq on cq.diary_id = d.id
            where d.diary_type_id = 2
            and cq.client_query_status_id <> 2';

        $approvalsPartial = '
            join payments p on p.diary_id = d.id
            where d.diary_type_id = 6
            and p.hold = 1';

        $insurancesPartial = '
		    join insurances i on i.diary_id = d.id
            where d.diary_type_id = 7';

        $emailsPartial = '
		    join emails e on e.diary_id = d.id
            where d.diary_type_id = 8';

        $baseSql = "
            select d.*, properties.capa_owner_id, properties.capa_owner2_id,
                   (select name from users where id = {$user->id}) as name,
                   (select name from diary_types where id = d.diary_type_id) as diary_type_name
            from diaries d
                 join properties on properties.id = d.property_id
                 __PARTIAL__
              and d.updated_by = {$user->id}
              and properties.property_status_id = 1
              {$where} {$whereMy}
            order by d.date desc";

        $serviceChargesSql = str_replace('__PARTIAL__', $serviceChargesPartial, $baseSql);
        $clientQueriesSql = str_replace('__PARTIAL__', $clientQueriesPartial, $baseSql);
        $approvalsSql = str_replace('__PARTIAL__', $approvalsPartial, $baseSql);
        $insurancesSql = str_replace('__PARTIAL__', $insurancesPartial, $baseSql);
        $emailsSql = str_replace('__PARTIAL__', $emailsPartial, $baseSql);

        if ($typeId) {
            if ($typeId == 1) {
                $sql = $serviceChargesSql;
            } elseif ($typeId == 2) {
                $sql = $clientQueriesSql;
            } elseif ($typeId == 4) {
                $sql = "
                SELECT
                    diaries.id,
                    diaries.diaryable_id,
                    diaries.diaryable_type,
                    diaries.comments,
                    diaries.property_id,
                    diaries.diary_type_id,
                    diaries.diary_title,
                    diaries.date as diary_date,
                    diaries.status ,
                    diaries.created_by,
                    diaries.updated_by,
                    diaries.created_at,
                    diaries.updated_at,
                    users.name,
                    properties.capa_owner_id,
                     properties.capa_owner2_id,
                    diary_types.name as diary_type_name,
                    maintenance.notice_date as date
                FROM
                    users,
                    diary_types,
                    diaries
				LEFT JOIN properties
    				ON (diaries.property_id = properties.id)
                LEFT JOIN maintenance
                    ON diaries.diary_type_id = 4 AND maintenance.diary_id=diaries.id AND maintenance.property_id=diaries.property_id
				where diaries.updated_by = users.id
				and diaries.diary_type_id = $typeId
				and diaries.diary_type_id = diary_types.id
                AND properties.property_status_id = 1
                and (diaries.diary_type_id = 4 AND maintenance.resolved != 1)
                ".$where.$whereMy.'
				order by updated_at desc';
            } elseif ($typeId == 5) {
                $sql = 'select diaries.*, users.name, properties.capa_owner_id, properties.capa_owner2_id,  diary_types.name as diary_type_name from users, diary_types, diaries
                LEFT JOIN properties
                    ON (diaries.property_id = properties.id)
                LEFT JOIN breaks
                    ON diaries.diary_type_id = 5 AND breaks.diary_id=diaries.id AND breaks.property_id=diaries.property_id
                where diaries.updated_by = users.id
                and diaries.diary_type_id != 1
                and diaries.diary_type_id = diary_types.id
                AND properties.property_status_id = 1
                and (breaks.notice_period >= NOW() AND breaks.archived != 1)
                '.$where.$whereMy.'
                order by updated_at desc';
            } elseif ($typeId == 6) {
                $sql = $approvalsSql;
            } elseif ($typeId == 7) {
                $sql = $insurancesSql;
            }
        } else {
            $sql = "select * FROM
			({$serviceChargesSql}) as sc
			union
			(
			SELECT
                diaries.id,
                diaries.diaryable_id,
                diaries.diaryable_type,
                diaries.comments,
                diaries.property_id,
                diaries.diary_type_id,
                diaries.diary_title,
                maintenance.notice_date as date,
                diaries.status ,
                diaries.created_by,
                diaries.updated_by,
                diaries.created_at,
                diaries.updated_at,
                users.name,
                 properties.capa_owner_id,
                  properties.capa_owner2_id,
                diary_types.name as diary_type_name
            FROM
                users,
                diary_types,
                diaries
			LEFT JOIN properties
    			ON (diaries.property_id = properties.id)
            LEFT JOIN maintenance
                ON diaries.diary_type_id=4 AND maintenance.diary_id=diaries.id AND maintenance.property_id=diaries.property_id
			where diaries.updated_by = users.id
			and diaries.diary_type_id != 1
			and diaries.diary_type_id = diary_types.id
            AND properties.property_status_id = 1
            and (diaries.diary_type_id = 4 AND maintenance.resolved != 1)
            ".$where.$whereMy.'
			order by updated_at desc
			)
            union
            (
            select diaries.*, users.name, properties.capa_owner_id, properties.capa_owner2_id, diary_types.name as diary_type_name from users, diary_types, diaries
            LEFT JOIN properties
                ON (diaries.property_id = properties.id)
            LEFT JOIN breaks
                ON diaries.diary_type_id = 5 AND breaks.diary_id=diaries.id AND breaks.property_id=diaries.property_id
            where diaries.updated_by = users.id
            and diaries.diary_type_id != 1
            and diaries.diary_type_id = diary_types.id
            AND properties.property_status_id = 1
            and (breaks.notice_period >= NOW() AND breaks.archived != 1)
            '.$where.$whereMy."
            order by updated_at desc
            )
            union
            ({$approvalsSql})
            union
            ({$clientQueriesSql})
            union
            ({$insurancesSql})
            union
            ({$emailsSql})";
        }

        $diaries = DB::select($sql);

        return $diaries;
    }

    //total count of active diary items either overdue or due today
    public static function getAllDiariesDueTodayOrOverdue()
    {
        $currentDate = date('Y-m-d');

        return self::where('status', 1)->where('date', '>', $currentDate)->count('id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurances()
    {
        return $this->hasMany(\App\Models\Insurance::class, 'diary_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payments()
    {
        return $this->hasMany(Payment::class, 'diary_id');
    }

    public function diaryable()
    {
        return $this->morphTo();
    }
}
