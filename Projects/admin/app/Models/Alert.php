<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;

/**
 * Class Agents.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property int object_type_id
 * @property int object_id
 * @property string message
 * @property bool active
 * @property int created_by
 * @property int updated_by
 */
class Alert extends BaseModel
{
    use CreateAndUpdateTrait;

    protected $fillable = ['property_id', 'object_type_id', 'object_id', 'message', 'active', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
