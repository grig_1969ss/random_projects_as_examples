<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DocumentTypes.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 */
class DocumentType extends BaseModel
{
    use SoftDeletes;

    public $table = 'document_types';

    public $fillable = [
        'id',
        'name',
        'attach_to_email',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [

    ];

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function getAllDocumentTypes()
    {
        return $this->orderBy('name')->get(['id', 'name']);
    }

    public function getNameByDocumentTypeId($documentTypeId)
    {
        return self::where('id', '=', $documentTypeId)->only('name');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function deleteType($id)
    {
        $this->delete($id);
    }

    public function getAllPairs()
    {
        return self::orderBy('name')->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
