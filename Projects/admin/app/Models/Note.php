<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;

/**
 * Class Notes.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int object_type_id
 * @property int object_id
 * @property int note_type_id
 * @property string note_text
 * @property int created_by
 * @property int updated_by
 */
class Note extends BaseModel
{
    use CreateAndUpdateTrait;

    public $table = 'notes';

    public $fillable = [
        'object_type_id',
        'object_id',
        'note_type_id',
        'note_text',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'object_type_id' => 'integer',
        'object_id' => 'integer',
        'note_type_id' => 'integer',
        'note_text' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function noteType()
    {
        return $this->belongsTo(NoteType::class, 'note_type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->find($id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public static function getAllNotesByObjectId($objectId, $objectTypeId)
    {
        $property = Property::find($objectId);

        $serviceCharges = $property->serviceCharge->pluck('id');
        $insurances = $property->insurances->pluck('id');
        $utilities = $property->utilities->pluck('id');

        return self::with(['user', 'noteType'])
            ->where(['object_id' => $objectId, 'object_type_id' => $objectTypeId])
            ->orWhere(function ($query) use ($serviceCharges) {
                $query->whereIn('object_id', $serviceCharges)->where('object_type_id', 8);
            })
            ->orWhere(function ($query) use ($insurances) {
                $query->whereIn('object_id', $insurances)->where('object_type_id', 9);
            })
            ->orWhere(function ($query) use ($utilities) {
                $query->whereIn('object_id', $utilities)->where('object_type_id', 11);
            })
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public static function noteExists($propertyId, $noteText)
    {
        if ($noteText == 'Service Charge was Edited') {
            return false;
        }

        $note = self::where('object_type_id', '=', 1)->where('object_id', '=', $propertyId)->orderBy('id', 'desc')->first();
        if (! $note) {
            return false;
        }

        if ($note->note_text != $noteText) {
            return false;
        }

        return true;
    }
}
