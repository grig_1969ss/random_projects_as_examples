<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;

/**
 * Class Maintenance.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int property_id
 * @property int diary_id
 * @property string|\Carbon\Carbon notice_date
 * @property string notes
 * @property bool resolved
 * @property int created_by
 * @property int updated_by
 */
class Maintenance extends BaseModel
{
    use CreateAndUpdateTrait, DocumentTrait;

    public $table = 'maintenance';
    public $documentStoreRoute = 'maintenances.documents.store';
    public $modelNameHuman = 'Maintenance';

    public $fillable = [
        'property_id',
        'diary_id',
        'notice_date',
        'notes',
        'resolved',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'diary_id' => 'integer',
        'notice_date' => 'datetime',
        'notes' => 'string',
        'resolved' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function diary()
    {
        return $this->belongsTo(Diary::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function maintenancePhotos()
    {
        return $this->hasMany(MaintenancePhoto::class, 'maintenance_id');
    }

    public function getAllMaintenaceByPropertyId($propertyId)
    {
        $maintenance = self::with('maintenancePhotos')
            ->where('property_id', '=', $propertyId)
            ->orderBy('notice_date', 'desc')
            ->get();

        return $maintenance;
    }

    public function getAllUnresolvedMaintenaceByPropertyId($propertyId)
    {
        $maintenance = self::where('property_id', '=', $propertyId)
            ->where('resolved', '=', '0')
            ->orderBy('notice_date', 'desc')
            ->get();

        return $maintenance;
    }

    public function getMaintenanceById($id)
    {
        return self::with([
            'diary',
        ])->find($id);
    }
}
