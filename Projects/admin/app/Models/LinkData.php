<?php

namespace App\Models;

/**
 * Class LinkData.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int user_id
 * @property string|\Carbon\Carbon expiry_date
 * @property string link_salt
 * @property string encrypted_id
 * @property string unique_id
 * @property int created_by
 * @property int updated_by
 */
class LinkData extends BaseModel
{
    public $table = 'link_data';

    public $fillable = [
        'user_id',
        'expiry_date',
        'link_salt',
        'encrypted_id',
        'unique_id',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'expiry_date' => 'datetime',
        'link_salt' => 'string',
        'encrypted_id' => 'string',
        'unique_id' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'expiry_date' => 'required',

    ];
}
