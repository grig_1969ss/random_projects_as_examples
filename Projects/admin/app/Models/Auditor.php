<?php

namespace App\Models;

/**
 * Class Auditors.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property \Illuminate\Database\Eloquent\Collection claims
 * @property string name
 * @property string email
 * @property int created_by
 * @property int updated_by
 */
class Auditor extends BaseModel
{
    public $table = 'auditors';

    public $fillable = [
        'name',
        'email',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public static $rules = [
        'name' => 'required',
        'email' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claims()
    {
        return $this->hasMany(Claim::class, 'auditor_id');
    }
}
