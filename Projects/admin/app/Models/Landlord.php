<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Support\Facades\DB;

/**
 * Class Landlords.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int address_id
 * @property string landlord_name
 * @property string telephone
 * @property \App\Models\Contact contact
 * @property string fax
 * @property string email
 * @property int created_by
 * @property int updated_by
 */
class Landlord extends BaseModel
{
    use CreateAndUpdateTrait;

    public $table = 'landlords';
    public $createContactRoute = 'landlords.contacts.store';
    public $contactElementName;

    const PAGINATE_COUNT = 10;

    public $fillable = [
        'address_id',
        'landlord_name',
        'telephone',
        'fax',
        'email',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'address_id' => 'integer',
        'landlord_name' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function toDropdown($fieldName = 'landlord_name', $idField = 'id', $selectLabel = 'No Landlord')
    {
        return parent::toDropdown($fieldName, $idField, $selectLabel);
    }

    public function contact()
    {
        return $this->morphMany(Contact::class, 'contactable');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function property()
    {
        return $this->hasMany(Property::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $instance = $this
                ->create($data);
            $id = $instance->id;
        }

        return $id;
    }

    public function getShowUrl()
    {
        return route('landlords.show', $this);
    }

    public function getShowName()
    {
        return $this->landlord_name;
    }

    public static function getLandlords()
    {
        $select = [
            'landlords.*',
            DB::raw('MIN(contacts.id) as contact_id'),
            'contacts.first_name',
            'contacts.surname',
            'contacts.telephone as contact_telephone',
            'contacts.email as contact_email',
            'properties.property_name as property',
            'properties.id as property_id', ];

        $landlords = self::leftJoin('contacts', 'landlords.id', '=', 'contacts.landlord_id')
            ->leftJoin('properties', 'landlords.id', '=', 'properties.landlord_id')
            ->groupBy('landlords.id')
            ->distinct()
            ->select($select)
            ->paginate(self::PAGINATE_COUNT);

        return $landlords;
    }

    public static function getLandlordsByClient($client)
    {
        $select = [
            'landlords.*',
            DB::raw('MIN(contacts.id) as contact_id'),
            'contacts.first_name',
            'contacts.surname',
            'contacts.telephone as contact_telephone',
            'contacts.email as contact_email',
            'properties.property_name as property',
            'properties.id as property_id',
        ];

        $landlords = self::leftJoin('contacts', 'landlords.id', '=', 'contacts.contactable_id')
            ->leftJoin('properties', 'landlords.id', '=', 'properties.landlord_id')
            ->where('properties.client_id', '=', $client)
            ->where('contacts.contactable_type', 'App\\Models\\Landlord')
            ->groupBy('landlords.id')
            ->distinct()
            ->select($select)
            ->paginate(self::PAGINATE_COUNT);

        return $landlords;
    }
}
