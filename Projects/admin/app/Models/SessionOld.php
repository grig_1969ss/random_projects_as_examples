<?php

namespace App\Models;

/**
 * Class SessionsOld.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int last_activity
 * @property string data
 */
class SessionOld extends BaseModel
{
    public $table = 'sessions_old';

    public $fillable = [
        'last_activity',
        'data',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'last_activity' => 'integer',
        'data' => 'string',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'last_activity' => 'required',
        'data' => 'required',
    ];
}
