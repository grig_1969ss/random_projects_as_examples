<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ClaimTypes.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property \Illuminate\Database\Eloquent\Collection claimSubtypes
 * @property \Illuminate\Database\Eloquent\Collection claims
 * @property string name
 * @property bool archived
 * @property int created_by
 * @property int updated_by
 */
class ClaimType extends BaseModel
{
    use SoftDeletes;

    public $table = 'claim_types';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
        'archived',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'archived' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'archived' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimSubtypes()
    {
        return $this->hasMany(ClaimSubtype::class, 'claim_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claims()
    {
        return $this->hasMany(Claim::class, 'claim_type_id');
    }
}
