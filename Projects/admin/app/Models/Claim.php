<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Claims.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property \App\Models\AuditRegister auditRegister
 * @property \App\Models\Auditor auditor
 * @property \App\Models\ClaimStatus claimStatus
 * @property \App\Models\Currencies currency
 * @property \App\Models\ClaimSubtype claimSubtype
 * @property \App\Models\ClaimType claimType
 * @property \App\Models\Client client
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property int claim_number
 * @property string supplier_number
 * @property string supplier_name
 * @property string invoice_number
 * @property string invoice_date
 * @property number original_net_value
 * @property number vat
 * @property number claim_adjustment
 * @property number cancelled_claim
 * @property string client_date
 * @property string po_number
 * @property string property_number
 * @property string property_address
 * @property string supplier_contact
 * @property int client_id
 * @property int audit_register_id
 * @property int auditor_id
 * @property int claim_type_id
 * @property int claim_subtype_id
 * @property int currency_id
 * @property int claim_status_id
 * @property int created_by
 * @property int updated_by
 */
class Claim extends BaseModel
{
    public $table = 'claims';

    public $fillable = [
        'claim_number',
        'supplier_number',
        'supplier_name',
        'invoice_number',
        'invoice_date',
        'original_net_value',
        'vat',
        'claim_adjustment',
        'cancelled_claim',
        'client_date',
        'po_number',
        'property_number',
        'property_address',
        'supplier_contact',
        'client_id',
        'audit_register_id',
        'auditor_id',
        'claim_type_id',
        'claim_subtype_id',
        'currency_id',
        'claim_status_id',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'claim_number' => 'integer',
        'supplier_number' => 'string',
        'supplier_name' => 'string',
        'invoice_number' => 'string',
        'invoice_date' => 'date',
        'original_net_value' => 'float',
        'vat' => 'float',
        'claim_adjustment' => 'float',
        'cancelled_claim' => 'float',
        'client_date' => 'date',
        'po_number' => 'string',
        'property_number' => 'string',
        'property_address' => 'string',
        'supplier_contact' => 'string',
        'client_id' => 'integer',
        'audit_register_id' => 'integer',
        'auditor_id' => 'integer',
        'claim_type_id' => 'integer',
        'claim_subtype_id' => 'integer',
        'currency_id' => 'integer',
        'claim_status_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function auditRegister()
    {
        return $this->belongsTo(AuditRegister::class, 'audit_register_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function auditor()
    {
        return $this->belongsTo(Auditor::class, 'auditor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function claimStatus()
    {
        return $this->belongsTo(ClaimStatus::class, 'claim_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function claimSubtype()
    {
        return $this->belongsTo(ClaimSubtype::class, 'claim_subtype_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function claimType()
    {
        return $this->belongsTo(ClaimType::class, 'claim_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function currency()
    {
        return $this->belongsTo(Currencies::class, 'currency_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinCurrencies($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'currencies as cu') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('currencies as cu', 'cu.id', '=', 'claims.currency_id')
            ->select(['claims.*', 'cu.html_code', 'cu.symbol']);
    }

    public function getGrossValueAttribute()
    {
        return $this->original_net_value + $this->vat;
    }

    public function getNetRecoveryAttribute()
    {
        return $this->original_net_value - $this->claim_adjustment - $this->cancelled_claim;
    }

    public static function generateClaimNumber()
    {
        $lastClaim = self::orderByDesc('id')->select(['claim_number'])->first();

        if ($lastClaim) {
            return $lastClaim->claim_number + 1;
        }

        return 1;
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinClients($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'clients as c') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('clients as c', 'c.id', '=', 'claims.client_id')
            ->select('claims.*');
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinAuditRegisters($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'audit_registers as ar') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('audit_registers as ar', 'ar.id', '=', 'claims.audit_register_id')
            ->select('claims.*');
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinAuditors($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'auditors as a') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('auditors as a', 'a.id', '=', 'claims.auditor_id')
            ->select('claims.*');
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinClaimTypes($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'claim_types as ct') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('claim_types as ct', 'ct.id', '=', 'claims.claim_type_id')
            ->select('claims.*');
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinClaimSubtypes($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'claim_subtypes as cs') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('claim_subtypes as cs', 'cs.id', '=', 'claims.claim_subtype_id')
            ->select('claims.*');
    }

    /**
     * @param Builder|Claim $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinClaimStatuses($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'claim_statuses as cst') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('claim_statuses as cst', 'cst.id', '=', 'claims.claim_status_id')
            ->select('claims.*');
    }

    /**
     * @param Claim $query
     * @param string $columnName
     *
     * @return Builder
     */
    public function scopeCustomOrderBy($query, $columnName)
    {
        if ($columnName == 'client') {
            $query = $query->joinClients();
            $columnName = 'c.client_name';
        } elseif ($columnName == 'audit_register') {
            $query = $query->joinAuditRegisters();
            $columnName = 'ar.audit_name';
        } elseif ($columnName == 'auditor') {
            $query = $query->joinAuditors();
            $columnName = 'a.name';
        } elseif ($columnName == 'claim_type') {
            $query = $query->joinClaimTypes();
            $columnName = 'ct.name';
        } elseif ($columnName == 'claim_subtype') {
            $query = $query->joinClaimSubtypes();
            $columnName = 'cs.name';
        } elseif ($columnName == 'claim_status') {
            $query = $query->joinClaimStatuses();
            $columnName = 'cst.name';
        }

        return $query->orderBy($columnName);
    }

    /**
     * @param Builder|Claim $query
     *
     * @return Builder
     */
    public function scopeFilterByClient($query)
    {
        $clientId = auth()->user()->client_id;

        if ($clientId) {
            return $query->where('claims.client_id', $clientId);
        }

        return $query;
    }
}
