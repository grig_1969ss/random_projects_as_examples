<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes; /**
 * Class Utility.
 *
 * @property int property_id
 * @property string|null period
 * @property string|null invoice_number
 * @property string|null date_from_to
 * @property int|null opening_read
 * @property int|null closing_read
 * @property float|null amount_net
 * @property bool archived
 * @property int|null created_by
 * @property int|null updated_by
 */
class Utility extends BaseModel
{
    use SoftDeletes, CreateAndUpdateTrait;

    public $table = 'utilities';

    public $fillable = [
        'payment_type',
        'property_id',
        'period',
        'invoice_number',
        'date_from_to',
        'opening_read',
        'closing_read',
        'amount_net',
        'archived',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function payment()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type');
    }
}
