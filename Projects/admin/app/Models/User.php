<?php

namespace App\Models;

use App\Presenters\UserPresenter;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Users.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection administrators
 * @property \Illuminate\Database\Eloquent\Collection auditRegisters
 * @property \Illuminate\Database\Eloquent\Collection auditors
 * @property \Illuminate\Database\Eloquent\Collection claimStatuses
 * @property \Illuminate\Database\Eloquent\Collection claimSubtypes
 * @property \Illuminate\Database\Eloquent\Collection claimTypes
 * @property \Illuminate\Database\Eloquent\Collection claims
 * @property \Illuminate\Database\Eloquent\Collection currencies
 * @property \Illuminate\Database\Eloquent\Collection insuranceParties
 * @property \Illuminate\Database\Eloquent\Collection insurances
 * @property \Illuminate\Database\Eloquent\Collection ownerTypes
 * @property \Illuminate\Database\Eloquent\Collection permissionUsers
 * @property \Illuminate\Database\Eloquent\Collection roleUsers
 * @property \Illuminate\Database\Eloquent\Collection storeTypes
 * @property \Illuminate\Database\Eloquent\Collection suppliers
 * @property string name
 * @property string username
 * @property string password
 * @property string remember_token
 * @property string email
 * @property string job_title
 * @property string avatar
 * @property int client_id
 * @property bool verified
 * @property bool disabled
 * @property bool deleted
 * @property int created_by
 * @property int updated_by
 * @property int region_id
 * @property bool read_only
 */
class User extends Authenticatable
{
    const CLIENT = 'client';

    use Notifiable, HasRoleAndPermission, CreateAndUpdateTrait, PresentableTrait, SoftDeletes;

    public $table = 'users';
    protected $presenter = UserPresenter::class;

    public $fillable = [
        'name',
        'username',
        'password',
        //  'remember_token',
        'email',
        'job_title',
        'avatar',
        'verified',
        'disabled',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
        'region_id',
        'read_only',
    ];

    public $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'username' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'email' => 'string',
        'job_title' => 'string',
        'avatar' => 'string',
        'verified' => 'boolean',
        'disabled' => 'boolean',
        'deleted' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'region_id' => 'integer',
        'read_only' => 'boolean',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function getNameByUserId($userId)
    {
        return self::where('id', '=', $userId)->select('name');
    }

    public function getAllUsers()
    {
        return $this->get(['id', 'name']);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public static function getStaffById($id)
    {
        /*     $staff = DB::select(
                 'SELECT
                             users.*,
                             clients.client_name,
                             roles.name AS role_name,
                             creator.name AS creator_name
                         FROM users
                         LEFT JOIN role_user ru ON ru.user_id = users.id
                         LEFT JOIN roles
                         ON ( roles.id = ru.role_id)
                         LEFT JOIN clients
                         ON ( clients.id = users.client_id)
                         LEFT JOIN users AS creator
                         ON ( users.created_by = creator.id)
                         WHERE users.id = ? ', [$id]
             );

             return $staff[0];*/

        return self::with('clients:client_name')->where('users.id', $id)
            ->leftJoin('role_user as ru', 'ru.user_id', 'users.id')
            ->leftJoin('roles', 'roles.id', 'ru.role_id')
            ->leftJoin('users as creator', 'users.created_by', 'creator.id')
            ->select(
                'users.*',
                'roles.name AS role_name',
                'creator.name AS creator_name'
            )
            ->first();
    }

    public function getAll()
    {
        return self::with('role')->get();
    }

    public function getUserByUsername($username)
    {
        return self::where('username', '=', $username)->first();
    }

    public function getByUserId($userId)
    {
        return self::find($userId);
    }

    public function isClient()
    {
        return $this->hasRole(self::CLIENT);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function currencyHtmlCode()
    {
        $client = $this->client;

        if ($client && $client->currency) {
            return $client->currency->html_code;
        }

        return '&pound;';
    }

    /**
     * The clients that belong to the user.
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class);
    }

    public function landlord()
    {
        return $this->hasOne(Landlord::class);
    }

    public function contact()
    {
        return $this->hasOne(Contact::class);
    }

    public function note()
    {
        return $this->hasOne(Note::class);
    }

    public function agent()
    {
        return $this->hasOne(Agent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function administrators()
    {
        return $this->hasMany(Administrator::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function administratorsUpdatedBy()
    {
        return $this->hasMany(Administrator::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function auditRegisters()
    {
        return $this->hasMany(AuditRegister::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function auditRegistersUpdatedBy()
    {
        return $this->hasMany(AuditRegister::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function auditors()
    {
        return $this->hasMany(Auditor::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function auditorsUpdatedBy()
    {
        return $this->hasMany(Auditor::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimStatuses()
    {
        return $this->hasMany(ClaimStatus::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimStatussUpdatedBy()
    {
        return $this->hasMany(ClaimStatus::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimSubtypes()
    {
        return $this->hasMany(ClaimSubtype::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimSubtypesUpdatedBy()
    {
        return $this->hasMany(ClaimSubtype::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimTypes()
    {
        return $this->hasMany(ClaimType::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimTypesUpdatedBy()
    {
        return $this->hasMany(ClaimType::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claims()
    {
        return $this->hasMany(Claim::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claimsUpdatedBy()
    {
        return $this->hasMany(Claim::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function currencies()
    {
        return $this->hasMany(Currency::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function currenciesUpdatedBy()
    {
        return $this->hasMany(Currency::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insuranceParties()
    {
        return $this->hasMany(InsuranceParty::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurancePartiesUpdatedBy()
    {
        return $this->hasMany(InsuranceParty::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurances()
    {
        return $this->hasMany(Insurance::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurancesUpdatedBy()
    {
        return $this->hasMany(Insurance::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ownerTypes()
    {
        return $this->hasMany(OwnerType::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ownerTypesUpdatedBy()
    {
        return $this->hasMany(OwnerType::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function storeTypes()
    {
        return $this->hasMany(StoreType::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function storeTypesUpdatedBy()
    {
        return $this->hasMany(StoreType::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function suppliers()
    {
        return $this->hasMany(Supplier::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function suppliersUpdatedBy()
    {
        return $this->hasMany(Supplier::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function permissionUsers()
    {
        return $this->hasMany(PermissionUser::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function roleUsers()
    {
        return $this->hasMany(RoleUser::class, 'user_id');
    }

    public function addRole(Role $role)
    {
        $this->role()->associate($role);
        $this->save();
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class);
    }
}
