<?php

namespace App\Models;

/**
 * Class Roles.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRoles
 * @property \Illuminate\Database\Eloquent\Collection roleUsers
 * @property string name
 * @property string slug
 * @property string description
 * @property int level
 * @property int created_by
 * @property int updated_by
 */
class Role extends BaseModel
{
    public $table = 'roles';

    public $fillable = [
        'name',
        'slug',
        'description',
        'level',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'level' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'level' => 'required',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function permissionRoles()
    {
        return $this->hasMany(PermissionRole::class, 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function roleUsers()
    {
        return $this->hasMany(RoleUser::class, 'role_id');
    }
}
