<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string html_code
 * @property string symbol
 */
class Currency extends BaseModel
{
    use SoftDeletes, CreateAndUpdateTrait;

    protected $fillable = ['name', 'currency_code', 'symbol', 'html_code'];

    public function getAllPairs()
    {
        return self::get(['id', 'name']);
    }
}
