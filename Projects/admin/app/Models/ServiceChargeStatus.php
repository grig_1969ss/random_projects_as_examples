<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceChargeStatuses.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property string button
 * @property int created_by
 * @property int updated_by
 */
class ServiceChargeStatus extends BaseModel
{
    use SoftDeletes;

    public $table = 'service_charge_statuses';

    public $fillable = [
        'name',
        'button',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'button' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function clientQuery()
    {
        return $this->hasOne(ServiceCharge::class);
    }

    public function getNameByServiceChargeStatusId($serviceChargeStatusId)
    {
        return self::where('id', '=', $serviceChargeStatusId)->only('name');
    }
}
