<?php

namespace App\Models;

use App\Presenters\PropertyRelatedPresenter;
use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class RentReviews.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int property_id
 * @property string break_option
 * @property string|\Carbon\Carbon date
 * @property string rent
 * @property string arbitrator_or_expert
 * @property bool time_of_essence
 * @property bool interest
 * @property string rent_review_pattern
 * @property string notice_by
 * @property string notice_period
 * @property string notice_amount
 * @property bool turnover_rent
 * @property string|\Carbon\Carbon notice_dates
 * @property string no_floors
 * @property string itza
 * @property string floors_occupied
 * @property string gross_area
 * @property string net_area
 * @property int bank_id
 * @property string base_rate
 * @property string turnover_above
 * @property string base_rent
 * @property string turnover
 * @property string notes
 * @property int created_by
 * @property int updated_by
 * @property int floor_areas_confirmed
 */
class RentReview extends BaseModel
{
    use CreateAndUpdateTrait, PresentableTrait, DocumentTrait;

    public $table = 'rent_reviews';
    protected $presenter = PropertyRelatedPresenter::class;
    public $documentStoreRoute = 'rent-reviews.documents.store';
    public $modelNameHuman = 'Rent Review';

    public $fillable = [
        'property_id',
        'break_option',
        'date',
        'rent',
        'arbitrator_or_expert',
        'time_of_essence',
        'interest',
        'rent_review_pattern',
        'notice_by',
        'notice_period',
        'notice_amount',
        'turnover_rent',
        'notice_dates',
        'no_floors',
        'itza',
        'floors_occupied',
        'gross_area',
        'net_area',
        'bank_id',
        'base_rate',
        'turnover_above',
        'base_rent',
        'turnover',
        'notes',
        'created_by',
        'updated_by',
        'floor_areas_confirmed',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'break_option' => 'string',
        'date' => 'datetime',
        'rent' => 'string',
        'arbitrator_or_expert' => 'string',
        'time_of_essence' => 'boolean',
        'interest' => 'boolean',
        'rent_review_pattern' => 'string',
        'notice_by' => 'string',
        'notice_period' => 'string',
        'notice_amount' => 'string',
        'turnover_rent' => 'boolean',
        'notice_dates' => 'datetime',
        'no_floors' => 'string',
        'itza' => 'string',
        'floors_occupied' => 'string',
        'gross_area' => 'string',
        'net_area' => 'string',
        'bank_id' => 'integer',
        'base_rate' => 'string',
        'turnover_above' => 'string',
        'base_rent' => 'string',
        'turnover' => 'string',
        'notes' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'floor_areas_confirmed' => 'integer',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getAllRentReviewsByPropertyId($propertyId)
    {
        $rentReview = self::where('property_id', '=', $propertyId)
            ->orderBy('date', 'desc')
            ->get();

        return $rentReview;
    }

    public function getRentReviewById($id)
    {
        return self::find($id);
    }
}
