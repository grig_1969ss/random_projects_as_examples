<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class EmailTemplate extends BaseModel
{
    use SoftDeletes, PresentableTrait;

    public $table = 'email_templates';

    public $fillable = ['subject', 'body', 'template_type_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
}
