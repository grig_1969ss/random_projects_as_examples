<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BreakTypes.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 */
class BreakType extends BaseModel
{
    use SoftDeletes;

    public $table = 'break_types';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function property()
    {
        return $this->hasOne(Property::class);
    }

    public function getAllBreakTypes()
    {
        return $this->orderBy('name')->get(['id', 'name']);
    }

    public function getNameByBreakTypesId($breakTypesId)
    {
        return self::where('id', '=', $breakTypesId)->only('name');
    }

    public static function getAllBreakTypesForDropdown()
    {
        $break_types = self::orderBy('name')->get(['id', 'name']);
        $breakTypesDropdown = [];
        $breakTypesDropdown[null] = 'Select ...';
        // $breakTypesDropdown['Verify\Models\Role'] = 'Add New Role';
        foreach ($break_types as $break_type) {
            $breakTypesDropdown[$break_type->id] = $break_type->name;
        }

        return $breakTypesDropdown;
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function deleteType($id)
    {
        $this->delete($id);
    }

    public function getAllPairs()
    {
        return $this->orderBy('name')->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
