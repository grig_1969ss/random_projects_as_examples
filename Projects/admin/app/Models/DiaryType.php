<?php

namespace App\Models;

/**
 * Class DiaryTypes.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 * @property bool archived
 */
class DiaryType extends BaseModel
{
    public $table = 'diary_types';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'archived',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'archived' => 'boolean',
    ];

    public function diary()
    {
        return $this->hasOne(Diary::class);
    }

    public function getAllDiaryTypes()
    {
        return $this->orderBy('name')->get(['id', 'name']);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function deleteType($id)
    {
        $this->delete($id);
    }

    public function getAllPairs()
    {
        return $this->where('archived', '=', 0)->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
