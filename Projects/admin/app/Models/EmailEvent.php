<?php

namespace App\Models;

class EmailEvent extends BaseModel
{
    protected $table = 'email_events';

    protected $fillable = ['email_id', 'type'];

    public function property()
    {
        return $this->belongsTo(Email::class, 'email_id');
    }
}
