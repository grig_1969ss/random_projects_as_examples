<?php

namespace App\Models;

use App\Presenters\SupplierPresenter;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Suppliers.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Address address
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property string name
 * @property string telephone
 * @property string fax
 * @property string email
 * @property int address_id
 * @property int created_by
 * @property int updated_by
 */
class Supplier extends BaseModel implements Searchable
{
    use SoftDeletes, CreateAndUpdateTrait, PresentableTrait;

    public $table = 'suppliers';
    public $createContactRoute = 'suppliers.contacts.store';
    public $contactElementName = 'contact_id';
    protected $presenter = SupplierPresenter::class;

    public $searchableType = 'Supplier';

    public $fillable = [
        'name',
        'telephone',
        'fax',
        'email',
        'address_id',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'address_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('suppliers.show', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->name.', '.$this->email,
            $url
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payments()
    {
        return $this->hasMany(Payment::class, 'supplier_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function contacts()
    {
        return $this->morphMany(Contact::class, 'contactable');
    }

    public function getShowUrl()
    {
        return route('suppliers.show', $this);
    }

    public function getShowName()
    {
        return $this->name;
    }

    public function createNote($data)
    {
        $data['object_type_id'] = 6;
        $data['object_id'] = $this->id;

        Note::create($data);
    }

    public function getNotes()
    {
        return Note::where('object_type_id', 6)->where('object_id', $this->id)->get();
    }

    public function getMainContactAttribute()
    {
        return $this->contacts()->first();
    }
}
