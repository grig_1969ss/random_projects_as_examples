<?php

namespace App\Models;

/**
 * Class PasswordResets.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string email
 * @property string token
 */
class PasswordResets extends BaseModel
{
    public $table = 'password_resets';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'email',
        'token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
        'token' => 'string',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required',
        'token' => 'required',
    ];
}
