<?php

namespace App\Models;

class EmailType extends BaseModel
{
    protected $fillable = ['name'];
}
