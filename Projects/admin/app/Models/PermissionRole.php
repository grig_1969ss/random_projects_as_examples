<?php

namespace App\Models;

/**
 * Class PermissionRole.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Permission permission
 * @property \App\Models\Role role
 * @property int permission_id
 * @property int role_id
 */
class PermissionRole extends BaseModel
{
    public $table = 'permission_role';

    public $fillable = [
        'permission_id',
        'role_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'permission_id' => 'integer',
        'role_id' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'permission_id' => 'required',
        'role_id' => 'required',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
