<?php

namespace App\Models;

use App\Presenters\EmailPresenter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laracasts\Presenter\PresentableTrait;

class Email extends BaseModel
{
    use SoftDeletes, PresentableTrait;

    protected $fillable = [
        'to',
        'from',
        'from_name',
        'property_id',
        'client_id',
        'diary_id',
        'to',
        'cc',
        'subject',
        'message_id',
        'date',
        'text_body',
        'html_body',
        'stripped_text_reply',
        'email_type_id',
        'read_at',
    ];

    protected $dates = ['read_at', 'date'];
    protected $presenter = EmailPresenter::class;

    private static $inbox;
    private static $sent;
    private static $draft;
    private static $scheduled;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function type()
    {
        return $this->belongsTo(EmailType::class, 'email_type_id');
    }

    public function attachments()
    {
        return $this->hasMany(AttachmentDetails::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function documents()
    {
        return $this->morphMany(Document::class, 'docable');
    }

    public static function getUnreadInboxCount($type = 'all')
    {
        $query = self::where('email_type_id', 1);

        $query = self::scopeFilterByEmailsTypeCount($query, $type);

        $query = self::scopeFilterByCurrentProperty($query);

        return $query->unread()->count();
    }

    private static function scopeFilterByEmailsType($query, $type)
    {
        if ($type == 'my') {
            $query = $query->whereHas('property', function (Builder $q) {
                $q->where('capa_owner_id', Auth::user()->id);
            });
        } else {
            // hide linked emails from ‘All’ inbox
            $query = self::hideLinkedOnes($query);
        }

        return $query;
    }

    private static function scopeFilterByEmailsTypeCount($query, $type)
    {
        if ($type == 'my') {
            $query = $query->whereHas('property', function (Builder $q) {
                $q->where('capa_owner_id', Auth::user()->id)
                    ->orWhere('capa_owner2_id', Auth::user()->id);
            });
        } else {
            // hide linked emails from ‘All’ inbox
            $query = self::hideLinkedOnes($query, true);
        }

        return $query->whereNull('archived_at');
    }

    public static function hideLinkedOnes($query, $count = false)
    {
        if ($count) {
            $query->whereNull('property_id');
        }

        if (class_basename(Route::current()->controller) != 'PropertyController' && ! request()->get('property')) {
            $query->whereNull('property_id');
        }

        return $query;
    }

    public static function getUnreadSentCount()
    {
        return self::where('email_type_id', 2)->unread()->count();
    }

    public static function getUnreadDraftCount($type = 'all')
    {
        $query = self::where('email_type_id', 3);

        $query = self::scopeFilterByEmailsTypeCount($query, $type);

        $query = self::scopeFilterByCurrentProperty($query);

        return $query->unread()->count();
    }

    public static function getUnreadScheduledCount()
    {
        return self::where('email_type_id', 4)->unread()->count();
    }

    public static function getUnreadTrashCount($type = 'all')
    {
        $query = self::onlyTrashed();

        $query = self::scopeFilterByEmailsTypeCount($query, $type);

        $query = self::scopeFilterByCurrentProperty($query);

        return $query->unread()->count();
    }

    public static function getUnreadArchivedCount($type = 'all')
    {
        $query = self::whereNotNull('archived_at');

        $query = self::scopeFilterByEmailsTypeCount($query, $type);

        $query = self::scopeFilterByCurrentProperty($query);

        return $query->unread()->count();
    }

    public function scopeInbox($query, $type = 'all', $onlyCount = false)
    {
        if (! $type) {
            $type = 'all';
        }

        if ($onlyCount) {
            $query = self::scopeFilterByEmailsTypeCount($query, $type);
        } else {
            $query = self::scopeFilterByEmailsType($query, $type);
        }

        $query = self::notArchived($query);

        return $query->where('email_type_id', 1);
    }

    public function scopeSent($query)
    {
        $query = self::scopeFilterByCurrentProperty($query);
        $query = self::notArchived($query);

        if (! request()->get('emails') || request()->get('emails') === 'all') {
            self::hideLinkedOnes($query);
        } else {
            $query->whereNotNull('property_id');
        }

        return $query->where('email_type_id', 2);
    }

    private static function notArchived($query)
    {
        return $query->whereNull('archived_at');
    }

    public function scopeDraft($query)
    {
        $query = self::scopeFilterByCurrentProperty($query);
        $query = self::notArchived($query);

        return $query->where('email_type_id', 3);
    }

    public function scopeArchived($query)
    {
        $query = self::scopeFilterByCurrentProperty($query);

        return $query->whereNotNull('archived_at');
    }

    public function scopeScheduled($query)
    {
        $query = self::scopeFilterByCurrentProperty($query);
        $query = self::notArchived($query);

        return $query->where('email_type_id', 4);
    }

    public function scopeTrash($query)
    {
        $query = self::scopeFilterByCurrentProperty($query);
        $query = self::notArchived($query);

        return $query->onlyTrashed();
    }

    public function scopeUnread($query)
    {
        $query = self::scopeFilterByCurrentProperty($query);

        $query->whereNull('read_at')->whereNull('emails.client_id')
            ->orWhere(function ($query) {
                $query->whereNull('read_at')
                    ->where('emails.client_id', auth()->user()->client_id);
            });

        return $query->whereNull('read_at');
    }

    public function scopeFilterByType($query, $type, $emailsType)
    {
        if ($type == 'inbox') {
            return $query->inbox($emailsType);
        }

        if ($type == 'sent') {
            return $query->sent();
        }

        if ($type == 'draft') {
            return $query->draft();
        }

        if ($type == 'scheduled') {
            return $query->scheduled();
        }

        if ($type == 'trash') {
            return $query->trash();
        }

        if ($type == 'archived') {
            return $query->archived();
        }

        return $query;
    }

    public function scopeSearch($query, $search)
    {
        return $query
            ->where('from', 'like', '%'.$search.'%')
            ->orWhere('from_name', 'like', '%'.$search.'%')
            ->orWhere('subject', 'like', '%'.$search.'%')
            ->orWhere('text_body', 'like', '%'.$search.'%');
    }

    public static function scopeFilterByCurrentProperty($query)
    {
        if (request()->route()->getName() == 'properties.show') {
            $query->where('property_id', request()->segment(2));
        }
        // for ajax load
        if (request()->get('property')) {
            $query->where('property_id', request()->get('property'));
        }

        //$query = self::hideLinkedOnes($query);

        return $query;
    }

    public function scopeFilterByCurrentClient($query)
    {
        return $query->where('emails.client_id', auth()->user()->client_id)
            ->orWhereNull('emails.client_id');
    }

    public function diaries()
    {
        return $this->morphMany(Diary::class, 'diaryable')->orderByDesc('created_at');
    }

    public function createDiaryEntry($comments, $date)
    {
        return $this->diary()->create([
            'comments' => $comments,
            'diary_type_id' => 8,
            'diary_title' => 'Email',
            'date' => Carbon::parse($date)->format('Y-m-d h:i:s'),
            'status' => 0,
        ]);
    }

    public function diary()
    {
        return $this->hasMany(Diary::class);
    }
}
