<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class DocumentsCloudFile extends BaseModel
{
    public $table = 'documents_cloudfiles';

    public function document()
    {
        return $this->hasOne(Document::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            DB::table('documents_cloudfiles')
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = DB::table('documents_cloudfiles')
                ->insertGetId($data);
        }

        return $id;
    }

    public static function getLatestCloudURI()
    {
        return self::where('status', 1)->orderByDesc('created_at')->pluck('id')->first();
    }
}
