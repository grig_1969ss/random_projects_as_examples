<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ApportionmentTypes.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 */
class ApportionmentType extends BaseModel
{
    use SoftDeletes;

    public $table = 'apportionment_types';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function serviceCharge()
    {
        return $this->hasOne(ServiceCharge::class);
    }

    public function getAllApportionmentTypes()
    {
        return $this->orderBy('name')->get(['id', 'name']);
    }

    public function getNameByApportionmentTypeId($apportionmentTypeId)
    {
        return self::where('id', '=', $apportionmentTypeId)->only('name');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function deleteType($id)
    {
        $this->delete($id);
    }

    public function getAllPairs()
    {
        return $this->orderBy('name')->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
