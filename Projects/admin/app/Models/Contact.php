<?php

namespace App\Models;

use App\Presenters\ContactPresenter;
use App\Traits\CreateAndUpdateTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Contacts.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property \Illuminate\Database\Eloquent\Collection properties
 * @property int contactable_id
 * @property string contactable_type
 * @property string first_name
 * @property string surname
 * @property string telephone
 * @property string fax
 * @property string email
 * @property string mobile
 * @property int created_by
 * @property int updated_by
 */
class Contact extends BaseModel implements Searchable
{
    use CreateAndUpdateTrait, PresentableTrait;

    public $table = 'contacts';
    protected $presenter = ContactPresenter::class;

    public $searchableType = 'Contact';

    public $fillable = [
        'contactable_id',
        'contactable_type',
        'first_name',
        'surname',
        'telephone',
        'fax',
        'email',
        'mobile',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contactable_id' => 'integer',
        'contactable_type' => 'string',
        'first_name' => 'string',
        'surname' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'mobile' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('contacts.show', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->first_name.' '.$this->surname.', '.$this->email,
            $url
        );
    }

    public function contactable()
    {
        return $this->morphTo();
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function landlord()
    {
        return $this->belongsTo(Landlord::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public static function getContactById($id)
    {
        $query = self::with(['client.address', 'landlord.address', 'agent.address', 'user']);

        // get client id of current user
        $userObj = new User();
        $user = $userObj->getByUserId(Session::get('id'));
        unset($userObj);

        // filter by client id if Client or if client_id is set
        if (Auth::is('Client') || (isset($user->client_id) && $user->client_id > 0)) {
            $query = $query
                ->where('id', '=', $id)
                ->where(function ($query) use ($user) {
                    $query->where('contacts.client_id', '=', $user->client_id);
                    $query->orWhereNull('contacts.client_id');
                });
        }

        return $query->first();
    }

    public function getByClientId($id)
    {
        return DB::table('contacts')
            ->where('contacts.client_id', '=', $id)
            ->get();
    }

    public function getByAgentId($id)
    {
        return DB::table('contacts')
            ->where('contacts.agent_id', '=', $id)
            ->get();
    }

    public function getByLandlordId($id)
    {
        return DB::table('contacts')
            ->where('contacts.landlord_id', '=', $id)
            ->get();
    }

    public static function getAll()
    {
        $user = auth()->user();

        if ($user->client_id) {
            return self::where(function ($query) use ($user) {
                $query->where('contactable_id', $user->client_id)->where('contactable_type', 'App\\Models\\Client');
            })->orWhere('contactable_type', '<>', 'App\\Models\\Client')->get();
        }

        return self::all();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payments()
    {
        return $this->hasMany(Payment::class, 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function properties()
    {
        return $this->hasMany(Property::class, 'agent_contact_id');
    }

    public function contact()
    {
        return $this->morphMany(self::class, 'contactable');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function property()
    {
        return $this->hasMany(Property::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currencies::class);
    }

    public function getShowUrl()
    {
        return route('clients.show', $this);
    }

    public function getShowName()
    {
        return $this->client_name;
    }

    public static function getClients()
    {
        $query = DB::table('clients');

        $query->leftJoin('contacts', 'clients.id', '=', 'contacts.contactable_id')
            ->where('contacts.contactable_type', 'App\\Models\\Client');

        $user = auth()->user();

        if ($user->client_id) {
            $query->where('clients.id', '=', $user->client_id);
        }

        $clients = $query
            ->groupBy('clients.id')
            ->get([
                'clients.*',
                DB::raw('MIN(contacts.id) as contact_id'),
                'contacts.first_name',
                'contacts.surname',
                'contacts.telephone as contact_telephone',
                'contacts.email as contact_email', ]);

        return $clients;
    }

    public function getById($id)
    {
        return self::find($id);
    }

    public function auditRegisters()
    {
        return $this->hasMany(AuditRegister::class);
    }

    public function administrator()
    {
        return $this->belongsTo(Administrator::class);
    }
}
