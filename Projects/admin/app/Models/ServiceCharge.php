<?php

namespace App\Models;

use App\Presenters\ServiceChargePresenter;
use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class ServiceCharges.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int property_id
 * @property int diary_id
 * @property string percentage_apportion
 * @property int apportionment_type_id
 * @property bool budget_details_recd
 * @property bool franchisee_updated
 * @property bool real_estate_updated
 * @property bool annual_reconciliation_recd
 * @property bool annual_franchisee_updated
 * @property bool annual_real_estate_updated
 * @property string year_end
 * @property bool exclusions
 * @property string year_end_cost_per_sq_ft
 * @property string balance_service_charge
 * @property int budget_quarterly_payment_months
 * @property string service_charge_budget_differential
 * @property string|\Carbon\Carbon service_charge_commencement_date
 * @property string agent_contributions
 * @property string potential_savings_against_budget
 * @property string amount_invoiced
 * @property string|\Carbon\Carbon date_invoiced
 * @property string amount_received
 * @property string|\Carbon\Carbon date_received
 * @property string budget_year_end
 * @property string wip
 * @property string lease_comments
 * @property string charge_details
 * @property bool invoiced
 * @property string budget_quarterly_payment
 * @property string actual_quarterly_payment
 * @property string wip_amount
 * @property string actual_year_end
 * @property string actual_against_budget
 * @property bool confirmed
 * @property string potential_budget
 * @property number annual_budget
 * @property string current_contribution
 * @property string forcast_contribution_year1
 * @property string current_turnover
 * @property string forcast_turnover_year1
 * @property string forcast_turnover_year2
 * @property string forcast_contribution_year2
 * @property string notes
 * @property int service_charge_status_id
 * @property int created_by
 * @property int updated_by
 * @property bool vat
 */
class ServiceCharge extends BaseModel
{
    const REPORT_LIMIT = 5;

    use CreateAndUpdateTrait, PresentableTrait, DocumentTrait, SoftDeletes;

    public $table = 'service_charges';
    protected $guarded = ['property_id'];
    protected $presenter = ServiceChargePresenter::class;
    public $documentStoreRoute = 'service-charges.documents.store';
    public $modelNameHuman = 'Service Charge';

    public $fillable = [
        'property_id',
        'diary_id',
        'percentage_apportion',
        'apportionment_type_id',
        'budget_details_recd',
        'franchisee_updated',
        'real_estate_updated',
        'annual_reconciliation_recd',
        'annual_franchisee_updated',
        'annual_real_estate_updated',
        'year_end',
        'exclusions',
        'year_end_cost_per_sq_ft',
        'balance_service_charge',
        'budget_quarterly_payment_months',
        'service_charge_budget_differential',
        'service_charge_commencement_date',
        'agent_contributions',
        'potential_savings_against_budget',
        'amount_invoiced',
        'date_invoiced',
        'amount_received',
        'date_received',
        'budget_year_end',
        'wip',
        'lease_comments',
        'charge_details',
        'invoiced',
        'budget_quarterly_payment',
        'actual_quarterly_payment',
        'wip_amount',
        'actual_year_end',
        'actual_against_budget',
        'confirmed',
        'potential_budget',
        'annual_budget',
        'current_contribution',
        'forcast_contribution_year1',
        'current_turnover',
        'forcast_turnover_year1',
        'forcast_turnover_year2',
        'forcast_contribution_year2',
        'notes',
        'service_charge_status_id',
        'created_by',
        'updated_by',
        'vat',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'diary_id' => 'integer',
        'percentage_apportion' => 'string',
        'apportionment_type_id' => 'integer',
        'budget_details_recd' => 'boolean',
        'franchisee_updated' => 'boolean',
        'real_estate_updated' => 'boolean',
        'annual_reconciliation_recd' => 'boolean',
        'annual_franchisee_updated' => 'boolean',
        'annual_real_estate_updated' => 'boolean',
        'year_end' => 'date',
        'exclusions' => 'boolean',
        'year_end_cost_per_sq_ft' => 'string',
        'balance_service_charge' => 'numeric',
        'budget_quarterly_payment_months' => 'integer',
        'service_charge_budget_differential' => 'string',
        'service_charge_commencement_date' => 'datetime',
        'agent_contributions' => 'string',
        'potential_savings_against_budget' => 'string',
        'amount_invoiced' => 'string',
        'date_invoiced' => 'datetime',
        'amount_received' => 'string',
        'date_received' => 'datetime',
        'budget_year_end' => 'string',
        'wip' => 'string',
        'lease_comments' => 'string',
        'charge_details' => 'string',
        'invoiced' => 'boolean',
        'budget_quarterly_payment' => 'numeric',
        'actual_quarterly_payment' => 'string',
        'wip_amount' => 'string',
        'actual_year_end' => 'string',
        'actual_against_budget' => 'string',
        'confirmed' => 'boolean',
        'potential_budget' => 'string',
        'annual_budget' => 'float',
        'current_contribution' => 'string',
        'forcast_contribution_year1' => 'string',
        'current_turnover' => 'string',
        'forcast_turnover_year1' => 'string',
        'forcast_turnover_year2' => 'string',
        'forcast_contribution_year2' => 'string',
        'notes' => 'string',
        'service_charge_status_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'vat' => 'boolean',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function diary()
    {
        return $this->belongsTo(Diary::class);
    }

    public function apportionmentType()
    {
        return $this->belongsTo(ApportionmentType::class);
    }

    public function serviceChargeStatus()
    {
        return $this->belongsTo(ServiceChargeStatus::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class)->orderBy('quarter');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getAllServiceChargesByPropertyId($propertyId, $years = self::REPORT_LIMIT)
    {
        // pass last Year from table to Carbon

        $lastYear = self::orderBy('year_end', 'DESC')->where('property_id', '=', $propertyId)->first();

        if (empty($lastYear)) {
            return;
        }

        $lastYear = Carbon::parse(
            $lastYear->year_end
        );

        // get latest 3 Years counting from existing Years not from current Year
        $last3Years = $lastYear->subYears($years)->format('Y-m-d');

        $serviceCharges = self::with(['apportionmentType' => function ($query) {
            $query->select('id', 'name');
        }])
            ->where('property_id', '=', $propertyId);
        if ($years) {
            $serviceCharges->whereDate('year_end', '>', $last3Years);
        } else {
            $serviceCharges->select('id', 'year_end');
        }

        $serviceCharges = $serviceCharges->orderBy('year_end', 'desc')->get();

        return $serviceCharges;
    }

    public function getServiceChargeById($id)
    {
        return self::with([
            'diary',
        ])->find($id);
    }

    /**
     * Gets all the active service charges for annual budget report.
     *
     * @return array returns all rents
     */
    public static function getAnnualBudgetDataByPropertyId($id)
    {
        $sql = '
            SELECT *,
            budget_quarterly_payment * 4 AS annual_payment
            FROM service_charges as sc
            WHERE sc.property_id='.$id.'
            AND YEAR(sc.year_end) >= YEAR(Date_sub(Curdate(), INTERVAL 4 YEAR))
            ORDER  BY property_id, year_end;';

        return DB::select($sql);
    }

    /**
     * Gets all the active service charges for annual actual report.
     *
     * @return array returns all rents
     */
    public static function getAnnualActualDataByPropertyId($id)
    {
        $sql = '
            SELECT *,
            actual_quarterly_payment * 4 AS annual_payment
            FROM service_charges as sc
            WHERE sc.property_id='.$id.'
            AND YEAR(sc.year_end) >= YEAR(Date_sub(Curdate(), INTERVAL 4 YEAR))
            ORDER  BY property_id, year_end;';

        return DB::select($sql);
    }

    public function getPreviousServiceCharges()
    {
        return $this->property
            ->serviceCharge()
            ->whereDate('year_end', '<', $this->year_end)
            ->orderByDesc('year_end')
            ->limit(2)
            ->get();
    }

    public function document()
    {
        return $this->morphMany(Document::class, 'docable')->where('archived', false)->orderByDesc('created_at');
    }

    public function diaries()
    {
        return $this->morphMany(Diary::class, 'diaryable')->orderByDesc('created_at');
    }
}
