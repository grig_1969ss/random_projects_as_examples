<?php

namespace App\Models;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Presenters\PropertyPresenter;
use App\Traits\DocumentTrait;
use App\Traits\QueryHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Properties.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Contact agentContact
 * @property \App\Models\StoreType storeType
 * @property \Illuminate\Database\Eloquent\Collection insurances
 * @property int property_type_id
 * @property int client_id
 * @property int landlord_id
 * @property int agent_id
 * @property int agent_contact_id
 * @property int owner_type_id
 * @property string reference
 * @property string property_name
 * @property string store_contact
 * @property string store_telephone
 * @property int property_status_id
 * @property int address_id
 * @property string floor_area_net_trading
 * @property number rateable_value
 * @property number proposed_rateable_value
 * @property number agreed_rateable_value
 * @property int appeal_type_id
 * @property string rv_notes
 * @property int tenure_type_id
 * @property string|\Carbon\Carbon purchase_date
 * @property string|\Carbon\Carbon commencement_date
 * @property number purchase_price
 * @property number book_value
 * @property number freehold_value
 * @property number rental_value
 * @property number book_rental_value
 * @property number est_repairs_cost
 * @property number repair_liability
 * @property string|\Carbon\Carbon date_last_inspected
 * @property string|\Carbon\Carbon lease_start_date
 * @property int break_clause_type_id
 * @property bool undergoing_refit
 * @property bool has_service_charge
 * @property bool has_break_options
 * @property bool has_client_queries
 * @property bool has_maintenances
 * @property bool has_rent_reviews
 * @property bool has_notes
 * @property string lease_comments
 * @property string lease_term
 * @property string|\Carbon\Carbon lease_expiry_date
 * @property string|\Carbon\Carbon rent_review_date
 * @property string|\Carbon\Carbon erv_date
 * @property string initial_concessions
 * @property number erv_amount
 * @property string|\Carbon\Carbon lease_notice_dates
 * @property string guarantor
 * @property string tenant_break_option
 * @property string agent_break_option
 * @property int break_notice_period
 * @property string|\Carbon\Carbon notice_dates
 * @property string annual_rent
 * @property string rent_year_2
 * @property string rent_year_3
 * @property string rent_year_4
 * @property string rent_year_5
 * @property string payment_frequency
 * @property int rent_payment_frequency_id
 * @property string payment_dates
 * @property number deposit
 * @property number rent_paid
 * @property string insurance_supplier
 * @property int disposal_type_id
 * @property string|\Carbon\Carbon disposal_date
 * @property int alienation_type_id
 * @property bool concessions
 * @property string alienation_note
 * @property string alteration_note
 * @property bool non_structural
 * @property bool structural
 * @property bool landlord_consent
 * @property bool schedule_condition
 * @property int restrictions
 * @property int user_type_id
 * @property string user_clause_note
 * @property string initial_concessions_note
 * @property int subletting_type_id
 * @property string ext_frequency
 * @property string|\Carbon\Carbon ext_first_due
 * @property string|\Carbon\Carbon ext_next_due
 * @property bool ext_final_year
 * @property string int_frequency
 * @property string|\Carbon\Carbon int_first_due
 * @property string|\Carbon\Carbon int_next_due
 * @property bool int_final_year
 * @property int repair_classification_type_id
 * @property string repair_obligations_note
 * @property bool share
 * @property bool auto_service_charge
 * @property string general_info
 * @property bool drive_thru
 * @property int store_type_id
 * @property string open_date
 * @property string client_surveyor
 * @property string client_admin
 * @property string capa_admin
 * @property int created_by
 * @property int updated_by
 * @property int region_id
 * @property int inside_landlord
 */
class Property extends BaseModel implements Searchable
{
    use QueryHelper, PresentableTrait, DocumentTrait;

    const PAGINATE_COUNT = 10;

    public $table = 'properties';
    protected $presenter = PropertyPresenter::class;
    public $showRoute = 'properties.show';
    protected $dates = ['open_date'];

    public $searchableType = 'Property';

    public $fillable = [
        'property_type_id',
        'client_id',
        'landlord_id',
        'agent_id',
        'agent_contact_id',
        'owner_type_id',
        'reference',
        'property_name',
        'store_contact',
        'store_telephone',
        'property_status_id',
        'address_id',
        'floor_area_net_trading',
        'rateable_value',
        'proposed_rateable_value',
        'agreed_rateable_value',
        'appeal_type_id',
        'rv_notes',
        'tenure_type_id',
        'purchase_date',
        'commencement_date',
        'purchase_price',
        'book_value',
        'freehold_value',
        'rental_value',
        'book_rental_value',
        'est_repairs_cost',
        'repair_liability',
        'date_last_inspected',
        'lease_start_date',
        'break_clause_type_id',
        'undergoing_refit',
        'has_service_charge',
        'has_break_options',
        'has_client_queries',
        'has_maintenances',
        'has_rent_reviews',
        'has_notes',
        'lease_comments',
        'lease_term',
        'lease_expiry_date',
        'rent_review_date',
        'erv_date',
        'initial_concessions',
        'erv_amount',
        'lease_notice_dates',
        'guarantor',
        'tenant_break_option',
        'agent_break_option',
        'break_notice_period',
        'notice_dates',
        'annual_rent',
        'rent_year_2',
        'rent_year_3',
        'rent_year_4',
        'rent_year_5',
        'payment_frequency',
        'rent_payment_frequency_id',
        'payment_dates',
        'deposit',
        'rent_paid',
        'insurance_supplier',
        'disposal_type_id',
        'disposal_date',
        'alienation_type_id',
        'concessions',
        'alienation_note',
        'alteration_note',
        'non_structural',
        'structural',
        'landlord_consent',
        'schedule_condition',
        'restrictions',
        'user_type_id',
        'user_clause_note',
        'initial_concessions_note',
        'subletting_type_id',
        'ext_frequency',
        'ext_first_due',
        'ext_next_due',
        'ext_final_year',
        'int_frequency',
        'int_first_due',
        'int_next_due',
        'int_final_year',
        'repair_classification_type_id',
        'repair_obligations_note',
        'share',
        'auto_service_charge',
        'general_info',
        'drive_thru',
        'store_type_id',
        'open_date',
        'client_surveyor',
        'client_admin',
        'capa_admin',
        'created_by',
        'updated_by',
        'region_id',
        'inside_landlord',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_type_id' => 'integer',
        'client_id' => 'integer',
        'landlord_id' => 'integer',
        'agent_id' => 'integer',
        'agent_contact_id' => 'integer',
        'owner_type_id' => 'integer',
        'reference' => 'string',
        'property_name' => 'string',
        'store_contact' => 'string',
        'store_telephone' => 'string',
        'property_status_id' => 'integer',
        'address_id' => 'integer',
        'floor_area_net_trading' => 'string',
        'rateable_value' => 'float',
        'proposed_rateable_value' => 'float',
        'agreed_rateable_value' => 'float',
        'appeal_type_id' => 'integer',
        'rv_notes' => 'string',
        'tenure_type_id' => 'integer',
        'purchase_date' => 'datetime',
        'commencement_date' => 'datetime',
        'purchase_price' => 'float',
        'book_value' => 'float',
        'freehold_value' => 'float',
        'rental_value' => 'float',
        'book_rental_value' => 'float',
        'est_repairs_cost' => 'float',
        'repair_liability' => 'float',
        'date_last_inspected' => 'datetime',
        'lease_start_date' => 'datetime',
        'break_clause_type_id' => 'integer',
        'undergoing_refit' => 'boolean',
        'has_service_charge' => 'boolean',
        'has_break_options' => 'boolean',
        'has_client_queries' => 'boolean',
        'has_maintenances' => 'boolean',
        'has_rent_reviews' => 'boolean',
        'has_notes' => 'boolean',
        'lease_comments' => 'string',
        'lease_term' => 'string',
        'lease_expiry_date' => 'datetime',
        'rent_review_date' => 'datetime',
        'erv_date' => 'datetime',
        'initial_concessions' => 'string',
        'erv_amount' => 'float',
        'lease_notice_dates' => 'datetime',
        'guarantor' => 'string',
        'tenant_break_option' => 'string',
        'agent_break_option' => 'string',
        'break_notice_period' => 'integer',
        'notice_dates' => 'datetime',
        'annual_rent' => 'string',
        'rent_year_2' => 'string',
        'rent_year_3' => 'string',
        'rent_year_4' => 'string',
        'rent_year_5' => 'string',
        'payment_frequency' => 'string',
        'rent_payment_frequency_id' => 'integer',
        'payment_dates' => 'string',
        'deposit' => 'float',
        'rent_paid' => 'float',
        'insurance_supplier' => 'string',
        'disposal_type_id' => 'integer',
        'disposal_date' => 'datetime',
        'alienation_type_id' => 'integer',
        'concessions' => 'boolean',
        'alienation_note' => 'string',
        'alteration_note' => 'string',
        'non_structural' => 'boolean',
        'structural' => 'boolean',
        'landlord_consent' => 'boolean',
        'schedule_condition' => 'boolean',
        'restrictions' => 'integer',
        'user_type_id' => 'integer',
        'user_clause_note' => 'string',
        'initial_concessions_note' => 'string',
        'subletting_type_id' => 'integer',
        'ext_frequency' => 'string',
        'ext_first_due' => 'datetime',
        'ext_next_due' => 'datetime',
        'ext_final_year' => 'boolean',
        'int_frequency' => 'string',
        'int_first_due' => 'datetime',
        'int_next_due' => 'datetime',
        'int_final_year' => 'boolean',
        'repair_classification_type_id' => 'integer',
        'repair_obligations_note' => 'string',
        'share' => 'boolean',
        'auto_service_charge' => 'boolean',
        'general_info' => 'string',
        'drive_thru' => 'boolean',
        'store_type_id' => 'integer',
        'open_date' => 'date',
        'client_surveyor' => 'string',
        'client_admin' => 'string',
        'capa_admin' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'region_id' => 'integer',
        'inside_landlord' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'has_break_options' => 'required',
        'has_client_queries' => 'required',
        'has_maintenances' => 'required',
        'has_rent_reviews' => 'required',
        'has_notes' => 'required',
        'drive_thru' => 'required',
        'inside_landlord' => 'required',
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('properties.show', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->property_name,
            $url
        );
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function clientSurveyor()
    {
        return $this->belongsTo(User::class, 'client_surveyor_id');
    }

    public function capaAdmin()
    {
        return $this->belongsTo(User::class, 'capa_owner_id');
    }

    public function capaAdmin2()
    {
        return $this->belongsTo(User::class, 'capa_owner2_id');
    }

    public function clientAdmin()
    {
        return $this->belongsTo(User::class, 'client_owner_id');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function landlord()
    {
        return $this->belongsTo(Landlord::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function propertyStatus()
    {
        return $this->belongsTo(PropertyStatus::class);
    }

    public function propertyTypes()
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    public function tenureType()
    {
        return $this->belongsTo(TenureType::class, 'tenure_type_id');
    }

    public function userType()
    {
        return $this->belongsTo(UserType::class, 'user_type_id');
    }

    public function alienationType()
    {
        return $this->belongsTo(AlienationType::class, 'alienation_type_id');
    }

    public function sublettingType()
    {
        return $this->belongsTo(SublettingType::class, 'subletting_type_id');
    }

    public function repairClassificationType()
    {
        return $this->belongsTo(RepairClassificationType::class, 'repair_classification_type_id');
    }

    public function breakType()
    {
        return $this->belongsTo(BreakType::class, 'break_clause_type_id');
    }

    public function appealType()
    {
        return $this->belongsTo(AppealType::class, 'appeal_type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function capaOwner()
    {
        return $this->belongsTo(User::class, 'capa_owner_id');
    }

    public function clientOwner()
    {
        return $this->belongsTo(User::class, 'client_owner_id');
    }

    public function rentPaymentFrequency()
    {
        return $this->belongsTo(RentPaymentFrequency::class, 'rent_payment_frequency_id');
    }

    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    public function serviceCharge()
    {
        return $this->hasMany(ServiceCharge::class);
    }

    public function last4ServiceCharges($reconciliationsDataTable = false)
    {
        $data = $this->hasMany(ServiceCharge::class)
            ->select(
                'id',
                'year_end',
                'annual_budget',
                'budget_details_recd',
                'franchisee_updated',
                'annual_franchisee_updated',
                'real_estate_updated',
                'annual_reconciliation_recd',
                DB::raw('DATE_FORMAT(year_end, "%Y") as year')
            );

        if ($reconciliationsDataTable) {
            $data = $data
                ->where('annual_reconciliation_recd', 0)
                ->whereRaw('year_end <=  DATE_SUB(CURDATE(),INTERVAL 6 MONTH)');
        }

        $data = $data->orderBy('year_end', 'DESC')
            ->limit(4)
            ->get()->keyBy('year');

        return $data->toArray() ?? [];
    }

    public function alerts()
    {
        return $this->hasMany(Alert::class)->where('active', 1);
    }

    public function last4Insurances()
    {
        $data = $this->hasMany(Insurance::class, 'property_id')
            ->select('insurance_renewal_date', 'insurance_premium', 'cover_note_received', DB::raw('DATE_FORMAT(insurance_renewal_date, "%Y") as year'))
            ->orderBy('insurance_renewal_date', 'DESC')->limit(4)->get()->keyBy('year');

        return $data->toArray() ?? [];
    }

    public function document()
    {
        return $this->morphMany(Document::class, 'docable')->where('archived', false)->orderByDesc('created_at');
    }

    public function diaries()
    {
        return $this->morphMany(Diary::class, 'diaryable')->orderByDesc('created_at');
    }

    public function diary()
    {
        return $this->hasMany(Diary::class);
    }

    public function rent()
    {
        return $this->hasMany(Rent::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function rentReviews()
    {
        return $this->hasMany(RentReview::class);
    }

    public function breaks()
    {
        return $this->hasMany(Breaks::class);
    }

    public function clientQueries()
    {
        return $this->hasMany(ClientQuery::class);
    }

    public function ownerType()
    {
        return $this->belongsTo(OwnerType::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            DB::table('properties')
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = DB::table('properties')
                ->insertGetId($data);
        }

        return $id;
    }

    public function addDocumentNote(Document $document, $comments = null)
    {
        Util::addNote(
            $this->id,
            'properties',
            6,
            $comments ? $comments : "Document {$document->document_name} Uploaded"
        );
    }

    public static function getPropertyById($id)
    {
        $query = self::with([
            'user',
            'address',
            'service_charge',
            'property_status',
            'propertytypes',
            'client',
            'client.contact',
            'agent',
            'landlord',
            'tenure_type',
            'break_type',
            'repair_classification_type',
            'user_type',
            'alienation_type',
            'subletting_type',
            'appeal_type',
            'rent_payment_frequency',
            'region',
        ]);

        // get client id of current user
        $userObj = new User();
        $user = $userObj->getByUserId(Session::get('id'));
        unset($userObj);

        // filter by client id if Client or if client_id is set
        if (Auth::is('Client')) {
            $query = $query->where('properties.client_id', '=', $user->client_id);
        }

        // filter by client id if Client or if client_id is set
        if (isset($user->client_id) && $user->client_id > 0) {
            $query = $query->where('properties.client_id', '=', $user->client_id);
        }

        return $query->where('id', '=', $id)->first();
    }

    public static function getInsideLandlord($code)
    {
        if ($code == 1) {
            return 'TBC';
        }
        if ($code == 3) {
            return 'YES';
        }
        if ($code == 2) {
            return 'NO';
        }
    }

    /**
     * Gets all the properties for rental liability comparison.
     *
     * @param string $order allows the ordering of results
     *
     * @return array returns all properties
     */
    public static function getRentalLiabilityAll($order)
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        $order = (isset($order)) ? $order.', property_name' : 'property_name';

        $sql
            = 'SELECT p.*,
                    rent_payment_frequency.name AS rent_payment_frequency,
                    regions.name AS region_name,
                   0 AS rent_sort
            FROM   properties as p
                    LEFT JOIN rents
                          ON ( rents.property_id = p.id )
                    LEFT JOIN rent_payment_frequency
                        ON rent_payment_frequency.id = p.rent_payment_frequency_id
                    LEFT JOIN regions
                        ON regions.id = p.region_id
            WHERE  rents.id IS NOT NULL
               AND rents.archived = 0
               AND rents.exp_rent_date > Curdate()
            GROUP  BY p.id
            UNION
            SELECT p.*,
                    rent_payment_frequency.name AS rent_payment_frequency,
                    regions.name AS region_name,
                   1 AS rent_sort
            FROM   properties as p
                   LEFT JOIN rents
                          ON ( rents.property_id = p.id )
                    LEFT JOIN rent_payment_frequency
                        ON rent_payment_frequency.id = p.rent_payment_frequency_id
                    LEFT JOIN regions
                        ON regions.id = p.region_id
            WHERE  rents.id IS NULL '.$clientQuery.'
            GROUP  BY p.id
            ORDER  BY rent_sort,
                      '.$order.';';

        $properties = DB::select($sql);

        foreach ($properties as $key => $value) {
            $rents = Rent::getRentalLiabilityByPropertyId($value->id);
            foreach ($rents as $rkey => $rvalue) {
                $properties[$key]->rent[$rkey] = $rvalue;
            }
        }

        return $properties;
    }

    public static function getAllPropertiesForServiceChargeReport($type, $order, $date, $interval = 0, $intervalType = 'MONTH')
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        $order = (isset($order)) ? $order.', property_name, sc.year_end;' : 'property_name, sc.year_end;';

        if ($date) {
            $dateQuery = Util::getDateQuery('sc.year_end', $date, $interval, $intervalType, 'AND');
        } else {
            $dateQuery = '';
        }

        $sql = 'SELECT p.id,
                       p.property_name,
                       p.reference,
                       sc.year_end,
                       regions.name AS region_name
                FROM   properties AS p
                       LEFT JOIN service_charges AS sc
                              ON ( sc.property_id = p.id AND sc.deleted_at IS NULL)
                       LEFT JOIN regions
                        ON regions.id = p.region_id
                WHERE  sc.id IS NOT NULL
                   '.$dateQuery.$clientQuery.'
                GROUP BY p.id
                ORDER  BY '.$order;

        $properties = DB::select($sql);

        foreach ($properties as $key => $value) {
            if ($type == 'Budget') {
                $serviceCharges = ServiceCharge::getAnnualBudgetDataByPropertyId($value->id);
            } else {
                $serviceCharges = ServiceCharge::getAnnualActualDataByPropertyId($value->id);
            }

            foreach ($serviceCharges as $sckey => $scvalue) {
                $properties[$key]->service_charge[$sckey] = $scvalue;
            }
        }

        return $properties;
    }

    public static function getAllPropertiesForQuarterServiceChargeReport($type, $order, $date)
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');

        $order = (isset($order)) ? $order.', property_name, sc.year_end;' : 'property_name, sc.year_end;';

        $sql = "
                select p.id, p.property_name, p.reference, sc.year_end, sc.budget_quarterly_payment
                FROM properties as p, service_charges as sc
                where sc.property_id = p.id
                {$clientQuery}
                AND sc.year_end <= '{$date}'
                and sc.year_end > DATE_SUB('{$date}', INTERVAL 1 YEAR)
                GROUP BY p.id
                ORDER BY {$order}
                ";

        $properties = DB::select($sql);

        return $properties;
    }

    public static function getAllRecentServiceCharges($prefix = null)
    {
        $user = auth()->user();
        $subQuery = '';

        if ($user->client_id) {
            $subQuery = "AND p.client_id = $user->client_id";
        }

        $sql = "SELECT p.id,
                       p.property_name,
                       p.reference,
                       DATE_FORMAT(MAX(sc.year_end), '%d %M') as year_end
                FROM   properties AS p
                       LEFT JOIN service_charges AS sc
                              ON ( sc.property_id = p.id )
                       LEFT JOIN regions
                        ON regions.id = p.region_id
                WHERE  sc.id IS NOT NULL
                    $subQuery
                GROUP BY p.id
                ORDER  BY MONTH(MAX(sc.year_end)), DAY(MAX(sc.year_end)), property_name, property_name, sc.year_end;";

        $properties = DB::select($sql);

        return $properties;
    }

    public function scopeClientId($query, $id)
    {
        return $query->where('client_id', $id);
    }

    public function createDiaryEntry($typeId, $title, $date)
    {
        return $this->diary()->create([
            'diary_type_id' => $typeId,
            'diary_title' => $title,
            'date' => Date::toMysql($date),
            'status' => 0,
        ]);
    }

    public function mainLandlordContact()
    {
        if ($this->landlord && $this->landlord->contact()->count('id') > 0) {
            return $this->landlord->contact()->first();
        }
    }

    /**
     * Counts the properties with active maintenance.
     *
     * @return int returns the total number of properties
     */
    public static function countProperties()
    {
        $user = auth()->user();
        $query = self::where('property_status_id', 1);

        if ($user->client_id) {
            $query = $query->where('client_id', $user->client_id);
        }

        return $query->count('id');
    }

    /**
     * Counts the properties undergoing a refit.
     *
     * @return int returns the total number of properties
     */
    public static function countPropertiesUndergoingRefit()
    {
        $user = auth()->user();
        $query = self::status(1)->where('undergoing_refit', 1);

        if ($user->client_id) {
            $query = $query->clientId($user->client_id);
        }

        return $query->count('id');
    }

    /**
     * Counts the properties with active maintenance.
     *
     * @return int returns the total number of properties
     */
    public static function countPropertiesWithActiveMaintenance()
    {
        $user = auth()->user();

        $query = self::whereHas('maintenance', function ($query) {
            $query->where('resolved', '<>', 1);
        })->where('property_status_id', 1);

        if ($user->client_id) {
            $query = $query->where('client_id', $user->client_id);
        }

        $count = $query->count(['properties.id']);

        return $count;
    }

    /**
     * Counts the properties with leases expiring given the date, the interval and interval type.
     *
     * @param string $date the date the query starts from
     * @param int $interval
     * @param string $intervalType the interval type check DATE_ADD function in MYSQL docs
     *
     * @return int the total number of properties
     */
    public static function countPropertiesDueLeaseExpiry($date = null, $interval = 0, $intervalType = 'MONTH')
    {
        $user = auth()->user();
        $query = self::status(1)->filterByDate('lease_expiry_date');

        if ($user->client_id) {
            $query = $query->clientId($user->client_id);
        }

        return $query->count('id');
    }

    /**
     * Counts the properties with a rent review due given the date, the interval and interval type.
     *
     * @param string $date the date the query starts from
     * @param int $interval
     * @param string $intervalType the interval type check DATE_ADD function in MYSQL docs
     *
     * @return int the total number of properties
     */
    public static function countPropertiesDueRentReview($date = null, $interval = 0, $intervalType = 'MONTH')
    {
        $user = auth()->user();
        $query = self::status(1)->filterByDate('rent_review_date');

        if ($user->client_id) {
            $query = $query->clientId($user->client_id);
        }

        return $query->count('id');
    }

    public static function getPropertiesIndexList($status = null, $all = false, $search = null)
    {
        $select = [
            'properties.id',
            'properties.property_name',
            'properties.reference',
            'addresses.post_code',
            'clients.client_name',
            'agents.agent_name',
            'property_statuses.name',
            'property_statuses.button',
            'regions.name as region_name',
        ];

        $query = DB::table('properties');
        $query
            ->leftJoin('property_statuses', 'properties.property_status_id', '=', 'property_statuses.id')
            ->leftJoin('clients', 'clients.id', '=', 'properties.client_id')
            ->leftJoin('agents', 'agents.id', '=', 'properties.agent_id')
            ->leftJoin('addresses', 'addresses.id', '=', 'properties.address_id')
            ->leftJoin('regions', 'regions.id', '=', 'properties.region_id');

        $user = auth()->user();

        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('property_name', 'like', '%'.$search.'%')
                    ->orWhere('reference', 'like', '%'.$search.'%');
            });
        }

        if ($user->client_id) {
            $query->where('properties.client_id', '=', $user->client_id);
        }

        if ($user->region_id) {
            $query->where('properties.region_id', '=', $user->region_id);
        }

        if ($status) {
            $query->where('property_statuses.id', $status);
        }

        $properties = $query->select($select);

        $properties = $properties->paginate(self::PAGINATE_COUNT);

        return $properties;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function agentContact()
    {
        return $this->belongsTo(Contact::class, 'agent_contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function storeType()
    {
        return $this->belongsTo(StoreType::class, 'store_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurances()
    {
        if (request()->route()->getName() == 'properties.show') {
            return $this->hasMany(Insurance::class, 'property_id')->orderBy('insurance_renewal_date', 'DESC');
        }

        return $this->hasMany(Insurance::class, 'property_id');
    }

    public function maintenance()
    {
        return $this->hasMany(Maintenance::class);
    }

    public function utilities()
    {
        return $this->hasMany(Utility::class);
    }

    public function scopeStatus($query, $statusId)
    {
        return $query->where('property_status_id', $statusId);
    }

    public static function getAllPropertiesDueLeaseExpiry($order, $dateFrom = null, $dateTo = null)
    {
        // get client check query
        $clientQuery = Util::getClientIdQuery('AND');
        // get properties whose lease has expired or is due to expire

        if ($dateFrom) :
            $dateQuery = " AND p.lease_expiry_date >= '".$dateFrom."' "; else :
            $dateQuery = ' AND TRUE ';
        endif;

        if ($dateTo) :
            $dateQuery .= " AND p.lease_expiry_date <= '".$dateTo."' ";
        endif;

        $sql = 'SELECT
                p.id,
                p.reference,
                p.property_name,
                p.general_info,
                p.lease_expiry_date,
                p.rateable_value,
                p.repair_liability,
                p.lease_expiry_date,
                p.lease_start_date,
                p.rent_review_date,
                p.repair_obligations_note,
                p.user_clause_note,
                breaks.break_date as break_option_date,
                breaks.notice_period as break_option_notice_period,
                l.landlord_name,
                la.address_1 as landlord_address_1,
                la.address_2 as landlord_address_2,
                la.town as landlord_town,
                la.county as landlord_county,
                la.post_code as landlord_post_code,
                ag.agent_name as landlord_agent,
                ma.address_1 as agent_address_1,
                ma.address_2 as agent_address_2,
                ma.town as agent_town,
                ma.county as agent_county,
                ma.post_code as agent_post_code,
                a.address_1,
                a.address_2,
                a.town,
                a.county,
                a.post_code,
                regions.name AS region_name
            FROM properties AS p
                LEFT JOIN regions
                    ON regions.id = p.region_id
                LEFT JOIN addresses AS a
                    ON ( a.id = p.address_id )
                LEFT JOIN landlords AS l
                    ON ( l.id = p.landlord_id )
                LEFT JOIN addresses AS la
                    ON ( la.id = l.address_id )
                LEFT JOIN agents AS ag
                    ON ( ag.id = p.agent_id )
                LEFT JOIN addresses AS ma
                    ON ( ma.id = ag.address_id )
                LEFT JOIN (
                        SELECT b.*
                        FROM (
                            SELECT DISTINCT property_id
                            FROM breaks) bo
                        JOIN breaks b
                        ON b.id = (
                            SELECT id
                            FROM   breaks bi
                            WHERE  bi.property_id = bo.property_id
                                AND bi.archived != 1
                            ORDER  BY break_date
                            LIMIT  1)
                    ) breaks
                    ON breaks.property_id = p.id
            WHERE p.property_status_id = 1'.$clientQuery.$dateQuery.
            (($order === null) ? 'ORDER BY property_name' : ' ORDER BY  '.$order.', property_name');

        $properties = DB::select($sql);

        //print_r($sql);exit;

        foreach ($properties as $key => $property) :
            $address = '';
        $address .= (isset($property->address_1)) ? $property->address_1 : '';
        $address .= (isset($property->address_2)) ? ', '.$property->address_2 : '';
        $address .= (isset($property->town)) ? ', '.$property->town : '';
        $address .= (isset($property->county)) ? ', '.$property->county : '';
        $address .= (isset($property->post_code)) ? ', '.$property->post_code : '';
        $properties[$key]->address = $address;

        $address = '';
        $address .= (isset($property->landlord_address_1)) ? $property->landlord_address_1 : '';
        $address .= (isset($property->landlord_address_2)) ? ', '.$property->landlord_address_2 : '';
        $address .= (isset($property->landlord_town)) ? ', '.$property->landlord_town : '';
        $address .= (isset($property->landlord_county)) ? ', '.$property->landlord_county : '';
        $address .= (isset($property->landlord_post_code)) ? ', '.$property->landlord_post_code : '';
        $properties[$key]->landlord_address = $address;

        $address = '';
        $address .= (isset($property->agent_address_1)) ? $property->agent_address_1 : '';
        $address .= (isset($property->agent_address_2)) ? ', '.$property->agent_address_2 : '';
        $address .= (isset($property->agent_town)) ? ', '.$property->agent_town : '';
        $address .= (isset($property->agent_county)) ? ', '.$property->agent_county : '';
        $address .= (isset($property->agent_post_code)) ? ', '.$property->agent_post_code : '';
        $properties[$key]->agent_address = $address;

        endforeach;

        return $properties;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'franchisee_property');
    }
}
