<?php

namespace App\Models;

/**
 * Class Regions.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property string manager
 * @property int created_by
 * @property int updated_by
 */
class Region extends BaseModel
{
    public $table = 'regions';

    public $fillable = [
        'name',
        'manager',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'manager' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $this
                ->insertGetId($data);
        }

        return $id;
    }

    public static function getAllNames()
    {
        return self::orderBy('name')->get(['id', 'name']);
    }
}
