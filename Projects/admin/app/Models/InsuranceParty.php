<?php

namespace App\Models;

/**
 * Class InsuranceParties.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * @property \Illuminate\Database\Eloquent\Collection insurances
 * @property \Illuminate\Database\Eloquent\Collection insurance2s
 * @property string name
 * @property int created_by
 * @property int updated_by
 */
class InsuranceParty extends BaseModel
{
    public $table = 'insurance_parties';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurancesCoveredBy()
    {
        return $this->hasMany(Insurance::class, 'covered_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function insurancesPlateGlass()
    {
        return $this->hasMany(Insurance::class, 'plate_glass');
    }
}
