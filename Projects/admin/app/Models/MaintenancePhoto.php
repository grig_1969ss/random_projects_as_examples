<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Support\Facades\Storage;

/**
 * Class MaintenancePhotos.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int maintenance_id
 * @property string document_name
 * @property string document_file
 * @property string content_type
 * @property bool archived
 * @property int created_by
 * @property int updated_by
 */
class MaintenancePhoto extends BaseModel
{
    use CreateAndUpdateTrait;

    public $table = 'maintenance_photos';

    public $fillable = [
        'maintenance_id',
        'document_name',
        'document_file',
        'content_type',
        'archived',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'maintenance_id' => 'integer',
        'document_name' => 'string',
        'document_file' => 'string',
        'content_type' => 'string',
        'archived' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function maintenance()
    {
        return $this->belongsTo(Maintenance::class, 'maintenance_id');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function countAllMaintenacePhotoByMaintenanceId($maintenanceId)
    {
        $count = self::where('maintenance_id', '=', $maintenanceId)
            ->count();

        return $count;
    }

    public function getUrl()
    {
        return Storage::url($this->document_file);
    }
}
