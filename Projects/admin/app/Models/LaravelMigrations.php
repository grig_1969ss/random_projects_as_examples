<?php

namespace App\Models;

/**
 * Class LaravelMigrations.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int id
 * @property int batch
 */
class LaravelMigrations extends BaseModel
{
    public $table = 'laravel_migrations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'batch',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'migration' => 'string',
        'batch' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'batch' => 'required',
    ];
}
