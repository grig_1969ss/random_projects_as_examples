<?php

namespace App\Models;

use App\Presenters\ClientPresenter;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Clients.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Administrator administrator
 * @property \Illuminate\Database\Eloquent\Collection auditRegisters
 * @property \Illuminate\Database\Eloquent\Collection claims
 * @property string client_name
 * @property int address_id
 * @property string telephone
 * @property \App\Models\Currencies currency
 * @property \App\Models\Contact contact
 * @property string fax
 * @property string email
 * @property string logo
 * @property bool is_administrator
 * @property int currency_id
 * @property int administrator_id
 * @property int created_by
 * @property int updated_by
 */
class Client extends BaseModel implements Searchable
{
    use PresentableTrait;

    public $table = 'clients';
    public $createContactRoute = 'clients.contacts.store';
    protected $presenter = ClientPresenter::class;
    public $contactElementName;

    public $searchableType = 'Client';

    const PAGINATE_COUNT = 10;

    public $fillable = [
        'client_name',
        'address_id',
        'telephone',
        'fax',
        'email',
        'logo',
        'is_administrator',
        'currency_id',
        'administrator_id',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'client_name' => 'string',
        'address_id' => 'integer',
        'telephone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'logo' => 'string',
        'is_administrator' => 'boolean',
        'currency_id' => 'integer',
        'administrator_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('clients.show', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->client_name.', '.$this->email,
            $url
        );
    }

    public function toDropdown($fieldName = 'client_name', $idField = 'id', $selectLabel = 'Select...')
    {
        return parent::toDropdown($fieldName, $idField, $selectLabel);
    }

    public function contact()
    {
        return $this->morphMany(Contact::class, 'contactable');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function property()
    {
        return $this->hasMany(Property::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currencies::class, 'currency_id', 'id');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getShowUrl()
    {
        return route('clients.show', $this);
    }

    public function getShowName()
    {
        return $this->client_name;
    }

    public static function getClients()
    {
        $select = [
            'clients.*',
            DB::raw('MIN(contacts.id) as contact_id'),
            'contacts.first_name',
            'contacts.surname',
            'contacts.telephone as contact_telephone',
            'contacts.email as contact_email',
        ];

        $query = DB::table('clients');

        $query->leftJoin('contacts', 'clients.id', '=', 'contacts.contactable_id')
            ->where('contacts.contactable_type', 'App\\Models\\Client');

        $user = auth()->user();

        if ($user->client_id) {
            $query->where('clients.id', '=', $user->client_id);
        }

        $clients = $query
            ->groupBy('clients.id')
            ->select($select)
            ->paginate(self::PAGINATE_COUNT);

        return $clients;
    }

    public function getById($id)
    {
        return self::find($id);
    }

    /**
     * The users that belong to the client.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function administrator()
    {
        return $this->belongsTo(Administrator::class, 'administrator_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function auditRegisters()
    {
        return $this->hasMany(AuditRegister::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function claims()
    {
        return $this->hasMany(Claim::class, 'client_id');
    }
}
