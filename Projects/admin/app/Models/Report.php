<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Report extends BaseModel
{
    public function saveData($data, $id = null)
    {
        if ($id) {
            DB::table('reports')
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = DB::table('reports')
                ->insertGetId($data);
        }

        return $id;
    }
}
