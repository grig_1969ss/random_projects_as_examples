<?php

namespace App\Models;

use App\Presenters\AgentPresenter;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Agents.
 * @version October 29, 2019, 3:31 am UTC
 *
 * @property string agent_name
 * @property int address_id
 * @property string telephone
 * @property string fax
 * @property string email
 * @property int created_by
 * @property int updated_by
 */
class Agent extends BaseModel implements Searchable
{
    use PresentableTrait;

    const PAGINATE_COUNT = 10;

    public $table = 'agents';
    public $createContactRoute = 'agents.contacts.store';
    public $contactElementName = 'agent_contact_id';
    protected $presenter = AgentPresenter::class;

    public $fillable = [
        'agent_name',
        'address_id',
        'telephone',
        'fax',
        'email',
        'created_by',
        'updated_by',
    ];

    public $searchableType = 'Agent';
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'agent_name' => 'string',
        'address_id' => 'integer',
        'telephone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function getSearchResult(): SearchResult
    {
        $url = route('agents.show', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->agent_name,
            $url
        );
    }

    public function toDropdown($fieldName = 'name', $idField = 'id', $selectLabel = 'Select...')
    {
        return parent::toDropdown('agent_name', 'id', 'No Managing Agent');
    }

    public function contacts()
    {
        return $this->morphMany(Contact::class, 'contactable')->orderBy('first_name');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function property()
    {
        return $this->hasMany(Property::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getShowUrl()
    {
        return route('agents.show', $this);
    }

    public function getShowName()
    {
        return $this->agent_name;
    }

    public static function getAgents()
    {
        $select = [
            'agents.*',
            DB::raw('MIN(contacts.id) as contact_id'),
            'contacts.first_name',
            'contacts.surname',
            'contacts.telephone as contact_telephone',
            'contacts.email as contact_email',
            'properties.property_name as property',
            'properties.id as property_id',
        ];

        $agents = self::leftJoin('contacts', function ($join) {
            $join->on('contacts.contactable_id', '=', 'agents.id')
                ->where('contacts.contactable_type', '=', 'App\\Models\\Agent');
        })
            ->leftJoin('properties', 'agents.id', '=', 'properties.agent_id')
            ->groupBy('agents.id')
            ->distinct()
            ->select($select)
            ->paginate(self::PAGINATE_COUNT);

        return $agents;
    }
}
