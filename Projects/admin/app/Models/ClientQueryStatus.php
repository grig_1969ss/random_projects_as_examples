<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ClientQueryStatuses.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 */
class ClientQueryStatus extends BaseModel
{
    use SoftDeletes;

    public $table = 'client_query_statuses';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];
}
