<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Currencies.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\User createdBy
 * @property \App\Models\User updatedBy
 * // * @property \Illuminate\Database\Eloquent\Collection claims
 * @property string name
 * @property string currency_code
 * @property string symbol
 * @property string html_code
 * @property int created_by
 * @property int updated_by
 */
class Currencies extends BaseModel
{
    use SoftDeletes, CreateAndUpdateTrait;

    public $table = 'currencies';

    public $fillable = [
        'name',
        'currency_code',
        'symbol',
        'html_code',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function getAllPairs()
    {
        return self::get(['id', 'name']);
    }
}
