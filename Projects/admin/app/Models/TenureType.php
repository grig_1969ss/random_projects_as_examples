<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TenureTypes.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property int created_by
 * @property int updated_by
 */
class TenureType extends BaseModel
{
    use SoftDeletes;

    public $table = 'tenure_types';

    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function property()
    {
        return $this->hasOne(Property::class);
    }

    public function getAllTenureTypes()
    {
        return $this->orderBy('name')->get(['id', 'name']);
    }

    public function getNameByTenureTypeId($tenureTypeId)
    {
        return self::where('id', '=', $tenureTypeId)->only('name');
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function deleteType($id)
    {
        $this->delete($id);
    }

    public function getAllPairs()
    {
        return $this->orderBy('name')->get(['id', 'name as name']);
    }

    public function getNameOfFieldName()
    {
        return 'name';
    }
}
