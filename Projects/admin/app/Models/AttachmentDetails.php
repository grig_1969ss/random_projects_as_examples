<?php

namespace App\Models;

use App\Traits\CreateAndUpdateTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Email     email
 * @property EmailType emailType
 * @property text      $content
 * @property int       $content_length
 * @property string    $content_type
 * @property int       $email_id
 * @property string    $name
 */
class AttachmentDetails extends BaseModel
{
    use SoftDeletes, CreateAndUpdateTrait;

    protected $table = 'attachment_details';
    protected $fillable = ['email_id', 'name', 'content', 'content_type', 'content_length'];
    protected $dates = ['sent_at'];

    public function email()
    {
        return $this->belongsTo(Email::class);
    }

    public function emailType()
    {
        return $this->belongsTo(EmailType::class);
    }
}
