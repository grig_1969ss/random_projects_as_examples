<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentTypes.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property string name
 * @property bool service_charge
 */
class PaymentType extends BaseModel
{
    use SoftDeletes;

    public $table = 'payment_types';

    public $fillable = [
        'name',
        'service_charge',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'service_charge' => 'boolean',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'service_charge' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payments()
    {
        return $this->hasMany(Payment::class, 'payment_type_id');
    }

    public function getAllPairs()
    {
        return self::get(['id', 'name']);
    }

    public function utilities()
    {
        return $this->hasMany(Utility::class);
    }
}
