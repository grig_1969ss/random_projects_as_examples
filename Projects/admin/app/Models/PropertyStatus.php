<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PropertyStatuses.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property string name
 * @property string button
 * @property int created_by
 * @property int updated_by
 */
class PropertyStatus extends BaseModel
{
    use SoftDeletes;

    public $table = 'property_statuses';

    public $fillable = [
        'name',
        'button',
        'created_by',
        'updated_by',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'button' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function property()
    {
        return $this->hasOne(Property::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }
}
