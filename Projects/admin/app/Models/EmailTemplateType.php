<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class EmailTemplateType extends BaseModel
{
    use SoftDeletes, PresentableTrait;

    public $table = 'email_template_types';

    public $fillable = ['name', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
}
