<?php

namespace App\Models;

use App\Presenters\PaymentPresenter;
use App\Traits\CreateAndUpdateTrait;
use App\Traits\DocumentTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Payments.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property \App\Models\Contact contact
 * @property \App\Models\Diary diary
 * @property \App\Models\PaymentType paymentType
 * @property \App\Models\Supplier supplier
 * @property int property_id
 * @property int service_charge_id
 * @property int supplier_id
 * @property int payment_type_id
 * @property int diary_id
 * @property int contact_id
 * @property string|\Carbon\Carbon payment_date
 * @property number gross_invoice_amount
 * @property number tax_amount
 * @property string payment_ref
 * @property string date_approved
 * @property string period
 * @property int quarter
 * @property bool hold
 * @property string note
 * @property string description
 * @property int created_by
 * @property int updated_by
 */
class Payment extends BaseModel
{
    use CreateAndUpdateTrait, PresentableTrait, DocumentTrait, SoftDeletes;

    public $table = 'payments';
    protected $presenter = PaymentPresenter::class;
    protected $dates = ['date_approved'];
    public $documentStoreRoute = 'payments.documents.store';

    public $fillable = [
        'property_id',
        'service_charge_id',
        'supplier_id',
        'payment_type_id',
        'diary_id',
        'contact_id',
        'payment_date',
        'gross_invoice_amount',
        'tax_amount',
        'payment_ref',
        'date_approved',
        'period',
        'quarter',
        'hold',
        'note',
        'description',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'service_charge_id' => 'integer',
        'supplier_id' => 'integer',
        'payment_type_id' => 'integer',
        'diary_id' => 'integer',
        'contact_id' => 'integer',
        'payment_date' => 'datetime',
        'gross_invoice_amount' => 'float',
        'tax_amount' => 'float',
        'payment_ref' => 'string',
        'date_approved' => 'date',
        'period' => 'string',
        'quarter' => 'integer',
        'hold' => 'boolean',
        'note' => 'string',
        'description' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function serviceCharge()
    {
        return $this->belongsTo(ServiceCharge::class);
    }

    public function saveData($data, $id = null)
    {
        if ($id) {
            $this
                ->where('id', '=', $id)
                ->update($data);
        } else {
            $id = $this
                ->insertGetId($data);
        }

        return $id;
    }

    public function getPayments()
    {
        $user = auth()->user();
        $query = DB::table('payments');

        $query->leftJoin('properties', 'properties.id', '=', 'payments.property_id')
            ->leftJoin('clients', 'clients.id', '=', 'properties.client_id');

        if ($user->client_id) {
            $query->where('properties.client_id', '=', $user->client_id);
        }

        $payments = $query->get([
            'properties.id',
            'properties.property_name',
            'payments.payment_date',
            'payments.gross_invoice_amount',
            'payments.payment_ref',
            'clients.client_name',
        ]);

        return $payments;
    }

    /**
     * @param Builder $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinProperties($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            if ($joins) {
                foreach ($joins as $join) {
                    if ($join->table == 'properties as p') {
                        return $query;
                    }
                }
            }
        }

        return $query->join('properties as p', 'p.id', '=', 'payments.property_id')->select('payments.*');
    }

    /**
     * @param Builder $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinServiceCharges($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'service_charges as sc') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('service_charges as sc', 'sc.id', '=', 'payments.service_charge_id')
            ->select('payments.*');
    }

    /**
     * @param Builder|Payment $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinAddresses($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'addresses as a') {
                    return $query;
                }
            }
        }

        return $query
            ->joinProperties(true)
            ->leftJoin('addresses as a', 'a.id', '=', 'p.address_id')
            ->select('payments.*');
    }

    /**
     * @param Builder|Payment $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinSuppliers($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'suppliers as s') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('suppliers as s', 's.id', '=', 'payments.supplier_id')
            ->select('payments.*');
    }

    /**
     * @param Builder|Payment $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinPaymentTypes($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'payment_types as pt') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('payment_types as pt', 'pt.id', '=', 'payments.payment_type_id')
            ->select('payments.*');
    }

    /**
     * @param Builder|Payment $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinDiaries($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'diaries as d') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('diaries as d', 'd.id', '=', 'payments.diary_id')
            ->select('payments.*');
    }

    /**
     * @param Builder|Payment $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeJoinContacts($query, $checkFirst = false)
    {
        if ($checkFirst) {
            $joins = $query->getQuery()->joins;

            foreach ($joins as $join) {
                if ($join->table == 'contacts as c') {
                    return $query;
                }
            }
        }

        return $query
            ->leftJoin('contacts as c', 'c.id', '=', 'payments.contact_id')
            ->select('payments.*');
    }

    /**
     * @param Payment $query
     * @param string $columnName
     *
     * @return Builder
     */
    public function scopeCustomOrderBy($query)
    {
        return $query;

        if ($columnName == 'site_no') {
            $query = $query->joinProperties();
            $columnName = 'p.reference';
        } elseif ($columnName == 'store_name') {
            $query = $query->joinProperties();
            $columnName = 'p.property_name';
        } elseif ($columnName == 'service_charge') {
            $query = $query->joinServiceCharges();
            $columnName = 'sc.year_end';
        } elseif (in_array($columnName, ['company', 'supplier'])) {
            $query = $query->joinSuppliers();
            $columnName = 's.name';
        } elseif ($columnName == 'payment_type') {
            $query = $query->joinPaymentTypes();
            $columnName = 'pt.name';
        } elseif ($columnName == 'diary_date') {
            $query = $query->joinDiaries();
            $columnName = 'd.date';
        } elseif ($columnName == 'contact_name') {
            $query = $query->joinContacts();
            $columnName = 'c.first_name';
        }

        return $query->orderBy($columnName);
    }

    /**
     * @param Builder $query
     * @param string $from
     * @param string $to
     *
     * @return Builder
     */
    public function scopeFilterByDate($query, $from, $to)
    {
        if ($from) {
            $query = $query->where('date_approved', '>=', $from);
        }

        if ($to) {
            $query = $query->where('date_approved', '<=', $to);
        }

        return $query;
    }

    /**
     * @param Builder|Payment $query
     * @param bool $checkFirst
     *
     * @return Builder
     */
    public function scopeFilterByClient($query, $checkFirst = true)
    {
        $clientId = auth()->user()->client_id;

        if ($clientId) {
            $query = $query->joinProperties($checkFirst);

            return $query->where('p.client_id', $clientId);
        }

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function diary()
    {
        return $this->belongsTo(Diary::class, 'diary_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function document()
    {
        return $this->morphMany(Document::class, 'docable')->where('archived', false)->orderByDesc('created_at');
    }
}
