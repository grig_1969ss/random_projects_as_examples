<?php

namespace App\Models;

/**
 * Class PropertyQueries.
 * @version October 29, 2019, 3:32 am UTC
 *
 * @property int property_id
 * @property string|\Carbon\Carbon diary_date
 * @property int status_id
 * @property int created_by
 * @property int updated_by
 */
class PropertyQuery extends BaseModel
{
    public $table = 'property_queries';

    public $fillable = [
        'property_id',
        'diary_date',
        'status_id',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_id' => 'integer',
        'diary_date' => 'datetime',
        'status_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public static function countPropertyQueries()
    {
        return self::where('status_id', 1)->count('id');
    }

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [

    ];
}
