<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ModifierStamper
{
    /**
     * The name of the "created by" column.
     *
     * @var string
     */
    protected $createdBy = 'created_by';

    /**
     * The name of the "updated by" column.
     *
     * @var string
     */
    protected $updatedBy = 'updated_by';

    /**
     * Set the value of the "created by" attribute.
     *
     * @param mixed $value
     */
    public function setCreatedBy($model, $value)
    {
        $model->{$this->createdBy} = $value;
    }

    /**
     * Set the value of the "updated by" attribute.
     *
     * @param mixed $value
     */
    public function setUpdatedBy($model, $value)
    {
        $model->{$this->updatedBy} = $value;
    }

    /**
     * Called when the model is creating.
     *
     * @param Model $model
     *
     * @return void
     */
    public function creating(Model $model)
    {
        $userId = Auth::id();

        if ($userId !== null) {
            $this->setCreatedBy($model, $userId);
            $this->setUpdatedBy($model, $userId);
        }
    }

    /**
     * Called when the model is saving.
     *
     * @param Model $model
     *
     * @return void
     */
    public function saving(Model $model)
    {
        $userId = Auth::id();

        // if ($userId !== null && ! $model->isDirty($model->updatedBy)) {
        $this->setUpdatedBy($model, $userId);
        //}
    }
}
