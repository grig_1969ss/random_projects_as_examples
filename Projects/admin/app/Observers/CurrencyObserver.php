<?php

namespace App\Observers;

use App\Models\Currencies;

class CurrencyObserver
{
    public function saving(Currencies $currency)
    {
        $currency->html_code = htmlentities($currency->symbol);
    }
}
