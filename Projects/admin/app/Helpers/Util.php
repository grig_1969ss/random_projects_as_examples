<?php

namespace App\Helpers;

use App\Models\Address;
use App\Models\Diary;
use App\Models\Note;
use App\Models\ObjectType;
use App\Models\Property;
use Carbon\Carbon;
use Hashids\Hashids;

class Util
{
    public static function sortDiariesList($diaries)
    {
        $sortedDates = array_fill(0, 6, []);
        foreach ($diaries as $diary) {
            $date = strtotime($diary->date);
            if ($date < strtotime('today +0 day')) {
                array_push($sortedDates[0], $diary);
            } elseif ($date < strtotime('today +1 day')) {
                array_push($sortedDates[1], $diary);
            } elseif ($date < strtotime('today +2 day')) {
                array_push($sortedDates[2], $diary);
            } elseif ($date < strtotime('next sunday +1 day')) {
                array_push($sortedDates[3], $diary);
            } elseif ($date < strtotime('next sunday +8 day')) {
                array_push($sortedDates[4], $diary);
            } else {
                array_push($sortedDates[5], $diary);
            }
        }
        $sortedDateTitles = [
            'Overdue',
            'Today',
            'Tomorrow',
            'This Week',
            'Next Week',
            'Later',
        ];
        $sortedDates = array_combine($sortedDateTitles, $sortedDates);

        return $sortedDates;
    }

    public static function showValue($data)
    {
        return $data ?? '-';
    }

    public static function showAgent($property)
    {
        if (! $property->agent) {
            return '-';
        }

        $agent = $property->agent;
        $email = $agent->email;
        $comma = $email ? ', ' : '';

        return $agent->agent_name.$comma.$email;
    }

    public static function getAllNotesByObjectIdAndTypeName($objectId, $typeName)
    {
        $objectType = ObjectType::getObjectByName($typeName);
        $notes = Note::getAllNotesByObjectId($objectId, $objectType->id);

        return $notes;
    }

    public static function getAllDiariesByPropertyId($propertyId)
    {
        $diaries = Diary::getAllDiariesByPropertyId($propertyId);
        $diaries = self::sortDiariesList($diaries);

        return $diaries;
    }

    public static function getAllDiariesByClientId($clientId)
    {
        $diaries = Diary::getAllDiariesByClientId($clientId);
        $diaries = self::sortDiariesList($diaries);

        return $diaries;
    }

    public static function getAllActiveDiaries($typeId)
    {
        $diaries = Diary::getAllActiveDiaries($typeId);
        $diaries = self::sortDiariesList($diaries);

        return $diaries;
    }

    public static function saveFormValues($input, $objectId, $table, $hasAddress = false)
    {
        if ($hasAddress) {
            $addressArray['address_1'] = $input['address_1'];
            $addressArray['address_2'] = $input['address_2'];
            $addressArray['town'] = $input['town'];
            $addressArray['county'] = $input['county'];
            $addressArray['post_code'] = $input['post_code'];
            $input = array_diff_key($input, $addressArray);
        }

        if ($objectId) {
            if ($hasAddress) {
                (new Address)->saveData($addressArray, $input['address_id']);
            }

            $tableObj = new $table();
            $tableObj->saveData($input, $objectId);
            unset($tableObj);
        } else {
            if ($hasAddress) {
                $input['address_id'] = (new Address)->saveData($addressArray);
            }

            $tableObj = new $table();
            $objectId = $tableObj->saveData($input);
            unset($tableObj);
        }

        return $objectId;
    }

    public static function addNote($objectId, $objectType, $noteTypeId, $noteText)
    {
        $objectTypeId = ObjectType::getObjectByName($objectType);

        $data = [
            'object_type_id' => $objectTypeId->id,
            'object_id' => $objectId,
            'note_type_id' => $noteTypeId,
            'note_text' => $noteText,
        ];

        Note::create($data);
        unset($noteObj);
    }

    public static function showText($data)
    {
        return $data ? str_replace(PHP_EOL, '<br>', $data) : '-';
    }

    public static function showYes($data)
    {
        return $data == 1 ? 'yes' : '-';
    }

    public static function showYesNo($data)
    {
        return $data == 1 ? 'Yes' : 'No';
    }

    public static function actualVsBudget($actual, $budget)
    {
        if (isset($actual) && isset($budget)) {
            $actual = str_replace(['£', ','], '', $actual);
            $budget = str_replace(['£', ','], '', $budget);
            if (is_numeric($actual) && is_numeric($budget)) {
                if ($actual != 0 && $budget != 0) {
                    $actual_vs_budget = number_format((($actual / $budget) - 1) * 100, 0).'%';

                    return $actual_vs_budget;
                } else {
                    return 'N/A';
                }
            }
        } else {
            return 'N/A';
        }
    }

    public static function joinName($items, $sep = ' ')
    {
        $name = implode($sep, $items);

        return $name;
    }

    public static function link($url, $text)
    {
        return \Html::link($url, $text ?: '-');
    }

    public static function emailLink($data)
    {
        return $data ? '<a href="mailto:'.$data.'">'.$data.'</a>' : '-';
    }

    public static function getClientIdQuery($type = 'AND')
    {
        $user = auth()->user();

        // Check if user is a client or has a client id assigned
        if (isset($user->client_id)) {
            if (strlen($user->client_id) > 0) {
                return ' '.$type." p.client_id = {$user->client_id}";
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public static function propertyId()
    {
        $propertyID = null;

        if (isset(request('property')->id)) {
            $propertyID = request('property')->id;
        } elseif (is_numeric(request()->get('property'))) {
            $propertyID = request()->get('property');
        }

        return $propertyID;
    }

    public static function bodyWithSignature($request, $user, $onlySignature = false)
    {
        $signature = view('admin.emails.partials.emailSignature', ['user' => $user])->render();

        $signature = '<br><br>'.$signature;

        if ($onlySignature) {
            return $signature;
        }

        $mainBody = view('emails.base_email', ['body' => nl2br($request['body']), 'user' => $user])->render();

        $position = stripos($mainBody, '</body>');

        $newBody = substr_replace($mainBody, $signature, $position, 0);

        return $newBody;
    }

    public static function replyEmailTime($email)
    {
        if (! $email || ! $email->from) {
            return '';
        }

        $cC = [];
        if ($email->cc) {
            $ccs = json_decode($email->cc);
            if (gettype($ccs) == 'array') {
                foreach ($ccs as $cc) {
                    $cC[] = $cc->Email;
                }
            } else {
                $cC[] = $email->cc;
            }
        }

        $comma = '';
        if (! empty($cC)) {
            $comma = ',';
        }

        $ccStr = implode(',', $cC);

        $time = Carbon::parse($email->created_at);

        $header = ' ---------- Forwarded message --------- <br>';
        $from = 'From: '.$email->from_name.' <'.$email->from.'><br>';
        $date = 'Date: '.$time->format('d M Y').' at '.$time->format('h:i').'<br>';
        $subject = 'Subject: Fwd: '.$email->subject.'<br>';
        $to = 'To: '.$email->to.$comma.$ccStr.'<br>';

        return $header.$from.$date.$subject.$to;
    }

    public static function replyTo($subject, $email)
    {
        $to = request()->get('to');
        if ($to) {
            return $to;
        }

        if ($email->property_id && request()->type != 'fw') {
            $hash = new Hashids();
            $hashId = $hash->encode($email->id);

            $identifier = ($email->client && $email->client->inbound_identifier) ? $email->client->inbound_identifier.'.' : '';

            $replyTo = 'capa-'.$hashId.'@'.$identifier.'inbound.capa.uk.com';

            return $replyTo;
        }

        return ! isset($subject) ? $email->from : '';
    }

    public static function propertyName($property)
    {
        return '#'.$property->reference.' - '.$property->property_name;
    }

    public static function attachmentsAsString($email)
    {
        $attachments = [];

        foreach ($email->attachments as $attachment) {
            if (strpos($email->html_body, 'cid:'.$attachment->name) !== false) {
                continue;
            }
            $attachments[] = $attachment->id;
        }

        $str = implode(',', $attachments);

        return $str;
    }

    public static function emailName($email)
    {
        return Carbon::parse($email->created_at)->format('d/m/Y h:m').' -- '.$email->subject;
    }

    public static function diaryProperty($propertyId = null)
    {
        if (! $propertyId) {
            return '-';
        }

        $property = Property::find($propertyId);

        return $property->reference.' '.$property->property_name;
    }

    public static function las4sc($property)
    {
        return $property->last4ServiceCharges();
    }

    public static function las4ins($property)
    {
        return $property->last4Insurances();
    }

    public static function showReportDatePickers($showInUrl, $segment)
    {
        return (bool) (
            $showInUrl || in_array($segment, ['weekly-rent-invoices', 'weekly-store-approvals'])
        );
    }

    public static function showReportSurveyor($showInUrl, $segment)
    {
        return (bool) (
            $showInUrl || in_array($segment, ['annual-service-charge', 'annual-insurance'])
        );
    }

    public static function showReportExportButton($showInUrl, $segment)
    {
        return (bool) (
            $showInUrl || in_array($segment, ['weekly-store-approvals', 'approvals-by-store'])
        );
    }

    public static function serviceChargeOrInsuranceLatestMonth($array, $key)
    {
        reset($array);
        $firstKey = key($array);

        return
            isset($array[$firstKey][$key])
                ? Carbon::parse($array[$firstKey][$key])->format('F')
                : null;
    }
}
