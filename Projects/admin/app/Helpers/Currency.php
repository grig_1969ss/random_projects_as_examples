<?php

namespace App\Helpers;

class Currency
{
    public static function format2($data)
    {
        if (isset($data)) {
            return is_numeric($data) ? number_format($data, 2) : $data;
        } else {
            return;
        }
    }

    public static function gbp($data)
    {
        $data = str_replace(['£', ','], '', $data);

        if ($data == null) {
            return '-';
        } elseif ($data == 0) {
            return '£0.00';
        } else {
            return is_numeric($data) ? '£'.number_format($data, 2) : $data;
        }
    }
}
