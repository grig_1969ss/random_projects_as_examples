<?php

namespace App\Helpers\Reports;

use App\Helpers\Date;
use App\Models\Payment;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class Approval
{
    protected $input;
    public $viewDate;
    public $order;
    public $dateFrom;
    public $dateTo;

    /**
     * @var Collection
     */
    public $approvals;

    public function __construct($input, $isPDF = false)
    {
        $this->input = $input;
        $this->viewDate = '';

        if (! $isPDF) {
            $this->setData();
        } else {
            $this->setDataPDF();
        }
    }

    protected function baseSet()
    {
        $this->setViewDate();
        $this->setOrder();
        $this->setDates();
    }

    protected function setData()
    {
        $this->baseSet();
        $this->setApprovals();
    }

    protected function setDataPDF()
    {
        $this->baseSet();
        $this->setApprovalsPDF();
    }

    protected function setViewDate()
    {
        if (isset($this->input['icon']) && $this->input['icon'] == 1) {
            $this->viewDate = date('d/m/Y');
        }
    }

    protected function setOrder()
    {
        $this->order = isset($this->input['orderCol']) ? $this->input['orderCol'] : 'site_no';
    }

    protected function setDates()
    {
        if (isset($this->input['dateFrom'])) {
            $this->dateFrom = Date::format($this->input['dateFrom'], 'd/m/Y', 'Y-m-d');
        }

        if (isset($this->input['dateTo'])) {
            $this->dateTo = Date::format($this->input['dateTo'], 'd/m/Y', 'Y-m-d');
        }
    }

    protected function setApprovals()
    {
        $supplier = '(select name from suppliers where id = payments.supplier_id limit 1) as company';
        $paymentType = '(select name from payment_types where id = payments.payment_type_id limit 1) as payment_type';

        $this->approvals = Payment::customOrderBy($this->order)
            ->joinProperties(true)
            ->filterByDate($this->dateFrom, $this->dateTo)
            ->filterByClient()
            ->select([
                'payments.id as id',
                'p.id as property_id',
                'payments.payment_type_id',
                DB::raw($paymentType),
                'p.reference as site_no',
                'p.property_name as store_name',
                DB::raw($supplier),
                'payments.payment_ref as invoice_no',
                DB::raw('DATE_FORMAT(payments.payment_date, "%d/%m/%Y") as invoice_date'),
                'gross_invoice_amount as amount',
                'payments.period',
                'payments.note as notes',
                'payments.description',
                'payments.date_approved',
            ])

            ->get();
    }

    protected function setApprovalsPDF()
    {
        $this->setApprovals();

        $this->approvals = $this->approvals->groupBy(function ($item, $key) {
            return $item->payment_type_id == 2 ? 'Rent Invoices' : 'Service Charge & Insurance';
        })->sortKeysDesc();
    }

    public function toArray()
    {
        return (array) $this;
    }

    protected function getHeaders()
    {
        return [
            'site_no',
            'address',
            'company',
            'invoice_no',
            'invoice_date',
            'amount',
            'description',
            'note',
        ];
    }

    public function toCSV()
    {
        $headers = $this->getHeaders();
        $data = [$headers];

        return array_merge($data, $this->approvals->toArray());
    }
}
