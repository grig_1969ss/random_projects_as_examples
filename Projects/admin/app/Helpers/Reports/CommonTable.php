<?php

namespace App\Helpers\Reports;

use App\Helpers\Date;
use App\Helpers\Util;
use App\Models\Property;
use App\Models\User;
use Carbon\Carbon;

class CommonTable
{
    public static $approvals = [
        'constants' => [
            'pageName' => 'Approvals',
            'pageTitle' => 'Reports | Approvals',
            'page' => 'ReportsApprovals',
        ],
        'headers' => [
            ['title' => 'Site No', 'data' => 'site_no'],
            ['title' => 'Store Name', 'data' => 'store_name'],
            ['title' => 'Company', 'data' => 'company'],
            ['title' => 'Invoice No', 'data' => 'invoice_no'],
            ['title' => 'Invoice Date', 'data' => 'invoice_date'],
            ['title' => 'Amount', 'data' => 'formatted_amount'],
            ['title' => 'Description', 'data' => 'payment_type'],
            ['title' => 'Period', 'data' => 'period'],
            ['title' => 'Notes', 'data' => 'notes'],
            ['title' => 'Documents', 'data' => 'attached_document'],
        ],
        'date_column' => 'date_approved',
        'dataMethod' => 'getApprovalsDataTAble',
    ];

    public static $rentApprovals = [
        'constants' => [
            'pageName' => 'Weekly Store Approvals',
            'pageTitle' => 'Reports | Weekly Store Approvals',
            'page' => 'RentReportsApprovals',
        ],
        'headers' => [
            ['title' => 'Site No', 'data' => 'site_no'],
            ['title' => 'Store Name', 'data' => 'store_name'],
            ['title' => 'Company', 'data' => 'company'],
            ['title' => 'Invoice No', 'data' => 'invoice_no'],
            ['title' => 'Invoice Date', 'data' => 'invoice_date'],
            ['title' => 'Amount', 'data' => 'formatted_amount'],
            ['title' => 'Description', 'data' => 'payment_type'],
            ['title' => 'Period', 'data' => 'period'],
            ['title' => 'Date Approved', 'data' => 'date_approved'],
            ['title' => 'Notes', 'data' => 'notes'],
            ['title' => 'Documents', 'data' => 'attached_document'],
        ],
        'date_column' => 'date_approved',
        'dataMethod' => 'getRentApprovalsDataTAble',
    ];

    public static $weeklyRentInvoices = [
        'constants' => [
            'pageName' => 'Weekly Rent Invoices',
            'pageTitle' => 'Reports | Weekly Rent Invoices',
            'page' => 'ReportsWeeklyRentInvoices',
        ],
        'headers' => [
            ['title' => 'Site No', 'data' => 'site_no'],
            ['title' => 'Store Name', 'data' => 'store_name'],
            ['title' => 'Company', 'data' => 'company'],
            ['title' => 'Invoice No', 'data' => 'invoice_no'],
            ['title' => 'Invoice Date', 'data' => 'invoice_date'],
            ['title' => 'Amount', 'data' => 'formatted_amount'],
            ['title' => 'Description', 'data' => 'payment_type'],
            ['title' => 'Period', 'data' => 'period'],
            ['title' => 'Date Approved', 'data' => 'date_approved'],
            ['title' => 'Notes', 'data' => 'notes'],
            ['title' => 'Documents', 'data' => 'attached_document'],
        ],
        'date_column' => 'date_approved',
        'dataMethod' => 'getWeeklyRentInvoicesDataTAble',
    ];

    public static $invoicesOnHold = [
        'constants' => [
            'pageName' => 'Invoices on Hold',
            'pageTitle' => 'Reports | Invoices on Hold',
            'page' => 'ReportsInvoicesOnHold',
        ],
        'headers' => [
            ['title' => 'Property No', 'data' => 'property_reference'],
            ['title' => 'Supplier', 'data' => 'supplier_name'],
            ['title' => 'Contact Name', 'data' => 'contact_name'],
            ['title' => 'Inv Number', 'data' => 'payment_ref'],
            ['title' => 'Invoice Date', 'data' => 'date_payment'],
            ['title' => 'Inv Gross', 'data' => 'invoice_amount'],
            ['title' => 'Inv VAT', 'data' => 'amount_tax'],
            ['title' => 'Type', 'data' => 'type_of_payment'],
            ['title' => 'Period', 'data' => 'period'],
            ['title' => 'Notes', 'data' => 'note'],
            ['title' => 'Diary Date', 'data' => 'date_diary'],
        ],
        'date_column' => '',
        'dataMethod' => 'getInvoicesOnHoldDataTable',
    ];

    // because of static, function can not be called as array value to merge
    public static function mergePropertiesHeaders($constants, $andRecs)
    {
        return self::$$constants['headers'] = array_merge(
            self::$$constants['headers'], self::budgetYears($andRecs)
        );
    }

    public static $annualServiceCharge = [
        'constants' => [
            'pageName' => 'Annual Service Charge',
            'pageTitle' => 'Reports | Annual Service Charge',
            'page' => 'ReportsAnnualServiceCharge',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
            ['title' => 'Y/E Month', 'data' => 'month'],
        ],
        'date_column' => '',
        'dataMethod' => 'getAnnualServiceChargeDataTable',
    ];

    public static $annualInsurance = [
        'constants' => [
            'pageName' => 'Annual Insurance',
            'pageTitle' => 'Reports | Annual Insurance',
            'page' => 'ReportsAnnualInsurance',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
            ['title' => 'Y/E Month', 'data' => 'month'],
        ],
        'date_column' => '',
        'dataMethod' => 'getAnnualInsuranceDataTable',
    ];

    public static $missingInsurance = [
        'constants' => [
            'pageName' => 'Missing Insurance',
            'pageTitle' => 'Reports | Missing Insurance',
            'page' => 'ReportsMissing Insurance',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
            ['title' => 'Y/E Month', 'data' => 'month'],
            ['title' => 'Main Capa Contact', 'data' => 'main_contact'],
        ],
        'date_column' => '',
        'dataMethod' => 'getMissingInsuranceDataTable',
    ];

    public static $missingServiceChargeBudgets = [
        'constants' => [
            'pageName' => 'Missing Service Charge Budgets',
            'pageTitle' => 'Reports | Missing Service Charge Budgets',
            'page' => 'ReportsMissingServiceChargeBudgets',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
            ['title' => 'Y/E Month', 'data' => 'month'],
            ['title' => 'Main Capa Contact', 'data' => 'main_contact'],

        ],
        'date_column' => '',
        'dataMethod' => 'getMissingServiceChargeBudgetsDataTable',
    ];

    public static $missingServiceChargeReconciliations = [
        'constants' => [
            'pageName' => 'Missing Service Charge Reconciliations',
            'pageTitle' => 'Reports | Missing Service Charge Reconciliations',
            'page' => 'ReportsMissingServiceChargeReconciliations',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
            ['title' => 'Month', 'data' => 'month'],
            ['title' => 'Main Capa Contact', 'data' => 'main_contact'],
        ],
        'date_column' => '',
        'dataMethod' => 'getMissingServiceChargeReconciliationsDataTable',
    ];

    public static $serviceChargeAndInsuranceRenewalDates = [
        'constants' => [
            'pageName' => 'Service Charge And Insurance Renewal Dates',
            'pageTitle' => 'Reports | Service Charge And Insurance Renewal Dates',
            'page' => 'ReportsServiceChargeAndInsuranceRenewalDates',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
        ],
        'date_column' => '',
        'dataMethod' => 'getServiceChargeAndInsuranceRenewalDatesDataTable',
    ];

    public static $missingLettersToFranchisee = [
        'constants' => [
            'pageName' => 'Missing Letters To Franchisee',
            'pageTitle' => 'Reports | Missing Letters To Franchisee',
            'page' => 'ReportsMissingLettersToFranchisee',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],

        ],
        'date_column' => '',
        'dataMethod' => 'getMissingLettersToFranchiseeDataTable',
    ];

    public static $missingRealEstateChangeForm = [
        'constants' => [
            'pageName' => 'Missing Real Estate Change Form',
            'pageTitle' => 'Reports | Missing Real Estate Change Form',
            'page' => 'ReportsMissingRealEstateChangeForm',
        ],
        'headers' => [
            ['title' => 'Store Name', 'data' => 'property_name'],
            ['title' => 'Store No', 'data' => 'reference'],
        ],
        'date_column' => '',
        'dataMethod' => 'getMissingRealEstateChangeDataTable',
    ];

    public static $types = [
        'approvals' => 'approvals',
        'weekly-store-approvals' => 'rentApprovals',
        'weekly-rent-invoices' => 'weeklyRentInvoices',
        'invoices-on-hold' => 'invoicesOnHold',
        'annual-service-charge' => 'annualServiceCharge',
        'annual-insurance' => 'annualInsurance',

        'missing-service-charge-budgets' => 'missingServiceChargeBudgets',
        'missing-service-charge-reconciliations' => 'missingServiceChargeReconciliations',
        'missing-insurance' => 'missingInsurance',
        'service-charge-and-insurance-renewal-dates' => 'serviceChargeAndInsuranceRenewalDates',
        'missing-letters-to-franchisee' => 'missingLettersToFranchisee',
        'missing-real-estate-change-form' => 'missingRealEstateChangeForm',
    ];

    public static function getConstants($type)
    {
        $constants = self::$types[$type];

        return self::$$constants['constants'];
    }

    public static function getHeaders($type)
    {
        $constants = self::$types[$type];

        if (in_array($type, [
            'annual-service-charge',
            'annual-insurance',
            'missing-service-charge-budgets',
            'missing-service-charge-reconciliations',
            'missing-insurance',
        ])) {
            return self::mergePropertiesHeaders($constants, false);
        } elseif (in_array($type, [
            'missing-letters-to-franchisee',
        ])) {
            return self::mergePropertiesHeaders($constants, 'missing-letters');
        } elseif (in_array($type, ['missing-real-estate-change-form'])) {
            return self::mergePropertiesHeaders($constants, 'missing-real-estate');
        } elseif (in_array($type, ['service-charge-and-insurance-renewal-dates'])) {
            return self::mergePropertiesHeaders($constants, 'renewal-dates');
        }

        return self::$$constants['headers'];
    }

    public static function getData($type)
    {
        $constants = self::$types[$type];

        return self::$$constants['dataMethod'];
    }

    public static function getDateColumn($type)
    {
        $constants = self::$types[$type];

        return self::$$constants['date_column'];
    }

    public static function getSurveyors()
    {
        $clientId = auth()->user()->client_id;

        $surveyorsIds = Property::where('client_surveyor_id', '<>', null)
            ->where('client_id', $clientId)
            ->groupBy('client_surveyor_id')
            ->pluck('client_surveyor_id')
            ->toArray();

        return User::whereIn('id', $surveyorsIds)
            ->select('id', 'name')
            ->cursor();
    }

    public static function filterByDate($data, $dateColumn, $request)
    {
        if (! isset($request['show_datepicker'])) {
            return $data;
        }

        if (isset($request['date_from'])) {
            $from = Date::toMysql($request['date_from']);
            $data = $data->where("{$dateColumn}", '>=', $from);
        }

        if (isset($request['date_to'])) {
            $to = Date::toMysql($request['date_to']);
            $data = $data->where("{$dateColumn}", '<=', $to);
        }

        return $data;
    }

    public static function filterSurveyorId($data, $surveyorId)
    {
        if (! isset($surveyorId)) {
            return $data;
        }

        return $data->where('client_surveyor_id', (int) $surveyorId);
    }

    public static function budgetYears($andRecs = false)
    {
        $yearBudgetTd = $yearBudgetTh = $yearRecTd = $yearRecTh = $yearInsTd = $yearInsTh = \Carbon\Carbon::now()->year;
        $subYears = \Carbon\Carbon::now()->subYears(3)->year;

        $budgetYears = [];

        for ($year = $yearBudgetTd; $subYears <= $year; $year--) {
            // when we need only Reconciliation, we don't need amounts in Budget column, so prevent to make them
            if (! $andRecs) {
                $budgetYears[] = ['title' => "Budget {$year}", 'data' => 'sc_or_ins_'.$year];
            } elseif ($andRecs === 'missing-letters') {
                $budgetYears[] = ['title' => "Budget {$year}", 'data' => 'sc_budget_recd_'.$year];
                $budgetYears[] = ['title' => "Rec {$year}", 'data' => 'sc_annual_recd_'.$year];
            } elseif ($andRecs === 'missing-real-estate') {
                $budgetYears[] = ['title' => "Budget {$year}", 'data' => 'sc_budget_estate_'.$year];
            } elseif ($andRecs === 'renewal-dates') {
                $budgetYears[] = ['title' => "Service charge Renewal Date {$year}", 'data' => 'sc_r_d_'.$year];
                $budgetYears[] = ['title' => "Insurance renewal Date {$year}", 'data' => 'ins_r_d_'.$year];
            }
        }

        return $budgetYears;
    }

    public static function checkLast4YearsSC($property, $currency, $reconciliationsDataTable, $tableForMissingOnes = false)
    {
        $serviceCharges = $property->last4ServiceCharges($reconciliationsDataTable);
        $insurances = [];
        $renewalDates = false;

        if (request()->segment(3) === 'service-charge-and-insurance-renewal-dates') {
            $insurances = $property->last4Insurances();
            $renewalDates = true;
        }

        $currentYear = Carbon::now()->year;
        $lastYearToCheck = Carbon::now()->subYears(3)->year;
        $missingOnes = false;
        $atLeastOneValue = false;

        $serviceChargeYears = array_keys($serviceCharges);
        $insurancesYears = array_keys($insurances);

        for ($year = $currentYear; $lastYearToCheck <= $year; $year--) {
            // renewal dates will be checked when it is true, means when url is service-charge-and-insurance-renewal-dates (see above)
            if (                                        // check insurances when it is renewal dates
                in_array($year, $serviceChargeYears) || (in_array($year, $insurancesYears) && $renewalDates)
            ) {
                $atLeastOneValue = true;

                if (! $renewalDates) {
                    $property->{'sc_or_ins_'.$year} = $tableForMissingOnes ? '<span style="color: darkgreen">OK</span>' : $currency.number_format($serviceCharges[$year]['annual_budget'], 2);
                    $property = self::addRecValues($property, $serviceCharges[$year], $year);
                } else {
                    $property = self::addRenewalDates($property, $serviceCharges, $insurances, $year);
                }
            } else {
                // cases when property doesn't have sc or ins
                $missingOnes = true;

                if (! $renewalDates) { //$renewalDates will be initialized at bottom
                    $property->{'sc_or_ins_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
                    $property->{'sc_budget_recd_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
                    $property->{'sc_annual_recd_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
                    $property->{'sc_budget_estate_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
                } else {
                    $property->{'sc_r_d_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
                    $property->{'ins_r_d_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
                }
            }
        }

        if (! $missingOnes && (request()->segment(3) == 'missing-service-charge-budgets')) {
            return false;
        }

        if (! $atLeastOneValue && (request()->segment(3) == 'missing-service-charge-reconciliations')) {
            return false;
        }

        // get month from latest service charge
        $month = Util::serviceChargeOrInsuranceLatestMonth($serviceCharges, 'year_end');

        return [$property, $month];
    }

    public static function addRenewalDates($property, $serviceCharges, $insurances, $year)
    {
        // prevent useless process
        if (! in_array(request()->segment(3), ['service-charge-and-insurance-renewal-dates'])) {
            return $property;
        }

        if (isset($serviceCharges[$year])) {
            $property->{'sc_r_d_'.$year} = Carbon::parse($serviceCharges[$year]['year_end'])->format('d/m/Y');
        } else {
            $property->{'sc_r_d_'.$year} = '<span style="color: darkred">MISSING</span>';
        }

        if (isset($insurances[$year])) {
            $property->{'ins_r_d_'.$year} = Carbon::parse($insurances[$year]['insurance_renewal_date'])->format('d/m/Y');
        } else {
            $property->{'ins_r_d_'.$year} = '<span style="color: darkred">MISSING</span>';
        }

        return $property;
    }

    public static function addRecValues($property, $serviceCharge, $year)
    {
        // prevent useless process
        if (! in_array(request()->segment(3), ['missing-letters-to-franchisee', 'missing-real-estate-change-form'])) {
            return $property;
        }

        // Budget Details Recd //
        if (! $serviceCharge['budget_details_recd']) {
            $property->{'sc_budget_recd_'.$year} = '<span style="color: darkred">NO</span>';
            $property->{'sc_budget_estate_'.$year} = '<span style="color: darkred">NO</span>';
        } else {
            if ($serviceCharge['franchisee_updated']) {
                $property->{'sc_budget_recd_'.$year} = '<span style="color: darkgreen">YES</span>';
            } else {
                $property->{'sc_budget_recd_'.$year} = '<span style="color: darkred">NO</span>';
            }

            // Real Estate Updated in Budget Details Recd
            if ($serviceCharge['real_estate_updated']) {
                $property->{'sc_budget_estate_'.$year} = '<span style="color: darkgreen">YES</span>';
            } else {
                $property->{'sc_budget_estate_'.$year} = '<span style="color: darkred">NO</span>';
            }
        }

        // Annual Reconciliation Recd //
        if (! $serviceCharge['annual_reconciliation_recd']) {
            $property->{'sc_annual_recd_'.$year} = '<span style="color: darkred">NO</span>';
        } else {
            if ($serviceCharge['annual_franchisee_updated']) {
                $property->{'sc_annual_recd_'.$year} = '<span style="color: darkgreen">YES</span>';
            } else {
                $property->{'sc_annual_recd_'.$year} = '<span style="color: darkred">NO</span>';
            }
        }

        return $property;
    }

    public static function checkLast4YearsIns($property, $currency, $tableForMissingOnes = false)
    {
        $insurances = $property->last4Insurances();

        $currentYear = Carbon::now()->year;
        $lastYearToCheck = Carbon::now()->subYears(3)->year;
        $missingOnes = false;

        for ($year = $currentYear; $lastYearToCheck <= $year; $year--) {
            if (in_array(
                $year, array_keys($insurances)
            )) {
                $property->{'sc_or_ins_'.$year} = $tableForMissingOnes ? '<span style="color: darkgreen">OK</span>' : $currency.number_format($insurances[$year]['insurance_premium'], 2);
            } else {
                $missingOnes = true;
                $property->{'sc_or_ins_'.$year} = $tableForMissingOnes ? '<span style="color: darkred">MISSING</span>' : '-';
            }
        }

        if (! $missingOnes && (request()->segment(3) == 'missing-insurance')) {
            return false;
        }

        $month = Util::serviceChargeOrInsuranceLatestMonth($insurances, 'insurance_renewal_date');

        return [$property, $month];
    }
}
