<?php

namespace App\Helpers\Reports;

use App\Helpers\Currency;
use App\Helpers\Date;

class MasterSpreadsheet
{
    protected $property;
    protected $schedules;
    protected $rents;

    public function __construct($property)
    {
        $this->property = $property;
    }

    public function fillPropertyData()
    {
        return [
            'branch_no'                  => $this->property->reference,
            'property_name'              => $this->property->property_name,
            'address'                    => $this->property->address,
            'lease_start_date'           => Date::dmY($this->property->lease_start_date),
            'rent_review_date'           => Date::dmY($this->property->rent_review_date),
            'lease_expiry_date'          => Date::dmY($this->property->lease_expiry_date),
            'current_rent'               => utf8_decode(Currency::gbp($this->property->current_rent)),
            'break_option_date'          => Date::dmY($this->property->break_option_date),
            'break_option_notice_period' => $this->property->break_option_notice_period,
            'rateable_value'             => ($this->property->rateable_value) ? utf8_decode(Currency::gbp($this->property->rateable_value)) : '',
            'landlord_name'              => $this->property->landlord_name,
            'landlord_address'           => $this->property->landlord_address,
            'landlord_agent'             => $this->property->landlord_agent,
            'agent_address'              => $this->property->agent_address,
            'repair_liability'           => ($this->property->repair_liability) ? utf8_decode(Currency::gbp($this->property->repair_liability)) : '',
            'schedule_of_condition'      => $this->property->schedule_of_condition,
        ];
    }

    public static function reportHeaders()
    {
        return [
            'branch_no',
            'property_name',
            'address',
            'lease_start_date',
            'rent_review_date',
            'lease_expiry_date',
            'current_rent',
            'break_option_date',
            'break_option_notice_period',
            'rateable_value',
            'landlord_name',
            'landlord_address',
            'landlord_agent',
            'agent_address',
            'repair_liability',
            'schedule_of_condition',
        ];
    }
}
