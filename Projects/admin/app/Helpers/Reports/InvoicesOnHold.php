<?php

namespace App\Helpers\Reports;

use App\Models\Payment;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class InvoicesOnHold
{
    protected $input;
    public $order;

    /**
     * @var Collection
     */
    public $approvals;

    public function __construct($input, $isCSV = false)
    {
        $this->input = $input;
        $this->approvals = collect();

        if (! $isCSV) {
            $this->setData();
        } else {
            $this->setDataCSV();
        }
    }

    protected function baseSet()
    {
        $this->setOrder();
    }

    protected function setData()
    {
        $this->baseSet();
        $this->setApprovals();
    }

    protected function setDataCSV()
    {
        $this->baseSet();
        $this->setApprovalsCSV();
    }

    protected function setOrder()
    {
        $this->order = isset($this->input['orderCol']) ? $this->input['orderCol'] : 'site_no';
    }

    protected function setApprovals()
    {
        //TODO: delete this customOrders
        //$this->approvals = Payment::customOrderBy($this->order)
        $this->approvals = Payment::customOrderBy()
            ->joinProperties(true)
            ->filterByClient()
            ->where('hold', true)
            ->get();
    }

    protected function setApprovalsCSV()
    {
        $supplier = '(select name from suppliers where id = payments.supplier_id limit 1)';
        $diaryDate = '(select DATE_FORMAT(date, "%d/%m/%Y") from diaries where id = payments.diary_id limit 1)';
        $contactName = '(select concat(first_name, " ", surname) from contacts where id = payments.contact_id limit 1) as contact_name';
        $type = '(select name from payment_types where id = payments.payment_type_id limit 1) as type';

        $this->approvals = Payment::customOrderBy($this->order)
            ->joinProperties(true)
            ->filterByClient()
            ->where('hold', true)
            ->select([
                'p.reference',
                DB::raw($supplier),
                DB::raw($contactName),
                'payment_ref',
                DB::raw('DATE_FORMAT(payment_date, "%d/%m/%Y")'),
                'gross_invoice_amount',
                'tax_amount',
                DB::raw($type),
                'period',
                'note',
                DB::raw($diaryDate),
            ])
            ->get();
    }

    public function toArray()
    {
        return (array) $this;
    }

    protected function getHeaders()
    {
        return [
            'property_no',
            'supplier',
            'contact_name',
            'inv_number',
            'invoice_date',
            'inv_gross',
            'inv_vat',
            'type',
            'period',
            'notes',
            'diary_date',
        ];
    }

    public function toCSV()
    {
        $headers = $this->getHeaders();
        $data = [$headers];

        return array_merge($data, $this->approvals->toArray());
    }
}
