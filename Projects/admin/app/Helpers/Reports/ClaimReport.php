<?php

namespace App\Helpers\Reports;

use App\Models\Claim;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ClaimReport
{
    protected $input;
    public $order;

    /**
     * @var Collection
     */
    public $claims;

    public function __construct($input, $isCSV = false)
    {
        $this->input = $input;
        $this->claims = collect();

        if (! $isCSV) {
            $this->setData();
        } else {
            $this->setData(true);
        }
    }

    protected function setData($isCsv = false)
    {
        $this->setOrder();
        $this->setClaims($isCsv);
    }

    protected function setOrder()
    {
        $this->order = isset($this->input['orderCol']) ? $this->input['orderCol'] : 'client';
    }

    protected function setClaims($isCsv)
    {
        $currencySymbol = $isCsv ? 'cu.symbol' : 'cu.html_code';
        $client = '(select client_name from clients where id = claims.client_id limit 1) as client';
        $auditRegister = '(select audit_name from audit_registers where id = claims.audit_register_id limit 1) as audit_register';
        $auditor = '(select name from auditors where id = claims.auditor_id limit 1) as auditor';
        $claimType = '(select name from claim_types where id = claims.claim_type_id limit 1) as claim_type';
        $claimSubtype = '(select name from claim_subtypes where id = claims.claim_subtype_id limit 1) as claim_subtype';
        $claimStatus = '(select name from claim_statuses where id = claims.claim_status_id limit 1) as claim_status';
        $grossValue = 'concat('.$currencySymbol.', (original_net_value + vat)) as custom_gross_value';
        $netRecovery = 'concat('.$currencySymbol.', (original_net_value - claim_adjustment - cancelled_claim)) as custom_net_recovery';
        $originalNetValue = 'concat('.$currencySymbol.', original_net_value) as original_net_value';
        $vat = 'concat('.$currencySymbol.', vat) as vat';
        $claimAdjustment = 'concat('.$currencySymbol.', claim_adjustment) as claim_adjustment';
        $cancelledClaim = 'concat('.$currencySymbol.', cancelled_claim) as cancelled_claim';
        $invoiceDate = 'date_format(invoice_date, "%d/%m/%Y") as custom_invoice_date';
        $clientDate = 'date_format(client_date, "%d/%m/%Y") as custom_client_date';

        $select = [
            DB::raw($client),
            DB::raw($auditRegister),
            'claim_number',
            DB::raw($auditor),
            'supplier_number',
            'supplier_name',
            DB::raw($claimType),
            DB::raw($claimSubtype),
            'invoice_number',
            DB::raw($invoiceDate),
            DB::raw($originalNetValue),
            DB::raw($vat),
            DB::raw($grossValue),
            DB::raw($claimAdjustment),
            DB::raw($cancelledClaim),
            DB::raw($netRecovery),
            DB::raw($claimStatus),
            DB::raw($clientDate),
            'po_number',
            'property_number',
            'property_address',
            'supplier_contact',
        ];

        if (! $isCsv) {
            $select[] = 'claims.id as id';
        }

        $this->claims = Claim::joinCurrencies()->customOrderBy($this->order)->filterByClient()->select($select)->get();
    }

    public function toArray()
    {
        return (array) $this;
    }

    protected function getHeaders()
    {
        return [
            'client',
            'audit_register',
            'claim_number',
            'auditor',
            'supplier_number',
            'supplier_name',
            'claim_type',
            'claim_subtype',
            'invoice_number',
            'invoice_date',
            'original_net_value',
            'vat',
            'gross_value',
            'claim_adjustment',
            'cancelled_claim',
            'net_recovery',
            'claim_status',
            'client_date',
            'po_number',
            'property_number',
            'property_address',
            'supplier_contact',
        ];
    }

    public function toCSV()
    {
        $headers = $this->getHeaders();
        $data = [$headers];

        return array_merge($data, $this->claims->toArray());
    }
}
