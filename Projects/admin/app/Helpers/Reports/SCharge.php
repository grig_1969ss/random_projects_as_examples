<?php

namespace App\Helpers\Reports;

use App\Helpers\Date;

class SCharge
{
    public static function setDates($properties, $props)
    {
        $maxDate = null;
        $minDate = null;
        $yearArray = [];
        $totalArray = [];
        // Loop through properties to get the earliest and latest service charge date
        foreach ($properties as $property) {
            if (isset($property->service_charge)) {
                foreach ($property->service_charge as $key => $service_charge) {
                    if (is_null($maxDate) || $service_charge->year_end > $maxDate) {
                        $maxDate = Date::Y($service_charge->year_end);
                    }
                    if (is_null($minDate) || $service_charge->year_end < $minDate) {
                        $minDate = Date::Y($service_charge->year_end);
                    }
                }
            }
        }

        while ($minDate <= $maxDate) {
            $yearArray[] = $minDate;
            $totalArray[$minDate] = 0;
            $props[0][] = (string) $minDate.' Amount';
            $props[0][] = (string) $minDate.' %';
            $props[0][] = (string) $minDate.' Year End';
            $minDate++;
        }

        return [
            'props' => $props,
            'yearArray' => $yearArray,
            'totalArray' => $totalArray,
        ];
    }
}
