<?php

namespace App\Helpers\Reports;

class Report
{
    public static function formatQueryDate($array, $key)
    {
        if (array_key_exists($key, $array) && ! empty($array[$key])) {
            $array[$key] = Date::format($array[$key], 'd/m/Y', 'Y-m-d');
        } else {
            $array[$key] = null;
        }

        return $array;
    }

    public static function collectToCSV($data)
    {
        if (($fp = fopen('php://output', 'w')) !== false) {
            foreach ($data->all() as $prop) {
                fputcsv($fp, (array) $prop->toArray());
            }
            fclose($fp);
        }
    }

    public static function addSortQuery($inputs, $key = null)
    {
        return isset($inputs['orderCol']) ? $inputs['orderCol'] : $key;
    }

    public static function addDateQuery($inputs, $key = null)
    {
        return isset($inputs['date']) ? Date::toMysql($inputs['date']) : $key;
    }

    public static function yearsRange()
    {
        $results = [];
        $starting_year = date('Y', strtotime('+1 year'));
        $ending_year = date('Y', strtotime('-3 year'));

        for ($starting_year; $starting_year >= $ending_year; $starting_year--) {
            $results[$starting_year] = 'Y/E '.$starting_year;
        }

        return $results;
    }

    public static function setViewDate($value)
    {
        if (isset($value['icon']) && $value['icon'] == 1) {
            return date('d/m/Y');
        }

        return '';
    }
}
