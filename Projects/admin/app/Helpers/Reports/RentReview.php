<?php

namespace App\Helpers\Reports;

use App\Helpers\Currency;
use App\Helpers\Date;
use App\Models\Document;
use App\Models\Rent;
use App\Repositories\PropertyRepository;
use Illuminate\Support\Facades\Storage;

class RentReview
{
    public $order;
    public $properties;
    public $viewDate;
    protected $dateFrom;
    protected $dateTo;
    protected $inputs;
    protected $rents;
    protected $schedules;

    public function __construct($inputs, $isCSV = false)
    {
        $this->inputs = $inputs;
        $this->viewDate = '';

        if (! $isCSV) {
            $this->setData();
        } else {
            $this->setDataCSV();
        }
    }

    protected function baseSet()
    {
        $this->setOrder();
        $this->setViewDate();
        $this->setDates();
        $this->setRents();
        $this->setSchedules();
    }

    protected function setData()
    {
        $this->baseSet();
        $this->setProperties();
    }

    protected function setDataCSV()
    {
        $this->baseSet();
        $this->setPropertiesCSV();
    }

    protected function setOrder()
    {
        $this->order = isset($this->inputs['orderCol']) ? $this->inputs['orderCol'] : 'rent_review_date';
    }

    protected function setViewDate()
    {
        if (isset($this->inputs['icon']) && $this->inputs['icon'] == 1) {
            $this->viewDate = date('d/m/Y');
        }
    }

    protected function setDates()
    {
        if (isset($this->inputs['dateFrom']) && $this->inputs['dateFrom']) {
            $this->dateFrom = Date::format($this->inputs['dateFrom'], 'd/m/Y', 'Y-m-d');
        }

        if (isset($this->inputs['dateTo']) && $this->inputs['dateTo']) {
            $this->dateTo = Date::format($this->inputs['dateTo'], 'd/m/Y', 'Y-m-d');
        }
    }

    protected function setProperties()
    {
        $property = new PropertyRepository();

        $properties = $property->getAllPropertiesDueRentReview($this->order, $this->dateFrom, $this->dateTo);
        $this->properties = $this->getPropertiesRentAndSOC($properties);
    }

    protected function setPropertiesCSV()
    {
        $property = new PropertyRepository();

        $this->properties = $property->getAllPropertiesDueRentReview($this->order, $this->dateFrom, $this->dateTo);
    }

    protected function getPropertiesData()
    {
        $data = [];

        foreach ($this->properties as $property) {
            $data[] = $this->fillItem($property);
        }

        return $data;
    }

    protected function fillItem($property)
    {
        return [
            'region_name'             => $property->region_name,
            'branch_no'               => $property->reference,
            'address'                 => $property->address,
            'lease_start_date'        => Date::dmY($property->lease_start_date),
            'rent_review_date'        => Date::dmY($property->rent_review_date),
            'lease_expiry_date'       => Date::dmY($property->lease_expiry_date),
            'current_rent'            => $this->getCurrentRent($property),
            'break_option_date'       => Date::dmY($property->break_option_date),
            'notice_date'             => Date::dmY($property->break_option_notice_period),
            'rateable_value'          => ($property->rateable_value) ? utf8_decode(Currency::gbp($property->rateable_value)) : '',
            'landlord_name'           => $property->landlord_name,
            'landlord_address'        => $property->landlord_address,
            'landlord_agent'          => $property->landlord_agent,
            'agent_address'           => $property->agent_address,
            'repair_liability'        => ($property->repair_liability) ? utf8_decode(Currency::gbp($property->repair_liability)) : '',
            'schedule_of_condition'   => $this->getScheduleOfCondition($property),
            'repair_obligations_note' => $property->repair_obligations_note,
            'user_clause_note'        => $property->user_clause_note,
        ];
    }

    protected function getPropertiesRentAndSOC($properties)
    {
        foreach ($properties as $property) {
            $property->current_rent = $this->fillPropertyCurrentRent($property);
            $property = $this->fillScheduleOfCondition($property);
            $property->rateable_value = $property->rateable_value ? Currency::gbp($property->rateable_value) : '-';
        }

        return $properties;
    }

    protected function fillPropertyCurrentRent($property)
    {
        $currentRent = '-';

        foreach ($this->rents as $rent) {
            if ($property->id == $rent->property_id) {
                $currentRent = Currency::gbp($rent->rent);

                break;
            }
        }

        return $currentRent;
    }

    protected function fillScheduleOfCondition($property)
    {
        foreach ($this->schedules as $schedule) {
            if ($property->id == $schedule->property_id) {
                $property->schedule_of_condition_link = Storage::url($schedule->document_file);
                $property->schedule_of_condition = $schedule->document_name;

                return $property;
            }
        }

        $property->schedule_of_condition_link = "/properties/{$property->id}";
        $property->schedule_of_condition = '-';

        return $property;
    }

    protected function getScheduleOfCondition($property)
    {
        foreach ($this->schedules as $schedule) {
            if ($property->id == $schedule->property_id) {
                return Storage::url($schedule->document_file);
            }
        }

        return '';
    }

    protected function getCurrentRent($property)
    {
        foreach ($this->rents as $rent) {
            if ($property->id == $rent->property_id) {
                return utf8_decode(Currency::gbp($rent->rent));
            }
        }

        return '';
    }

    protected function setRents()
    {
        $this->rents = Rent::getCurrentRents();
    }

    protected function setSchedules()
    {
        $this->schedules = Document::getDocumentsByType(7);
    }

    protected function getHeaders()
    {
        return [
            'region_name',
            'branch_no',
            'address',
            'lease_start_date',
            'rent_review_date',
            'lease_expiry_date',
            'current_rent',
            'break_option_date',
            'notice_date',
            'rateable_value',
            'landlord_name',
            'landlord_address',
            'landlord_agent',
            'agent_address',
            'repair_liability',
            'schedule_of_condition',
            'repair_obligations_note',
            'user_clause_note',
        ];
    }

    public function toCSV()
    {
        $headers = $this->getHeaders();
        $data = [$headers];

        return array_merge($data, $this->getPropertiesData());
    }
}
