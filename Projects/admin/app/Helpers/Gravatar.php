<?php

    function gravatar_url($email, $size = 120)
    {
        $email = md5($email);

        return "https://gravatar.com/avatar/{$email}".http_build_query([
            'd' => 'identicon',
            's' => $size,
        ]);
    }
