<?php

// in index page, make display words from column nmae
if (! function_exists('displayColumn')) {
    function displayColumn($column)
    {
        $column = str_replace('_', ' ', $column);

        return ucwords($column);
    }
}

// left menu
if (! function_exists('settingsMenu')) {
    function settingsMenu()
    {
        return [
            'DocumentType' => 'Document Types',
            'AlienationType' => 'Alienation Types',
            'AppealType' => 'Appeal Types',
            'ApportionmentType' => 'Apportionment Types',
            'BreakType' => 'Break Types',
            'ClaimStatus' => 'Claim Statuses',
            'ClaimType' => 'Claim Types',
            'ClaimSubtype' => 'Claim Subtypes',
            'ClientQueryStatus' => 'Client Query Statuses',
            'Currencies' => 'Currencies',
            'DisposalType' => 'Disposal Types',
            'NoteType' => 'Note Types',
            'OwnerType' => 'Owner Types',
            'PaymentType' => 'Payment Types',
            'PropertyStatus' => 'Property Statuses',
            'PropertyType' => 'Property Types',
            'RepairClassificationType' => 'Repair Classification Types',
            'ServiceChargeStatus' => 'Service Charge Statuses',
            'StoreType' => 'Store Types',
            'SublettingType' => 'Subletting Types',
            'TenureType' => 'Tenure Types',
            'User' => 'Users',
            'EmailTemplate' => 'Email Templates',
            'EmailTemplateType' => 'Email Template Types',
        ];
    }
}

// add space in model name words
if (! function_exists('displayName')) {
    function displayName($model)
    {
        return preg_replace('/(?<!\ )[A-Z]/', ' $0', $model);
    }
}

// generate class from model name
if (! function_exists('classObject')) {
    function classObject($name)
    {
        $model = 'App\Models\\'.$name;

        return new $model();
    }
}

// instead of 0 and 1, show Yes or No
if (! function_exists('filterValue')) {
    function filterValue($key, $value)
    {
        $keys = [
            'attach_to_email',
            'archived',
            'archived',
            'service_charge',
        ];
        if (in_array($key, $keys)) {
            return $value ? 'Yes' : 'No';
        }

        if ($key == 'avatar') {
            return "<img src='".$value."' alt=''>";
        }

        return $value;
    }
}

// in blade decide show this field or not
if (! function_exists('showThisField')) {
    function showThisField($model, $field)
    {
        return in_array($field, modelCreateFields($model));
    }
}

// in blade decide show this field or not
if (! function_exists('inputValues')) {
    function userInputValues()
    {
        return [
            [
                'name' => 'email',
                'type' => 'email',
                'placeholder' => 'Email',
            ],
            [
                'name' => 'job_title',
                'type' => 'text',
                'placeholder' => 'Job Title',
            ],
        ];
    }
}

// special fields that are visible only for that Model
function modelCreateFields($model)
{
    $modelFields = [
        'DocumentType' => ['attach_to_email'],
        'ClaimSubtype' => ['claim_type_id'],
        'PaymentType' => ['service_charge'],
        'PropertyStatus' => ['button'],
        'ServiceChargeStatus' => ['button'],
        'Currencies' => ['currency_code', 'symbol'],
        'EmailTemplate' => ['subject', 'body', 'template_type_id'],
        'User' => ['username', 'email', 'job_title', 'avatar', 'client_id', 'region_id', 'read_only'],
    ];

    return $modelFields[$model] ?? [];
}
