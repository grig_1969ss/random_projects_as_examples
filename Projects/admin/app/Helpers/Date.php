<?php

namespace App\Helpers;

use Carbon\Carbon;

class Date
{
    public static function dmY($date)
    {
        if ($date) {
            return date('d/m/Y', strtotime($date));
        } else {
            return;
        }
    }

    public static function dmYHi($date)
    {
        if ($date) {
            return date('d/m/Y H:i', strtotime($date));
        } else {
            return;
        }
    }

    public static function Md($date)
    {
        if ($date) {
            return date('M d', strtotime($date));
        } else {
            return;
        }
    }

    public static function M($date)
    {
        if ($date) {
            return date('F', strtotime($date));
        } else {
            return;
        }
    }

    public static function dFY($date)
    {
        if ($date) {
            return date('d F Y', strtotime($date));
        }

        return false;
    }

    public static function Y($date)
    {
        if ($date) {
            return date('Y', strtotime($date));
        } else {
            return;
        }
    }

    public static function toMysql($date)
    {
        if ($date) {
            return date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $date)));
        } else {
            return;
        }
    }

    public static function format($date, $from, $to)
    {
        return Carbon::createFromFormat($from, $date)->format($to);
    }

    public static function dateOrNow($date)
    {
        return $date ? (new Carbon($date)) : Carbon::now();
    }
}
