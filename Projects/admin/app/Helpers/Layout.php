<?php

    function site_title($page_title = null)
    {
        $page_title = isset($page_title) ? ' | '.$page_title : '';

        return config('app.title').$page_title;
    }
