<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Administrator;
use App\Models\Agent;
use App\Models\Auditor;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Landlord;
use App\Models\Property;
use App\Models\Region;
use App\Models\Supplier;
use Illuminate\Console\Command;

class DummyData extends Command
{
    private $faker = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert real data to dummy data for testing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->faker = \Faker\Factory::create('en_GB');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Address::chunk(100, function ($addresses) {
            foreach ($addresses as $address) {
                $address->address_1 = $this->faker->streetAddress;
                $address->address_2 = null;
                $address->town = $this->faker->cityPrefix.' '.$this->faker->firstName.$this->faker->citySuffix;
                $address->county = $this->faker->county();
                $address->post_code = $this->faker->postcode;

                $address->save();
            }
        });

        Administrator::chunk(100, function ($administrators) {
            foreach ($administrators as $administrator) {
                $administrator->name = $this->faker->name;

                $administrator->save();
            }
        });

        Agent::chunk(100, function ($agents) {
            foreach ($agents as $agent) {
                $agent->agent_name = $this->faker->firstName.' '.$this->faker->companySuffix;
                $agent->telephone = $this->faker->phoneNumber;
                $agent->fax = $this->faker->phoneNumber;
                $agent->email = $this->faker->email;

                $agent->save();
            }
        });

        Auditor::chunk(100, function ($auditors) {
            foreach ($auditors as $auditor) {
                $auditor->name = $this->faker->name;
                $auditor->email = $this->faker->email;

                $auditor->save();
            }
        });

        Client::chunk(100, function ($clients) {
            foreach ($clients as $client) {
                $client->client_name = $this->faker->firstName.' '.$this->faker->companySuffix;
                $client->telephone = $this->faker->phoneNumber;
                $client->fax = $this->faker->phoneNumber;
                $client->email = $this->faker->email;

                $client->save();
            }
        });

        Contact::chunk(100, function ($contacts) {
            foreach ($contacts as $contact) {
                $contact->first_name = $this->faker->firstName;
                $contact->surname = $this->faker->lastName;
                $contact->telephone = $this->faker->phoneNumber;
                $contact->fax = $this->faker->phoneNumber;
                $contact->email = $this->faker->email;
                $contact->mobile = $this->faker->phoneNumber;

                $contact->save();
            }
        });

        Landlord::chunk(100, function ($landlords) {
            foreach ($landlords as $landlord) {
                $landlord->landlord_name = $this->faker->firstName.' '.$this->faker->companySuffix;
                $landlord->telephone = $this->faker->phoneNumber;
                $landlord->fax = $this->faker->phoneNumber;
                $landlord->email = $this->faker->email;

                $landlord->save();
            }
        });

        Property::chunk(100, function ($properties) {
            foreach ($properties as $property) {
                $property->store_contact = $this->faker->name;
                $property->store_telephone = $this->faker->phoneNumber;

                $property->save();
            }
        });

        Region::chunk(100, function ($regions) {
            foreach ($regions as $region) {
                $region->manager = $this->faker->name;

                $region->save();
            }
        });

        Supplier::chunk(100, function ($suppliers) {
            foreach ($suppliers as $supplier) {
                $supplier->name = $this->faker->name;
                $supplier->telephone = $this->faker->phoneNumber;
                $supplier->fax = $this->faker->phoneNumber;
                $supplier->email = $this->faker->email;

                $supplier->save();
            }
        });
    }
}
