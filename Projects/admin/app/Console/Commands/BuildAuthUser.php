<?php

namespace App\Console\Commands;

use App\Jobs\RegisterUser;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class BuildAuthUser extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'capa:build-user {firstName} {lastName} {email} {password} {role=web}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates new user into the system';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->dispatch(new RegisterUser(
            $this->argument('firstName'),
            $this->argument('lastName'),
            $this->argument('email'),
            $this->argument('password'),
            $this->argument('role')
        ));

        $this->info("{$this->argument('role')} user created successfully.");
    }
}
