<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
        'url_secret' => env('POSTMARK_URL_SECRET'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'dial9' => [
        'token' => env('DIAL9_TOKEN'),
        'secret' => env('DIAL9_SECRET'),
    ],

    'openweather' => [
        'token' => env('OPENWEATHER_TOKEN'),
        'location' => env('OPENWEATHER_DEFAULT_ID', '2647570'),
    ],

    'postcodes' => [
        'account' => env('POSTCODES_ACCOUNT'),
        'password' => env('POSTCODES_PASSWORD'),
    ],

    'inbound' => [
        'replyto' => env('USE_INBOUND_REPLYTO', false),
        'domain' => env('INBOUND_DOMAIN', 'inbound.darceyquigley.co.uk'),
    ],

    'hashid' => [
        'library' => env('HASHID_LIBRARY', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'),
    ],

    'email_templates' => [
        'sc_realestate_budget_type_id' => 1,
        'sc_franchisee_budget_type_id' => 2,
        'sc_franchisee_rec_type_id' => 3,
        'ins_franchisee_rec_type_id' => 4,
    ],
];
