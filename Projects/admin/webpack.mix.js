const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/custom.js', 'public/js/moved')
    //.sass('resources/sass/pixel.scss', 'public/css')
    .styles([
        'resources/assets/dist/css/pixel.css',
        'resources/assets/dist/css/pixel-bootstrap.css',
        'resources/assets/dist/css/pixel-owl-carousel.css',
        'resources/assets/dist/css/pixel-font-awesome.css',
        'resources/assets/dist/css/pixel-main.css',
        'resources/assets/css/customvvvv.scss',
        'resources/assets/css/custom.css',
       // 'resources/assets/css/bootstrap-datetimepicker.min.css',
        'public/vendor/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
    ], 'public/css/all.css')
    .version();
