$(document).ready(function () {
    $('.uik-tutorials-top-bar__btnMenu').on('click', function () {
        var $sidebar = $('.uik-tutorials-navigation__wrapper');

        if ($sidebar.hasClass('uik-tutorials-navigation__isMenuOpen')) {
            $sidebar.removeClass('uik-tutorials-navigation__isMenuOpen');
        } else {
            $sidebar.addClass('uik-tutorials-navigation__isMenuOpen');
        }
    });

    $('.uik-social-header-user__wrapper').on('click', function () {
        var $userMenu = $('.uik-menuDrop__list');

        if ($userMenu.is(':visible')) {
            $userMenu.hide();
        } else {
            $userMenu.show();
        }
    });

    $('body').on('click', '.clickable', function () {
        location.href = $(this).data('url');
    });

    $('.uik-tab__item').on('click', function () {
        var $tabContent = $(this).parent().siblings('.tab-content');

        $('.uik-tab__item').removeClass('active');
        $(this).addClass('active');

        $tabContent.children().removeClass('active');
        $tabContent.find('[data-target="#' + $(this).attr('id') + '"]').addClass('active');
    });

    $('.fly-add').flyadd();

    $(document).on('click', '.select-file', function () {
        $('.inputfile').trigger('click');
    });

    $(document).on('change', '.inputfile', function (e) {
        var fileName = e.target.files[0].name;

        $('.file-wrapper .form-control').val(fileName);
    });

    tagsEvents();

    if ($('#datetimepicker1').length > 0) {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY HH:mm'
        });
    }
});

function reloadPage() {
    location.reload();
}

function tagsEvents() {
    $('.add-tag').on('click', function () {
        $('.new-tag').toggle();
        $('.back-btn').trigger('click');
    });

    $('.new-tag .close').on('click', function () {
        $('.new-tag').hide();
        $('.back-btn').trigger('click');
    });

    $('.add-tag-btn').on('click', function () {
        $('.current-tags').hide();
        $('.create-tag').show();
    });

    $('.back-btn').on('click', function () {
        $('.current-tags').show();
        $('.create-tag').hide();
    });

    $('.colours .uik-tag__wrapper').on('click', function () {
        $('.colours .uik-tag__wrapper').removeClass('active');
        $(this).addClass('active');
    });

    $('.create-tag .save-btn').on('click', function () {
        var name = $('.create-tag .uik-input__input').val();
        var tagColourId = $('.colours .uik-tag__wrapper.active').data('colour');
        var url = $(this).data('url');

        if (name && tagColourId) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    name: name,
                    tag_colour_id: tagColourId,
                    _token: $('meta[name="csrf_token"]').attr('content')
                },
                success: function (response) {
                    $('.tags-wrapper').replaceWith(response.view);

                    tagsEvents();
                }
            });
        }
    });

    $('.attached-tag').on('click', function () {
        var url = $(this).data('url');

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                _token: $('meta[name="csrf_token"]').attr('content')
            },
            success: function (response) {
                $('.tags-wrapper').replaceWith(response.view);

                tagsEvents();
            }
        });
    });

    $('.current-tags .tag').on('click', function () {
        var url = $(this).data('url');

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                _token: $('meta[name="csrf_token"]').attr('content')
            },
            success: function (response) {
                $('.tags-wrapper').replaceWith(response.view);
                $('.new-tag').hide();

                tagsEvents();
            }
        });
    });
}
