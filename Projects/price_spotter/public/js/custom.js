$(document).ready(function () {
    // $.ajax({
    //     dataType: "json",
    //     url: "/get-types",
    //     success: function (result) {
    //         $('#doc-types').html(result);
    //         $('.spinners').addClass('sr-only');
    //     }
    // });

    $(document).on('click', '.edit', function () {
        let id = $(this).data('id');
        let spinner = $('.spinners');
        spinner.removeClass('sr-only');
        $('.doc-list-table').addClass('sr-only');

        $.ajax({
            dataType: "json",
            url: `/show-type/${id}`,
            success: function (result) {
                spinner.addClass('sr-only');
                $('#edit-part').html(result);
            }
        });
    });

    $(document).on('click', '.archive-type', function () {
        $('.archive-modal').removeClass('sr-only');
        let id = $(this).data('id');
        let deleteInput = $('#delete-id');
        deleteInput.attr('value', id);
    });

    $('#new-doc-type').click(function () {
        let spinner = $('.spinners');
        spinner.removeClass('sr-only');
        $('.doc-list-table').addClass('sr-only');
        $('#add-page').removeClass('sr-only');
        spinner.addClass('sr-only');
        // $('#uik-content').addClass('sr-only')

    });

    $(document).on('click', '.go-back', function () {
        $('#doc-types').removeClass('sr-only');
        $('.doc-list-table').removeClass('sr-only');
        $('#add-page, #edit-page').addClass('sr-only');
    });

});




