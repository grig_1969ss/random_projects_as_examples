var system = require('system');
var args = system.args;
var url = args[1];
var path = './public/js/phantomjs/pages/' + args[2];
var page = new WebPage();
var fs = require('fs');

page.open(url, function (status) {
    just_wait();
});

function just_wait() {
    setTimeout(function () {
        // console.log returns the page content as well,
        // but we can store the content as html and parse from it like below

        console.log(page.content);

        //save the content in html file for future parse

      /*fs.write(path, page.content
            , 'w');*/
        phantom.exit();
    }, 3000);
}
