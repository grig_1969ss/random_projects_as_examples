@extends('auth.app')

@section('content')
    <main>
        <section class="min-vh-100 d-flex align-items-center">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-8 col-md-6 col-lg-7 offset-md-1 order-md-2"><!-- Image --> <img
                            src="{{asset('dist/img/pages/signin.svg')}}" alt="..." class="img-fluid"></div>
                    <div class="col-12 col-md-5 col-lg-4 order-md-1">
                        <div>
                            <div class="text-center text-md-center mt-4 mt-md-0"><h1>Sign in</h1>
                                <p class="text-gray mb-0">Simplify your workflow in minutes.</p></div>
                            <div class="btn-wrapper mt-4 mb-5 text-center"><a href="#"
                                                                              class="btn btn-sm btn-icon btn-twitter animate-up-1 mb-1 mb-sm-0"><span
                                        class="btn-inner-icon"><i class="fab fa-twitter"></i> </span><span
                                        class="btn-inner-text">Twitter</span> </a><a href="#"
                                                                                     class="btn btn-sm btn-icon btn-facebook animate-up-1 mb-1 mb-sm-0"><span
                                        class="btn-inner-icon"><i class="fab fa-facebook"></i> </span><span
                                        class="btn-inner-text">Facebook</span></a></div>
                            <span class="clearfix"></span>


                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i
                                                    class="far fa-user"></i></span></div>
                                        <input
                                            name="email"
                                            type="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            id="input-email"
                                            placeholder="Enter email"
                                            value="{{ old('email') }}"
                                            required>
                                        @error('email')
                                        <p class="">{{ $errors->first('email') }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i
                                                    class="fas fa-unlock-alt"></i></span></div>
                                        <input
                                            name="password"
                                            class="form-control"
                                            placeholder="Password"
                                            type="password"
                                            required>
                                        @error('password')
                                        <p class="">{{ $errors->first('password') }}</p>
                                        @enderror

                                        <div class="input-group-append"><span class="input-group-text"><i
                                                    class="far fa-eye"></i></span></div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-block btn-secondary">Sign in</button>
                                </div>
                            </form>


                            <div class="d-block d-sm-flex justify-content-between align-items-center mt-4"><span><small>Not registered?</small> <a
                                        href="signup.html" class="small font-weight-bold">Create account</a></span>
                                <div><a href="{{ route('password.request') }}" class="small text-right">Lost password?</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- Core -->




{{--    <div class="uik-buildings-signup__pageWrapper">--}}
{{--        <div class="uik-widget__wrapper uik-buildings-signup__widgetWrapper">--}}
{{--            <div class="uik-buildings-signup__content">--}}
{{--                <div class="fixed-icon">--}}
{{--                    <img src="{{ asset('img/villa-white.svg') }}">--}}
{{--                </div>--}}
{{--                <div class="uik-widget-content__wrapper uik-buildings-signup__left">--}}
{{--                    <h2 class="uik-headline__wrapper mt-3">Sign in</h2>--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}
{{--                        <div class="uik-form-input-group__vertical">--}}
{{--                            <div class="">--}}
{{--                                <span class="uik-content-title__wrapper">E-mail address</span>--}}
{{--                                <div class="uik-input__inputWrapper">--}}
{{--                                    <input type="text"--}}
{{--                                           class="uik-input__input @error('email') uik-input__errorHighlight @enderror"--}}
{{--                                           name="email"--}}
{{--                                           placeholder="your@email.com"--}}
{{--                                           value="{{ old('email') }}">--}}
{{--                                    @error('email')--}}
{{--                                    <p class="uik-input__errorMessage">{{ $errors->first('email') }}</p>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="">--}}
{{--                                <span class="uik-content-title__wrapper">Password</span>--}}
{{--                                <div class="uik-input__inputWrapper">--}}
{{--                                    <input type="password"--}}
{{--                                           class="uik-input__input @error('password') uik-input__errorHighlight @enderror"--}}
{{--                                           name="password"--}}
{{--                                           placeholder="·····">--}}
{{--                                    @error('password')--}}
{{--                                    <p class="uik-input__errorMessage">{{ $errors->first('password') }}</p>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button type="submit"--}}
{{--                                class="uik-btn__base uik-btn__success uik-buildings-signup__btnAction mt-3">--}}
{{--                            <span class="uik-btn__content">Sign In</span>--}}
{{--                        </button>--}}
{{--                        <div class="uik-divider__horizontal uik-divider__margin"></div>--}}
{{--                        <a href="#" class="uik-btn__base uik-btn__transparent uik-buildings-signup__btnAction">--}}
{{--                            <span class="uik-btn__content">Contact Us</span>--}}
{{--                        </a>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--                <div class="uik-buildings-signup__right">--}}
{{--                    <h2>Welcome to Ellis Exclusive Villas</h2>--}}
{{--                    <p class="uik-buildings-signup__desc">--}}
{{--                        @if($_SUBDOMAIN == 'admin')--}}
{{--                            Looking for our <a href="http://portal.{{ config('app.host') }}">customer portal</a> or--}}
{{--                            <a href="http://agent.{{ config('app.host') }}">agent portal</a>?--}}
{{--                        @elseif($_SUBDOMAIN == 'portal')--}}
{{--                            Looking for our <a href="http://admin.{{ config('app.host') }}">admin portal</a> or--}}
{{--                            <a href="http://agent.{{ config('app.host') }}">agent portal</a>?--}}
{{--                        @else--}}
{{--                            Looking for our <a href="http://portal.{{ config('app.host') }}">customer portal</a> or--}}
{{--                            <a href="http://admin.{{ config('app.host') }}">admin portal</a>?--}}
{{--                        @endif--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <p>Forgot your password? <a href="{{ route('password.request') }}">Get a new password</a></p>--}}
{{--    </div>--}}
@endsection
