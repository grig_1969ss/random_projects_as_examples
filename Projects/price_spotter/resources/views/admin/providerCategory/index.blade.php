@extends('layouts.app')

@section('content')
    <div class="section section-sm">
        <div class="container">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-5 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Provider Categories</h5>
                    </div>
                    <div class="dropdown">
                        <div class="btn-group mr-2 mb-2">
                            <a class="btn add-property btn-outline-primary">Add Property</a>
                        </div>
                        <div class="btn-group mr-2 mb-2">
                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option>All properties</option>
                                <option>Active</option>
                                <option>Disposed</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Reference</th>
                                <th scope="col">Url</th>
                                <th scope="col">Provider</th>
                                <th scope="col">Archived</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td><a href="{{route('provider-categories.show', $category->id)}}">{{$category->name ?? ' - '}}</a></td>
                                    <td>{{$category->reference ?? ' - '}}</td>
                                    <td>{{$category->url}}</td>
                                    <td>{{$category->provider->name}}</td>
                                    <td>{{$category->archived ? 'Yes': 'No'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <nav class="property-list-pagination" aria-label="Page navigation example">
                            <span>Showing {{$categories->firstItem()}} to {{$categories->lastItem()}} of {{$categories->total()}} entries</span>
                            {{$categories->links()}}
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
