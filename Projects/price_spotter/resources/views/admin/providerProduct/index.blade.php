@extends('layouts.app')

@section('content')
    <div class="section section-sm">
        <div class="container">
            <!-- Title  -->
            <div class="row">
                <div class="col-12 mb-5 d-flex justify-content-between">
                    <div>
                        <h5 class="font-weight-bold">Provider Products</h5>
                    </div>
                    <div class="dropdown">
                        <div class="btn-group mr-2 mb-2">
                            <a class="btn add-property btn-outline-primary">Add Property</a>
                        </div>
                        <div class="btn-group mr-2 mb-2">
                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option>All properties</option>
                                <option>Active</option>
                                <option>Disposed</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End of Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-5">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Reference</th>
                                <th scope="col">Url</th>
                                <th scope="col">Provider</th>
                                <th scope="col">Approved</th>
                                <th scope="col">Archived</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td><a href="{{route('provider-products.show', $product->id)}}">{{$product->name ?? ' - '}}</a></td>
                                    <td>{{$product->reference ?? ' - '}}</td>
                                    <td>{{$product->url}}</td>
                                    <td>{{$product->provider->name}}</td>
                                    <td>
                                        {{!is_null($product->checked_at) ? $product->user->name . ' ' . \Carbon\Carbon::parse($product->checked_at)->format('d/m/y') : 'No'}}
                                    </td>
                                    <td>{{$product->archived ? 'Yes': 'No'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <nav class="property-list-pagination" aria-label="Page navigation example">
                            <span>Showing {{$products->firstItem()}} to {{$products->lastItem()}} of {{$products->total()}} entries</span>
                            {{$products->links()}}
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
