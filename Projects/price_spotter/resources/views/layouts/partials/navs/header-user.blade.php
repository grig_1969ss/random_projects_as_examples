<nav class="navbar navbar-expand-lg navbar-transparent navbar-dark navbar-theme-dark mb-4">
    <div class="position-relative menu-wrapper">


        <div class="menu-wrapper_left">


            <a class="navbar-brand mr-lg-5" href="../../index.html"><img
                    class="navbar-brand-dark" src="{{asset('dist/img/brand/light.svg')}}" alt="menuimage"> <img
                    class="navbar-brand-light" src="{{asset('dist/img/brand/dark.svg')}}" alt="menuimage"></a>
            <div class="navbar-collapse collapse" id="navbar-dark-profile">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand"><a href="../../index.html"><img
                                    src="{{asset('dist/img/brand/dark.svg')}}"
                                    alt="menuimage"></a></div>
                        <div class="col-6 collapse-close"><i class="fas fa-times" data-toggle="collapse"
                                                             role="button"
                                                             data-target="#navbar-dark-profile"
                                                             aria-controls="navbar-dark-profile"
                                                             aria-expanded="false"
                                                             aria-label="Toggle navigation"></i></div>
                    </div>
                </div>
                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                    <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown" role="button"><i
                                class="fas fa-angle-down nav-link-arrow"></i> <span
                                class="nav-link-inner-text">Pages</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu"><a href="#"
                                                            class="dropdown-toggle dropdown-item d-flex justify-content-between align-items-center"
                                                            aria-haspopup="true" aria-expanded="false">About<i
                                        class="fas fa-angle-right nav-link-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../../html/pages/about-company.html" class="dropdown-item">About
                                            Company</a> <a href="../../html/pages/about-startup.html"
                                                           class="dropdown-item">About Startup</a> <a
                                            href="../../html/pages/about-team.html" class="dropdown-item">About
                                            Team</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a href="#"
                                                            class="dropdown-toggle dropdown-item d-flex justify-content-between align-items-center"
                                                            aria-haspopup="true" aria-expanded="false">Blog <i
                                        class="fas fa-angle-right nav-link-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../../html/pages/blog.html" class="dropdown-item">Blog</a> <a
                                            href="../../html/pages/single-article.html" class="dropdown-item">Single
                                            Article</a> <a href="../../html/pages/single-article-2.html"
                                                           class="dropdown-item">Single Article 2</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a href="#"
                                                            class="dropdown-toggle dropdown-item d-flex justify-content-between align-items-center"
                                                            aria-haspopup="true" aria-expanded="false">Posts <i
                                        class="fas fa-angle-right nav-link-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../../html/pages/posts.html" class="dropdown-item">Posts</a> <a
                                            href="../../html/pages/single-post.html" class="dropdown-item">Single
                                            Post</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a href="#"
                                                            class="dropdown-toggle dropdown-item d-flex justify-content-between align-items-center"
                                                            aria-haspopup="true" aria-expanded="false">Profile<i
                                        class="fas fa-angle-right nav-link-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../../html/pages/profile.html" class="dropdown-item">Profile</a> <a
                                            href="../../html/pages/profile-clean.html" class="dropdown-item">Profile
                                            Clean</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a href="#"
                                                            class="dropdown-toggle dropdown-item d-flex justify-content-between align-items-center"
                                                            aria-haspopup="true" aria-expanded="false">Specials <i
                                        class="fas fa-angle-right nav-link-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../../html/pages/signin.html" class="dropdown-item">Login</a> <a
                                            href="../../html/pages/signup.html" class="dropdown-item">Signup</a> <a
                                            href="../../html/pages/coming-soon.html" class="dropdown-item">Coming
                                            Soon</a><a href="../../html/pages/coming-soon-counter.html"
                                                       class="dropdown-item">Coming Soon 2</a> <a
                                            href="../../html/pages/maintenance.html"
                                            class="dropdown-item">Maintenance</a> <a
                                            href="../../html/pages/404.html"
                                            class="dropdown-item">404 Not
                                            Found</a></li>
                                </ul>
                            </li>
                            <li><a class="dropdown-item" href="../../html/pages/contact.html">Contact</a></li>
                            <li><a class="dropdown-item" href="../../html/pages/pricing.html">Pricing</a></li>
                            <li><a class="dropdown-item" href="../../html/pages/services.html">Services</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown" role="button"><i
                                class="fas fa-angle-down nav-link-arrow"></i> <span
                                class="nav-link-inner-text">Sections</span></a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="../../html/sections/about.html">About</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/blog.html">Blog</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/clients.html">Clients</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/contact.html">Contact</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/features.html">Features</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/pricing.html">Pricing</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/team.html">Team</a></li>
                            <li><a class="dropdown-item"
                                   href="../../html/sections/testimonials.html">Testimonials</a>
                            </li>
                            <li><a class="dropdown-item" href="../../html/sections/navbars.html">Navbars</a></li>
                            <li><a class="dropdown-item" href="../../html/sections/footers.html">Footers</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown mega-dropdown"><a href="#" class="nav-link" data-toggle="dropdown"
                                                                   role="button"><i
                                class="fas fa-angle-down nav-link-arrow"></i> <span class="nav-link-inner-text">Components</span></a>
                        <div class="dropdown-menu">
                            <div class="row">
                                <div class="col-lg-6 inside-bg d-none d-lg-block">
                                    <div
                                        class="bg-img d-flex align-items-end justify-content-center bg-tertiary text-white overflow-hidden"
                                        data-background="{{asset('dist/img/megamenu_bg.jpg')}}"
                                        style="background-image: url(&quot;{{asset('dist/img/megamenu_bg.jpg')}}&quot;);">
                                        <div class="z-2 pb-3"><a href="../../html/components/all.html"
                                                                 target="_blank"
                                                                 class="btn btn-primary btn-icon animate-up-2 mb-3 mb-sm-0 mr-2"><span
                                                    class="btn-inner-icon"><i class="fas fa-th-large"></i></span>
                                                <span
                                                    class="btn-inner-text">All components</span> </a><a
                                                href="../../docs/introduction.html"
                                                class="btn btn-white btn-icon animate-up-2 mb-3 mb-sm-0"><span
                                                    class="btn-inner-icon icon-tertiary"><i
                                                        class="fas fa-book"></i></span> <span
                                                    class="btn-inner-text text-tertiary">Documentation</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col pl-0">
                                    <ul class="list-style-none">
                                        <li><a class="dropdown-item" href="../../html/components/accordions.html">Accordions</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/alerts.html">Alerts</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/badges.html">Badges</a>
                                        </li>
                                        <li><a class="dropdown-item" href="../../html/components/blog-cards.html">Blog
                                                Cards</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/bootstrap-carousels.html">Bootstrap
                                                Carousels</a></li>
                                        <li><a class="dropdown-item" href="../../html/components/breadcrumbs.html">Breadcrumbs</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/buttons.html">Buttons</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/counters.html">Counters</a></li>
                                        <li><a class="dropdown-item" href="../../html/components/dropdowns.html">Dropdowns</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/forms.html">Forms</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col pl-0">
                                    <ul class="list-style-none">
                                        <li><a class="dropdown-item" href="../../html/components/icon-boxes.html">Icon
                                                Boxes</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/modals.html">Modals</a>
                                        </li>
                                        <li><a class="dropdown-item" href="../../html/components/navs.html">Navs</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/owl-carousels.html">Owl
                                                Carousels</a></li>
                                        <li><a class="dropdown-item" href="../../html/components/pagination.html">Pagination</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/popovers.html">Popovers</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/pricing-cards.html">Pricing
                                                Cards</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/profile-cards.html">Profile
                                                Cards</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/progress-bars.html">Progress
                                                Bars</a></li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/steps.html">Steps</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col pl-0">
                                    <ul class="list-style-none">
                                        <li><a class="dropdown-item"
                                               href="../../html/components/tables.html">Tables</a>
                                        </li>
                                        <li><a class="dropdown-item" href="../../html/components/tabs.html">Tabs</a>
                                        </li>
                                        <li><a class="dropdown-item" href="../../html/components/timelines.html">Timelines</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/tooltips.html">Tooltips</a></li>
                                        <li><a class="dropdown-item" href="../../html/components/typography.html">Typography</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="../../html/components/widgets.html">Widgets</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown" role="button"><i
                                class="fas fa-angle-down nav-link-arrow"></i> <span
                                class="nav-link-inner-text">Support</span></a>
                        <div class="dropdown-menu dropdown-menu-lg">
                            <div class="col-auto px-0" data-dropdown-content="">
                                <div class="list-group list-group-flush"><a href="../../docs/introduction.html"
                                                                            class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4"><span
                                            class="icon icon-sm icon-dark"><i class="fas fa-file-alt"></i></span>
                                        <div class="ml-4"><span class="text-dark d-block">Documentation</span> <span
                                                class="small">Examples and guides</span></div>
                                    </a><a href="https://themesberg.com/contact" target="_blank"
                                           class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4"><span
                                            class="icon icon-sm icon-dark"><i
                                                class="fas fa-microphone-alt"></i></span>
                                        <div class="ml-4"><span class="text-dark d-block">Support</span> <span
                                                class="small">Looking for answers? Ask us!</span></div>
                                    </a></div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>


        </div>
        <div class="d-flex align-items-center">
            @guest
                <a href="{{route('login')}}" class="btn mr-2 mb-2 btn-white" type="button">Login</a>
            @endguest

            @auth
                <div class="dropdown pl-1">
                    <div class="d-flex align-items-center" id="dropdownMenuButton" data-toggle="dropdown">
                        <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle mr-2"
                                type="button"
                                aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span></button>
                        <p class="font-small text-light m-0">{{auth()->user()->name}}</p></div>
                    <div class="dropdown-menu dropdown-menu-md" aria-labelledby="dropdownMenuButton"><h6
                            class="dropdown-header">Hi, {{auth()->user()->name}}</h6><a class="dropdown-item" href="#">My
                            profile</a> <a
                            class="dropdown-item" href="#">Dashboard</a> <a class="dropdown-item"
                                                                            href="#">Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="$('#logout').submit()"><i
                                class="fas fa-sign-out-alt mr-2"></i>Sign out</a></div>
                    <form id="logout" action="{{ route('logout') }}" method="POST" hidden>
                        @csrf
                        <button class="uik-btn__base uik-dropdown-item__wrapper" type="submit">
                            <span class="uik-btn__content">Logout</span>
                        </button>
                    </form>
                </div>
            @endauth

            <button class="navbar-toggler ml-2" type="button" data-toggle="collapse"
                    data-target="#navbar-dark-profile" aria-controls="navbar-dark-profile" aria-expanded="false"
                    aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</nav>
