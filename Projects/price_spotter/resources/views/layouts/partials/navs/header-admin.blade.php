<nav class="navbar navbar-expand-lg navbar-transparent navbar-dark navbar-theme-dark mb-4">
    <div class="container position-relative menu-wrapper">
        <a class="navbar-brand mr-lg-5" href="../../template.html">
            <img class="navbar-brand-dark" src="{{asset('img/villa-white.svg')}}" alt="Logo">
            <img class="navbar-brand-light" src="{{asset('img/villa-white.svg')}}" alt="menuimage">
        </a>
        <div class="navbar-collapse collapse" id="navbar-dark-profile">
            <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="../../template.html">
                            <img src="{{asset('img/villa.svg')}}" alt="menuimage">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <i class="fas fa-times" data-toggle="collapse" role="button" data-target="#navbar-dark-profile"
                           aria-controls="navbar-dark-profile" aria-expanded="false" aria-label="Toggle navigation"></i>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                <li class="nav-item dropdown">
                    <a href="{{route('categories.index')}}" class="nav-link" role="button">
                        <span class="nav-link-inner-text">Categories</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="{{route('products.index')}}" class="nav-link" role="button">
                        <span class="nav-link-inner-text">Products</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="{{route('providers.index')}}" class="nav-link" role="button">
                        <span class="nav-link-inner-text">Providers</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="{{route('provider-categories.index')}}" class="nav-link" role="button">
                        <span class="nav-link-inner-text">Provider Categories</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="{{route('provider-products.index')}}" class="nav-link" role="button">
                        <span class="nav-link-inner-text">Provider Products</span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="d-flex align-items-center">
            <div class="dropdown pl-1">
                <div class="d-flex align-items-center" id="dropdownMenuButton" data-toggle="dropdown"
                     aria-expanded="false">
                    <button class="btn btn-xs btn-circle btn-icon-only btn-soft dropdown-toggle mr-2" type="button"
                            aria-haspopup="true" aria-expanded="false">
                        <span class="fa fa-user"></span>
                    </button>
                    <p class="font-small text-light m-0">{{auth()->user()->name}}</p>
                </div>
                <div class="dropdown-menu dropdown-menu-md" aria-labelledby="dropdownMenuButton">
                    <h6 class="dropdown-header">{{env('APP_NAME')}}</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" onclick="$('#logout').submit()"><i class="fas fa-sign-out-alt mr-2"></i>Sign
                        out</a>
                </div>
                <form id="logout" action="{{ route('logout') }}" method="POST" hidden>
                    @csrf
                    <button type="submit">
                        <span >Logout</span>
                    </button>
                </form>
            </div>
            <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#navbar-dark-profile"
                    aria-controls="navbar-dark-profile" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</nav>
