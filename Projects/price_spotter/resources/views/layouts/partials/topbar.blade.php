<div class="uik-top-bar__wrapper">
    <button class="uik-btn__base uik-tutorials-top-bar__btnMenu" type="button">
              <span class="uik-btn__content">
                <div class="uik-nav-icon__wrapper">
                    <i class="fas fa-bars"></i>
                </div>
              </span>
    </button>
    <div class="uik-top-bar-section__wrapper section-1">
        <h2 class="uik-top-bar-title__wrapper">
            <img src="{{ asset('img/villa.svg') }}" class="pr-2">
            <a href="/">{{ config('app.name') }}</a>
        </h2>
        <div class="uik-nav-divider__wrapper"></div>
        <div class="uik-top-bar-link-container__wrapper">
            <a href="/" class="uik-top-bar-link__wrapper">Bookings</a>
            <a href="/" class="uik-top-bar-link__wrapper">Properties</a>
            <a href="/" class="uik-top-bar-link__wrapper">Schedule</a>
            <a href="/" class="uik-top-bar-link__wrapper">Inbox</a>
            <a href="/" class="uik-top-bar-link__wrapper">Issues</a>
            <a href="/" class="uik-top-bar-link__wrapper">Leads</a>
            {{--
            <a href="#" class="uik-top-bar-link__wrapper">Rates</a>
            <a href="#" class="uik-top-bar-link__wrapper">Channels</a>
            <a href="#" class="uik-top-bar-link__wrapper">Contacts</a>--}}
            <div class="uik-nav-divider__wrapper"></div>
        </div>
        <div class="uik-input__clear">
            <div class="uik-input__inputWrapper">
                <span class="uik-input__iconWrapper"><i class="uikon">search_left</i></span>
                <input type="text" class="uik-input__input" placeholder="Search...">
            </div>
        </div>
    </div>
    <div class="uik-top-bar-section__wrapper">
        <div class="alerts">
            <i class="far fa-comment uik-social-header__topBarIcon" data-badge="5"></i>
            <i class="fas fa-inbox uik-social-header__topBarIcon" data-badge="5"></i>
            <i class="far fa-bell uik-social-header__topBarIcon" data-badge="5"></i>
            <div class="uik-nav-divider__wrapper" style="margin: 0 0 0 30px"></div>
        </div>
        <div class="uik-social-header-user__wrapper">
            <div class="uik-avatar__wrapper">
                <div class="uik-avatar__avatarWrapper">
                    <img src="//gravatar.com/avatar/{{ md5(auth()->user()->email) }}?d=mm&s=100&size=70"
                         alt="{{ auth()->user()->name }}"
                         class="uik-avatar__avatar">
                </div>
            </div>
            <div class="uik-avatar__user-name">{{ auth()->user()->name }}</div>
            <i class="uikon uik-social-header-user__dropdownArrow">arrow_dropdown</i>
        </div>
    </div>
    <div class="uik-menuDrop__list" style="display: none">
        <a class="uik-btn__base uik-dropdown-item__wrapper" href="/social/profile">
            <span class="uik-btn__content">Profile</span>
        </a>
        <a class="uik-btn__base uik-dropdown-item__wrapper" href="{{route('account.settings')}}">
            <span class="uik-btn__content">Account Settings</span>
        </a>
        <form action="{{ route('logout') }}" method="POST">
            @csrf
            <button class="uik-btn__base uik-dropdown-item__wrapper" type="submit">
                <span class="uik-btn__content">Logout</span>
            </button>
        </form>
    </div>
</div>
<div class="uik-nav-panel__wrapper uik-tutorials-navigation__wrapper">
    <span class="uik-nav-title__wrapper">Menu</span>
    <section class="uik-nav-section__wrapper">
        <a href="/" class="uik-nav-link__wrapper">Bookings</a>
        <a href="/" class="uik-nav-link__wrapper">Schedule</a>
        <a href="/" class="uik-nav-link__wrapper">Inbox</a>
        <a href="/" class="uik-nav-link__wrapper">Issues</a>
        <a href="/" class="uik-nav-link__wrapper">Leads</a>
        <a href="#" class="uik-nav-link__wrapper">Search</a>
        <a class="uik-nav-link__wrapper">
            <span class="uik-nav-link__text">Messages</span><span class="uik-nav-link__rightEl">5</span>
        </a>
        <a class="uik-nav-link__wrapper">
            <span class="uik-nav-link__text">Inbox</span><span class="uik-nav-link__rightEl">5</span>
        </a>
        <a class="uik-nav-link__wrapper">
            <span class="uik-nav-link__text">Alerts</span><span class="uik-nav-link__rightEl">5</span>
        </a>
    </section>
</div>
