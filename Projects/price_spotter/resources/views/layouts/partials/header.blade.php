<header class="header-global">
    @role('admin')
       @include('layouts.partials/navs.header-admin')
    @else
        @include('layouts.partials/navs.header-user')
    @endrole

</header>
