<?php

namespace App\Providers;

use App\Services\Argos;
use App\Services\Curry;
use App\Services\ScrapeInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

//        $this->app->bind(ScrapeInterface::class, Curry::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
