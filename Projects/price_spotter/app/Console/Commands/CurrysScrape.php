<?php

namespace App\Console\Commands;

use App\Services\ScrapeInterface;
use Illuminate\Console\Command;

class CurrysScrape extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currys:scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Currys category items from https://www.currys.co.uk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ScrapeInterface $curry App\Services\ScrapeInterface
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\Www\HomeController')->scrape('currys');
    }
}
