<?php

namespace App\Console\Commands;

use App\Services\ScrapeInterface;
use Illuminate\Console\Command;

class Very extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'very:scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Very category items from https://www.very.co.uk/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ScrapeInterface $argos App\Services\ScrapeInterface
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\Www\HomeController')->scrape('very');
    }
}
