<?php

namespace App\Console\Commands;

use App\Services\ScrapeInterface;
use Illuminate\Console\Command;

class JohnLewis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'johnlewis:scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape JohnLewis category items from https://www.johnlewis.com/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ScrapeInterface $curry App\Services\ScrapeInterface
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\Www\HomeController')->scrape('johnlewis');
    }
}
