<?php

namespace App\Console\Commands;

use App\Services\ScrapeInterface;
use Illuminate\Console\Command;

class ArgosScrape extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'argos:scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Argos category items from https://www.argos.co.uk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ScrapeInterface $argos App\Services\ScrapeInterface
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\Www\HomeController')->scrape('argos');
    }
}
