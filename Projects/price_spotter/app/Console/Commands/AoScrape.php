<?php

namespace App\Console\Commands;

use App\Services\ScrapeInterface;
use Illuminate\Console\Command;

class AoScrape extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ao:scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Ao category items from https://ao.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ScrapeInterface $argos App\Services\ScrapeInterface
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\Www\HomeController')->scrape('ao');
    }
}
