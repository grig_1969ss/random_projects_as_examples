<?php

namespace App\Http\Controllers\Www;

use App\Http\Controllers\Controller as Controller;
use App\Models\Provider;
use App\Models\ProviderCategory;
use App\Services\Ao;
use App\Services\Apple;
use App\Services\Argos;
use App\Services\Curry;
use App\Services\JohnLewis;
use App\Services\ScrapeInterface;
use App\Services\Very;
use App\Traits\ScrapeHelper;
use vipnytt\SitemapParser;
use vipnytt\SitemapParser\Exceptions\SitemapParserException;

class HomeController extends Controller
{
    use ScrapeHelper;

    protected $argosXml = 'https://www.argos.co.uk/browse_sitemap.xml';
    protected $currysXml = 'https://www.currys.co.uk/segment.xml';
    protected $aoXml = 'https://ao.com/sitemaps/CategorySitemap.xml';
    protected $johnlewisXml = 'https://www.johnlewis.com/category.xml';
    protected $veryXml = 'https://www.very.co.uk/sitemap/very/navigation/navigation0.xml';
    protected $maxInsert = 500;

    public function home()
    {
        return redirect('/');
    }

    /**
     * @param $webSite
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function scrape($webSite)
    {
        if ($webSite === 'argos') {
            app()->bind(ScrapeInterface::class, Argos::class);
        } elseif ($webSite === 'currys') {
            app()->bind(ScrapeInterface::class, Curry::class);
        } elseif ($webSite === 'ao') {
            app()->bind(ScrapeInterface::class, Ao::class);
        } elseif ($webSite === 'johnlewis') {
            app()->bind(ScrapeInterface::class, JohnLewis::class);
        } elseif ($webSite === 'apple') {
            app()->bind(ScrapeInterface::class, Apple::class);
        } elseif ($webSite === 'very') {
            app()->bind(ScrapeInterface::class, Very::class);
        } else {
            exit('Unknown Website !');
        }

        $ScrapeInterface = app()->make(ScrapeInterface::class);

        $ScrapeInterface->scrape();
    }


    private function siteMapWithIssue($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 300); //timeout in seconds
        $data = curl_exec($curl);
        curl_close($curl);

        preg_match_all('/<loc ?.*>(.*)<\/loc>/', $data, $out);

        return $out[1];
    }

    /**
     * @param $webSite
     * @throws SitemapParserException
     * @throws SitemapParser\Exceptions\TransferException
     */
    public function pull($webSite)
    {

        if (in_array($webSite, ['argos', 'currys', 'ao', 'johnlewis', 'apple', 'very'])) {
            $xml = $webSite . 'Xml';
            $this->storeSiteMap($this->$xml, $webSite);
        } else {
            exit('Unknown Website !');
        }
    }

    /**
     * @param $xml
     * @param $provider
     * @throws SitemapParserException
     * @throws SitemapParser\Exceptions\TransferException
     */
    private function storeSiteMap($xml, $provider)
    {
        $provider = Provider::updateOrCreate(['name' => $provider]);
        $providerId = $provider->id;

        if ($provider->name == 'very') {
            $siteMaps = $this->siteMapWithIssue($xml);
        } else {
            $parser = new SitemapParser();
            $parser->parse($xml);
            $data = $parser->getURLs();
            $siteMaps = array_keys($data);
        }

        // insert only the ones which are not in table ( new ones in xml )
        $siteMaps = $this->getDiff($siteMaps, $provider);

        // if new ones are not found
        if (empty($siteMaps)) {
            echo 'New ones are not found!' . "\n" . '</br>';
            exit('done');
        }

        $providerCategories = [];
        //bulk insert
        foreach ($siteMaps as $key => $siteMap) {
            $providerCategory = ['url' => $siteMap, 'provider_id' => $providerId];

            $providerCategories[] = $providerCategory;

            if ($key % $this->maxInsert === 0 && $key !== 0) {
                echo "insert $this->maxInsert " . "\n" . '</br>';
                ProviderCategory::insert($providerCategories);
                $providerCategories = [];
            }
        }

        // insert the rest
        if (!empty($providerCategories)) {
            echo 'less ones: ' . count($providerCategories);
            echo "\n" . '</br>';
            ProviderCategory::insert($providerCategories);
        }

        exit('done');
    }

    /* Return only the ones that are not in table already
     * @param $siteMapsFromXml
     * @param $provider
     * @return array
     */
    private function getDiff($siteMapsFromXml, $provider)
    {
        try {
            $providerExistingCategories = Provider::where(['name' => $provider->name])->first()->categories->pluck('url')->toArray();

            $diff = array_diff($siteMapsFromXml, $providerExistingCategories);

            return $diff;
        } catch (SitemapParserException $e) {
            echo $e->getMessage();
        }
    }
}
