<?php

namespace App\Services;

use App\Models\Provider;
use App\Models\ProviderProduct;
use App\Traits\ScrapeHelper;
use Goutte\Client;

class Curry implements ScrapeInterface
{
    use ScrapeHelper;

    private $curryDomain = 'https://www.currys.co.uk';
    private $curryUri_1 = '/gbuk/tv-and-home-entertainment/televisions/televisions/301_3002_30002_xx_xx/';
    private $curryUri_2 = '_20/relevance-desc/xx-criteria.html';

    // to avoid infinity loop, stop the loop if the iteration is more than 100
    private $maxIterations = 100;

    /**
     * @return bool
     */
    public function scrape()
    {
        $client = new Client();

        $provider = Provider::where(['name' => 'currys'])->first();

        foreach ($provider->categories as $key => $category) {
            $page = 1;
            //set limit for tetsing
            if ($key > 20) {
                break;
            }

            do {
                $url = str_replace('xx-criteria.html', '', $category->url);

                // if no response
                if (! $crawler = $client->request('GET', $url)) {
                    return false;
                }

                $answer = $this->getUrl($client, $url, $page, null, $page.$this->curryUri_2, false);
                if (! $answer) {
                    return false;
                }

                // if there are no items
                if (! $products = $this->getItemsBoxInfo($answer['url'], $answer['crawler'], $client, $provider, $category->id)) {
                    break;
                }

                $page++;

                // check if pagination is over
            } while ($this->checkNotFound($answer['crawler'], 'currys') && $page < $this->maxIterations);

            dump('===================> end of each normal product category pages <===========');
        }

        echo "\n";
        exit('---------------DONE--------------------------');
    }

    /**
     * @param $categoryUrl
     * @param $crawler
     * @param $client
     * @param $provider
     * @param $parentId
     * @return mixed
     */
    private function getItemsBoxInfo($categoryUrl, $crawler, $client, $provider, $parentId)
    {
        $productClass = $this->productClass('currys');

        $products = $crawler->filter($productClass)->each(function ($node) use ($client, $provider, $parentId, $categoryUrl) {
            $product = [];

            $header = $node->filter('.productTitle');
            $product['title'] = $header->filter(' span[data-product="brand"]')->text().' '.$header->filter(' span[data-product="name"]')->text();
            $product['url'] = $node->filter(' .productTitle > a')->attr('href');

            if (! $crawlerSingle = $client->request('GET', $product['url'])) {
                return false;
            }

            $newProduct = ProviderProduct::create([
                'url' => $product['url'],
                'provider_id' => $provider->id,
                'parent_id' => $parentId,
            ]);

            $this->singleItemInfo($crawlerSingle, $newProduct, $categoryUrl);

            return $product;
        });

        return $products;
    }

    /**
     * @param $crawlerSingle
     * @param $newProduct
     * @param $categoryUrl
     */
    private function singleItemInfo($crawlerSingle, $newProduct, $categoryUrl)
    {
        $title = '';
        try {
            $productPage = $crawlerSingle->filter('.product-page');
            $titleBlock = $productPage->filter('.page-title');
            $title = $titleBlock->filter('span:nth-child(1)')->text().' '.$titleBlock->filter('span:nth-child(2)')->text();
            $price = $productPage->filter('strong[data-key=current-price]')->text();
            $productInfo = $productPage->filter('#product-info article')->html();
            $productSpecification = $productPage->filter('.productSheet')->html();
            $productCodeString = $productPage->filter('.prd-code')->text();
            preg_match('!\d+!', $productCodeString, $productCode);
            $productCode = $productCode[0];

            $reviewScoreString = $productPage->filter('.reevoo-score')->first()->attr('class');
            preg_match('!\d+!', $reviewScoreString, $reviewScore);
            $reviewScore = $reviewScore[0];
            $reviewCountString = $productPage->filter('.reevoo-badge-lk.anchor-lk')->first()->text();
            preg_match('!\d+!', $reviewCountString, $reviewCount);
            $reviewCount = $reviewCount[0];

            dump($title);
        } catch (\Exception $e) {
            dump($e->getMessage());

            $this->logException($title, $categoryUrl, $newProduct, $crawlerSingle, $e->getMessage());
        }
    }
}
