<?php

namespace App\Services;

use App\Models\Provider;
use App\Models\ProviderProduct;
use App\Traits\ScrapeHelper;
use Goutte\Client;

class Apple implements ScrapeInterface
{
    use ScrapeHelper;

    private $aoDomain = 'https://www.apple.com/uk/';
    private $pageSize = 12;

    // to avoid infinity loop, stop the loop if the iteration is more than 100
    private $maxIterations = 100;

    /**
     * @return bool
     */
    public function scrape()
    {
        $client = new Client();

        $provider = Provider::where(['name' => 'apple'])->first();

        foreach ($provider->categories as $key => $category) {
            $page = 0;

            //set limit for testing
            if ($key > 20) {
                break;
            }

            do {
                $url = $category->url;

                $answer = $this->getUrl($client, $url, $page, $this->pageSize);
                if (! $answer) {
                    return false;
                }

                // if there are no items
                if (! $products = $this->getItemsBoxInfo($answer['url'], $answer['crawler'], $client, $provider, $category->id)) {
                    break;
                }

                $page++;

                // check if pagination is over
            } while ($this->checkNotFound($answer['crawler'], 'apple') && $page < $this->maxIterations);

            dump('===================> end of each normal product category pages <===========');
        }
        echo "\n";
        exit('---------------DONE--------------------------');
    }

    /**
     * @param $categoryUrl
     * @param $crawler
     * @param $client
     * @param $provider
     * @param $parentId
     * @return mixed
     */
    private function getItemsBoxInfo($categoryUrl, $crawler, $client, $provider, $parentId)
    {
        $productClass = $this->productClass('apple');

        $products = $crawler->filter($productClass)->each(function ($node) use ($client, $provider, $parentId, $categoryUrl) {
            $product = [];

            $a = $node->filter('a');

            $title = $a->filter('span')->text();
            // delete line breaks
            $title = str_replace(["\r", "\n"], '', $title);

            $product['title'] = $title;
            $product['url'] = $this->aoDomain.$a->attr('href');

            if (! $crawlerSingle = $client->request('GET', $product['url'])) {
                return false;
            }

            $newProduct = ProviderProduct::create([
                'url' => $product['url'],
                'provider_id' => $provider->id,
                'parent_id' => $parentId,
            ]);

            $this->singleItemInfo($crawlerSingle, $newProduct, $categoryUrl);

            return $product;
        });

        return $products;
    }

    private function singleItemInfo($crawlerSingle, $newProduct, $categoryUrl)
    {
        $title = '';
        try {
            $title = $crawlerSingle->filter('h1')->text();
             $brand = $crawlerSingle->filter('div[itemprop="brand"]')->text();
             $sku = $crawlerSingle->filter('.addToBasket.btn-add-to-basket.notInBasket')->attr('data-productcode');
             $price = $crawlerSingle->filter('span[itemprop=price]')->text();
             $score = $crawlerSingle->filter('a[class=score]')->text();
             $reviewCount = $crawlerSingle->filter('span[itemprop=reviewCount]')->text();
             $score = substr($score, 0, strpos($score, '/'));

            dump($title);
        } catch (\Exception $e) {
            dump($e->getMessage());

            $this->logException($title, $categoryUrl, $newProduct, $crawlerSingle, $e->getMessage());
        }
    }
}
