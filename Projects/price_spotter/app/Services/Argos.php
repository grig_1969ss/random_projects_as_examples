<?php

namespace App\Services;

use App\Models\Provider;
use App\Models\ProviderCategory;
use App\Models\ProviderProduct;
use App\Traits\ScrapeHelper;
use Goutte\Client;
use Illuminate\Support\Arr;

class Argos implements ScrapeInterface
{
    use ScrapeHelper;

    private $argosDomain = 'https://www.argos.co.uk';

    // to avoid infinity loop, stop the loop if the iteration is more than 100
    private $maxIterations = 100;

    /**
     * @return bool
     */
    public function scrape()
    {
        $client = new Client();

        $provider = Provider::where(['name' => 'argos'])->first();

        foreach ($provider->categories as $key => $category) {
            $page = 1;
            //set limit for testing
            if ($key > 20) {
                break;
            }

            do {
                $url = $category->url;

                $answer = $this->getUrl($client, $url, $page, null, 'opt/page:');
                if (! $answer) {
                    return false;
                }

                // if there are no items
                if (! $products = $this->getItemsBoxInfo($answer['url'], $answer['crawler'], $client, $provider, $category->id)) {
                    break;
                }

                $page++;

                // check if pagination is over
            } while ($this->checkNotFound($answer['crawler'], 'argos') && $page < $this->maxIterations);
        }

        echo "\n";
        exit('---------------DONE--------------------------');
    }

    /**
     * @param $categoryUrl
     * @param $crawler
     * @param $client
     * @param $provider
     * @param $parentId
     * @return mixed
     */
    private function getItemsBoxInfo($categoryUrl, $crawler, $client, $provider, $parentId)
    {
        $productClass = $this->productClass('argos');

        $products = $crawler->filter($productClass)->each(function ($node) use ($client, $provider, $parentId, $categoryUrl) {
            $product = [];

            $product['title'] = $node->filter('a[data-test^="component-product-card-title"]')->text();
            $product['url'] = $this->argosDomain.$node->filter('a[data-test="component-product-card-title"]')->attr('href');

            if (! $crawlerSingle = $client->request('GET', $product['url'])) {
                return false;
            }

            $newProduct = ProviderProduct::create([
                'url' => $product['url'],
                'provider_id' => $provider->id,
                'parent_id' => $parentId,
            ]);

            $this->singleItemInfo($crawlerSingle, $newProduct, $categoryUrl);

            return $product;
        });

        return $products;
    }

    private function singleItemInfo($crawlerSingle, $newProduct, $categoryUrl)
    {
        $title = '';
        try {
            $block = $crawlerSingle->filter('.xs-block');
            $title = $block->filter('.h2.product-name-main span[data-test="product-title"]')->text();
            $sku = $block->filter('.h2.product-name-main span[itemprop="sku"]')->text();
             $price = $block->filter('.price.product-price-primary h2')->text();

             $isbnString = $crawlerSingle->filter('.product-description-content-text ul')->last()->filter('li')->last()->text();
             preg_match('!\d+!', $isbnString, $matches);
             $isbn = $matches[0];

             $reviewCount = $block->filter('span[itemprop=ratingCount]')->text();
             $reviewCode = $crawlerSingle->filter('.trustmark-recommend-wrap p')->text();

             $descriptionBlock = $crawlerSingle->filter('#pdp-description')->html();
             $attributesHtml = $crawlerSingle->filter('.pdp-comparison-attributes')->html();

            dump($title);
        } catch (\Exception $e) {
            dump($e->getMessage());

            $this->logException($title, $categoryUrl, $newProduct, $crawlerSingle, $e->getMessage());
        }
    }
}
