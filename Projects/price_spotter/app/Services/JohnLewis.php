<?php

namespace App\Services;

use App\Models\Provider;
use App\Models\ProviderProduct;
use App\Traits\ScrapeHelper;
use Goutte\Client;
use PHPHtmlParser\Dom;

class JohnLewis implements ScrapeInterface
{
    use ScrapeHelper;

    private $johnLewisDomain = 'https://www.johnlewis.com/';
    private $phantomJsPath = 'public/js/phantomjs';
    private $phantomJsTempPath = 'johnlewis.html';

    // to avoid infinity loop, stop the loop if the iteration is more than 100
    private $maxIterations = 100;

    /**
     * @return bool
     */
    public function scrape()
    {
        $client = new Client();

        $provider = Provider::where(['name' => 'johnlewis'])->first();

        foreach ($provider->categories as $key => $category) {
            //set limit for testing
            if ($key > 20) {
                break;
            }

            $categoryGroup = $category->url;

            // check if url has left side menu panel, and get the first one (because the second seems just advices)
            if (!$crawler = $client->request('GET', $categoryGroup)) {
                return false;
            }

            if ($crawler->filter('.area-categories-nav__section')->count() < 1) {
                dump('------- not-found -------');
                continue;
            }

            $crawler->filter('.area-categories-nav__section')->first()->filter(' ul li')->each(function ($node) use ($client, $provider, $category) {
                $page = 1;
                do {
                    $url = $this->johnLewisDomain . $node->filter('a')->attr('href');

                    $answer = $this->getUrl($client, $url, $page);
                    if (!$answer) {
                        return false;
                    }

                    // if there are no items
                    if (!$products = $this->getItemsBoxInfo($answer['url'], $answer['crawler'], $client, $provider, $category->id)) {
                        break;
                    }

                    $page++;

                    // check if pagination is over
                } while ($this->checkNotFound($answer['crawler'], 'johnLewis') && $page < $this->maxIterations);

                dump('===================> end of each normal product category pages <===========');
            });
        }

        echo "\n";
        exit('---------------DONE--------------------------');
    }

    /**
     * @param $categoryUrl
     * @param $crawler
     * @param $client
     * @param $provider
     * @param $parentId
     * @return mixed
     */
    private function getItemsBoxInfo($categoryUrl, $crawler, $client, $provider, $parentId)
    {
        $productClass = $this->productClass('johnLewis');

        $products = $crawler->filter($productClass)->each(function ($node) use ($client, $provider, $parentId, $categoryUrl) {
            $product = [];

            $product['title'] = $node->filter('.product-card__title-inner')->text();
            $product['url'] = $this->johnLewisDomain . $node->filter('.product-card__wrap-link')->attr('href');

            if (!$crawlerSingle = $client->request('GET', $product['url'])) {
                return false;
            }

            $newProduct = ProviderProduct::create([
                'url' => $product['url'],
                'provider_id' => $provider->id,
                'parent_id' => $parentId,
            ]);

            $this->singleItemInfo($crawlerSingle, $newProduct, $categoryUrl);

            return $product;
        });

        return $products;
    }

    /**
     * @param $crawlerSingle
     * @param $newProduct
     * @param $categoryUrl
     */
    private function singleItemInfo($crawlerSingle, $newProduct, $categoryUrl)
    {
        $title = '';
        try {
            // for title there are different classes
            $title = $crawlerSingle->filter('h1[class*="title"]')->text();
            $price = $crawlerSingle->filter('.u-centred.price')->text();

            $productCodeString = $crawlerSingle->filter('.product-code')->text();
            preg_match('!\d+!', $productCodeString, $matches);
            $productCode = $matches[0];

            $description = $crawlerSingle->filter('.product-detail.product-detail--info > div')->html();
            $descriptionSpecification = $crawlerSingle->filter('.product-detail.product-detail--specifications > div')->html();


            $reviewBlock = $this->getReview($newProduct);
            $reviewCount = $reviewBlock['reviewCount'];
            $score = $reviewBlock['score'];

            dump($title);
        } catch (\Exception $e) {
            $this->logException($title, $categoryUrl, $newProduct, $crawlerSingle, $e->getMessage());
        }
    }

    private function getReview($newProduct)
    {
        /*
         * PhantomJs are used to get sections which are appended by javascript
         * And can not reached by php
         * For this service (website) it is review block
         *
         * Note! we need to have PhantomJs installed in OS
         * See the commands http://bologer.ru/kak-ustanovitobnovit-phantomjs-v-ubuntu-14-04/
        */

        // PhantomJS will wait for page loading as much as the timeout is set in phantom.js
        // this will make additional request
        $page = shell_exec("phantomjs {$this->phantomJsPath}/phantom.js {$newProduct->url} {$this->phantomJsTempPath}");

        $dom = new Dom();
        $dom->load($page);
        $reviewBlock = $dom->find('.BVRRRatingSummary.BVRRPrimarySummary.BVRRPrimaryRatingSummary');

        $scoreString = $reviewBlock->find('.BVImgOrSprite')->getAttribute('title');
        $scoreArray = explode(' ', $scoreString, 2);
        $score = $scoreArray[0];

        $reviewCount = $reviewBlock->find('.BVRRCount.BVRRNonZeroCount .BVRRNumber')->text;

        return ['score' => $score, 'reviewCount' => $reviewCount];
    }
}
