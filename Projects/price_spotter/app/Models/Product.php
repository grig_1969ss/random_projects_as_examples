<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'archived', 'category_id', 'archived', 'checked_at', 'checked_by', 'created_by', 'updated_by',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'checked_by');
    }
}
