<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Provider extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'archived',
    ];

    /**
     * Get the categories for the Provider.
     */
    public function categories()
    {
        return $this->hasMany('App\Models\ProviderCategory');
    }
}
