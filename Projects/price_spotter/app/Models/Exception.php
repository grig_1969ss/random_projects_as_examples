<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'custom_message', 'html_body',
    ];

    /**
     * Get the owning exceptionable model.
     */
    public function exceptionable()
    {
        return $this->morphTo();
    }
}
