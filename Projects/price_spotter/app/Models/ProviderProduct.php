<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProviderProduct extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'reference', 'url', 'provider_id', 'parent_id', 'product_id', 'archived', 'checked_at', 'checked_by',
        'created_by', 'updated_by',
    ];

    /**
     * Get the Provider that owns the provider_category.
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'checked_by');
    }

    /**
     * Get all of the product's exceptions.
     */
    public function exceptions()
    {
        return $this->morphMany('App\Models\Exception', 'exceptionable');
    }
}
