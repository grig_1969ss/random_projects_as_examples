<?php

namespace App\Traits;

use App\Models\ProviderProduct;

trait ScrapeHelper
{
    private $argos = '.ProductCardstyles__ContentBlock-l8f8q8-4';
    private $currys = '.resultList .result-prd.productCompare';
    private $ao = '.product-card.product-card--list-card.productInfoArea .listerPodtitle';
    private $johnLewis = '.product-card';
    private $apple = '';
    private $very = '.product';

    /** For each Provider(website) we will pass product box class, and will check if it is > 0 or not
     * @param $crawler
     * @param $website
     * @return bool
     */
    public function checkNotFound($crawler, $website)
    {
        if ($crawler->filter($this->$website)->count()) {
            return true;
        }

        return false;
    }

    /**
     * @param $website
     * @return mixed
     */
    public function productClass($website)
    {
        return $this->$website;
    }

    public function logException($title, $categoryUrl, $newProduct, $crawlerSingle, $message)
    {
        dump($message);

        $errorObject = new \StdClass();
        $errorObject->title = $title;
        $errorObject->url = $newProduct->url;
        $errorObject->category_url = $categoryUrl;
        $errorObject->error_message = $message;

        ProviderProduct::find($newProduct->id)->exceptions()->create([
            'custom_message' => json_encode($errorObject),
            'html_body' => $crawlerSingle->html(),
        ]);
    }

    public function getUrl($client, $url, $page, $pageSize = null, $pageParamConcat = '?page=', $pageParamAtLast = true)
    {
        dump($url);
        // url from xml

        if (!$crawler = $client->request('GET', $url)) {
            return false;
        }


        dd('here');

        if ($pageSize) {
            $page = $page * $pageSize;
        }

        // actual url (if redirected)
        // e.g. ?page=1 or /page:1
        $url = $crawler->getUri() . $pageParamConcat . ($pageParamAtLast ? $page : '');

        dump("$url ===> Page: $page");

        // make request again with actual url
        if (!$crawler = $client->request('GET', $url)) {
            return false;
        }

        return ['crawler' => $crawler, 'url' => $url];
    }
}
