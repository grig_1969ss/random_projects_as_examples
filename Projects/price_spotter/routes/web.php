<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use vipnytt\SitemapParser;
use vipnytt\SitemapParser\Exceptions\SitemapParserException;

Auth::routes();

Route::get('/scrape/{website}', 'Www\HomeController@scrape');
Route::get('/pull/{website}', 'Www\HomeController@pull');

Route::middleware(['auth', 'role:admin'])->namespace('Admin')->domain('admin.'.env('APP_HOST', 'price-spotter.loc'))->group(function () {
    Route::get('/', 'HomeController@home');
    Route::resource('/providers', 'ProviderController');
    Route::resource('/provider-categories', 'ProviderCategoryController');
    Route::resource('/provider-products', 'ProviderProductController');

    Route::resource('/products', 'ProductsController');
    Route::resource('/categories', 'CategoriesController');
});

Route::get('/', 'HomeController@index');

Route::middleware(['auth', 'role:user'])->namespace('Www')->domain('www.'.env('APP_HOST', 'price-spotter.loc'))->group(function () {
    Route::get('/', 'HomeController@home');
});

Route::middleware(['auth', 'role:user'])->group(function () {
    Route::get('/', 'HomeController@home');
});

Route::get('/', 'HomeController@index');

