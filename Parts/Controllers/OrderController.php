<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\OrderCallRequest;
use App\Http\Requests\Order\OrderCreateRequest;
use App\Interfaces\OrderInterface;
use Illuminate\Http\Request;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController
{
    /**
     * @var
     */
    protected $orderService;

    /**
     * OrderController constructor.
     *
     * @param OrderInterface $orderInterface
     */
    public function __construct(OrderInterface $orderInterface)
    {
        $this->orderService = $orderInterface;
    }

    /**
     * @param OrderCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeOrder(OrderCreateRequest $request)
    {
        $order = $this->orderService->makeOrder($request->all());
        return response()->json($order);
    }

    /**
     * @param OrderCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeSingleOrder(OrderCreateRequest $request)
    {
        $order = $this->orderService->makeSingleOrder($request->all());
        return response()->json($order);
    }

    /**
     * @param OrderCallRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderCall(OrderCallRequest $request)
    {
       $response = $this->orderService->orderCall($request->all());

       return response()->json($response);
    }

}
