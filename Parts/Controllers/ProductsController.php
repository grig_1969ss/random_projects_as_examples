<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\OrderCreateRequest;
use App\Interfaces\ProductInterface;

use App\Models\Image;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

/**
 * Class ProductsController
 * @package App\Http\Controllers
 */
class ProductsController
{
    /**
     * @var
     */
    public $productService;

    /**
     * ProductsController constructor.
     * @param ProductInterface $productService
     */
    public function __construct(ProductInterface $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
      $res =  $this->productService->index($request->all());
      return response()->json($res,500);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sheetCategory(Request $request)
    {
        $res =  $this->productService->parseImages();
        return response()->json($res,500);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts(Request $request, $categoryName = null)
    {
        $products = $this->productService->get($request->all(), $categoryName);

        return response()->json($products);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGroupProduct(Request $request)
    {
       $response = $this->productService->getSingle($request->all());
        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFilters(Request $request)
    {
        $filters = $this->productService->getFilters($request->all());
        return response()->json($filters);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ideaProducts(Request $request)
    {
        $response = $this->productService->ideaProducts($request->all());
        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $slug)
    {
        $response = $this->productService->show($request->all(), $slug);
        return response()->json($response);
    }

    /**
     * @return mixed
     */
    public function exportCrm()
    {
        $response = $this->productService->exportCrm();
//            dd($response);
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function offers(Request $request)
    {
        $response = $this->productService->offers($request->all());

        return response()->json($response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showRoom()
    {
        $images = json_decode(file_get_contents(database_path('seeds/sliderImages.json'),true))[1];
        $images =  explode(', ', trim($images)) ;
        $images = preg_filter('/^/', 'categories/', $images);
        return response()->json($images);
    }
}
