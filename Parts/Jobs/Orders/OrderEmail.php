<?php

namespace App\Jobs\Orders;

use App\Mail\Order\OrderSend;
use App\Traits\ProductTransformer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use function foo\func;

class OrderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ProductTransformer;
    /**
     * @var
     */
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(Arr::get($this->data,'products')) {
             $data = $this->transform($this->data['products']);
             $this->data['products'] = $data;
        }

        if(Arr::get($this->data,'cart')) {
            $cart = $this->transform($this->data['cart']);
            $this->data['cart'] = $cart;
        }

        Mail::to(config('mail.to.email'))->send(new OrderSend($this->data));
    }
}
