import translator from '../../../mixins/translator';

export default {
    mixins: [
        translator
    ],
    props: [
        'activated'
    ],
    data() {
        return {
            form: {},
            errors: {},
            loading: false,
            owners: [],
            roomTypes: [],
            // properties: [],
            frequencies: [],
            // selectedProperties: [],
        }
    },
    methods: {
        getOwners() {
            this.axios.get(route('clusters.get.owners')).then((response) => {
                this.owners = response.data;
            }).catch((error) => {
            });
        },
        getRoomTypes() {
            this.axios.get(route('clusters.get.room_types')).then((response) => {
                this.roomTypes = response.data;
            }).catch((error) => {
            });
        },
        getProperties() {
            this.axios.get(route('clusters.get.properties')).then((response) => {
                this.properties = response.data;
            }).catch((error) => {
            });
        },
        getFrequencies() {
            this.axios.get(route('clusters.get.frequencies')).then((response) => {
                this.frequencies = response.data;
            }).catch((error) => {
            });
        },
        reset() {
            this.resetScope();
            this.resetErrors();
        },
        resetScope() {
            this.form = {};
            this.loading = false;
            this.selectedProperties = [];
        },
        resetErrors() {
            this.errors = {};
        },
        cancel() {
            this.reset();
            this.$emit('cancel');
        },
        save() {
            this.loading = true;
            this.resetErrors();

            this.axios.post(this.saveRoute, this.form).then((response) => {
                this.reset();
                this.$emit('success', response.data);
            }).catch((error) => {
                this.loading = false;

                if (error.response.status === 422) {
                    this.errors = error.response.data.errors;
                }
            });
        }
    },
    watch: {
        activated(val) {
            if (!val) {
                return false;
            }

            this.getOwners();
            this.getRoomTypes();
            this.getFrequencies();
        },

    }
}