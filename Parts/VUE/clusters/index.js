require('../../bootstrap');

import 'vuetify/dist/vuetify.min.css';

import Vue from 'vue';
import Vuetify from 'vuetify';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Multiselect from 'vue-multiselect';
import MainService from '../../services/main';
import ConfigService from '../../services/config';
import InterceptorService from '../../services/interceptor';
import ClustersList from './ClustersList';

Vue.component('multiselect', Multiselect);

Vue.use(Vuetify);
Vue.use(VueAxios, axios);

ConfigService.init();
InterceptorService.init();

MainService.init().then(() => {
    new Vue({
        el: '#clusters',
        template: '<ClustersList/>',
        components: { ClustersList }
    });
});