import { mapState } from "vuex";

import formable from "@/mixins/formable";

import {
  GET_COMPANIES_LIST
} from "@/store/companies/actions";

import {
  GET_TAGS_LIST
} from "@/store/tags/actions";

import VueTagsInput from "@johmun/vue-tags-input";

export default {
  mixins: [
    formable
  ],
  components: {
    VueTagsInput
  },
  data() {
    return {
      tag: "",
      form: {
        type: 1
      },
      activeTab: "tab-main"
    };
  },
  beforeMount() {
    this.$store.dispatch(GET_COMPANIES_LIST);
    this.$store.dispatch(GET_TAGS_LIST);
  },
  methods: {
    resetPrivate() {
      if (!this.item) {
        return false;
      }

      this.form.first_name = this.item.first_name;
      this.form.last_name = this.item.last_name;
    },
    resetCompany() {
      if (!this.item) {
        return false;
      }

      this.form.SDI = this.item.SDI;
      this.form.taxcode = this.item.taxcode;
      this.form.vatcode = this.item.vatcode;
    }
  },
  computed: {
    ...mapState({
      tags: state => state.tags.getListRequest.list,
      companies: state => state.companies.getListRequest.list
    }),
    initialized() {
      return !!this.companies.length;
    },
    filteredItems() {
      return this.tags.filter(i => {
        return i.text.toLowerCase().indexOf(this.tag.toLowerCase()) !== -1;
      });
    }
  },
  watch: {
    "form.type": {
      handler(val) {
        if (val === 1) {
          this.resetCompany();
        }
        if (val === 2) {
          this.resetPrivate();
        }
      },
      deep: true
    }
  }
};
