<?php

namespace App\Traits;

use Illuminate\Support\Arr;

trait ProductTransformer
{

    /**
     * @param $products
     * @return array
     */
    protected function transform($products)
    {
        $transformed = [];
        $fields = $this->productFields();
        foreach ($products as $product) {
            $transformed[] = $this->transformKeys(Arr::only($product, $fields));
        }

        return $transformed;
    }

    /**
     * @param $data
     * @return array
     */
    protected function transformKeys($data)
    {
        $transformed = [];

        foreach ($data as $key => $val) {
            $newkey = ucwords(str_replace('_', ' ', $key));
            $transformed[$newkey] = $val;
        }

        return $transformed;
    }

    /**
     * @return array
     */
    protected function productFields() {
        return [
            'name',
            'price',
            'static_param',
            'dynamic_param',
            'image',
            'count'
        ];
    }
}
