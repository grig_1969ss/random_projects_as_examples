<?php

namespace App\Traits;

use Illuminate\Support\Arr;

trait ProductServiceTrait
{


    /**
     * @param $products
     * @return array
     */
    protected function getAditionals($products)
    {
        $aditionals = [];

        foreach ($products as $product) {
            $aditionals[] = $product['additional_products'] ? explode(',', $product['additional_products']) : [];
        }
        $ids = array_unique(Arr::flatten($aditionals));

        return $ids;
    }

    /**
     * @param $data
     * @return array
     */
    protected function getOffers($data)
    {
        $productIds = $this->model
            ->select('id','additional_products')
            ->whereIn('id', $data['productIds'])
            ->get()
            ->toArray();

        $ids = $this->getAditionals($productIds);
        return $ids;
    }

}
