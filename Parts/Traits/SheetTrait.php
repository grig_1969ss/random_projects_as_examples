<?php

namespace App\Traits;

use Sheets;

trait SheetTrait
{
    /**
     * @return mixed
     */
    protected function getSheet()
    {
      return Sheets::spreadsheet(config('google.productSheetId'))
          ->sheet(config('google.productSheetName'));
    }

    /**
     * @return mixed
     */
    protected function getImageSheet()
    {
        return Sheets::spreadsheet(config('google.imageSheetId'))
            ->sheet(config('google.imageSheetName'));
    }

    /**
     * @param $product
     */
    protected function removeProductFromStore($product)
    {
        $id = $product['id'] + 1;

        $this->getSheet()
            ->range('O'.$id)
            ->update([[$product['in_store'] ?? '']]);
    }

}
