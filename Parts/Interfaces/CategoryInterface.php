<?php

namespace App\Interfaces;

interface CategoryInterface
{

    /**
     * @param $data
     * @return mixed
     */
    public function get($data);

}
