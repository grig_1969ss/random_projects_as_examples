<?php


namespace App\Interfaces;


interface ProductInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function index($data);

    /**
     * @param $data
     * @return mixed
     */
    public function get($data, $categoryName= null);

    /**
     * @param $data
     * @return mixed
     */
    public function getSingle($data);

    /**
     * @param $data
     * @return mixed
     */
    public function makeOrder($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getFilters($data);

    /**
     * @param $data
     * @return mixed
     */
    public function ideaProducts($data);

    /**
     * @param $data
     * @return mixed
     */
    public function show($data, $slug);

    /**
     * @return mixed
     */
    public function offers($data);



}
