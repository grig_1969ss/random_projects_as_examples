<?php
namespace App\Interfaces;


interface OrderInterface
{

    /**
     * @param $data
     * @return mixed
     */
    public function makeOrder($data);

    /**
     * @param $data
     * @return mixed
     */
    public function makeSingleOrder($data);

    /**
     * @param $data
     * @return mixed
     */
    public function orderCall($data);

}
