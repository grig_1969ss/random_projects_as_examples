<?php

namespace App\Classes\Scripts;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\Scripts\ScriptException;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\v201809\cm\{Selector, DateRange, ReportDefinitionReportType};
use Google\AdsApi\AdWords\Reporting\v201809\{ReportDefinition, ReportDownloader, DownloadFormat, ReportDefinitionDateRangeType};

class AdgroupPerformanceReport extends Script
{
    /**
     * @var string
     */
    private $reportFile;

    /**
     * @var array
     */
    private $columnsMapping = [
        'campaign_id'             => 'Campaign ID',
        'adgroup_id'              => 'Ad group ID',
        'label'                   => 'Labels',
        'clicks'                  => 'Clicks',
        'impressions'             => 'Impressions',
        'cost'                    => 'Cost',
        'conversion_value'        => 'Total conv. value',
        'conversions'             => 'Conversions',
        'avg_position'            => 'Avg. position',
        'search_impression_share' => 'Search Impr. share',
        'date'                    => 'Day'
    ];

    /**
     * @var array
     */
    private $lookupColumnsMapping = [
        'campaign_id'   => 'Campaign ID',
        'campaign_name' => 'Campaign',
        'adgroup_id'    => 'Ad group ID',
        'adgroup_name'  => 'Ad group'
    ];

    public function pull()
    {
        $for30Days = ($this->getDaysRange() == 'true') ? true : false;

        $this->printMessage("Currently running script '{$this->getName()}'");

        $script     = DB::table('scripts_details')->where('script_name', $this->getName())->first();
        $scriptId   = $script->id;
        $daysToPull = $for30Days ? 30 : $script->days_to_pull;
        $logId      = $this->log(null, $scriptId);

        $this->reportFile = "{$this->tmpDir}/AdgroupPerformanceReport_{$this->getAccountId()}_" . date('Y_m_d') . '.csv';

        $start = date('Y-m-d', strtotime("-{$daysToPull} days"));
        $end   = date('Y-m-d', strtotime('-1 day'));
        $this->printMessage("Data being collected for account {$this->getAccountId()} for the period {$start} - {$end}.");

        try {
            if (file_exists($this->reportFile)) {
                $this->import();
                $this->log($logId, $scriptId);
                $this->printMessage("Job has been finished for account {$this->getAccountId()}.");

                return true;
            }

            $session = $this->auth();

            $selector = (new Selector)
                ->setDateRange(new DateRange(date('Ymd', strtotime("-{$daysToPull} days")), date('Ymd', strtotime('-1 day'))))
                ->setFields([
                    'CampaignId',
                    'CampaignName',
                    'Labels',
                    'AdGroupId',
                    'AdGroupName',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'Conversions',
                    'ConversionValue',
                    'AveragePosition',
                    'SearchImpressionShare',
                    'Date'
                ]);

            $reportDefinition = new ReportDefinition;
            $reportDefinition->setReportName('Adgroup Performance report #' . uniqid());
            $reportDefinition->setDateRangeType(ReportDefinitionDateRangeType::CUSTOM_DATE);
            $reportDefinition->setReportType(ReportDefinitionReportType::ADGROUP_PERFORMANCE_REPORT);
            $reportDefinition->setDownloadFormat(DownloadFormat::CSV);
            $reportDefinition->setSelector($selector);

            $reportDownloader = new ReportDownloader($session);
            $reportSettingsOverride = (new ReportSettingsBuilder)
                ->includeZeroImpressions(false)
                ->build();

            $reportDownloadResult = $reportDownloader->downloadReport($reportDefinition, $reportSettingsOverride);
            $reportDownloadResult->saveToFile($this->reportFile);

            if (!file_exists($this->reportFile)) {
                throw new ScriptException("File '{$this->reportFile}' does not exist.");
            }

            $this->import();
            $this->log($logId, $scriptId);
            $this->printMessage("Job has been finished for account {$this->getAccountId()}.");

        } catch (Exception $e) {
            $this->log($logId ?? null, $scriptId, $e->getMessage());

            $this->printMessage("ERROR: {$e->getMessage()}");

            Log::error($e);
        }
    }

    private function import()
    {
        if (($handle = fopen($this->reportFile, 'r')) === FALSE) {
            throw new ScriptException("Unable to open file '{$this->reportFile}'.");
        }

        $i             = 1;
        $headers       = [];
        $insert        = [];
        $insertLookups = [];
        $now           = Carbon::now();
        $update        = array_merge(['updated_at', 'sis_imps'], array_keys($this->columnsMapping));
        $updateLookups = array_merge(['updated_at'], array_keys($this->lookupColumnsMapping));

        while (($row = fgetcsv($handle)) !== FALSE) {
            if ($row[0] === 'Total') {
                break;
            }

            if ($i <= 2) {
                if ($i === 2) {
                    $headers = array_flip($row);
                }

                ++$i;

                continue;
            }

            // Insert main report data.
            $insert[$i] = [
                'client_id'  => $this->getClientId(),
                'account_id' => $this->getAccountId(),
                'created_at' => $now,
                'updated_at' => $now
            ];

            foreach ($this->columnsMapping as $dbColumn => $fileColumn) {
                $insert[$i][$dbColumn] = $row[$headers[$fileColumn]];
            }

            if ($insert[$i]['search_impression_share'] === '< 10%') {
                $insert[$i]['sis_imps'] = $insert[$i]['impressions'] / (5 / 100);
            } elseif ($insert[$i]['search_impression_share'] == 0 || $insert[$i]['search_impression_share'] == '--') {
                $insert[$i]['sis_imps'] = 0;
            } else {
                $insert[$i]['sis_imps'] = $insert[$i]['impressions'] / ((float)$insert[$i]['search_impression_share'] / 100);
            }

            if (!empty($insert) && (($i % self::MAX_ARRAY_PER_INSERT) === 0)) {
                self::insertOnDuplicateUpdate('reports_adgroup_performance', $insert, $update);
                $insert = [];
            }

            // Insert lookup data.
            $insertLookups[$i] = [
                'client_id'  => $this->getClientId(),
                'account_id' => $this->getAccountId(),
                'created_at' => $now,
                'updated_at' => $now
            ];

            foreach ($this->lookupColumnsMapping as $dbColumn => $fileColumn) {
                $insertLookups[$i][$dbColumn] = $row[$headers[$fileColumn]];
            }

            if (!empty($insertLookups) && (($i % self::MAX_ARRAY_PER_INSERT) === 0)) {
                self::insertOnDuplicateUpdate('adgroup_data_lookups', $insertLookups, $updateLookups);
                $insertLookups = [];
            }

            ++$i;
        }

        if (!empty($insert)) {
            self::insertOnDuplicateUpdate('reports_adgroup_performance', $insert, $update);
            $insert = [];
        }

        if (!empty($insertLookups)) {
            self::insertOnDuplicateUpdate('adgroup_data_lookups', $insertLookups, $updateLookups);
            $insertLookups = [];
        }

        fclose($handle);
    }
}
