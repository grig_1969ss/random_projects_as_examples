<?php

namespace App\Classes\Scripts;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\Scripts\ScriptException;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\v201809\cm\{Selector, DateRange, ReportDefinitionReportType};
use Google\AdsApi\AdWords\Reporting\v201809\{ReportDefinition, ReportDownloader, DownloadFormat, ReportDefinitionDateRangeType};

class AccountPerformanceReport extends Script
{
    /**
     * @var string
     */
    private $reportFile;

    /**
     * @var array
     */
    private $columnsMapping = [
        'impressions'             => 'Impressions',
        'clicks'                  => 'Clicks',
        'cost'                    => 'Cost',
        'conversions'         => 'All conv.',
        'conversion_value'        => 'Total conv. value',
        'average_position'        => 'Avg. position',
        'search_impression_share' => 'Search Impr. share',
        'date'                    => 'Day'
    ];

    public function pull()
    {
        $for30Days = ($this->getDaysRange() == 'true') ? true : false;

        $this->printMessage("Currently running script '{$this->getName()}'");

        $script     = DB::table('scripts_details')->where('script_name', $this->getName())->first();
        $scriptId   = $script->id;
        $daysToPull = $for30Days ? 30 :$script->days_to_pull;
        $logId      = $this->log(null, $scriptId);

        $this->reportFile = "{$this->tmpDir}/AccountPerformanceReport_{$this->getAccountId()}_" . date('Y_m_d') . '.csv';

        $start = date('Y-m-d', strtotime("-{$daysToPull} days"));
        $end   = date('Y-m-d', strtotime('-1 day'));
        $this->printMessage("Data being collected for account {$this->getAccountId()} for the period {$start} - {$end}.");

        try {
            if (file_exists($this->reportFile)) {
                $this->import();
                $this->log($logId, $scriptId);
                $this->printMessage("Job has been finished for account {$this->getAccountId()}.");

                return true;
            }
            $session = $this->auth();

            $selector = (new Selector)
                ->setDateRange(new DateRange(date('Ymd', strtotime("-{$daysToPull} days")), date('Ymd', strtotime('-1 day'))))
                ->setFields([
                    'ExternalCustomerId',
                    'Impressions',
                    'Clicks',
                    'Cost',
                    'AllConversions',
                    'ConversionValue',
                    'AveragePosition',
                    'SearchImpressionShare',
                    'Date'
                ]);

            $reportDefinition = new ReportDefinition;
            $reportDefinition->setReportName('Account performance report #' . uniqid());
            $reportDefinition->setDateRangeType(ReportDefinitionDateRangeType::CUSTOM_DATE);
            $reportDefinition->setReportType(ReportDefinitionReportType::ACCOUNT_PERFORMANCE_REPORT);
            $reportDefinition->setDownloadFormat(DownloadFormat::CSV);
            $reportDefinition->setSelector($selector);

            $reportDownloader = new ReportDownloader($session);
            $reportSettingsOverride = (new ReportSettingsBuilder)
                ->includeZeroImpressions(false)
                ->build();

            $reportDownloadResult = $reportDownloader->downloadReport($reportDefinition, $reportSettingsOverride);
            $reportDownloadResult->saveToFile($this->reportFile);

            if (!file_exists($this->reportFile)) {
                throw new ScriptException("File '{$this->reportFile}' does not exist.");
            }

            $this->import();
            $this->log($logId, $scriptId);
            $this->printMessage("Job has been finished for account {$this->getAccountId()}.");

        } catch (Exception $e) {
            $this->log($logId ?? null, $scriptId, $e->getMessage());

            $this->printMessage("ERROR: {$e->getMessage()}");

            Log::error($e);
        }
    }

    private function import()
    {
        if (($handle = fopen($this->reportFile, 'r')) === FALSE) {
            throw new ScriptException("Unable to open file '{$this->reportFile}'.");
        }

        $i       = 1;
        $headers = [];
        $insert  = [];
        while (($row = fgetcsv($handle)) !== FALSE) {
            if ($row[0] === 'Total') {
                break;
            }

            if ($i <= 2) {
                if ($i === 2) {
                    $headers = array_flip($row);
                }

                ++$i;

                continue;
            }

            $insert[$i] = [
                'client_id'  => $this->getClientId(),
                'account_id' => $this->getAccountId(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            foreach ($this->columnsMapping as $dbColumn => $fileColumn) {
                $insert[$i][$dbColumn] = $row[$headers[$fileColumn]];
            }

            if ($insert[$i]['search_impression_share'] === '< 10%') {
                $insert[$i]['sis_imps'] = $insert[$i]['impressions'] / (5 / 100);
            } elseif ($insert[$i]['search_impression_share'] == 0 || $insert[$i]['search_impression_share'] == '--') {
                $insert[$i]['sis_imps'] = 0;
            } else {
                $insert[$i]['sis_imps'] = $insert[$i]['impressions'] / ((float)$insert[$i]['search_impression_share'] / 100);
            }

            ++$i;
        }

        if (!empty($insert)) {
            $update = array_merge(['updated_at', 'sis_imps'], array_keys($this->columnsMapping));
            self::insertOnDuplicateUpdate('reports_account_performance', $insert, $update);
        }

        fclose($handle);
    }
}
