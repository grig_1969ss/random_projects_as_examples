<?php

namespace App\Classes\Scripts;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\Scripts\ScriptException;
use Google\AdsApi\AdWords\{ReportSettingsBuilder, AdWordsSession};
use Google\AdsApi\AdWords\Reporting\v201809\{ReportDownloader, DownloadFormat};

class AccountAnomalyDetector extends Script
{
    /**
     * @var string
     */
    private $reportFile;

    public function pull()
    {
        $this->printMessage("Currently running script '{$this->getName()}'");

        $script   = DB::table('scripts_details')->where('script_name', $this->getName())->first();
        $scriptId = $script->id;

        $accounts = DB::table('adwords_accounts_2 as aa')
            ->select(
                'aa.account_id',
                'at.refresh_token'
            )
            ->join('admin_clients AS ac', 'aa.adwords_token_id', '=', 'ac.adwords_token_2_id')
            ->join('adwords_tokens_2 as at', 'at.id', '=', 'aa.adwords_token_id')
            ->join('client_x_adwords_accounts as cxaa', function($join)
            {
                $join->on('cxaa.admin_client_id', '=', 'ac.id');
                $join->on('cxaa.adwords_token_id', '=', 'at.id');
                $join->on('cxaa.adwords_accounts_id', '=', 'aa.id');
                $join->on('cxaa.adwords_accounts_account_id', '=', 'aa.account_id');
            })
            ->where('aa.can_manage_clients', '<>', 1)
            ->where('ac.id', $this->getClientId())
            ->where('aa.is_deleted', 0)
            ->get()
            ->toArray();

        if (empty($accounts)) {
            return false;
        }

        $logIds = [];
        foreach ($accounts as $account) {
            $logId    = $this->log(null, $scriptId, null, $this->getClientId(), $account->account_id);
            $logIds[] = $logId;

            $session = $this->auth($account->refresh_token, $account->account_id);

            $weeks                = 26;
            $impressionsThreshold = 0.8;
            $clicksThreshold      = 0.5;
            $conversionsThreshold = 0.5;
            $costThreshold        = 1.5;

            $configs = DB::table('script_x_options_selected')
                ->select(
                    'script_config_id',
                    'selection_numeric'
                )
                ->where([
                    'script_id' => $scriptId,
                    'client_id' => $this->getClientId()
                ])
                ->get();

            foreach ($configs as $config) {
                $weeks                = $config->script_config_id == 2 && (int)$config->selection_numeric ? $config->selection_numeric : $weeks;
                $impressionsThreshold = $config->script_config_id == 3 ? $config->selection_numeric : $impressionsThreshold;
                $clicksThreshold      = $config->script_config_id == 4 ? $config->selection_numeric : $clicksThreshold;
                $conversionsThreshold = $config->script_config_id == 5 ? $config->selection_numeric : $conversionsThreshold;
                $costThreshold        = $config->script_config_id == 6 ? $config->selection_numeric : $costThreshold;
            }

            $dateRange        = date('Y-m-d H:i:s', strtotime('-3 hour'));
            $dateRangeToCheck = date('Ymd', strtotime($dateRange));
            $dateRangeToCheckStart = date('Ymd', strtotime('-24 hours', strtotime($dateRangeToCheck)));

            $upToHour         = date('G', strtotime($dateRange));
            $dayOfWeek        = strtoupper(date('l', strtotime($dateRange)));
            $dateRangeToEnd   = date('Ymd', strtotime('-1 day', strtotime($dateRangeToCheckStart)));
            $dateRangeToStart = date('Ymd', strtotime("-{$weeks} week", strtotime($dateRangeToEnd)));

            $currentlyChecking = date('Y-m-d', strtotime($dateRange));
            $this->printMessage("Data being collected for account {$account->account_id} for day {$currentlyChecking}.");

            $todayQuery =
                "SELECT HourOfDay, DayOfWeek, Clicks, Impressions, Conversions, Cost
                FROM ACCOUNT_PERFORMANCE_REPORT DURING {$dateRangeToCheckStart},{$dateRangeToCheck}";

            $pastQuery =
                "SELECT HourOfDay, DayOfWeek, Clicks, Impressions, Conversions, Cost
                FROM ACCOUNT_PERFORMANCE_REPORT
                WHERE DayOfWeek = {$dayOfWeek}
                DURING {$dateRangeToStart},{$dateRangeToEnd}";

            try {

                $this->reportFile = "{$this->tmpDir}/AccountAnomalyDetector_{$account->account_id}_{$dateRangeToCheck}.csv";
                $todayStats       = $this->getReport($session, $todayQuery, $upToHour, 1, $dateRange);

                $this->reportFile = "{$this->tmpDir}/AccountAnomalyDetector_{$account->account_id}_{$dateRangeToStart}_{$dateRangeToEnd}.csv";
                $pastStats        = $this->getReport($session, $pastQuery, $upToHour, $weeks);

                if (empty($todayStats) || empty($pastStats)) {
                    continue;
                }

                $issue = false;
                if ($clicksThreshold && $todayStats['clicks'] < $pastStats['clicks'] * $clicksThreshold) {
                    $issue = true;

                } elseif ($impressionsThreshold && $todayStats['impressions'] < $pastStats['impressions'] * $impressionsThreshold) {
                    $issue = true;

                } elseif ($conversionsThreshold && $todayStats['conversions'] < $pastStats['conversions'] * $conversionsThreshold) {
                    $issue = true;

                } elseif ($costThreshold && $todayStats['cost'] > $pastStats['cost'] * $costThreshold) {
                    $issue = true;
                }

                $insert = [
                    'client_id'                => $this->getClientId(),
                    'account_id'               => $account->account_id,
                    'hour'                     => $upToHour,
                    'day_of_week'              => date('w', strtotime($dateRange)),
                    'has_issue'                => $issue,
                    'clicks_today'             => round($todayStats['clicks']),
                    'clicks_historically'      => round($pastStats['clicks']),
                    'impressions_today'        => round($todayStats['impressions']),
                    'impressions_historically' => round($pastStats['impressions']),
                    'conversions_today'        => round($todayStats['conversions']),
                    'conversions_historically' => round($pastStats['conversions'], 2),
                    'cost_today'               => round($todayStats['cost']),
                    'cost_historically'        => round($pastStats['cost']),
                    'weeks_to_look_at'         => $weeks,
                    'created_at'               => Carbon::now(),
                    'updated_at'               => Carbon::now()
                ];

                self::insertOnDuplicateUpdate('results_account_anomaly', $insert, array_diff(array_keys($insert), ['created_at']));

                $this->printMessage("Job has been finished for account {$account->account_id}.");

            } catch (Exception $e) {

                $this->log($logId ?? null, $scriptId, $e->getMessage());

                $this->printMessage("ERROR: {$e->getMessage()}");

                Log::error($e);
            }
        }

        foreach ($logIds as $logId) {
            $this->log($logId);
        }
    }

    private function getReport(AdWordsSession $session, $query, $hour, $weeks = 1, $dateRange = null)
    {
        if (!file_exists($this->reportFile)) {
            $reportDownloader = new ReportDownloader($session);

            $reportSettingsOverride = (new ReportSettingsBuilder())
                ->includeZeroImpressions(false)
                ->build();

            $reportDownloadResult = $reportDownloader->downloadReportWithAwql($query, DownloadFormat::CSV, $reportSettingsOverride);
            $reportDownloadResult->saveToFile($this->reportFile);
        }

        $result = [
            'clicks'      => 0,
            'impressions' => 0,
            'conversions' => 0,
            'cost'        => 0
        ];

        if (($handle = fopen($this->reportFile, 'r')) === FALSE) {
            throw new ScriptException("Unable to open file '{$this->reportFile}'.");
        }

        $coefficient = 1 / $weeks;
        if (!$coefficient) {
            $coefficient = 1;
        }

        $i = 0;
        $accountsByHour = [];
        while (($row = fgetcsv($handle)) !== FALSE) {
            if ($row[0] === 'Total') {
                break;
            }

            if ($i < 2) {
                $i++;
                continue;
            }

            if ($row[0] < $hour || (int) $row[0] === 0) {
                $result = [
                    'clicks'      => (int) $row[2] * $coefficient + $result['clicks'],
                    'impressions' => (int) $row[3] * $coefficient + $result['impressions'],
                    'conversions' => (int) $row[4] * $coefficient + $result['conversions'],
                    'cost'        => (float) $row[5] * $coefficient + $result['cost']
                ];

                if ((int) $weeks === 1) {
                    $accountsByHour[] = [
                        'client_id'   => $this->getClientId(),
                        'account_id'  => $session->getClientCustomerId(),
                        'hour'        => $row[0],
                        'day_of_week' => date('w', strtotime($dateRange)),
                        'clicks'      => round($row[2]),
                        'impressions' => round($row[3]),
                        'conversions' => round($row[4]),
                        'cost'        => round($row[5]),
                        'created_at'  => Carbon::now(),
                        'updated_at'  => Carbon::now()
                    ];
                }
            }

            $i++;
        }

        if (!empty($accountsByHour)) {
            self::insertOnDuplicateUpdate('results_account_by_hour', $accountsByHour, array_diff(array_keys($accountsByHour[0]), ['created_at']));
        }

        return $result;
    }
}