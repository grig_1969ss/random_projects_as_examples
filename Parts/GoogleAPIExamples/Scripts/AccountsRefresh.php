<?php

namespace App\Classes\Scripts;

use Carbon\Carbon;

use App\Classes\Google\GoogleOAuth2Exception;
use Google\AdsApi\AdWords\{
    AdWordsServices,
    AdWordsSessionBuilder
};
use Google\AdsApi\AdWords\v201809\cm\{
    Selector, OrderBy, SortOrder
};
use Google\AdsApi\AdWords\v201809\mcm\{
    CustomerService, ManagedCustomerService
};
use App\AdwordsAccountRefresh_2;
use Google\AdsApi\Common\OAuth2TokenBuilder;

class AccountsRefresh extends Script
{
    const CREDENTIALS_FILE = 'protected/adwords/adsapi_php.ini';

    public function pull()
    {
        $token  = $this->getRefreshToken();

        echo "------------------ collecting accounts for token {$token->refresh_token} ----------------------";
        echo "<br>";

        $adWordsServices = new AdWordsServices();

        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->fromFile(storage_path('app/' . self::CREDENTIALS_FILE))
            ->withRefreshToken($token->refresh_token)
            ->build();

        $session = (new AdWordsSessionBuilder())
            ->fromFile(storage_path('app/' . self::CREDENTIALS_FILE))
            ->withOAuth2Credential($oAuth2Credential)
            ->build();

        $customerService = $adWordsServices->get($session, CustomerService::class);

        $customers = $customerService->getCustomers();


        if (empty($customers)) {
            throw new GoogleOAuth2Exception('No customer found.');
        }

        foreach ($customers as $customer) {

            $session = (new AdWordsSessionBuilder)
                ->fromFile(storage_path('app/' . self::CREDENTIALS_FILE))
                ->withOAuth2Credential($oAuth2Credential)
                ->withClientCustomerId($customer->getCustomerId())
                ->build();

            $selector = (new Selector)
                ->setFields(['CustomerId', 'Name', 'CanManageClients', 'CurrencyCode', 'DateTimeZone', 'TestAccount'])
                ->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);

            $managedCustomerService = $adWordsServices->get($session, ManagedCustomerService::class);
            $customersList = $managedCustomerService->get($selector);

            $customerToManager = [];
            if (!empty($customersList->getLinks())) {
                foreach ($customersList->getLinks() as $link) {
                    $customerToManager[$link->getClientCustomerId()][] = $link->getManagerCustomerId();
                }
            }

            try {
                if (!empty($customersList->getEntries())) {
                    $insert = [];
                    $i = 0;

                    foreach ($customersList->getEntries() as $key => $entry) {
                        // A customer may have multiple managers and a manager may have multiple customers.
                        if (isset($customerToManager[$entry->getCustomerId()])) {
                            foreach ($customerToManager[$entry->getCustomerId()] as $managerCustomerId) {
                                $name = $entry->getName() ?? '"' . ' ' . '"';
                                $insert[] = [
                                    'adwords_token_id' => $token->adwords_token_id,
                                    'account_id' => $entry->getCustomerId(),
                                    'manager_customer_id' => $managerCustomerId,
                                    'name' => addslashes($name),
                                    'can_manage_clients' => $entry->getCanManageClients(),
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now()
                                ];

                                $i++;

                            }

                        } else {
                            $now = Carbon::now();
                            $name = $entry->getName() ?? '"' . ' ' . '"';
                            $insert[] = [
                                'adwords_token_id' => $token->adwords_token_id,
                                'account_id' => $entry->getCustomerId(),
                                'manager_customer_id' => -1,
                                'name' => addslashes($name),
                                'can_manage_clients' => $entry->getCanManageClients(),
                                'created_at' => $now,
                                'updated_at' => $now
                            ];

                            $i++;
                        }


                        if (!empty($insert) && count($insert) >= self::MAX_ARRAY_PER_INSERT) {
                            echo "------------------ inserting " . self::MAX_ARRAY_PER_INSERT . " accounts: ----------------------";
                            echo "<br>";

                            //trait is used in model for this method
                            AdwordsAccountRefresh_2::insertOnDuplicateKey($insert);
                            $insert = [];
                        }
                    }


                    if (!empty($insert)) {
                        echo "------------------ inserting single ones: ----------------------";
                        echo "<br>";

                        //trait is used in model for this method
                        AdwordsAccountRefresh_2::insertOnDuplicateKey($insert);
                        $insert = [];
                    }
                }
            } catch (\Exception $e) {

                print_r($e->getMessage());
            }
        }
    }
}