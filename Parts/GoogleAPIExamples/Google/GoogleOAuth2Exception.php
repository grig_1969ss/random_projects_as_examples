<?php

namespace App\Classes\Google;

class GoogleOAuth2Exception extends \Exception
{
    const DEFAULT_MCC_EXCEPTION_CODE = 1;
}
