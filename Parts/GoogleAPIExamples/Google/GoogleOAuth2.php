<?php

namespace App\Classes\Google;

use Exception;
use Google_Client;
use Carbon\Carbon;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Classes\Google\GoogleOAuth2Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Google\AdsApi\AdWords\v201809\cm\{Selector, OrderBy, SortOrder};
use Google\AdsApi\AdWords\v201809\mcm\{CustomerService, ManagedCustomerService};
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\{
    AdWordsSessionBuilder,
    AdWordsServices
};

final class GoogleOAuth2
{
    const MAX_ARRAY_PER_INSERT = 1000;

    const PROJECT_ID                  = 'starcom-ai-apis';
    const AUTH_URI                    = 'https://accounts.google.com/o/oauth2/v2/auth';
    const TOKEN_URI                   = 'https://oauth2.googleapis.com/token';
    const AUTH_PROVIDER_X509_CERT_URL = 'https://www.googleapis.com/oauth2/v1/certs';
    const CLIENT_SECRETS_FILE         = 'protected/adwords/client_secrets.json';
    const CREDENTIALS_FILE            = 'protected/adwords/adsapi_php.ini';

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $redirectUris;

    /**
     * @var string
     */
    private $javascriptOrigins;

    /**
     * @var string
     */
    private $adWordsServices;

    /**
     * @param Google\AdsApi\AdWords\AdWordsServices $adWordsServices
     */
    public function __construct(AdWordsServices $adWordsServices)
    {
        $appUrl = config('app.url');

        $this->adWordsServices = $adWordsServices;
        $this->redirectUris    = "{$appUrl}/google_oauth2_callback";

        if (!Storage::exists(self::CLIENT_SECRETS_FILE)) {
            if (!Storage::exists(self::CREDENTIALS_FILE)) {
                throw new GoogleOAuth2Exception('Credentials file were not found.');
            }

            $iniFileData = parse_ini_file(storage_path('app/' . self::CREDENTIALS_FILE), true);

            $this->clientId          = $iniFileData['OAUTH2']['clientId'];
            $this->clientSecret      = $iniFileData['OAUTH2']['clientSecret'];
            $this->javascriptOrigins = "{$appUrl}";

            Storage::put(self::CLIENT_SECRETS_FILE, json_encode(['web' => [
                'client_id'                   => $this->clientId,
                'client_secret'               => $this->clientSecret,
                'project_id'                  => self::PROJECT_ID,
                'auth_uri'                    => self::AUTH_URI,
                'token_uri'                   => self::TOKEN_URI,
                'auth_provider_x509_cert_url' => self::AUTH_PROVIDER_X509_CERT_URL,
                'redirect_uris'               => $this->redirectUris,
                'javascript_origins'          => $this->javascriptOrigins
            ]]));
        }
    }

    /**
     * User will be redirected to a Google page where the user should select an account and grant access.
     */
    public function connectMCCAccount()
    {
        $clientId  = request()->get('client_id');
        $companyId = request()->get('company_id');
        $isCompany = request()->get('company_id');

        if (empty($clientId) && empty($companyId)) {
            throw new BadRequestHttpException("Missing 'client_id'.");
        }

        if (!empty($clientId)) {
            session(['adwords_connection_client_id' => $clientId]);
        }
        if (!empty($companyId)) {
            session(['adwords_connection_company_id' => $companyId]);
        }
        if (!empty($isCompany)) {
            session(['adwords_connection_is_company' => $isCompany]);
        }

        $client = new Google_Client;
        $client->setAuthConfig(storage_path('app/' . self::CLIENT_SECRETS_FILE));
        $client->setRedirectUri($this->redirectUris);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->addScope([\Google_Service_Oauth2::USERINFO_PROFILE, 'https://www.googleapis.com/auth/adwords']);

        return redirect(filter_var($client->createAuthUrl(), FILTER_SANITIZE_URL));
    }

    /**
     * Google will redirect user to this function.
     */
    public function callback()
    {
        $clientId = session('adwords_connection_client_id') ?? 0;
        $companyId = session('adwords_connection_company_id') ?? 0;
        $isCompany = session('adwords_connection_is_company') ?? 0;

        $resourceId = $isCompany ? $companyId : $clientId;

        if (empty($resourceId)) {
            Log::error('Something went wrong - $resourceId variable is empty.');

            abort(500);
        }

        if (session()->has('adwords_connection_client_id')) {
            session()->forget('adwords_connection_client_id');
        }
        if (session()->has('adwords_connection_company_id')) {
            session()->forget('adwords_connection_company_id');
        }
        if (session()->has('adwords_connection_is_company')) {
            session()->forget('adwords_connection_is_company');
        }

        DB::beginTransaction();
        try {
            $this->proceedCallback($resourceId, $isCompany, $companyId);
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);

            if ($e instanceof GoogleOAuth2Exception && $e->getCode() === GoogleOAuth2Exception::DEFAULT_MCC_EXCEPTION_CODE) {
                return $this->failed($resourceId, $isCompany, $e->getMessage());
            }

            return $this->failed($resourceId, $isCompany);
        }

        return $this->success($resourceId, $isCompany);
    }

    /**
     * @param int $resourceId
     * @param bool $isCompany
     */
    private function proceedCallback($resourceId, $isCompany, $companyId=0)
    {
        $request = request()->all();

        if (empty($request['code'])) {
            throw new GoogleOAuth2Exception($request['error']);
        }

        $client = new Google_Client;
        $client->setAuthConfig(storage_path('app/' . self::CLIENT_SECRETS_FILE));
        $client->setRedirectUri($this->redirectUris);
        $client->setAccessType('offline');
        $client->addScope([\Google_Service_Oauth2::USERINFO_PROFILE, 'https://www.googleapis.com/auth/adwords']);

        $data = $client->fetchAccessTokenWithAuthCode($request['code']);

        $oAuth2Credential = (new OAuth2TokenBuilder)
            ->fromFile(storage_path('app/' . self::CREDENTIALS_FILE))
            ->withRefreshToken($data['refresh_token'])
            ->build();

        $session = (new AdWordsSessionBuilder)
            ->fromFile(storage_path('app/' . self::CREDENTIALS_FILE))
            ->withOAuth2Credential($oAuth2Credential)
            ->build();

        $customerService = $this->adWordsServices->get($session, CustomerService::class);
        $customers       = $customerService->getCustomers();

        if (empty($customers)) {
            throw new GoogleOAuth2Exception('No customer found.');
        }

        $companyCustomerId = DB::table('adwords_tokens_2 as at')
            ->join('admin_companies as ac', 'ac.adwords_token_2_id', 'at.id')
            ->where('ac.id', $companyId)->value('customer_id');

        $customersCount    = count($customers);

        if ($customersCount === 1 && $customers[0]->getCustomerId() == $companyCustomerId) {
            throw new GoogleOAuth2Exception(
                'This is the default MCC and is already connected with all the accounts listed below',
                GoogleOAuth2Exception::DEFAULT_MCC_EXCEPTION_CODE
            );
        }

        DB::table('adwords_accounts_2 as aa')
            ->join('admin_clients as ac', 'ac.adwords_token_2_id', 'aa.adwords_token_id')
            ->where('ac.id', $resourceId)->delete();

        $conversionTrackingSettings = null;
        $remarketingSettings        = null;

        if ($customersCount === 1) {
            $conversionTrackingSettings = [
                'effective_conversion_tracking_id'       => $customers[0]->getConversionTrackingSettings()->getEffectiveConversionTrackingId(),
                'uses_cross_account_conversion_tracking' => $customers[0]->getConversionTrackingSettings()->getUsesCrossAccountConversionTracking()
            ];

            $remarketingSettings = [
                'snippet'                => $customers[0]->getRemarketingSettings()->getSnippet(),
                'google_global_site_tag' => $customers[0]->getRemarketingSettings()->getGoogleGlobalSiteTag()
            ];
        }

        if ($isCompany) {
            $resourceName = DB::table('admin_companies')->where('id', $resourceId)->first()->company_name;
        } else {
            $resourceName = DB::table('admin_clients')->where('id', $resourceId)->first()->client_name;
        }

        $name = $customersCount === 1 ? $customers[0]->getDescriptiveName() : '';
        $adwordsTokenId = DB::table('adwords_tokens_2')->insertGetId([
            'customer_id'           => $customersCount === 1 ? $customers[0]->getCustomerId() : 0,
            'name'                  => $name ?? '',
            'refresh_token'         => $data['refresh_token'],
            'currency_code'         => $customersCount === 1 ? $customers[0]->getCurrencyCode() : null,
            'date_time_zone'        => $customersCount === 1 ? $customers[0]->getDateTimeZone() : null,
            'can_manage_clients'    => $customersCount === 1 ? $customers[0]->getCanManageClients() : null,
            'is_company'            => $isCompany ? 1 : 0,
            'connected_by'          => auth()->user()->id,
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now()
        ]);

        if ($isCompany) {
            DB::table('admin_companies')->where('id', $resourceId)->update(['adwords_token_2_id' => $adwordsTokenId]);
        } else {
            DB::table('admin_clients')->where('id', $resourceId)->update(['adwords_token_2_id' => $adwordsTokenId]);
        }

        foreach ($customers as $customer) {
            // We already have all accounts for default customer.
            if ($customer->getCustomerId() == $companyCustomerId) {
                continue;
            }

            $session = (new AdWordsSessionBuilder)
                ->fromFile(storage_path('app/' . self::CREDENTIALS_FILE))
                ->withOAuth2Credential($oAuth2Credential)
                ->withClientCustomerId($customer->getCustomerId())
                ->build();

            $selector = (new Selector)
                ->setFields(['CustomerId', 'Name', 'CanManageClients', 'CurrencyCode', 'DateTimeZone', 'TestAccount'])
                ->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);

            $managedCustomerService = $this->adWordsServices->get($session, ManagedCustomerService::class);
            $customersList          = $managedCustomerService->get($selector);

            $customerToManager = [];
            if (!empty($customersList->getLinks())) {
                foreach ($customersList->getLinks() as $link) {
                    $customerToManager[$link->getClientCustomerId()][] = $link->getManagerCustomerId();
                }
            }

            if (!empty($customersList->getEntries())) {
                $insert = [];
                $i      = 0;

                foreach ($customersList->getEntries() as $entry) {
                    // A customer may have multiple managers and a manager may have multiple customers.
                    if (isset($customerToManager[$entry->getCustomerId()])) {
                        foreach ($customerToManager[$entry->getCustomerId()] as $managerCustomerId) {
                            $insert[$i] = [
                                'adwords_token_id'      => $adwordsTokenId,
                                'account_id'            => $entry->getCustomerId(),
                                'name'                  => $entry->getName(),
                                'manager_customer_id'   => $managerCustomerId,
                                'can_manage_clients'  => $entry->getCanManageClients(),
                                'created_at'          => Carbon::now(),
                                'updated_at'          => Carbon::now()
                            ];

                            $i++;
                        }
                    } else {
                        $insert[$i] = [
                            'adwords_token_id'      => $adwordsTokenId,
                            'account_id'            => $entry->getCustomerId(),
                            'name'                  => $entry->getName(),
                            'manager_customer_id'   => null,
                            'can_manage_clients'    => $entry->getCanManageClients(),
                            'created_at'            => Carbon::now(),
                            'updated_at'            => Carbon::now()
                        ];

                        $i++;
                    }

                    if (!empty($insert) && $i >= self::MAX_ARRAY_PER_INSERT) {
                        DB::table('adwords_accounts_2')->insert($insert);
                        $insert = [];
                    }
                }

                if (!empty($insert)) {
                    DB::table('adwords_accounts_2')->insert($insert);
                    $insert = [];
                }
            }
        }
    }

    /**
     * @param int $resourceId
     * @param bool $isCompany
     * @param string|null $message
     */
    private function failed($resourceId, $isCompany, $message = null)
    {
        $message = $message ?? 'There was an error during the process so it could not be completed. Please use the REPORT AN ERROR process to explain what has happened.';

        if ($isCompany) {
            return redirect(config('app.url') . "/admin-companies?active={$resourceId}")->with('message', $message);
        } else {
            return redirect(config('app.url') . "/admin/brands/{$resourceId}/edit")->with('message', $message);
        }
    }

    /**
     * @param int $resourceId
     * @param bool $isCompany
     */
    private function success($resourceId, $isCompany)
    {
        if ($isCompany) {
            return redirect(config('app.url') . "/admin-companies?active={$resourceId}");
        } else {
            return redirect(config('app.url') . "/admin/brands/{$resourceId}/edit");
        }
    }
}
