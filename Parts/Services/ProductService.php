<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Event;
use App\Models\Order;
use App\Models\Product;
use App\Models\Whom;

use App\Traits\ProductServiceTrait;
use App\Traits\SheetTrait;
use Illuminate\Support\Facades\File;
use Sheets;
use Slugify;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Mockery\Exception;

/**
 * Class ProductService
 */
class ProductService implements \App\Interfaces\ProductInterface
{

    use ProductServiceTrait, SheetTrait;

    /**
     * @var Product
     */
    protected $model;

    /**
     * @var CRMService
     */
    protected $crmService;

    const productPerPage = 18;

    const productKeys = [
        'group' => 0,
        'name' => 2,
        'static_param' => 3,
        'dynamic_param' => 4,
        'content' => 12,
        'price' => 6,
        'img' => 13,
        'additional_products' => 9,
        'in_store' => 5,
        'short_description_1' => 10,
        'short_description_2' => 11,
        'age' => 8,
        'gender' => 7
    ];

    const categoryKey = [
        'name' => 1,
    ];
    const categoryImageKeys = [
        'name' => 0,
        'image' => 1,
        'slider' => 2,
    ];

    const eventKey = [
        'name' => 12,
    ];

    const sendKey = [
        'name' => 13
    ];

    const genders = [
        'woman' => 0,
        'man' => 1
    ];
    /**
     * ProductService constructor.
     * @param Product $product
     * @param CRMService $crmService
     */
    public function __construct(Product $product, CRMService $crmService)
    {
        $this->model = $product;
        $this->crmService = $crmService;
    }

    /**
     * @param $data
     */
    public function index($data)
    {
        $data = $this->parseData($data);
        $this->insertSql($data);
        $this->parseImages();

        exit('done');
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function get($data, $categoryName = null)
    {
        $query = $this->getProductQuery();

        if ($categoryName) {
            $query->whereHas('category', function (Builder $query) use ($categoryName) {
                $query->where('name_slug', $categoryName);
            });
        }
        $images = Category::where('name_slug', $categoryName)
            ->first()
            ->images
            ->toArray();

        if (Arr::get($data, 'filters')) {
            $data['filters'] = json_decode($data['filters'], true);
            $query = $this->filterProductQuery($query, $data['filters']);
        }
        $query = $query->with(['image']);

        return [
            'products' => $query->simplePaginate(self::productPerPage),
            'images' => $images
        ];
    }

    /**
     * @return mixed
     */
    protected function getProductQuery()
    {
        return $this->model
            ->select('products.*', DB::raw("group_concat(if (in_store=0, null, dynamic_param)) as params"))
            ->where(function ($query) {
                $query->where('in_store','!=','0')
                    ->orWhereNull('in_store');
            })
            ->orderBy('id', 'asc')
            ->groupBy('group');
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getSingle($data)
    {
        return $this->model->where($data)->first()->load('image','images');
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    protected function parseData($data)
    {
        $response = $this->getSheet()->get();

        return $response;
    }

    /**
     *
     */
    public function parseImages()
    {
        $response = $this->getImageSheet()->get();
        $this->parseCategoryImages($response);

        exit('done');
    }

    /**
     * @param $data
     */
    protected function parseCategoryImages($data)
    {
        unset($data[0]);
        $this->showRoomImages($data[1]);
        unset($data[1]);
        $categories = Arr::pluck($data,'0');

        $notExistingCategories = Category::whereNotIn('name', $categories)->get();
        foreach ($notExistingCategories as $category) {
            $category->delete();
        }

        foreach ($data as $key => $row) {
            $output = preg_replace('/\s+/', ' ', $row[self::categoryImageKeys['name']]);
            $category = Category::where('name', trim(mb_strtoupper($output)));

            $category->update(
                [
                    'image' => isset($row[self::categoryImageKeys['image']]) ? 'categories/' . $row[self::categoryImageKeys['image']] : 'categories/default.jpg',
                    'index' => $key
                ]);
            $images = isset($row[self::categoryImageKeys['slider']]) ? explode(',', $row[self::categoryImageKeys['slider']]) : ['default_slide.jpg'];
            $this->insertImages($category->first(), $images);
        }

    }

    /**
     * @param $data
     */
    protected function showRoomImages($data)
    {
        if (! File::exists(database_path('/seeds/sliderImages.json'))) {
            File::put(database_path('/seeds/sliderImages.json'), '');
        }
        file_put_contents(database_path('/seeds/sliderImages.json'), trim(json_encode($data)));
    }
    /**
     * @param $category
     * @param $paths
     */
    protected function insertImages($category, $paths)
    {
        $images = [];
        foreach ($paths as $path) {
            $images[] = ['name' => 'categories/' . trim($path)];
        }

        if ($category) {
            $category->images()->delete();
            $category->images()->createMany($images);
        }
    }

    /**
     * @param $data
     */
    public function insertSql($data)
    {
        //delete table headers
        unset($data[0]);

        try {
            DB::beginTransaction();
            $ids = [];
            foreach ($data as $key => $row) {
                $output = preg_replace('/\s+/', ' ', $row[self::categoryKey['name']]);
                $categoryFields = [
                    'name' =>trim(mb_strtoupper($output)),
                    'name_slug' => Slugify::slugify(($row[self::categoryKey['name']]))
                ];

                $filterEvent = isset($row[self::eventKey['name']]) ? explode('/', $row[self::eventKey['name']]) : [];
                $filterTo = isset($row[self::sendKey['name']]) ? explode('/', $row[self::sendKey['name']]) : [];
                $productImages = Arr::get($row, self::productKeys['img']) ? explode(',', $row[self::productKeys['img']]) : ['/nofoto.jpg'];

                $productFields = $this->getProductInsertData($row);

                $category = Category::firstOrCreate($categoryFields);

                $productFields['category_id'] = $category['id'];
                $productFields['id'] = $key;
                $ids[] = $key;
                $products = $this->model->updateOrCreate(
                    Arr::only($productFields, ['id']),
                    Arr::except($productFields, ['id'])
                );
                $images  = $this->insertFilters($productImages, $products->images());

            }
            $this->model->whereNotIn('id', $ids)->delete();
            DB::commit();

        } catch (Exception $exception) {
            DB::rollBack();
            Log::info($exception->getMessage());
        }
    }

    /**
     * @param $row
     * @return array
     */
    public function getProductInsertData($row)
    {
        $slug = Slugify::slugify($row[self::productKeys['name']] . ' ' . $row[self::productKeys['dynamic_param']]);
        $gender = null;
        if (Arr::get($row, self::productKeys['gender'])) {
            $gender = strpos(Arr::get($row, self::productKeys['gender']), 'м') !== false ?
                self::genders['man'] :
                self::genders['woman'];
        }

        return [
            'group' => Arr::get($row, self::productKeys['group']),
            'name' => Arr::get($row, self::productKeys['name']),
            'name_slug' => $slug,
            'static_param' => Arr::get($row, self::productKeys['static_param']),
            'dynamic_param' => trim( Arr::get($row,self::productKeys['dynamic_param'])),
            'content' =>  Arr::get($row, self::productKeys['content']),
            'price' => Arr::get($row, self::productKeys['price']) == '####' ? null : Arr::get($row, self::productKeys['price']),
            'additional_products' => Arr::get($row, self::productKeys['additional_products']),
            'in_store' => (isset($row[self::productKeys['in_store']]) and $row[self::productKeys['in_store']]!= '') ? $row[self::productKeys['in_store']] : null,
            'short_description_1' => Arr::get($row, self::productKeys['short_description_1']),
            'short_description_2' => Arr::get($row, self::productKeys['short_description_2']),
            'age' => Arr::get($row, self::productKeys['age']),
            'gender' => $gender,
        ];
    }

    /**
     * @param $filters
     * @return array
     */
    public function insertFilters($filters, $query)
    {
        $filterIds = [];
        $query->delete();

        foreach ($filters as $filter) {
            $test = clone $query;

            if ($filter) {
                $data = $test->firstOrCreate(['name' => trim($filter)]);
                $filterIds[] = $data->id;
            }
        }

        return $filterIds;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function makeOrder($data)
    {
        $orders = [];

        foreach ($data['products'] as $product) {
            $orderData = Arr::only($product, ['id', 'group', 'count']);
            $orderData['product_id'] = $orderData['id'];
            $orderData['phone'] = $data['phone'];

            unset($orderData['id']);
            $orders[] = $orderData;
        }

        $inserted = Order::insert($orders);

        return $inserted;
    }

    /**
     * @return array[]
     */
    public function ageFilters()
    {
        return [
            [
                'id' => 9,
                'name' => 'до 9 лет'
            ],
            [
                'id' => 10,
                'name' => '10 - 15'
            ],
            [
                'id' => 16,
                'name' => '16 - 20'
            ],
            [
                'id' => 21,
                'name' => '21 - 25'
            ],
            [
                'id' => 26,
                'name' => '26 - 30'
            ],
            [
                'id' => 31,
                'name' => '31 - 35'
            ],
            [
                'id' => 36,
                'name' => '36 и старше'
            ],
        ];

    }

    /**
     * @return array[]
     */
    protected function genderFilters()
    {
        return [
            [
                'id' => 0,
                'name' => 'Ж'
            ],
            [
                'id' => 1,
                'name' => 'М'
            ],
        ];
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function getFilters($data)
    {
        $costFilters = $this->costFilters();
        $ageFilters = $this->ageFilters();
        $genderFilters = $this->genderFilters();


        return [
            'costFilters' => [
                'header' => 'Бюджет',
                'list' => $costFilters
            ],
            'genderFilters' => [
                'header' => 'Пол',
                'list' => $genderFilters
            ],
            'ageFilters' => [
                'header' => 'Возраст',
                'list' => $ageFilters
            ],
        ];

    }

    /**
     * @param $query
     * @param $data
     * @return mixed
     */
    protected function filterProductQuery($query, $data)
    {

        if (Arr::get($data, 'costFilters')) {

            $query->havingRaw('price <=?', [$data['costFilters']]);
        }
        if(isset($data['genderFilters'])) {
            Log::info($data['genderFilters']);
            $query->havingRaw('gender =?', [ $data['genderFilters']]);
        }
        if(Arr::get($data,'ageFilters')) {
            $query->where('age', 'like', '%'. $data['ageFilters'].'%');
        }


        return $query;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function ideaProducts($data)
    {
        $query = $this->getProductQuery();
        $query = $this->filterProductQuery($query, $data);
        $response = $query->with('image')->get();

        return $response->random($response->count() < 10 ? $response->count() : 10);
    }

    /**
     * @return array
     */
    protected function eventFilters()
    {
        return Event::query()
            ->select('id', 'name')
            ->get()
            ->toArray();
    }

    /**
     * @return array
     */
    protected function toFilters()
    {
        return Whom::query()
            ->select('id', 'name')
            ->get()
            ->toArray();
    }

    /**
     * @return array
     */
    protected function costFilters()
    {
        return [
            [
                'id' => '500',
                'name' => 'до 500р'
            ],
            [
                'id' => '1000',
                'name' => 'до 1000р'
            ],
            [
                'id' => '1500',
                'name' => 'до 1500р'
            ],
            [
                'id' => '2000',
                'name' => 'до 2000р'
            ],
            [
                'id' => '2500',
                'name' => 'до 2500р'
            ],
            [
                'id' => '3000',
                'name' => 'до 3000р'
            ],
            [
                'id' => '5000',
                'name' => 'до 5000р'
            ],
        ];
    }

    /**
     * @param $data
     * @param $slug
     * @return mixed
     */
    public function show($data, $slug)
    {
        $data = $this->model
            ->select('*')
            ->where('name_slug', $slug)
            ->first()
            ->load('images')
            ->toArray();

        $params = $this->model
            ->select('id', DB::raw("group_concat(if (in_store=0, null, dynamic_param)) as params"))
            ->groupBy('group')
            ->having('group', $data['group'])
            ->first();

        $data['params'] = $params->params;

        return $data;
    }

    /**
     * @return array
     */
    public function exportCrm()
    {
        $categories = Category::all();
        $products = $this->model->all();

        $contents = View::make('export.retail')->with('data', compact('categories', 'products'));
        $response = response()->make($contents);
        $response->header('Content-Type', 'text/xml');

        return $response;
    }


    /**
     * @param $data
     * @return mixed
     */
    public function offers($data)
    {
        $query = $this->getProductQuery();

        if (Arr::get($data, 'productIds')) {
            $offerIds = $this->getOffers($data);

            return $this->model->select('products.*',  DB::raw("group_concat(if (in_store=0, null, dynamic_param)) as params"))
                ->whereIn('id', $offerIds)
                ->groupBy('group')
                ->get()
                ->load('image')
                ->toArray();
        }

        return $query
            ->get()
            ->random(8)
            ->load('image')
            ->toArray();
    }


}

