<?php
namespace App\Services;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

/**
 * Class ProductService
 */
class CRMService {

    /**
     * @var \RetailCrm\ApiClient
     */
    protected $client;
	
    protected $backCall = 'ОБРАТНЫЙ ЗВОНОК';

    /**
     * CRMService constructor.
     */
    public function __construct()
    {
        $this->client = new \RetailCrm\ApiClient(
            config('app.retailCrmUrl'),
            config('app.retailCrm'),
            \RetailCrm\ApiClient::V5
        );
    }

    /**
     * @return mixed
     */
    public function export($data)
    {
       $item = $this->getProducts($data);

       return $item;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function exportOrder($data)
    {
        $item = $this->getOrders($data);

        $response = $this->client->request->ordersCreate([
            'phone' => $data['phone'],
            'items' => $item,
            'countryIso' => 'RU',
            'firstName' => 'client'
        ]);

        return  $response['success'];
    }

    /**
     * @param $data
     * @return array
     */
    public function getOrders($data)
    {
        $transformed = $this->transformCart($data);

        foreach ($data['products'] as $product) {
            array_unshift($transformed, $this->transformSingle($product));
        }

        return $transformed;
    }

    /**
     * @param $data
     * @return array
     */
    protected function transformCart($data)
    {
        $transformed = [];

        if (Arr::get($data, 'cart')) {
            foreach ($data['cart'] as $product) {
                $data = $this->transformSingle($product);
                $data['properties'][] = [
                    'name' => 'from',
                    'value' => 'cart'
                ];
                $transformed[] = $data;
            }
        }

        return $transformed;
    }

    /**
     * @param $product
     * @return array
     */
    public function transformSingle($product)

        return [
            'quantity' => $product['count'],
            'purchasePrice' => $product['price'],
            'initialPrice' => $product['price'],
            'productName' => $product['name'] . $product['dynamic_param'],
        ];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function saveNumber($data)
    {
        $transformed = $this->transformCart($data);
        $callData = [
            'productName' => $this->backCall,
        ];
        array_unshift($transformed, $callData);

        $response = $this->client->request->ordersCreate([
            'phone' => $data['phone'],
            'items' => $transformed,
            'firstName' => 'client'
        ]);

        return $response['success'];
    }

}

